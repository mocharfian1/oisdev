<?php

  /*
   *---------------------------------------------------
   * Nama Project               : E-Checklist (OIS)
   * Pemilik                    : PT Toyota Astra Motor
   * Nama Pengembang            : Ayub Anggara
   * Perusahaan Pengembang      : AFEDIGI
   * Tanggal Pengembangan       : 18 9 2018
   *---------------------------------------------------
   * Copyright (C) 2018 AFEDIGI - All Rights Reserved
   * You may use, distribute and modify this code under the
   * terms of the license or permission from AFEDIGI.
   * For Contact Person please visit : https://afedigi.com/
   */

  /**
  * A simple class which handles loading multiple-databases in codeigniter
  */

  class DatabaseLoader {

         public function __construct() {
                 $this->load();
         }

         /**
          * Load the databases and ignore the old ordinary CI loader which only allows one
          */
         public function load() {
                 $CI =& get_instance();

                 $CI->db = $CI->load->database('default', TRUE);
                 $CI->log_db = $CI->load->database('log_db', TRUE);
         }

  }
?>
