<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MInspectionSubArea extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_project_equipment_and_tools($id_project)
    {
        //return $this->db->query("select * from tbl_project_config_equipment_and_tools where id_project='$id_project'");
        return $this->db
            ->from('tbl_project_config_equipment_and_tools')
            ->where('id_project', $id_project)
            ->get();
    }

    public function read_project_pic_kpd($id_project)
    {
        //return $this->db->query("select * from tbl_project_kpd_pic where id_project='$id_project' and is_delete='0' order by id");
        return $this->db
            ->from('tbl_project_kpd_pic')
            ->where('id_project', $id_project)
            ->order_by('id')
            ->get();
    }

    public function read_ic_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_ic');
    }
    public function read_ket_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_ket');
    }

    public function read_km_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_km');
    }

    public function read_pic_where($id, $jenis)
    {
        return $this->db
            ->select('*,tbl_project_sa_config_pic.id')
            ->join('tbl_project_area_pic', 'tbl_project_area_pic.id=tbl_project_sa_config_pic.id_project_area_pic', 'left')

            ->where('tbl_project_sa_config_pic.id_project_area', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_pic');
    }

    public function read_pic_area_where($id)
    {
        return $this->db
            ->select('*')
            ->where('id_project_area', $id)
            ->where('is_delete', false)
            ->get('tbl_project_area_pic');
    }

    public function read_kp_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_kp');
    }
    public function read_tps_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_tps_line');
    }

    public function read_minreq_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->order_by('position', 'ASC')
            ->get('tbl_project_sa_config_minreq');
    }

    public function read_catatan($id, $jenis, $user_type)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->where('user_type =', $user_type)
            ->get('tbl_project_catatan');
    }
    public function read_catatan_lainnya($id, $jenis, $user_type)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->where('user_type =', $user_type)
            ->get('tbl_project_catatan_lainnya');
    }

    public function insert_score_ic($id, $data)
    {
        return $this->db->where('id =', $id)
            ->update('tbl_project_sa_config_ic', $data);
    }

    public function insert_score_kp($id, $data)
    {
        return $this->db->where('id =', $id)
            ->update('tbl_project_sa_config_kp', $data);
    }

    public function insert_score_km($id, $data)
    {
        return $this->db->where('id =', $id)
            ->update('tbl_project_sa_config_km', $data);
    }

    public function insert_pic($id, $data)
    {
        $this->delete_pic($id);
        return $this->db->where('id =', $id)
            ->update('tbl_project_sa_config_pic', $data);
    }
    public function delete_pic($id_config_pic)
    {
        $pic = $this->db->where('id', $id_config_pic)->get('tbl_project_sa_config_pic');
        if ($pic->num_rows() > 0) {
            $pic                 = $pic->row();
            $id_project_area_pic = $pic->id_project_area_pic;

            $this->db->where('id', $id_project_area_pic)->update('tbl_project_area_pic', ['is_delete' => '1']);
            #print_r($this->db);
        }
    }
    public function update_catatan($id, $jenis, $catatan, $user_type)
    {
        $where = ['id_project_area' => $id, 'jenis_epm' => $jenis, 'user_type' => $user_type];
        $qry   = $this->db->where($where)->get('tbl_project_catatan');
        if ($qry->num_rows() > 0) {
            return $this->db->where($where)
                ->update('tbl_project_catatan', ['catatan' => $catatan]);
        } else {
            $data            = $where;
            $data['catatan'] = $catatan;
            return $this->db->insert('tbl_project_catatan', $data);
        }
    }
    public function update_catatan_lainnya($id, $jenis, $catatan, $user_type)
    {
        $where = ['id_project_area' => $id, 'jenis_epm' => $jenis, 'user_type' => $user_type];
        $qry   = $this->db->where($where)->get('tbl_project_catatan_lainnya');
        if ($qry->num_rows() > 0) {
            return $this->db->where($where)
                ->update('tbl_project_catatan_lainnya', ['catatan' => $catatan]);
        } else {
            $data            = $where;
            $data['catatan'] = $catatan;
            return $this->db->insert('tbl_project_catatan_lainnya', $data);
        }
    }

    public function read_evaluasi_kunjungan($id_project, $jenis_epm)
    {
        return $this->db->where('id_project =', $id_project)
            ->where('jenis_epm =', $jenis_epm)
            ->get('tbl_evaluasi_kunjungan');
    }

    public function read_evaluasi_detail($id, $id_catatan)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('id_project_catatan =', $id_catatan)
            ->get('tbl_evaluasi_kunjungan_detail');
    }

    public function insert_evaluasi_detail($data_evaluasi_detail)
    {
        return $this->db->insert('tbl_evaluasi_kunjungan_detail', $data_evaluasi_detail);
    }

    public function update_evaluasi_detail($id_evaluasi, $data_evaluasi_detail)
    {
        return $this->db->where('id =', $id_evaluasi)
            ->update('tbl_evaluasi_kunjungan_detail', $data_evaluasi_detail);
    }

    public function read_id_project($id)
    {
        return $this->db
            ->select('tbl_project.*,tbl_project_area.*,tbl_sub_area.*,tbl_area.*,user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3')
            ->where('tbl_project_area.id =', $id)
            ->join('tbl_area', 'tbl_area.id=tbl_project_area.id_area')
            ->join('tbl_sub_area', 'tbl_sub_area.id=tbl_project_area.id_sub_area')
            ->join('tbl_project', 'tbl_project.id=tbl_project_area.id_project')
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3')
            ->get('tbl_project_area');
    }
    public function is_locked($data_project, $user_type)
    {
        if ($user_type == $data_project->type_level_1) {
            if ($data_project->lock_user_level_1 == 1) {
                return true;
            }
        } elseif ($user_type == $data_project->type_level_2) {
            if ($data_project->lock_user_level_2 == 1) {
                return true;
            }
        } elseif ($user_type == $data_project->type_level_3) {
            if ($data_project->lock_user_level_3 == 1) {
                return true;
            }
        }
        return false;
    }
    public function save_photo_data($file_name, $id_project_area, $type)
    {

        //filepath URL
        $fileUrlPath = base_url('adminx/upload/' . $file_name);

        $app_status = APP_STATUS;
        if ($app_status == 'local') {
            $fileTruePath = FCPATH . "adminx/upload\\" . $file_name; //for local develop
        } else if ($app_status == 'live') {
            $fileTruePath = FCPATH . "adminx/upload/" . $file_name; //for hosting upload
        }

        $data = array(
            'id_project_area' => $id_project_area,
            'file_path'       => $fileUrlPath,
            'file_true_path'  => $fileTruePath,
            'file_name'       => $file_name,
            'type'            => $type,

        );
        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        $smodule  = 'back end project picture';
        $activity = 'Change Project Picture Data';
        $tbl_name = 'tbl_project_area_pic';
        $action   = 'insert project picture data';

        $this->db->trans_begin();
        $this->db->insert('tbl_project_area_pic', $data);
        $id = $this->db->insert_id();

        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $addtional_information = '';

            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }
            $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            $response = array(
                "status"  => 'error',
                "message" => 'Error: Update data failed',
            );

        } else {

            $addtional_information = '';

            $this->db->trans_commit();

            //insert update log
            $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            $response = array(
                'id'        => $id,
                'file_path' => $fileUrlPath,
                "status"    => 'success',
                "message"   => 'Update data success',
            );
        }
        return $response;

    }

}
