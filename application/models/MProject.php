<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MProject extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_project($id_user)
    {
        return $this->db
            ->select("tbl_project.*,user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3")
            ->group_start()
            ->where('tbl_project.id_user_level_1 =', $id_user)
            ->or_where('tbl_project.id_user_level_2 =', $id_user)
            ->or_where('tbl_project.id_user_level_3 =', $id_user)
            ->group_end()
            ->where('tbl_project.is_delete =', 0)
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3')
            ->get('tbl_project');
    }

    public function get_data_project($id_project)
    {
        return $this->db
            ->select("tbl_project.*, user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3")
            ->where('tbl_project.id =', $id_project)
            ->where('tbl_project.is_delete =', 0)
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3')
            ->get('tbl_project');
    }

}
