<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MArea extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_project_area($id_project)
    {
        $this->db->select('tbl_project_area.id, tbl_project.jenis_epm, tbl_project_area.is_delete, tbl_project_area.id_project, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_sub_area.description')
            ->from('tbl_project_area')
            ->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left')
            ->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left')
            ->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left')
            ->where('id_project =', $id_project)
            ->where('tbl_project_area.is_delete =', 0)
            ->where('tbl_project.is_delete =', 0)
            ->order_by('tbl_project_area.position')
            ->group_by('id_area');
        return $this->db->get();
    }

    public function data_project_sub_area($id_project)
    {
        $this->db->select('tbl_project_area.id, tbl_project_area.position, tbl_project.jenis_epm, tbl_sub_area.is_delete, tbl_project_area.id_project, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_sub_area.description')
            ->from('tbl_project_area')
            ->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left')
            ->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left')
            ->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left')
            ->where('id_project', $id_project)
            ->where('tbl_project_area.is_delete', 0)
            ->where('tbl_project.is_delete', 0)
            ->order_by('tbl_project_area.position');
        return $this->db->get();
    }

    public function data_project_equipment_and_tools($id_project)
    {
        // return $this->db->query("select * from tbl_project_config_equipment_and_tools as peat
        //                       where peat.is_delete = 0 and peat.id_project = '$id_project' and peat.jenis_epm = (select p.jenis_epm from tbl_project as p where id='$id_project')");
        return $this->db
            ->from('tbl_project_config_equipment_and_tools peat')
            ->where('peat.is_delete', 0)
            ->where('peat.id_project', $id_project)
            ->where("peat.jenis_epm = (SELECT p.jenis_epm FROM tbl_project as p WHERE id='$id_project')", null, false)
            ->get();
    }

    public function get_data_project($id_project)
    {
        //return $this->db->query("select * from tbl_project where id='$id_project'");
        return $this->db
            ->where('id', $id_project)
            ->get("tbl_project");
    }

    public function checking_project_ic_input($id_project_area, $user_type)
    {
        if ($user_type == 'konsultan') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_ic where id_project_area='$id_project_area' and score_konsultan IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_ic')
                ->where('id_project_area', $id_project_area)
                ->where('score_konsultan IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'dealer') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_ic where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_ic')
                ->where('id_project_area', $id_project_area)
                ->where('score_dealer IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'tam') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_ic where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_ic')
                ->where('id_project_area', $id_project_area)
                ->where('score_tam IS NOT NULL', null, false)->get()->row()->total;
        }
    }

    public function checking_project_km_input($id_project_area, $user_type)
    {
        if ($user_type == 'konsultan') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_km where id_project_area='$id_project_area' and score_konsultan IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_km')
                ->where('id_project_area', $id_project_area)
                ->where('score_konsultan IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'dealer') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_km where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_km')
                ->where('id_project_area', $id_project_area)
                ->where('score_dealer IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'tam') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_km where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;
            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_km')
                ->where('id_project_area', $id_project_area)
                ->where('score_tam IS NOT NULL', null, false)->get()->row()->total;
        }
    }

    public function checking_project_kp_input($id_project_area, $user_type)
    {
        if ($user_type == 'konsultan') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_kp where id_project_area='$id_project_area' and score_konsultan IS NOT NULL")->row()->total;

            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_kp')
                ->where('id_project_area', $id_project_area)
                ->where('score_konsultan IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'dealer') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_kp where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;

            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_kp')
                ->where('id_project_area', $id_project_area)
                ->where('score_dealer IS NOT NULL', null, false)->get()->row()->total;
        }

        if ($user_type == 'tam') {
            //return $this->db->query("select count(id) as total from tbl_project_sa_config_kp where id_project_area='$id_project_area' and score_dealer IS NOT NULL")->row()->total;

            return $this->db
                ->select("count(id) as total")
                ->from('tbl_project_sa_config_kp')
                ->where('id_project_area', $id_project_area)
                ->where('score_tam IS NOT NULL', null, false)->get()->row()->total;
        }
    }

    public function data_project_pic($id)
    {
        return $this->db
            ->select('*')
            ->where('id_project', $id)
            ->where('is_delete', '0')
            ->get('tbl_project_pic');
    }

    public function save_photo_data($file_name, $id_project, $type)
    {

        //filepath URL
        $fileUrlPath = base_url('adminx/upload/' . $file_name);

        $app_status = APP_STATUS;
        $date       = date('Ymd');
        if ($app_status == 'local') {
            $fileTruePath = FCPATH . "adminx/upload\\" . $file_name; //for local develop
        } else if ($app_status == 'live') {
            $fileTruePath = FCPATH . "adminx/upload/" . $file_name; //for hosting upload
        }

        $data = array(
            'id_project'     => $id_project,
            'file_path'      => $fileUrlPath,
            'file_true_path' => $fileTruePath,
            'file_name'      => $file_name,
            'type'           => $type,
        );
        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        $smodule  = 'back end project picture';
        $activity = 'Change Project Picture Data';
        $tbl_name = 'tbl_project_pic';
        $action   = 'insert project picture data';
        $this->db->trans_begin();

        $this->db->insert('tbl_project_pic', $data);
        $id = $this->db->insert_id();

        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $addtional_information = '';

            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }
            $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            $response = array(
                "status"  => 'error',
                "message" => 'Error: Update data failed',
            );

        } else {

            $addtional_information = '';

            $this->db->trans_commit();

            //insert update log
            $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            $response = array(
                'id'        => $id,
                'file_path' => $fileUrlPath,
                "status"    => 'success',
                "message"   => 'Update data success',
            );
        }
        return $response;

    }
}
