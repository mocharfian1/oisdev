<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MHome extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_project($id_user)
    {
        return $this->db->group_start()
            ->where('id_user_level_1 =', $id_user)
            ->or_where('id_user_level_2 =', $id_user)
            ->or_where('id_user_level_3 =', $id_user)
            ->group_end()
            ->where('is_delete =', 0)
            ->get('tbl_project');
    }

}
