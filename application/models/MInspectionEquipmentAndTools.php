<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MInspectionEquipmentAndTools extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function data_project_equipment_and_tools($id_project, $jenis_epm)
    {
        //return $this->db->query("select * from tbl_project_config_equipment_and_tools where id_project='$id_project' and jenis_epm='$jenis_epm' and is_delete='0'");
        return $this->db
            ->from('tbl_project_config_equipment_and_tools')
            ->where('id_project', $id_project)
            ->where('jenis_epm', $jenis_epm)
            ->where('is_delete', 0)
            ->get();
    }

    public function update_data_project_equipment_and_tools($id_project_eat, $data)
    {
        return $this->db->where('id =', $id_project_eat)
            ->update('tbl_project_config_equipment_and_tools', $data);
    }

}
