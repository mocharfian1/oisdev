<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MManPowerList extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function read_project_man_power($id_project, $jenis_epm)
    {
        //return $this->db->query("select mp.*, lt.level_training_name from tbl_project_man_power as mp
          //                      join tbl_project_level_training as lt on lt.id = mp.id_man_power_level_training
            //                    where mp.is_delete='0' and mp.id_project='$id_project' and jenis_epm='$jenis_epm'");
        return $this->db
            ->select('mp.*, lt.level_training_name')
            ->from('tbl_project_man_power mp')
            ->join('tbl_project_level_training lt', 'lt.id = mp.id_project_level_training')
            ->where('mp.is_delete', 0)
            ->where('mp.id_project', $id_project)
            ->where('jenis_epm', $jenis_epm)->get();
    }

}
