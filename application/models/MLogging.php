<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 23 02 2019
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

/*===========================================USAGE==================================================

just use this smodule name on the list for your smdule parameter to automatically adjust table name

SMODULE GUIDE
=========================================Master Table===============================================

smodule                         = master table name             = detail table name
--------------------------------------------------------------------
back end project picture        = be_project_picture            = be_project_picture_detail
back end evaluasi kunjugan      = be_evaluasi_kunjungan         = be_evaluasi_kunjungan_detail
back end area                   = be_area                       = be_area_detail
back end sub area               = be_sub_area                   = be_sub_area_detail
back end user                   = be_user                       = be_user_detail
back end guidance toss          = be_guidance_toss              = be_guidance_toss_detail
back end toss database          = be_toss_database              = be_toss_database_detail
back end toss dealer            = be_toss_dealer                = be_toss_dealer_detail
back end project level training = be_project_level_training     = be_project_level_training_detail
back end project template       = be_project_template           = be_project_template_detail
back end project                = be_project                    = be_project_detail
change password                 = change_password               = change_password_detail
login                           = login                         = (N/A)
logout                          = logout                        = (N/A)
front end project               = fe_project                    = fe_project_detail
front end toss                  = fe_toss                       = fe_toss_detail

===================================================================================================*/

defined('BASEPATH') or exit('No direct script access allowed');

class MLogging extends CI_Model
{
    private $username;
    public function __Construct()
    {
        $ci             = &get_instance();
        $this->username = $ci->session->userdata('username');
    }
    private function get_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        $ip = $this->input->ip_address() . ' / ' . gethostbyaddr($ipaddress);

        return $ip;

    }

    public function generate_log_id($smodule)
    {

        $query = $this->log_db->query('call master_data_count("' . $smodule . '")');


        //catch output before result is being freed
        $log_count = $query->row('TOTAL');

        //prepare for next query result, because CI not allowed to used more than one query at the same request with result
        mysqli_next_result($this->log_db->conn_id);

        //free query result, stop executing query and free up memory
        $query->free_result();

        $log_id = date('ymd') . str_pad(($log_count + 1), 10, '0', STR_PAD_LEFT);
        return $log_id;
    }
    //insert log
    public function insert_log($smodule, $log_category, $activity, $application_module, $status, $additional_information = '', $table_name = '', $change_type = '', $old_value = '', $new_value = array())
    {

        if ($smodule != '') {
            //data untuk master
            $log_id = $this->generate_log_id($smodule);
            // $log_category = 'Data Change';

            $created_date = date('Y-m-d H:i:s');
            $username     = $this->username;
            $ip           = $this->get_ip();

            //insert master
            $query_master = $this->log_db->query('CALL proc_master_insert(
                                                      "' . $smodule . '",
                                                      "' . $log_id . '",
                                                      "' . $log_category . '",
                                                      "' . $activity . '",
                                                      "' . $application_module . '",
                                                      "' . $username . '",
                                                      "' . $created_date . '",
                                                      "' . $ip . '",
                                                      "' . $status . '",
                                                      "' . $additional_information . '")
                                            ');

            if ($status == 'Success') {
                //insert detail
                for ($x = 0; $x < count($new_value); $x++) {
                    $query_detail = $this->log_db->query('CALL proc_detail_insert(
                                                        "' . $smodule . '",
                                                        "' . $log_id . '",
                                                        "' . $table_name . '",
                                                        "' . $change_type . '",
                                                        "' . $old_value . '",
                                                        "' . $new_value[$x] . '")
                                                    ');

                }
            }
        }
    }

    //update log
    public function update_log($smodule, $log_category, $activity, $application_module, $status, $additional_information = '', $table_name = '', $change_type = '', $old_value = '', $new_value = '')
    {
        if ($smodule != '') {
            //data untuk master
            $log_id = $this->generate_log_id($smodule);

            $created_date = date('Y-m-d H:i:s');
            $username     = $this->username;
            $ip           = $this->get_ip();

            //insert master
            $query_master = $this->log_db->query('CALL proc_master_insert(
                                                      "' . $smodule . '",
                                                      "' . $log_id . '",
                                                      "' . $log_category . '",
                                                      "' . $activity . '",
                                                      "' . $application_module . '",
                                                      "' . $username . '",
                                                      "' . $created_date . '",
                                                      "' . $ip . '",
                                                      "' . $status . '",
                                                      "' . $additional_information . '")
                                            ');
            if ($status == 'Success') {
                //insert detail
                for ($x = 0; $x < count($old_value); $x++) {
                    if (isset($new_value[$x])) {
                        $query_detail = $this->log_db->query('CALL proc_detail_insert(
                                                        "' . $smodule . '",
                                                        "' . $log_id . '",
                                                        "' . $table_name . '",
                                                        "' . $change_type . '",
                                                        "' . $old_value[$x] . '",
                                                        "' . $new_value[$x] . '")
                                                    ');
                    }
                }
            }
        }

    }

    //system event log
    public function system_event($smodule, $log_category, $activity, $application_module, $status, $additional_information = '')
    {
        if ($smodule != '') {
            $log_id = $this->generate_log_id($smodule);

            $created_date = date('Y-m-d H:i:s');
            $username     = $this->session->userdata('username');
            $ip           = $this->get_ip();

            $query_master = $this->log_db->query('CALL proc_master_insert(
                                                      "' . $smodule . '",
                                                      "' . $log_id . '",
                                                      "' . $log_category . '",
                                                      "' . $activity . '",
                                                      "' . $application_module . '",
                                                      "' . $username . '",
                                                      "' . $created_date . '",
                                                      "' . $ip . '",
                                                      "' . $status . '",
                                                      "' . $additional_information . '")
                                            ');
        }
    }

    //delete log
    public function delete_log($log_db_table_name, $module, $activity, $table = '', $old = '', $status, $info = '')
    {
        // $old = (is_array($old) ? json_encode($old) : $old);
        $this->data_change($log_db_table_name, $module, $activity, $table, 'Delete', $old, '', $status, $info);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    private function insert($log_db_table_name, $category, $module, $activity, $status, $info = '', $username = '')
    {
        $log_count = $this->log_db->get($log_db_table_name)->num_rows();
        $log_id    = date('ymd') . str_pad(($log_count + 1), 8, '0', STR_PAD_LEFT);
        $data      = [
            'application_name'       => 'E-Checklist',
            'log_id'                 => $log_id,
            'log_category'           => $category,
            'activity'               => $activity,
            'application_module'     => $module,
            'username'               => ($username == '' ? $this->username : $username),
            'created_date'           => date('Y-m-d H:i:s'),
            'ip'                     => $this->get_ip(),
            // 'status'                 => ($status ? 'Success' : 'Failed'),
            'status'                 => $status,
            'additional_information' => $info,
        ];
        $this->log_db->insert($log_db_table_name, $data);

        return $log_id;
    }

    private function insert_detail($log_id, $detail_log_db_table_name, $table_name, $change_type, $old_value = '', $new_value)
    {
        $data = [
            'table_name'  => $table_name,
            'change_type' => $change_type,
            'old_value'   => $old_value,
            'new_value'   => $new_value,
        ];
        $this->log_db->insert($detail_log_db_table_name, $data);
    }
}
