<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Model_toss extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function front_end_ls_toss($id_user)
    {
        /* return $this->db->query('select
        tb.*,
        (select nama from tbl_lokasi_kota_kabupaten where id=tb.kota_kabupaten) as nama_kota,
        (select nama from tbl_lokasi_kecamatan where id=tb.kecamatan) as nama_kecamatan
        from
        tbl_toss_front_end as tb
        where
        tb.is_delete=0')->result();*/
        return $this->db
            ->select("
        	tb.*,
        	(select nama from tbl_lokasi_kota_kabupaten where id=tb.kota_kabupaten) as nama_kota,
        	(select nama from tbl_lokasi_kecamatan where id=tb.kecamatan) as nama_kecamatan
        	")
            ->from('tbl_toss_front_end tb')
            ->join('tbl_toss_database as tdb', 'tdb.id=tb.id_toss_database')
            ->where('tb.is_delete', 0)
            ->where('tdb.id_user_dealer', $id_user)
            ->get()
            ->result();
    }
//
    public function data_profile($id)
    {
        //return $this->db->query('select (select sertifikasi_attachment_file_name from tbl_toss_database where id=a.id_toss_database) as file_attach,a.* from tbl_toss_front_end as a where id=' . $id)->result();
        return $this->db
            ->select("
    		(select sertifikasi_attachment_file_name from tbl_toss_database where id=a.id_toss_database) as file_attach,
    		a.*
    		")
            ->from('tbl_toss_front_end a')
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function ls_manpower($id)
    {
        //return $this->db->query('select * from tbl_toss_front_end_cs_manpower where is_delete=0 and id_toss_database=' . $id)->result();
        return $this->db
            ->from('tbl_toss_front_end_cs_manpower')
            ->where('is_delete', 0)
            ->where('id_toss_database', $id)
            ->get()
            ->result();
    }

    public function areaFE($id_toss_database)
    {
        // return $this->db->query('select a.id_toss_database,a.id_area,(select area_name from tbl_area where id=a.id_area) as area_name from tbl_toss_front_end_cs_area as a where a.id_toss_database=' . $id_toss_database . ' group by a.id_area order by a.id')->result();

        return $this->db
            ->select("
        	a.id_toss_database,
        	a.id_area,(select area_name from tbl_area where id=a.id_area) as area_name
        	")
            ->from('tbl_toss_front_end_cs_area a')
            ->where('a.id_toss_database', $id_toss_database)
            ->group_by('a.id_area')
            ->order_by('a.id')
            ->get()
            ->result();
    }

    public function sub_areaFE($id_area, $id_toss_database)
    {
        return $this->db
            ->select("
            	a.id_toss_database,
            	a.id_area,
            	a.id_sub_area,
            	(select sub_area_name from tbl_sub_area where id=a.id_sub_area) as sub_area_name
            	")
            ->from('tbl_toss_front_end_cs_area a')
            ->where('a.id_area', $id_area)
            ->where('a.id_toss_database', $id_toss_database)
            ->group_by('a.id_sub_area')
            ->order_by('a.id')
            ->get()
            ->result();
    }

    public function items($id_toss_database, $id_sub_area)
    {

        return $this->db
            ->select("
            	a.*,
            	(select sub_area_name from tbl_sub_area where id=a.id_sub_area) as sub_area_name
            	")
            ->from('tbl_toss_front_end_cs_area a')
            ->where('a.id_sub_area', $id_sub_area)
            ->where('a.id_toss_database', $id_toss_database)
            ->order_by('a.id')
            ->get()
            ->result();
    }

    public function GroupEQFE($id_toss_database)
    {
        return $this->db
            ->select("
        	id,
        	id_toss_database,
        	group_key,
        	group_name")
            ->from("tbl_toss_front_end_cs_equipment")
            ->where("id_toss_database", $id_toss_database)
            ->group_by('group_key')
            ->order_by('id', 'ASC')->get()->result();
    }

    public function ItemsEQFE($id_toss_database, $group_key)
    {
        return $this->db
            ->from('tbl_toss_front_end_cs_equipment')
            ->where('id_toss_database', $id_toss_database)
            ->where('group_key', $group_key)
            ->order_by('id', 'asc')->get()->result();
    }

    public function Isi_EQ($id_items)
    {
        return $this->db->select("id,foto_dealer_file_name_64")
            ->from('tbl_toss_front_end_cs_equipment_foto_dealer')
            ->where('is_delete', 0)
            ->where('id_toss_database_cs_equipment', $id_items)
            ->order_by('id', 'asc')->get()->result();
    }

    public function updateAreaToFE($id_toss_fe)
    {
        $area = $this->db->select('t.id_toss_database as id_toss_be,ar.*')
            ->from('tbl_toss_front_end_cs_area  as ar')
            ->join('tbl_toss_front_end as t', 'ar.id_toss_database=t.id')
            ->where('ar.is_delete', 0)
            ->where('ar.id_toss_database', $id_toss_fe)->get()->result();

        if (count($area) > 0) {

            // soft delete
            // $toDel = $this->db->where('id_toss_database',$area[0]->id_toss_be)->update('tbl_toss_database_cs_area',array('is_delete'=>1));

            // hard delete
            $toDel = $this->db->where('id_toss_database', $area[0]->id_toss_be)->delete('tbl_toss_database_cs_area');

            foreach ($area as $key => $value) {

                $ck_ = $this->db->where(array('img_key' => $value->img_key, 'foto_64' => $value->foto_dealer_file_name_64))->select('id')->get('tbl_toss_database_cs_area_histori');
                if ($ck_->num_rows() > 0) {

                } else {
                    if (!empty($value->foto_dealer_file_name_64)) {
                        $this->db->insert('tbl_toss_database_cs_area_histori', array(
                            'img_key'       => $value->img_key,
                            'foto_64'       => $value->foto_dealer_file_name_64,
                            'diupload_oleh' => 'Dealer',
                            'upload_date'   => date("Y-m-d H:i:s"),
                        ));
                    }
                }

                // $sel = array(
                //     'id_toss_database'=>$value->id_toss_be,
                //     ''
                // );
                // $this->db->where('id_toss_database')->select('*')->
            }

            $toInsert = [];

            if (!empty($area)) {
                foreach ($area as $key => $value) {
                    array_push($toInsert, array(
                        'id_toss_database'           => $value->id_toss_be,
                        'id_area'                    => $value->id_area,
                        'id_sub_area'                => $value->id_sub_area,
                        'img_key'                    => $value->img_key,
                        'kriteria_minimum'           => $value->kriteria_minimum,
                        'jumlah'                     => $value->jumlah,
                        // 'foto_contoh_file_path'=>$value->foto_contoh_file_path,
                        // 'foto_contoh_file_true_path'=>$value->foto_contoh_file_true_path,
                        // 'foto_contoh_file_name'=>$value->foto_contoh_file_name,
                        'foto_contoh_file_name_64'   => $value->foto_contoh_file_name_64,
                        'foto_dealer_file_path'      => $value->foto_dealer_file_path,
                        'foto_dealer_file_true_path' => $value->foto_dealer_file_true_path,
                        'foto_dealer_file_name'      => $value->foto_dealer_file_name,
                        'foto_dealer_file_name_64'   => $value->foto_dealer_file_name_64,
                        'keterangan'                 => $value->keterangan,
                        'score_evaluasi_dealer'      => $value->score_evaluasi_dealer,
                    ));
                }
            }

            $insert = $this->db->insert_batch('tbl_toss_database_cs_area', $toInsert);
            if ($insert) {
                return 1;
            } else {
                return null;
            }

        } else {
            return 1;
        }
    }

    public function updateEQToFE($id_toss_fe)
    {
        $equipment = $this->db->select('e.id_toss_database as id_be,t.*')
            ->from('tbl_toss_front_end_cs_equipment as t')
            ->join('tbl_toss_front_end as e', 't.id_toss_database=e.id')
            ->where('t.id_toss_database' , $id_toss_fe)->get()->result();

        if (count($equipment) > 0) {

            $toDel = $this->db->where('id_toss_database', $equipment[0]->id_be)->delete('tbl_toss_database_cs_equipment');

            $eqKey = $this->db->select("e.id_toss_database as id_be,t.*")
                ->from("tbl_toss_front_end_cs_equipment as t")
                ->join('tbl_toss_front_end as e', 't.id_toss_database=e.id')
                ->where('t.id_toss_database', $id_toss_fe)
                ->group_by('t.group_key')->get()->result();

            if (!empty($eqKey)) {
                foreach ($eqKey as $key => $value) {
                    $toDel = $this->db->where('group_key', $value->group_key)->update('tbl_toss_database_cs_equipment_foto_dealer', array('is_delete' => 1));
                }
            }

            if (!empty($equipment)) {
                foreach ($equipment as $key => $value) {
                    $eqInsert = array(
                        'id_toss_database'           => $value->id_be,
                        'group_key'                  => $value->group_key,
                        'group_name'                 => $value->group_name,
                        'item_key'                   => $value->item_key,
                        'kriteria_minimum'           => $value->kriteria_minimum,
                        'jumlah_minimum'             => $value->jumlah_minimum,
                        'foto_contoh_file_path'      => $value->foto_contoh_file_path,
                        'foto_contoh_file_true_path' => $value->foto_contoh_file_true_path,
                        'foto_contoh_file_name'      => $value->foto_contoh_file_name,
                        'foto_contoh_file_name_64'   => $value->foto_contoh_file_name_64,
                        'keterangan'                 => $value->keterangan,
                        'score_evaluasi_dealer'      => $value->score_evaluasi_dealer,
                    );

                    $this->db->insert('tbl_toss_database_cs_equipment', $eqInsert);

                    $insert_id = $this->db->insert_id();

                    $ftEQ = $this->db
                        ->select("
                    	img_key,group_key,
                    	item_key,
                    	foto_dealer_file_name_64")
                        ->from('tbl_toss_front_end_cs_equipment_foto_dealer')
                        ->where('is_delete', 0)
                        ->where('id_toss_database_cs_equipment', $value->id)->get()->result();

                    if (!empty($ftEQ)) {
                        foreach ($ftEQ as $key_ft => $value_ft) {
                            $ftEQ[$key_ft]->id_toss_database_cs_equipment = $insert_id;
                            $ftEQ[$key_ft]->diupload_oleh                 = 'Dealer';
                            $ftEQ[$key_ft]->upload_date                   = date('Y-m-d H:i:s');
                        }

                        $this->db->insert_batch('tbl_toss_database_cs_equipment_foto_dealer', $ftEQ);
                        //print_r($ftEQ);
                    }

                }
            }

        } else {

        }

    }

    public function updateITEQToFE($id_toss_fe)
    {
        $equipment = $this->db->select("e.id_toss_database as id_be,t.group_key,te.*")
            ->from('tbl_toss_front_end_cs_equipment_foto_dealer as te')
            ->join('tbl_toss_front_end_cs_equipment as t', 'te.id_toss_database_cs_equipment=t.id')
            ->join('tbl_toss_front_end as e', 't.id_toss_database=e.id')
            ->where('te.is_delete', 0)
            ->where('e.id=', $id_toss_fe)->get()->result();

        if (!empty($equipment)) {
            foreach ($equipment as $key => $value) {
                $toDel = $this->db->where('group_key', $value->group_key)->update('tbl_toss_front_end_cs_equipment_foto_dealer', array('is_delete' => 1));
            }
        }

        $toInsert = [];

        if (!empty($equipment)) {
            foreach ($equipment as $key => $value) {
                array_push($toInsert, array(
                    'id_toss_database_cs_equipment' => $value->id_toss_database_cs_equipment,
                    'group_key'                     => $value->group_key,
                    'foto_dealer_file_path'         => $value->foto_dealer_file_path,
                    'foto_dealer_file_true_path'    => $value->foto_dealer_file_true_path,
                    'foto_dealer_file_name'         => $value->foto_dealer_file_name,
                    'foto_dealer_file_name_64'      => $value->foto_dealer_file_name_64,
                ));
            }
        }

        $insert = $this->db->insert_batch('tbl_toss_database_cs_equipment', $toInsert);
        if ($insert) {
            return 1;
        } else {
            return null;
        }
    }

    public function ls_dealer()
    {
        $get = $this->db->select('*')->group_by('dealer_name')->get('tbl_toss_dealer');
        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return null;
        }
    }

}
