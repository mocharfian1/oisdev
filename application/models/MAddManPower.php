<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class MAddManPower extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function insert_data($data, $table_name)
    {
        return $this->db->insert($table_name, $data);
    }

    public function read_mp_level_training()
    {
        return $this->db->where('is_delete =', 0)
            ->order_by('id')
            ->get('tbl_project_level_training');
    }

}
