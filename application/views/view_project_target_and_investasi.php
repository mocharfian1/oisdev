<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<form enctype="multipart/form-data" id="formTarget" action="<?=base_url('Project_target_and_investasi/save/'.$id)?>" method="post">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <!-- <center> -->
    <h3><b>1. Target</b></h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
         <tr>
            <td>a. </td>
            <td style="width: 150px">Target Penjualan (unit/bulan)</td>
            <td>
                <input type="number" name="input[target_penjualan]" value="<?=(isset($target_penjualan)?$target_penjualan:'')?>" class="form-control">
            </td>
         </tr>
         <tr>
            <td>b. </td>
            <td>Jumlah Salesman (orang)</td>
            <td>
                <input type="number" name="input[jumlah_salesman]" value="<?=(isset($jumlah_salesman)?$jumlah_salesman:'')?>" class="form-control">
            </td>
         </tr>
         <tr>
            <td>c. </td>
            <td>Target Servis (unit/hari)</td>
            <td>
                <input type="number" name="input[target_servis]" value="<?=(isset($target_servis)?$target_servis:'')?>" class="form-control">
            </td>
         </tr>
         <tr>
            <td>d. </td>
            <td>Jumlah Stall GR :</td>
            <td>
                &nbsp;
            </td>
         </tr>
         <tr>
            <td>- </td>
            <td>EM</td>
            <td><input type="number" name="input[em]" value="<?=(isset($em)?$em:'')?>" class="form-control" name=""></td>
          </tr>
          <tr>
            <td>- </td>
            <td>SBNP</td>
            <td><input type="number" name="input[sbnp]" value="<?=(isset($sbnp)?$sbnp:'')?>" class="form-control" name=""></td>
          </tr>
          <tr>
            <td>- </td>
            <td>GR</td>
            <td><input type="number" name="input[gr]" value="<?=(isset($gr)?$gr:'')?>" class="form-control" name=""></td>
          </tr>
      </table>
      <table border="1" width="100%" class="text-center">
          <thead>
              <tr>
                  <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;"></th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y</th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 1</th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 2</th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 3</th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 4</th>
                  <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 5</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td style="padding: 3px;font-size: x-small;">Market/tahun</td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y]" value="<?=(isset($market_y)?$market_y:'')?>" value="120" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y1]" value="<?=(isset($market_y1)?$market_y1:'')?>" value="130" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y2]" value="<?=(isset($market_y2)?$market_y2:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y3]" value="<?=(isset($market_y3)?$market_y3:'')?>" value="150" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y4]" value="<?=(isset($market_y4)?$market_y4:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[market_y5]" value="<?=(isset($market_y5)?$market_y5:'')?>" value="150" class="form-control"></td>
              </tr>
              <tr>
                  <td style="padding: 3px;font-size: x-small;">Sales/bulan</td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y]" value="<?=(isset($sales_y)?$sales_y:'')?>" value="120" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y1]" value="<?=(isset($sales_y1)?$sales_y1:'')?>" value="130" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y2]" value="<?=(isset($sales_y2)?$sales_y2:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y3]" value="<?=(isset($sales_y3)?$sales_y3:'')?>" value="150" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y4]" value="<?=(isset($sales_y4)?$sales_y4:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[sales_y5]" value="<?=(isset($sales_y5)?$sales_y5:'')?>" value="150" class="form-control"></td>
              </tr>
              <tr>
                  <td style="padding: 3px;font-size: x-small;">UE GR/hari</td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y]" value="<?=(isset($uegr_y)?$uegr_y:'')?>" value="120" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y1]" value="<?=(isset($uegr_y1)?$uegr_y1:'')?>" value="130" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y2]" value="<?=(isset($uegr_y2)?$uegr_y2:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y3]" value="<?=(isset($uegr_y3)?$uegr_y3:'')?>" value="150" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y4]" value="<?=(isset($uegr_y4)?$uegr_y4:'')?>" value="140" class="form-control"></td>
                  <td><input style="text-align: center;font-size: x-small;" type="number" name="input[uegr_y5]" value="<?=(isset($uegr_y5)?$uegr_y5:'')?>" value="150" class="form-control"></td>
              </tr>
          </tbody>
      </table>
      
      <br>
      <span style="font-size: x-small;">*Y = Tahun Otorisasi</span>
      <hr>

    <h3><b>2. Investasi</b></h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
         <tr>
            <td>a. </td>
            <td style="width: 175px">Land Total (m2)</td>
            <td>
                <input type="number" name="input[land_total]" value="<?=(isset($land_total)?$land_total:'')?>" class="form-control">
            </td>
         </tr>
         <tr>
            <td>b. </td>
            <td>Building Total (m2)</td>
            <td>
                <input type="number" name="input[building_total]" value="<?=(isset($building_total)?$building_total:'')?>" class="form-control">
            </td>
         </tr>
         <tr>
            <td>c. </td>
            <td>Investasi :</td>
            <td>
                &nbsp;
            </td>
         </tr>
          <tr>
            <td>- </td>
            <td>Land (Rp)</td><td><input type="text" class="form-control" name="input[land]" value="<?=(isset($land)?$land:'')?>" id="land" value="0"></td>
          </tr>
          <tr>
            <td>- </td>
            <td>Building (Rp)</td><td><input type="text" class="form-control" name="input[building]" value="<?=(isset($building)?$building:'')?>"  value="0" id="building"></td>
          </tr>
          <tr>
            <td>- </td>
            <td>Equipment (Rp)</td><td><input type="text" class="form-control" name="input[equipment]" value="<?=(isset($equipment)?$equipment:'')?>"  value="0" id="equipment"></td>
          </tr>
          <tr>
            <td>- </td>
            <td>TOTAL (Rp)</td><td><input type="text" class="form-control" name="input[total]" value="<?=(isset($total)?$total:'')?>" id="total"  value="0" readonly=""></td>
          </tr>
      </table>
    

  <!-- </center> -->
  <input type="submit" id="submitFormButton" name="" style="display: none">
</form>
<hr>
<input style="width: 100%; background-color: #00cc99" type="button" onclick="$('#submitFormButton').click();" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Submit"/>

<br/><br/>
<a href="<?=base_url('area/set_page/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>