<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<style type="text/css">
    .checker{
        display: none !important;
    }
</style>

<form enctype="multipart/form-data" id="formChecklist" action="<?php echo base_url('inspection_equipment_and_tools/update_data/'.$id_project.'/'.$jenis_epm.''); ?>" method="post">
  <input type="hidden" id="id_project" value="<?php echo $id_project; ?>">
  <input type="hidden" id="jenis_epm" value="<?php echo $jenis_epm; ?>">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
<center>
    
<?php if(empty($data_equipment_and_tools)){ ?>

    <h2><b>Configuration not found</b></h2>
    <hr><br>

<?php }else{ ?>
    
    <h2>Equipment & Tools</h2>
    <br>
    <table class="table" border="0" style="border: 0px !important">
      <?php 
        if(count($data_equipment_and_tools)>0){
          foreach ($data_equipment_and_tools as $row) {
      ?>
       <tr>
          <td width="40px"><b><?php echo $row['position']; echo " ."; ?></b></td>
          <td colspan="2"><b><?php echo $row['item_name'].' ('.$row['group_name'].')'; ?></b></td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Belum Order</td>
          <td>
              <div class="pull-right">
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                  <input name="<?php echo 'belumorder-'.$row['id']; ?>" type="checkbox" id="switch-1" class="mdl-switch__input" value="1" <?php if($row['belum_order_score'] == '1'){echo 'checked="true"';}; ?> >
                  <span class="mdl-switch__label"></span>
                </label>
              </div>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Order</td>
          <td>
              <div class="pull-right">
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                  <input name="<?php echo 'order-'.$row['id']; ?>" type="checkbox" id="switch-1" class="mdl-switch__input" value="1" <?php if($row['order_score'] == '1'){echo 'checked="true"';}; ?> >
                  <span class="mdl-switch__label"></span>
                </label>
              </div>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Tanggal Order</td>
          <td>
              <input type="text" class="form-control datedropper" data-format="d M Y" data-large-mode="true" name="<?php echo 'tanggalorder-'.$row['id']; ?>" data-default-date="<?php if(!empty($row['tanggal_order'])){echo date('m-d-Y', strtotime($row['tanggal_order']));}else{echo date('m-d-Y');}; ?>" >
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Material Onsite</td>
          <td>
              <div class="pull-right">
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                  <input name="<?php echo 'materialonsite-'.$row['id']; ?>" type="checkbox" id="switch-1" class="mdl-switch__input" value="1" <?php if($row['material_onsite_score'] == '1'){echo 'checked="true"';}; ?> >
                  <span class="mdl-switch__label"></span>
                </label>
              </div>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Terpasang</td>
          <td>
              <div class="pull-right">
                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                  <input name="<?php echo 'terpasang-'.$row['id']; ?>" type="checkbox" id="switch-1" class="mdl-switch__input" value="1" <?php if($row['terpasang_score'] == '1'){echo 'checked="true"';}; ?> >
                  <span class="mdl-switch__label"></span>
                </label>
              </div>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Keterangan</td>
          <td>
              <input type="text" class="form-control" name="<?php echo 'keterangan-'.$row['id']; ?>" value="<?php echo $row['keterangan']; ?>">
          </td>
       </tr>
      <?php 
          }
        } 
      ?>
    </table>
    

    <br><br>

</center>
<input type="submit" id="submitChecklist" name="" style="display: none">
</form>


   
    <input type="button" onclick="$('#submitChecklist').click();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-green" name="submitForm" id="submitForm" value="Save"/><br><br>
    <a href="<?=base_url('area/set_page/'.$id_project)?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a><br><br>

<?php } ?>
