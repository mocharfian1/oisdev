<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<form enctype="multipart/form-data" id="formChecklist" action="<?=base_url('project_executive_summary/save/' . $id)?>" method="post">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <center>
    <h3>Executive Summary<br>(<?=$data_project->nama_outlet?>)</h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td style="padding-top: 15px" colspan="2"><b>I. General</b></td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Function</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[func]" value="<?=isset($func)?$func:''?>">
          </td>
        </tr>
        <tr>
          <td style="font-size: x-small; padding-top: 15px">Date of authorization</td>
          <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper" data-format="d M Y" data-large-mode="true"  data-default-date=""  name="data[date]" value="<?=isset($date)?$date:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Address</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[address]" value="<?=isset($address)?$address:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Phone</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[phone]" value="<?=isset($phone)?$phone:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Fax</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[fax]" value="<?=isset($fax)?$fax:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Sales Category</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[sales_category]" value="<?=isset($sales_category)?$sales_category:''?>">
          </td>
        </tr>
      </table>

      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td style="padding-top: 15px" colspan="2"><b>II. Investasi</b></td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Land total (m2)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[land_total]" value="<?=isset($land_total)?$land_total:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Building total (m2)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[building_total]" value="<?=isset($building_total)?$building_total:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">Invesment total (Rp)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[total]" value="<?=isset($total)?$total:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Land (Rp)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[land]" value="<?=isset($land)?$land:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Building (Rp)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[building]" value="<?=isset($building)?$building:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Equipment (Rp)</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[equipment]" value="<?=isset($equipment)?$equipment:''?>">
          </td>
        </tr>
      </table>
      
      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td style="padding-top: 15px" colspan="2"><b>III. Target</b></td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px">
            
            <table border="1" width="100%" class="text-center">
              <thead>
                  <tr>
                      <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;"></th>
                      <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y</th>
                      <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 1</th>
                      <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 2</th>
                      <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 3</th>
                      <th width="13%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 4</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td style="padding: 3px;font-size: x-small; text-align: left">Market(/year)</td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[my_awal]" value="<?=isset($my_awal)?$my_awal:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[my_plus1]" value="<?=isset($my_plus1)?$my_plus1:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[my_plus2]" value="<?=isset($my_plus2)?$my_plus2:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[my_plus3]" value="<?=isset($my_plus3)?$my_plus3:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[my_plus4]" value="<?=isset($my_plus4)?$my_plus4:''?>" value="" class="form-control"></td>
                  </tr>
                  <tr>
                      <td style="padding: 3px;font-size: x-small; text-align: left">- Sales(/mth)</td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[sy_awal]" value="<?=isset($sy_awal)?$sy_awal:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[sy_plus1]" value="<?=isset($sy_plus1)?$sy_plus1:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[sy_plus2]" value="<?=isset($sy_plus2)?$sy_plus2:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[sy_plus3]" value="<?=isset($sy_plus3)?$sy_plus3:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[sy_plus4]" value="<?=isset($sy_plus4)?$sy_plus4:''?>" value="" class="form-control"></td>
                  </tr>
                  <tr>
                      <td style="padding: 3px;font-size: x-small; text-align: left" colspan="6">- Service</td>
                  </tr>
                  <tr>
                      <td style="padding: 3px;font-size: x-small; text-align: left">UE GR(/day)</td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[gry_awal]" value="<?=isset($gry_awal)?$gry_awal:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[gry_plus1]" value="<?=isset($gry_plus1)?$gry_plus1:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[gry_plus2]" value="<?=isset($gry_plus2)?$gry_plus2:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[gry_plus3]" value="<?=isset($gry_plus3)?$gry_plus3:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[gry_plus4]" value="<?=isset($gry_plus4)?$gry_plus4:''?>" value="" class="form-control"></td>
                  </tr>
                  <tr>
                      <td style="padding: 3px;font-size: x-small; text-align: left">UE BP(/day)</td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[bpy_awal]" value="<?=isset($bpy_awal)?$bpy_awal:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[bpy_plus1]" value="<?=isset($bpy_plus1)?$bpy_plus1:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[bpy_plus2]" value="<?=isset($bpy_plus2)?$bpy_plus2:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[bpy_plus3]" value="<?=isset($bpy_plus3)?$bpy_plus3:''?>" value="" class="form-control"></td>
                      <td><input style="text-align: center;font-size: x-small;" type="text" name="data[bpy_plus4]" value="<?=isset($bpy_plus4)?$bpy_plus4:''?>" value="" class="form-control"></td>
                  </tr>
              </tbody>
          </table><br>
          <span style="font-size: x-small;">Y = year of authorization</span>

          </td>
        </tr>
      </table>

      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td style="padding-top: 15px" colspan="2"><b>IV. Facility</b></td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Sales</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Unit Display</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[display]" value="<?=isset($display)?$display:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Stock Capacity</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[capacity]" value="<?=isset($capacity)?$capacity:''?>">
          </td>
        </tr>

        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Production Stall</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">EM</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[em]" value="<?=isset($em)?$em:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">GR</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr]" value="<?=isset($gr)?$gr:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Dyna/LC</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[lc]" value="<?=isset($lc)?$lc:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Allocation For Expansion</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[allocation]" value="<?=isset($allocation)?$allocation:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">No. Of Lift</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[lift]" value="<?=isset($lift)?$lift:''?>">
          </td>
        </tr>

        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Other Stall</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">PSD/DIO</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[psd]" value="<?=isset($psd)?$psd:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Spooring</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[spooring]" value="<?=isset($spooring)?$spooring:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Washing For Service</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[Washing_service]" value="<?=isset($Washing_service)?$Washing_service:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Washing For New Car</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[washing_new]" value="<?=isset($washing_new)?$washing_new:''?>">
          </td>
        </tr>
        
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- No. Of Parking Stall</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Service</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[park_service]" value="<?=isset($park_service)?$park_service:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Customer Car/Motorcycle</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[park_customer]" value="<?=isset($park_customer)?$park_customer:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Employee Car/Motorcycle</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[park_employee]" value="<?=isset($park_employee)?$park_employee:''?>">
          </td>
        </tr>
      </table>


      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td style="padding-top: 15px" colspan="2"><b>V. Manpower</b></td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Management</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Branch Head</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[branch_head]" value="<?=isset($branch_head)?$branch_head:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Service Head</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[service_head]" value="<?=isset($service_head)?$service_head:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Administration Head</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[admin_head]" value="<?=isset($admin_head)?$admin_head:''?>">
          </td>
        </tr>

        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Sales (No. of person current/room capacity)</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Sales Supervisor/room capacitiy</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[supervisor_per_room]" value="<?=isset($supervisor_per_room)?$supervisor_per_room:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Salesman/room capacity</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[salesman_per_room]" value="<?=isset($salesman_per_room)?$salesman_per_room:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px">Counter Sales/room capacity</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[countersales_per_room]" value="<?=isset($countersales_per_room)?$countersales_per_room:''?>">
          </td>
        </tr>

        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px" colspan="2">- Service</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px" colspan="2">GR</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Service Supervisor</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_supervisor]" value="<?=isset($gr_supervisor)?$gr_supervisor:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Technical Leader</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_technical_leader]" value="<?=isset($gr_technical_leader)?$gr_technical_leader:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Foreman</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_foreman]" value="<?=isset($gr_foreman)?$gr_foreman:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">PTM</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_ptm]" value="<?=isset($gr_ptm)?$gr_ptm:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Technician</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_technician]" value="<?=isset($gr_technician)?$gr_technician:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Service Advisor</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_service_advisor]" value="<?=isset($gr_service_advisor)?$gr_service_advisor:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Partsman</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[gr_partsman]" value="<?=isset($gr_partsman)?$gr_partsman:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px" colspan="2">BP</td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Service Supervisor</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[bp_supervisor]" value="<?=isset($bp_supervisor)?$bp_supervisor:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Foreman</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[bp_foreman]" value="<?=isset($bp_foreman)?$bp_foreman:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Technician</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[bp_technician]" value="<?=isset($bp_technician)?$bp_technician:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 40px">Body</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[bp_body]" value="<?=isset($bp_body)?$bp_body:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 40px">Paint</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[bp_paint]" value="<?=isset($bp_paint)?$bp_paint:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 20px" colspan="2">Others</td>=
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Part Head</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[other_head]" value="<?=isset($other_head)?$other_head:''?>">
          </td>
        </tr>
        <tr>
          <td style="width: 125px; font-size: x-small; padding-top: 15px; padding-left: 30px">Part Salesman</td>
          <td style="font-size: x-small;">
            <input type="text" class="form-control input-sm" name="data[other_salesman]" value="<?=isset($other_salesman)?$other_salesman:''?>">
          </td>
        </tr>
        
      </table>
  </center>
  <input type="submit" id="submitFormButton" name="" style="display: none">
</form>

<hr>
<input style="width: 100%; background-color: #00cc99" type="button" onclick="$('#submitFormButton').click();" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Submit"/>

<br/><br/>
<a href="<?=base_url('area/set_page/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>