<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<style type="text/css">
    .checker{
        display: none !important;
    }
</style>
<form enctype="multipart/form-data" id="formChecklist" action="<?php echo base_url('Add_man_power/add_project_man_power/'); ?>" method="post">
  <input type="hidden" id="id" name="id" value="<?php echo $id_project; ?>">
  <input type="hidden" id="jenis_epm" name="jenis_epm" value="<?php echo $jenis_epm; ?>">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
<center>
  <h2>Add Man Power Data</h2>
    <h3>(EPM <?=$jenis_epm?>%)</h3>
    <?php if ($this->input->get('error') != ''): ?>
      <p style="color: red"> Level training cannot be empty</p>
    <?php endif;?>
    <br>
    <table class="table" border="0" style="border: 0px !important">
       <tr>
          <td>Posisi/Jabatan</td>
          <td>
              <input type="text" class="form-control" name="posisi" id="posisi">
          </td>
       </tr>
       <tr>
          <td>Nama</td>
          <td>
              <input type="text" class="form-control" name="nama" id="nama">
          </td>
       </tr>
       <tr>
          <td>Level Training</td>
          <td>
              <select class="form-control" name="id_project_level_training" id="id_project_level_training" required="">
                <?php foreach ($data_mp_level_training as $level_training) { ?>

                <option value="<?php echo $level_training['id']; ?>"><?=html_escape($level_training['level_training_name'])?></option>

                <?php }?>
              </select>
          </td>
       </tr>
       <tr>
          <td>Rotasi/Mutasi</td>
          <td>
              <label class="radio-inline">
                <input type="radio" name="rotasi_mutasi_score" id="rotasi_mutasi_score" value="1">Ya
              </label>
              <label class="radio-inline">
                <input type="radio" name="rotasi_mutasi_score" id="rotasi_mutasi_score" value="0" checked>Tidak
              </label>
          </td>
       </tr>
       <tr>
          <td>Cabang Sumber</td>
          <td>
              <input type="text" class="form-control" name="cabang_sumber" id="cabang_sumber">
          </td>
       </tr>
       <tr>
          <td>Keterangan</td>
          <td>
              <input type="text" class="form-control" name="keterangan" id="keterangan">
          </td>
       </tr>
    </table>
</center>
<input type="submit" id="submitFormButton" name="" style="display: none">
</form>

<hr>
<center><h4 id="formMessage" style="display: none"></h4></center>
<button onclick="submitForm();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-green" name="submitForm" id="submitForm" >Save</button><br><br>

<a href="<?=base_url('area/set_page/' . $id_project)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a><br><br>


