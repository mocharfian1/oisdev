<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<?php if ($mode == 'view_detail_group') {?>

<center>
 <div class="row">
  <div class="col-md-12">
    <h2>SDM Area After Sales</h2>
    <h3><?=$data_project->nama_outlet?></h3>
    <br><hr>

    <a href="<?php echo base_url() . 'project_sdm_area_after_sales/set_page/add_sdm_data/'.$id; ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Add SDM Data</a><br><br>

    <a href="<?php echo base_url() . 'project_sdm_area_after_sales/set_page/view_sdm_list/'.$id; ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">SDM List</a><br><br>
    <a href="<?=base_url('area/set_page/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>
  </div>
 </div>
</center>

<?php }?>

<?php if ($mode == 'add_sdm_data') {?>

<form enctype="multipart/form-data" id="formChecklist" action="<?=base_url('Project_sdm_area_after_sales/save/' . $id)?>"method="post">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <center>
    <h3><b>Add SDM Data (Area After Sales)</b></h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
          <tr>
            <td style="width: 125px; font-size: x-small; padding-top: 15px">Nama Karyawan</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[nama]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Jabatan</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[jabatan]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Jenis Kelamin</td>
            <td style="font-size: x-small;">
                <select name="input[jenis_kelamin]" class="form-control input-sm">
                  <option disabled="" selected style="display: none"></option>
                  <option>L</option>
                  <option>P</option>
                </select>
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Tgl. Lahir</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[tanggal_lahir]" class="form-control input-sm datedropper" data-format="d M Y" data-large-mode="true" data-default-date="" >
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Agama</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[agama]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Pendidikan Terakhir</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[pendidikan_terakhir]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">No. Telepon</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[no_telp]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Tgl. Masuk</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[tanggal_masuk]" class="form-control input-sm datedropper" data-format="d M Y" data-large-mode="true" data-default-date="" >
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">No. ID TAM (*jika punya)</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[no_id_tam]" class="form-control input-sm">
            </td>
         </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Teknisi Level 1 :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Toyota Technician</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[tt]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Pro Technician</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[pt]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Teknisi Level 2 :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Diagnose Technician Engine</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[dtg]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Diagnose Technician Electrical</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[dtl]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Diagnose Technician Chasis</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[dtc]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Teknisi Level 3 :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Diagnose Master Technician</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[dmt]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Latest Diagnosis</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[ld]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Service Advisor GR :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 1 TSA2 1-1 (Service Process)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[l1]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 2 TSA2 1-2 (Service Excellence)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[l2]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 3 TSA2 1-3 (Service Consultacy)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[l3]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Partman :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 1 (Basic Part Operation)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[pl1]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 2 (Smooth Process Of Parts & Syncronization)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[pl2]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 3 (Officiant & Profitable Parts Operation)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[pl3]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Foreman :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 1 (Basic Procedure Standard & Leadership)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[fol1]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 2 (Motivate & Develop Team)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[fol2]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 3 (MRA-SA-PART Syncronization & Kaizeng)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[fol3]" data-default-date="" >
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Kepala Bengkel :</td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Toyota Service Management Training 1 (Managing Basic Operation)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[tsmtl1]" data-default-date="" >
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Toyota Service Management Training 2 (Managing & Improving Service & CS)</td>
            <td style="font-size: x-small;">
              <input type="text" class="form-control input-sm datedropper tgl-training" data-format="d M Y" data-large-mode="true" name="input[tsmtl2]" data-default-date="">
            </td>
          </tr>

      </table>



  </center>
  <input type="submit" id="submitFormButton" style="display: none">
</form>

<hr>
<input style="width: 100%; background-color: #00cc99" type="button" onclick="$('#submitFormButton').click();" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Submit"/>
<br/><br/>
<a href="<?=base_url('project_sdm_area_after_sales/set_page/view_detail_group/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>
<?php }?>

<?php if ($mode == 'view_sdm_list') {?>

<center>
  <h3>SDM Area After Sales List<br>(Nama Outlet)</h3>
    <br>

    <!-- <table class="table" border="0" style="border: 0px !important"> -->

      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="man_power_list">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Tgl. Lahir</th>
            <th>Agama</th>
            <th>Pendidikan Terakhir</th>
            <th>No. Telepon</th>
            <th>Tgl. Masuk</th>
            <th>Jabatan</th>
            <th>No. ID TAM</th>
            <th>Train Teknisi (TT)</th>
            <th>Train Teknisi (PT)</th>
            <th>Train Teknisi (DTG)</th>
            <th>Train Teknisi (DTL)</th>
            <th>Train Teknisi (DTC)</th>
            <th>Train Teknisi (DMT)</th>
            <th>Train Teknisi (LD)</th>
            <th>Train SA GR (L1)</th>
            <th>Train SA GR (L2)</th>
            <th>Train SA GR (L3)</th>
            <th>Train Partman (P-L1)</th>
            <th>Train Partman (P-L2)</th>
            <th>Train Partman (P-L3)</th>
            <th>Train Foreman (FO-L1)</th>
            <th>Train Foreman (FO-L2)</th>
            <th>Train Foreman (FO-L3)</th>
            <th>Train Kabeng (TSMTL1)</th>
            <th>Train Kabeng (TSMTL2)</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>


        <?php $n = 0;?>
      <?php foreach ($sdm_after_sales as $row): $n++?>
           <tr>
                   <td style="text-align: center"><?=$n?></td>
                   <td><?=html_escape($row['nama'])?></td>
                   <td><?=html_escape($row['jabatan'])?></td>
                   <td><?=html_escape($row['jenis_kelamin'])?></td>
                   <td><?=html_escape($row['tanggal_lahir'])?></td>
                   <td><?=html_escape($row['agama'])?></td>
                   <td><?=html_escape($row['pendidikan_terakhir'])?></td>
                   <td><?=html_escape($row['no_telp'])?></td>
                   <td><?=html_escape($row['tanggal_masuk'])?></td>
                   <td><?=html_escape($row['no_id_tam'])?></td>
                   <td style="text-align: center"><?=html_escape($row['tt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['pt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dtg'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dtl'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dtc'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dmt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['l1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['l2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['l3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['pl1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['pl2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['pl3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['fol1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['fol2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['fol3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['tsmtl2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['tsmtl2'])?></td>
                  <td>
                    <a href="javascript:;" onclick="delete_sdm('<?=$row['id']?>');" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                  </td>
           </tr>

        <?php endforeach;?>
      </tbody>
    </table>
    <br/><br/>
<a href="<?=base_url('project_sdm_area_after_sales/set_page/view_detail_group/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>
</center>
<script type="text/javascript">
  function delete_sdm(id) {
   if(confirm("Hapus Data ini ?")){
     $.ajax({
         url:base_url+'project_sdm_area_after_sales/delete/'+id, //URL submit
         type:"get", //method Submit
          datatype : "JSON",
          
          success: function(data){
            window.location.reload();
          }
     });
   }
}
</script>
<?php }?>