<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>

  <input type="hidden" id="id" name="id" value="<?php echo $id_project; ?>">
  <input type="hidden" id="jenis_epm" name="jenis_epm" value="<?php echo $jenis_epm; ?>">
<center>
  <h2>Man Power <?php echo $jenis_epm; ?>% List</h2>
    <br>
    
    <!-- <table class="table" border="0" style="border: 0px !important"> -->

      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="man_power_list">
        <thead>
          <tr>
            <th>No.</th>
            <th>Posisi/Jabatan</th>
            <th>Nama</th>
            <th>Level Training</th>
            <th>Rotasi/Mutasi</th>
            <th>Cabang Sumber</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>

      <?php 
        $no=1; foreach ($data_man_power as $row){ 
      ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?=html_escape($row['posisi'])?></td>
            <td><?=html_escape($row['nama'])?></td>
            <td><?=html_escape($row['level_training_name'])?></td>
            <td><?php if($row['rotasi_mutasi_score'] == '1'){echo 'Ya';}else if($row['rotasi_mutasi_score'] == '0'){echo 'tidak';}; ?></td>
            <td><?=html_escape($row['cabang_sumber'])?></td>
            <td><?=html_escape($row['keterangan'])?></td>
        </tr>
       <!-- <tr>
          <td style="width: 10%"><?=html_escape($no)?></td>
          <td>Posisi/Jabatan</td>
          <td>
              <input type="text" class="form-control" value="<?php echo $row['posisi']; ?>" readonly>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Nama</td>
          <td>
              <input type="text" class="form-control" value="<?php echo $row['nama']; ?>" readonly>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Level Training</td>
          <td>
              <input type="text" class="form-control" value="<?php echo $row['level_training_name']; ?>" readonly>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Rotasi/Mutasi</td>
          <td>
              <input type="text" class="form-control" value="<?php if($row['rotasi_mutasi_score'] == '1'){echo 'Ya';}else if($row['rotasi_mutasi_score'] == '0'){echo 'tidak';}; ?>" readonly>
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Cabang Sumber</td>
          <td>
              <input type="text" class="form-control" value="<?php echo $row['cabang_sumber']; ?>">
          </td>
       </tr>
       <tr>
          <td>&nbsp;</td>
          <td>Keterangan</td>
          <td>
              <input type="text" class="form-control" value="<?php echo $row['keterangan']; ?>">
          </td>
       </tr> -->
      <?php 
        $no++;
        } 
      ?>
      </tbody>
    </table>
    <br><br>
    <a href="<?=base_url('area/set_page/'.$id_project)?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a><br><br>
</center>





