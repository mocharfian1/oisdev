<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<!-- Square card -->
<style>
.demo-card-square.mdl-card {
    /*max-width: 320px;*/
  width: 100%;
  height: 320px;
}
.demo-card-square > .mdl-card__title {
  color: #fff;
  /*background-size: inherit; background: url('') bottom right 15% no-repeat #46B6AC;*/
  background-color: black;
}
</style>

<!-- <center> -->
<div class="row">
  <div class="col-md-12">
    <center>
      <h2>Project Card</h2>
      <hr><br>
    </center>
  </div>
  <!-- <?php echo $user_session['id']; ?> -->
  <?php if (count($data_project)>0) { ?>
    <?php foreach ($data_project as $row) { ?>
    <div class="col-md-4" style="padding-bottom: 50px">
      <a href="<?php echo base_url('area/set_page/'.$row['id']); ?>">
        <div class="demo-card-square mdl-card mdl-shadow--2dp">
          <div class="mdl-card__title mdl-card--expand" style="background-size: cover !important; background: url('<?php if(!empty($row['outlet_pic_file_name']) && $row['outlet_pic_file_name'] !== null && $row['outlet_pic_file_name'] !== ''){ echo base_url('adminx/upload/outlet/'.$row['outlet_pic_file_name']);}; ?>') center top no-repeat black;">
          <!-- <div class="mdl-card__title mdl-card--expand"> -->
            <h2 class="mdl-card__title-text"><?=html_escape($row['nama_outlet'])?></h2>
          </div>
          <div class="mdl-card__supporting-text">
            <?=html_escape($row['alamat'])?> - <?=html_escape($row['type_pembangunan'])?> - <?=html_escape($row['jenis_epm'])?>
          </div>
        </div>
      </a>
    </div>
    <?php } ?>
  <?php } else { ?>
    <center>
    <h2>Data Project Kosong!</h2>
    </center>
  <?php } ?>
</div>
<!-- </center> -->