<script type="text/javascript">
  var URL = '<?php echo base_url(); ?>';  
</script>

<?php if($mode == 'view_option'){ ?>
  <center>
    <h3>TOSS Option</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black">Set Data</a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi/').$id_toss_front_end.'/is/evaluation'; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black">Evaluation</a>
    <br><br>
    <hr>

    <?php if($data_profile->status=='certified'){ ?>
      <a href="javascript:;" onclick="location.href = '<?php echo base_url().'adminx/upload/toss_attachments/'.$data_profile->file_attach; ?>'" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: silver; color: black">Attach. Download</a>
      <br><br>
    <?php } ?>
    <a href="<?php echo base_url('toss'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
  </center>
<?php } ?>

<?php if($mode == 'view_option_certified'){ ?>
  <center>
    <h3>TOSS Option<br>(Certified)</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black">Set Data</a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black">Evaluation</a>
    <br><br>
    <hr>
    <a href="javascript:;" onclick="alert('Not available in demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: silver; color: black">Attach. Download</a>
    <br><br>
    <a href="<?php echo base_url('toss'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
  </center>
<?php } ?>

<?php if($mode == 'toss_set_data_group'){ ?>
  <center>
    <h3>TOSS Set Data</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/toss_profile/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'TOSS Profile'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/kapasitas_bengkel/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Kapasitas Bengkel'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/target_dan_investasi/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Target & Investasi'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Checklist Sertifikasi'; ?></a><br><br>
    <hr>
    
    <?php if($is_evaluate!='evaluation'){ ?>
        <?php if($data_profile->status!='certified'){ ?>
          <a href="<?php echo base_url('toss_content/set_page/final_submit/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: silver; color: black">Final Submit</a><br><br>
        <?php } ?>
    <?php } ?>
    
    <a href="<?php echo base_url('toss_content/set_page/view_option/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a><br><br>
  </center>
<?php } ?>

<!-- just mockup -->
<?php if($mode == 'toss_set_data_group_certified'){ ?>
  <center>
    <h3>TOSS Set Data<br>(Certified)</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/toss_profile_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'TOSS Profile'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/kapasitas_bengkel_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Kapasitas Bengkel'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/target_dan_investasi_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Target & Investasi'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'Checklist Sertifikasi'; ?></a><br><br>
    <hr>
    
    <a href="<?php echo base_url('toss_content/set_page/final_submit_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: silver; color: black">Final Submit</a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/view_option_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a><br><br>
  </center>
<?php } ?>

<!-- just mockup -->
<?php if($mode == 'toss_evaluation_group'){ ?>

  <center>
    <h3>TOSS Evaluation</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/manpower_evaluation/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Manpower'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/equipment_evaluation/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Equipment'; ?></a><br>
    <hr>
    <h3><b>FASILITAS UMUM</b></h3>
    <br>
    <a href="<?php echo base_url('toss_content/set_page/sub_area_evaluation/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'PERSPEKTIF & FACIA TOSS'; ?></a><br><br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL PARKING'; ?></a><br><br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA TOILET'; ?></a><br><br>
    <hr>
    <h3><b>AREA PELANGGAN</b></h3>
    <br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA PENERIMAAN SERVIS'; ?></a><br><br>
    <hr>
    <h3><b>AREA PRODUKSI</b></h3>
    <br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL GR'; ?></a><br><br>
    <hr>
    <a href="<?php echo base_url('toss_content/set_page/view_option/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
    <hr>
  </center>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'toss_evaluation_group_certified'){ ?>

  <center>
    <h3>TOSS Evaluation<br>(Certified)</h3>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/manpower_evaluation_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Manpower'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/equipment_evaluation_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Equipment'; ?></a><br>
    <hr>
    <h3><b>FASILITAS UMUM</b></h3>
    <br>
    <a href="<?php echo base_url('toss_content/set_page/sub_area_evaluation_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'PERSPEKTIF & FACIA TOSS'; ?></a><br><br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL PARKING'; ?></a><br><br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA TOILET'; ?></a><br><br>
    <hr>
    <h3><b>AREA PELANGGAN</b></h3>
    <br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA PENERIMAAN SERVIS'; ?></a><br><br>
    <hr>
    <h3><b>AREA PRODUKSI</b></h3>
    <br>
    <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL GR'; ?></a><br><br>
    <hr>
    <a href="<?php echo base_url('toss_content/set_page/view_option/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
    <hr>
  </center>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'toss_profile'){ ?>

  <div class="flagTOSS">
    <form enctype="multipart/form-data" id="formChecklist" class="__frmSetProfile" action="" method="post" onload="">

      <center>
        <h2>TOSS Profile</h2>
        <br><br>
        <h3 style="font-weight: bold;">TOSS Data</h3>
        <br>
        <table class="table" border="0" style="border: 0px !important">

        <tr>
          <td>Sertifikasi</td>
          <td>
            <input type="text" class="form-control" value="<?php echo strtoupper($data_profile->sertifikasi); ?>" readonly>
          </td>
        </tr>
        <tr>
          <td>Nama TOSS</td>
          <td>
            <input type="text" class="form-control" value="<?php echo $data_profile->nama_toss; ?>" readonly>
          </td>
        </tr>
        <tr>
          <td>Dealer</td>
          <td>
            <!-- <input value="<?php echo $data_profile->dealer; ?>" type="text" class="frm_ form-control" name="dealer"> -->
            <select class="select2 frm_ form-control" name="dealer" placeholder="Please fill Dealer Name">
              <?php $arr_val = []; ?>
              <?php if(!empty($data_profile->dealer)){ ?>
                  <option>-- Pilih Dealer --</option>
                  <?php if(!empty($dealer)){ ?>
                      <?php foreach ($dealer as $key => $value) { ?>
                          <?php  array_push($arr_val, $value->dealer_name); ?>
                          <option value="<?php echo $value->dealer_name; ?>" <?php echo $value->dealer_name==$data_profile->dealer?'selected="selected"':''; ?>><?php echo $value->dealer_name; ?></option>
                      <?php } ?>
                      <?php if(!in_array($data_profile->dealer, $arr_val)){ ?>
                          <option value="<?php echo $data_profile->dealer; ?>" selected="selected"><?php echo $data_profile->dealer; ?></option>
                      <?php } ?>
                  <?php }else{ ?>
                      <option value="<?php echo $data_profile->dealer; ?>" selected="selected"><?php echo $data_profile->dealer; ?></option>
                  <?php } ?>
              <?php }else{ ?>
                  <option disabled="disabled" selected="selected">-- Pilih dealer --</option>
                  <?php if(!empty($dealer)){ ?>
                      <?php foreach ($dealer as $key => $value) { ?>
                          <option value="<?php echo $value->dealer_name; ?>"><?php echo $value->dealer_name; ?></option>
                      <?php } ?>
                  <?php } ?>
              <?php } ?>
            </select>

          </td>
        </tr>
        <tr>
          <td>Cabang Induk</td>
          <td>
            <select class="select2 frm_ form-control" name="cabang_induk" placeholder="Please fill Dealer Name">
                <option disabled="disabled" selected="selected">-- Pilih Cabang Induk --</option>
                <?php $arr_val_cab = []; ?>
                <?php if(!empty($data_profile->cabang_induk)){ ?>

                    <?php if(!empty($outlet_name)){ ?>
                        <?php foreach ($outlet_name as $key => $value) { ?>
                            <?php  array_push($arr_val_cab, $value->outlet_name); ?>
                            <option value="<?php echo $value->outlet_name; ?>" <?php echo $value->outlet_name==$data_profile->cabang_induk ?'selected="selected"':''; ?>><?php echo $value->outlet_name; ?></option>
                        <?php } ?>
                        <?php if(!in_array($data_profile->cabang_induk, $arr_val_cab)){ ?>
                            <option value="<?php echo $data_profile->cabang_induk; ?>" selected="selected"><?php echo $data_profile->cabang_induk; ?></option>
                        <?php } ?>
                    <?php }else{ ?>
                        <option value="<?php echo $data_profile->cabang_induk; ?>" selected="selected"><?php echo $data_profile->cabang_induk; ?></option>
                    <?php } ?>
                <?php }else{ ?>
                    <?php if(!empty($outlet_name)){ ?>
                    <!-- <option disabled="disabled" selected="selected">-- Pilih Cabang Induk --</option> -->
                        <?php foreach ($outlet_name as $key => $value) { ?>
                            <option value="<?php echo $value->outlet_name; ?>" <?php echo $value->outlet_name==$data_profile->cabang_induk ?'selected="selected"':''; ?>><?php echo $value->outlet_name; ?></option>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Tipe Bangunan</td>
          <td>
            <select class="frm_ form-control" name="tipe_bangunan">
              <option style="display: none" disabled>Please select</option>
              <option value="Ruko" <?php echo $data_profile->tipe_bangunan=='Ruko'?'selected="selected"':''; ?> >Ruko</option>
              <option value="Non-Ruko" <?php echo $data_profile->tipe_bangunan=='Non-Ruko'?'selected="selected"':''; ?> >Non-Ruko</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>Status Bangunan</td>
          <td>
            <select class="frm_ form-control" name="status_bangunan">
              <option style="display: none" disabled>Please select</option>
              <option value="Sewa" <?php echo $data_profile->status_bangunan=='Sewa'?'selected="selected"':''; ?> >Sewa</option>
              <option value="Milik Pribadi" <?php echo $data_profile->status_bangunan=='Milik Pribadi'?'selected="selected"':''; ?> >Milik Pribadi</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>Dari(Sewa)</td>
          <td>
            <input type="text" data-max-year="2050" class="frm_ form-control datedropper" data-format="d M Y" data-large-mode="true" name="tgl_sewa_start" value="" data-default-date="<?php echo strtotime($data_profile->tgl_sewa_start)>strtotime('0000-00-00')?date('m-d-Y', strtotime($data_profile->tgl_sewa_start)):''; ?>">
          </td>
        </tr>
        <tr>
          <td>Sampai(Sewa)</td>
          <td>
            <input type="text" data-max-year="2050" class="frm_ form-control datedropper" data-format="d M Y" data-large-mode="true" name="tgl_sewa_end" data-default-date="<?php echo strtotime($data_profile->tgl_sewa_end)>strtotime('0000-00-00')?date('m-d-Y', strtotime($data_profile->tgl_sewa_end)):''; ?>" >
          </td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td>
            <textarea class="frm_ form-control" name="alamat"><?php echo $data_profile->alamat; ?></textarea>
          </td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>
            <select class="select2 frm_ form-control" name="provinsi" placeholder="Please fill Provinsi">
                <?php if(!empty($data_profile->provinsi)){ ?>
                    <?php if(!empty($provinsi)){ ?>
                        <?php foreach ($provinsi as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo $value->id==$data_profile->provinsi?'selected="selected"':''; ?>><?php echo $value->nama; ?></option>
                        <?php } ?>
                    <?php } ?>
                <?php }else{ ?>
                    <option disabled="disabled" selected="selected">-- Pilih Provinsi --</option>
                    <?php foreach ($provinsi as $key => $value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Kota / Kabupaten</td>
          <td>
            <select class="select2 frm_ form-control" name="kota_kabupaten" placeholder="Please fill Kota/Kabupaten" >
                <?php if(!empty($data_profile->kota_kabupaten)){ ?>
                    <?php if(!empty($kota_kab)){ ?>
                        <?php foreach ($kota_kab as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo $value->id==$data_profile->kota_kabupaten ?'selected="selected"':''; ?>><?php echo $value->nama; ?></option>
                        <?php } ?>
                    <?php } ?>
                <?php }else{ ?>
                    <option disabled="disabled" selected="selected">-- Pilih Kota/Kabupaten --</option>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>
            <select class="select2 frm_ form-control" name="kecamatan" placeholder="Please fill Kecamatan" >
                <?php if(!empty($data_profile->kecamatan)){ ?>
                    <?php if(!empty($kecamatan)){ ?>
                        <?php foreach ($kecamatan as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo $value->id==$data_profile->kecamatan ?'selected="selected"':''; ?>><?php echo $value->nama; ?></option>
                        <?php } ?>
                    <?php } ?>
                <?php }else{ ?>
                    <option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>
                <?php } ?>
            </select>
          </td>
        </tr>

        <tr>
          <td>Telp/Fax</td>
          <td>
            <input type="text" class="frm_ form-control" name="telp_fax" value="<?php echo $data_profile->telp_fax; ?>">
          </td>
        </tr>

      </table>
      <hr>
      <h3 style="font-weight: bold;">Latitude & Longitude</h3>
      <br>

      <input id="pac-input" class="form-control controls" type="text" placeholder="Search location...">
      <br>
      <div id="map" style="width: 80%;height:200px"></div>
      <br><br>

      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td>Latitude</td>
          <td>
            <input type="text" name="latitude" class="frm_ form-control" id="latitude" value="<?php echo $data_profile->latitude; ?>">
          </td>
        </tr>
        <tr>
          <td>Longitude</td>
          <td>
            <input type="text" name="longitude" class="frm_ form-control" id="longitude" value="<?php echo $data_profile->longitude; ?>">
          </td>
        </tr>
      </table>

      <hr>

      <h3 style="font-weight: bold;">TOSS PIC.</h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
       <tr>
        <td>Nama</td>
        <td>
          <input type="text" class="frm_ form-control"  name="pic_nama" value="<?php echo $data_profile->pic_nama; ?>">
        </td>
      </tr>
      <tr>
        <td>Jabatan</td>
        <td>
          <input type="text" class="frm_ form-control" name="pic_jabatan" value="<?php echo $data_profile->pic_jabatan; ?>">
        </td>
      </tr>
      <tr>
        <td>No. HP</td>
        <td>
          <input type="text" class="frm_ form-control" name="pic_no_hp" value="<?php echo $data_profile->pic_no_hp; ?>">
        </td>
      </tr>
      <tr>
        <td>Email</td>
        <td>
          <input type="text" class="frm_ form-control" name="pic_email" value="<?php echo $data_profile->pic_email; ?>">
        </td>
      </tr>
    </table>

  </center>

</form>

<hr>
<?php if($data_profile->status!='certified'){ ?>
  <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT(<?php echo $id_toss_front_end; ?>,$(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save">Save</button>
  <br><br>
<?php } ?>
<a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
</div>

<?php } ?>

<?php if($mode == 'toss_profile_certified'){ ?>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post" onload="">

    <center>
      <h2>TOSS Profile<br>(Certified)</h2>
      <br><br>
      <h3 style="font-weight: bold;">TOSS Data</h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
       <tr>
        <td>Nama TOSS</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Dealer</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Cabang Induk</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Tipe Bangunan</td>
        <td>
          <select class="form-control">
            <option style="display: none" disabled>Please select</option>
            <option>Ruko</option>
            <option>Non-Ruko</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Status Bangunan</td>
        <td>
          <select class="form-control">
            <option style="display: none" disabled>Please select</option>
            <option>Sewa</option>
            <option>Milik Pribadi</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Dari(Sewa)</td>
        <td>
          <input type="text" class="form-control datedropper" value="12 Nov 2018" readonly>
        </td>
      </tr>
      <tr>
        <td>Sampai(Sewa)</td>
        <td>
          <input type="text" class="form-control datedropper" value="12 Nov 2019" readonly>
        </td>
      </tr>
      <tr>
        <td>Kecamatan</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Kota / Kabupaten</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Provinsi</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>
      <tr>
        <td>Telp/Fax</td>
        <td>
          <input type="text" class="form-control" value="" readonly>
        </td>
      </tr>

    </table>
    <hr>
    <h3 style="font-weight: bold;">Latitude & Longitude</h3>
    <br>
    <input type="text" class="form-control" id="latitude" style="display: none">
    <input type="text" class="form-control" id="longitude" style="display: none">


    <div id="map" style="width: 80%;height:200px"></div>


    <hr>

    <h3 style="font-weight: bold;">TOSS PIC.</h3>
    <br>
    <table class="table" border="0" style="border: 0px !important">
     <tr>
      <td>Nama</td>
      <td>
        <input type="text" class="form-control" value="" readonly>
      </td>
    </tr>
    <tr>
      <td>Jabatan</td>
      <td>
        <input type="text" class="form-control" value="" readonly>
      </td>
    </tr>
    <tr>
      <td>No. HP</td>
      <td>
        <input type="text" class="form-control" value="" readonly>
      </td>
    </tr>
    <tr>
      <td>Email</td>
      <td>
        <input type="text" class="form-control" value="" readonly>
      </td>
    </tr>
  </table>

</center>

</form>

<hr>
<a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>


<?php } ?>

<?php if($mode == 'kapasitas_bengkel'){ ?>
  <div class="flagTOSS">
    <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

      <center>
        <h2>Kapasitas Bengkel</h2>
        <br><br>
        <h3 style="font-weight: bold;">Jumlah Stall</h3>
        <br>
        <table class="table" border="0" style="border: 0px !important">
         <tr>
          <td>Stall GR</td>
          <td>
            <input type="number" class="frm_ form-control" name="jumlah_stall_stall_gr" value="<?php echo $data_profile->jumlah_stall_stall_gr; ?>">
          </td>
        </tr>
        <tr>
          <td><b>Stall Lain</b></td>
          <td>
            &nbsp;
          </td>
        </tr>
        <tr>
          <td>THS Motor</td>
          <td>
            <input type="number" class="frm_ form-control" name="jumlah_stall_ths_motor" value="<?php echo $data_profile->jumlah_stall_ths_motor; ?>">
          </td>
        </tr>
        <tr>
          <td>THS Mobil</td>
          <td>
            <input type="number" class="frm_ form-control" name="jumlah_stall_ths_mobil" value="<?php echo $data_profile->jumlah_stall_ths_mobil; ?>">
          </td>
        </tr>
        <tr>
          <td>Lain lain</td>
          <td>
            <input type="number" class="frm_ form-control" name="jumlah_stall_lain_lain" value="<?php echo $data_profile->jumlah_stall_lain_lain; ?>">
          </td>
        </tr>
        <tr>
          <td>Keterangan</td>
          <td>
            <input type="text" class="frm_ form-control" name="jumlah_stall_keterangan" value="<?php echo $data_profile->jumlah_stall_keterangan; ?>">
          </td>
        </tr>
      </table>

    </center>
    <!-- <input type="submit" id="submitFormButton" name="" style="display: none"> -->
  </form>
  

  <hr>
  <div>
      <?php if($data_profile->status!='certified'){ ?>
        <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT(<?php echo $id_toss_front_end; ?>,$(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save">Save</button>
        <br><br>
      <?php } ?>
  </div>
  <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
</div>
<?php } ?>

<?php if($mode == 'kapasitas_bengkel_certified'){ ?>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">
    <center>
      <h2>Kapasitas Bengkel<br>(Certified)</h2>
      <br><br>
      <h3 style="font-weight: bold;">Jumlah Stall</h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
       <tr>
        <td>Stall GR</td>
        <td>
          <input type="text" class="form-control">
        </td>
      </tr>
      <tr>
        <td><b>Stall Lain</b></td>
        <td>
          &nbsp;
        </td>
      </tr>
      <tr>
        <td>THS Motor</td>
        <td>
          <input type="text" class="form-control">
        </td>
      </tr>
      <tr>
        <td>THS Mobil</td>
        <td>
          <input type="text" class="form-control">
        </td>
      </tr>
      <tr>
        <td>Lain lain</td>
        <td>
          <input type="text" class="form-control">
        </td>
      </tr>
      <tr>
        <td>Keterangan</td>
        <td>
          <input type="text" class="form-control">
        </td>
      </tr>
    </table>

  </center>
  <input type="submit" id="submitFormButton" name="" style="display: none">
</form>

<hr>

<a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>

<?php } ?>

<?php if($mode == 'target_dan_investasi'){ ?>
  <div class="flagTOSS">
    <form enctype="multipart/form-data" id="formChecklist" action="" method="post">
      <center>
        <h2>Target & Investasi</h2>
        <br><br>
        <h3 style="font-weight: bold;">Target</h3>
        <br>
        <table border="1" width="100%" class="text-center">
          <thead>
            <tr>
              <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Item</th>
              <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y</th>
              <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 1</th>
              <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 2</th>
              <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 3</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding: 3px;font-size: x-small;">UE/Bulan</td>
              <td><input style="text-align: center;font-size: x-small;" type="number" name="target_ue_bulan_year" value="<?php echo $data_profile->target_ue_bulan_year==0?'-':$data_profile->target_ue_bulan_year; ?>" class="frm_ form-control"></td>
              <td><input style="text-align: center;font-size: x-small;" type="number" name="target_ue_bulan_year_plus_1" value="<?php echo $data_profile->target_ue_bulan_year_plus_1==0?'-':$data_profile->target_ue_bulan_year_plus_1; ?>" class="frm_ form-control"></td>
              <td><input style="text-align: center;font-size: x-small;" type="number" name="target_ue_bulan_year_plus_2" value="<?php echo $data_profile->target_ue_bulan_year_plus_2==0?'-':$data_profile->target_ue_bulan_year_plus_2; ?>" class="frm_ form-control"></td>
              <td><input style="text-align: center;font-size: x-small;" type="number" name="target_ue_bulan_year_plus_3" value="<?php echo $data_profile->target_ue_bulan_year_plus_3==0?'-':$data_profile->target_ue_bulan_year_plus_3; ?>" class="frm_ form-control"></td>
            </tr>
            <tr>
              <td style="padding: 3px;font-size: x-small;">UE/Hari</td>
              <td class="input frm_" style="font-size: x-small;" name="target_ue_hari_year"><?php echo $data_profile->target_ue_hari_year; ?></td>
              <td class="input frm_" style="font-size: x-small;" name="target_ue_hari_year_plus_1"><?php echo $data_profile->target_ue_hari_year_plus_1; ?></td>
              <td class="input frm_" style="font-size: x-small;" name="target_ue_hari_year_plus_2"><?php echo $data_profile->target_ue_hari_year_plus_2; ?></td>
              <td class="input frm_" style="font-size: x-small;" name="target_ue_hari_year_plus_3"><?php echo $data_profile->target_ue_hari_year_plus_3; ?></td>
            </tr>
          </tbody>
        </table>
        <br>
        <b style="font-size: x-small;">Note :</b>
        <br>
        <span style="font-size: x-small;">Y = Tahun sertifikasi | 1 Bulan = 22.5 hari</span>
        <hr>
        <h3 style="font-weight: bold;">Investasi</h3>
        <br>
        <table border="1" width="100%" id="tb_investasi">
          <tbody>
            <tr>
              <td style="padding: 3px; font-size: x-small;" width="30%">Pembelian lahan *</td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="padding: 3px; font-size: x-small;width: 30px"> : IDR </td>
                    <td><input style="text-align: right; border: none" type="text" name="investasi_pembelian_lahan" value="<?php echo number_format_dots($data_profile->investasi_pembelian_lahan); ?>" class="frm_ sum form-control"> </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 3px; font-size: x-small;" width="30%">Sewa Bangunan</td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="padding: 3px; font-size: x-small;;width: 30px""> : IDR </td>
                    <td> <input style="text-align: right; border: none" type="text" name="investasi_sewa_bangunan" value="<?php echo number_format_dots($data_profile->investasi_sewa_bangunan); ?>" class="frm_ sum form-control"> </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 3px; font-size: x-small;" width="30%">Renovasi Bangunan</td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="padding: 3px; font-size: x-small;width: 30px"> : IDR </td>
                    <td> <input style="text-align: right; border: none" type="text" name="investasi_renovasi_bangunan" value="<?php echo number_format_dots($data_profile->investasi_renovasi_bangunan); ?>" class="frm_ sum form-control"> </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 3px; font-size: x-small;" width="30%">Corporate Identity</td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="padding: 3px; font-size: x-small;width: 30px"> : IDR </td>
                    <td> <input style="text-align: right; border: none" type="text" name="investasi_corporate_identity" value="<?php echo number_format_dots($data_profile->investasi_corporate_identity); ?>" class="frm_ sum form-control"> </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding: 3px; font-size: x-small;" width="30%">Equipment</td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="padding: 3px; font-size: x-small;width: 30px""> : IDR </td>
                    <td> <input style="text-align: right; border: none" type="text" name="investasi_equipment" value="<?php echo number_format_dots($data_profile->investasi_equipment); ?>" class="frm_ sum form-control"> </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td width="30%" style="padding: 3px; background-color: grey; color: white; font-size: x-small"><b>TOTAL</b></td>
              <td width="30%">
                <table width="100%">
                  <tr>
                    <td style="background-color: grey; color: white; padding: 3px; font-size: x-small"> : IDR </td>
                    <td class="input frm_ num_sum" style="text-align: right; height: 40px; padding-right: 10px; background-color: grey; color: white" name="investasi_total" id="investasi_total"><?php echo number_format_dots($data_profile->investasi_total); ?></td>
                  </tr>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <br>
        <b style="font-size: x-small;">Note :</b>
        <br>
        <span style="font-size: x-small;">* Apabila milik sendiri</span>

      </center>
      <!-- <input type="submit" id="submitFormButton" name="" style="display: none"> -->
    </form>

    <hr>
    <div>
        <?php if($data_profile->status!='certified'){ ?>
          <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT(<?php echo $id_toss_front_end; ?>,$(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save">Save</button>
          <br><br>
        <?php } ?>
    </div>
    <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
  </div>
<?php } ?>

<?php if($mode == 'target_dan_investasi_certified'){ ?>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">
    <center>
      <h2>Target & Investasi<br>(Certified)</h2>
      <br><br>
      <h3 style="font-weight: bold;">Target</h3>
      <br>
      <table border="1" width="100%" class="text-center">
        <thead>
          <tr>
            <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Item</th>
            <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y</th>
            <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 1</th>
            <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 2</th>
            <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white; font-size: x-small;">Y + 3</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="padding: 3px;font-size: x-small;">UE/Bulan</td>
            <td>120</td>
            <td>130</td>
            <td>140</td>
            <td>150</td>
          </tr>
          <tr>
            <td style="padding: 3px;font-size: x-small;">UE/Hari</td>
            <td style="font-size: x-small;">5</td>
            <td style="font-size: x-small;">6</td>
            <td style="font-size: x-small;">6</td>
            <td style="font-size: x-small;">7</td>
          </tr>
        </tbody>
      </table>
      <br>
      <b style="font-size: x-small;">Note :</b>
      <br>
      <span style="font-size: x-small;">Y = Tahun sertifikasi | 1 Bulan = 22.5 hari</span>
      <hr>
      <h3 style="font-weight: bold;">Investasi</h3>
      <br>
      <table border="1" width="100%">
        <tbody>
          <tr>
            <td style="padding: 3px; font-size: x-small;" width="30%">Pembelian lahan *</td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="padding: 3px; font-size: x-small;;width: 30px""> : IDR </td>
                  <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="" class="form-control"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 3px; font-size: x-small;" width="30%">Sewa Bangunan</td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="padding: 3px; font-size: x-small;;width: 30px""> : IDR </td>
                  <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="100000000" class="form-control"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 3px; font-size: x-small;" width="30%">Renovasi Bangunan</td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="padding: 3px; font-size: x-small;width: 30px""> : IDR </td>
                  <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="43000000" class="form-control"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 3px; font-size: x-small;" width="30%">Corporate Identity</td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="padding: 3px; font-size: x-small;width: 30px"> : IDR </td>
                  <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="10000000" class="form-control"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 3px; font-size: x-small;" width="30%">Equipment</td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="padding: 3px; font-size: x-small;width: 30px""> : IDR </td>
                  <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="292000000" class="form-control"> </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="30%" style="padding: 3px; background-color: grey; color: white; font-size: x-small"><b>TOTAL</b></td>
            <td width="30%">
              <table width="100%">
                <tr>
                  <td style="background-color: grey; color: white; padding: 3px; font-size: x-small"> : IDR </td>
                  <td style="text-align: right; height: 40px; padding-right: 10px; background-color: grey; color: white"> 445.000.000 </td>
                </tr>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
      <br>
      <b style="font-size: x-small;">Note :</b>
      <br>
      <span style="font-size: x-small;">* Apabila milik sendiri</span>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>

<?php } ?>

<?php if($mode == 'checklist_sertifikasi'){ ?>

  <center>
    <h2>Checklist Sertifikasi</h2>
    <br><hr>
    <a href="<?php echo base_url('toss_content/set_page/manpower/').$id_toss_front_end.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Manpower'; ?></a><br><br>
    <a href="<?php echo base_url('toss_content/set_page/equipment/').$id_toss_front_end.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Equipment'; ?></a><br>
    <hr>
    

    <?php foreach ($area as $key => $value) { ?>
      <h3 data-toggle="collapse" data-target="#demo-<?= $key; ?>"><b><?php echo $value->area_name; ?></b></h3>
      <div id="demo-<?= $key; ?>" class="collapse">
        <?php foreach ($value->sub_area as $key_sub => $value_sub) { ?>
          <a href="<?php echo base_url('toss_content/set_page/sub_area/').$value->id_toss_database.'/'.$value_sub->id_sub_area.$sub_evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo $value_sub->sub_area_name; ?></a><br><br>
        <?php } ?>
      </div>
        <!-- <br>
        <a href="" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'PERSPEKTIF & FACIA TOSS'; ?></a><br><br>
        <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL PARKING'; ?></a><br><br>
        <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA TOILET'; ?></a><br><br>
        <hr>
        <h3><b>AREA PELANGGAN</b></h3>
        <br>
        <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA PENERIMAAN SERVIS'; ?></a><br><br>
        <hr>
        <h3><b>AREA PRODUKSI</b></h3>
        <br>
        <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL GR'; ?></a><br><br> -->
      <?php } ?>

      <hr>
      <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$data_profile->id.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
    </center>

  <?php } ?>

<?php if($mode == 'checklist_sertifikasi_certified'){ ?>

    <center>
      <h2>Checklist Sertifikasi<br>(Certified)</h2>
      <br><hr>
      <a href="<?php echo base_url('toss_content/set_page/manpower_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Manpower'; ?></a><br><br>
      <a href="<?php echo base_url('toss_content/set_page/equipment_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a"><?php echo 'Equipment'; ?></a><br>
      <hr>
      <h3><b>FASILITAS UMUM</b></h3>
      <br>
      <a href="<?php echo base_url('toss_content/set_page/sub_area_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'PERSPEKTIF & FACIA TOSS'; ?></a><br><br>
      <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL PARKING'; ?></a><br><br>
      <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA TOILET'; ?></a><br><br>
      <hr>
      <h3><b>AREA PELANGGAN</b></h3>
      <br>
      <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA PENERIMAAN SERVIS'; ?></a><br><br>
      <hr>
      <h3><b>AREA PRODUKSI</b></h3>
      <br>
      <a href="javascript:;" onclick="alert('sub area demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: black"><?php echo 'AREA STALL GR'; ?></a><br><br>
      <hr>
      <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
    </center>

<?php } ?>

<?php if($mode == 'manpower'){ ?>
    <div class="flagTOSS">
      <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

        <center>
          <h2>Manpower</h2>
          <br><br>
          <h3><b>MANPOWER LIST</b></h3>
          <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="man_power_list">
            <thead>
              <tr>
                <th>No.</th>
                <th>Jabatan</th>
                <th>Nama</th>
                <th>Level Training</th>
                <th>Keterangan</th>
                <?php if($data_profile->status!='certified'){ ?>
                    <th style="text-align: center">Act.</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach ($ls_manpower as $key => $value) { ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo ucfirst($value->jabatan); ?></td>
                  <td><?php echo $value->nama_karyawan; ?></td>
                  <td><?php echo strtoupper($value->level_training); ?></td>
                  <td><?php echo $value->keterangan; ?></td>
                  <?php if($is_evaluate=='evaluation'){ ?>
                    <td class="check-control">
                      <?php if($value->jabatan=='admin'||$value->jabatan=='others'){ ?>

                      <?php }else{ ?>
                          
                            <input type="checkbox" <?php echo $data_profile->status=='certified'?'disabled="disabled"':''; ?> class="form-control out evaluasi" name="score_evaluasi_dealer" value="<?php echo $value->id; ?>" <?php echo $value->score_evaluasi_dealer==1?'checked':''; ?>>
                      <?php } ?>
                    </td>
                  <?php }else{ ?>
                      <?php if($data_profile->status!='certified'){ ?>
                        <td style="text-align: center"><a href="javascript:;" onclick="delete_manpower(<?php echo $value->id; ?>)" class="btn btn-xs btn-default"><i class="fa fa-close"></i></a></td>
                      <?php } ?>
                  <?php } ?>
                </tr>
                <?php $no++; } ?>
             <!--  <tr>
                <td>2</td>
                <td>Pro Tech</td>
                <td>Tester 2</td>
                <td>PT</td>
                <td>-</td>
                <td style="text-align: center"><a href="javascript:;" onclick="alert('Not available in demo');" class="btn btn-xs btn-default"><i class="fa fa-close"></i></a></td>
              </tr> -->
            </tbody>
          </table>

          <?php if($is_evaluate!='evaluation'){ ?>
              <br><br>
              <h3><b>CATATAN</b></h3><br>
              <textarea class="form-control" id="catatan_manpower" placeholder="catatan disini..."><?php echo $data_profile->jumlah_manpower_catatan; ?></textarea>
              <br>

              <?php if($data_profile->status!='certified'){ ?>
                  <div>
                    <button style="width: 100%; background-color: #00cc99" type="button" onclick="submit_catatan_manpower(<?php echo $id_toss_front_end; ?>,$(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save Catatan">Save Catatan</button>
                  </div>
              <?php } ?>
              <br><br><br>

              <!-- <i>*You can add manpower data to list fields below or just tap on 'X' button to delete data</i> -->
              <h3><b>ADD MANPOWER DATA</b></h3><br>
              
              <table class="table" border="0" style="border: 0px !important">
               <tr>
                <td style="width: 95px">Nama</td>
                <td>
                  <input type="text" class="frm_ form-control" name="nama_karyawan">
                </td>
                </tr>
                <tr>
                  <td>Jabatan</td>
                  <td>
                    <select class="frm_ form-control" id="manpower_jabatan" onchange="addManpowerJabatan();" name="jabatan">
                      <option style="display: none" disabled selected></option>
                      <option value="tech">Tech</option>
                      <option value="admin">Admin</option>
                      <option value="pro_tech">Pro Tech</option>
                      <option value="others">Others</option>
                    </select>
                  </td>
                </tr>
                <tr id="manpower_level_training_row" style="display: none">
                  <td>Level Training</td>
                  <td>
                    <select class="frm_ form-control" id="manpower_level_training" name="level_training">
                      <option style="display: none" disabled selected></option>
                      <option value='PT'>level 1 PT</option>
                      <option value='DTG'>level 2 DTG</option>
                      <option value='DTL'>level 2 DTL</option>
                      <option value='DTC'>level 2 DTC</option>
                      <option value='DMT'>level 3 DMT</option>
                      <option value='LD'>level 3 LD</option>
                      <option value='L1'>SA GR L1</option>
                      <option value='L2'>SA GR L2</option>
                      <option value='L3'>SA GR L3</option>
                    </select>
                  </td>
                </tr>
                <tr id="manpower_no_ktp_row" style="display: none">
                  <td>No. KTP</td>
                  <td>
                    <input type="text" class="frm_ form-control" id="manpower_no_ktp" name="no_ktp">
                  </td>
                </tr>
                <tr id="manpower_tanggal_masuk_row" style="display: none">
                  <td>Tgl. masuk</td>
                  <td>
                    <input type="text" class="frm_ form-control datedropper" data-max-year="2050" data-format="d M Y" data-large-mode="true" name="tgl_masuk" data-default-date="" >
                  </td>
                </tr>
                <tr id="manpower_keterangan_row" style="display: none">
                  <td>Keterangan</td>
                  <td>
                    <input type="text" class="frm_ form-control" id="manpower_keterangan" name="keterangan">
                  </td>
                </tr>
              </table>
          <?php } ?>


        </center>
        <!-- <input type="submit" id="submitFormButton" name="" style="display: none"> -->
      </form>

      <hr>
      <?php if($is_evaluate!='evaluation'){ ?>
        <?php if($data_profile->status!='certified'){ ?>
          <div>
            <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT(<?php echo $id_toss_front_end ?>,$(this),'MANPOWER')" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save Manpower Data">Save Manpower Data</button><br><br>
          </div>
        <?php } ?>
      <?php }else{ ?>
          <button style="width: 100%; background-color: #00cc99" onclick="SUBMIT_EVALUATE(<?php echo $id_toss_front_end ?>,$(this),'tbl_toss_front_end_cs_manpower')" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Save</button><br><br>
      <?php } ?>
      <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi/').$id_toss_front_end.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>
    </div>
<?php } ?>


<!-- just mockup -->
<?php if($mode == 'manpower_certified'){ ?>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Manpower<br>(Certified)</h2>
      <br><br>
      <h3><b>MANPOWER LIST</b></h3>
      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="man_power_list">
        <thead>
          <tr>
            <th>No.</th>
            <th>Jabatan</th>
            <th>Nama</th>
            <th>Level Training</th>
            <th>Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Tech</td>
            <td>Tester</td>
            <td>-</td>
            <td>-</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Pro Tech</td>
            <td>Tester 2</td>
            <td>PT</td>
            <td>-</td>
          </tr>
        </tbody>
      </table>
      <br><br>
      <h3><b>CATATAN</b></h3><br>
      <textarea placeholder="" class="form-control" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
      
      

    </center>

  </form>

  <hr>
  <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>

<?php } ?>

<?php if($mode == 'equipment'){ ?>

  <style type="text/css">
  .checker{
    display: none !important;
  }

  .swiper-container {
    width: 100%;
    height: 100%;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    /*background: #fff;*/

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
</style>
<form enctype="multipart/form-data" id="formChecklist" action="" method="post">

  <center>
    <h2>Equipment</h2>
    <?php if($is_evaluate=='evaluation'){ ?>
        <center>*Just tick checkbox in right side of the item/equipment name you prefer to verified </center>
    <?php } ?>
    <br>

    <?php foreach ($groupEQ as $key => $value) { ?>
      <h4><b><?php echo $value->group_name; ?></b></h4><br>


      <?php $no=1; foreach ($value->items as $key_items => $value_items){ ?>
        <div id="item-wrapper">  
          
          <hr>
          <p>
            <b><?php echo $no; ?>. <?php echo $value_items->kriteria_minimum; ?></b>    
            <?php if($is_evaluate=='evaluation'){ ?>  
              <input <?php echo $data_profile->status=='certified'?'disabled="disabled"':''; ?> type="checkbox" class="form-control out evaluasi" name="score_evaluasi_dealer" value="<?php echo $value_items->id; ?>" <?php echo $value_items->score_evaluasi_dealer==1?'checked':''; ?>>       
            <?php } ?>
            <br>(Jumlah Min : <?php echo !empty($value_items->jumlah_minimum)?$value_items->jumlah_minimum:'-'; ?>)
            <br>(<?php echo !empty($value_items->keterangan)?$value_items->keterangan:'-'; ?>)
            <br>
          </p>

          <u>Foto Contoh</u><br><br>
          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
                $file = $value_items->foto_contoh_file_name_64; 

                      //with @ in front of method, error will be ignore and file will be empty
                $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
                $width = $dataImage[0];
                $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo $value_items->foto_contoh_file_name_64; ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo $value_items->foto_contoh_file_name_64; ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br>
          <!-- start button add gallery -->
          <?php if($is_evaluate!='evaluation'){ ?>
              <?php if($data_profile->status!='certified'){ ?>
                  <a href="javascript:;" onclick="$('#gallery-<?php echo $value_items->id; ?>').click();" class="btn btn-default btn-lg">Add Photo</a>
                  <span id="photoLayoutMessage-<?php echo $value_items->id; ?>" style="display: none"></span><br>
                  <input type="file" accept="image/*" id="gallery-<?php echo $value_items->id; ?>" name="gallery" class="" onchange="ch_imgTmp(this,<?php echo $value_items->id; ?>,<?php echo $value_items->group_key; ?>,'<?php echo $value_items->item_key; ?>')" style="display: none;">
              <?php } ?>
          <?php } ?>
          <!-- <input type="submit" id="submitPhotoLayout-<?php echo $value_items->id; ?>" name="" style="display: none"> -->
          <!-- end of add gallery -->

          <hr>
          <div class="eqPhoto" id="item_photo_wrapper" style="width: 100%">

            <?php if(!empty($value_items->isi_eq)){ foreach ($value_items->isi_eq as $key_it => $value_it) { ?>
              <?php
                $file = $value_it->foto_dealer_file_name_64; 

                  //with @ in front of method, error will be ignore and file will be empty
                $dataImage = @getimagesize($file);
                
                  //if file not found variable will be empty
                  // if(empty($dataImage)){  
                  //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                  // }else{
                $width = $dataImage[0];
                $height = $dataImage[1];
              ?>
              <div class="_root">
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo $value_it->foto_dealer_file_name_64; ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height ?>">
                            <img style="height: 190px" src="<?php echo $value_it->foto_dealer_file_name_64; ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                    </div>
                  </div>                     
                  <div class="swiper-pagination"></div>
                </div>
                <?php if($is_evaluate!='evaluation'){ ?>
                    <?php if($data_profile->status!='certified'){ ?>
                        <br>
                        <a class="btn btn-default btn-lg btn-danger" onclick="delITEQ($(this),<?php echo $value_it->id; ?>)">Delete Photo</a>
                    <?php } ?>
                <?php } ?>
                <br><br>
              </div>
            <?php }} ?>

          </div>

          <br>
        </div>
        <hr>
        <?php $no++; } ?>
      <?php } ?>

      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <div>
    <?php if($is_evaluate!='evaluation'){ ?>
        <?php if($data_profile->status!='certified'){ ?>
            <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT_foto_eq($(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save">Save</button>
        <?php } ?>
    <?php }else{ ?>
        <button style="width: 100%; background-color: #00cc99" onclick="SUBMIT_EVALUATE(<?php echo $id_toss_front_end ?>,$(this),'tbl_toss_front_end_cs_equipment')" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Save</button><br><br>
    <?php } ?>
  </div>
  <br>
  <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi/').$data_profile->id.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>


<!-- just mockup -->
<?php if($mode == 'equipment_certified'){ ?>

  <style type="text/css">
    .checker{
      display: none !important;
    }

    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      /*background: #fff;*/

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
    }
  </style>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Equipment<br>(Certified)</h2>
      <br>

      <h4><b>Power Equipment Tools</b></h4><br>

      <div id="item-wrapper">  

        <p>
          <b>1. Air Comp. Min. 2 PK</b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>


        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/1.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->
        <br>
        
        
        <hr>
        <u>Foto Dealer</u><br><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->
          <br>


          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->
          <br>


          <br>

        </div>
        <br>
      </div>

      <hr>
      
      
      <div id="item-wrapper">  

        <p>
          <b>2. Air House Rail & Kabel row</b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/2.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        
        <hr>
        <u>Foto Dealer</u><br><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->
          <br>


          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->
          <br>


          <br>

        </div>

        <hr>
      </div>

      <h4><b>Lifting And Garage Equipment</b></h4><br>

      <div id="item-wrapper">  
        <p>
          <b>1. Hydraullic Garage Jack ST</b>
          <br>(Jumlah Min : 1 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/3.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        <hr>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Jack Stand 3T</b>
          <br>(Jumlah Min : 4 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/4.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>

        
        <hr>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>

      </div>

      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>

  <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>

<?php if($mode == 'sub_area'){ ?>

  <style type="text/css">
  .checker{
    display: none !important;
  }

  .swiper-container {
    width: 100%;
    height: 100%;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    /*background: #fff;*/

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
  </style>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
    <h2>Sub Area<br><?php echo $items[0]->sub_area_name; ?></h2>
    <br>

    <?php $no=1; foreach ($items as $key => $value) { ?>
      <div id="item-wrapper">  
        <p>
          <b><?php echo $no .'. '. $value->kriteria_minimum; ?></b>
          <?php if($is_evaluate=='evaluation'){ ?>  
              <input <?php echo $data_profile->status=='certified'?'disabled="disabled"':''; ?> type="checkbox" class="form-control out evaluasi" name="score_evaluasi_dealer" value="<?php echo $value->id; ?>" <?php echo $value->score_evaluasi_dealer==1?'checked':''; ?>>       
          <?php } ?>
          <br>(Jumlah : <?php echo !empty($value->jumlah)?$value->jumlah:'-&nbsp;'; ?>)
          <br><br><textarea data_id=<?php echo $value->id; ?> class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none" <?php echo $is_evaluate=='evaluation'?'readonly':''; ?>><?php echo $value->keterangan; ?></textarea>
          <br>
        </p>
        

        <!-- start button add gallery -->
        <?php if($is_evaluate!='evaluation'){ ?>
            <?php if($data_profile->status!='certified'){ ?>
                <a href="javascript:;" onclick="setPhoto('<?= $value->id; ?>')" class="btn btn-default btn-lg"><span class="fa fa-picture-o"></span></a>
                <a href="javascript:;" onclick="setPhoto('<?= $value->id; ?>',1)" class="btn btn-default btn-lg"><span class="fa fa-camera"></span></a>
                <span id="photoLayoutMessage-<?php echo $value->id; ?>" style="display: none"></span><br>
                <input type="file" accept="image/*" id="gallery-<?php echo $value->id; ?>" name="gallery" class="" onchange="ch_imgTmp(this,<?php echo $value->id; ?>)" style="display: none;">
            <?php } ?>
        <?php } ?>
        <!-- <input type="submit" id="submitPhotoLayout" name="" style="display: none"> -->
        <!-- end of add gallery -->
        <br>
        
        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = $value->foto_contoh_file_name_64; 

                        //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                        //if file not found variable will be empty
                        // if(empty($dataImage)){  
                        //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                        // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                        // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo $value->foto_contoh_file_name_64; ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>" onclick="$('.my-gallery').off('click');">
                            <img style="height: 90px" src="<?php echo $value->foto_contoh_file_name_64; ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = (!empty($value->foto_dealer_file_name_64))?$value->foto_dealer_file_name_64:base_url('adminx/upload/kpd/default.jpg'); 

                        //with @ in front of method, error will be ignore and file will be empty
                   

                    //because linux cant read default data width and height....so use this filter to set default width and height for default photo
                    $dataImage = @getimagesize($file);
                    if(empty($dataImage)){
                      $width = 225;
                      $height = 150;
                    }else{
                      $width = $dataImage[0];
                      $height = $dataImage[1];
                    }



                   
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure id="foto_item-<?php echo $value->id; ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a href="<?php echo (!empty($value->foto_dealer_file_name_64))?$value->foto_dealer_file_name_64:base_url('adminx/upload/kpd/default.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo (!empty($value->foto_dealer_file_name_64))?$value->foto_dealer_file_name_64:base_url('adminx/upload/kpd/default.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>
                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>
      <?php $no++; } ?>

      
      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <div>
      <?php if($is_evaluate!='evaluation'){ ?>
          <?php if($data_profile->status!='certified'){ ?>
              <button style="width: 100%; background-color: #00cc99" type="button" onclick="SUBMIT_foto_area($(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save">Save</button>
          <?php } ?>
      <?php }else{ ?>
          <button style="width: 100%; background-color: #00cc99" onclick="SUBMIT_EVALUATE(<?php echo $id_toss_front_end ?>,$(this),'tbl_toss_front_end_cs_area')" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Save</button><br><br>
      <?php } ?>
  </div>
  <br>
  <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi/').$id_toss_front_end.$evaluate; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
<?php } ?>


<!-- just mockup -->
<?php if($mode == 'sub_area_certified'){ ?>
  <style type="text/css">
  .swiper-container {
    width: 100%;
    height: 100%;
  }
  .swiper-slide {
    text-align: center;
    font-size: 18px;
    /*background: #fff;*/

    /* Center slide text vertically */
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;
  }
  </style>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Sub Area Evaluation<br>PERSPEKTIF & FACIA TOSS<br>(Certified)</h2>
      <br>

      <div id="item-wrapper">  
        <p>
          <b>1. Memiliki Fascia Toyota & Dealer Name sesuai dengan standar*</b>
          <br>(Jumlah : -)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none"> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                        $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                        //with @ in front of method, error will be ignore and file will be empty
                        $dataImage = @getimagesize($file);

                        //because linux cant read default data width and height....so use this filter to set default width and height for default photo
                          
                          if(empty($dataImage)){
                            $width = 225;
                            $height = 150;
                          }else{
                            $width = $dataImage[0];
                            $height = $dataImage[1];
                          }

                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                          //because linux cant read default data width and height....so use this filter to set default width and height for default photo
                            $dataImage = @getimagesize($file);
                            if(empty($dataImage)){
                              $width = 225;
                              $height = 150;
                            }else{
                              $width = $dataImage[0];
                              $height = $dataImage[1];
                            }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Terdapat identitas Toyota Service Station yang mudah dibaca oleh pelanggan</b>
          <br>(Jumlah : 1)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none"> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                         //because linux cant read default data width and height....so use this filter to set default width and height for default photo
                          $dataImage = @getimagesize($file);
                          if(empty($dataImage)){
                            $width = 225;
                            $height = 150;
                          }else{
                            $width = $dataImage[0];
                            $height = $dataImage[1];
                          }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                          //because linux cant read default data width and height....so use this filter to set default width and height for default photo
                            $dataImage = @getimagesize($file);
                            if(empty($dataImage)){
                              $width = 225;
                              $height = 150;
                            }else{
                              $width = $dataImage[0];
                              $height = $dataImage[1];
                            }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>


      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <a href="<?php echo base_url('toss_content/set_page/checklist_sertifikasi_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
<?php } ?>

<?php if($mode == 'final_submit'){ ?>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">
    <center>
      <h2>Final Submit</h2>
      <br>
      <table class="table" border="0" style="border: 0px !important" id="sign_final_submit">
        <img id="IMAGE_CANVAS" style="display: none;" src="<?php echo $data_profile->hand_sign_dealer_image_64; ?>">
        <tr>
          <td>Nama</td>
          <td>
            <input type="text" class="form-control" name="hand_sign_dealer_nama" value="<?php echo $data_profile->hand_sign_dealer_nama; ?>">
          </td>
        </tr>
        <tr>
          <td>Jabatan</td>
          <td>
            <input type="text" class="form-control" name="hand_sign_dealer_jabatan" value="<?php echo $data_profile->hand_sign_dealer_jabatan; ?>">
          </td>
        </tr>

      </table>
      <hr>
      <h3>Hand Signature</h3>
      <br>

      <table style="width: 80%" border="0">
        <tr>
          <td style="text-align: center">
            <div style="width: 100%; text-align: center; height: 460px">
              <div id="signature-pad" class="signature-pad">
                <div class="signature-pad--body">
                  <canvas id="myCanvas"></canvas>
                </div>
                <div class="signature-pad--footer">
                  <div class="description">Sign above</div>

                  <div class="signature-pad--actions" style="">
                    <div>
                      <center>
                        <button type="button" class="btn btn-default btn-md" data-action="clear">Clear</button>
                      </center>

                      <button type="button" class="button" data-action="change-color" style="display: none">Change color</button>
                      <button type="button" class="button" data-action="undo" style="display: none">Undo</button>

                    </div>
                    <div>
                      <button type="button" class="button save" data-action="save-png" style="display: none">Save as PNG</button>
                      <button type="button" class="button save" data-action="save-jpg" style="display: none">Save as JPG</button>
                      <button type="button" class="button save" data-action="save-svg" style="display: none">Save as SVG</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </center>
    <br>

    <br>

    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <div>
      <a style="width: 100%; background-color: #00cc99" type="button" onclick="final_submit(<?php echo $id_toss_front_end; ?>,$(this))" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm">Submit</a>
  </div>
  <br>
  <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group/').$id_toss_front_end; ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>

<?php } ?>


<!-- just mockup -->
<?php if($mode == 'final_submit_certified'){ ?>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">
    <center>
      <h2>Final Submit<br>(Certified)</h2>
      <br>
      <table class="table" border="0" style="border: 0px !important">
        <tr>
          <td>Nama</td>
          <td>
            <input type="text" class="form-control" value="Lorem ipsum dolor sit amet, consectetur adipisicing elit" readonly>
          </td>
        </tr>
        <tr>
          <td>Jabatan</td>
          <td>
            <input type="text" class="form-control" value="Lorem ipsum dolor sit amet, consectetur adipisicing elit" readonly>
          </td>
        </tr>

      </table>
    <hr>
    <h3>Hand Signature</h3>
    <br>

    <table style="width: 80%" border="0">
      <tr>
        <td style="text-align: center">
          <div style="width: 100%; text-align: center; height: 460px">
            <div id="signature-pad" class="signature-pad">
              <div class="signature-pad--body">
                <canvas></canvas>
              </div>
              <div class="signature-pad--footer">
                <div class="description">Sign above</div>

                <div class="signature-pad--actions" style="">
                  <div>
                    <center>
                      <button type="button" class="btn btn-default btn-md" data-action="clear">Clear</button>
                    </center>

                    <button type="button" class="button" data-action="change-color" style="display: none">Change color</button>
                    <button type="button" class="button" data-action="undo" style="display: none">Undo</button>

                  </div>
                  <div>
                    <button type="button" class="button save" data-action="save-png" style="display: none">Save as PNG</button>
                    <button type="button" class="button save" data-action="save-jpg" style="display: none">Save as JPG</button>
                    <button type="button" class="button save" data-action="save-svg" style="display: none">Save as SVG</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>
    </table>
    </center>
    <br>

    <br>

    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>

  <a style="width: 100%; background-color: #9d0a0a; color: white" type="button" href="<?php echo base_url('toss_content/set_page/toss_set_data_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect">Back</a>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'manpower_evaluation'){ ?>

  <style type="text/css">
  
  </style>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Manpower Evaluation</h2>
      <br><br>
      <h3><b>MANPOWER LIST</b></h3>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="">
          <thead>
            <tr>
              <th>No.</th>
              <th>Jabatan</th>
              <th>Nama</th>
              <th>Level Training</th>
              <th>Keterangan</th>
              <th style="width: 50px">Evaluation</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Tech</td>
              <td>Tester</td>
              <td>-</td>
              <td>-</td>
              <td>

                <input type="checkbox" class="" name="" checked="true">

              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Pro Tech</td>
              <td>Tester 2</td>
              <td>PT</td>
              <td>-</td>
              <td>

                <input type="checkbox" class="form-control" name="">

              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>Admin</td>
              <td>Tester 3</td>
              <td>PT</td>
              <td>-</td>
              <td>
                &nbsp;
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br><br>
      <h3><b>CATATAN</b></h3><br>
      <textarea placeholder="" class="form-control" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
      <br>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <input style="width: 100%; background-color: #00cc99" type="button" onclick="alert('Not available in demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save"/>
  <br><br>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'manpower_evaluation_certified'){ ?>

  <style type="text/css">
  
  </style>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Manpower Evaluation<br>(Certified)</h2>
      <br><br>
      <h3><b>MANPOWER LIST</b></h3>
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="">
          <thead>
            <tr>
              <th>No.</th>
              <th>Jabatan</th>
              <th>Nama</th>
              <th>Level Training</th>
              <th>Keterangan</th>
              <th style="width: 50px">Evaluation</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Tech</td>
              <td>Tester</td>
              <td>-</td>
              <td>-</td>
              <td>

                <input type="checkbox" class="" name="" checked="true" disabled>

              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Pro Tech</td>
              <td>Tester 2</td>
              <td>PT</td>
              <td>-</td>
              <td>

                <input type="checkbox" class="form-control" name="" disabled>

              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>Admin</td>
              <td>Tester 3</td>
              <td>PT</td>
              <td>-</td>
              <td>
                &nbsp;
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <br><br>
      <h3><b>CATATAN</b></h3><br>
      <textarea placeholder="" class="form-control" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
      <br>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'equipment_evaluation'){ ?>

  <style type="text/css">

    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      /*background: #fff;*/

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Equipment Evaluation</h2><br>
      <i>*Just tick checkbox in right side of the item/equipment name you prefer to verified</i>
      <br><br>

      <h4><b>Power Equipment Tools</b></h4><br>

      <div id="item-wrapper">  

        <p>
          <b>1. Air Comp. Min. 2 PK <input type="checkbox"/></b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>


        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/1.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        
        <hr>
        <u>Foto Dealer</u><br>

        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

        </div>
        <br>
      </div>

      <hr>
      
      
      <div id="item-wrapper">  

        <p>
          <b>2. Air House Rail & Kabel row <input type="checkbox" name=""/></b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/2.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>

        
        <hr>
        <u>Foto Dealer</u><br>

        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

        </div>

        <hr>
      </div>

      <h4><b>Lifting And Garage Equipment</b></h4><br>

      <div id="item-wrapper">  
        <p>
          <b>1. Hydraullic Garage Jack ST <input type="checkbox" name=""></b>
          <br>(Jumlah Min : 1 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/3.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        <hr>
        <u>Foto Dealer</u><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Jack Stand 3T <input type="checkbox" name=""/></b>
          <br>(Jumlah Min : 4 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/4.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        <hr>
        <u>Foto Dealer</u><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>

      </div>

      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <input style="width: 100%; background-color: #00cc99" type="button" onclick="alert('Not available in demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save"/>
  <br><br>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'equipment_evaluation_certified'){ ?>

  <style type="text/css">

    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      /*background: #fff;*/

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>
  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Equipment Evaluation<br>(Certified)</h2><br>
      <i>*Just tick checkbox in right side of the item/equipment name you prefer to verified</i>
      <br><br>

      <h4><b>Power Equipment Tools</b></h4><br>

      <div id="item-wrapper">  

        <p>
          <b>1. Air Comp. Min. 2 PK <input type="checkbox" checked disabled/></b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>


        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/1.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        
        <hr>
        <u>Foto Dealer</u><br>

        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/1.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/1.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

        </div>
        <br>
      </div>

      <hr>
      
      
      <div id="item-wrapper">  

        <p>
          <b>2. Air House Rail & Kabel row <input type="checkbox" disabled/></b>
          <br>(Jumlah Min : 1 Set)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/2.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>

        
        <hr>
        <u>Foto Dealer</u><br>

        <div class="" id="item_photo_wrapper" style="width: 100%">
          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

          <!-- Swiper -->
          <div class="swiper-container">
            <div class="swiper-wrapper">

              <?php
              $file = base_url('adminx/upload/toss_equipment/2.png'); 

                      //with @ in front of method, error will be ignore and file will be empty
              $dataImage = @getimagesize($file);

                      //if file not found variable will be empty
                      // if(empty($dataImage)){  
                      //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                      // }else{
              $width = $dataImage[0];
              $height = $dataImage[1];
                      // }
              ?>

              <div class="swiper-slide">
                <!-- photoswipe format, do not change this code -->
                <div class="my-gallery">
                  <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                    <a href="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                      <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/2.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                    </a>

                  </figure>
                </div>
                <!-- end of photoswipe format, do not change this code -->
              </div>

              <!-- disini -->

            </div>
            <!-- Add Pagination -->

            <div class="swiper-pagination"></div>
          </div>
          <!-- End Of Swiper -->


          <br><br>

          <br>

        </div>

        <hr>
      </div>

      <h4><b>Lifting And Garage Equipment</b></h4><br>

      <div id="item-wrapper">  
        <p>
          <b>1. Hydraullic Garage Jack ST <input type="checkbox" disabled/></b>
          <br>(Jumlah Min : 1 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/3.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/3.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        <hr>
        <u>Foto Dealer</u><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Jack Stand 3T <input type="checkbox" disabled/></b>
          <br>(Jumlah Min : 4 Pcs)
          <br>(Keterangan disini)
          <br>
        </p>
        
        <u>Foto Contoh</u><br><br>
        <!-- Swiper -->
        <div class="swiper-container">
          <div class="swiper-wrapper">

            <?php
            $file = base_url('adminx/upload/toss_equipment/4.png'); 

              //with @ in front of method, error will be ignore and file will be empty
            $dataImage = @getimagesize($file);
            
              //if file not found variable will be empty
              // if(empty($dataImage)){  
              //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
              // }else{
            $width = $dataImage[0];
            $height = $dataImage[1];
              // }
            ?>

            <div class="swiper-slide">
              <!-- photoswipe format, do not change this code -->
              <div class="my-gallery">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                  <a href="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                    <img style="height: 190px" src="<?php echo base_url('adminx/upload/toss_equipment/4.png'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                  </a>

                </figure>
              </div>
              <!-- end of photoswipe format, do not change this code -->
            </div>

            <!-- disini -->

          </div>
          <!-- Add Pagination -->
          
          <div class="swiper-pagination"></div>
        </div>
        <!-- End Of Swiper -->

        
        <br>
        
        <hr>
        <u>Foto Dealer</u><br>
        <div class="" id="item_photo_wrapper" style="width: 100%">
          <h4>Photo Item Not Found</h4>
        </div>
        <br>
        <hr>

      </div>

      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>

<?php } ?>

<!-- just mockup -->
<?php if($mode == 'sub_area_evaluation'){ ?>
  <style type="text/css">
    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      /*background: #fff;*/

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Sub Area Evaluation<br>PERSPEKTIF & FACIA TOSS</h2>
      <br>

      <div id="item-wrapper">  
        <p>
          <b>1. Memiliki Fascia Toyota & Dealer Name sesuai dengan standar*</b>
          <br>(Evaluation : <input type="checkbox" name="">)
          <br>(Jumlah : -)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Terdapat identitas Toyota Service Station yang mudah dibaca oleh pelanggan</b>
          <br>(Evaluation : <input type="checkbox" name="">)
          <br>(Jumlah : 1)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>


      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <input style="width: 100%; background-color: #00cc99" type="button" onclick="alert('Not available in demo');" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Save"/>
  <br><br>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
<?php } ?>

<!-- just mockup -->
<?php if($mode == 'sub_area_evaluation_certified'){ ?>
  <style type="text/css">
    .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      /*background: #fff;*/

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>

  <form enctype="multipart/form-data" id="formChecklist" action="" method="post">

    <center>
      <h2>Sub Area Evaluation<br>PERSPEKTIF & FACIA TOSS<br>(Certified)</h2>
      <br>

      <div id="item-wrapper">  
        <p>
          <b>1. Memiliki Fascia Toyota & Dealer Name sesuai dengan standar*</b>
          <br>(Evaluation : <input type="checkbox" name="" disabled>)
          <br>(Jumlah : -)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>

      <div id="item-wrapper">  
        <p>
          <b>2. Terdapat identitas Toyota Service Station yang mudah dibaca oleh pelanggan</b>
          <br>(Evaluation : <input type="checkbox" name="" disabled>)
          <br>(Jumlah : 1)
          <br><br><textarea class="form-control" placeholder="Isi keterangan disini" style="text-align: left; resize: none" readonly> tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</textarea>
          <br>
        </p>

        <br>

        <table>
          <tr>
            <td>
              <center>
                <u>Foto Contoh</u><br>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>

            <td>
              <center>
                <u>Foto Dealer</u>
                <!-- Swiper -->
                <div class="swiper-container">
                  <div class="swiper-wrapper">

                    <?php
                    $file = base_url('adminx/upload/toss_cheklist_item/1.jpg'); 

                          //with @ in front of method, error will be ignore and file will be empty
                    $dataImage = @getimagesize($file);

                          //if file not found variable will be empty
                          // if(empty($dataImage)){  
                          //   echo '<center><i>*Picture file not found, please contact admin to re upload.</i></center>';
                          // }else{
                    $width = $dataImage[0];
                    $height = $dataImage[1];
                          // }
                    ?>

                    <div class="swiper-slide">
                      <!-- photoswipe format, do not change this code -->
                      <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" itemprop="contentUrl" data-size="<?php echo $width.'x'.$height; ?>">
                            <img style="height: 90px" src="<?php echo base_url('adminx/upload/toss_cheklist_item/1.jpg'); ?>" class="img-thumbnail" itemprop="thumbnail" alt="Image description" />
                          </a>

                        </figure>
                      </div>
                      <!-- end of photoswipe format, do not change this code -->
                    </div>

                    <!-- disini -->

                  </div>
                  <!-- Add Pagination -->
                  <br><br>
                  <div class="swiper-pagination"></div>
                </div>
                <!-- End Of Swiper -->
              </center>
            </td>
          </tr>
        </table>
      </div>


      <?php $this->load->view('tpl_photoswipe'); ?>

    </center>
    <input type="submit" id="submitFormButton" name="" style="display: none">
  </form>

  <hr>
  <a href="<?php echo base_url('toss_content/set_page/toss_evaluation_group_certified/0'); ?>" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Back</a>
<?php } ?>


<div class="modal fade" id="modal_success" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-body btn-primary">
        <h4>Berhasil mengubah data.</h4><button type="button" class="btn btn-default" data-dismiss="modal">OKE</button>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="modal_success" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-body btn-danger">
        <h4>Gagal mengubah data.</h4><button type="button" class="btn btn-default" data-dismiss="modal">OKE</button>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  var STATUS = '<?php echo $data_profile->status; ?>';
  if(STATUS=='certified'){
    $('[type="text"],[type="number"]').attr('readonly','');
    $('select').attr('disabled','disabled');
    $('textarea').attr('disabled','disabled');
  }

  $(function(){
    var checkbox = setInterval(function(){
        $('.checker').addClass('ck');
        $('.ck').removeClass('checker');

        if($('.checker').length==0){
            clearInterval(checkbox);
        }
    },10);
    
  });
</script>