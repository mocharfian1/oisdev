<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
 
<?php if($jenis == '50'): ?>
  <form enctype="multipart/form-data" id="formChecklist" action="<?=base_url('Inspection_sub_area/checklist/'.$id)?>" method="post">
  
  <input type="hidden" id="id" value="<?=$id?>">
  <input type="hidden" name="id_project" value="<?=$id_project?>">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <center>
      <h2><?=html_escape($data_project->area_name)?> - <?=html_escape($data_project->sub_area_name)?></h2>
      <?php if($is_locked):?>
        <h4 style="color:red"><i class='fa fa-lock'></i> Input are locked</h4>
      <?php endif;?>
      <input type="hidden" name="jenis" value="<?=$jenis?>">

  <?php if(count($data_ic)==0 && count($data_kp)==0 && count($data_minreq)==0): ?>
      <h2>Configuration not found</h2>
      <hr>
  <?php else: ?>
  <?php if(count($data_project_pic_kpd)>0):?>
    <h3>KPD</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <?php foreach ($data_project_pic_kpd as $pic_kpd):?>
                <?php list($width, $height) = @def_imagesize($pic_kpd['file_true_path']);?>
                  <div class="swiper-slide">
                  <div class="my-gallery">
                      <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                        <a href="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:'#')?>" 
                            itemprop="contentUrl" 
                            data-size="<?=$width?>x<?=$height?>">
                            <img style="height: 200px" 
                              src="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:base_url('adminx/upload/kpd/default.jpg'))?>"
                              class="img-thumbnail"
                              itemprop="thumbnail"
                              alt="<?=$pic_kpd['file_name']?>" />
                        </a>

                       </figure>
                  </div>
                </div>
              <?php endforeach;?>
            </div>
            <br><br>
            <div class="swiper-pagination"></div>
          </div>
        </div>
      </div>
      <hr/>
    <?php endif;?>
      <div class="row">
        <h3>Catatan Lainnya</h3>
        <textarea class="form-control disabled" id="catatan_lainnya" name="catatan_lainnya" <?=$is_locked?'disabled':''?>><?=($data_catatan_lainnya['catatan']!='')?$data_catatan_lainnya['catatan']:''?></textarea>
      </div>
      <hr/>
      <?php if(count($data_ic) > 0): ?>
        <div class="row">
          <h3>Item Checklist</h3>
          
          <table class="table" border="0">
            <?php foreach($data_ic as $row):?>
                <tr>
                  <td width="30px"><?=$row['position']?>.</td>
                  <td style="line-height: 1.8!important;"><?=$row['item_name'].' - '.$row['kriteria']?>
                  </td>
                  <td class="pull-right">
                    <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                        <input name="ic-<?=$row['id']?>" type="checkbox" class="mdl-switch__input" <?=($row['score_'.$user_type]=='1')?'checked':''?> value="1" <?=$is_locked?'disabled':''?>>
                    </label>
                  </td>
                </tr>
                <?php if($row['model']=='Model 2'):?>
                <tr>
                  <td></td>
                  <td>Jarak</td>
                  <td class="pull-right">
                    <input type="text" class="form-control jarak-input" style="width: 70px" placeholder="Jarak" name="<?='ic-jarak-'.$row['id']; ?>" value="<?=($row['jarak']>0)?$row['jarak']:0 ?>" <?=$is_locked?'disabled':''?>/>
                  </td>
                </tr>
                <?php endif;?>
                <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                <tr>
                  <td></td>
                  <td><?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                      Level 1 (<?=strtoupper($data_project->type_level_1)?>)<br/>
                    <?php endif;?>
                    <?php if($user_type==$data_project->type_level_3):?>
                     Level 2 (<?=strtoupper($data_project->type_level_2)?>)
                    <?php endif;?></td>
                  <td class="pull-right">
                    <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                              <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_1]=='1')?'checked':''?> value="1" disabled>
                          </label><br/>
                      <?php endif;?>
                      <?php if($user_type==$data_project->type_level_3):?>
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                            <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_2]=='1')?'checked':''?> value="1" disabled>
                        </label>
                    <?php endif;?>
                  </td>
                </tr>
              <?php endif;?>
            <?php endforeach;?>
          </table>
        </div>
      <?php endif; ?>

      <?php if(count($data_kp) > 0): ?>
        <div class="row">
          <h3>Kriteria Pekerjaan</h3>
          <table class="table" border="0">
            <?php foreach($data_kp as $row):?>
                <tr>
                  <td width="30px"><?=$row['position']?>.</td>
                  <td style="line-height: 1.8!important;"><?=html_escape($row['item_kriteria_pekerjaan'])?>
                  </td>
                  <td class="pull-right">
                    <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                        <input name="kp-<?=$row['id']?>" type="checkbox" class="mdl-switch__input" <?=($row['score_'.$user_type]=='1')?'checked':''?> value="1" <?=$is_locked?'disabled':''?>>
                    </label>
                  </td>
                </tr>
                <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                <tr>
                  <td></td>
                  <td><?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                      Level 1 (<?=strtoupper($data_project->type_level_1)?>)<br/>
                    <?php endif;?>
                    <?php if($user_type==$data_project->type_level_3):?>
                     Level 2 (<?=strtoupper($data_project->type_level_2)?>)
                    <?php endif;?></td>
                  <td class="pull-right">
                    <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                              <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_1]=='1')?'checked':''?> value="1" disabled>
                          </label><br/>
                      <?php endif;?>
                      <?php if($user_type==$data_project->type_level_3):?>
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                            <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_2]=='1')?'checked':''?> value="1" disabled>
                        </label>
                    <?php endif;?>
                  </td>
                </tr>
              <?php endif;?>
            <?php endforeach;?>
          </table>
        </div>
      <?php endif; ?>

      <?php if(count($data_minreq) > 0): ?>
        <div class="row">
          <h3>Minimum Requirement</h3>
          <table class="table" border="0">
            <?php foreach($data_minreq as $row):?>
                <tr>
                  <td width="30px"><?=$row['position']?>.</td>
                  <td><?=html_escape($row['item_minreq'])?></td>
                </tr>
            <?php endforeach;?>
          </table>
        </div>
      <?php endif; ?>

        <?php if(count($data_ket)>0):?>
          <div class="row">
        <h2>Keterangan</h2>
        <table class="table" border="0">
          
        <?php foreach ($data_ket as $row):?>
           <tr>
                <td width="30px"><?=$row['position']?>.</td>
                <td><?=html_escape($row['item_ket'])?></td>
           </tr>
         <?php endforeach;?>
        </table>
        </div>
      <?php endif;?>

      <?php //if(count($data_tps)>0):?>
        <!-- <h2>TPS Line</h2>
        <table class="table" border="0">
          <?php foreach ($data_ket as $row):?>
           <tr>
                <td width="30px"><?=$row['position']?>. </td>
                <td><?=html_escape($row['item_tps_line'])?></td>
           </tr>
          <?php endforeach;?>
        </table> -->
      <?php //endif;?>
      <div class="row">
        <h3>Catatan</h3>
        <textarea class="form-control" id="catatan" name="catatan" <?=$is_locked?'disabled':''?>><?=($data_catatan['catatan']!='')?$data_catatan['catatan']:''?></textarea>
      </div>
      <hr/>
  <?php endif; ?>
  
  </center>
  <input type="submit" id="submitChecklist" name="" style="display: none">
  </form>

  <div class="row">
    <form id="formPhotoArea" method="post" enctype="multipart/form-data">
      <center>
        <h3>Add Photo Area</h3>
         <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic_area as $row):?>
                <?php if($row['type']=='area'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4"  id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked ):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
        <h4 id="photoAreaMessage" style="display: none"></h4>
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
          <input type="file" accept="image/*" capture="camera" id="camera" name="camera" class="" onchange="$('#submitPhotoArea').click();" style="display: none">
            <?php if(!$is_locked):?>
              <a href="javascript:;" onclick="$('#camera').click();" class="mdl-button btn-file mdl-button--raised mdl-js-ripple-effect btn-black" >Open Camera</a>
            <?php else:?>
              <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Open Camera</a>
            <?php endif;?>
        <input type="submit" id="submitPhotoArea" name="" style="display: none">
      </center>
    </form>
  </div>
  <hr/>
  <div class="row">
    <form id="formPhotoLayout" method="post" enctype="multipart/form-data">
      <center>
        <h3>Add Layout Area</h3>
        <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic_area as $row):?>
                <?php if($row['type']=='layout'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked ):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
        <h4 id="photoLayoutMessage" style="display: none"></h4>
        <input type="file" accept="image/*" id="gallery" name="gallery" class="" onchange="$('#submitPhotoLayout').click();" style="display: none">
        <?php if(!$is_locked):?>
          <a href="javascript:;" onclick="$('#gallery').click();" class="mdl-button btn-file mdl-button--raised mdl-js-ripple-effect btn-black" >Open Gallery</a>
        <?php else:?>
          <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Open Gallery</a>
        <?php endif;?>

        <input type="submit" id="submitPhotoLayout" name="" style="display: none">

      </center>
    </form>
  </div>
  <hr/>
<?php elseif($jenis == '75'): ?>
  <form enctype="multipart/form-data" id="formChecklist" action="<?=base_url('Inspection_sub_area/checklist/'.$id)?>" method="post">
  <center>
    <input type="hidden" id="id" value="<?=$id?>">
    <input type="hidden" name="id_project" value="<?=$id_project?>">
    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
     <h2><?=html_escape($data_project->area_name)?> - <?=html_escape($data_project->sub_area_name)?></h2>
      <?php if($is_locked):?>
        <h4 style="color:red"><i class='fa fa-lock'></i> Input are locked</h4>
      <?php endif;?>
      <input type="hidden" name="jenis" value="<?=$jenis?>">

  <?php if(count($data_ic)==0 && count($data_kp)==0 && count($data_minreq)==0): ?>
        <h2>Configuration not found</h2>
        <hr>
  <?php else: ?>

      <?php if(count($data_project_pic_kpd)>0):?>
      <h3>KPD</h3>
        <div class="row">
          <div class="col-md-12">
            <div class="swiper-container">
              <div class="swiper-wrapper">
                <?php foreach ($data_project_pic_kpd as $pic_kpd):?>
                  <?php list($width, $height) = def_imagesize($pic_kpd['file_true_path']);?>
                    <div class="swiper-slide">
                    <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:'#')?>" 
                              itemprop="contentUrl" 
                              data-size="<?=$width?>x<?=$height?>">
                              <img style="height: 200px" 
                                src="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:base_url('adminx/upload/kpd/default.jpg'))?>"
                                class="img-thumbnail"
                                itemprop="thumbnail"
                                alt="<?=$pic_kpd['file_name']?>" />
                          </a>

                         </figure>
                    </div>
                  </div>
                <?php endforeach;?>
              </div>
              <br><br>
              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
        <hr/>
      <?php endif;?>
        <div class="row">
          <h3>Catatan Lainnya</h3>
          <textarea class="form-control disabled" id="catatan_lainnya" name="catatan_lainnya" <?=$is_locked?'disabled':''?>><?=($data_catatan_lainnya['catatan']!='')?$data_catatan_lainnya['catatan']:''?></textarea>
        </div>
        <hr/>

        <?php if(count($data_ic) > 0): ?>
          <div class="row">
            <h3>Item Checklist</h3>
            
            <table class="table" border="0">
              <?php foreach($data_ic as $row):?>
                  <tr>
                    <td width="30px"><?=$row['position']?>.</td>
                    <td style="line-height: 1.8!important;"><?=$row['item_name'].' - '.$row['kriteria']?>
                    </td>
                    <td class="pull-right">
                      <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                          <input name="ic-<?=$row['id']?>" type="checkbox" class="mdl-switch__input" <?=($row['score_'.$user_type]=='1')?'checked':''?> value="1" <?=$is_locked?'disabled':''?>>
                      </label>
                    </td>
                  </tr>
                  <?php if($row['model']=='Model 2'):?>
                  <tr>
                    <td></td>
                    <td>Jarak</td>
                    <td class="pull-right">
                      <input type="number" min="0" class="form-control" style="width: 70px" placeholder="Jarak" name="<?='ic-jarak-'.$row['id']; ?>" value="<?=($row['jarak']>0)?$row['jarak']:0 ?>" <?=$is_locked?'disabled':''?>/>
                    </td>
                  </tr>
                  <?php endif;?>
                  <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                  <tr>
                    <td></td>
                    <td><?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                        Level 1 (<?=strtoupper($data_project->type_level_1)?>)<br/>
                      <?php endif;?>
                      <?php if($user_type==$data_project->type_level_3):?>
                       Level 2 (<?=strtoupper($data_project->type_level_2)?>)
                      <?php endif;?></td>
                    <td class="pull-right">
                      <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                                <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_1]=='1')?'checked':''?> value="1" disabled>
                            </label><br/>
                        <?php endif;?>
                        <?php if($user_type==$data_project->type_level_3):?>
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                              <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_2]=='1')?'checked':''?> value="1" disabled>
                          </label>
                      <?php endif;?>
                    </td>
                  </tr>
                <?php endif;?>
              <?php endforeach;?>
            </table>
          </div>
        <?php endif; ?>

        <?php if(count($data_kp) > 0): ?>
          <div class="row">
            <h3>Kriteria Pekerjaan</h3>
            <table class="table" border="0">
              <?php foreach($data_kp as $row):?>
                  <tr>
                    <td width="30px"><?=$row['position']?>.</td>
                    <td style="line-height: 1.8!important;"><?=html_escape($row['item_kriteria_pekerjaan'])?>
                    </td>
                    <td class="pull-right">
                      <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                          <input name="kp-<?=$row['id']?>" type="checkbox" class="mdl-switch__input" <?=($row['score_'.$user_type]=='1')?'checked':''?> value="1" <?=$is_locked?'disabled':''?>>
                      </label>
                    </td>
                  </tr>
                  <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                  <tr>
                    <td></td>
                    <td><?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                        Level 1 (<?=strtoupper($data_project->type_level_1)?>)<br/>
                      <?php endif;?>
                      <?php if($user_type==$data_project->type_level_3):?>
                       Level 2 (<?=strtoupper($data_project->type_level_2)?>)
                      <?php endif;?></td>
                    <td class="pull-right">
                      <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                                <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_1]=='1')?'checked':''?> value="1" disabled>
                            </label><br/>
                        <?php endif;?>
                        <?php if($user_type==$data_project->type_level_3):?>
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                              <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_2]=='1')?'checked':''?> value="1" disabled>
                          </label>
                      <?php endif;?>
                    </td>
                  </tr>
                <?php endif;?>
              <?php endforeach;?>
            </table>
          </div>
        <?php endif; ?>

        <?php if(count($data_minreq) > 0): ?>
          <div class="row">
            <h3>Minimum Requirement</h3>
            <table class="table" border="0">
              <?php foreach($data_minreq as $row):?>
                  <tr>
                    <td width="30px"><?=$row['position']?>.</td>
                    <td><?=html_escape($row['item_minreq'])?></td>
                  </tr>
              <?php endforeach;?>
            </table>
          </div>
        <?php endif; ?>
        
          <?php if(count($data_ket)>0):?>
            <div class="row">
          <h2>Keterangan</h2>
          <table class="table" border="0">
            
          <?php foreach ($data_ket as $row):?>
             <tr>
                  <td width="30px"><?=$row['position']?>.</td>
                  <td><?=html_escape($row['item_ket'])?></td>
             </tr>
           <?php endforeach;?>
          </table>
          </div>
        <?php endif;?>
        <div class="row">
          <h3>Catatan</h3>
          <textarea class="form-control" id="catatan" name="catatan" <?=$is_locked?'disabled':''?>><?=($data_catatan['catatan']!='')?$data_catatan['catatan']:''?>
            </textarea>
        </div>

      </center>
      <input type="submit" id="submitChecklist" name="" style="display: none">
      </form>

      <div class="row">
        <form id="formPhotoArea" method="post" enctype="multipart/form-data">
          <center>
            <h3>Add Photo Area</h3>
             <!--<div class="row" style="margin-bottom: 15px;">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                  <div class="row">
                  <?php foreach($data_pic_area as $row):?>
                    <?php if($row['type']=='area'):?>
                    <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                    <div class="col-xs-4 col-sm-4"  id="img_<?=$row['id']?>">
                      <div class="my-gallery" style="padding-bottom: 10px">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                          <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                            itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                              <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                              src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                              class="img-thumbnail" itemprop="thumbnail" 
                              alt="<?=$row['file_name']?>" />
                          </a>
                         </figure>
                      </div>
                      <?php if(!$is_locked ):?>
                        <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                      <?php endif;?>
                    </div>
                  <?php endif;?>
                  <?php endforeach;?>
                  </div>
                </div>
              </div>!-->
            <h4 id="photoAreaMessage" style="display: none"></h4>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="file" accept="image/*" capture="camera" id="camera" name="camera" class="" onchange="$('#submitPhotoArea').click();" style="display: none">
                <?php if(!$is_locked):?>
                  <a href="javascript:;" onclick="$('#camera').click();" class="mdl-button btn-file mdl-button--raised mdl-js-ripple-effect btn-black" >Open Camera</a>
                <?php else:?>
                  <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Open Camera</a>
                <?php endif;?>
            <input type="submit" id="submitPhotoArea" name="" style="display: none">
          </center>
        </form>
      </div>
      <hr/>
      <div class="row">
        <form id="formPhotoLayout" method="post" enctype="multipart/form-data">
          <center>
            <h3>Add Layout Area</h3>
            <!--<div class="row" style="margin-bottom: 15px;">
              <div class="col-xs-12 col-md-6 col-md-offset-3">
                <div class="row">
                <?php foreach($data_pic_area as $row):?>
                  <?php if($row['type']=='layout'):?>
                  <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                  <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                    <div class="my-gallery" style="padding-bottom: 10px">
                      <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                        <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                          itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                            <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                            src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                            class="img-thumbnail" itemprop="thumbnail" 
                            alt="<?=$row['file_name']?>" />
                        </a>
                       </figure>
                    </div>
                    <?php if(!$is_locked ):?>
                      <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                    <?php endif;?>
                  </div>
                <?php endif;?>
                <?php endforeach;?>
                </div>
              </div>
            </div>!-->
            <h4 id="photoLayoutMessage" style="display: none"></h4>
            <input type="file" accept="image/*" id="gallery" name="gallery" class="" onchange="$('#submitPhotoLayout').click();" style="display: none">
            <?php if(!$is_locked):?>
              <a href="javascript:;" onclick="$('#gallery').click();" class="mdl-button btn-file mdl-button--raised mdl-js-ripple-effect btn-black" >Open Gallery</a>
            <?php else:?>
              <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Open Gallery</a>
            <?php endif;?>

            <input type="submit" id="submitPhotoLayout" name="" style="display: none">

          </center>
        </form>
      </div>
      <hr/>
  <?php endif; ?>

<?php elseif($jenis == '100'): ?>

  <input type="hidden" id="id" value="<?=$id?>">

  <center>
      <h2><?=html_escape($data_project->area_name)?> - <?=html_escape($data_project->sub_area_name)?></h2>
      <?php if($is_locked):?>
        <h4 style="color:red"><i class='fa fa-lock'></i> Input are locked</h4>
      <?php endif;?>

    <?php if(count($data_project_pic_kpd)>0):?>
    <h3>KPD</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <?php foreach ($data_project_pic_kpd as $pic_kpd):?>
                <?php list($width, $height) = def_imagesize($pic_kpd['file_true_path']);?>
                <div class="swiper-slide">
                  <div class="my-gallery">
                        <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                          <a href="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:'#')?>" 
                              itemprop="contentUrl" 
                              data-size="<?=$width?>x<?=$height?>">
                              <img style="height: 200px" 
                                src="<?=($pic_kpd['file_path']!=''?$pic_kpd['file_path']:base_url('adminx/upload/kpd/default.jpg'))?>"
                                class="img-thumbnail"
                                itemprop="thumbnail"
                                alt="<?=$pic_kpd['file_name']?>" />
                          </a>

                         </figure>
                  </div>
                </div>
              <?php endforeach;?>
            </div>
            <br><br>
            <div class="swiper-pagination"></div>
          </div>
              
           

        </div>

      </div>
      <br><br>
<?php endif;?>
      <h2>Pictures<br><i style="font-size: small">*tap picture name to add or replace picture</i></h2>
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
          <div class="row">
          <?php foreach($data_pic as $row):?>   
            <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
            <div class="col-xs-6 col-sm-4 <?=(count($data_pic)==1)?'col-xs-offset-3 col-sm-offset-4':''?>">
              <div class="my-gallery" style="padding-bottom: 10px">
                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                  <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                    itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                      <img id="img_src_<?=$row['id']?>" style="height: 100px" 
                      src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                      class="img-thumbnail" itemprop="thumbnail" 
                      alt="<?=$row['item_pic']?>" />
                  </a>
                 </figure>
              </div>

              <form id="formPhotoConfig_<?=$row['id']?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <input type="hidden" name="id_config_pic" value="<?=$row['id']?>">
                <input type="hidden" name="id_project_area" value="<?=$id?>">
                <h4 id="formPhotoConfig_<?=$row['id']?>Message" style="display: none"></h4>
                <input type="file" accept="image/*" id="gallery_<?=$row['id']?>" name="gallery" class="" onchange="uploadConfigPic('<?=$row['id']?>')" style="display: none">
                <?php if(!$is_locked):?>
                <a href="javascript:;" onclick="$('#gallery_<?=$row['id']?>').click();" class=""><?=$row['position']?>. <?=html_escape($row['item_pic'])?></a>
                <?php else:?>
                <a class=""><?=$row['position']?>. <?=html_escape($row['item_pic'])?></a>
                <?php endif;?>
              </form>
            </div>
          <?php endforeach;?>
          </div>
        </div>
      </div>
      <form enctype="multipart/form-data" id="formChecklist" action="<?=base_url('Inspection_sub_area/checklist/'.$id)?>" method="post">
      <input type="hidden" name="id_project" value="<?=$id_project?>">
      <input type="hidden" name="jenis" value="<?=$jenis?>">
      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <h2>Kriteria Minimum</h2>
        <table class="table" border="0">
          
        <?php foreach ($data_km as $row):?>
           <tr>
                <td width="30px"><?=$row['position']?>.</td>
                <td style="line-height: 1.8!important;"><?=html_escape($row['item_kriteria_minimum'])?>
                  </td>
                  <td class="pull-right">
                    <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                        <input name="km-<?=$row['id']?>" type="checkbox" class="mdl-switch__input" <?=($row['score_'.$user_type]=='1')?'checked':''?> value="1" <?=$is_locked?'disabled':''?>>
                    </label>
                  </td>
           </tr>

            <?php if($row['model']=='Model 2'):?>
            <tr>
              <td></td>
              <td>Jarak</td>
              <td class="pull-right">
                <input type="number" min="0" class="form-control" style="width: 70px" placeholder="Jarak" name="<?='km-jarak-'.$row['id']; ?>" value="<?=($row['jarak']>0)?$row['jarak']:0 ?>" <?=$is_locked?'disabled':''?>/>
              </td>
            </tr>
            <?php endif;?>
            <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
            <tr>
              <td></td>
              <td><?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                  Level 1 (<?=strtoupper($data_project->type_level_1)?>)<br/>
                <?php endif;?>
                <?php if($user_type==$data_project->type_level_3):?>
                 Level 2 (<?=strtoupper($data_project->type_level_2)?>)
                <?php endif;?></td>
              <td class="pull-right">
                <?php if($user_type==$data_project->type_level_2 or $user_type==$data_project->type_level_3):?>
                    <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                          <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_1]=='1')?'checked':''?> value="1" disabled>
                      </label><br/>
                  <?php endif;?>
                  <?php if($user_type==$data_project->type_level_3):?>
                    <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                        <input  type="checkbox" class="mdl-switch__input" <?=($row['score_'.$data_project->type_level_2]=='1')?'checked':''?> value="1" disabled>
                    </label>
                <?php endif;?>
              </td>
            </tr>
          <?php endif;?>
         <?php endforeach;?>
        </table>
        <br>
        <?php if(count($data_ket)>0):?>
          <div class="row">
        <h2>Keterangan</h2>
        <table class="table" border="0">
          
        <?php foreach ($data_ket as $row):?>
           <tr>
                <td width="30px"><?=$row['position']?>.</td>
                <td><?=html_escape($row['item_ket'])?></td>
           </tr>
         <?php endforeach;?>
        </table>
        </div>
      <?php endif;?>

        <h2>Catatan</h2>
        <textarea class="form-control" id="catatan" name="catatan"><?=html_escape($data_catatan['catatan'])?></textarea>

        <input type="submit" id="submitChecklist" name="" style="display: none">
      </form>
  </center>

  
<?php endif; ?>

<br><br>
<?php if($is_locked):?>
  <input type="button"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled "  value="Save"/>
<?php else:?>
  <input type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-green "  onclick="$('#submitChecklist').click();" value="Save"/>
<?php endif;?>
<br><br>
<a href="<?=base_url('area/set_page/' . $id_project)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>

<?php $this->load->view('tpl_photoswipe'); ?>