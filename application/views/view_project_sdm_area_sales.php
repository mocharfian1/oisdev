<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<?php if ($mode == 'view_detail_group') {?>

<center>
 <div class="row">
  <div class="col-md-12">
    <h2>SDM Area Sales</h2>
    <h3><?=$data_project->nama_outlet?></h3>
    <br><hr>

    <a href="<?php echo base_url() . 'project_sdm_area_sales/set_page/add_sdm_data/' . $id; ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">Add SDM Data</a><br><br>

    <a href="<?php echo base_url() . 'project_sdm_area_sales/set_page/view_sdm_list/' . $id; ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" style="width: 100%; background-color: #9d0a0a; color: white">SDM List</a><br><br>
    <a href="<?=base_url('area/set_page/' . $id)?>"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red" >Back</a>
  </div>
 </div>
</center>
<?php }elseif ($mode == 'add_sdm_data') {?>

<form enctype="multipart/form-data" id="formSdm" action="<?=base_url('Project_sdm_area_sales/save/' . $id)?>" method="post">
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <center>
    <h3><b>Add SDM Data (Area Sales)</b></h3>
      <br>
      <table class="table" border="0" style="border: 0px !important">
         <tr>
            <td style="width: 125px; font-size: x-small; padding-top: 15px">Nama Karyawan</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[nama]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Jabatan</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[jabatan]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Jenis Kelamin</td>
            <td style="font-size: x-small;">
                <select name="input[jenis_kelamin]" class="form-control input-sm">
                  <option disabled="" selected style="display: none"></option>
                  <option>L</option>
                  <option>P</option>
                </select>
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Tgl. Lahir</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[tanggal_lahir]" class="form-control input-sm datedropper" data-format="d M Y" data-large-mode="true" data-default-date="" >
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Agama</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[agama]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Pendidikan Terakhir</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[pendidikan_terakhir]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">No. Telepon</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[no_telp]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Tgl. Masuk</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[tanggal_masuk]" class="form-control input-sm datedropper" data-format="d M Y" data-large-mode="true" data-default-date="" >
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">No. ID TAM (*jika punya)</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[no_id_tam]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Cabang Asal (*jika mutasi)</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[cabang_asli]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Grade (*untuk CS dan SM)</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[grade]" class="form-control input-sm">
            </td>
         </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Kategori Sales (*untuk CS dan SM)</td>
            <td style="font-size: x-small;">
                <input type="text" name="input[kategori_sales]" class="form-control input-sm">
            </td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training CRC/Partsman :</td>
         </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 1</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[level_1][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>s
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[level_1][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 2</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[level_2][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[level_2][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Level 3</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[level_3][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[level_3][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Salesman & Countersales (Training < 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">First Selling Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[fst][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[fst][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Basic Sellig Step Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[bsst][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[bsst][tahun]"  placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Sales Potential Dealer</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[spd][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[spd][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Advance Negotiation Skill</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[ans][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[ans][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Supervisor (Training < 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">SAM 1</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[sam1][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[sam1][tahun]"  placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">SAM 2</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[sam2][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[sam2][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">SAM 3</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[sam3][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[sam3][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Training Kepala Cabang (Training < 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Dealer Operation Management 1</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[dom1][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[dom1][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Dealer Operation Management 2</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[dom2][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[dom2][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Dealer Operation Management 3</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[dom3][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[dom3][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Salesman & Countersales (Training &ge; 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Personal Selling Skill Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[psst][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[psst][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Advance Selling Skill Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[asst][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[asst][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Supervisor (Training &ge; 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Basic Supervisory Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[bst][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[bst][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Managing Supervisory Task Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[mstt][bulan]"  class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[mstt][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Strategic Supervisory Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[sst][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[sst][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>

          <tr>
            <td style="font-size: x-small; padding-top: 15px" colspan="2">Tgl. Kepala Cabang (Training &ge; 2011) :</td>
          </tr>
         <tr>
            <td style="font-size: x-small; padding-top: 15px">Dealer Function Management Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[dfmt][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[dfmt][tahun]" placeholder="- Tahun Lulus- ">
            </td>
          </tr>
          <tr>
            <td style="font-size: x-small; padding-top: 15px">Dealer Branch Management Training</td>
            <td style="font-size: x-small;">
              <select name="bulantahun[dbmt][bulan]" class="form-control input-sm">
                <option disabled selected  style="display: none">- Bulan Lulus -</option>
                <option>Jan</option>
                <option>Feb</option>
                <option>Mar</option>
                <option>Apr</option>
                <option>May</option>
                <option>Jun</option>
                <option>Jul</option>
                <option>Aug</option>
                <option>Sep</option>
                <option>Oct</option>
                <option>Nov</option>
                <option>Des</option>
              </select><br>
              <input type="text" class="form-control input-sm" name="bulantahun[dbmt][tahun]"  placeholder="- Tahun Lulus- ">
            </td>
          </tr>

      </table>



  </center>
  <input type="submit" id="submitFormButton" name="" style="display: none">
</form>

<hr>
<input style="width: 100%; background-color: #00cc99" type="button" onclick="$('#submitFormButton').click();" class="mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect" name="submitForm" id="submitForm" value="Submit"/>
<br/><br/>
<a href="<?=base_url('project_sdm_area_sales/set_page/view_detail_group/'.$id)?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Back</a>
<?php }elseif ($mode == 'view_sdm_list') {?>

<center>
  <h3>SDM Area Sales List<br>(<?=$data_project->nama_outlet?>)</h3>
    <br>

    <!-- <table class="table" border="0" style="border: 0px !important"> -->

      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="man_power_list">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Jabatan</th>
            <th>JK</th>
            <th>Tgl. Lahir</th>
            <th>Agama</th>
            <th>Pendidikan Terakhir</th>
            <th>No. Telepon</th>
            <th>Tgl. Masuk</th>
            <th>No. ID TAM</th>
            <th>Cabang Asal</th>
            <th>Train CRC/Partsman Level 1</th>
            <th>Train CRC/Partsman Level 2</th>
            <th>Train CRC/Partsman Level 3</th>
            <th>Train Salesman & Countersales FST</th>
            <th>Train Salesman & Countersales BSST</th>
            <th>Train Salesman & Countersales SPD</th>
            <th>Train Salesman & Countersales ANS</th>
            <th>Train Supervisor SAM 1</th>
            <th>Train Supervisor SAM 2</th>
            <th>Train Supervisor SAM 3</th>
            <th>Train Kepala Cabang DOM 1</th>
            <th>Train Kepala Cabang DOM 2</th>
            <th>Train Kepala Cabang DOM 3</th>
            <th>Train Salesman & Countersales PSST</th>
            <th>Train Salesman & Countersales ASST</th>
            <th>Train Supervisor BST</th>
            <th>Train Supervisor MSTT</th>
            <th>Train Supervisor SST</th>
            <th>Train Kepala Cabang DFMT</th>
            <th>Train Kepala Cabang DBMT</th>
            <th>Grade</th>
            <th>Kategori Sales</th>
            <th>Act.</th>
          </tr>
        </thead>
        <tbody>

      <?php $n = 0;?>
      <?php foreach ($sdm_sales as $row): $n++?>
           <tr>
                   <td style="text-align: center"><?=$n?></td>
                   <td ><?=html_escape($row['nama'])?></td>
                   <td ><?=html_escape($row['jabatan'])?></td>
                   <td style="text-align: center"><?=html_escape($row['jenis_kelamin'])?></td>
                   <td ><?=html_escape($row['tanggal_lahir'])?></td>
                   <td ><?=html_escape($row['agama'])?></td>
                   <td ><?=html_escape($row['pendidikan_terakhir'])?></td>
                   <td ><?=html_escape($row['no_telp'])?></td>
                   <td ><?=html_escape($row['tanggal_masuk'])?></td>
                   <td ><?=html_escape($row['no_id_tam'])?></td>
                   <td><?=html_escape($row['cabang_asli'])?></td>
                   <td style="text-align: center"><?=html_escape($row['level_1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['level_2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['level_3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['fst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['bsst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['spd'])?></td>
                   <td style="text-align: center"><?=html_escape($row['ans'])?></td>
                   <td style="text-align: center"><?=html_escape($row['sam1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['sam2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['sam3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dom1'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dom2'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dom3'])?></td>
                   <td style="text-align: center"><?=html_escape($row['psst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['asst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['bst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['mstt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['sst'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dfmt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['dbmt'])?></td>
                   <td style="text-align: center"><?=html_escape($row['grade'])?></td>
                   <td style="text-align: center"><?=html_escape($row['kategori_sales'])?></td>
                  <td>
                    <a href="javascript:;" onclick="delete_sdm('<?=$row['id']?>');" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                  </td>
           </tr>

        <?php endforeach;?>


      </tbody>
    </table>
</center>
<br/>
<a href="<?=base_url('project_sdm_area_sales/set_page/view_detail_group/'.$id)?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Back</a>
<script type="text/javascript">
  function delete_sdm(id) {
   if(confirm("Hapus Data ini ?")){
     $.ajax({
         url:base_url+'project_sdm_area_sales/delete/'+id, //URL submit
         type:"get", //method Submit
          datatype : "JSON",
          
          success: function(data){
            window.location.reload();
          }
     });
   }
}
</script>
<?php }?>