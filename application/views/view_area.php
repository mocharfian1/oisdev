<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
?>
<center>
  <input type="" hidden id="id" value='<?=$data_project->id?>'/>
 <div class="row">
  <div class="col-md-12">
    <h2>
      <?=html_escape($data_project->nama_outlet)?>
    </h2>
    <h3>(EPM <?=$data_project->jenis_epm?>%)</h3>
    <hr>

    <?php if($data_project->jenis_epm == '50' || $data_project->jenis_epm == '75'): ?>
      <?php if(count($data_equipment_and_tools)>0): ?>
      		
      <?php if(!$is_locked):?>
      		<a href="<?php echo base_url('inspection_equipment_and_tools/set_page/'.$id_project.'/'.$data_project->jenis_epm )?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Equipment & Tools</a><br><br>
      		
      <?php else:?>
        <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">Equipment & Tools</a><br><br>
      <?php endif;?>
      		<hr style="border-color: #dce0ec">
      <?php endif; ?>
      <?php if(!$is_locked):?>
            	<a href="<?= base_url('add_man_power/set_page/'.$data_project->id.'/'.$data_project->jenis_epm) ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Add Man Power</a><br><br>

            	<a href="<?= base_url('man_power_list/set_page/'.$data_project->id.'/'.$data_project->jenis_epm) ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Man Power List</a><br><br>
      <?php else:?>
            <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">Add Man Power</a><br><br>

              <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">Man Power List</a><br><br>
      <?php endif;?>
      	<hr style="border-color: #dce0ec">
      <?php if(count($dataProjectArea)>0):?>

        <?php foreach ($dataProjectArea as $key_a => $row) :?>
            <h2 data-toggle="collapse" data-target="#demo-<?= $key_a; ?>" style="text-decoration-line: underline;"><?=$row['area_name']?></h2>
            <div id="demo-<?= $key_a; ?>" class="collapse">
            <?php foreach($row['sub'] as $key_sa => $sub):?>
              <?php if($sub['allow_inspection']):?>
                <a href="<?= base_url('inspection_sub_area/set_page/'.$sub['id'].'/'.$data_project->jenis_epm) ?>" 
                  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-black" 
                  ><?=$sub['sub_area_name']?></a>
              <?php else:?>
                <a href="javascript:;" 
                  class="mdl-button mdl-js-button mdl-button--raised btn-disabled" 
                  ><?=$sub['sub_area_name']?></a>
              <?php endif;?>
                <br><br>
            <?php endforeach;?>
            </div>

        <?php endforeach;?>
    	<?php else:?>
          <h2>Data Project Area Kosong!</h2>
      <?php endif;  ?>

    <?php elseif($data_project->jenis_epm == '100'): ?>

      <form id="formPetaLokasi" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <center>
          <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic as $row):?>
                <?php if($row['type']=='lokasi'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
          <div class="row">
            <div class="col-xs-12 ">
              <h4 id="msg" style="display: none"></h4>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="file" accept="image/*" id="peta" name="gallery" class="" onchange="uploadImage('formPetaLokasi','lokasi')" style="display: none">
              <?php if(!$is_locked):?>
                <a href="javascript:;" onclick="$('#peta').click();" class="mdl-button btn-file mdl-button  mdl-button--raised mdl-js-ripple-effect btn-red" >Add Peta Lokasi</a>
              <?php else:?>
                <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Add Peta Lokasi</a>
              <?php endif;?>
            </div>
          </div>
        </center>
      </form>
      <br/>

      <form id="formStruktur" method="post" enctype="multipart/form-data">
        <center>
          <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic as $row):?>
                <?php if($row['type']=='struktur'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
          <div class="row">
            <div class="col-xs-12 ">
              <h4 id="msg" style="display: none"></h4>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="file" accept="image/*" id="struktur" name="gallery" class="" onchange="uploadImage('formStruktur','struktur')" style="display: none">
              <?php if(!$is_locked):?>
                <a href="javascript:;" onclick="$('#struktur').click();" class="mdl-button btn-file mdl-button  mdl-button--raised mdl-js-ripple-effect btn-red" >Add Struktur Organisasi</a>
              <?php else:?>
                <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Add Struktur Organisasi</a>
              <?php endif;?>
            </div>
          </div>
        </center>
      </form>
      <br/>
      <form id="formSSP" method="post" enctype="multipart/form-data">
        <center>
          <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic as $row):?>
                <?php if($row['type']=='ssp'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
          <div class="row">
            <div class="col-xs-12 ">
              <h4 id="msg" style="display: none"></h4>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="file" accept="image/*" id="ssp" name="gallery" class="" onchange="uploadImage('formSSP','ssp')" style="display: none">
              <?php if(!$is_locked):?>
                <a href="javascript:;" onclick="$('#ssp').click();" class="mdl-button btn-file mdl-button  mdl-button--raised mdl-js-ripple-effect btn-red" >Add SSP</a>
              <?php else:?>
                <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Add SSP</a>
              <?php endif;?>
            </div>
          </div>
        </center>
      </form>
      <br/>
      <form id="formDokumen" method="post" enctype="multipart/form-data">
        <center>
          <!--<div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
              <div class="row">
              <?php foreach($data_pic as $row):?>
                <?php if($row['type']=='dokumen'):?>
                <?php list($width, $height) = def_imagesize($row['file_true_path']);?>
                <div class="col-xs-4 col-sm-4" id="img_<?=$row['id']?>">
                  <div class="my-gallery" style="padding-bottom: 10px">
                    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
                      <a id="img_href_<?=$row['id']?>" href="<?=($row['file_path']!='')?$row['file_path']:'#'?>" 
                        itemprop="contentUrl" data-size="<?=$width?>x<?=$height?>">
                          <img id="img_src_<?=$row['id']?>" style="max-height: 100px" 
                          src="<?=($row['file_path']!='')?$row['file_path']:base_url('adminx/upload/kpd/default.jpg')?>" 
                          class="img-thumbnail" itemprop="thumbnail" 
                          alt="<?=$row['file_name']?>" />
                      </a>
                     </figure>
                  </div>
                  <?php if(!$is_locked):?>
                    <a href="javascript:;" onclick="delete_image('<?=$row['id']?>')" class=""><i class="fa fa-trash"></i></a>
                  <?php endif;?>
                </div>
              <?php endif;?>
              <?php endforeach;?>
              </div>
            </div>
          </div>!-->
          <div class="row">
            <div class="col-xs-12 ">
              <h4 id="msg" style="display: none"></h4>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
              <input type="file" accept="application/pdf,application/zip,application/rar,application/vnd.ms-excel" id="dokumen" name="gallery" class="" onchange="uploadFile('formDokumen','dokumen')" style="display: none">
              <?php if(!$is_locked):?>
                <a href="javascript:;" onclick="$('#dokumen').click();" class="mdl-button btn-file mdl-button  mdl-button--raised mdl-js-ripple-effect btn-red" >Add Dokumen Pendukung</a>
              <?php else:?>
                <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled ">Add Dokumen Pendukung</a>
              <?php endif;?>
            </div>
          </div>
        </center>
      </form>
      <br/>
      <hr>
      <?php if(!$is_locked):?>
              <a href="<?= base_url().'project_target_and_investasi/set_page/'.$data_project->id ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Target & Investasi</a><br><br>

              <a href="<?= base_url().'project_sdm_area_sales/set_page/view_detail_group/'.$data_project->id  ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">SDM Area Sales</a><br><br>

              <a href="<?= base_url().'project_sdm_area_after_sales/set_page/view_detail_group/'.$data_project->id  ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">SDM Area After Sales</a><br><br>

              <a href="<?= base_url().'project_executive_summary/set_page/'.$data_project->id ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Executive Summary</a><br><br>
      <?php else:?>
        <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">Target & Investasi</a><br><br>

        <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">SDM Area Sales</a><br><br>

        <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">SDM Area After Sales</a><br><br>

        <a href="javascript:;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-disabled">Executive Summary</a><br><br>

      <?php endif;?>
       <!--  <a href="<?= base_url().'add_man_power/set_page/'.$data_project->id.'/'.$data_project->jenis_epm ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Dokumen Pendukung</a><br><br> -->

      <hr>
      <?php if(count($dataProjectArea)>0):?>
        <?php foreach ($dataProjectArea as $row) :?>
            <h2><?=$row['area_name']?></h2>
            <?php foreach($row['sub'] as $sub):?>
              <?php if($sub['allow_inspection']):?>
                <a href="<?= base_url('inspection_sub_area/set_page/'.$sub['id'].'/'.$data_project->jenis_epm) ?>" 
                  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-black" 
                  ><?=$sub['sub_area_name']?></a>
              <?php else:?>
                <a href="javascript:;" 
                  class="mdl-button mdl-js-button mdl-button--raised btn-disabled" 
                  ><?=$sub['sub_area_name']?></a>
              <?php endif;?>
                <br><br>
            <?php endforeach;?>

        <?php endforeach;?>
      <?php else:?>
          <h2>Data Project Area Kosong!</h2>
      <?php endif;  ?>
    <?php else:?>
      <center>
        <h2>Data tidak ditemukan</h2>
      </center>
    <?php endif; ?>

      <hr>
      <a href="<?=base_url('project')?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-red">Back</a><br><br>
    </div>
  </div>
</center>
<?php $this->load->view('tpl_photoswipe'); ?>