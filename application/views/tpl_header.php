<!--BEGIN HEADER -->
<div class="page-header navbar-fixed-top" style="background: url('<?php echo base_url("assets/pages/img/toyota-background.png"); ?>') left top no-repeat;">
    <!-- BEGIN HEADER INNER -->
    <div class="clearfix">
        <!-- BEGIN BURGER TRIGGER -->
        <div class="burger-trigger">
            <button class="menu-trigger" style="opacity: 0.5">
                <img src="<?php echo base_url(); ?>assets/layouts/layout7/img/m_toggler.png" alt="" style=""> 
            </button>
            <div class="menu-overlay menu-overlay-bg-transparent">
                <div class="menu-overlay-content">
                    <ul class="menu-overlay-nav text-uppercase">
                        <li>
                            <a href="<?php echo base_url('home'); ?>">Home</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('project'); ?>">Project</a>
                        </li>
                        <?php if($this->session->userdata('user_type') == 'dealer' || $this->session->userdata('user_type') == 'superadmin'){ ?>
                        <li>
                            <a href="<?php echo base_url('toss'); ?>">TOSS</a>
                        </li>
                        <?php } ?>
                        <li>
                            <a href="<?php echo base_url('change_password'); ?>">Change Password</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('login/logout'); ?>">Log Out</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="menu-bg-overlay">
                <button class="menu-close">&times;</button>
            </div>
            <!-- the overlay element -->
        </div>
        <!-- END NAV TRIGGER -->
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/pages/img/logo-tam.png'); ?>" style="width: 75px" alt="logo" class="logo-default" />
                <!-- <span style="font-size: x-large; color: black"><b>OIS</b></span>  -->
            </a>
        </div>
        <!-- END LOGO -->
        
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER