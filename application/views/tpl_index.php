<?php

/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Ayub Anggara
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */


$username = $this->session->userdata('username');
$email = $this->session->userdata('email');
$user_type = $this->session->userdata('user_type');

if(empty($username) || empty($email) || empty($user_type)){
    redirect('login');
}else{
    if($user_type == 'Administrator' || $user_type == 'Admin'){
        redirect('login');    
    }
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>OIS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

       
        <script>
            var base_url = '<?php echo base_url(); ?>';
            paceOptions = {
                ajax: {ignoreURLs: [base_url+'connection_test']}
            }
        </script>

        <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="<?=base_url()?>assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="<?=base_url()?>assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.deep_purple-pink.min.css" />

        <meta content="Yk Digital" name="author" />
        <link rel="icon" type="image/png" href="<?=base_url('assets/global/img/favicon/')?>favicon-toyota.png" sizes="32x32" />
        <script defer src="https://code.getmdl.io/1.3.0/material.js"></script>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- <link href="<?php echo base_url();?>assets/global/plugins/mapplic/mapplic/mapplic.css" rel="stylesheet" type="text/css" /> -->

        <!-- datedropper 3 -->
        <link href="<?php echo base_url();?>assets/global/plugins/datedropper3/datedropper.css" rel="stylesheet" type="text/css" />

        <!-- swiper 4.4.1 -->
        <link href="<?php echo base_url();?>assets/global/plugins/swiper/css/swiper.min.css" rel="stylesheet" type="text/css" />

        <!-- offline js -->
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-theme-chrome.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-language-english.css" rel="stylesheet" type="text/css" />
        <!-- holdon js -->
        <link href="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.css" rel="stylesheet" type="text/css" />

        <?php if(isset($issignature) && $issignature=='true'){ ?>

        <!-- signature pad -->
        <link href="<?php echo base_url();?>assets/global/plugins/signature-pad/css/signature-pad.css" rel="stylesheet" type="text/css" /> 
        
        <?php } ?>       

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>assets/global/css/components-rounded.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>assets/layouts/layout7/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/layouts/layout7/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/style.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->

        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        
        
        <link rel="shortcut icon" href="#" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid" style="">
        <input type="text" id="isHoldOnOpen" value="false" style="display: none">
        <?php $this->load->view('tpl_header'); ?>
        <!-- BEGIN CONTAINER -->
        <div class="page-container page-content-inner page-container-bg-solid">
            
            <br>
            <!-- BEGIN CONTENT -->
            <div class="container-fluid container-lf-space">
                <?php $this->load->view($content); ?>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('tpl_footer'); ?>
        <!-- END FOOTER -->
       
        <!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

        <!-- datedropper 3 -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/datedropper3/datedropper.js" type="text/javascript"></script>

        <!-- swiper 4.4.1 -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/swiper/js/swiper.min.js" type="text/javascript"></script>

        <!-- offline js -->
        <script src="<?=base_url()?>assets/global/plugins/offline-js/offline.min.js" type="text/javascript"></script>
        <!-- holdon js -->
        <script src="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.js" type="text/javascript"></script>

        <?php if(isset($issignature) && $issignature=='true'){ ?>

        <!-- signature pad -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/signature-pad/js/signature_pad.umd.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/signature-pad/js/app.js" type="text/javascript"></script>

        <?php } ?>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>

        <!-- <script src="<?php echo base_url(); ?>assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/layouts/layout7/scripts/layout.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <?php if(isset($isgmap) && $isgmap=='true'){ ?>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkT5BSWYhFpd-qliznJjlMtQP-L5A9LJg&libraries=places"></script>

        <?php } ?>

        <script type="text/javascript">
            var csrf = {
                name:'<?php echo $this->security->get_csrf_token_name();?>',
                token:'<?php echo $this->security->get_csrf_hash(); ?>',
            };
            var burl='<?=base_url()?>';
        </script>
        <script src="<?=base_url('assets/scripts/'.$tpl_js.'.js')?>"></script>
        <script type="text/javascript">
            //offline js with hold on js for network down error handling
            var run = function(){
            
              var req = new XMLHttpRequest();
              req.timeout = 5000;
              req.open('GET', base_url+'adminx/connection_test', true);
              req.send();

                Offline.on('confirmed-down', function () {
                   
                        if($('#isHoldOnOpen').val() == 'true'){

                        }else if($('#isHoldOnOpen').val() == 'false'){
                            console.log('open holdon');
                            $('#isHoldOnOpen').val('true');
                            HoldOn.open();
                        }
                        
                });

                Offline.on('confirmed-up', function () {

                    if($('#isHoldOnOpen').val() == 'true'){
                        console.log('open holdon');
                        $('#isHoldOnOpen').val('false');
                        HoldOn.close();
                    }

                });


            }

            setInterval(run, 3000);
            // end of offline js with hold on js for network down error handling
        </script>
    </body>

</html>
