<style>
.demo-card-square.mdl-card {
    /*max-width: 320px;*/
  width: 100%;
  height: 320px;
}
.demo-card-square > .mdl-card__title {
  color: #fff;
  /*background-size: inherit; background: url('') bottom right 15% no-repeat #46B6AC;*/
  background-color: black;
}
</style>

<div class="row">
    <?php if(!empty($fe_ls_toss)){ ?>
        <?php foreach ($fe_ls_toss as $key => $value) { ?>
            <?php if($value->status=='uncertified'){ ?>
            	  <div class="col-md-4" style="padding-bottom: 50px">
                  
                        <a href="<?php echo base_url('toss_content/set_page/view_option/').$value->id; ?>">
                          <div class="demo-card-square mdl-card mdl-shadow--2dp">
                            <div class="mdl-card__title mdl-card--expand" style="background-size: cover !important; background: url('<?php echo $value->outlet_photo_file_name_64; ?>') center top no-repeat black;">
                            <!-- <div class="mdl-card__title mdl-card--expand"> -->
                              <h2 class="mdl-card__title-text"><?php echo $value->nama_toss; ?></h2>
                            </div>
                            <div class="mdl-card__supporting-text">
                              <?php echo $value->nama_kecamatan; ?> - <?php echo $value->nama_kota; ?> - <span style="font-size: large; color: red;">Uncertified</span>
                            </div>
                          </div>
                        </a>
                </div>
            <?php } ?>

            <?php if($value->status=='certified'){ ?>
                <div class="col-md-4" style="padding-bottom: 50px">
                  <a href="<?php echo base_url('toss_content/set_page/view_option/').$value->id; ?>">
                    <div class="demo-card-square mdl-card mdl-shadow--2dp">
                      <div class="mdl-card__title mdl-card--expand" style="background-size: cover !important; background: url('<?php echo $value->outlet_photo_file_name_64; ?>') center top no-repeat black;">
                      <!-- <div class="mdl-card__title mdl-card--expand"> -->
                        <h2 class="mdl-card__title-text"><?php echo $value->nama_toss; ?></h2>
                      </div>
                      <div class="mdl-card__supporting-text">
                        <?php echo $value->kecamatan; ?> - <?php echo $value->kota_kabupaten; ?> - <span style="font-size: large;">Certified</span>
                      </div>
                    </div>
                  </a>
                </div>
            <?php } ?>
        <?php } ?>
    <?php }else{ ?>
        <center><h1>Data TOSS Kosong!</h1></center>
    <?php } ?>
</div>