<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Inspection_equipment_and_tools extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MInspectionEquipmentAndTools");
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    public function set_page($id_project='', $jenis_epm='')
    {

        if (!empty($id_project) and $this->check_its_mine($id_project)) {

            if ($jenis_epm == '50' || $jenis_epm == '75' || $jenis_epm == '100') {

                $data['id_project']               = $id_project;
                $data['jenis_epm']                = $jenis_epm;
                $data['data_equipment_and_tools'] = $this->MInspectionEquipmentAndTools->data_project_equipment_and_tools($id_project, $jenis_epm)->result_array();
                $data['content']                  = 'view_inspection_equipment_and_tools';
                $data['tpl_js']                   = 'inspection_equipment_and_tools';
                $this->load->view('tpl_index', $data);

            } else {
                redirect('errors/code/404');
            }

        } else {

            redirect('errors/code/404');
        }
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function update_data($id_project, $jenis_epm)
    {

        $project_equipment_and_tools_data = $this->MInspectionEquipmentAndTools->data_project_equipment_and_tools($id_project, $jenis_epm)->result_array();
        
        for ($i = 0; $i < count($project_equipment_and_tools_data); $i++) {
            $belumorder     = xss_clean($this->input->post('belumorder-' . $project_equipment_and_tools_data[$i]['id']));
            $order          = xss_clean($this->input->post('order-' . $project_equipment_and_tools_data[$i]['id']));
            $tanggalorder   = xss_clean($this->input->post('tanggalorder-' . $project_equipment_and_tools_data[$i]['id']));
            $materialonsite = xss_clean($this->input->post('materialonsite-' . $project_equipment_and_tools_data[$i]['id']));
            $terpasang      = xss_clean($this->input->post('terpasang-' . $project_equipment_and_tools_data[$i]['id']));
            $keterangan     = xss_clean($this->input->post('keterangan-' . $project_equipment_and_tools_data[$i]['id']));

            if (!empty($belumorder) && $belumorder == 1) {
                $belum_order_score = 1;
            } else if (empty($belumorder)) {
                $belum_order_score = 0;
            }

            if (!empty($order) && $order == 1) {
                $order_score = 1;
            } else if (empty($order)) {
                $order_score = 0;
            }

            if (!empty($tanggalorder)) {
                $tanggal_order = date('Y-m-d H:i:s', strtotime($tanggalorder));
            } else if (empty($tanggalorder)) {
                $tanggal_order = null;
            }

            if (!empty($materialonsite) && $materialonsite == 1) {
                $material_onsite_score = 1;
            } else if (empty($materialonsite)) {
                $material_onsite_score = 0;
            }

            if (!empty($terpasang) && $terpasang == 1) {
                $terpasang_score = 1;
            } else if (empty($terpasang)) {
                $terpasang_score = 0;
            }

            if (!empty($keterangan)) {
                $keterangan = $keterangan;
            } else if (empty($keterangan)) {
                $keterangan = null;
            }

            $data = array(
                'belum_order_score'     => $belum_order_score,
                'order_score'           => $order_score,
                'tanggal_order'         => $tanggal_order,
                'material_onsite_score' => $material_onsite_score,
                'terpasang_score'       => $terpasang_score,
                'keterangan'            => $keterangan,
            );

        }

        $res = $this->MInspectionEquipmentAndTools->update_data_project_equipment_and_tools($project_equipment_and_tools_data[$i]['id'], $data);

        if ($res) {
            redirect('area/set_page/' . $id_project);
        }
    }
}
