<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        $user_type = $this->session->userdata('user_type');
        if ($logged_in && ($user_type == 'tam' || $user_type == 'dealer' || $user_type == 'konsultan' || $user_type == 'superadmin')) {
            redirect('home');
        } else {
            $this->load->view('view_login');
        }
    }

    public function validate()
    {

        $this->form_validation->set_rules('email', '', 'required');
        $this->form_validation->set_rules('password', '', 'required');

        $email    = xss_clean($this->input->post('email'));
        $password = xss_clean($this->input->post('password'));
        if ($this->form_validation->run() == true) {

            $q = $this->db->select('id,username,email,user_type,password')
                ->from('tbl_user')
                ->where('email',$email)
                ->where('is_delete', 0)
                ->where_in('user_type', ['superadmin', 'konsultan', 'dealer', 'tam'])
                ->get();

            if ($q->num_rows() > 0) {
                //userdata
                $data = $q->row_array();
                if (compare_password($password, $data['password'])) {

                    $data['logged_in'] = true;

                    $this->session->set_userdata($data);
                    $this->MLogging->system_event('login', 'System Event', 'Login', 'Authentication', 'Success', 'Via Front End');

                    redirect('home');
                } else {
                    $this->session->set_flashdata('message', 'Wrong email or password');
                    // redirect($this->agent->referrer());
                    redirect('login');
                }

            } else {
                $this->session->set_flashdata('message', 'Wrong email');
                redirect('login');
            }

        } else {
            $this->session->set_flashdata('message', 'Please complete all fields');
            redirect('login');
        }

    }

    public function logout()
    {
        $this->MLogging->system_event('logout', 'System Event', 'Logout', 'Authentication', 'Success', 'Via Front End');

        $this->session->sess_destroy();
        redirect('login');

    }
}
