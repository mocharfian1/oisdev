<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Man_power_list extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('MManPowerList');
        $this->load->model('MHome');
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    public function set_page($id_project='', $jenis_epm='')
    {

        if (!empty($id_project) and $this->check_its_mine($id_project)) {

            if($jenis_epm == '50' || $jenis_epm == '75' || $jenis_epm == '100'){
                $data['data_man_power'] = $this->MManPowerList->read_project_man_power($id_project, $jenis_epm)->result_array();
                $data['id_project']     = $id_project;
                $data['jenis_epm']      = $jenis_epm;
                $data['content']        = 'view_man_power_list';
                $data['tpl_js']         = 'man_power_list';
                $this->load->view('tpl_index', $data);
            }else{
                redirect('errors/code/404');
            }

            
        } else {
            redirect('errors/code/404');
        }
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
}
