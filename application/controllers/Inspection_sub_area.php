<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Inspection_sub_area extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('MInspectionSubArea');
        $this->load->model('MHome');
    }

    public function index()
    {
        echo 'page_not_found';
    }
    public function delete_image($id)
    {
        $this->db->where('id', $id)->update('tbl_project_area_pic', ['is_delete' => true]);
    }
    public function set_page($id='', $jenis_epm='')
    {
        $user_type            = $this->session->userdata('user_type');
        $user_id              = $this->session->userdata('id');
        $data_project         = $this->MInspectionSubArea->read_id_project($id)->row();
        $data['data_project'] = $data_project;
        $data['id_project']   = $data_project->id_project;

        if (!empty($id) and $this->check_its_mine($data_project->id_project)) {
            $data['data_project_pic_kpd'] = $this->MInspectionSubArea->read_project_pic_kpd($data['id_project'])->result_array();
            $data['data_pic']             = $this->MInspectionSubArea->read_pic_where($id, $jenis_epm)->result_array();
            $data['data_pic_area']        = $this->MInspectionSubArea->read_pic_area_where($id)->result_array();
            $data['data_km']              = $this->MInspectionSubArea->read_km_where($id, $jenis_epm)->result_array();
            $data['data_ic']              = $this->MInspectionSubArea->read_ic_where($id, $jenis_epm)->result_array();
            $data['data_kp']              = $this->MInspectionSubArea->read_kp_where($id, $jenis_epm)->result_array();
            $data['data_ket']             = $this->MInspectionSubArea->read_ket_where($id, $jenis_epm)->result_array();
            $data['data_tps']             = $this->MInspectionSubArea->read_tps_where($id, $jenis_epm)->result_array();
            $data['data_minreq']          = $this->MInspectionSubArea->read_minreq_where($id, $jenis_epm)->result_array();
            $data['data_catatan']         = $this->MInspectionSubArea->read_catatan($id, $jenis_epm, $user_type)->row_array();
            $data['data_catatan_lainnya'] = $this->MInspectionSubArea->read_catatan_lainnya($id, $jenis_epm, $user_type)->row_array();
            $data['id']                   = $id;
            $data['jenis']                = $jenis_epm;
            $data['user_type']            = $user_type;
            $data['user_id']              = $user_id;
            $data['content']              = 'view_inspection_sub_area';
            $data['tpl_js']               = 'inspection_sub_area';
            $data['is_locked']            = $this->MInspectionSubArea->is_locked($data_project, $user_type);
            $this->load->view('tpl_index', $data);
        } else {
            redirect(base_url('errors/code/404'));
        }
    }
    private function check_its_mine($id_project)
    {

        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function checklist($id)
    {
        $jenis_epm       = xss_clean($this->input->post('jenis'));
        $id_project      = xss_clean($this->input->post('id_project'));
        $catatan         = xss_clean($this->input->post('catatan'));
        $catatan_lainnya = xss_clean($this->input->post('catatan_lainnya'));
        $dataic          = $this->MInspectionSubArea->read_ic_where($id, $jenis_epm)->result_array();
        $datakp          = $this->MInspectionSubArea->read_kp_where($id, $jenis_epm)->result_array();
        $datakm          = $this->MInspectionSubArea->read_km_where($id, $jenis_epm)->result_array();
        $dataminreq      = $this->MInspectionSubArea->read_minreq_where($id, $jenis_epm)->result_array();

        $user_type = $this->session->userdata('user_type');

        foreach ($dataic as $row) {

            $score      = xss_clean($this->input->post('ic-' . $row["id"]));
            $score      = ($score == 1) ? 1 : 0;
            $data_score = array(
                'is_filled'           => '1',
                'score_' . $user_type => $score,
                'jarak'               => xss_clean($this->input->post('ic-jarak-' . $row["id"])),
            );

            $this->MInspectionSubArea->insert_score_ic($row['id'], $data_score);
        }
        foreach ($datakp as $row) {

            $score      = xss_clean($this->input->post('kp-' . $row["id"]));
            $score      = ($score == 1) ? 1 : 0;
            $data_score = array(
                'is_filled'           => '1',
                'score_' . $user_type => $score,
            );
            $this->MInspectionSubArea->insert_score_kp($row['id'], $data_score);
        }

        foreach ($datakm as $row) {

            $score      = xss_clean($this->input->post('km-' . $row["id"]));
            $score      = ($score == 1) ? 1 : 0;
            $data_score = array(
                'is_filled'           => '1',
                'score_' . $user_type => $score,
                'jarak'               => xss_clean($this->input->post('km-jarak-' . $row["id"])),
            );
            $this->MInspectionSubArea->insert_score_km($row['id'], $data_score);
        }

        $this->MInspectionSubArea->update_catatan($id, $jenis_epm, $catatan, $user_type);
        $this->MInspectionSubArea->update_catatan_lainnya($id, $jenis_epm, $catatan_lainnya, $user_type);

        $data_project = $this->MInspectionSubArea->read_id_project($id)->result_array();

        $data_evaluasi = $this->MInspectionSubArea->read_evaluasi_kunjungan($id_project, $jenis_epm)->result_array();
        if (count($data_evaluasi) > 0) {
            $data_project_catatan = $this->MInspectionSubArea->read_catatan($id, $jenis_epm, $this->session->userdata('user_type'))->result_array();
            $data_evaluasi_detail = array(
                'id_evaluasi_kunjungan'  => $data_evaluasi[0]['id'],
                'id_project_area'        => $id,
                'id_project_catatan'     => $data_project_catatan[0]['id'],
                'eval_problem_deskripsi' => $data_project_catatan[0]['catatan'],
            );
            if ($this->MInspectionSubArea->read_evaluasi_detail($id, $data_project_catatan[0]['id'])->num_rows() > 0) {
                $data_evaluasi_kunjungan_detail = $this->MInspectionSubArea->read_evaluasi_detail($id, $data_project_catatan[0]['id'])->result_array();
                $this->MInspectionSubArea->update_evaluasi_detail($data_evaluasi_kunjungan_detail[0]['id'], $data_evaluasi_detail);
            } else {
                $this->MInspectionSubArea->insert_evaluasi_detail($data_evaluasi_detail);
            }
        }
        return redirect('area/set_page/' . $id_project);
    }

    public function file_camera_upload($id_project_area)
    {

        if (!empty($_FILES['camera']['name'])) {
            $path                  = FCPATH . 'adminx/upload/'; // path folder
            $config['upload_path'] = $path; // variabel path untuk config upload

            $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
            $config['max_size']         = '20480000'; // maksimum besar file 2M
            $config['file_ext_tolower'] = true;
            $config['encrypt_name']     = true;
            // $config['file_name']        = 'camera';

            // $this->load->library('upload', $config);
            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!is_writable($path)) {
                //folder not writable

                $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

            } else {
                //folder writable
                if (!$this->upload->do_upload('camera')) //element name here
                {
                    $response = array('status' => 'error', 'message' => 'Picture upload failed, error : ' . $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name   = $upload_data['file_name'];
                    // $response = array('status'=>'success','message'=>'File upload success');

                    //save data
                    $response = $this->MInspectionSubArea->save_photo_data($file_name, $id_project_area, 'area');
                }
            }
        } else {
            $response = array('status' => 'error', 'message' => 'Picture upload failed, error : file not exist');
        }

        echo json_encode($response);
    }

    public function file_gallery_upload($id_project_area)
    {

        if (!empty($_FILES['gallery']['name'])) {
            $path                  = FCPATH . 'adminx/upload/'; // path folder
            $config['upload_path'] = $path; // variabel path untuk config upload

            $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
            $config['max_size']         = '2048000'; // maksimum besar file 20M
            $config['file_ext_tolower'] = true;
            // $config['file_name']        = 'gallery';
            $config['encrypt_name'] = true;

            // $this->load->library('upload', $config);
            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!is_writable($path)) {
                //folder not writable

                $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

            } else {
                //folder writable
                if (!$this->upload->do_upload('gallery')) //element name here
                {
                    $response = array('status' => 'error', 'message' => 'Profile picture upload failed, error : ' . $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name   = $upload_data['file_name'];
                    // $response = array('status'=>'success','message'=>'File upload success');

                    //save data
                    $response = $this->MInspectionSubArea->save_photo_data($file_name, $id_project_area, 'layout');
                }
            }
        } else {
            $response = array('status' => 'error', 'message' => 'Profile picture upload failed, error : file not exist');
        }

        echo json_encode($response);
    }

    public function file_config_upload()
    {
        $id_config_pic   = xss_clean($this->input->post('id_config_pic'));
        $id_project_area = xss_clean($this->input->post('id_project_area'));
        if (!empty($_FILES['gallery']['name'])) {
            $path                  = FCPATH . 'adminx/upload/'; // path folder
            $config['upload_path'] = $path; // variabel path untuk config upload

            $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
            $config['max_size']         = '2048000'; // maksimum besar file 20M
            $config['file_ext_tolower'] = true;
            // $config['file_name']        = 'gallery';
            $config['encrypt_name'] = true;

            // $this->load->library('upload', $config);
            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!is_writable($path)) {
                //folder not writable

                $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

            } else {
                //folder writable
                if (!$this->upload->do_upload('gallery')) //element name here
                {
                    $response = array('status' => 'error', 'message' => 'Profile picture upload failed, error : ' . $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name   = $upload_data['file_name'];
                    // $response = array('status'=>'success','message'=>'File upload success');

                    //save data
                    $response = $this->MInspectionSubArea->save_photo_data($file_name, $id_project_area, 'picture');
                    $this->MInspectionSubArea->insert_pic($id_config_pic, ['id_project_area_pic' => $response['id']]);
                }
            }
        } else {
            $response = array('status' => 'error', 'message' => 'Profile picture upload failed, error : file not exist');
        }

        echo json_encode($response);
    }
}
