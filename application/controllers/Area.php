<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MArea");
        $this->load->model("MProject");
    }

    public function index()
    {
        //redirect to page not found
        redirect('errors/code/404');
    }
    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function set_page($id_project = '')
    {

        $user_type          = $this->session->userdata('user_type');
        $user_id            = $this->session->userdata('id');
        $data['id_project'] = $id_project;

        if (!empty($id_project) and $this->check_its_mine($id_project)) {
            $data_project          = $this->MProject->get_data_project($id_project)->row();
            $data['data_project']  = $data_project;
            $data_project_area     = $this->MArea->data_project_area($id_project)->result_array();
            $data_project_pic      = $this->MArea->data_project_pic($id_project)->result_array();
            $data_project_sub_area = $this->MArea->data_project_sub_area($id_project)->result_array();
            // $project_sub_area = $this->MArea->data_project_sub_area($id_project)->result_array();
            for ($x = 0; $x < count($data_project_sub_area); $x++) {
                $allow_inspection = $this->allow_inspection_filter($data_project, $this->session->userdata('id'), $data_project_sub_area[$x]['id']);

                $data_project_sub_area[$x]['allow_inspection'] = $allow_inspection;
            }
            $dataProjectArea = [];
            foreach ($data_project_area as $row) {
                $dataProjectArea[$row['id_area']] = $row;
            }
            foreach ($data_project_sub_area as $row) {
                $dataProjectArea[$row['id_area']]['sub'][$row['id']] = $row;
            }
            $data['dataProjectArea']          = $dataProjectArea;
            $data['data_pic']                 = $data_project_pic;
            $data['data_equipment_and_tools'] = $this->MArea->data_project_equipment_and_tools($id_project)->result_array();
            $data['is_locked']                = $this->is_locked($data_project, $user_type);
            $data['content']                  = 'view_area';
            $data['tpl_js']                   = 'area';
            $this->load->view('tpl_index', $data);
        } else {
            redirect('errors/code/404');
        }
    }

    public function allow_inspection_filter($data_project, $userid, $id_project_area)
    {
        $level1_km = $this->MArea->checking_project_km_input($id_project_area, $data_project->type_level_1);
        $level1_ic = $this->MArea->checking_project_ic_input($id_project_area, $data_project->type_level_1);
        $level1_kp = $this->MArea->checking_project_kp_input($id_project_area, $data_project->type_level_1);

        $level2_km = $this->MArea->checking_project_km_input($id_project_area, $data_project->type_level_2);
        $level2_ic = $this->MArea->checking_project_ic_input($id_project_area, $data_project->type_level_2);
        $level2_kp = $this->MArea->checking_project_kp_input($id_project_area, $data_project->type_level_2);

        $level3_km = $this->MArea->checking_project_km_input($id_project_area, $data_project->type_level_3);
        $level3_ic = $this->MArea->checking_project_ic_input($id_project_area, $data_project->type_level_3);
        $level3_kp = $this->MArea->checking_project_kp_input($id_project_area, $data_project->type_level_3);
        $result    = false;
        if ($data_project->jenis_epm == '50' or $data_project->jenis_epm == '75') {
            //Berurutan, level 1 dulu, kemudian level 2, kemudian level 3.

            if ($userid == $data_project->id_user_level_1) {
                //Level 1 langsung input
                $result = true;
            } elseif ($userid == $data_project->id_user_level_2) {
                if ($level1_ic > 0 or $level1_kp > 0) {
                    $result = true;
                }
            } elseif ($userid == $data_project->id_user_level_3) {
                if ($level2_ic > 0 or $level2_kp > 0) {
                    $result = true;
                }
            }
        } elseif ($data_project->jenis_epm == '100') {
            //harus level 1 dulu, 2&3 boleh barengan
            if ($userid == $data_project->id_user_level_1) {
                $result = true;
            } elseif ($userid == $data_project->id_user_level_2 or $userid == $data_project->id_user_level_3) {
                if ($level1_km > 0) {
                    $result = true;
                }
            }
        }

        return $result;
    }

    public function is_locked($data_project, $user_type)
    {
        if ($user_type == $data_project->type_level_1) {
            if ($data_project->lock_user_level_1 == 1) {
                return true;
            }
        } elseif ($user_type == $data_project->type_level_2) {
            if ($data_project->lock_user_level_2 == 1) {
                return true;
            }
        } elseif ($user_type == $data_project->type_level_3) {
            if ($data_project->lock_user_level_3 == 1) {
                return true;
            }
        }
        return false;
    }

    public function delete_image($id)
    {
        $this->db->where('id', $id)->update('tbl_project_pic', ['is_delete' => true]);
    }
    public function file_gallery_upload($id_project, $type)
    {

        if (!empty($_FILES['gallery']['name'])) {
            $path                  = FCPATH . 'adminx/upload/'; // path folder
            $config['upload_path'] = $path; // variabel path untuk config upload

            $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
            $config['max_size']         = '2048000'; // maksimum besar file 20M
            $config['file_ext_tolower'] = true;
            // $config['file_name']        = 'gallery';
            $config['encrypt_name'] = true;

            // $this->load->library('upload', $config);
            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!is_writable($path)) {
                //folder not writable

                $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

            } else {
                //folder writable
                if (!$this->upload->do_upload('gallery')) //element name here
                {
                    $response = array('status' => 'error', 'message' => ' upload failed, error : ' . $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name   = $upload_data['file_name'];
                    // $response = array('status'=>'success','message'=>'File upload success');

                    //save data
                    $response = $this->MArea->save_photo_data($file_name, $id_project, $type);
                }
            }
        } else {
            $response = array('status' => 'error', 'message' => ' upload failed, error : file not exist');
        }

        echo json_encode($response);
    }

    public function file_upload($id_project, $type)
    {

        if (!empty($_FILES['gallery']['name'])) {
            $path                  = FCPATH . 'adminx/upload/'; // path folder
            $config['upload_path'] = $path; // variabel path untuk config upload

            $config['allowed_types']    = 'xls|xlsx|pdf|zip|rar'; // type yang dapat diakses bisa anda sesuaikan
            $config['max_size']         = '2048000'; // maksimum besar file 20M
            $config['file_ext_tolower'] = true;
            // $config['file_name']        = 'gallery';
            $config['encrypt_name'] = true;

            // $this->load->library('upload', $config);
            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!is_writable($path)) {
                //folder not writable

                $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

            } else {
                //folder writable
                if (!$this->upload->do_upload('gallery')) //element name here
                {
                    $response = array('status' => 'error', 'message' => ' upload failed, error : ' . $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());

                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name   = $upload_data['file_name'];
                    // $response = array('status'=>'success','message'=>'File upload success');
                    $this->db->where(['id_project' => $id_project, 'type' => $type])->delete('tbl_project_pic');
                    //save data
                    $response = $this->MArea->save_photo_data($file_name, $id_project, $type);
                }
            }
        } else {
            $response = array('status' => 'error', 'message' => 'upload failed, error : file not exist');
        }

        echo json_encode($response);
    }

}
