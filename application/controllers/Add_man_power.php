<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Add_man_power extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('MAddManPower');
        $this->load->model('MHome');
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function set_page($id_project = '', $jenis_epm='')
    {
        if (!empty($id_project) and $this->check_its_mine($id_project)) {

            if ($jenis_epm == '50' || $jenis_epm == '75' || $jenis_epm == '100') {

                $data['data_mp_level_training'] = $this->MAddManPower->read_mp_level_training()->result_array();
                $data['id_project']             = $id_project;
                $data['jenis_epm']              = $jenis_epm;
                $data['content']                = 'view_add_man_power';
                $data['tpl_js']                 = 'add_man_power';
                $this->load->view('tpl_index', $data);

            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }
    }

    public function add_project_man_power()
    {

        $id_project                = xss_clean($this->input->post('id'));
        $jenis_epm                 = xss_clean($this->input->post('jenis_epm'));
        $id_project_level_training = xss_clean($this->input->post('id_project_level_training'));
        if ($id_project_level_training == 0 or $id_project_level_training == '') {

            redirect('add_man_power/set_page/' . $id_project . '/' . $jenis_epm . '?error=1');
            return false;
        }
        $this->db->trans_begin();
        $data = array(
            'id_project'                => $id_project,
            'id_project_level_training' => $id_project_level_training,
            'jenis_epm'                 => $jenis_epm,
            'posisi'                    => xss_clean($this->input->post('posisi')),
            'nama'                      => xss_clean($this->input->post('nama')),
            'rotasi_mutasi_score'       => xss_clean($this->input->post('rotasi_mutasi_score')),
            'cabang_sumber'             => xss_clean($this->input->post('cabang_sumber')),
            'keterangan'                => xss_clean($this->input->post('keterangan')),
        );
        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        $insert = $this->MAddManPower->insert_data($data, 'tbl_project_man_power');

        $smodule  = 'front end project';
        $activity = 'Change Project Data';
        $tbl_name = 'tbl_project_man_power';
        $action   = 'insert project man power';

        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {
            if ($dberror['code'] != '0') {
                // catch error DB

                $this->db->trans_rollback();

                //insert insert log
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

            } else {

                $this->db->trans_rollback();

                //insert insert log
                $addtional_information = '';
                $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

            }
        } else {
            $this->db->trans_commit();

            //insert insert log
            $addtional_information = '';
            $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

        }
        redirect('area/set_page/' . $id_project);

    }

}
