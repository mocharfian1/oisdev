<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toss_content extends CI_Controller {

	public function __Construct() {
		parent::__Construct();
		if(!$this->session->userdata('logged_in')) {
			redirect(base_url());
		}
        // $this->load->model("MHome");
	}

	public function index()
	{
		//redirect to page not found
		echo 'Page not found';
		echo "";
	}

	public function getkota($id_prov){
		echo json_encode($data['kota_kab'] = $this->db->where('id_provinsi',$id_prov)->select('*')->get('tbl_lokasi_kota_kabupaten')->result());
	}

	public function getkec($id_kota){
		echo json_encode($this->db->where('id_kota_kabupaten',$id_kota)->select('*')->get('tbl_lokasi_kecamatan')->result());
	}

	public function getcabanginduk(){
		echo json_encode($this->db->where('dealer_name',$_GET['dealer'])->select('*')->order_by('outlet_name','asc')->group_by('outlet_name')->get('tbl_toss_dealer')->result());
	}

	public function set_page($mode,$id,$id_sub_area=null,$is_evaluation=null) {
		$data['is_evaluate'] = $is_evaluation;
		if(!empty($id_sub_area) && $is_evaluation=='evaluation'){
			$data['evaluate'] = '/is/evaluation';
			$data['sub_evaluate'] = '/evaluation';
		}else{
			$data['evaluate'] = '';
			$data['sub_evaluate'] = '';
		}

		$data['id_project'] = $id;
		$data['mode'] = $mode;
		$this->load->model('model_toss');
		$data['data_profile'] = $this->model_toss->data_profile($id)[0];

		if($mode == 'toss_profile'){
			$data['isgmap'] = 'true';
			$data['tpl_js'] = 'toss_profile';
			$data['plugin_mode'] = '1';

			$data['provinsi'] = $this->db->select('*')->order_by('nama','asc')->get('tbl_lokasi_provinsi')->result();
			$data['kota_kab'] = $this->db->select('*')->order_by('nama','asc')->get('tbl_lokasi_kota_kabupaten')->result();
			$data['kecamatan'] = $this->db->select('*')->order_by('nama','asc')->get('tbl_lokasi_kecamatan')->result();

			$data['dealer'] = $this->db->select('*')->order_by('dealer_name','asc')->group_by('dealer_name')->get('tbl_toss_dealer')->result();

			$dealer_name = $data['data_profile']->dealer;
			$data['outlet_name'] = $this->db->where('dealer_name',$dealer_name)->select('*')->group_by('outlet_name')->order_by('outlet_name','asc')->get('tbl_toss_dealer')->result();

		}else if($mode == 'toss_profile_certified'){
			$data['isgmap'] = 'true';
			$data['tpl_js'] = 'toss_profile_certified';
		}else if($mode == 'final_submit'){
			$data['issignature'] = 'true';
			$data['tpl_js'] = 'toss_content';
		}else{
			$data['tpl_js'] = 'toss_content';
		}

		if($mode=='checklist_sertifikasi'){

			$data['area'] = $this->model_toss->areaFE($id);
			
			foreach ($data['area'] as $key => $value) {
				$data['area'][$key]->sub_area = [];
				$sub = $this->model_toss->sub_areaFE($value->id_area,$id);
				foreach ($sub as $key_sub => $value_sub) {
					array_push($data['area'][$key]->sub_area,$value_sub);
				}
			}

		}

		if($mode=='sub_area'){
			$data['items'] = $this->model_toss->items($id,$id_sub_area);
		}


		if($mode=='equipment'){
			$data['groupEQ'] = $this->model_toss->GroupEQFE($id);

			foreach ($data['groupEQ'] as $key => $value) {
				$data['groupEQ'][$key]->items = [];
				$items = $this->model_toss->ItemsEQFE($id,$value->group_key);
				foreach ($items as $key_items => $value_items) {
					array_push($data['groupEQ'][$key]->items,$value_items);
					$data['groupEQ'][$key]->items[$key_items]->isi_eq = [];
					$isi_eq = $this->model_toss->Isi_EQ($value_items->id);
					foreach ($isi_eq as $key_pt => $value_pt) {
						array_push($data['groupEQ'][$key]->items[$key_items]->isi_eq,$value_pt);
					}
				}
			}
			// print_r($data['groupEQ']);
		}

		if($mode=='manpower'){
			$data['ls_manpower'] = $this->model_toss->ls_manpower($id);
		}

		$data['id_toss_front_end'] = $id;

		$data['content'] = 'view_toss_content';
		
		$this->load->view('tpl_index',$data);
	}


}
