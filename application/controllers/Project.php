<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Project extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MProject");
    }

    public function index()
    {
        $data['user_session'] = $this->session->userdata;
        $id_user              = $this->session->userdata('id');
        $data['data_project'] = $this->MProject->data_project($id_user)->result_array();
        $data['menu']         = 'project';
        $data['sub_menu']     = '';
        $data['content']      = 'view_project';
        $data['tpl_js']       = 'project';
        $data['plugin_mode']  = '1';
        $this->load->view('tpl_index', $data);
    }
}
