<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Errors extends CI_Controller 
{
 public function __construct() 
 {
    parent::__construct(); 
 } 

 public function index() 
 { 
   $this->code('404'); 
 } 

 public function code($code='', $user_message=null)
 {
 	if($code == '' || $code == '404'){
		$this->output->set_status_header('404'); 
		$user_message = "We can not find the page you're looking for.";
		$data = ['user_message' => $user_message, 'error_code' => '404'];
    	$this->load->view('view_error', $data);
	}else{
		$this->output->set_status_header($code); 
		$user_message = "We can not find the page you're looking for.";
		$data = ['user_message' => $user_message, 'error_code' => $code];
    	$this->load->view('view_error', $data);
	}
 }
} 
