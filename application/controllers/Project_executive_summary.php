<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Project_executive_summary extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MProject");
    }

    public function index()
    {
        echo 'page not found';
    }

    public function set_page($id_project = '')
    {
        if (!empty($id_project) and $this->check_its_mine($id_project)) {
            $data                 = $this->db->where('id_project', $id_project)->get('tbl_executive_summary')->row_array();
            $data['content']      = 'view_project_executive_summary';
            $data['tpl_js']       = 'project_executive_summary';
            $data_project         = $this->MProject->get_data_project($id_project)->row();
            $data['data_project'] = $data_project;
            $data['id']           = $id_project;
            $this->load->view('tpl_index', $data);
        } else {
            redirect(base_url('errors/code/404'));
        }
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function save($id_project)
    {
        $data               = $this->input->post('data');
        $data['id_project'] = $id_project;
        $qry                = $this->db->where('id_project', $id_project)->get('tbl_executive_summary');

        $this->db->trans_begin();

        $smodule            = 'front end project';
        $activity           = 'Change Project Data';
        $tbl_name           = 'tbl_executive_summary';
        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        if ($qry->num_rows() > 0) {

            $log_old_value_data = [];
            foreach ($qry->row_array() as $key => $value) {
                $log_old_value_data[] = $value;
            }
            $action = 'update project executive summery';

            $this->db->where('id_project', $id_project)->update('tbl_executive_summary', $data);

            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {

                if ($dberror['code'] != '0') {
                    // catch error DB

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data    = array();
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Update', '', '');
                } else {

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data = array();
                    $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Update', '', '');

                }

            } else {

                $addtional_information = '';

                $this->db->trans_commit();

                //insert update log
                $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Update', $log_old_value_data, $log_new_value_data);

            }
        } else {
            $action = 'insert project executive summery';

            $this->db->insert('tbl_executive_summary', $data);

            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {

                if ($dberror['code'] != '0') {
                    // catch error DB

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data    = array();
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', '');

                } else {

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data = array();
                    $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', '');

                }

            } else {

                $addtional_information = '';

                $this->db->trans_commit();

                //insert update log
                $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            }

        }

        redirect('area/set_page/' . $id_project);

    }
}
