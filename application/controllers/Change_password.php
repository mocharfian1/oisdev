<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Change_password extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data['content'] = 'view_change_password';
        $data['tpl_js']  = 'change_password';
        $this->load->view('tpl_index', $data);
    }

}
