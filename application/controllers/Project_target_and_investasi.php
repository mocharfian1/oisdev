<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Project_target_and_investasi extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
    }

    public function index()
    {
        echo 'page not found';
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function set_page($id_project='')
    {

        if (!empty($id_project) and $this->check_its_mine($id_project)) {
            $data            = $this->db->where('id_project', $id_project)->get('tbl_project_target_investasi')->row_array();
            $data['id']      = $id_project;
            $data['content'] = 'view_project_target_and_investasi';
            $data['tpl_js']  = 'project_target_and_investasi';
            $this->load->view('tpl_index', $data);
        } else {
            redirect(base_url('errors/code/404'));
        }
    }
    public function save($id_project)
    {

        $qry                = $this->db->where('id_project', $id_project)->get('tbl_project_target_investasi');
        $data               = xss_clean($this->input->post('input'));
        $data['id_project'] = $id_project;
        if ($qry->num_rows() > 0) {
            $this->db->where('id_project', $id_project)->update('tbl_project_target_investasi', $data);
        } else {
            $this->db->insert('tbl_project_target_investasi', $data);
        }
        return redirect('area/set_page/' . $id_project);
    }

}
