<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Toss extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('model_toss');
        // $this->load->model("MHome");
    }

    public function index()
    {
        $data['user_session'] = $this->session->userdata;
        $id_user              = $this->session->userdata('id');
        $data['menu']         = 'toss';
        $data['sub_menu']     = '';
        $data['content']      = 'view_toss';
        $data['tpl_js']       = 'toss';

        $data['fe_ls_toss'] = $this->model_toss->front_end_ls_toss($id_user);

        $this->load->view('tpl_index', $data);
    }

    public function SUBMIT_set_profile()
    {
        $id    = xss_clean($this->input->post('id'));
        $data  = $this->input->post('data');
        $extra = xss_clean($this->input->post('extra'));
        if (!empty($data['tgl_sewa_start']) || !empty($data['tgl_sewa_end'])) {
            $data['tgl_sewa_start'] = date('Y-m-d', strtotime($data['tgl_sewa_start']));
            $data['tgl_sewa_end']   = date('Y-m-d', strtotime($data['tgl_sewa_end']));
        }
        $smodule  = 'front end toss';
        $activity = 'Change TOSS Data';
        $this->db->trans_begin();

        if (!empty($this->input->post('extra'))) {
            $tbl_name = 'tbl_toss_front_end_cs_manpower';
            $action   = 'insert toss manpower data';
            if ($extra == 'MANPOWER') {
                $data['id_toss_database'] = $id;
                $data['key']              = md5(time());
                $data['tgl_masuk']        = date('Y-m-d', strtotime($data['tgl_masuk']));
                $log_new_value_data       = [];
                foreach ($data as $key => $value) {
                    $log_new_value_data[] = $value;
                }
                $insert  = $this->db->insert('tbl_toss_front_end_cs_manpower', $data);
                $dberror = $this->db->error();
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    $additional_information = '';
                    if ($dberror['code'] != '0') {
                        $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    }
                    $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Toss', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array('status' => 0, 'result' => 'error'));
                } else {
                    $this->db->trans_commit();
                    $addtional_information = '';
                    $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Toss', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array('status' => 1, 'result' => 'success'));

                }
            }
        } else {
            $tbl_name = 'tbl_toss_front_end';
            $action   = 'updpate toss data';
            $old      = $this->db->where('id', $id)->get('tbl_toss_front_end')->row_array();

            $log_old_value_data = [];
            $log_new_value_data = [];
            foreach ($data as $key => $value) {
                $log_old_value_data[] = isset($old[$key]) ? $old[$key] : '';
                $log_new_value_data[] = $value;
            }
            $update = $this->db->where('id', $id)->update('tbl_toss_front_end', $data);

            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $additional_information = '';
                if ($dberror['code'] != '0') {
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                }
                $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Toss', 'Error', $addtional_information, $tbl_name, 'Update', '', $log_new_value_data);
                echo json_encode(array('status' => 0, 'result' => 'error'));
            } else {
                $this->db->trans_commit();
                $addtional_information = '';
                $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Toss', 'Success', $addtional_information, $tbl_name, 'Update', $log_old_value_data, $log_new_value_data);
                echo json_encode(array('status' => 1, 'result' => 'success'));

            }
        }
    }

    public function delete_manpower()
    {
        $id = xss_clean($this->input->post('id'));
        $this->db->trans_begin();

        $update = $this->db->where('id', $id)->update('tbl_toss_front_end_cs_manpower', array('is_delete' => 1));

        $smodule            = 'front end toss';
        $activity           = 'Change Toss Data';
        $tbl_name           = 'tbl_toss_front_end_cs_manpower';
        $action             = 'delete toss manpower';
        $dberror            = $this->db->error();
        $log_old_value_data = [$id, 0];
        $log_new_value_data = [$id, 1];
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $additional_information = '';
            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }
            $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Toss', 'Error', $addtional_information, $tbl_name, 'Delete', '', $log_new_value_data);
            echo json_encode(array('status' => 0, 'result' => 'error'));
        } else {
            $this->db->trans_commit();
            $addtional_information = '';
            $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Toss', 'Success', $addtional_information, $tbl_name, 'Delete', $log_old_value_data, $log_new_value_data);
            echo json_encode(array('status' => 1, 'result' => 'success'));
        }
    }

    public function final_submit()
    {

        $id       = xss_clean($this->input->post('id'));
        $nama     = xss_clean($this->input->post('nama'));
        $jabatan  = xss_clean($this->input->post('jabatan'));
        $img_sign = $this->input->post('image');
        $this->db->trans_begin();
        $smodule  = 'front end toss';
        $activity = 'Change Toss Data';
        $tbl_name = 'tbl_toss_front_end';
        $action   = 'update toss data';
        $data     = array(
            'hand_sign_dealer_nama'     => $nama,
            'hand_sign_dealer_jabatan'  => $jabatan,
            'hand_sign_dealer_image_64' => $img_sign,
            'final_submit_date'         => date('Y-m-d'),
        );
        $old                = $this->db->where('id', $id)->get('tbl_toss_front_end')->row_array();
        $update             = $this->db->where('id', $id)->update('tbl_toss_front_end', $data);
        $log_new_value_data = [];
        $log_old_value_data = [];
        foreach ($data as $key => $value) {
            $log_old_value_data[] = (isset($old[$key]) ? $old[$key] : '');
            $log_new_value_data[] = $value;
        }

        //$db_col   = $this->db->query('SHOW columns from tbl_toss_database')->result();
        $db_field = $this->db->list_fields('tbl_toss_database');

        //$db_front = $this->db->query('select * from tbl_toss_front_end where is_delete=0 and id=' . $id)->result();
        $db_front = $this->db
            ->where('is_delete', 0)
            ->where('id', $id)
            ->get('tbl_toss_front_end')
            ->row_array();

        if (!empty($db_front)) {
            $update_data = [];
            foreach ($db_front as $key => $value) {
                if (in_array($key, $db_field)) {
                    $update_data[$key] = $value;
                }
            }
            $update_data['pernah_update'] = 1;
            unset($update_data['id']);
            unset($update_data['outlet_photo_file_name_64']);

            $update = $this->db->where('id', $db_front['id_toss_database'])->update('tbl_toss_database', $update_data);
        }

        $db_field = $this->db->list_fields('tbl_toss_database_cs_manpower');

        $getManpower       = $this->db->where('id_toss_database', $db_front['id_toss_database'])->update('tbl_toss_database_cs_manpower', array('is_delete' => 1));
        $db_manpower_front = $this->db
            ->select("t.*")
            ->from('tbl_toss_front_end_cs_manpower t')
            ->where('t.is_delete', 0)
            ->where('t.id_toss_database', $id)
            ->get()
            ->result_array();

        if (!empty($db_manpower_front)) {
            $insert_data = [];
            foreach ($db_manpower_front as $row) {
                $new_data = array();
                foreach ($row as $key => $value) {
                    if (in_array($key, $db_field)) {
                        $new_data[$key] = $value;
                    }
                }
                unset($new_data['id']);
                $new_data['id_toss_database'] = $db_front['id_toss_database'];
                $insert_data[]                = $new_data;

            }
            $update = $this->db->insert_batch('tbl_toss_database_cs_manpower', $insert_data);
        }

        $duplicateKeArea      = $this->model_toss->updateAreaToFE($id);
        $duplicateKeEquipment = $this->model_toss->updateEQToFE($id);
        $dberror              = $this->db->error();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $additional_information = '';
            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }
            $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Toss', 'Error', $addtional_information, $tbl_name, 'Delete', '', $log_new_value_data);
            echo json_encode(array('status' => 0, 'result' => 'error'));
        } else {
            $this->db->trans_commit();
            $addtional_information = '';
            $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Toss', 'Success', $addtional_information, $tbl_name, 'Delete', $log_old_value_data, $log_new_value_data);
            echo json_encode(array('status' => 1, 'result' => 'success'));
        }
    }

    public function catatan_manpower()
    {
        $id      = xss_clean($this->input->post('id'));
        $data    = $this->input->post('data');
        $catatan = xss_clean($this->input->post('catatan'));
        $update  = $this->db->where('id', $id)->update('tbl_toss_front_end', array('jumlah_manpower_catatan' => $catatan));

        if ($update) {
            echo json_encode(array('status' => 1, 'result' => 'success'));
        } else {
            echo json_encode(array('status' => 0, 'result' => 'error'));
        }
    }

    public function submit_photo_eq()
    {
        $this->db->trans_begin();

        $smodule            = 'front end toss';
        $activity           = 'Change Toss Data';
        $action             = 'insert toss photo data';
        $log_new_value_data = [];
        $data               = $this->input->post('data');
        if (!empty($this->input->post('delete'))) {
            foreach ($this->input->post('delete') as $key => $value) {
                $this->db->where('id', $value)->update('tbl_toss_front_end_cs_equipment_foto_dealer', array('is_delete' => 1));
            }
        }

        if (!empty($this->input->post('data'))) {
            $no_key = 1;
            foreach ($data as $key => $value) {
                $data[$key]['img_key'] = md5(time() . $no_key);
                $no_key++;
            }

            foreach ($data as $row) {
                $value_data = [];
                foreach ($row as $key => $value) {
                    $value_data[] = $value;
                }
                $log_new_value_data[] = $value_data;
            }
            $tbl_name = 'tbl_toss_front_end_cs_equipment_foto_dealer';

            $insert = $this->db->insert_batch('tbl_toss_front_end_cs_equipment_foto_dealer', $data);

        }
        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $additional_information = '';
            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }
            $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Toss', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
            echo json_encode(array('status' => 1, 'result' => 'error'));

        } else {
            $this->db->trans_commit();
            $addtional_information = '';
            foreach ($log_new_value_data as $row) {

                $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Toss', 'Success', $addtional_information, $tbl_name, 'Insert', '', $row);
            }
            echo json_encode(array('status' => 1, 'result' => 'success'));

        }
    }

    public function submit_photo_area()
    {
        // print_r($this->input->post('data'));
        $data = $this->input->post('data');
        if (!empty($this->input->post('data'))) {
            foreach ($data as $key => $value) {
                $update = $this->db->where('id', $value['id'])->update('tbl_toss_front_end_cs_area', array('foto_dealer_file_name_64' => $value['foto']));
            }
        }

        if (!empty($this->input->post('keterangan'))) {
            foreach ($this->input->post('keterangan') as $key => $value) {
                $update = $this->db->where('id', $value['id'])->update('tbl_toss_front_end_cs_area', array('keterangan' => $value['keterangan']));
            }
        }

        echo json_encode(array('status' => 1, 'result' => 'success'));
    }

    public function SUBMIT_EVALUATION()
    {
        $id    = xss_clean($this->input->post('id'));
        $data  = $this->input->post('data');
        $table = xss_clean($this->input->post('table'));

        foreach ($data as $key => $value) {
            $this->db->where('id', $value['id'])->update($table, array('score_evaluasi_dealer' => $value['check']));

            if ($table == 'tbl_toss_front_end_cs_manpower') {
                $key    = $this->db->where('id', $value['id'])->select('key')->get($table)->row();
                $update = $this->db->where('key', $key->key)->update('tbl_toss_database_cs_manpower', array('score_evaluasi_dealer' => $value['check']));

                if ($value['check'] == 0) {
                    $upd_tam = $this->db->where('key', $key->key)->update('tbl_toss_database_cs_manpower', array('score_evaluasi_tam' => $value['check']));
                }

            }

            if ($table == 'tbl_toss_front_end_cs_area') {
                $key    = $this->db->where('id', $value['id'])->select('img_key')->get($table)->row();
                $update = $this->db->where('img_key', $key->img_key)->update('tbl_toss_database_cs_area', array('score_evaluasi_dealer' => $value['check']));

                if ($value['check'] == 0) {
                    $upd_tam = $this->db->where('img_key', $key->img_key)->update('tbl_toss_database_cs_area', array('score_evaluasi_tam' => $value['check']));
                }
            }

            if ($table == 'tbl_toss_front_end_cs_equipment') {
                $key    = $this->db->where('id', $value['id'])->select('item_key')->get($table)->row();
                $update = $this->db->where('item_key', $key->item_key)->update('tbl_toss_database_cs_equipment', array('score_evaluasi_dealer' => $value['check']));

                if ($value['check'] == 0) {
                    $upd_tam = $this->db->where('item_key', $key->item_key)->update('tbl_toss_database_cs_equipment', array('score_evaluasi_tam' => $value['check']));
                }
            }
        }

        // echo json_encode(array('status'=>1,'result'=>'success'));
        if ($update) {
            echo json_encode(array('status' => 1, 'result' => 'success'));
        } else {
            echo json_encode(array('status' => 0, 'result' => 'error'));
        }

    }

}
