<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Project_sdm_area_sales extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MProject");
    }

    public function index()
    {
        echo 'page not found';
    }

    private function check_its_mine($id_project)
    {
        $id_user = $this->session->userdata('id');
        $qry     = $this->db->where('id', $id_project)
            ->group_start()
            ->where('id_user_level_1', $id_user)
            ->or_where('id_user_level_2', $id_user)
            ->or_where('id_user_level_3', $id_user)
            ->group_end()
            ->get('tbl_project');
        if ($qry->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function set_page($mode = '', $id_project = null)
    {

        if (!empty($id_project) and $this->check_its_mine($id_project)) {
            $data['id']           = $id_project;
            $data['mode']         = $mode;
            $data_project         = $this->MProject->get_data_project($id_project)->row();
            $data['data_project'] = $data_project;
            $data['sdm_sales']    = $this->db->where('id_project', $id_project)->get('tbl_project_sdm_sales')->result_array();
            $data['content']      = 'view_project_sdm_area_sales';
            $data['tpl_js']       = 'project_sdm_area_sales';
            $this->load->view('tpl_index', $data);
        } else {
            redirect(base_url('errors/code/404'));
        }
    }
    public function save($id_project)
    {
        $data               = xss_clean($this->input->post('input'));
        $bulantahun         = xss_clean($this->input->post('bulantahun'));
        $data['id_project'] = $id_project;
        foreach ($bulantahun as $name => $val) {
            if (isset($val['bulan']) and isset($val['tahun'])) {
                $data[$name] = $val['bulan'] . ' ' . $val['tahun'];
            }
        }
        $this->db->trans_begin();
        //$qry = $this->db->where('id_project', $id_project)->get('tbl_project_sdm_sales');
        $this->db->insert('tbl_project_sdm_sales', $data);
        $smodule            = 'front end project';
        $activity           = 'Change Project Data';
        $tbl_name           = 'tbl_project_sdm_sales';
        $action             = 'insert project sdm area sales';
        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {
            if ($dberror['code'] != '0') {
                // catch error DB

                $this->db->trans_rollback();

                //insert insert log
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

            } else {

                $this->db->trans_rollback();

                //insert insert log
                $addtional_information = '';
                $this->MLogging->insert_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

            }
        } else {
            $this->db->trans_commit();

            //insert insert log
            $addtional_information = '';
            $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);

        }
        redirect('project_sdm_area_sales/set_page/view_detail_group/' . $id_project);
    }

    public function delete($id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id)->delete('tbl_project_sdm_sales');
        $dberror = $this->db->error();

        $smodule    = 'front end project';
        $activity   = 'Change Project Data';
        $table_name = 'tbl_project_sdm_sales';
        $action     = 'delete project sdm area sales';
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $log_new_value_data    = array();
            $addtional_information = 'Hard Delete. Error';
            if ($dberror['code'] != '0') {
                $addtional_information = 'Hard Delete. Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
            }

            $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', '', $table_name, 'Update', '', $log_new_value_data);

        } else {

            $log_old_value_data = array(
                $id,
            );

            $log_new_value_data = array(
                $id,
            );

            $addtional_information = 'Hard Delete';

            $this->db->trans_commit();

            $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $table_name, 'Delete', $log_old_value_data, $log_new_value_data);
        }
    }
}
