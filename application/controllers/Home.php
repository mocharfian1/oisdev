<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model("MHome");
    }

    public function index()
    {
        $data['user_session'] = $this->session->userdata;
        $id_user              = $this->session->userdata('id');
        $data['data_project'] = $this->MHome->data_project($id_user)->result_array();
        $data['menu']         = 'home';
        $data['sub_menu']     = '';
        $data['content']      = 'view_home';
        $data['tpl_js']       = 'home';
        $data['plugin_mode']  = '1';
        $this->load->view('tpl_index', $data);
    }
}
