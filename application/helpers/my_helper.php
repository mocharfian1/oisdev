<?php
function def_imagesize($file)
{
    if (!file_exists($file)) {
        $file = 'adminx/upload/kpd/default.jpg';
    }
    return getimagesize($file);
}


function number_format_dots($number) {
    return number_format($number,0,',','.');
}
function compare_password($password, $true_password)
{
    if (is_valid_sha1($true_password)) {
        if (sha1($password) == $true_password) {
            return true;
        }
    } else {
        if ($password == $true_password) {
            return true;
        }
    }
    return false;
}
function is_valid_sha1($str)
{
    return (bool) preg_match('/^[0-9a-f]{40}$/i', $str);
}