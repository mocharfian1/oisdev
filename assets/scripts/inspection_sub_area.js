$('#add_layout[type=file]').change(function () {
      $fileCount = this.files.length;
      $('#count').text($fileCount + " " + 'Selected');
  });

  function camera_upload(){
    
    
  }


  var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
      },
    });

  var swiper = new Swiper('.swiper-container2', {
    });

  $(document).ready(function(){
      $("#formPhotoArea").submit(function(e) {
          e.preventDefault(); 
          console.log($(this));
          var formData = new FormData($(this)[0]);
          var id = $('#id').val();

           $.ajax({
               url:base_url+'inspection_sub_area/file_camera_upload/'+id, //URL submit
               type:"post", //method Submit
               
               datatype : "JSON",
                data: formData,
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){

                // console.log(data);

                var res = JSON.parse(data);

                if(res.status == 'success'){
                  $('#photoAreaMessage').html('(Photo submitted)');
                  $('#photoAreaMessage').fadeIn();

                  setTimeout(function () {
                      $('#photoAreaMessage').fadeOut();
                  }, 5000);
                }

                if(res.status == 'error'){
                  $('#photoAreaMessage').html('(Error : '+data.message+')');
                  $('#photoAreaMessage').fadeIn();

                  setTimeout(function () {
                      $('#photoAreaMessage').fadeOut();
                  }, 5000);
                }

             }
           });
      });


      $("#formPhotoLayout").submit(function(e) {
          e.preventDefault(); 
          console.log($(this));
          var formData = new FormData($(this)[0]);
          var id = $('#id').val();

           $.ajax({
               url:base_url+'inspection_sub_area/file_gallery_upload/'+id, //URL submit
               type:"post", //method Submit
               
               datatype : "JSON",
                data: formData,
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){

                // console.log(data);

                var res = JSON.parse(data);

                if(res.status == 'success'){
                  $('#photoLayoutMessage').html('(Photo submitted)');
                  $('#photoLayoutMessage').fadeIn();

                  setTimeout(function () {
                      $('#photoLayoutMessage').fadeOut();
                  }, 5000);
                }

                if(res.status == 'error'){
                  $('#photoLayoutMessage').html('(Error : '+res.message+')');
                  $('#photoLayoutMessage').fadeIn();

                  setTimeout(function () {
                      $('#photoLayoutMessage').fadeOut();
                  }, 5000);
                }

             }
           });
      });
      
      $('.jarak-input').on('input', function() {
        match = (/(\d{0,8})[^.]*((?:\.\d{0,1})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
        this.value = match[1] + match[2];
      });
  });

function uploadConfigPic(formId) {
  var formData = new FormData($('#formPhotoConfig_'+formId)[0]);
   $.ajax({
       url:base_url+'inspection_sub_area/file_config_upload', //URL submit
       type:"post", //method Submit
       
       datatype : "JSON",
        data: formData,
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data){

        // console.log(data);

        var res = JSON.parse(data);

        if(res.status == 'success'){
          $('#formPhotoConfig_'+formId+'Message').html('(Photo submitted)');
          $('#img_href_'+formId).attr('href',res.file_path);
          $('#img_src_'+formId).attr('src',res.file_path);
          $('#formPhotoConfig_'+formId+'Message').fadeIn();

          setTimeout(function () {
              $('#'+formId+'Message').fadeOut();
          }, 5000);
        }

        if(res.status == 'error'){
          $('#formPhotoConfig_'+formId+'Message').html('(Error : '+res.message+')');
          $('#formPhotoConfig_'+formId+'Message').fadeIn();

          setTimeout(function () {
              $('#formPhotoConfig_'+formId+'Message').fadeOut();
          }, 5000);
        }

     }
   });
}
function delete_image(id) {
   if(confirm("Hapus Gambar ini ?")){
     $.ajax({
         url:base_url+'inspection_sub_area/delete_image/'+id, //URL submit
         type:"get", //method Submit
          datatype : "JSON",
          
          success: function(data){
            $("#img_"+id).remove();
          }
     });
   }
}