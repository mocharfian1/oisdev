

jQuery(document).ready(function() {
   
});

function uploadImage(formId,type){
	var formData = new FormData($("#"+formId)[0]);
	var form = $("#"+formId);
	var id = $('#id').val();
	$.ajax({
	   url:base_url+'area/file_gallery_upload/'+id+'/'+type, //URL submit
	   type:"post", //method Submit
	   
	   datatype : "JSON",
	    data: formData,
	    // async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function(data){

	    // console.log(data);

	    var res = JSON.parse(data);

	    if(res.status == 'success'){
	      form.find('#msg').html('(Photo submitted)');
	      form.find('#msg').fadeIn();
	          window.location.reload();
	    }

	    if(res.status == 'error'){
	      form.find('#msg').html('(Error : '+res.message+')');
	      form.find('#msg').fadeIn();

	      setTimeout(function () {
	          form.find('#msg').fadeOut();
	      }, 5000);
	    }

	 }
	});
}
function uploadFile(formId,type){
	var formData = new FormData($("#"+formId)[0]);
	var form = $("#"+formId);
	var id = $('#id').val();

	$.ajax({
	   url:base_url+'area/file_upload/'+id+'/'+type, //URL submit
	   type:"post", //method Submit
	   
	   datatype : "JSON",
	    data: formData,
	    // async: false,
	    cache: false,
	    contentType: false,
	    processData: false,
	    success: function(data){

	    // console.log(data);

	    var res = JSON.parse(data);

	    if(res.status == 'success'){
	      form.find('#msg').html('(Document submitted)');
	      form.find('#msg').fadeIn();
	          window.location.reload();
	    }

	    if(res.status == 'error'){
	      form.find('#msg').html('(Error : '+res.message+')');
	      form.find('#msg').fadeIn();

	      setTimeout(function () {
	          form.find('#msg').fadeOut();
	      }, 5000);
	    }

	 }
	});
}

function delete_image(id) {

   if(confirm("Hapus Gambar ini ?")){
	   $.ajax({
	       url:base_url+'area/delete_image/'+id, //URL submit
	       type:"get", //method Submit
	        datatype : "JSON",
	        
	        success: function(data){
	          $("#img_"+id).remove();
	        }
	   });
	}
}