// $('#gallery-<?php echo $value->id; ?>').click();
// alert('');
var isCam = 0;

function setPhoto(id,cam=null){
    // $('#gallery-'+id).click();
    if(cam==1){
        $('#gallery-'+id).attr({
            "capture":"capture"
        });
        isCam = 1;
    }else{
        $('#gallery-'+id).removeAttr('capture');
        isCam = 0;
    }

    setTimeout(()=>{
        $('#gallery-'+id).click();
    },200);
}
try{
    var canvas = document.querySelector("canvas");
    var signaturePad = new SignaturePad(canvas);

    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var img = document.getElementById("IMAGE_CANVAS");
    ctx.drawImage(img, 0, 0, img.width,    img.height,     // source rectangle
                   0, 0, canvas.width, canvas.height);
}catch(e){
    console.log('Signature Down');
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

$('input[type=text].frm_.sum').on('keydown',function(e){
    var a = [9,49,50,51,52,53,54,55,56,57,48,8,13,37,39,38,40];
    // console.log(e.which);
    if(a.indexOf(e.which)<0){
        return false;
    }

    var self = this;
    setTimeout(function(){
        if($(self).val()==''){
            $(self).val(0);
        }else{
            var v = parseInt($(self).val().replace(/\./g, ""));
            var c = addCommas(v);
            $(self).val(c);
        }
    },10);    
});

function addManpowerJabatan(){

	$('#manpower_level_training_row').fadeOut();
	$('#manpower_no_ktp_row').fadeOut();
	$('#manpower_tanggal_masuk_row').fadeOut();
	$('#manpower_keterangan_row').fadeOut();

	$('#manpower_level_training_row').val('');
	$('#manpower_no_ktp_row').val('');
	$('#manpower_tanggal_masuk_row').val('');
	$('#manpower_keterangan_row').val('');

	// setTimeout(showDetailManpowerJabatan, 500);

	showDetailManpowerJabatan();
}


function resetFormManPower(){
    $('#manpower_level_training').val('');
    $('#manpower_no_ktp').val('');
    $('#manpower_tanggal_masuk').val('');
    $('#manpower_keterangan').val('');
}

function showDetailManpowerJabatan(){
    resetFormManPower();
	if($('#manpower_jabatan').val() == 'tech'){
		$('#manpower_no_ktp_row').fadeIn('slow');
		$('#manpower_tanggal_masuk_row').fadeIn('slow');
		$('#manpower_keterangan_row').fadeIn('slow');
	}

	if($('#manpower_jabatan').val() == 'admin' || $('#manpower_jabatan').val() == 'others'){
		$('#manpower_tanggal_masuk_row').fadeIn('slow');
		$('#manpower_keterangan_row').fadeIn('slow');
	}

	if($('#manpower_jabatan').val() == 'pro_tech'){
		$('#manpower_level_training_row').fadeIn('slow');
		$('#manpower_no_ktp_row').fadeIn('slow');
		$('#manpower_tanggal_masuk_row').fadeIn('slow');
		$('#manpower_keterangan_row').fadeIn('slow');
	}

    
}

var DatatablesResponsive = function () {

    var initTable1 = function () {
        var table = $('#man_power_list');

        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "paging":   false,
            "info":     false,
            searching: false,

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: {
                details: {
                   
                }
            },

            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });
    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();



jQuery(document).ready(function() {
    $('.datedropper').dateDropper();
    DatatablesResponsive.init();
});

function startLoading(x,all=null){
    if(all==1){
        $('#btnSave').attr('disabled','disabled');
        $('#btnSave').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnCancel').attr('disabled','disabled');
        $('#btnCancel').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnDelete').attr('disabled','disabled');
        $('#btnDelete').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }else{
        x.attr('disabled','disabled');
        x.append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }
}

function endLoading(x=null,all=null){
    if(all==1){
        $('#btnSave').removeAttr('disabled');
        $('#btnSave').find('span').remove();

        $('#btnCancel').removeAttr('disabled');
        $('#btnCancel').find('span').remove();

        $('#btnDelete').removeAttr('disabled');
        $('#btnDelete').find('span').remove();
    }else{
        x.removeAttr('disabled');
        x.find('span').remove();
      
    }
    // x.append('&nbsp;&nbsp;<span class="fa fa-circle-o-notch fa-spin"></span>');
}

function error_msg(x,message=null){
    if(message == null){
        x.parent().prepend(`<center class="error_msg"><label style="color: red;">Error mengubah data. Silahkan coba lagi.</label></center>`);
    }else{
        x.parent().prepend(`<center class="error_msg"><label style="color: red;">`+message+`</label></center>`);
    }
}


function SUBMIT(id,el,ex=null){
    $('.error_msg').remove();
    startLoading(el);
    var elm = el.closest('.flagTOSS');
    var elmInput = elm.find('input.frm_');
    var elmSelect = elm.find('select.frm_');
    var elmOther = elm.find('.input.frm_');

    var vSend = new Object();

    elmInput.toArray().forEach(function(item,index){
        var key = elmInput.eq(index).attr('name');
        var value = elmInput.eq(index).val();

        if(elmInput.hasClass('sum')){
            vSend[key] = parseInt(value.replace(/\./g, ""));
        }else{
            vSend[key] = value;
        }
    });

    elmSelect.toArray().forEach(function(item,index){
        var key = elmSelect.eq(index).attr('name');
        var value = elmSelect.eq(index).val();

        vSend[key] = value;
    });

    elmOther.toArray().forEach(function(item,index){
        var key = elmOther.eq(index).attr('name');
        var value = elmOther.eq(index).html();

        if(elmInput.hasClass('sum')||elmInput.hasClass('num_sum')){
            vSend[key] = parseInt(value.replace(/\./g, ""));
        }else{
            vSend[key] = value;
        }
    });

    console.log(vSend);

    // return false;
    var ALL_DATA = {id:id,data:vSend,extra:ex};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/SUBMIT_set_profile',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            // $('#modal_success').modal('show');
            endLoading(el);
            location.reload();
        }else{
            endLoading(el);
            error_msg(el);
            
            // $('#modal_gagal').modal('show');
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });
}

function delete_manpower(id){
    var ALL_DATA = {id:id};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/delete_manpower',ALL_DATA).done(function(data){
        // var res = JSON.parse(data);
        // if(res.status==1){
        //     alert('Sukses menghapus data manpower.');
            location.reload();
        // }else{
        //     alert('Gagal menghapus data manpower.');
        // }
    }).fail(function(e){

    });
}



$(function(){
    $('#modal_success').on('hidden.bs.modal', function () {
        location.reload();
    });

  $('input[name="target_ue_bulan_year"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('td[name="target_ue_hari_year"]').html(isNaN(angka.toFixed(0))?0:angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_1"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('td[name="target_ue_hari_year_plus_1"]').html(angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_2"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('td[name="target_ue_hari_year_plus_2"]').html(angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_3"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('td[name="target_ue_hari_year_plus_3"]').html(angka.toFixed(0));
    },10);
  });


  $('input[name="investasi_pembelian_lahan"],input[name="investasi_sewa_bangunan"],input[name="investasi_renovasi_bangunan"],input[name="investasi_corporate_identity"],input[name="investasi_equipment"],input[name="investasi_total"]').on('keydown',function(e){
    
    setTimeout(function(){
        var arrSUM = $('#tb_investasi').find('input.sum');
        var SUM_TOTAL = 0;
        arrSUM.toArray().forEach(function(item,index){
            var v = parseInt(arrSUM.eq(index).val().replace(/\./g, ""));

            SUM_TOTAL += v;
        });
        
        $('td[name="investasi_total"]').val(SUM_TOTAL);
        $('#investasi_total').html(addCommas(SUM_TOTAL));
    },10);
  });
});


function final_submit(id,elm=null){
        var el = $('#sign_final_submit');
        if( signaturePad.isEmpty() || el.find('input[name="hand_sign_dealer_nama"]').val()=='' || el.find('input[name="hand_sign_dealer_jabatan"]').val() == ''){
            error_msg(elm, 'Please complete all fields!');
        }else{
            $('.error_msg').remove();
            startLoading(elm);

            var el = $('#sign_final_submit');
            var nama = el.find('input[name="hand_sign_dealer_nama"]').val();
            var jabatan = el.find('input[name="hand_sign_dealer_jabatan"]').val();
            var image = signaturePad.toDataURL();
            var ALL_DATA = {id:id,nama:nama,jabatan:jabatan,image:image}
            ALL_DATA[csrf.name]=csrf.token;
            $.post(URL+'toss/final_submit',ALL_DATA).done(function(data){
                console.log(data);
                var res = JSON.parse(data);
                console.log(res);
                if(res.status==1){
                    endLoading(elm);
                }else{
                    endLoading(elm);
                    error_msg(elm);
                }
            }).fail(function(e){
                endLoading(elm);
                error_msg(elm);
            });
        }

   

}

function submit_catatan_manpower(id,el=null){
    $('.error_msg').remove();
    startLoading(el);
    var ALL_DATA ={id:id,catatan:$('#catatan_manpower').val()};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/catatan_manpower',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            endLoading(el);
        }else{
            endLoading(el);
            error_msg(el);
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });

}


var imgTmp;
var valSizeImg = 1;
var noImage = 1;

function sh_thumb(input,el) {
    if(noImage==1){
        el.attr('src', URL+'upload/default.jpg');
        el.attr('onclick','imagePreviewDialog("'+URL+'upload/default.jpg'+'")');
    }else{
        if (input.files && input.files[0]) {
                var reader = new FileReader();

                
                reader.onload = function (e) {
                      el.attr('src', e.target.result);
                      el.attr('onclick','imagePreviewDialog("'+e.target.result+'")');
                      noImage = 1;
                      imgTmp = null;
                }
                
                reader.readAsDataURL(input.files[0]);
        }
        //return input.files[0];
    }
}

var img_equipment = function(img64,height,width,id,group_key,item_key){
    
    return `<div class="_root">
                <div class="swiper-container ">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                          <div class="my-gallery">
                                <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">

                                  <a href="`+img64+`" itemprop="contentUrl" data-size="`+width+`x`+height+`">
                                      <img id_item="`+id+`" style="height: 190px" src="`+img64+`" group_key=`+group_key+` item_key="`+item_key+`" class="img-thumbnail img_submit" itemprop="thumbnail" alt="Image description" />
                                  </a>

                                 </figure>
                          </div>
                        </div>
                    </div>                     
                    <div class="swiper-pagination"></div>
                </div>
                <br>
                <a href="javascript:;" class="btn btn-default btn-lg btn-danger" onclick="delITEQ($(this))">Delete Photo</a>
                <br><br>
            </div>`;
}   

var area_items = function(img64,height,width,id){
    return `<a href="`+img64+`" itemprop="contentUrl" data-size="`+width+`x`+height+`">
                <img id_item=`+id+` style="height: 90px" src="`+img64+`" class="img-thumbnail img_submit" itemprop="thumbnail" alt="Image description" />
            </a>`;
};             

function ch_imgTmp(x,id=null,group_key=null,item_key=null){
    valSizeImg = 1;
    noImage = 1;

    if(x.files.length==0){

        noImage = 1;
    }else{
        noImage = 0;
        if(x.files[0].size<=2000000){
            imgTmp = x;

            var reader = new FileReader();     

            reader.onload = function (e) {
                var img = new Image();
                img.onload = function () {
                    var area = $(x).parent().find('#foto_item-'+id);
                    if(area.length>0){
                        console.log("JUMLAH AREA = " + area.length);
                        area.html(area_items(e.target.result,this.height,this.width,id));
                    }

                    var equipment = $(x).parent().find('.eqPhoto');
                    if(equipment.length>0){
                        console.log("JUMLAH EQUIPMENT = " + equipment.length);
                        equipment.append(img_equipment(e.target.result,this.height,this.width,id,group_key,item_key));
                        $('#'+x.id).val('');
                    }
                        
                    initPhotoSwipeFromDOM('.my-gallery');
                };
                img.src = e.target.result;
            }
            
            reader.readAsDataURL(x.files[0]);

        }else{
            if(isCam==1){
                imgTmp = x;

                var reader = new FileReader();     
                // alert("1");
                reader.onload = function (e) {
                    var img = new Image();
                    img.onload = function () {
                        // alert("2");
                        var area = $(x).parent().find('#foto_item-'+id);
                        if(area.length>0){
                            console.log("JUMLAH AREA = " + area.length);
                            area.html(area_items(e.target.result,this.height,this.width,id));
                        }

                        var equipment = $(x).parent().find('.eqPhoto');
                        if(equipment.length>0){
                            console.log("JUMLAH EQUIPMENT = " + equipment.length);
                            equipment.append(img_equipment(e.target.result,this.height,this.width,id,group_key,item_key));
                            $('#'+x.id).val('');
                        }
                            
                        initPhotoSwipeFromDOM('.my-gallery');
                    };
                    img.src = e.target.result;
                }
                
                reader.readAsDataURL(x.files[0]);
            }else{
                  valSizeImg = 0;
                  alert('Ukuran gambar melebihi 2MB');
                  $(x).replaceWith($(x).val('').clone(true));
                  return false;
            }
        }
    }
}

var del_EQ_arr = [];
function delITEQ(el,id=null){
    // var r = confirm('Anda yakin ingin menghapus foto ini?');
    // if(r){
        if(id==null){
            el.closest('._root').remove();
        }else{
            del_EQ_arr.push(id);
            el.closest('._root').remove();
        }
    // }
}

function SUBMIT_foto_eq(el){
    $('.error_msg').remove();
    startLoading(el);
    var data = [];
    var photo = $('.img_submit');
    // var keterangan = $('.img_submit').closest('.item__').find('textarea');
    photo.toArray().forEach(function(item,index){
        data.push({
            id_toss_database_cs_equipment:$('.img_submit').eq(index).attr('id_item'),
            foto_dealer_file_name_64:$('.img_submit').eq(index).attr('src'),
            group_key:$('.img_submit').eq(index).attr('group_key'),
            item_key:$('.img_submit').eq(index).attr('item_key')
        });
    });

    console.log(data);
    var ALL_DATA ={data:data,delete:del_EQ_arr};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/submit_photo_eq',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            // $('#modal_success').modal('show');
            endLoading(el);
            location.reload();
        }else{
            endLoading(el);
            error_msg(el);
            
            // $('#modal_gagal').modal('show');
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });
}

function SUBMIT_foto_area(el){
    $('.error_msg').remove();
    startLoading(el);
    var data = [];
    var keterangan = [];
    var photo = $('.img_submit');
    var el_ket = $('textarea');
        
    photo.toArray().forEach(function(item,index){
        data.push({
            id:$('.img_submit').eq(index).attr('id_item'),
            foto:$('.img_submit').eq(index).attr('src'),
        });
    });

    el_ket.toArray().forEach(function(item,index){
        keterangan.push({
            id:el_ket.eq(index).attr('data_id'),
            keterangan:el_ket.eq(index).val()
        });
    });


    console.log(data);
    var ALL_DATA ={data:data,keterangan:keterangan};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/submit_photo_area',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            // $('#modal_success').modal('show');
            endLoading(el);
            location.reload();
        }else{
            endLoading(el);
            error_msg(el);
            
            // $('#modal_gagal').modal('show');
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });
}

function SUBMIT_EVALUATE(id_toss,el,table){
    $('.error_msg').remove();
    startLoading(el);

    var data = [];
    var element = $('.evaluasi');
    element.toArray().forEach(function(item,index){
        data.push({
            id:$('.evaluasi').eq(index).val(),
            check:$('.evaluasi').eq(index).is(':checked')==true?1:0
        });
    });
    var ALL_DATA ={id:id_toss,data:data,table:table};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/SUBMIT_EVALUATION',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            endLoading(el);
            location.reload();
        }else{
            endLoading(el);
            error_msg(el);
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });

}


