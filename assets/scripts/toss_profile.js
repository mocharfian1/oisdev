//Start GMAP


function initMap(latitude,longitude) {

    var marker = '';
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latitude, lng: longitude},
      zoom: 8,
      mapTypeId: 'roadmap',
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      // disableDoubleClickZoom: true,

      gestureHandling: 'greedy',
      // fullscreenControl: false,
      fullscreenControlOptions : {
        position: google.maps.ControlPosition.RIGHT_BOTTOM
      },
    });

    marker = new google.maps.Marker({
      map: map,
      draggable: true,
      position: {lat: latitude, lng: longitude}
    });

    // set lat lng to input text
    // document.getElementById('latitude').value = '-4.402969';
    // document.getElementById('longitude').value = '122.380632';

    //get client location and set map to view

    // navigator.geolocation.watchPosition(function(position) {
    //   console.log("i'm tracking you!");
    // },
    // function (error) { 
    //   if (error.code == error.PERMISSION_DENIED)
    //       console.log("you denied me :-(");
    // });

    // if (navigator.geolocation) {
    //     // alert('');
    //      navigator.geolocation.getCurrentPosition(function (position) {
    //          initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //          map.setCenter(initialLocation);
             
    //      });
    // }

    // if (navigator.geolocation) {
    if(document.getElementById('latitude').value=='' || document.getElementById('longitude').value==''){
        initialLocation = new google.maps.LatLng(-0.789275, 113.921327);
               map.setCenter(initialLocation);
               map.setZoom(4);
               marker = new google.maps.Marker({
                  map: map,
                  draggable: true,
                  position: {lat: -0.789275, lng: 113.921327}
                });

              var lt = -0.789275;
              var lg = 113.921327;
              document.getElementById('latitude').value = lt.toFixed(6);
              document.getElementById('longitude').value = lg.toFixed(6);
    }
    
    // navigator.permissions.query({
    //         name: 'geolocation'
    //   }).then(function(result) {
    //     if (result.state == 'granted') {
    //       navigator.geolocation.getCurrentPosition(function (position) {
    //            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //            map.setCenter(initialLocation);

    //            marker = new google.maps.Marker({
    //               map: map,
    //               draggable: true,
    //               position: {lat: position.coords.latitude, lng: position.coords.longitude}
    //             });

    //           document.getElementById('latitude').value = position.coords.latitude.toFixed(6);
    //           document.getElementById('longitude').value = position.coords.longitude.toFixed(6);
    //       });
    //       // report(result.state);
          
    //     } else if (result.state == 'prompt') {
    //        // report(result.state);
           

    //        navigator.geolocation.getCurrentPosition(function (position) {
    //            initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //            map.setCenter(initialLocation);
               
    //             marker = new google.maps.Marker({
    //               map: map,
    //               draggable: true,
    //               position: {lat: position.coords.latitude, lng: position.coords.longitude}
    //             });

    //           document.getElementById('latitude').value = position.coords.latitude.toFixed(6);
    //           document.getElementById('longitude').value = position.coords.longitude.toFixed(6);
    //        });
    //        //navigator.geolocation.getCurrentPosition(revealPosition, positionDenied, geoSettings);
    //     } else if (result.state == 'denied') {
    //        // report(result.state);
           
    //            initialLocation = new google.maps.LatLng('-0.789', '113.921');
    //            map.setCenter(initialLocation);
    //            map.setZoom(4);
    //            marker = new google.maps.Marker({
    //               map: map,
    //               draggable: true,
    //               position: {lat: -0.789, lng: 113.921}
    //             });

    //           var lt = -0.789;
    //           var lg = 113.921;
    //           document.getElementById('latitude').value = lt.toFixed(6);
    //           document.getElementById('longitude').value = lg.toFixed(6);
               

           
    //     }
    //     result.onchange = function() {
    //        //report(result.state);
    //     }
    // });
    // };

    google.maps.event.addListener(marker, 'dragend', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });

    google.maps.event.addListener(marker, 'dragstart', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });

    google.maps.event.addListener(marker, 'idle', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });
   
    var input = document.getElementById('pac-input');
   
    var autocomplete = new google.maps.places.Autocomplete(input);
    
    //set input text inside map
    // map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

   
    

    autocomplete.addListener('place_changed', function() {
      
      // marker.setVisible(false);
      marker.setMap(null);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        // window.alert("No details available for input: '" + place.name + "'");
        // set alert with style if place not found

        $.alert("No details available for input: '" + place.name + "'");

        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      // marker.setPosition(place.geometry.location);
      // marker.setVisible(true);

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          position: place.geometry.location
        });

        document.getElementById('latitude').value = place.geometry.location.lat().toFixed(6);
        document.getElementById('longitude').value = place.geometry.location.lng().toFixed(6);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      
      google.maps.event.addListener(marker, 'dragend', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });

      google.maps.event.addListener(marker, 'dragstart', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });

      google.maps.event.addListener(marker, 'idle', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });


    });

}
//end of GMAP

jQuery(document).ready(function() {
  $('.datedropper').dateDropper();


  var latitude = parseFloat($('#latitude').val());
  var longitude = parseFloat($('#longitude').val());


  google.maps.event.addDomListener(window, 'load', initMap(latitude,longitude));
});

function startLoading(x,all=null){
    if(all==1){
        $('#btnSave').attr('disabled','disabled');
        $('#btnSave').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnCancel').attr('disabled','disabled');
        $('#btnCancel').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnDelete').attr('disabled','disabled');
        $('#btnDelete').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }else{
        x.attr('disabled','disabled');
        x.append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }
}

function endLoading(x=null,all=null){
    if(all==1){
        $('#btnSave').removeAttr('disabled');
        $('#btnSave').find('span').remove();

        $('#btnCancel').removeAttr('disabled');
        $('#btnCancel').find('span').remove();

        $('#btnDelete').removeAttr('disabled');
        $('#btnDelete').find('span').remove();
    }else{
        x.removeAttr('disabled');
        x.find('span').remove();
      
    }
    // x.append('&nbsp;&nbsp;<span class="fa fa-circle-o-notch fa-spin"></span>');
}

function error_msg(x){
    x.parent().append(`<center class="error_msg"><br><label style="color: red;">Error mengubah data. Silahkan coba lagi.</label></center>`);
}


function SUBMIT(id,el){
    $('.error_msg').remove();
    startLoading(el);
    var elm = el.closest('.flagTOSS');
    var elmInput = elm.find('input.frm_');
    var elmSelect = elm.find('select.frm_');
    var elmTextarea = elm.find('textarea.frm_');

    var vSend = new Object();
    elmInput.toArray().forEach(function(item,index){
        var key = elmInput.eq(index).attr('name');
        var value = elmInput.eq(index).val();

        vSend[key] = value;
    });

    elmTextarea.toArray().forEach(function(item,index){
        var key = elmTextarea.eq(index).attr('name');
        var value = elmTextarea.eq(index).val();

        vSend[key] = value;
    });

    elmSelect.toArray().forEach(function(item,index){
        var key = elmSelect.eq(index).attr('name');
        var value = elmSelect.eq(index).val();

        vSend[key] = value;
    });
    var ALL_DATA={id:id,data:vSend};
    ALL_DATA[csrf.name]=csrf.token;
    $.post(URL+'toss/SUBMIT_set_profile',ALL_DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            endLoading(el);
            location.reload();
        }else{
            endLoading(el);
            error_msg(el);
        }
    }).fail(function(e){
        endLoading(el);
        error_msg(el);
    });
}


$('select[name="provinsi"]').on('change',function(){
    $('select[name="kota_kabupaten"]').html('<option disabled="disabled" selected="selected">-- Pilih Kota/Kabupaten --</option>');
    $('select[name="kecamatan"]').html('<option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>');

    $.get(URL+'toss_content/getkota/'+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="kota_kabupaten"]').append(`<option value=`+item.id+`>`+item.nama+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();

    $('select[name="kota_kabupaten"]');
});

$('select[name="kota_kabupaten"]').on('change',function(){
    $('select[name="kecamatan"]').html('<option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>');

    $.get(URL+'toss_content/getkec/'+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="kecamatan"]').append(`<option value=`+item.id+`>`+item.nama+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();

    $('select[name="kecamatan"]');
});

$('select[name="dealer"]').on('change',function(){
    $('select[name="cabang_induk"]').html('<option disabled="disabled" selected="selected">-- Pilih Cabang Induk --</option>');

    $.get(URL+'toss_content/getcabanginduk?dealer='+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="cabang_induk"]').append(`<option value="`+item.outlet_name+`">`+item.outlet_name+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();
});
