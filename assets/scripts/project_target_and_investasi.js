jQuery(document).ready(function() {
    $("#land, #building, #equipment").on("change, keyup, input", function() {
    	if($("#land").val()==''){
    		$("#land").val('0');
    	}
    	if($("#building").val()==''){
    		$("#building").val('0');
    	}
    	if($("#equipment").val()==''){
    		$("#equipment").val('0');
    	}
        land = parseInt($("#land").val(),0);
        building = parseInt($("#building").val(),0);
        equipment = parseInt($("#equipment").val(),0);
        total = land + building + equipment;
        $("#total").val(total);
    });
});