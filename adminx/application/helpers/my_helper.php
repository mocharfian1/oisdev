<?php
function compare_password($password, $true_password)
{
    if (is_valid_sha1($true_password)) {
        if (sha1($password) == $true_password) {
            return true;
        }
    } else {
        if ($password == $true_password) {
            return true;
        }
    }
    return false;
}
function compressImage($source, $destination, $quality)
{

    $info = getimagesize($source);

    if ($info['mime'] == 'image/jpeg') {
        $image = imagecreatefromjpeg($source);
    } elseif ($info['mime'] == 'image/gif') {
        $image = imagecreatefromgif($source);
    } elseif ($info['mime'] == 'image/png') {
        $image = imagecreatefrompng($source);
    }

    imagejpeg($image, $destination, $quality);

}
function print_image($s)
{
    set_time_limit(0);
    $src = '';
    if (substr($s, 0, 8) == 'https://' or substr($s, 0, 7) == 'http://') {
        $s = str_replace(base_url(), '', $s);
        if (file_exists($s)) {
            $s_new=$s.'_lowres';
            if(!file_exists($s_new)){
                compressImage($s, $s_new, 10);
                $t=0;
                while((filesize($s_new) > (1 * 1024 * 1024))){
                    compressImage($s_new, $s_new, 8);
                    $t++;
                    if($t>10){
                        break;
                    }
                }
            }
            $imageData = base64_encode(file_get_contents($s_new));

            $m   = explode('.', $s);
            $m   = array_pop($m);
            $src = 'data:image/' . $m . ';base64,' . $imageData;

        }
    }
    echo '<img src="' . $src . '" style="max-width: 300px; border:2px solid black; padding : 10px; vertical-align: top;">';
}
function https_to_http($s)
{
    $arrContextOptions = array(
        "ssl" => array(
            "verify_peer"      => false,
            "verify_peer_name" => false,
        ),
    );
    if (substr($s, 0, 8) == 'https://' or substr($s, 0, 7) == 'http://') {
        $s = str_replace(base_url(), '', $s);
        if (file_exists($s)) {
            $imageData = base64_encode(file_get_contents($s));

            $m   = explode('.', $s);
            $m   = array_pop($m);
            $src = 'data:image/' . $m . ';base64,' . $imageData;
            return $src;
        }
    }
    return $s;
}
function is_valid_sha1($str)
{
    return (bool) preg_match('/^[0-9a-f]{40}$/i', $str);
}
function my_form_dropdown($data = '', $options = array(), $selected = array(), $extra = '', $disabled = array(), $hidden = array())
{
    $defaults = array();

    if (is_array($data)) {
        if (isset($data['selected'])) {
            $selected = $data['selected'];
            unset($data['selected']); // select tags don't have a selected attribute
        }

        if (isset($data['options'])) {
            $options = $data['options'];
            unset($data['options']); // select tags don't use an options attribute
        }

        if (isset($data['disabled'])) {
            $disabled = $data['disabled'];
            unset($data['disabled']); // select tags don't use an disabled attribute
        }

        if (isset($data['hidden'])) {
            $hidden = $data['hidden'];
            unset($data['hidden']); // select tags don't use an hidden attribute
        }
    } else {
        $defaults = array('name' => $data);
    }

    is_array($selected) or $selected = array($selected);
    is_array($options) or $options   = array($options);
    is_array($disabled) or $disabled = array($disabled);
    is_array($hidden) or $hidden     = array($hidden);

    // If no selected state was submitted we will attempt to set it automatically
    if (empty($selected)) {
        if (is_array($data)) {
            if (isset($data['name'], $_POST[$data['name']])) {
                $selected = array($_POST[$data['name']]);
            }
        } elseif (isset($_POST[$data])) {
            $selected = array($_POST[$data]);
        }
    }

    $extra = _attributes_to_string($extra);

    $multiple = (count($selected) > 1 && stripos($extra, 'multiple') === false) ? ' multiple="multiple"' : '';

    $form = '<select ' . rtrim(_parse_form_attributes($data, $defaults)) . $extra . $multiple . ">\n";

    foreach ($options as $key => $val) {
        $key = (string) $key;

        if (is_array($val)) {
            if (empty($val)) {
                continue;
            }

            $form .= '<optgroup label="' . $key . "\">\n";

            foreach ($val as $optgroup_key => $optgroup_val) {

                $sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
                $dis = in_array($optgroup_key, $disabled) ? ' disabled' : '';
                $hid = in_array($optgroup_key, $hidden) ? ' hidden' : '';
                $form .= '<option value="' . html_escape($optgroup_key) . '"' . $sel . $dis . $hid . '>'
                . (string) xss_clean($optgroup_val) . "</option>\n";
            }

            $form .= "</optgroup>\n";
        } else {

            $form .= '<option value="' . html_escape($key) . '"'
            . (in_array($key, $selected) ? ' selected="selected"' : '') . (in_array($key, $disabled) ? ' disabled' : '') . (in_array($key, $hidden) ? ' hidden' : '') . '>'
            . (string) xss_clean($val) . "</option>\n";
        }
    }

    return $form . "</select>\n";
}

function format_date_week($date)
{

    $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $time  = strtotime($date);
    $d     = date('j', $time);
    $y     = date('Y', $time);
    $m     = date('n', $time);
    $m     = $month[$m - 1];
    $week  = '';
    if ($d > 1 and $d <= 7) {
        $week = 'W1';
    } elseif ($d > 7 and $d <= 14) {
        $week = 'W2';
    } elseif ($d > 14 and $d <= 21) {
        $week = 'W3';
    } elseif ($d > 21) {
        $week = 'W4';
    }

    return $week . ' ' . $m . ' ' . $y;
}

function todayIndDate()
{
    $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $time  = time();
    $d     = date('j', $time);
    $y     = date('Y', $time);
    $m     = date('n', $time);
    $m     = $month[$m - 1];
    return $d . ' ' . $m . ' ' . $y;
}
function format_date_full($date)
{
    $month = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $time  = strtotime($date);
    $d     = date('j', $time);
    $y     = date('Y', $time);
    $m     = date('n', $time);
    $m     = $month[$m - 1];
    return $d . ' ' . $m . ' ' . $y;
}
