<?php 

function Terbilang($x)
{
    $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    if ($x < 12)
        return " " . $abil[$x];
    elseif ($x < 20)
        return Terbilang($x - 10) . " Belas";
    elseif ($x < 100)
        return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
    elseif ($x < 200)
        return " Seratus" . Terbilang($x - 100);
    elseif ($x < 1000)
        return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
    elseif ($x < 2000)
        return " Seribu" . Terbilang($x - 1000);
    elseif ($x < 1000000)
        return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
    elseif ($x < 1000000000)
        return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function check_00($str) {
    if( strpos($str,'00') === false ) {
        return false;
    } else {
        return true;
    }
}

function check_000($str) {
    if( strpos($str,'000') === false ) {
        return false;
    } else {
        return true;
    }   
}

function TerbilangKoma($x) {
    // echo $x;
    $kalimat = "";
    if($x == "") {
        $kalimat = "Kosong";
    } else {
        if(strpos($x,',') === false) {
            // tidak ada koma
            // echo "tidak ada koma";
            $kata = preg_replace("/[^0-9]/", "", $x);
            $kalimat = Terbilang($kata);
        } else {
            // echo "tidak ada koma 2 ";
            $kata = explode(',',$x);
            // print_r($kata);
            $kata0 = preg_replace("/[^0-9]/", "",$kata[0]);
            $kata1 = $kata[1];
            // echo count($kata);
            if(count($kata) > 1) {
                if($kata1 == "00") {
                    $kalimat = Terbilang($kata0);
                } else {
                    $kalimat = Terbilang($kata0) . " Koma " . Terbilang($kata1);
                }
            } else {
                $kalimat = Terbilang($kata0);
            }
        }
    }
    return $kalimat;
}

function number_format_dots($number) {
    return number_format($number,0,',','.');
}

function number_format_dots_rupiah($number) {
    return "Rp ".number_format($number,0,',','.');
}

function number_format_dots_decimal($number) {
    return number_format($number,2,',','.');
}

function number_format_dots_rupiah_decimal($number) {
    return "Rp ".number_format($number,2,',','.');
}

function number_format_dot($angka){
    if(!empty($angka)){
        return number_format($angka,0,',','.');
    }else{
        return $angka;
    }
}

function number_format_decimal($angka){
    if(!empty($angka)){
        return number_format($angka,2,',','.');
    }else{

        $angka = str_replace("0","",$angka);
        return $angka.'0,00';
    }
}

function rupiah_format_no_flag($angka) {
    return number_format($angka,2,',','.');
}

function formatDesimalPhp($value){
    return number_format($value, 2, '.', '');
}

function numberConvertToDB($number) {

}

function delete_min_char($value){
    return str_replace('-', '', $value);
}

function terbilang_english($data){
    $bilangan=$data;
    $kalimat="";
    $angka   =  array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
    $kata    =  array('','One','Two','Three','Four','Five','Six','Seven','Eight','Nine');
    $tingkat =  array('','Thousand','Million','Billion','Trillion');
    $panjang_bilangan = strlen($bilangan);
     
    /* pengujian panjang bilangan */
    if($panjang_bilangan > 15){
        $kalimat = "Beyond the limits";
    }else{
        /* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
        for($i = 1; $i <= $panjang_bilangan; $i++) {
            $angka[$i] = substr($bilangan,-$i,1);
        }
         
        $i = 1;
        $j = 0;
         
        /* mulai proses iterasi terhadap array angka */
        while($i <= $panjang_bilangan){
            $subkalimat = "";
            $kata1 = "";
            $kata2 = "";
            $kata3 = "";
             
            /* untuk Ratusan */
            if($angka[$i+2] != "0"){
                if($angka[$i+2] == "1"){
                    $kata1 = "One Hundred";
                }else{
                    $kata1 = $kata[$angka[$i+2]] ." Hundred";
                }
            }
             
            /* untuk Puluhan atau Belasan */
            if($angka[$i+1] != "0"){
                if($angka[$i+1] == "1"){
                    if($angka[$i] == "0"){
                        $kata2 = "Ten";
                    }else if($angka[$i] == "1"){
                        $kata2 = "Elevent";
                    }else if($angka[$i] == "2"){
                        $kata2 = "Twelve";
                    }else if($angka[$i] == "3"){
                        $kata2 = "Thirteen";
                    }else{
                        $kata2 = $kata[$angka[$i]]. "teen";
                    }
                }else if($angka[$i+1] == "2"){
                    $kata2 = "Twenty";
                }else if($angka[$i+1] == "3"){
                    $kata2 = "Thirty";
                }else{
                    $kata2 = $kata[$angka[$i+1]]. "ty";
                }
            }
             
            /* untuk Satuan */
            if ($angka[$i] != "0"){
                if ($angka[$i+1] != "1"){
                    $kata3 = $kata[$angka[$i]];
                }
            }
             
            /* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
            if (($angka[$i] != "0") || ($angka[$i+1] != "0") || ($angka[$i+2] != "0")){
                $subkalimat = $kata1.' '.$kata2.' '.$kata3.' '.$tingkat[$j].' ';
            }
             
            /* gabungkan variabel sub kalimat (untuk Satu blok 3 angka) ke $iabel kalimat */
            $kalimat = $subkalimat.''. $kalimat;
            $i = $i + 3;
            $j = $j + 1;
        }
         
        /* mengganti Satu Ribu jadi Seribu jika diperlukan */
        if (($angka[5] == "0") && ($angka[6] == "0")){
            $kalimat = str_replace("One Thousand","One Thousand",$kalimat);

            
        }
    }
    return $kalimat;
}

