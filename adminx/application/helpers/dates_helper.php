<?php 
function f_next_month($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year + 1;
    }else{
      $next_month=$this_month + 1;
      $next_year=$this_year;
    }
    
    return date('F-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function f_next_month_number($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year + 1;
    }else{
      $next_month=$this_month + 1;
      $next_year=$this_year;
    }
    
    return date('m-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function f_last_day($month){
    $day = date('Y-m-d', strtotime($month.'- 1 day'));

    return $day;
}

function f_last_month_number($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year - 1;
    }else{
      $next_month=$this_month - 1;
      $next_year=$this_year;
    }
    
    return date('m-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function f_date($dt){
    if(!empty($dt)){
        $bulan = array("","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");

        $date=date_create($dt);
        $month = (int)date_format($date,"m");
        return date_format($date,"d").'&nbsp;'.$bulan[$month].'&nbsp;'.date_format($date,"Y");
    }else{
        return false;
    }
} 

