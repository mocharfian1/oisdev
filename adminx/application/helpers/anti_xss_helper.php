<?php

function axss($str){
    echo htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function sanitize_string($str){
    $str_sanitized_1 = str_replace("'", "", $str);
    $str_sanitized_2 = str_replace('"', '', $str_sanitized_1);

    return $str_sanitized_2;  
}