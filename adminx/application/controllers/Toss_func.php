<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Toss_func extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function ls_area()
    {
        echo json_encode($this->db->where('is_delete', 0)->select('*')->get('tbl_area')->result());
    }

    public function ls_sub_area($id_area = null)
    {
        echo json_encode($this->db->where('is_delete', 0)->where('id_area', $id_area)->select('*')->get('tbl_sub_area')->result());
    }

    public function add_pic_visit()
    {
        $data   = $this->input->post('data');
        $insert = $this->db->insert('tbl_toss_database_visit', $data);

        if ($insert) {
            echo json_encode(array('status' => 1, 'result' => 'success'));
        }
    }

}
