<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('EditPass_model');
        $this->form_validation->set_error_delimiters('<strong><div class="text-danger">', '</div></strong>');
        $logged_in = $this->session->userdata('logged_in');
        if (!$logged_in) {
            redirect('login/index');
        }
    }

    public function index()
    {
        $data['menu']        = 'home';
        $data['sub_menu']    = '';
        $data['content']     = 'view_home';
        $data['tpl_js']      = 'home';
        $data['plugin_mode'] = '1';
        $this->load->view('tpl_index', $data);

    }

    public function changePass()
    {
        $data['menu']        = 'home';
        $data['sub_menu']    = '';
        $data['content']     = 'change_pass';
        $data['tpl_js']      = 'home';
        $data['plugin_mode'] = '2';
        $this->load->view('tpl_index', $data);
    }

    public function editPass($id)
    {

        $new_password  = xss_clean($this->input->post('newpass'));
        $conf_password = xss_clean($this->input->post('confpass'));

        if ($new_password == $conf_password) {
            $data = array(
                'password'    => sha1($new_password),
            );

            $id = $this->session->userdata('id');

            if ($this->EditPass_model->updatePass($data, $id)) {
                $this->session->set_flashdata('message', "<div class='alert alert-success alert-dismissible'><strong>
                            Password change successfully!</strong>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                          </div>");
                redirect(base_url('home/changePass'));
            } else {
                $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissible'><strong>
                            Password change failed</strong>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                              <span aria-hidden='true'>&times;</span>
                            </button>
                          </div>");
                redirect(base_url('home/changePass'));
            }

        } else {
            $this->session->set_flashdata('message', "<div class='alert alert-danger alert-dismissible'><strong>
                        New password & Confirm password is not matching</storng>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                          <span aria-hidden='true'>&times;</span>
                        </button>
                      </div>");
            redirect(base_url('home/changePass'));
        }
    }

}
