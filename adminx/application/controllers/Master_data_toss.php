<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Master_data_toss extends CI_Controller
{
    private $userdata = null;

    private $application_module = 'TOSS';
    public function __construct()
    {
        parent::__Construct();
        $this->load->helper('form');
        $this->load->model("MMaster_data");
        $this->load->model("MEvaluasi_kunjungan");
        $this->load->model("MLogging");

        $this->userdata = (object) $this->session->userdata();
        $logged_in      = $this->session->userdata('logged_in');
        if (!$logged_in) {
            redirect('login/index');
        }
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    // //ordinary master insert data
    // public function insert_data($what_data){

    // }

    // //ordinary master update data
    // public function update_data($what_data){

    // }

    // //ordinary master delete data
    // public function delete_data($what_data, $id){

    // }

    //master toss
    public function toss($mode = null, $id = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'tam'])) {

            $data['menu']     = 'master_data';
            $data['sub_menu'] = 'toss';
            $data['mode']     = $mode;
            $data['content']  = 'view_master_data';
            $data['tpl_js']   = 'master_data';

            if ($mode == 'view') {
                // $data['data_area'] = $this->MMaster_data->showAllArea()->result_array();
                $data['plugin_mode'] = '2';
            } else if ($mode == 'add' || $mode == 'edit' || $mode == 'kapasitas_bengkel' || $mode == 'target_investasi' || $mode == 'checklist_sertifikasi' || $mode == 'checklist_sertifikasi_manpower' || $mode == 'checklist_sertifikasi_equipment') {
                $data['plugin_mode'] = '1';
            } else {
                redirect('errors/code/404');
            }

            $this->load->view('tpl_index', $data);
        } else {
            redirect('errors/code/404');

        }
    }

    //master guidance toss
    public function guidance_toss($mode = null, $id = null)
    {

        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss'])) {
            $data['menu']     = 'master_data';
            $data['sub_menu'] = 'guidance_toss';
            $data['mode']     = $mode;
            $data['content']  = 'view_master_data_toss';
            $data['tpl_js']   = 'master_data_toss';

            $data['plugin_mode'] = '4';
            if ($mode == 'config') {
                $this->load->model('guidance_toss');
                $data['data_toss']    = $this->guidance_toss->getDataGuidance();
                $data['getDataItems'] = $this->guidance_toss->getDataGuidance_items();
                $data['getDataArea']  = $this->guidance_toss->getDataGuidance_area();
                $data['getDataSub']   = $this->guidance_toss->getDataGuidance_sub();

                $data['getDataEQGroup'] = $this->guidance_toss->getDataGuidance_equipment_group();
                $data['getDataEQItems'] = $this->guidance_toss->getDataGuidance_equipment_item();
            } else {
                redirect('errors/code/404');
            }

            $this->load->view('tpl_index', $data);
        } else {
            redirect('errors/code/404');

        }
    }

    //master toss database
    public function toss_database($mode = null, $id = null, $type = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'tam'])) {
            $data['menu']     = 'master_data';
            $data['sub_menu'] = 'toss_database';
            $data['mode']     = $mode;
            $data['content']  = 'view_master_data_toss';
            $data['tpl_js']   = 'master_data_toss';

            if ($mode == 'view') {
                $data['plugin_mode'] = '2';

                $data['tb_db_toss'] = null;
                /*$tb_list_toss       = $this->db->query('select
                t.*,
                (select nama from tbl_lokasi_provinsi where id=t.provinsi) as provinsi_name,
                (select nama from tbl_lokasi_kota_kabupaten where id=t.kota_kabupaten) as kota_kabupaten_name,
                (select nama from tbl_lokasi_kecamatan where id=t.kecamatan) as kecamatan_name,
                (select username from tbl_user where id=t.id_user_dealer) as user_dealer
                from
                tbl_toss_database as t
                where t.is_delete=0');*/
                $tb_list_toss = $this->db
                    ->select("
                    t.*,
                    (select nama from tbl_lokasi_provinsi where id=t.provinsi) as provinsi_name,
                    (select nama from tbl_lokasi_kota_kabupaten where id=t.kota_kabupaten) as kota_kabupaten_name,
                    (select nama from tbl_lokasi_kecamatan where id=t.kecamatan) as kecamatan_name,
                    (select username from tbl_user where id=t.id_user_dealer) as user_dealer")
                    ->from('tbl_toss_database t')
                    ->where('t.is_delete', 0)->get();
                if ($tb_list_toss->num_rows() > 0) {
                    $data['tb_db_toss'] = $tb_list_toss->result();
                }
            } else if ($mode == 'content') {
                $data['plugin_mode'] = '3';

                $data['dt'] = null;
                // $dt = $this->db->where('id',$id)->select('*')->get('tbl_toss_database');
                /*$dt = $this->db->query('select
                t.*,
                (select nama from tbl_lokasi_provinsi where id=t.provinsi) as provinsi_name,
                (select nama from tbl_lokasi_kota_kabupaten where id=t.kota_kabupaten) as kota_kabupaten_name,
                (select nama from tbl_lokasi_kecamatan where id=t.kecamatan) as kecamatan_name,
                (select username from tbl_user where id=t.id_user_dealer) as user_dealer
                from
                tbl_toss_database as t
                where
                t.id=' . $id);*/
                $dt = $this->db->select("
                    t.*,
                    (select nama from tbl_lokasi_provinsi where id=t.provinsi) as provinsi_name,
                    (select nama from tbl_lokasi_kota_kabupaten where id=t.kota_kabupaten) as kota_kabupaten_name,
                    (select nama from tbl_lokasi_kecamatan where id=t.kecamatan) as kecamatan_name,
                    (select username from tbl_user where id=t.id_user_dealer) as user_dealer")
                    ->from('tbl_toss_database t')
                    ->where('t.id', $id)
                    ->get();
                if ($dt->num_rows() > 0) {
                    $data['data_toss'] = $dt->row();
                }

                $this->load->model('database_toss');
                $data['getDataItems'] = $this->database_toss->getDataGuidance_items($id);
                $data['getDataArea']  = $this->database_toss->getDataGuidance_area($id);
                $data['getDataSub']   = $this->database_toss->getDataGuidance_sub($id);

                $data['provinsi'] = $this->db->select('*')->get('tbl_lokasi_provinsi')->result();

                $data['getDataEQGroup'] = $this->database_toss->getDataGuidance_equipment_group($id);
                $data['getDataEQItems'] = $this->database_toss->getDataGuidance_equipment_item($id);

                $this->load->model('guidance_toss');
                $data['ls_pic_visit'] = $this->guidance_toss->ls_pic_visit($id);

                $data['ls_manpower_sdm_after_sales'] = $this->database_toss->ls_manpower_sdm_after_sales($id);
                $data['ls_manpower_sdm_umum']        = $this->database_toss->ls_manpower_sdm_umum($id);

                $data['getCountManPower'] = $this->database_toss->getCountManPower($id);

                if (!empty($data['getDataEQItems'])) {
                    foreach ($data['getDataEQItems'] as $key => $value) {
                        $data['getDataEQItems'][$key]->img = $this->database_toss->ls_img_eq($value->id);
                    }
                }

            } else if ($mode == 'add') {
                $data['plugin_mode'] = '4';

                $data['user_dealer'] = null;
                $user_dealer         = $this->db->where('is_delete', 0)->where('user_type', 'dealer')->select('*')->get('tbl_user');
                if ($user_dealer->num_rows() > 0) {
                    $data['user_dealer'] = $user_dealer->result();
                }

                $data['provinsi'] = $this->db->select('*')->get('tbl_lokasi_provinsi')->result();

                $data['kota_kab'] = $this->db->select('*')->get('tbl_lokasi_kota_kabupaten')->result();

                $data['kecamatan'] = $this->db->select('*')->get('tbl_lokasi_kecamatan')->result();
                $data['ls_dealer'] = $this->db->select('*')->order_by('dealer_name', 'asc')->group_by('dealer_name')->get('tbl_toss_dealer')->result();

            } else if ($mode == 'histori_foto') {
                $data['plugin_mode'] = '4';
                $data['type']        = $type;

                if ($type == 'equipment') {
                    $data['img_list'] = $this->db->where('item_key', $id)->select('*')->group_by('img_key')->order_by('id', 'desc')->get('tbl_toss_database_cs_equipment_foto_dealer')->result();
                }

                if ($type == 'area') {
                    $data['img_list'] = $this->db->where('img_key', $id)->select('*')->order_by('id', 'desc')->get('tbl_toss_database_cs_area_histori')->result();
                }

            } else {
                redirect('errors/code/404');
            }

            $this->load->view('tpl_index', $data);
        } else {
            redirect('errors/code/404');

        }
    }

    public function getkota($id_prov = null)
    {
        if ($id_prov > 0 && $id_prov !== null) {
            echo json_encode($data['kota_kab'] = $this->db->where('id_provinsi', $id_prov)->select('*')->get('tbl_lokasi_kota_kabupaten')->result());
        } else {
            redirect('errors/code/404');
        }
    }

    public function getkec($id_kota = null)
    {
        if ($id_kota > 0 && $id_kota !== null) {
            echo json_encode($this->db->where('id_kota_kabupaten', $id_kota)->select('*')->get('tbl_lokasi_kecamatan')->result());
        } else {
            redirect('errors/code/404');
        }
    }

    public function getcabanginduk()
    {
        if (isset($_GET['dealer'])) {
            echo json_encode($this->db->where('dealer_name', $_GET['dealer'])->select('*')->group_by('outlet_name')->order_by('dealer_name', 'asc')->get('tbl_toss_dealer')->result());
        } else {
            redirect('errors/code/404');
        }
    }

    public function submit_save_guidance()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss'])) {

            if (!empty($this->input->post('data'))) {
                $this->db->trans_begin();
                $data      = $this->input->post('data');
                $area      = !empty($this->input->post('area')) ? $this->input->post('area') : null;
                $equipment = !empty($this->input->post('equipment')) ? $this->input->post('equipment') : null;

                $itemsDelete   = !empty($this->input->post('delete')) ? xss_clean($this->input->post('delete')) : null;
                $itemsEQDelete = !empty($this->input->post('delete_eq')) ? xss_clean($this->input->post('delete_eq')) : null;

                if (!empty($itemsDelete)) {
                    foreach ($itemsDelete as $key => $value) {
                        $this->db->where('id', $value)->update('tbl_toss_guidance_cs_area', array('is_delete' => 1, 'is_active' => 0));
                    }
                }

                if (!empty($itemsEQDelete)) {
                    foreach ($itemsEQDelete as $key => $value) {
                        $this->db->where('id', $value)->update('tbl_toss_guidance_cs_equipment', array('is_delete' => 1, 'is_active' => 0));
                    }
                }

                $data['id'] = 1;
                $ck_tb      = $this->db->select('*')->get('tbl_toss_guidance');

                if ($ck_tb->num_rows() > 0) {
                    $insert                = $this->db->where('id', 1)->update('tbl_toss_guidance', $data);
                } else {
                    $insert = $this->db->insert('tbl_toss_guidance', $data);
                }

                $log_new_value_data = [];
                foreach ($data as $row) {
                    $log_new_value_data[] = $row;
                }

                if (!empty($area)) {
                    $areaInsert = $this->db->insert_batch('tbl_toss_guidance_cs_area', $area);
                } 

                if (!empty($equipment)) {
                    $equipmentInsert = $this->db->insert_batch('tbl_toss_guidance_cs_equipment', $equipment);
                } 

                $smodule  = 'back end guidance toss';
                $activity = 'Change Guidance Toss';
                $tbl_name = 'tbl_toss_guidance';

                $dberror = $this->db->error();
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    $addtional_information = 'Save data failed';
                    $log_new_value_data    = [];
                    if ($dberror['code'] != '0') {
                        $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';

                    }
                    $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array("status" => 0, 'result' => 'Save data failed'));

                } else {
                    $this->db->trans_commit();

                    //insert insert log
                    $addtional_information = '';
                    $this->MLogging->insert_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array('status' => 1, 'result' => 'success'));
                }
            } else {
                echo json_encode(array('status' => 0, 'result' => 'error'));
            }

        } else {
            echo json_encode(array('status' => 0, 'result' => 'Access Denied'));
        }

    }

    public function submit_save_data()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'tam'])) {
            if (!empty($this->input->post('data'))) {
                $this->db->trans_begin();

                $data = $this->input->post('data');

                if (!empty($data['tgl_sewa_start'])) {
                    $data['tgl_sewa_start'] = date('Y-m-d', strtotime($data['tgl_sewa_start']));
                }

                if (!empty($data['tgl_sewa_end'])) {
                    $data['tgl_sewa_end'] = date('Y-m-d', strtotime($data['tgl_sewa_end']));
                }

                $log_new_value_data = [];
                foreach ($data as $row) {
                    $log_new_value_data[] = $row;

                }
                $insert       = $this->db->insert('tbl_toss_database', $data);
                $insert_id_db = $this->db->insert_id();

                // INSERT INTO TOSS FRONT END

                $field = array();
                // $field_front = $this->db->query('select * from tbl_toss_front_end where id_toss_database='.$insert_id_db);
                //$col = $this->db->query('SHOW columns from tbl_toss_front_end')->result();
                $col = $this->db->list_fields('tbl_toss_front_end');
                foreach ($col as $key => $value) {
                    $field[$value] = '';
                }

                //$dt_toClone                = $this->db->query('select * from tbl_toss_database where id=' . $insert_id_db)->row();
                $dt_toClone                = $this->db->from('tbl_toss_database')->where('id', $insert_id_db)->get()->row();
                $field['id_toss_database'] = $insert_id_db;
                foreach ($dt_toClone as $key => $value) {
                    $ck = array_key_exists($key, $field);
                    if ($ck) {
                        $field[$key] = $value;
                        unset($field['id']);
                    }
                }

                $insert_fe    = $this->db->insert('tbl_toss_front_end', $field);
                $id_front_end = $this->db->insert_id();

                //#########################################

                $this->load->model('duplicate_toss');
                //GET DATA AREA FROM GUIDANCE TOSS
                $db_field = array();
                $db_col   = $this->db->list_fields('tbl_toss_database_cs_area');
                foreach ($db_col as $key => $value) {
                    $db_field[$value] = '';
                }

                //$db_toss = $this->db->query('select * from tbl_toss_guidance_cs_area where is_delete=0 and is_active=1')->result();
                $db_toss = $this->db->from('tbl_toss_guidance_cs_area')->where('is_delete', 0)->where('is_active', 1)->get()->result();

                if (!empty($db_toss) && count($db_toss) > 0) {
                    $dt_db   = [];
                    $key_img = 1;
                    foreach ($db_toss as $key => $value) {
                        $_dt = array();
                        foreach ($value as $key_v => $value_v) {

                            $ck = array_key_exists($key_v, $db_field);
                            if ($ck) {
                                $_dt[$key_v] = $value_v;
                                unset($_dt['id']);
                            }
                        }
                        $_dt['id_toss_database'] = $insert_id_db;
                        $_dt['img_key']          = md5(time() . $key_img);
                        array_push($dt_db, $_dt);
                        $key_img++;
                    }
                    $insert_db_area = $this->db->insert_batch('tbl_toss_database_cs_area', $dt_db);

                    $this->duplicate_toss->addAreaToFront($insert_id_db, $id_front_end);
                }
                //###############################

                //GET DATA EQUIPMENT FROM GUIDANCE TOSS
                $db_field_eq = array();
                //$db_col_eq   = $this->db->query('SHOW columns from tbl_toss_database_cs_equipment')->result();
                $db_col_eq = $this->db->list_fields('tbl_toss_database_cs_equipment');
                foreach ($db_col_eq as $key => $value) {
                    $db_field_eq[$value] = '';
                }

                //$db_toss_eq = $this->db->query('select * from tbl_toss_guidance_cs_equipment where is_delete=0 and is_active=1')->result();
                $db_toss_eq = $this->db->from('tbl_toss_guidance_cs_equipment')->where('is_delete', 0)->where('is_active', 1)->get()->result();

                if (!empty($db_toss_eq) && count($db_toss_eq) > 0) {
                    $dt_db     = [];
                    $no_it_key = 1;
                    foreach ($db_toss_eq as $key => $value) {
                        $_dt = array();
                        foreach ($value as $key_v => $value_v) {

                            $ck = array_key_exists($key_v, $db_field_eq);
                            if ($ck) {
                                $_dt[$key_v] = $value_v;
                                unset($_dt['id']);
                            }
                        }
                        $_dt['id_toss_database'] = $insert_id_db;
                        $_dt['item_key']         = md5(time() . $no_it_key);
                        array_push($dt_db, $_dt);
                        $no_it_key++;
                    }
                    $insert_db_eq = $this->db->insert_batch('tbl_toss_database_cs_equipment', $dt_db);

                    $this->duplicate_toss->addEQToFront($insert_id_db, $id_front_end);
                }
                //###############################

                $smodule  = 'back end toss database';
                $activity = 'Change Toss Database';
                $tbl_name = 'tbl_toss_database';
                $action   = 'insert toss database';

                $dberror = $this->db->error();
                if ($this->db->trans_status() === false) {
                    if ($dberror['code'] != '0') {
                        // catch error DB

                        $this->db->trans_rollback();

                        //insert insert log
                        $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                        $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                        echo json_encode(array('status' => 0, 'result' => $addtional_information));

                    } else {

                        $this->db->trans_rollback();

                        //insert insert log
                        $addtional_information = '';
                        $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                        echo json_encode(array("status" => 0, 'result' => 'Save data failed'));

                    }
                } else {
                    $this->db->trans_commit();

                    //insert insert log
                    $addtional_information = '';
                    $this->MLogging->insert_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array('status' => 1, 'result' => 'success'));
                }

            } else {
                echo json_encode(array('status' => 0, 'result' => 'error'));
            }
        } else {
            echo json_encode(array('status' => 0, 'result' => 'Access Denied'));

        }
    }

    public function submit_toss_database()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'tam'])) {

            if (!empty($this->input->post('id')) && !empty($this->input->post('data'))) {
                $this->db->trans_begin();

                $id           = xss_clean($this->input->post('id'));
                $data         = $this->input->post('data');
                $pic          = !empty($this->input->post('pic')) ? xss_clean($this->input->post('pic')) : null;
                $del_pic      = !empty($this->input->post('del_pic')) ? xss_clean($this->input->post('del_pic')) : null;
                $evaluasi     = !empty($this->input->post('evaluasi')) ? $this->input->post('evaluasi') : null;
                $ch_area      = !empty($this->input->post('ch_area')) ? $this->input->post('ch_area') : null;
                $del_eq_photo = !empty($this->input->post('del_eq_photo')) ? xss_clean($this->input->post('del_eq_photo')) : null;
                $ch_eq        = !empty($this->input->post('ch_eq')) ? $this->input->post('ch_eq') : null;
                $img_outlet   = !empty($this->input->post('img_outlet')) ? $this->input->post('img_outlet') : null;
                foreach ($evaluasi as $key => $value) {
                    if ($value['field'] == 'status') {
                        $this->db->where('id_toss_database', $value['id'])->update('tbl_toss_front_end', array('status' => $value['value']));
                    }
                    $this->db->where('id', $value['id'])->update($value['table'], array($value['field'] => $value['value']));
                }

                $data['sertifikasi_tgl'] = !empty($data['sertifikasi_tgl']) ? date('Y-m-d', strtotime($data['sertifikasi_tgl'])) : null;
                if (!empty($img_outlet)) {
                    $data['outlet_photo_file_name_64'] = $img_outlet;
                    $this->db->where('id_toss_database', $id)->update('tbl_toss_front_end', array('outlet_photo_file_name_64' => $img_outlet));
                }
                $log_new_value_data = [];
                foreach ($data as $row) {
                    $log_new_value_data[] = $row;

                }

                $update = $this->db->where('id', $id)->update('tbl_toss_database', $data);

                // $insert = 0;
                if (!empty($pic)) {
                    foreach ($pic as $key => $value) {
                        $pic[$key]['visit_date'] = date('Y-m-d', strtotime($pic[$key]['visit_date']));
                    }
                    $insert = $this->db->insert_batch('tbl_toss_database_visit', $pic);
                }

                if (!empty($del_pic)) {
                    foreach ($del_pic as $key => $value) {
                        $update = $this->db->where('id', $value)->update('tbl_toss_database_visit', array('is_delete' => 1));
                    }
                }

                if (!empty($ch_area)) {
                    foreach ($ch_area as $key => $value) {
                        // $bef = $this->db->where(array('img_key'=>$value['img_key'],'is_delete'=>0))->select('*')->get('tbl_toss_database_cs_area');

                        $this->db->where(array('img_key' => $value['img_key'], 'is_delete' => 0))->update('tbl_toss_database_cs_area', array('foto_dealer_file_name_64' => $value['src']));
                        $this->db->where(array('img_key' => $value['img_key'], 'is_delete' => 0))->update('tbl_toss_front_end_cs_area', array('foto_dealer_file_name_64' => $value['src']));

                        $this->db->insert('tbl_toss_database_cs_area_histori', array(
                            'img_key'       => $value['img_key'],
                            'foto_64'       => $value['src'],
                            'diupload_oleh' => 'TAM',
                            'upload_date'   => date('Y-m-d H:m:s'),
                        ));

                    }
                }

                if (!empty($del_eq_photo)) {
                    foreach ($del_eq_photo as $key => $value) {
                        // print_r($value);
                        $this->db->where(array('img_key' => $value['key'], 'item_key' => $value['item_key'], 'is_delete' => 0))->update('tbl_toss_database_cs_equipment_foto_dealer', array('is_delete' => 1));
                        $this->db->where(array('img_key' => $value['key'], 'item_key' => $value['item_key'], 'is_delete' => 0))->update('tbl_toss_front_end_cs_equipment_foto_dealer', array('is_delete' => 1));
                        // $this->db->delete('tbl_toss_database_cs_equipment_foto_dealer', array('img_key' => $value));
                        // $this->db->delete('tbl_toss_front_end_cs_equipment_foto_dealer', array('img_key' => $value));
                    }
                }

                if (!empty($ch_eq)) {
                    $no_key = 1;
                    foreach ($ch_eq as $key => $value) {
                        $value['img_key']       = md5(time() . $no_key);
                        $value['diupload_oleh'] = 'TAM';
                        $this->db->insert('tbl_toss_database_cs_equipment_foto_dealer', $value);

                        unset($value['diupload_oleh']);
                        $value['id_toss_database_cs_equipment'] = $this->db->where('item_key', $value['item_key'])->select('id')->get('tbl_toss_front_end_cs_equipment')->row()->id;
                        $this->db->insert('tbl_toss_front_end_cs_equipment_foto_dealer', $value);
                        $no_key++;
                        // $this->db->where('img_key',$value['img_key'])->update('tbl_toss_front_end_cs_area',array('foto_dealer_file_name_64'=>$value['src']));
                    }
                }

                $smodule  = 'back end toss database';
                $activity = 'Change Toss Database';
                $tbl_name = 'tbl_toss_database';
                $action   = 'insert toss database';

                $dberror = $this->db->error();
                if ($this->db->trans_status() === false) {
                    if ($dberror['code'] != '0') {
                        // catch error DB

                        $this->db->trans_rollback();

                        //insert insert log
                        $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                        $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                        echo json_encode(array('status' => 0, 'result' => $addtional_information));

                    } else {

                        $this->db->trans_rollback();

                        //insert insert log
                        $addtional_information = '';
                        $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                        echo json_encode(array("status" => 0, 'result' => 'Save data failed'));

                    }
                } else {
                    $this->db->trans_commit();

                    //insert insert log
                    $addtional_information = '';
                    $this->MLogging->insert_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array('status' => 1, 'result' => 'success'));
                }
            } else {
                echo json_encode(array('status' => 0, 'result' => 'error'));
            }
        } else {
            echo json_encode(array('status' => 0, 'result' => 'Access Denied'));

        }
    }

    public function uploadfile($id = null)
    {

        if ($id !== null) {

            $name_file = time() . '_' . $_FILES["file"]["name"];
            // $target_dir = "assets/";
            // $target_file = $target_dir . basename($_FILES["file"]["name"]);
            // $uploadOk = 1;
            // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // echo $target_file;
            // Check if image file is a actual image or fake image
            if (isset($_POST["submit"])) {
                if (move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    'upload/toss_attachments/' . $name_file
                )) {
                    $this->db->where('id', $id)->update('tbl_toss_database', array('sertifikasi_attachment_file_name' => $name_file));
                    echo json_encode(array('status' => 1, 'result' => 'success'));
                } else {
                    echo json_encode(array('status' => 0, 'result' => 'error'));
                }

                // if(is_writable('upload/toss_attachments'.$_FILES["file"]["name"])){
                //   echo "SIP";
                // }
            } else {
                echo json_encode(array('status' => 0, 'result' => 'error'));
            }
        } else {
            echo json_encode(array('status' => 0, 'result' => 'error'));
        }
    }

    public function delete_toss()
    {

        if (!empty($this->input->post('id'))) {
            $this->db->trans_begin();
            $id        = xss_clean($this->input->post('id'));
            $delete    = $this->db->where('id', $id)->update('tbl_toss_database', array('is_delete' => 1));
            $delete_fr = $this->db->where('id_toss_database', $id)->update('tbl_toss_front_end', array('is_delete' => 1));

            $table_name = 'tbl_toss_database';
            $smodule    = 'back end toss database';
            $activity   = 'Change TOSS database';
            $dberror    = $this->db->error();
            if ($this->db->trans_status() === false) {

                if ($dberror['code'] != '0') {
                    // catch error DB

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data    = array();
                    $addtional_information = 'Soft Delete. Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $table_name, 'Update', '', $log_new_value_data);
                    echo json_encode(array("status" => false, 'result' => 'Hapus data gagal. DB Error.'));

                } else {

                    $this->db->trans_rollback();

                    //insert update log
                    $addtional_information = 'Soft Delete. Error';
                    $log_new_value_data    = array();
                    $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', '', $table_name, 'Update', '', $log_new_value_data);
                    echo json_encode(array("status" => false, 'result' => 'Hapus data gagal'));

                }
            } else {

                $log_old_value_data = array(
                    $id,
                    '0',
                );

                $log_new_value_data = array(
                    $id,
                    '1',
                );

                $addtional_information = 'Soft Delete';

                $this->db->trans_commit();

                //insert update log
                $this->MLogging->update_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $table_name, 'Update', $log_old_value_data, $log_new_value_data);
                echo json_encode(array("status" => true, 'result' => ''));

            }
        } else {
            redirect('errors/code/404');
        }
    }

    public function baca_excel()
    {

        if (isset($_FILES['file']['tmp_name'])) {

            include_once APPPATH . 'libraries/PHPExcel.php';
            $objPHPExcel     = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']);
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            foreach ($cell_collection as $cell) {
                $column     = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row        = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();

                //The header will/should be in row 1 only. of course, this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                } else {
                    $arr_data[$row][$column] = $data_value;
                }
            }

            $data = [];
            foreach ($arr_data as $key => $value) {
                array_push($data, array('dealer_name' => $value['A'], 'outlet_name' => $value['B']));
            }

            $empty = $this->db->truncate('tbl_toss_dealer');
            if ($empty) {
                $insert = $this->db->insert_batch('tbl_toss_dealer', $data);

                if ($insert) {
                    echo json_encode(array('status' => 1, 'result' => 'success'));
                } else {
                    echo json_encode(array('status' => 0, 'result' => 'error'));
                }
            } else {
                echo json_encode(array('status' => 0, 'result' => 'error'));
            }

        } else {

            redirect('errors/code/404');

        }

    }

    public function toss_dealer($mode = null)
    {
        $data['menu']     = 'master_data';
        $data['sub_menu'] = 'toss_dealer';
        $data['mode']     = $mode;
        $data['content']  = 'view_master_data_toss_dealer';
        $data['tpl_js']   = 'master_data_toss';
        $this->load->model('database_toss');

        if ($mode == 'view') {
            $data['plugin_mode'] = '2';
        } else if ($mode == 'add' || $mode == 'edit' || $mode == 'kapasitas_bengkel' || $mode == 'target_investasi' || $mode == 'checklist_sertifikasi' || $mode == 'checklist_sertifikasi_manpower' || $mode == 'checklist_sertifikasi_equipment') {
            $data['plugin_mode'] = '1';
        } else {
            redirect('errors/code/404');
        }

        $data['ls_dealer'] = $this->database_toss->ls_dealer();

        $this->load->view('tpl_index', $data);
    }

    public function cek_dealer()
    {

        if (!empty($this->input->post('tb'))) {

            $table    = xss_clean($this->input->post('tb'));
            $table_fe = xss_clean($this->input->post('tb_fe'));
            $id       = xss_clean($this->input->post('id'));
            $name     = xss_clean($this->input->post('name'));

            if ($table == 'tbl_toss_database_cs_manpower') {
                $key = $this->db->where('id', $id)->select('key')->get($table)->row();
                $res = $this->db->where('key', $key->key)->select('id,score_evaluasi_dealer')->get($table_fe)->row();

                if ($res->score_evaluasi_dealer == 1) {
                    echo 'sudah';
                } else {
                    echo 'belum';
                }
            }

            if ($table == 'tbl_toss_database_cs_area') {
                $key = $this->db->where('id', $id)->select('img_key')->get($table)->row();
                $res = $this->db->where('img_key', $key->img_key)->select('id,score_evaluasi_dealer')->get($table_fe)->row();

                if ($res->score_evaluasi_dealer == 1) {
                    echo 'sudah';
                } else {
                    echo 'belum';
                }
            }

            if ($table == 'tbl_toss_database_cs_equipment') {
                $key = $this->db->where('id', $id)->select('item_key')->get($table)->row();
                $res = $this->db->where('item_key', $key->item_key)->select('id,score_evaluasi_dealer')->get($table_fe)->row();

                if ($res->score_evaluasi_dealer == 1) {
                    echo 'sudah';
                } else {
                    echo 'belum';
                }
            }

        } else {
            redirect('errors/code/404');
        }

    }

}
