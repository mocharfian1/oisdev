<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');
class Master_data extends CI_Controller
{
    private $application_module = 'Project (EPM)';
    private $userdata           = null;
    public function __construct()
    {
        parent::__Construct();
        $this->load->model("MMaster_data");
        $this->load->model("MEvaluasi_kunjungan");
        $this->load->model("MLogging");
        $this->userdata = (object) $this->session->userdata();
        $logged_in      = $this->session->userdata('logged_in');
        if (!$logged_in) {
            redirect('login/index');
        }
    }

    public function index()
    {
        redirect('errors/code/404');
    }
    public function insert_data($what_data = null)
    {
        $log_new_value_data = [];
        if ($what_data !== null && !empty($this->input->post())) {

            if ($what_data !== 'area' && $what_data !== 'sub_area' && $what_data !== 'equipment_and_tools_group' && $what_data !== 'equipment_and_tools_item' && $what_data !== 'project' && $what_data !== 'project_area' && $what_data !== 'project_level_training' && $what_data !== 'project_template') {
                redirect('errors/code/404');
            }

            $this->db->trans_begin();

            if ($what_data == 'area') {

                $this->form_validation->set_rules('area_name', 'Area Name', 'required');
                $this->form_validation->set_rules('area_code', 'Area Code', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                    $this->db->trans_commit();
                    exit();
                }

                $data = array(
                    'area_name'   => xss_clean($this->input->post('area_name')),
                    'area_code'   => xss_clean($this->input->post('area_code')),
                    'description' => xss_clean($this->input->post('description')),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('area_name')),
                    1 => xss_clean($this->input->post('area_code')),
                    2 => xss_clean($this->input->post('description')),
                );

                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_area');

                $smodule  = 'back end area';
                $activity = 'Change Master Area';
                $tbl_name = 'tbl_area';

                $action = 'insert area data';

            } else if ($what_data == 'sub_area') {

                $this->form_validation->set_rules('id_area', 'Area Name', 'required');
                $this->form_validation->set_rules('sub_area_name', 'Sub Area Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                    $this->db->trans_commit();
                    exit();
                }

                $data = array(
                    'id_area'       => xss_clean($this->input->post('id_area')),
                    'sub_area_name' => xss_clean($this->input->post('sub_area_name')),
                    'description'   => xss_clean($this->input->post('description')),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('id_area')),
                    1 => xss_clean($this->input->post('sub_area_name')),
                    2 => xss_clean($this->input->post('description')),
                );

                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_sub_area');

                $smodule  = 'back end sub area';
                $activity = 'Change Master Sub Area';
                $tbl_name = 'tbl_sub_area';

                $action = 'insert sub area data';

            } else if ($what_data == 'equipment_and_tools_group') {

                $data = array(
                    'group_name' => xss_clean($this->input->post('group_name')),
                );
                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_equipment_and_tools_group');

                $smodule  = 'back end project';
                $activity = 'Change Master Project';
                $tbl_name = 'tbl_equipment_and_tools_group';

                $action = 'insert eat group data';
            } else if ($what_data == 'equipment_and_tools_item') {

                $data = array(
                    'item_name' => xss_clean($this->input->post('item_name')),
                );
                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_equipment_and_tools_item');

                $smodule  = 'back end project';
                $activity = 'Change Master Project';
                $tbl_name = 'tbl_equipment_and_tools_item';

                $action = 'insert eat group item';
            } else if ($what_data == 'project') {

                $template_set = xss_clean($this->input->post('template_set', 0));
                if ($template_set > 0) {
                    $id_user_level_1 = xss_clean($this->input->post('assign_user_level_1', 0));
                    $id_user_level_2 = xss_clean($this->input->post('assign_user_level_2', 0));
                    $id_user_level_3 = xss_clean($this->input->post('assign_user_level_3', 0));

                    if ($id_user_level_1 != $id_user_level_2 and $id_user_level_2 != $id_user_level_3 and $id_user_level_3 != $id_user_level_1) {

                        $nama_outlet           = xss_clean($this->input->post('nama_outlet'));
                        $main_dealer           = xss_clean($this->input->post('main_dealer'));
                        $fungsi_outlet         = xss_clean($this->input->post('fungsi_outlet'));
                        $type_pembangunan      = xss_clean($this->input->post('type_pembangunan'));
                        $alamat                = xss_clean($this->input->post('alamat'));
                        $jenis_epm             = xss_clean($this->input->post('jenis_epm'));
                        $epm_50                = xss_clean($this->input->post('epm_50'));
                        $epm_75                = xss_clean($this->input->post('epm_75'));
                        $epm_100               = xss_clean($this->input->post('epm_100'));
                        $target_otorisasi      = xss_clean($this->input->post('target_otorisasi'));
                        $tanggal_otorisasi     = xss_clean($this->input->post('tanggal_otorisasi'));
                        $telp_fax              = xss_clean($this->input->post('telp_fax'));
                        $outlet_file_path      = xss_clean($this->input->post('outlet_file_path'));
                        $outlet_file_name      = xss_clean($this->input->post('outlet_file_name'));
                        $outlet_file_true_path = xss_clean($this->input->post('outlet_file_true_path'));
                        $lock_user_level_1     = xss_clean($this->input->post('lock_user_level_1', 0));
                        $lock_user_level_2     = xss_clean($this->input->post('lock_user_level_2', 0));
                        $lock_user_level_3     = xss_clean($this->input->post('lock_user_level_3', 0));
                        $data                  = array(
                            'nama_outlet'               => $nama_outlet,
                            'main_dealer'               => $main_dealer,
                            'fungsi_outlet'             => $fungsi_outlet,
                            'type_pembangunan'          => $type_pembangunan,
                            'alamat'                    => $alamat,
                            'jenis_epm'                 => $jenis_epm,
                            'epm_50_date'               => date('Y-m-d', strtotime($epm_50)),
                            'epm_75_date'               => date('Y-m-d', strtotime($epm_75)),
                            'epm_100_date'              => date('Y-m-d', strtotime($epm_100)),
                            'target_otorisasi'          => date('Y-m-d', strtotime($target_otorisasi)),
                            'id_user_level_1'           => $id_user_level_1,
                            'id_user_level_2'           => $id_user_level_2,
                            'id_user_level_3'           => $id_user_level_3,
                            'lock_user_level_1'         => ($lock_user_level_1 == 1 ? 1 : 0),
                            'lock_user_level_2'         => ($lock_user_level_2 == 1 ? 1 : 0),
                            'lock_user_level_3'         => ($lock_user_level_3 == 1 ? 1 : 0),
                            'id_project_template'       => $template_set,
                            'telp_fax'                  => $telp_fax,
                            'tanggal_otorisasi'         => date('Y-m-d', strtotime($tanggal_otorisasi)),
                            'outlet_pic_file_path'      => $outlet_file_path,
                            'outlet_pic_file_name'      => $outlet_file_name,
                            'outlet_pic_file_true_path' => $outlet_file_true_path,
                        );
                        foreach ($data as $key => $value) {
                            $log_new_value_data[] = $value;
                        }
                        $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_project');

                        $id_project = $this->db->insert_id();
                        $this->MMaster_data->assign_project_template($id_project, $template_set);

                        $smodule  = 'back end project ';
                        $activity = 'Change Master Project';
                        $tbl_name = 'tbl_project';

                        $action = 'insert project data';
                    } else {
                        $this->db->trans_commit();
                        echo json_encode(array("status" => false, 'ket' => 'User Level 1, 2 dan 3 tidak boleh sama !'));
                        exit;

                    }
                } else {
                    $this->db->trans_commit();
                    echo json_encode(array("status" => false, 'ket' => 'Please choose template'));
                    exit;
                }

            } else if ($what_data == 'project_area') {

            } else if ($what_data == 'project_level_training') {

                $this->form_validation->set_rules('level_training_name', 'Level Training Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                    exit();
                }

                $data = array(
                    'level_training_name' => xss_clean($this->input->post('level_training_name')),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('level_training_name')),
                );

                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_project_level_training');

                $smodule  = 'back end project level training';
                $activity = 'Change Master Project Level Training';
                $tbl_name = 'tbl_project_level_training';

                $action = 'insert project level training data';

            } else if ($what_data == 'project_template') {

                $this->form_validation->set_rules('template_name', 'Template Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));

                    exit;
                }

                $data = array(
                    'template_name' => xss_clean($this->input->post('template_name')),
                    'type'          => 'template',
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('template_name')),
                    1 => 'template',
                );

                $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_project');

                $smodule  = 'back end project template';
                $activity = 'Change Master Project Template';
                $tbl_name = 'tbl_sub_area';

                $action = 'insert project template';

            }
            $dberror = $this->db->error();

            if ($this->db->trans_status() === false) {
                if ($dberror['code'] != '0') {
                    // catch error DB

                    $this->db->trans_rollback();

                    //insert insert log
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array("status" => false, 'ket' => $addtional_information));

                } else {

                    $this->db->trans_rollback();

                    //insert insert log
                    $addtional_information = '';
                    $this->MLogging->insert_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                    echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

                }
            } else {
                $this->db->trans_commit();

                //insert insert log
                $addtional_information = '';
                $this->MLogging->insert_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $tbl_name, 'Insert', '', $log_new_value_data);
                echo json_encode(array("status" => true, 'ket' => $action));
            }

        } else {
            redirect('errors/code/404');
        }
    }

    //ordinary master update data
    public function update_data($what_data = null)
    {
        if ($what_data !== null && !empty($this->input->post())) {

            if ($what_data !== 'area' && $what_data !== 'sub_area' && $what_data !== 'project' && $what_data !== 'project_level_training' && $what_data !== 'project_template') {
                redirect('errors/code/404');
            }

            if (empty($this->input->post())) {
                redirect('errors/code/404');
            }

            $id = xss_clean($this->input->post('id'));

            $this->db->trans_begin();

            if ($what_data == 'area') {

                $this->form_validation->set_rules('area_name', 'Area Name', 'required');
                $this->form_validation->set_rules('area_code', 'Area Code', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                    exit();
                }

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get('tbl_area')->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();

                }

                $data = array(
                    'area_name'   => xss_clean($this->input->post('area_name')),
                    'area_code'   => xss_clean($this->input->post('area_code')),
                    'description' => xss_clean($this->input->post('description')),
                );

                $log_old_value_data = array(
                    0 => xss_clean($old_data->area_name),
                    1 => xss_clean($old_data->area_code),
                    2 => xss_clean($old_data->description),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('area_name')),
                    1 => xss_clean($this->input->post('area_code')),
                    2 => xss_clean($this->input->post('description')),
                );

                $update = $this->MMaster_data->ordinary_update($id, $data, 'tbl_area');

                $smodule  = 'back end area';
                $activity = 'Change Master Area';
                $tbl_name = 'tbl_area';

                $action = 'update area data';

            } else if ($what_data == 'sub_area') {

                $this->form_validation->set_rules('id_area', 'Area Name', 'required');
                $this->form_validation->set_rules('sub_area_name', 'Sub Area Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                }

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get('tbl_sub_area')->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();

                }

                $data = array(
                    'id_area'       => xss_clean($this->input->post('id_area')),
                    'sub_area_name' => xss_clean($this->input->post('sub_area_name')),
                    'description'   => xss_clean($this->input->post('description')),
                );

                $log_old_value_data = array(
                    0 => xss_clean($old_data->id_area),
                    1 => xss_clean($old_data->sub_area_name),
                    2 => xss_clean($old_data->description),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('id_area')),
                    1 => xss_clean($this->input->post('sub_area_name')),
                    2 => xss_clean($this->input->post('description')),
                );

                $update = $this->MMaster_data->ordinary_update($id, $data, 'tbl_sub_area');

                $smodule  = 'back end sub area';
                $activity = 'Change Master Sub Area';
                $tbl_name = 'tbl_sub_area';

                $action = 'update sub area data';

            } else if ($what_data == 'project') {
                $old = $this->db->where('id', $id)->get('tbl_project')->row_array();

                $id_user_level_1 = $this->input->post('assign_user_level_1', 0);
                $id_user_level_2 = $this->input->post('assign_user_level_2', 0);
                $id_user_level_3 = $this->input->post('assign_user_level_3', 0);
                if ($id_user_level_1 != $id_user_level_2 and $id_user_level_2 != $id_user_level_3 and $id_user_level_3 != $id_user_level_1) {
                    $nama_outlet           = xss_clean($this->input->post('nama_outlet'));
                    $main_dealer           = xss_clean($this->input->post('main_dealer'));
                    $fungsi_outlet         = xss_clean($this->input->post('fungsi_outlet'));
                    $type_pembangunan      = xss_clean($this->input->post('type_pembangunan'));
                    $alamat                = xss_clean($this->input->post('alamat'));
                    $jenis_epm             = xss_clean($this->input->post('jenis_epm'));
                    $epm_50                = xss_clean($this->input->post('epm_50'));
                    $epm_75                = xss_clean($this->input->post('epm_75'));
                    $epm_100               = xss_clean($this->input->post('epm_100'));
                    $target_otorisasi      = xss_clean($this->input->post('target_otorisasi'));
                    $tanggal_otorisasi     = xss_clean($this->input->post('tanggal_otorisasi'));
                    $telp_fax              = xss_clean($this->input->post('telp_fax'));
                    $outlet_file_path      = xss_clean($this->input->post('outlet_file_path'));
                    $outlet_file_name      = xss_clean($this->input->post('outlet_file_name'));
                    $outlet_file_true_path = xss_clean($this->input->post('outlet_file_true_path'));
                    $lock_user_level_1     = xss_clean($this->input->post('lock_user_level_1', 0));
                    $lock_user_level_2     = xss_clean($this->input->post('lock_user_level_2', 0));
                    $lock_user_level_3     = xss_clean($this->input->post('lock_user_level_3', 0));
                    $data                  = array(
                        'nama_outlet'       => $nama_outlet,
                        'main_dealer'       => $main_dealer,
                        'fungsi_outlet'     => $fungsi_outlet,
                        'type_pembangunan'  => $type_pembangunan,
                        'alamat'            => $alamat,
                        'jenis_epm'         => $jenis_epm,
                        'epm_50_date'       => date('Y-m-d', strtotime($epm_50)),
                        'epm_75_date'       => date('Y-m-d', strtotime($epm_75)),
                        'epm_100_date'      => date('Y-m-d', strtotime($epm_100)),
                        'telp_fax'          => $telp_fax,
                        'tanggal_otorisasi' => date('Y-m-d', strtotime($tanggal_otorisasi)),
                        'target_otorisasi'  => date('Y-m-d', strtotime($target_otorisasi)),
                        'id_user_level_1'   => $id_user_level_1,
                        'id_user_level_2'   => $id_user_level_2,
                        'id_user_level_3'   => $id_user_level_3,
                        'lock_user_level_1' => ($lock_user_level_1 == 1 ? 1 : 0),
                        'lock_user_level_2' => ($lock_user_level_2 == 1 ? 1 : 0),
                        'lock_user_level_3' => ($lock_user_level_3 == 1 ? 1 : 0),
                    );
                    if ($outlet_file_path !== '' && $outlet_file_name !== '' && $outlet_file_true_path !== '') {
                        $data['outlet_pic_file_path']      = $outlet_file_path;
                        $data['outlet_pic_file_name']      = $outlet_file_name;
                        $data['outlet_pic_file_true_path'] = $outlet_file_true_path;

                    }
                    foreach ($old as $key => $value) {
                        $log_old_value_data[] = $value;
                    }
                    foreach ($data as $key => $value) {
                        $log_new_value_data[] = $value;
                    }
                    $insert = $this->MMaster_data->ordinary_update($id, $data, 'tbl_project');

                    $smodule  = 'back end project ';
                    $activity = 'Change Master Project';
                    $tbl_name = 'tbl_project';

                    $action = 'update project data';
                } else {

                    $this->db->trans_commit();
                    echo json_encode(array("status" => false, 'ket' => 'User Level 1,2 dan 3 tidak boleh sama !'));
                    exit();
                }

            } else if ($what_data == 'project_level_training') {

                $this->form_validation->set_rules('level_training_name', 'Level Training Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                    exit();

                }

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get('tbl_project_level_training')->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();
                }

                $data = array(
                    'level_training_name' => xss_clean($this->input->post('level_training_name')),
                );

                $log_old_value_data = array(
                    0 => xss_clean($old_data->level_training_name),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('level_training_name')),
                );

                $update = $this->MMaster_data->ordinary_update($id, $data, 'tbl_project_level_training');

                $smodule  = 'back end project level training';
                $activity = 'Change Master Project Level Training';
                $tbl_name = 'tbl_project_level_training';

                $action = 'update project level training data';

            } else if ($what_data == 'project_template') {

                $this->form_validation->set_rules('template_name', 'Template Name', 'required');

                if ($this->form_validation->run() == false) {
                    $this->db->trans_commit();
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                }

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get('tbl_project')->row();
                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                }

                $data = array(
                    'template_name' => xss_clean($this->input->post('template_name')),
                );

                $log_new_value_data = array(
                    0 => xss_clean($this->input->post('template_name')),
                );

                $log_old_value_data = array(
                    0 => xss_clean($old_data->template_name),
                );

                $update = $this->MMaster_data->ordinary_update($id, $data, 'tbl_project');

                $smodule  = 'back end project template';
                $activity = 'Change Master Project Template';
                $tbl_name = 'tbl_project';

                $action = 'update project template';

            }

            // Logging
            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {

                if ($dberror['code'] != '0') {
                    // catch error DB

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data    = array();
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                    $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Update', '', '');
                    echo json_encode(array("status" => false, 'ket' => $addtional_information));
                } else {

                    $this->db->trans_rollback();

                    //insert update log
                    $log_new_value_data = array();
                    $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $tbl_name, 'Update', '', '');
                    echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

                }

            } else {

                $addtional_information = '';

                $this->db->trans_commit();

                //insert update log
                $this->MLogging->update_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $tbl_name, 'Update', $log_old_value_data, $log_new_value_data);
                echo json_encode(array("status" => true, 'ket' => $action));

            }

        } else {

        }

    }

    //ordinary master delete data
    public function delete_data($what_data = '')
    {
        $isSoftDelete = true;

        if ($what_data == '' || empty($this->input->post())) {

            die(json_encode(array("status" => 'error', 'ket' => 'Incomplete parameter')));

        } else {

            if ($what_data !== 'area' && $what_data !== 'sub_area' && $what_data !== 'equipment_and_tools_group' && $what_data !== 'equipment_and_tools_item' && $what_data !== 'user' && $what_data !== 'project' && $what_data !== 'project_area' && $what_data !== 'project_level_training' && $what_data !== 'project_kpd_picture' && $what_data !== 'project_template' && $what_data !== 'project_template_area') {
                die(json_encode(array("status" => 'error', 'ket' => 'Access Deniad')));
            }

            if (empty($this->input->post())) {
                die(json_encode(array("status" => 'error', 'ket' => 'Incomplete parameter')));
            }

            $this->db->trans_begin();
            $id = xss_clean($this->input->post('id'));

            if ($what_data == 'area') {
                $table_name = 'tbl_area';

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get($table_name)->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();
                }

                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end area';
                $activity     = 'Change Master Area';

            } else if ($what_data == 'sub_area') {

                $table_name = 'tbl_sub_area';

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get($table_name)->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                }

                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end sub area';
                $activity     = 'Change Master Sub Area';

            } else if ($what_data == 'equipment_and_tools_group') {

                $delete = $this->MMaster_data->ordinary_delete($id, 'tbl_equipment_and_tools_group'); //koyoke ga guna

            } else if ($what_data == 'equipment_and_tools_item') {

                $delete = $this->MMaster_data->ordinary_delete($id, 'tbl_equipment_and_tools_item'); //koyoke ga guna

            } else if ($what_data == 'user') {

                $table_name = 'tbl_user';

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get($table_name)->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();
                }

                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end user';
                $activity     = 'Change Master User';

            } else if ($what_data == 'project') {
                $table_name   = 'tbl_project';
                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end project';
                $activity     = 'Change Project Data';
            } else if ($what_data == 'project_area') {
                $table_name   = 'tbl_project_area';
                $delete       = $this->MMaster_data->ordinary_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end project';
                $activity     = 'Change Master Project';
            } else if ($what_data == 'project_level_training') {

                $table_name = 'tbl_project_level_training';

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get($table_name)->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();
                }

                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end project level training';
                $activity     = 'Change Master Project Level Training';

            } else if ($what_data == 'project_kpd_picture') {
                $table_name = 'tbl_project_kpd_pic';
                $delete     = $this->MMaster_data->ordinary_delete($id, 'tbl_project_kpd_pic');

                $isSoftDelete = true;
                $smodule      = 'back end project';
                $activity     = 'Change Master Project';
            } else if ($what_data == 'project_template') {

                $table_name = 'tbl_project';

                $old_data = $this->db->where('id', $id)->where('is_delete', 0)->get($table_name)->row();

                if (!$old_data) {
                    $this->db->trans_commit();
                    die(json_encode(array("status" => false, 'ket' => 'This data has ben deleted by another user')));
                    exit();
                }

                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end project template';
                $activity     = 'Change Master Project Template';

            } else if ($what_data == 'project_template_area') {
                $table_name   = 'tbl_project_area';
                $delete       = $this->MMaster_data->ordinary_soft_delete($id, $table_name);
                $isSoftDelete = true;
                $smodule      = 'back end project template';
                $activity     = 'Change Master Project Template Area';

            }

            $dberror = $this->db->error();

            if ($this->db->trans_status() === false) {

                if ($isSoftDelete) {

                    if ($dberror['code'] != '0') {
                        // catch error DB

                        $this->db->trans_rollback();

                        //insert update log
                        $log_new_value_data    = array();
                        $addtional_information = 'Soft Delete. Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                        $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', $addtional_information, $table_name, 'Update', '', $log_new_value_data);
                        echo json_encode(array("status" => false, 'ket' => 'Hapus data gagal. DB Error.'));

                    } else {

                        $this->db->trans_rollback();

                        //insert update log
                        $addtional_information = 'Soft Delete. Error';
                        $log_new_value_data    = array();
                        $this->MLogging->update_log($smodule, 'Exception', $activity, $this->application_module, 'Error', '', $table_name, 'Update', '', $log_new_value_data);
                        echo json_encode(array("status" => false, 'ket' => 'Hapus data gagal'));

                    }

                } else {

                }
            } else {

                if ($isSoftDelete) {

                    $log_old_value_data = array(
                        $id,
                        '0',
                    );

                    $log_new_value_data = array(
                        $id,
                        '1',
                    );

                    $addtional_information = 'Soft Delete';

                    $this->db->trans_commit();

                    //insert update log
                    $this->MLogging->update_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $table_name, 'Update', $log_old_value_data, $log_new_value_data);
                    echo json_encode(array("status" => true, 'ket' => ''));

                } else {

                    $log_old_value_data = array(
                        $id,
                    );

                    $log_new_value_data = array(
                        $id,
                    );

                    $addtional_information = 'Hard Delete';

                    $this->db->trans_commit();

                    //insert update log
                    $this->MLogging->update_log($smodule, 'Data Change', $activity, $this->application_module, 'Success', $addtional_information, $table_name, 'Delete', $log_old_value_data, $log_new_value_data);
                    echo json_encode(array("status" => true, 'ket' => ''));

                }

            }

        }

    }

    //master area
    public function area($mode = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'admin_project'])) {
            if ($mode !== null) {

                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'area';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {
                    $data['data_area']   = $this->MMaster_data->showAllArea()->result_array();
                    $data['plugin_mode'] = '2';
                } else if ($mode == 'add') {
                    $data['plugin_mode'] = '1';
                } else if ($mode == 'edit') {
                    if (!empty($this->input->post()) && $this->input->post('id') !== null && $this->input->post('id') !== '') {
                        $id                = xss_clean($this->input->post('id'));
                        $data['data_area'] = $this->MMaster_data->area_id($id)->row();

                        if (!$data['data_area']) {
                            redirect('errors/code/404');
                        }

                        $data['plugin_mode'] = '1';
                    } else {
                        redirect('errors/code/404');
                    }

                } else {
                    redirect('errors/code/404');
                }

                $this->load->view('tpl_index', $data);

            } else {
                redirect('errors/code/404');
            }
        } elseif ($this->userdata->user_type == 'tam') {
            redirect('errors/code/404');
        }
    }

    //master sub area
    public function sub_area($mode = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'admin_project'])) {
            if ($mode !== null) {
                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'sub_area';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {
                    $data['data_sub_area'] = $this->MMaster_data->showAllSub()->result_array();

                    $data['plugin_mode'] = '2';
                } else if ($mode == 'add') {
                    $data['data_area']   = $this->MMaster_data->showAllArea()->result_array();
                    $data['plugin_mode'] = '2';
                } else if ($mode == 'edit') {
                    if (!empty($this->input->post()) && $this->input->post('id') !== null && $this->input->post('id') !== '') {
                        $data['data_area'] = $this->MMaster_data->showAllArea()->result_array();

                        $id                    = xss_clean($this->input->post('id'));
                        $data['data_sub_area'] = $this->MMaster_data->sub_area_id($id)->row();

                        if (!$data['data_sub_area']) {
                            redirect('errors/code/404');
                        }

                        $data['plugin_mode'] = '2';
                    } else {
                        redirect('errors/code/404');
                    }
                } else {
                    redirect('errors/code/404');
                }

                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }
        } elseif ($this->userdata->user_type == 'tam') {
            redirect('errors/code/404');
        }

    }

    //master project level training
    public function project_level_training($mode = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($mode !== null) {

                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'project_level_training';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {
                    $data['data_level_training'] = $this->MMaster_data->showAllManPowerLevelTraining()->result_array();
                    $data['plugin_mode']         = '2';
                } else if ($mode == 'add') {
                    $data['plugin_mode'] = '1';
                } else if ($mode == 'edit') {
                    if (!empty($this->input->post()) && $this->input->post('id') !== null && $this->input->post('id') !== '') {
                        $data['plugin_mode']         = '1';
                        $id                          = xss_clean($this->input->post('id'));
                        $data['data_level_training'] = $this->MMaster_data->project_level_training_id($id)->row();
                    } else {
                        redirect('errors/code/404');
                    }
                } else {
                    redirect('errors/code/404');
                }

                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }
    }

    //Master user
    public function user($mode = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'admin_project'])) {
            if ($mode !== null) {
                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'user';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {
                    $data['data_user']   = $this->MMaster_data->showAllUser()->result_array();
                    $data['plugin_mode'] = '2';
                } else if ($mode == 'add') {
                    $data['plugin_mode'] = '1';
                } else if ($mode == 'edit') {
                    if (!empty($this->input->post()) && $this->input->post('id') !== null && $this->input->post('id') !== '') {
                        $id                = xss_clean($this->input->post('id'));
                        $data['data_user'] = $this->MMaster_data->get_user_by_id($id)->row();

                        if (!$data['data_user']) {
                            redirect('errors/code/404');
                        }

                        $data['plugin_mode'] = '1';
                    } else {
                        redirect('errors/code/404');
                    }
                } else {
                    redirect('errors/code/404');
                }

                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }

        } else {
            redirect('errors/code/404');
        }
    }

    public function add_user()
    {

        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'admin_project'])) {
            if (!empty($this->input->post())) {

                $this->form_validation->set_rules('username', 'Username', 'required');
                $this->form_validation->set_rules('user_type', 'User Type', 'required');
                $this->form_validation->set_rules('no_hp', 'No. HP', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == false) {
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                }
                $username    = xss_clean($this->input->post('username'));
                $user_type   = xss_clean($this->input->post('user_type'));
                $no_hp       = xss_clean($this->input->post('no_hp'));
                $description = xss_clean($this->input->post('description'));
                $email       = xss_clean($this->input->post('email'));
                $password    = xss_clean($this->input->post('password'));
                $emailExist  = $this->MMaster_data->emailExist('', $email);

                if ($emailExist) {
                    echo json_encode(array("status" => false, 'ket' => 'Save data failed, email already in used'));
                } else {
                    $data = array(
                        'username'    => $username,
                        'user_type'   => $user_type,
                        'no_hp'       => $no_hp,
                        'description' => $description,
                        'email'       => $email,
                        // 'password'    => sha1($password),
                        'password'    => $password,
                    );

                    $this->db->trans_begin();
                    $insert  = $this->MMaster_data->ordinary_insert($data, 'tbl_user');
                    $dberror = $this->db->error();

                    // Logging
                    if ($this->db->trans_status() === false) {

                        if ($dberror['code'] != '0') {
                            // catch error DB

                            $this->db->trans_rollback();

                            //insert insert log
                            $log_new_value_data    = array();
                            $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                            $this->MLogging->insert_log('back end user', 'Exception', 'Change Master User', $this->application_module, 'Error', $addtional_information, 'tbl_user', 'Insert', '', $log_new_value_data);
                            echo json_encode(array("status" => false, 'ket' => $addtional_information));

                        } else {

                            $this->db->trans_rollback();

                            //insert insert log
                            $log_new_value_data = array();
                            $this->MLogging->insert_log('back end user', 'Exception', 'Change Master User', $this->application_module, 'Error', $addtional_information, 'tbl_user', 'Insert', '', $log_new_value_data);
                            echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

                        }

                    } else {

                        $log_new_value_data = array(
                            0 => xss_clean($username),
                            1 => xss_clean($user_type),
                            2 => xss_clean($no_hp),
                            3 => xss_clean($description),
                            4 => xss_clean($email),
                            5 => xss_clean($password),
                        );

                        $addtional_information = '';

                        $this->db->trans_commit();

                        //insert insert log
                        $this->MLogging->insert_log('back end user', 'Data Change', 'Change Master User', $this->application_module, 'Success', $addtional_information, 'tbl_user', 'Insert', '', $log_new_value_data);
                        echo json_encode(array("status" => true, 'ket' => 'insert user data'));

                    }

                }

            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }

        } else {
            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function update_user()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_toss', 'admin_project'])) {

            if (!empty($this->input->post())) {

                $this->form_validation->set_rules('username', 'Username', 'required');
                $this->form_validation->set_rules('user_type', 'User Type', 'required');
                $this->form_validation->set_rules('no_hp', 'No. HP', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'required');

                if ($this->form_validation->run() == false) {
                    $this->form_validation->set_error_delimiters('- ', '');
                    die(json_encode(array("status" => false, 'ket' => validation_errors())));
                }

                $id          = xss_clean($this->input->post('id'));
                $username    = xss_clean($this->input->post('username'));
                $user_type   = xss_clean($this->input->post('user_type'));
                $no_hp       = xss_clean($this->input->post('no_hp'));
                $description = xss_clean($this->input->post('description'));
                $email       = xss_clean($this->input->post('email'));
                $password    = xss_clean($this->input->post('password'));
                $emailExist  = $this->MMaster_data->emailExist($id, $email);

                if ($emailExist) {
                    echo json_encode(array("status" => false, 'ket' => 'Save data failed, email already in used'));
                } else {
                    //get old data before insert
                    $old_data = $this->db->where('id', $id)->get('tbl_user')->row();

                    $data = array(
                        'username'    => $username,
                        'user_type'   => $user_type,
                        'no_hp'       => $no_hp,
                        'description' => $description,
                        'email'       => $email,
                        // 'password'    => sha1($password),
                        'password'    => $password,
                    );

                    $this->db->trans_begin();
                    $update  = $this->MMaster_data->ordinary_update($id, $data, 'tbl_user');
                    $dberror = $this->db->error();

                    // Logging
                    if ($this->db->trans_status() === false) {

                        if ($dberror['code'] != '0') {
                            // catch error DB

                            $this->db->trans_rollback();

                            //insert update log
                            $log_new_value_data    = array();
                            $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                            $this->MLogging->update_log('back end user', 'Exception', 'Change Master User', $this->application_module, 'Error', $addtional_information, 'tbl_user', 'Update', '', '');
                            echo json_encode(array("status" => false, 'ket' => $addtional_information));
                        } else {

                            $this->db->trans_rollback();

                            //insert update log
                            $log_new_value_data = array();
                            $this->MLogging->update_log('back end user', 'Exception', 'Change Master User', $this->application_module, 'Error', $addtional_information, 'tbl_user', 'Update', '', '');
                            echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

                        }

                    } else {

                        $log_new_value_data = array(
                            0 => xss_clean($username),
                            1 => xss_clean($user_type),
                            2 => xss_clean($no_hp),
                            3 => xss_clean($description),
                            4 => xss_clean($email),
                            5 => xss_clean($password),
                        );

                        $log_old_value_data = array(
                            0 => xss_clean($old_data->username),
                            1 => xss_clean($old_data->user_type),
                            2 => xss_clean($old_data->no_hp),
                            3 => xss_clean($old_data->description),
                            4 => xss_clean($old_data->email),
                            5 => xss_clean($old_data->password),
                        );

                        $addtional_information = '';

                        $this->db->trans_commit();

                        //insert update log
                        $this->MLogging->update_log('back end user', 'Data Change', 'Change Master User', $this->application_module, 'Success', $addtional_information, 'tbl_user', 'Update', $log_old_value_data, $log_new_value_data);
                        echo json_encode(array("status" => true, 'ket' => 'update user data'));

                    }

                }

            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }

        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }

    }

    //Master project
    public function project($mode = null, $id = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($mode !== null) {
                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'project';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {
                    $data['data_project'] = $this->MMaster_data->read_project()->result_array();
                    $data['plugin_mode']  = '2';
                } else if ($mode == 'add') {
                    $data['data_user']     = $this->MMaster_data->list_user(['tam', 'dealer', 'konsultan']);
                    $data['data_template'] = $this->MMaster_data->read_project_template()->result_array();
                    $data['plugin_mode']   = '1';
                } else if ($mode == 'edit') {
                    if ($id !== null) {
                        $data['data_user']     = $this->MMaster_data->list_user(['tam', 'dealer', 'konsultan']);
                        $data['plugin_mode']   = '1';
                        $data['data_template'] = $this->MMaster_data->read_project_template()->result_array();
                        $data['data_project']  = $this->MMaster_data->get_by_id($id)->row();
                    } else {
                        redirect('errors/code/404');
                    }

                } else if ($mode == 'set_area') {

                    if ($id !== null) {

                        $data['id_project']           = $id;
                        $data['data_project']         = $this->MMaster_data->read_project()->result_array();
                        $data['data_current_project'] = $this->MMaster_data->read_project_where($id)->row();
                        //$data['data_set_area'] = $this->MMaster_data->read_project_area_where_id_project($id)->result_array();

                        /*$data['list_area'] = $this->db->query('select ta.*,
                        (select area_name from tbl_area where id = ta.id_area) as area_name,
                        (select sub_area_name from tbl_sub_area where id = ta.id_sub_area) as sub_area_name,
                        (select description from tbl_sub_area where id = ta.id_sub_area) as sub_area_description
                        from tbl_project_area ta where ta.id_project="' . $id . '" and ta.is_delete=0');*/
                        $data['list_area'] = $this->db->select("ta.*,
                              (select area_name from tbl_area where id = ta.id_area) as area_name,
                              (select sub_area_name from tbl_sub_area where id = ta.id_sub_area) as sub_area_name,
                              (select description from tbl_sub_area where id = ta.id_sub_area) as sub_area_description")
                            ->from('tbl_project_area ta')
                            ->where('ta.id_project', $id)
                            ->where('ta.is_delete', 0)->get();

                        $data['plugin_mode'] = '2';

                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'add_set_area') {
                    if ($id !== null) {
                        $data['id_project']  = $id;
                        $data['list_area']   = $this->db->order_by('area_name')->get_where('tbl_area', ['is_delete' => '0']);
                        $data['plugin_mode'] = '1';
                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'sub_area_epm50' || $mode == 'sub_area_epm75' || $mode == 'sub_area_epm100') {

                    if ($id !== null) {
                        // echo 'oeoeoeo';
                        $data['data_project_area'] = $this->MMaster_data->read_project_area_where_id_project_area($id)->result_array();
                        // $data['data_project_area'] = $this->MMaster_data->read_project_area_where_id_project_area($id);
                        // echo $this->db->last_query();
                        // die();
                        $data['data_project_join'] = $this->MMaster_data->read_project_join()->result_array();
                        if ($mode == 'sub_area_epm50') {
                            $data['jenis_epm'] = '50';
                            $jenis             = '50';
                        } else if ($mode == 'sub_area_epm75') {
                            $data['jenis_epm'] = '75';
                            $jenis             = '75';
                        } else {
                            $data['jenis_epm'] = '100';
                            $jenis             = '100';
                        }

                        $data['data_config_ic']       = $this->MMaster_data->read_ic_where($id, $jenis)->result_array();
                        $data['data_config_kp']       = $this->MMaster_data->read_kp_where($id, $jenis)->result_array();
                        $data['data_config_minreq']   = $this->MMaster_data->read_minreq_where($id, $jenis)->result_array();
                        $data['data_config_pic']      = $this->MMaster_data->read_pic_where($id, $jenis)->result_array();
                        $data['data_config_km']       = $this->MMaster_data->read_km_where($id, $jenis)->result_array();
                        $data['data_config_ket']      = $this->MMaster_data->read_ket_where($id, $jenis)->result_array();
                        $data['data_config_tps_line'] = $this->MMaster_data->read_tps_line_where($id, $jenis)->result_array();
                        $data['data_project_catatan'] = $this->MMaster_data->read_project_catatan_where($id, $jenis)->result_array();
                        $data['plugin_mode']          = '1';
                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'equipment_and_tools_configuration_50' || $mode == 'equipment_and_tools_configuration_75' || $mode == 'equipment_and_tools_configuration_100') {
                    if ($id !== null) {
                        if ($mode == 'equipment_and_tools_configuration_50') {$epm = '50';} else if ($mode == 'equipment_and_tools_configuration_75') {$epm = '75';} else { $epm = '100';};

                        $data['id_project']      = $id;
                        $data['jenis_epm']       = $epm;
                        $data['data_project']    = $this->MMaster_data->read_project_where($id)->result_array();
                        $data['data_config_eat'] = $this->MMaster_data->get_project_equipment_and_tools_data($id, $epm)->result_array();
                        $data['plugin_mode']     = '1';

                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'kpd_picture') {
                    if ($id !== null) {
                        $data['id_project']   = $id;
                        $data['data_project'] = $this->MMaster_data->read_project_where($id)->result_array();
                        $data['data_kpd_pic'] = $this->MMaster_data->read_project_kpd_pic_where($id)->result_array();
                        $data['plugin_mode']  = '2';
                    } else {
                        redirect('errors/code/404');
                    }
                } else {
                    redirect('errors/code/404');
                };

                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }

        } else {
            redirect('errors/code/404');
        }
    }

    //Master project template
    public function lock_user()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post())) {
                $id   = xss_clean($this->input->post('id'));
                $type = xss_clean($this->input->post('type'));
                $lock = xss_clean($this->input->post('checked'));
                $lock = ($lock == 'true') ? 1 : 0;
                if ($type != '' and $id != '') {
                    $this->db->where('id', $id)->update('tbl_project', [$type => $lock]);
                    # print_r($this->db);
                }
            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }
    }

    public function project_template($mode = null, $id = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($mode !== null) {
                if ($id == null) {
                    $id = xss_clean($this->input->post('id'));
                }
                $data['menu']     = 'master_data';
                $data['sub_menu'] = 'project_template';
                $data['mode']     = $mode;
                $data['content']  = 'view_master_data';
                $data['tpl_js']   = 'master_data';

                if ($mode == 'view') {

                    $data['plugin_mode']      = '2';
                    $data['project_template'] = $this->db->order_by('id', 'DESC')->get_where('tbl_project', ['type' => 'template', 'is_delete' => '0'])->result_array();
                } else if ($mode == 'add') {

                    $data['plugin_mode'] = '1';
                } else if ($mode == 'edit') {
                    if (!empty($id)) {

                        $data['id']               = $id;
                        $data['plugin_mode']      = '1';
                        $data['project_template'] = $this->db->get_where('tbl_project', ['id' => $id, 'is_delete' => '0'])->row();
                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'set_area') {
                    if (!empty($id)) {
                        $data['id_project']            = $id;
                        $data['data_project_template'] = $this->MMaster_data->read_project_template()->result_array();
                        $data['data_current_project']  = $this->MMaster_data->read_project_where($id)->row();
                        $data['plugin_mode']           = '2';

                        $data['list_area'] = $this->db->select('ta.*,(select area_name from tbl_area where id = ta.id_area) as area_name,(select sub_area_name from tbl_sub_area where id = ta.id_sub_area) as sub_area_name,(select description from tbl_sub_area where id = ta.id_sub_area) as sub_area_description')
                            ->where('ta.id_project', $id)
                            ->where('ta.is_delete', 0)
                            ->get('tbl_project_area ta')->result();

                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'add_set_area') {
                    if (!empty($id)) {
                        $data['id_project']    = $id;
                        $data['template_name'] = xss_clean($this->input->post('template_name'));

                        $data['plugin_mode'] = '1';
                        $data['list_area']   = $this->db->order_by('area_name')->get_where('tbl_area', ['is_delete' => '0']);
                    } else {
                        redirect('errors/code/404');
                    }

                } else if ($mode == 'sub_area_epm50' || $mode == 'sub_area_epm75' || $mode == 'sub_area_epm100') {
                    if (!empty($id)) {
                        $jenis = 0;
                        if ($mode == 'sub_area_epm50') {
                            $data['jenis_epm'] = '50';
                            $jenis             = '50';
                        } else if ($mode == 'sub_area_epm75') {
                            $data['jenis_epm'] = '75';
                            $jenis             = '75';
                        } else {
                            $data['jenis_epm'] = '100';
                            $jenis             = '100';
                        }

                        $data['data_project_area'] = $this->MMaster_data->read_project_area_where_id_project_area($id)->result_array();
                        $data['data_project_join'] = $jenis == 100 ? $this->MMaster_data->read_project_template100_join($jenis)->result_array() : $this->MMaster_data->read_project_template_join($jenis)->result_array();

                        $data['data_config_ic']       = $this->MMaster_data->read_ic_where($id, $jenis)->result_array();
                        $data['data_config_kp']       = $this->MMaster_data->read_kp_where($id, $jenis)->result_array();
                        $data['data_config_minreq']   = $this->MMaster_data->read_minreq_where($id, $jenis)->result_array();
                        $data['data_config_ket']      = $this->MMaster_data->read_ket_where($id, $jenis)->result_array();
                        $data['data_config_pic']      = $this->MMaster_data->read_pic_where($id, $jenis)->result_array();
                        $data['data_config_km']       = $this->MMaster_data->read_km_where($id, $jenis)->result_array();
                        $data['data_config_tps_line'] = $this->MMaster_data->read_tps_line_where($id, $jenis)->result_array();
                        $data['data_project_catatan'] = $this->MMaster_data->read_project_catatan_where($id, $jenis)->result_array();

                        $data['plugin_mode'] = '1';
                    } else {
                        redirect('errors/code/404');
                    }
                } else if ($mode == 'equipment_and_tools_configuration_50' || $mode == 'equipment_and_tools_configuration_75' || $mode == 'equipment_and_tools_configuration_100') {
                    if (!empty($id)) {
                        if ($mode == 'equipment_and_tools_configuration_50') {$epm = '50';} else if ($mode == 'equipment_and_tools_configuration_75') {$epm = '75';} else { $epm = '100';};

                        $data['id_project'] = $id;
                        $data['jenis_epm']  = $epm;

                        $data['data_project']    = $this->MMaster_data->read_project_where($id)->result_array();
                        $data['data_config_eat'] = $this->MMaster_data->get_project_equipment_and_tools_data($id, $epm)->result_array();

                        $data['plugin_mode'] = '1';
                    } else {
                        redirect('errors/code/404');
                    }
                } else {
                    redirect('errors/code/404');
                };

                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }

        } else {
            redirect('errors/code/404');
        }
    }

    public function get_subarea_by_area($id_area = null)
    {
#        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

        if ($id_area !== null) {
            $data = $this->db->get_where('tbl_sub_area', ['id_area' => $id_area, 'is_delete' => '0']);
            if ($data->num_rows() > 0) {
                echo json_encode($data->result());
            } else {
                echo null;
            }
        } else {
            echo null;
        }

        #       } elseif (in_array($this->userdata->user_type, ['tam', 'admin_toss'])) {
        #         redirect('errors/code/404');
        #      }
    }

    //this is for template
    public function save_project_template_area()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post())) {
                $id_project  = xss_clean($this->input->post('id_project'));
                $id_area     = xss_clean($this->input->post('id_area'));
                $id_sub_area = xss_clean($this->input->post('id_sub_area'));
                $position    = xss_clean($this->input->post('position'));
                $data        = array(
                    'id_project'  => $id_project,
                    'id_area'     => $id_area,
                    'id_sub_area' => $id_sub_area,
                    'position'    => $position,
                );

                $check = $this->db->get_where('tbl_project_area', [
                    'id_project'  => $id_project,
                    'id_area'     => $id_area,
                    'id_sub_area' => $id_sub_area,
                    'is_delete'   => '0']);
                if ($check->num_rows() > 0) {
                    echo json_encode(array("status" => false, 'ket' => 'Data already exist'));
                } else {
                    $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_project_area');

                    echo json_encode(array("status" => true, 'ket' => 'Insert Project Area'));
                }
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }

        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    // PROJECT AREA

    public function add_set_area($id = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($id !== null) {
                $data['menu']          = 'master_data';
                $data['sub_menu']      = 'project';
                $data['mode']          = 'add_area';
                $data['content']       = 'view_master_data';
                $data['tpl_js']        = 'master_data';
                $data['id_project']    = $id;
                $data['data_area']     = $this->MMaster_data->read_area()->result_array();
                $data['data_sub_area'] = $this->MMaster_data->read_sub_area($id)->result_array();

                $data['plugin_mode'] = '1';
                $this->load->view('tpl_index', $data);
            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }
    }

    public function add_project_area()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post())) {
                $id   = $this->input->post('id_project');
                $data = array(
                    'id_project'  => $id,
                    'id_area'     => xss_clean($this->input->post('id_area')),
                    'id_sub_area' => xss_clean($this->input->post('id_sub_area')),
                    'position'    => xss_clean($this->input->post('position')),
                );
                $insert = $this->MMaster_data->add_project_area($data);

                $this->MLogging->insert_log('back end user', 'Data Change', 'Change Master User', $this->application_module, 'Success', $addtional_information, 'tbl_user', 'Insert', '', $log_new_value_data);
                //
                //tambahkan catatan
                $id_project_area = $this->db->insert_id();

                //looping untuk tiap user type
                for ($x = 0; $x < 3; $x++) {

                    if ($x == 0) {$user_type = 'konsultan';};
                    if ($x == 1) {$user_type = 'tam';};
                    if ($x == 2) {$user_type = 'dealer';};

                    //looping untuk tiap jenis EPM
                    for ($i = 0; $i < 3; $i++) {
                        if ($i == 0) {$jenis_epm = '50';};
                        if ($i == 1) {$jenis_epm = '75';};
                        if ($i == 2) {$jenis_epm = '100';};

                        $data = array(
                            'id_project_area' => $id_project_area,
                            'jenis_epm'       => $jenis_epm,
                            'user_type'       => $user_type,
                        );

                        $insertCatatan = $this->MMaster_data->add_project_catatan($data);
                        $insertCatatan = $this->db->insert_id();
                        // Insert Detail Kunjungan Evaluasi
                        $dataEvalKunjungan = $this->MEvaluasi_kunjungan->read_eval($id, $jenis_epm)->result_array();
                        if (count($dataEvalKunjungan) > 0) {
                            $data_eval = array(
                                'id_project_catatan'    => $insertCatatan,
                                'id_evaluasi_kunjungan' => $dataEvalKunjungan[0]['id'],
                                'id_project_area'       => $id_project_area,
                            );
                            $this->MEvaluasi_kunjungan->add_detail_evaluasi($data_eval);
                        };
                    }

                }

                echo json_encode(array("status" => true, "ket" => 'insert project area data', "id" => $id));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function delete_project_area($id = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($id !== null) {
                $data = array(
                    'is_delete' => 1,
                );
                $this->MMaster_data->delete_project_area_by_id($id, $data);

                //
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    // PROJECT SET AREA
    public function set_area_complete_for_project()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post())) {

                $menu                    = xss_clean($this->input->post('menu'));
                $id_project_current      = xss_clean($this->input->post('id_project_current'));
                $id_project_get_set_area = xss_clean($this->input->post('id_project_get_set_area'));

                //get project area
                $current_project_area = $this->MMaster_data->read_project_area_where_id_project($id_project_current)->result_array();
                $get_set_project_area = $this->MMaster_data->read_project_area_where_id_project($id_project_get_set_area)->result_array();

                //cleaning up current data
                for ($x = 0; $x < count($current_project_area); $x++) {
                    //delete current project catatan slot & content
                    $this->MMaster_data->delete_all_project_catatan_where_id_pa($current_project_area[$x]['id']);

                    //delete current project sa config ic
                    $this->MMaster_data->delete_all_project_sa_config_ic_where_id_pa($current_project_area[$x]['id']);

                    //delete current project sa config ket
                    $this->MMaster_data->delete_all_project_sa_config_ket_where_id_pa($current_project_area[$x]['id']);

                    //delete current project sa config kp
                    $this->MMaster_data->delete_all_project_sa_config_kp_where_id_pa($current_project_area[$x]['id']);

                    //delete current project sa config minreq
                    $this->MMaster_data->delete_all_project_sa_config_minreq_where_id_pa($current_project_area[$x]['id']);

                    $this->MMaster_data->delete_all_project_sa_config_tps_line_where_id_pa($current_project_area[$x]['id']);

                    $this->MMaster_data->delete_all_project_sa_config_km_where_id_pa($current_project_area[$x]['id']);

                    $this->MMaster_data->delete_all_project_sa_config_pic_where_id_pa($current_project_area[$x]['id']);

                    //delete current project area pic (only data, not file)
                    $this->MMaster_data->delete_all_project_area_pic_where_id_pa($current_project_area[$x]['id']);

                    //delete current project area
                    $this->MMaster_data->delete_all_project_area_where_id_p($id_project_current);
                }

                //duplicating data from project
                for ($y = 0; $y < count($get_set_project_area); $y++) {
                    //insert project area dan catatan to current project
                    $data = array(
                        'id_project'  => $id_project_current,
                        'id_area'     => $get_set_project_area[$y]['id_area'],
                        'id_sub_area' => $get_set_project_area[$y]['id_sub_area'],
                        'position'    => $get_set_project_area[$y]['position'],
                    );
                    $insert = $this->MMaster_data->add_project_area($data);

                    //tambahkan catatan
                    $id_project_area_new = $this->db->insert_id();

                    //looping untuk tiap user type
                    for ($x = 0; $x < 3; $x++) {

                        if ($x == 0) {$user_type = 'konsultan';};
                        if ($x == 1) {$user_type = 'tam';};
                        if ($x == 2) {$user_type = 'dealer';};

                        //looping untuk tiap jenis EPM
                        for ($i = 0; $i < 3; $i++) {
                            if ($i == 0) {$jenis_epm = '50';};
                            if ($i == 1) {$jenis_epm = '75';};
                            if ($i == 2) {$jenis_epm = '100';};

                            $data = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'user_type'       => $user_type,
                            );

                            $insertCatatan = $this->MMaster_data->add_project_catatan($data);

                        }

                    }
                    //end of insert project area dan catatan

                    //insert config item checklist to current project
                    //looping untuk tiap jenis EPM
                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_ic = $this->MMaster_data->get_config_ic($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        for ($i = 0; $i < count($data_config_ic); $i++) {
                            $dataic = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'model'           => $data_config_ic[$i]['model'],
                                'position'        => $data_config_ic[$i]['position'],
                                'item_name'       => $data_config_ic[$i]['item_name'],
                                'kriteria'        => $data_config_ic[$i]['kriteria'],
                            );

                            $insert = $this->MMaster_data->add_project_config_ic($dataic);
                        }
                    }

                    //insert config keterangan to current project
                    //looping untuk tiap jenis EPM
                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_ket = $this->MMaster_data->get_config_ket($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        for ($i = 0; $i < count($data_config_ket); $i++) {
                            $dataket = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'position'        => $data_config_ket[$i]['position'],
                                'item_ket'        => $data_config_ket[$i]['item_ket'],
                            );
                            $insert = $this->MMaster_data->add_project_config_ket($dataket);
                        }
                    }

                    //insert config kriteria pekerjaan to current project
                    //looping untuk tiap jenis EPM
                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_kp = $this->MMaster_data->get_config_kp($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        for ($i = 0; $i < count($data_config_kp); $i++) {
                            $datakp = array(
                                'id_project_area'         => $id_project_area_new,
                                'jenis_epm'               => $jenis_epm,
                                'position'                => $data_config_kp[$i]['position'],
                                'item_kriteria_pekerjaan' => $data_config_kp[$i]['item_kriteria_pekerjaan'],
                            );

                            $insert = $this->MMaster_data->add_project_config_kp($datakp);
                        }
                    }

                    //insert config minimum requirement to current project
                    //looping untuk tiap jenis EPM
                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_minreq = $this->MMaster_data->get_config_minreq($jenis_epm, $get_set_project_area[$y]['id'])->result_array();

                        for ($i = 0; $i < count($data_config_minreq); $i++) {

                            $dataminreq = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'position'        => $data_config_minreq[$i]['position'],
                                'item_minreq'     => $data_config_minreq[$i]['item_minreq'],
                            );
                            $insert = $this->MMaster_data->add_project_config_minreq($dataminreq);
                        }
                    }

                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_minreq = $this->MMaster_data->get_config_tps_line($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        $count              = 0;
                        for ($i = 0; $i < count($data_config_minreq); $i++) {
                            $count++;
                            $dataminreq = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'position'        => $data_config_minreq[$i]['position'],
                                'item_tps_line'   => $data_config_minreq[$i]['item_tps_line'],
                            );
                            if ($count > 0) {
                                $insert = $this->MMaster_data->add_project_config_tps_line($dataminreq);
                            }

                        }
                    }

                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_minreq = $this->MMaster_data->get_config_km($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        $count              = 0;
                        for ($i = 0; $i < count($data_config_minreq); $i++) {
                            $count++;
                            $dataminreq = array(
                                'id_project_area'       => $id_project_area_new,
                                'position'              => $data_config_minreq[$i]['position'],
                                'model'                 => $data_config_minreq[$i]['model'],
                                'item_kriteria_minimum' => $data_config_minreq[$i]['item_kriteria_minimum'],
                            );
                            if ($count > 0) {
                                $insert = $this->MMaster_data->add_project_config_km($dataminreq);
                            }

                        }
                    }

                    for ($a = 0; $a < 3; $a++) {
                        if ($a == 0) {$jenis_epm = '50';};
                        if ($a == 1) {$jenis_epm = '75';};
                        if ($a == 2) {$jenis_epm = '100';};

                        $data_config_minreq = $this->MMaster_data->get_config_pic($jenis_epm, $get_set_project_area[$y]['id'])->result_array();
                        $count              = 0;
                        for ($i = 0; $i < count($data_config_minreq); $i++) {
                            $count++;
                            $dataminreq = array(
                                'id_project_area' => $id_project_area_new,
                                'jenis_epm'       => $jenis_epm,
                                'position'        => $data_config_minreq[$i]['position'],
                                'item_pic'        => $data_config_minreq[$i]['item_pic'],
                            );

                            if ($count > 0) {
                                $insert = $this->MMaster_data->add_project_config_pic($dataminreq);
                            }
                        }
                    }

                }

                return redirect('master_data/' . (empty($menu) ? 'project' : 'project_template') . '/set_area/' . $id_project_current);

            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }

    }

    // PROJECT AREA SET IC, KP, MINREQ
    public function set_config_for_project_area()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post())) {
                $menu               = xss_clean($this->input->post('menu'));
                $input              = xss_clean($this->input->post('config'));
                $id_project_areanya = xss_clean($this->input->post('id_project_areanya'));
                $jenis_epmnya       = xss_clean($this->input->post('jenis_epmnya'));
                $id_projectnya      = xss_clean($this->input->post('id_projectnya'));
                $input              = xss_clean($this->input->post('config'));
                $jenisid            = explode(" ", $input);
                $jenis              = $jenisid['0'];
                $id_project_area    = $jenisid['1'];

                // GET CONFIG IC
                $data_config_ic   = $this->MMaster_data->get_config_ic($jenis, $id_project_area)->result_array();
                $delete_config_ic = $this->MMaster_data->delete_config_ic($id_project_areanya, $jenis_epmnya);
                for ($i = 0; $i < count($data_config_ic); $i++) {
                    $dataic = array(
                        'id_project_area' => $id_project_areanya,
                        'jenis_epm'       => $jenis_epmnya,
                        'model'           => $data_config_ic[$i]['model'],
                        'position'        => $data_config_ic[$i]['position'],
                        'item_name'       => $data_config_ic[$i]['item_name'],
                        'kriteria'        => $data_config_ic[$i]['kriteria'],
                    );

                    $insert = $this->MMaster_data->add_project_config_ic($dataic);
                }
                // GET CONFIG MINREQ
                $data_config_minreq   = $this->MMaster_data->get_config_minreq($jenis, $id_project_area)->result_array();
                $delete_config_minreq = $this->MMaster_data->delete_config_minreq($id_project_areanya, $jenis_epmnya);
                for ($i = 0; $i < count($data_config_minreq); $i++) {
                    $dataminreq = array(
                        'id_project_area' => $id_project_areanya,
                        'jenis_epm'       => $jenis_epmnya,
                        'position'        => $data_config_minreq[$i]['position'],
                        'item_minreq'     => $data_config_minreq[$i]['item_minreq'],
                    );

                    $insert = $this->MMaster_data->add_project_config_minreq($dataminreq);
                }
                // GET CONFIG KP
                $data_config_kp   = $this->MMaster_data->get_config_kp($jenis, $id_project_area)->result_array();
                $delete_config_kp = $this->MMaster_data->delete_config_kp($id_project_areanya, $jenis_epmnya);
                for ($i = 0; $i < count($data_config_kp); $i++) {
                    $datakp = array(
                        'id_project_area'         => $id_project_areanya,
                        'jenis_epm'               => $jenis_epmnya,
                        'position'                => $data_config_kp[$i]['position'],
                        'item_kriteria_pekerjaan' => $data_config_kp[$i]['item_kriteria_pekerjaan'],
                    );

                    $insert = $this->MMaster_data->add_project_config_kp($datakp);
                }

                // GET CONFIG KET
                $data_config_ket   = $this->MMaster_data->get_config_ket($jenis, $id_project_area)->result_array();
                $delete_config_ket = $this->MMaster_data->delete_config_ket($id_project_areanya, $jenis_epmnya);
                for ($i = 0; $i < count($data_config_ket); $i++) {
                    $dataket = array(
                        'id_project_area' => $id_project_areanya,
                        'jenis_epm'       => $jenis_epmnya,
                        'position'        => $data_config_ket[$i]['position'],
                        'item_ket'        => $data_config_ket[$i]['item_ket'],
                    );
                    $insert = $this->MMaster_data->add_project_config_ket($dataket);
                }
                if ($jenis_epmnya == '75') {

                    // GET CONFIG KET
                    $data_config_ket   = $this->MMaster_data->get_config_tps_line($jenis, $id_project_area)->result_array();
                    $delete_config_ket = $this->MMaster_data->delete_config_tps($id_project_areanya, $jenis_epmnya);
                    for ($i = 0; $i < count($data_config_ket); $i++) {
                        $dataket = array(
                            'id_project_area' => $id_project_areanya,
                            'jenis_epm'       => $jenis_epmnya,
                            'position'        => $data_config_ket[$i]['position'],
                            'item_tps_line'   => $data_config_ket[$i]['item_tps_line'],
                        );
                        $insert = $this->MMaster_data->add_project_config_tps_line($dataket);
                    }
                }
                if ($jenis_epmnya == '100') {

                    // GET CONFIG KET
                    $data_config_ket   = $this->MMaster_data->get_config_km($jenis, $id_project_area)->result_array();
                    $delete_config_ket = $this->MMaster_data->delete_config_km($id_project_areanya, $jenis_epmnya);
                    for ($i = 0; $i < count($data_config_ket); $i++) {
                        $dataket = array(
                            'id_project_area'       => $id_project_areanya,
                            'position'              => $data_config_ket[$i]['position'],
                            'model'                 => $data_config_ket[$i]['model'],
                            'item_kriteria_minimum' => $data_config_ket[$i]['item_kriteria_minimum'],
                        );
                        $insert = $this->MMaster_data->add_project_config_km($dataket);
                    }
                    $data_config_ket   = $this->MMaster_data->get_config_pic($jenis, $id_project_area)->result_array();
                    $delete_config_ket = $this->MMaster_data->delete_config_pic($id_project_areanya, $jenis_epmnya);
                    for ($i = 0; $i < count($data_config_ket); $i++) {
                        $dataket = array(
                            'id_project_area' => $id_project_areanya,
                            'jenis_epm'       => $jenis_epmnya,
                            'position'        => $data_config_ket[$i]['position'],
                            'item_pic'        => $data_config_ket[$i]['item_pic'],
                        );
                        $insert = $this->MMaster_data->add_project_config_pic($dataket);
                    }
                }
                $this->session->set_flashdata('info', "Data konfigurasi berhasil ditambahkan!");

                return redirect('master_data/' . (empty($menu) ? 'project' : 'project_template') . '/set_area/' . $id_projectnya);
            } else {
                redirect('errors/code/404');
            }
        } else {
            redirect('errors/code/404');
        }
    }

    public function add_config_eat()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_ic = $this->MMaster_data->delete_config_eat($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataeat = array(
                        'id_project' => $data['id'],
                        'jenis_epm'  => $data['jenis'],
                        'position'   => $row['0'],
                        'group_name' => $row['1'],
                        'item_name'  => $row['2'],
                    );

                    //
                    $insert = $this->MMaster_data->add_project_config_eat($dataeat);

                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_ic()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_ic = $this->MMaster_data->delete_config_ic($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataic = array(
                        'id_project_area' => $data['id'],
                        'jenis_epm'       => $data['jenis'],
                        'model'           => $data['item_checklist_model'],
                        'position'        => $row['0'],
                        'item_name'       => $row['1'],
                        'kriteria'        => $row['2'],
                    );
                    $insert = $this->MMaster_data->add_project_config_ic($dataic);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_kp()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_kp = $this->MMaster_data->delete_config_kp($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $datakp = array(
                        'id_project_area'         => $data['id'],
                        'jenis_epm'               => $data['jenis'],
                        'position'                => $row['0'],
                        'item_kriteria_pekerjaan' => $row['1'],
                        'keterangan' => $row['2'],
                    );
                    $insert = $this->MMaster_data->add_project_config_kp($datakp);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_minreq()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);

                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_minreq = $this->MMaster_data->delete_config_minreq($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataminreq = array(
                        'id_project_area' => $data['id'],
                        'jenis_epm'       => $data['jenis'],
                        'position'        => $row['0'],
                        'item_minreq'     => $row['1'],
                    );
                    $insert = $this->MMaster_data->add_project_config_minreq($dataminreq);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_ket()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_ket = $this->MMaster_data->delete_config_ket($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataket = array(
                        'id_project_area' => $data['id'],
                        'jenis_epm'       => $data['jenis'],
                        'position'        => $row['0'],
                        'item_ket'        => $row['1'],
                    );
                    $insert = $this->MMaster_data->add_project_config_ket($dataket);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_tps_line()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_tps = $this->MMaster_data->delete_config_tps($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataket = array(
                        'id_project_area' => $data['id'],
                        'jenis_epm'       => $data['jenis'],
                        'position'        => $row['0'],
                        'item_tps_line'   => $row['1'],
                    );
                    $insert = $this->MMaster_data->add_project_config_tps($dataket);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_pic()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_tps = $this->MMaster_data->delete_config_pic($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataket = array(
                        'id_project_area' => $data['id'],
                        'jenis_epm'       => $data['jenis'],
                        'position'        => $row['0'],
                        'item_pic'        => $row['1'],
                    );
                    $insert = $this->MMaster_data->add_project_config_pic($dataket);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function add_config_sub_area_km()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($this->input->post('datanya'))) {
                $data = xss_clean($this->input->post('datanya'));
                $data = stripcslashes($data);
                $data = json_decode($data, true);

                //clear data before insert
                $delete_config_tps = $this->MMaster_data->delete_config_km($data['id'], $data['jenis']);

                foreach ($data['data'] as $row) {
                    $dataket = array(
                        'id_project_area'       => $data['id'],
                        'model'                 => $data['item_checklist_model'],
                        'position'              => $row['0'],
                        'item_kriteria_minimum' => $row['1'],
                    );
                    $insert = $this->MMaster_data->add_project_config_km($dataket);
                };
                echo json_encode(array("status" => true));
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

    public function file_upload_outlet()
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if (!empty($_FILES['imgInput2']['name'])) {
                $path                  = FCPATH . 'upload/outlet/'; // path folder
                $config['upload_path'] = $path; // variabel path untuk config upload

                $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
                $config['max_size']         = '2048000'; // maksimum besar file 20M
                $config['file_ext_tolower'] = true;
                // $config['file_name']     = 'gallery';
                $config['encrypt_name'] = true;

                // $this->load->library('upload', $config);
                $this->load->library('upload');
                $this->upload->initialize($config);

                if (!is_writable($path)) {
                    //folder not writable

                    $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

                } else {
                    //folder writable
                    if (!$this->upload->do_upload('imgInput2')) //element name here
                    {
                        $response = array('status' => 'error', 'message' => 'Image upload failed, error : ' . $this->upload->display_errors());
                    } else {
                        $data = array('upload_data' => $this->upload->data());

                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $file_name   = $upload_data['file_name'];

                        //filepath URL
                        $fileUrlPath = base_url('upload/outlet/' . $file_name);

                        $app_status = APP_STATUS;
                        if ($app_status == 'local') {
                            $fileTruePath = FCPATH . "upload/outlet\\" . $file_name; //for local develop
                        } else if ($app_status == 'live') {
                            $fileTruePath = FCPATH . "upload/outlet/" . $file_name; //for hosting upload
                        }

                        $response = array('status' => 'success', 'message' => 'File upload success', 'file_name' => $file_name, 'file_path' => $fileUrlPath, 'file_true_path' => $fileTruePath);
                    }
                }
            } else {
                $response = array('status' => 'error', 'message' => 'Image upload failed, error : file not exist');
            }

            echo json_encode($response);

        } else {

            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }
    public function file_upload_kpd($id_project = null)
    {
        if (in_array($this->userdata->user_type, ['superadmin', 'admin_project'])) {

            if ($id_project !== null) {
                if (!empty($_FILES['imgInputKpd']['name'])) {
                    $path                  = FCPATH . 'upload/kpd/'; // path folder
                    $config['upload_path'] = $path; // variabel path untuk config upload

                    $config['allowed_types']    = 'jpg|jpeg|png'; // type yang dapat diakses bisa anda sesuaikan
                    $config['max_size']         = '2048000'; // maksimum besar file 20M
                    $config['file_ext_tolower'] = true;
                    // $config['file_name']     = 'gallery';
                    $config['encrypt_name'] = true;

                    // $this->load->library('upload', $config);
                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!is_writable($path)) {
                        //folder not writable

                        $response = array('status' => 'error', 'message' => 'Can`t upload File; no write Access');

                    } else {
                        //folder writable
                        if (!$this->upload->do_upload('imgInputKpd')) //element name here
                        {
                            $response = array('status' => 'error', 'message' => 'Image upload failed, error : ' . $this->upload->display_errors());
                        } else {
                            $data = array('upload_data' => $this->upload->data());

                            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                            $file_name   = $upload_data['file_name'];

                            //filepath URL
                            $fileUrlPath = base_url('upload/kpd/' . $file_name);

                            $app_status = APP_STATUS;
                            if ($app_status == 'local') {
                                $fileTruePath = FCPATH . "upload/kpd\\" . $file_name; //for local develop
                            } else if ($app_status == 'live') {
                                $fileTruePath = FCPATH . "upload/kpd/" . $file_name; //for hosting upload
                            }

                            $data = array(
                                'id_project'     => $id_project,
                                'file_path'      => $fileUrlPath,
                                'file_true_path' => $fileTruePath,
                                'file_name'      => $file_name,
                            );

                            $insert = $this->MMaster_data->ordinary_insert($data, 'tbl_project_kpd_pic');

                            $response = array('status' => 'success', 'message' => 'File upload success', 'file_name' => $file_name, 'file_path' => $fileUrlPath, 'file_true_path' => $fileTruePath);
                        }
                    }
                } else {
                    $response = array('status' => 'error', 'message' => 'Image upload failed, error : file not exist');
                }

                echo json_encode($response);
            } else {
                $response = array('status' => 'error', 'message' => 'Incomplete parameter');
                echo json_encode($response);
            }
        } else {
            $response = array('status' => 'error', 'message' => "Access denied");
            echo json_encode($response);
        }
    }

}
