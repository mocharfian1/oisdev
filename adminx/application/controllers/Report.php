<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model("MReport");
        $this->load->model("MMaster_data");
        $this->load->model("MEvaluasi_kunjungan");
    }

    public function index()
    {
        redirect('errors/code/404');
    }
    private function load_data($id_project, $jenis_epm)
    {
        $data                   = $this->MReport->data_project($id_project)->row_array();
        $dataArea               = $this->MReport->data_project_area($id_project)->result_array();
        $dataCatatan            = $this->MReport->data_project_catatan($id_project, $jenis_epm)->result_array();
        $dataItemChecklist      = $this->MReport->read_ic_where($id_project, $jenis_epm)->result_array();
        $dataKriteriaPekerjaan  = $this->MReport->read_kp_where($id_project, $jenis_epm)->result_array();
        $dataMinimumRequirement = $this->MReport->read_minreq_where($id_project, $jenis_epm)->result_array();
        $dataKet                = $this->MReport->read_ket_where($id_project, $jenis_epm)->result_array();
        $dataTpsLine            = $this->MReport->read_tps_line_where($id_project, $jenis_epm)->result_array();
        $dataProjectAreaPic     = $this->MReport->read_project_area_pic($id_project, $jenis_epm)->result_array();
        $dataProjectEat         = $this->MReport->data_project_eat($id_project, $jenis_epm)->result_array();
        $dataManPower           = $this->MReport->read_project_man_power($id_project, $jenis_epm)->result_array();

        $dataKm                  = $this->MReport->read_km_where($id_project, $jenis_epm)->result_array();
        $dataProjectConfigPic    = $this->MReport->data_project_config_pic($id_project, $jenis_epm)->result_array();
        $dataTargetInvestasi     = $this->MReport->read_project_target_investasi($id_project)->row_array();
        $data_sdm_sales          = $this->MReport->read_project_sdm_sales($id_project)->result_array();
        $data_sdm_after_sales    = $this->MReport->read_project_sdm_after_sales($id_project)->result_array();
        $data_ex                 = $this->MReport->read_ex($id_project)->row_array();
        $data_project_pic        = $this->MReport->read_project_pic($id_project)->result_array();
        $data_area               = [];
        $data_sub_area           = [];
        $data['eat']             = $dataProjectEat;
        $data['manpower']        = $dataManPower;
        $data['ti']              = $dataTargetInvestasi;
        $data['sdm_sales']       = $data_sdm_sales;
        $data['sdm_after_sales'] = $data_sdm_after_sales;
        $data['ex']              = $data_ex;
        $data['project_pic']     = $data_project_pic;
        $data_area               = [];
        $data_sub_area           = [];
        foreach ($dataArea as $row) {
            $data_sub_area[$row['id']] = $row;
        }
        foreach ($dataItemChecklist as $row) {
            $data_sub_area[$row['id_project_area']]['ic'][] = $row;
        }
        foreach ($dataCatatan as $row) {
            $data_sub_area[$row['id_project_area']]['catatan'][$row['user_type']] = $row['catatan'];
        }
        foreach ($dataKriteriaPekerjaan as $row) {
            $data_sub_area[$row['id_project_area']]['kp'][] = $row;
        }
        foreach ($dataMinimumRequirement as $row) {
            $data_sub_area[$row['id_project_area']]['minreq'][] = $row;
        }
        foreach ($dataProjectAreaPic as $row) {
            $data_sub_area[$row['id_project_area']]['pic'][] = $row;
        }
        #print_r($dataProjectAreaPic);
        foreach ($dataKet as $row) {
            $data_sub_area[$row['id_project_area']]['ket'][] = $row;
        }
        foreach ($dataTpsLine as $row) {
            $data_sub_area[$row['id_project_area']]['tps_line'][] = $row;
        }
        foreach ($dataArea as $row) {
            $data_area[$row['id_area']] = $row;
        }

        foreach ($dataKm as $row) {
            $data_sub_area[$row['id_project_area']]['km'][] = $row;
        }

        foreach ($dataProjectConfigPic as $row) {
            $data_sub_area[$row['id_project_area']]['config_pic'][] = $row;
        }

        foreach ($data_sub_area as $row) {
            if (isset($row['id_area'])) {
                $data_area[$row['id_area']]['sub'][$row['id']] = $row;
            }

        }
        $data['jenis_epm'] = $jenis_epm;
        $data['data_area'] = $data_area;
        $babroman          = [];
        $i                 = 0;
        $bab               = 3;
        foreach ($data_area as $id_area => $n) {
            $bab++;
            $babroman[$id_area] = $this->numberToRomanRepresentation($bab);
        }
        $data['babroman'] = $babroman;
        $data['lastbab']  = $this->numberToRomanRepresentation($bab + 1);
        return $data;
    }
    public function print_check()
    {
        $id_project = xss_clean($this->input->post('id_project'));
        $jenis_epm  = xss_clean($this->input->post('jenis_epm'));
        $data       = $this->load_data($id_project, $jenis_epm);
        $this->load->view('print_check', $data);

    }
    public function print_report()
    {
        $id_project = xss_clean($this->input->post('id_project'));
        $jenis_epm  = xss_clean($this->input->post('jenis_epm'));
        $data       = $this->load_data($id_project, $jenis_epm);
        $this->load->view('print_report', $data);
    }
    public function print_report_100()
    {
        $id_project        = xss_clean($this->input->post('id_project'));
        $jenis_epm         = xss_clean($this->input->post('jenis_epm'));
        $page              = xss_clean($this->input->post('page'));
        $data              = $this->load_data($id_project, $jenis_epm);
        $data['page']      = $page;
        $data['jenis_epm'] = $jenis_epm;
        $this->load->view('print_report_100', $data);
    }
    public function view_report($sub_menu)
    {
        $data['menu']         = 'report';
        $data['sub_menu']     = $sub_menu;
        $data['content']      = 'view_report';
        $data['plugin_mode']  = '1';
        $data['data_project'] = $this->MReport->read_data_project()->result_array();
        if ($sub_menu == 'buku_epm') {
            $data['tpl_js'] = 'report_buku_epm';
        } elseif ($sub_menu == 'catatan_evaluasi') {
            $data['data_evaluasi'] = $this->MEvaluasi_kunjungan->showAllEvaluasi()->result_array();
            $data['tpl_js']        = 'report_catatan_evaluasi';
        } elseif ($sub_menu == 'checklist_summary') {
            $data['tpl_js'] = 'report_checklist_summary';
        } elseif ($sub_menu == 'toss') {
            $data['tpl_js']    = 'report_toss';
            $data['ls_dealer'] = $this->MReport->ls_dealer();
        } else {
            redirect('errors/code/404');
        }

        $this->load->view('tpl_index', $data);
    }

    public function get_project()
    {

        $idProject = xss_clean($this->input->post('id_project'));

        $data = $this->MReport->data_project($idProject)->row();
        echo json_encode($data);
    }

    public function get_eval_kunjungan()
    {

        $idProject = xss_clean($this->input->post('id_project'));
        $jenisEpm  = xss_clean($this->input->post('jenis_epm'));

        $dataProject           = $this->MReport->data_project($idProject)->row();
        $dataArea              = $this->MReport->data_project_area($idProject)->result_array();
        $dataEvaluasiKunjungan = $this->MReport->data_evaluasi_kunjungan($idProject, $jenisEpm)->result_array();
        $dataInsp              = $this->MEvaluasi_kunjungan->dataEvaluasi($dataEvaluasiKunjungan[0]['id'])->result_array();
        if ($this->MReport->data_evaluasi_kunjungan($idProject, $jenisEpm)->num_rows() > 0) {
            $dataDetailEvaluasiKunjungan = $this->MReport->data_detail_evaluasi_kunjungan($dataEvaluasiKunjungan[0]['id'])->result_array();

            $data = array(
                'dataInsp'                    => $dataInsp,
                'dataProject'                 => $dataProject,
                'dataEvaluasiKunjungan'       => $dataEvaluasiKunjungan,
                'dataDetailEvaluasiKunjungan' => $dataDetailEvaluasiKunjungan,
            );
        } else {
            $data = array('dataInsp' => $dataInsp,
                'dataProject'            => $dataProject,
                'dataEvaluasiKunjungan'  => $dataEvaluasiKunjungan,
            );
        }

        echo json_encode($data);
    }
    private function numberToRomanRepresentation($number)
    {
        $map         = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if ($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
    public function get_bab()
    {
        $idProject = xss_clean($this->input->post('id_project'));
        $dataArea  = $this->MReport->data_project_area($idProject)->result_array();
        $area      = [];
        foreach ($dataArea as $row) {
            $area[$row['id_area']] = $row['id_area'];
        }
        $babroman = [];
        $bab      = 3;
        foreach ($area as $id_area) {
            $bab++;
            $babroman[] = [
                'label' => $this->numberToRomanRepresentation($bab),
                'id'    => $id_area];
        }
        $data['bab']     = $babroman;
        $data['nextbab'] = $this->numberToRomanRepresentation($bab + 1);
        $file            = $this->db->where('id_project', $idProject)
            ->where('is_delete', '0')
            ->order_by('id', 'DESC')->get('tbl_project_pic')->row_array();
        $data['file'] = $file['file_path'];
        echo json_encode($data);
    }
    public function pdf_report($report_type, $id_project = null, $jenis_epm = null, $page = null)
    {
        if ($jenis_epm == 100) {

            $data              = $this->load_data($id_project, $jenis_epm);
            $data['page']      = $page;
            $data['jenis_epm'] = $jenis_epm;
            $htmlReport        = $this->load->view('print_report_100', $data, true);
        } else {
            $data       = $this->load_data($id_project, $jenis_epm);
            $htmlReport = $this->load->view('print_report', $data, true);
        }
        $htmlReport = "<!DOCTYPE html>
        <html>
            <head>
                <style>
                    body {
                        font-family: DejaVu Sans, sans-serif;
                        font-size:12px;
                         }
                    table{
                         border-collapse: collapse;
                    }
                    .solid{
                        page-break-inside:avoid;
                    }
                    .nextpage{
                        page-break-after:always;
                    }
                </style>
            </head>
            <body>
            {$htmlReport}
            </body>
        </html>
        ";
        $this->load->library('pdf');
        $paperOrientation = 'potrait';
        $fileName         = 'Buku-EPM.pdf';
        $size             = 'A4';
        if ($page == 'bab3') {
            $size = 'A3';
        }
        $this->pdf->base_html($htmlReport, $fileName, $size, $paperOrientation);
        //$this->MReport->generate_pdf_report($report_type, $param, $param2, $param3);
    }

    public function pdf_report_summary($report_type, $id_project = null, $jenis_epm = null)
    {
        $data       = $this->load_data($id_project, $jenis_epm);
        $htmlReport = $this->load->view('print_check', $data, true);

        $htmlReport = "<!DOCTYPE html>
        <html>
            <head>
                <style>
                    body {
                        font-family: DejaVu Sans, sans-serif;
                        font-size:12px;
                         }
                    table{
                         border-collapse: collapse;
                    }
                    .solid{
                        page-break-inside:avoid;
                    }
                    .nextpage{
                        page-break-after:always;
                    }
                </style>
            </head>
            <body>
            {$htmlReport}
            </body>
        </html>
        ";
        $this->load->library('pdf');
        $paperOrientation = 'potrait';
        $fileName         = 'Report-summary.pdf';
//        print_r($data);
        //echo $htmlReport;
        $this->pdf->base_html($htmlReport, $fileName, 'A4', $paperOrientation);
    }

    public function pdf_report_catatan($report_type, $param = null, $param2 = null)
    {
        $this->MReport->generate_pdf_report_catatan($report_type, $param, $param2);
    }

    // public function pdf_report_toss($report_type, $param = null, $param2 = null)
    // {
    //     $this->MReport->generate_pdf_report_toss($report_type, $param, $param2);
    // }

    // public function pdf_report_toss($report_type, $param = null, $param2 = null)
    // {
    //     $this->MReport->gen_toPDF($report_type, $param, $param2);
    // }

    public function pdf_report_toss($id_ = null, $mode = null)
    {
        if ((empty($mode) || empty($id_))) {
            echo "Not Found / Bad parameter";
            return false;
        } else {
            if ($mode == 'pdf' || $mode == 'html') {
                // return true;
            } else {
                echo "Not Found / Bad parameter";
                return false;
            }
        }

        /*  $toss = $this->db->query('select
        tb.*,
        (select nama from tbl_lokasi_provinsi where id=tb.provinsi) as nama_provinsi,
        (select nama from tbl_lokasi_kota_kabupaten where id=tb.provinsi) as nama_kota_kabupaten,
        (select nama from tbl_lokasi_kecamatan where id=tb.provinsi) as nama_kecamatan,
        (tb.jumlah_stall_stall_gr + tb.jumlah_stall_ths_motor + tb.jumlah_stall_ths_mobil + tb.jumlah_stall_lain_lain) as jumlah_stall
        from
        tbl_toss_database as tb
        where
        tb.id=' . $id_)->result();*/
        $toss = $this->db
            ->select("
            tb.*,
            (select nama from tbl_lokasi_provinsi where id=tb.provinsi) as nama_provinsi,
            (select nama from tbl_lokasi_kota_kabupaten where id=tb.provinsi) as nama_kota_kabupaten,
            (select nama from tbl_lokasi_kecamatan where id=tb.provinsi) as nama_kecamatan,
            (tb.jumlah_stall_stall_gr + tb.jumlah_stall_ths_motor + tb.jumlah_stall_ths_mobil + tb.jumlah_stall_lain_lain) as jumlah_stall")
            ->from('tbl_toss_database as tb')
            ->where('tb.id', $id_)->get()->result();
        if (!empty($toss)) {
            $data['data'] = $toss[0];
        } else {
            echo "Not Found";
            return false;
        }

        /*$data['jumlah_manpower'] = $this->db->query("select
        (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database=" . $id_ . " and jabatan='admin') as admin,
        (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database=" . $id_ . " and jabatan='pro_tech') as pro_tech,
        (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database=" . $id_ . " and jabatan='tech') as tech,
        (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database=" . $id_ . " and jabatan='others') as others,
        (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database=" . $id_ . ") as semua")->result()[0];*/
        $data['jumlah_manpower'] = $this->db
            ->select("
            (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id_' and jabatan='admin') as admin,
            (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id_' and jabatan='pro_tech') as pro_tech,
             (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id_' and jabatan='tech') as tech,
             (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id_' and jabatan='others') as others,
             (select count(*) as a from  tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id_') as semua")->get()->row();

        $area = $this->MReport->areaFE($id_);

        foreach ($area as $key => $value) {
            $area[$key]->sub_area = [];
            $sub_area             = $this->MReport->sub_areaFE($value->id_area, $value->id_toss_database);
            foreach ($sub_area as $key_sub => $value_sub) {
                array_push($area[$key]->sub_area, $value_sub);
                $area[$key]->sub_area[$key_sub]->items = [];
                $items                                 = $this->MReport->items($value_sub->id_toss_database, $value_sub->id_sub_area);
                foreach ($items as $key_items => $value_items) {
                    array_push($area[$key]->sub_area[$key_sub]->items, $value_items);
                }
            }
        }

        $equipment = $this->MReport->GroupEQFE($id_);

        foreach ($equipment as $key => $value) {
            $equipment[$key]->items = [];
            $sub_area               = $this->MReport->ItemsEQFE($value->id_toss_database, $value->group_key);
            foreach ($sub_area as $key_item => $value_item) {
                array_push($equipment[$key]->items, $value_item);
                $equipment[$key]->items[$key_item]->foto = [];
                $items                                   = $this->MReport->Isi_EQ($value_item->id);
                foreach ($items as $key_foto => $value_foto) {
                    array_push($equipment[$key]->items[$key_item]->foto, $value_foto);
                }
            }
        }

        $data['manpower_as']   = $this->MReport->ls_manpower_sdm_after_sales($id_);
        $data['manpower_umum'] = $this->MReport->ls_manpower_sdm_umum($id_);

        $data['area']      = $area;
        $data['equipment'] = $equipment;

        if ($mode == 'pdf') {
            $this->load->library('pdf');
            $htmlReport = '';

            if ($report_type == 'toss') {

                ob_start();
                setlocale(LC_ALL, 'IND');
            }

            ob_end_clean();

            $htmlReport = $this->load->view('print_report_toss', $data, true);

            $paperOrientation = 'potrait';
            $fileName         = 'tes.pdf';

            $this->pdf->base_html($htmlReport, $fileName, 'A3', $paperOrientation);
        }

        if ($mode == 'html') {
            $this->load->view('print_report_toss', $data);
        }
    }

}
