<?php
// defined('BASEPATH') or exit('No direct script access allowed');
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

defined('BASEPATH') or exit('No direct script access allowed');

class Evaluasi_kunjungan extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model("MEvaluasi_kunjungan");
        $this->load->model("MMaster_data");
        $this->load->model("MLogging");
        $logged_in = $this->session->userdata('logged_in');
        if (!$logged_in) {
            redirect('login/index');
        }
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    public function set_page($mode, $id = null)
    {
        $data['menu']     = 'evaluasi_kunjungan';
        $data['sub_menu'] = '';
        $data['mode']     = $mode;
        $data['content']  = 'view_evaluasi_kunjungan';
        $data['tpl_js']   = 'evaluasi_kunjungan';

        if ($mode == 'view' || $mode == 'detail_evaluasi_kunjungan') {
            $data['data_evaluasi']        = $this->MEvaluasi_kunjungan->showAllEvaluasi()->result_array();
            $data['data_detail_evaluasi'] = $this->MEvaluasi_kunjungan->showAllDetailEvaluasi($id)->result_array();
            $data['project']              = $this->MEvaluasi_kunjungan->showProject($id)->result_array();
            $data['data_inspeksi']        = $this->MEvaluasi_kunjungan->dataEvaluasi($id)->result_array();
            $data['plugin_mode']          = '2';
        } else if ($mode == 'add' || $mode == 'edit' || $mode == 'edit_detail_evaluasi_kunjungan') {
            $data['data_detail_evaluasi'] = $this->MEvaluasi_kunjungan->showAllDetailEvaluasi($id)->result_array();
            $data['data_project']         = $this->MMaster_data->read_project()->result_array();
            $data['data_evaluasi']        = $this->MEvaluasi_kunjungan->evaluasi_id($id)->row();
            $data['data_evaluasi_result'] = $this->MEvaluasi_kunjungan->showAllEvaluasi()->result_array();
            $data['plugin_mode']          = '1';
        } else {
            redirect('error/code/404');
        };

        $this->load->view('tpl_index', $data);
    }

    public function insert_data()
    {
        $id_project    = xss_clean($this->input->post('id_project'));
        $epm           = xss_clean($this->input->post('epm'));
        $tgl_kunjungan = xss_clean($this->input->POST('tgl_kunjungan'));
        $data          = array(
            'id_project'    => $id_project,
            'jenis_epm'     => $epm,
            'tgl_kunjungan' => date('Y-m-d', strtotime($tgl_kunjungan)),
        );

        $insert                = $this->MEvaluasi_kunjungan->add_evaluasi_kunjungan($data);
        $id_evaluasi_kunjungan = $this->db->insert_id();
        $all_catatan           = $this->db
            ->select('tbl_project_catatan.*')
            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_catatan.jenis_epm', $epm)
            ->from('tbl_project_area')
            ->join('tbl_project_catatan', 'tbl_project_catatan.id_project_area=tbl_project_area.id')
            ->get()->result_array();
        foreach ($all_catatan as $row) {
            $data_evaluasi_detail = array(
                'id_evaluasi_kunjungan'  => $id_evaluasi_kunjungan,
                'id_project_area'        => $row['id_project_area'],
                'id_project_catatan'     => $row['id'],
                'eval_problem_deskripsi' => $row['catatan'],
            );
            $this->MEvaluasi_kunjungan->insert_evaluasi_detail($data_evaluasi_detail);
        }
        $smodule  = 'back end evaluasi kunjugan';
        $activity = 'Change Project Data';
        $tbl_name = 'tbl_evaluasi_kunjungan';
        $action   = 'insert evaluasi kunjungan';

        $log_new_value_data = [];
        foreach ($data as $key => $value) {
            $log_new_value_data[] = $value;
        }
        $dberror = $this->db->error();
        if ($this->db->trans_status() === false) {

            if ($dberror['code'] != '0') {
                // catch error DB

                $this->db->trans_rollback();

                //insert update log
                $log_new_value_data    = array();
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';
                $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', '');
                echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

            } else {

                $this->db->trans_rollback();

                //insert update log
                $log_new_value_data = array();
                $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Insert', '', '');
                echo json_encode(array("status" => false, 'ket' => 'Save data failed'));

            }

        } else {

            $addtional_information = '';

            $this->db->trans_commit();

            //insert update log
            $this->MLogging->insert_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Insert', '',$log_new_value_data);
            echo json_encode(array("status" => true, 'ket' => 'insert data evaluasi kunjungan'));

        }

    }

    public function update_data($what_data)
    {

        $smodule               = 'back end evaluasi kunjugan';
        $activity              = 'Change Project Data';
        $id                    = xss_clean($this->input->post('id'));
        $id_evaluasi_kunjungan = xss_clean($this->input->post('id_evaluasi_kunjungan'));
        if ($what_data == 'evaluasi_kunjungan') {
            $tbl_name      = 'tbl_evaluasi_kunjungan';
            $action        = 'insert evaluasi kunjungan';
            $id_project    = xss_clean($this->input->post('id_project'));
            $epm           = xss_clean($this->input->post('epm'));
            $tgl_kunjungan = xss_clean($this->input->post('tgl_kunjungan'));

            $old = $this->db->where('id', $id)->get('tbl_evaluasi_kunjungan')->row_array();

            $log_old_value_data = [$id];
            $log_new_value_data = [$id];
            $data               = array(
                'id_project'    => $id_project,
                'jenis_epm'     => $epm,
                'tgl_kunjungan' => date('Y-m-d', strtotime($tgl_kunjungan)),
            );
            foreach ($data as $key => $value) {
                $log_old_value_data[] = isset($old[$key]) ? $old[$key] : '';
                $log_new_value_data[] = $key;
            }
            $update = $this->MEvaluasi_kunjungan->update_evaluasi($id, $data);

            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $log_new_value_data    = array();
                $addtional_information = '';
                if ($dberror['code'] != '0') {
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';

                }

                $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Update', '', '');
                echo json_encode(array("status" => false, 'ket' => 'Save data failed'));
            } else {

                $addtional_information = '';

                $this->db->trans_commit();
                $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Update', $log_old_value_data, $log_new_value_data);
                echo json_encode(array("status" => true, 'ket' => 'update data evaluasi kunjungan detail', 'id_evaluasi_kunjungan' => $id));

            }
        } else if ($what_data == 'detail_evaluasi_kunjungan') {
            $tbl_name               = 'tbl_evaluasi_kunjungan_detail';
            $action                 = 'insert evaluasi kunjungan';
            $eval_problem_deskripsi = xss_clean($this->input->post('eval_problem_deskripsi'));
            $tindak_lanjut          = xss_clean($this->input->post('tindak_lanjut'));
            $target                 = xss_clean($this->input->post('target'));
            $pic                    = xss_clean($this->input->post('pic'));
            $data                   = array(
                'eval_problem_deskripsi' => $eval_problem_deskripsi,
                'eval_tindak_lanjut'     => $tindak_lanjut,
                'eval_target'            => $target,
                'eval_pic'               => $pic,
            );
            $old                = $this->db->where('id', $id)->get('tbl_evaluasi_kunjungan_detail')->row_array();
            $log_old_value_data = [$id];
            $log_new_value_data = [$id];
            foreach ($data as $key => $value) {
                $log_old_value_data[] = isset($old[$key]) ? $old[$key] : '';
                $log_new_value_data[] = $key;
            }
            $update  = $this->MEvaluasi_kunjungan->update_detail_evaluasi($id, $data);
            $dberror = $this->db->error();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $log_new_value_data    = array();
                $addtional_information = '';
                if ($dberror['code'] != '0') {
                    $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';

                }

                $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Update', '', '');
                echo json_encode(array("status" => false, 'ket' => 'Save data failed'));
            } else {

                $addtional_information = '';

                $this->db->trans_commit();
                $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Update', $log_old_value_data, $log_new_value_data);
                echo json_encode(array("status" => true, 'ket' => 'update data evaluasi kunjungan detail', 'id_evaluasi_kunjungan' => $id_evaluasi_kunjungan));

            }
        }
    }

    public function delete_data()
    {

        $data = array(
            'is_delete' => '1',
        );

        //if ($what_data == 'evaluasi_kunjungan') {
        $id = xss_clean($this->input->post('id'));
        //$delete = $this->MEvaluasi_kunjungan->delete_evaluasi($id, $data);
        $this->db->where('id', $id)->delete('tbl_evaluasi_kunjungan');
        $this->db->where('id_evaluasi_kunjungan', $id)->delete('tbl_evaluasi_kunjungan_detail');

        $smodule  = 'back end evaluasi kunjugan';
        $activity = 'Change Project Data';
        $tbl_name = 'tbl_evaluasi_kunjungan';
        $action   = 'delete evaluasi kunjungan';

        $log_old_value_data = [$id, 0];
        $log_new_value_data = [$id, 1];
        $dberror            = $this->db->error();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $log_new_value_data    = array();
            $addtional_information = '';

            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';

            }
            $this->db->trans_rollback();

            //insert update log
            $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $tbl_name, 'Delete', '', '');
            echo json_encode(array("status" => false, 'ket' => 'Delete data failed'));

        } else {

            $addtional_information = '';

            $this->db->trans_commit();

            //insert update log
            $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $tbl_name, 'Delete', $log_old_value_data, $log_new_value_data);
            echo json_encode(array("status" => true, 'ket' => 'Delete data evaluasi kunjungan'));

        }

    }

    public function get_epm($id_project)
    {
        $data = $this->MEvaluasi_kunjungan->get_epm($id_project)->result_array();
        echo json_encode($data);
    }

}
