<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');
class Project_picture extends CI_Controller
{

    public function __Construct()
    {
        parent::__Construct();
        $this->load->model("MProject_picture");
    }

    public function index()
    {
        redirect('errors/code/404');
    }

    public function set_page($mode = null)
    {

        if ($mode == 'view') {

            $data['menu']                  = 'project_picture';
            $data['sub_menu']              = '';
            $data['mode']                  = $mode;
            $data['content']               = 'view_project_picture';
            $data['tpl_js']                = 'project_picture';
            $data['read_project_area_pic'] = $this->MProject_picture->read_project_area_pic()->result_array();
            $data['data_project_pic']      = $this->MProject_picture->read_project_pic()->result_array();
            $data['plugin_mode']           = '2';

            $this->load->view('tpl_index', $data);

        } else {
            redirect('errors/code/404');
        }
    }

    public function delete_project_area_pic($id = null)
    {
        if ($id == null) {
            redirect('errors/code/404');
        } else {
            $data = array(
                'is_delete' => 1,
            );
            $this->MProject_picture->delete_project_area_pic($id, $data);
            echo json_encode(array("status" => true));
        }

    }

    public function delete_project_pic($id = null)
    {
        if ($id == null) {
            redirect('errors/code/404');
        } else {
            $data = array(
                'is_delete' => 1,
            );
            $this->MProject_picture->delete_project_pic($id, $data);
            echo json_encode(array("status" => true));
        }
    }
}
