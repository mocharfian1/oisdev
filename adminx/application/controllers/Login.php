<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MLogging');
    }

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');
        $user_type = $this->session->userdata('user_type');
        if ($logged_in) {
            redirect('home');
        } else {
            $this->load->view('view_login');
        }

    }

    public function validate()
    {

        $this->form_validation->set_rules('email', '', 'required');
        $this->form_validation->set_rules('password', '', 'required');

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('message', 'Please complete all fields');
            redirect('login');
            return false;
        }
        $email    = xss_clean($this->input->post('email'));
        $password = xss_clean($this->input->post('password'));

        $q = $this->db->select('id, username, email, user_type, password')
            ->where('email', $email)
            ->where_in('user_type', ['admin_toss', 'admin_project', 'superadmin', 'tam'])
            ->where('is_delete', 0)
            ->get('tbl_user');

        if ($q->num_rows() > 0) {
            $data = $q->row_array();

            if (compare_password($password, $data['password'])) {
                $data['logged_in'] = true;

                $this->session->set_userdata($data);
                $this->MLogging->system_event('login', 'System Event', 'Login', 'Authentication', 'Success', 'Via Back End');
                redirect('home');

            } else {
                $this->session->set_flashdata('message', 'Wrong email or password');
                // $this->MLogging->system_event('login', 'System Event', 'Login', 'Authentication', 'Failed', 'Via Back End : Wrong email or password');
                redirect('login');
            }

        } else {
            $this->session->set_flashdata('message', 'Wrong email');
            // $this->MLogging->system_event('login', 'System Event', 'Login', 'Authentication', 'Failed', 'Via Back End : Wrong email');
            redirect('login');
        }

    }

    public function logout()
    {

        $this->MLogging->system_event('logout', 'System Event', 'Logout', 'Authentication', 'Success', 'Via Back End');
        $this->session->sess_destroy();
        redirect('login');

    }

    // public function ftoken(){
    //     $logged_in = $this->session->userdata('logged_in');
    //     if (!$logged_in || !$this->input->is_ajax_request()) {
    //         redirect('errors/code/404');
    //     }else{
    //         echo $this->security->get_csrf_hash();
    //     }

    // }
}
