<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Database_toss extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function getDataGuidance()
    {

    }

    public function getDataGuidance_items($id)
    {
        $tb_items = $this->db->select("t.* ,a.*,sa.*,t.id as id_item")
            ->from('tbl_toss_database_cs_area as t')
            ->join('tbl_area as a', 't.id_area=a.id')
            ->join('tbl_sub_area as sa', 't.id_sub_area=sa.id')
            ->where('t.is_delete', 0)
            ->where('id_toss_database', $id)
            ->get();

        if ($tb_items->num_rows() > 0) {
            return $tb_items->result();
        }
    }

    public function getDataGuidance_area($id)
    {
        $tb_area = $this->db->select("a.*,t.id_area")
            ->from('tbl_toss_database_cs_area as t')
            ->join('tbl_area as a', 't.id_area=a.id')
            ->where('t.is_delete', 0)
            ->where('id_toss_database', $id)
            ->group_by('a.id')
            ->get();

        if ($tb_area->num_rows() > 0) {
            return $tb_area->result();
        }
    }

    public function getDataGuidance_sub($id)
    {
        $tb_sub_area = $this->db->select("sa.*, t.id_sub_area")
            ->from('tbl_toss_database_cs_area as t')
            ->join('tbl_sub_area as sa', 't.id_sub_area=sa.id')
            ->where('t.is_delete', 0)
            ->where('id_toss_database', $id)
            ->group_by('t.id_sub_area')
            ->order_by('t.id', 'asc')
            ->get();

        if ($tb_sub_area->num_rows() > 0) {
            return $tb_sub_area->result();
        }
    }

    public function getDataGuidance_equipment_group($id)
    {
        $tb_eq_group = $this->db->select('group_key,group_name')
            ->from('tbl_toss_database_cs_equipment')
            ->where('id_toss_database', $id)
            ->group_by('group_key')
            ->get();

        if ($tb_eq_group->num_rows() > 0) {
            return $tb_eq_group->result();
        }
    }

    public function getDataGuidance_equipment_item($id)
    {
        $tb_eq_item = $this->db->from('tbl_toss_database_cs_equipment')
            ->where('id_toss_database', $id)
            ->get();

        if ($tb_eq_item->num_rows() > 0) {
            return $tb_eq_item->result();
        }
    }

    public function ls_pic_visit($id)
    {
        $data = $this->db->from('tbl_toss_database_visit')->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function front_end_ls_toss()
    {
        $data = $this->db->get('tbl_toss_front_end');
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_manpower_sdm_umum($id)
    {
        $data = $this->db->from('tbl_toss_database_cs_manpower')->where_in('jabatan', ["admin", "others"])->where('is_delete', 0)->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_manpower_sdm_after_sales($id)
    {
        $data = $this->db->from('tbl_toss_database_cs_manpower')->where_in('jabatan', ["tech", "pro_tech"])->where('is_delete', 0)->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_img_eq($id)
    {
        $data = $this->db->from('tbl_toss_database_cs_equipment_foto_dealer')->where('id_toss_database_cs_equipment', $id)->where('is_delete', 0)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function getCountManPower($id)
    {
        $data = $this->db->select("
		(select count(*) as jumlah from tbl_toss_database_cs_manpower where is_delete=0 and jabatan='pro_tech' and id_toss_database='$id') as pro_tech,
		(select count(*) as jumlah from tbl_toss_database_cs_manpower where is_delete=0 and jabatan='tech' and id_toss_database='$id') as tech,
		(select count(*) as jumlah from tbl_toss_database_cs_manpower where is_delete=0 and jabatan='admin' and id_toss_database='$id') as admin,
		(select count(*) as jumlah from tbl_toss_database_cs_manpower where is_delete=0 and jabatan='others' and id_toss_database='$id') as other,
		(select count(*) as jumlah from tbl_toss_database_cs_manpower where is_delete=0 and id_toss_database='$id') as total")->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_dealer()
    {
        $get = $this->db->select('*')->get('tbl_toss_dealer');
        if ($get->num_rows() > 0) {
            return $get->result();
        } else {
            return null;
        }
    }

}
