<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Duplicate_toss extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function addAreaToFront($id_toss_database, $id_front)
    {
        $areaDatabase = $this->db->from('tbl_toss_database_cs_area')->where('is_delete', 0)->where('id_toss_database', $id_toss_database)->get()->result();

        $toInsert = [];

        if (!empty($areaDatabase)) {
            foreach ($areaDatabase as $key => $value) {
                array_push($toInsert, array(
                    'id_toss_database'           => $id_front,
                    'id_area'                    => $value->id_area,
                    'id_sub_area'                => $value->id_sub_area,
                    'img_key'                    => $value->img_key,
                    'kriteria_minimum'           => $value->kriteria_minimum,
                    'jumlah'                     => $value->jumlah,
//                    'foto_contoh_file_path'      => $value->foto_contoh_file_path,
//                    'foto_contoh_file_true_path' => $value->foto_contoh_file_true_path,
//                    'foto_contoh_file_name'      => $value->foto_contoh_file_name,
                    'foto_contoh_file_name_64'   => $value->foto_contoh_file_name_64,
                    'foto_dealer_file_path'      => $value->foto_dealer_file_path,
                    'foto_dealer_file_true_path' => $value->foto_dealer_file_true_path,
                    'foto_dealer_file_name'      => $value->foto_dealer_file_name,
                    'foto_dealer_file_name_64'   => $value->foto_dealer_file_name_64,
                    'keterangan'                 => $value->keterangan,
                    'score_evaluasi_dealer'      => $value->score_evaluasi_dealer,
                    'is_delete'                  => $value->is_delete,
                ));
            }

        }

        $insert = $this->db->insert_batch('tbl_toss_front_end_cs_area', $toInsert);
        if ($insert) {
            return 1;
        } else {
            return null;
        }

    }

    public function addEQToFront($id_toss_database, $id_front)
    {
        $eqDatabase = $this->db->from('tbl_toss_database_cs_equipment')->where('id_toss_database', $id_toss_database)->get()->result();

        $toInsert = [];

        if (!empty($eqDatabase)) {
            foreach ($eqDatabase as $key => $value) {
                array_push($toInsert, array(
                    'id_toss_database'           => $id_front,
                    'group_key'                  => $value->group_key,
                    'group_name'                 => $value->group_name,
                    'item_key'                   => $value->item_key,
                    'kriteria_minimum'           => $value->kriteria_minimum,
                    'jumlah_minimum'             => $value->jumlah_minimum,
                    //'foto_contoh_file_path'      => $value->foto_contoh_file_path,
                    //'foto_contoh_file_true_path' => $value->foto_contoh_file_true_path,
                    //'foto_contoh_file_name'      => $value->foto_contoh_file_name,
                    'foto_contoh_file_name_64'   => $value->foto_contoh_file_name_64,
                    'keterangan'                 => $value->keterangan,
                    'score_evaluasi_dealer'      => $value->score_evaluasi_dealer,
                ));
            }

        }

        $insert = $this->db->insert_batch('tbl_toss_front_end_cs_equipment', $toInsert);
        if ($insert) {
            return 1;
        } else {
            return null;
        }

    }

}
