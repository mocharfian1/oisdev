<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

defined('BASEPATH') or exit('No direct script access allowed');

class MProject_picture extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function read_project_area_pic()
    {
        $this->db->select('tbl_project_area_pic.id, tbl_project_area_pic.file_name, tbl_project_area_pic.file_path, tbl_project_area_pic.id_project_area, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_project_area.id_project, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_project.nama_outlet, tbl_project.main_dealer, tbl_project.fungsi_outlet, tbl_project.type_pembangunan, tbl_project.alamat, tbl_project.jenis_epm');
        $this->db->from('tbl_project_area_pic');
        $this->db->join('tbl_project_area', 'tbl_project_area.id = tbl_project_area_pic.id_project_area', 'left');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left');
        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
        $this->db->where('tbl_project_area_pic.is_delete =', 0);
        return $query = $this->db->get();
    }

    public function read_project_pic()
    {
        $this->db->select('tbl_project_pic.id, tbl_project_pic.file_name,tbl_project_pic.type, tbl_project_pic.file_path,  tbl_project.nama_outlet, tbl_project.main_dealer, tbl_project.fungsi_outlet, tbl_project.type_pembangunan, tbl_project.alamat, tbl_project.jenis_epm');
        $this->db->from('tbl_project_pic');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_pic.id_project', 'left');
        $this->db->where('tbl_project_pic.is_delete =', 0);
        return $query = $this->db->get();
    }

    public function delete_project_area_pic($id, $data)
    {
        return $this->db->where('id =', $id)
            ->update('tbl_project_area_pic', $data);
    }
    public function delete_project_pic($id, $data)
    {

        $smodule    = 'back end project picture';
        $activity   = 'Change Master Projct Picture';
        $table_name = 'tbl_project_pic';

        $this->db->trans_begin();
        $dberror = $this->db->error();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $log_new_value_data    = array();
            $addtional_information = 'Error';
            if ($dberror['code'] != '0') {
                $addtional_information = 'Error DB (code :' . $dberror["code"] . ') (message :' . $dberror["message"] . ')';

            }
            $this->MLogging->update_log($smodule, 'Exception', $activity, 'Project (EPM)', 'Error', $addtional_information, $table_name, 'Update', '', $log_new_value_data);

        } else {
            $this->db->trans_commit();
            $log_old_value_data = array($id, '0');
            $log_new_value_data = array($id, '1');
            $addtional_information = '';
            $this->MLogging->update_log($smodule, 'Data Change', $activity, 'Project (EPM)', 'Success', $addtional_information, $table_name, 'Update', $log_old_value_data, $log_new_value_data);
            echo json_encode(array("status" => true, 'ket' => ''));

        }
        return $this->db->where('id =', $id)
            ->update('tbl_project_pic', $data);
    }

    public function read_project_pic_where($id)
    {
        return $this->db->where('id_project_area =', $id)
            ->get('tbl_project_area_pic');
    }

}
