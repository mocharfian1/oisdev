<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guidance_toss extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

    }

    public function getDataGuidance()
    {
        $query = $this->db->select('*')
            ->from('tbl_toss_guidance')
            ->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function getDataGuidance_items()
    {
        $tb_items = $this->db->select('t.* ,a.*,sa.*,t.id as id_item')
            ->from('tbl_toss_guidance_cs_area as t')
            ->join('tbl_area as a', 't.id_area=a.id')
            ->join('tbl_sub_area as sa', 't.id_sub_area=sa.id')
            ->where('t.is_delete', 0)
            ->where('t.is_active', 1)
            ->order_by('t.id', 'asc')->get();

        if ($tb_items->num_rows() > 0) {
            return $tb_items->result();
        }
    }

    public function getDataGuidance_area()
    {
        $tb_area = $this->db->select('a.*,t.id_area')
            ->from('tbl_toss_guidance_cs_area as t')
            ->join('tbl_area as a', 't.id_area=a.id')->where('t.is_delete', 0)
            ->where('t.is_active', 1)
            ->group_by('a.id')
            ->order_by('a.id', 'asc')->get();

        if ($tb_area->num_rows() > 0) {
            return $tb_area->result();
        }
    }

    public function getDataGuidance_sub()
    {
        $tb_sub_area = $this->db->select('sa.*, t.id_sub_area')
            ->from('tbl_toss_guidance_cs_area as t')
            ->join('tbl_sub_area as sa', 't.id_sub_area=sa.id')
            ->where('t.is_delete', 0)
            ->where('t.is_active', 1)
            ->group_by('t.id_sub_area')
            ->order_by('t.id', 'asc')
            ->get();

        if ($tb_sub_area->num_rows() > 0) {
            return $tb_sub_area->result();
        }
    }

    public function getDataGuidance_equipment_group()
    {
        $tb_eq_group = $this->db->select('group_key,group_name')
            ->from('tbl_toss_guidance_cs_equipment')
            ->where('is_delete', 0)
            ->where('is_active', 1)
            ->group_by('group_key')
            ->order_by('id', 'asc')
            ->get();

        if ($tb_eq_group->num_rows() > 0) {
            return $tb_eq_group->result();
        }
    }

    public function getDataGuidance_equipment_item()
    {
        $tb_eq_item = $this->db->from('tbl_toss_guidance_cs_equipment')
            ->where('is_delete', 0)->where('is_active', 1)
            ->order_by('id', 'asc')
            ->get();

        if ($tb_eq_item->num_rows() > 0) {
            return $tb_eq_item->result();
        }
    }

    public function ls_pic_visit($id)
    {
        $data = $this->db->from('tbl_toss_database_visit')->where('is_delete', 0)->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function front_end_ls_toss()
    {
        $data = $this->db->from('tbl_toss_front_end')->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

}
