<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MReport extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function read_data_project()
    {
        return $this->db->where('is_delete =', 0)
            ->where('type', 'project')
        // ->where('id =', $id_project)
            ->get('tbl_project');
    }

    // public function data_project_area($id_project) {
    //        $this->db->select('tbl_project_area.id, tbl_project.jenis_epm, tbl_project_area.is_delete, tbl_project_area.id_project, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_sub_area.description');
    //        $this->db->from('tbl_project_area');
    //        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area','left');
    //        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area','left');
    //        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
    //        $this->db->where('id_project =',$id_project);
    //        $this->db->where('tbl_project_area.is_delete =', 0);
    //        $this->db->where('tbl_project.is_delete =', 0);
    //        $this->db->group_by('id_area');
    //        return $query = $this->db->get();
    //    }

    //    public function data_project_sub_area($id_project) {
    //        $this->db->select('tbl_project_area.id, tbl_project.jenis_epm, tbl_sub_area.is_delete, tbl_project_area.id_project, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_sub_area.description');
    //        $this->db->from('tbl_project_area');
    //        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area','left');
    //        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area','left');
    //        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
    //        $this->db->where('id_project =',$id_project);
    //        $this->db->where('tbl_project_area.is_delete =', 0);
    //        $this->db->where('tbl_project.is_delete =', 0);
    //        return $query = $this->db->get();
    //    }

    public function data_project($id_project)
    {
        return $this->db
            ->select("tbl_project.*, user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3")
            ->where('tbl_project.id =', $id_project)
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1', 'left')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2', 'left')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3', 'left')
            ->get('tbl_project');
    }

    public function data_project_area($id_project)
    {
        return $this->db->select('a.*, ta.area_name, tsa.sub_area_name')->from('tbl_project_area as a')
            ->join('tbl_area as ta', 'ta.id = a.id_area')
            ->join('tbl_sub_area as tsa', 'tsa.id = a.id_sub_area')
            ->where('a.is_delete', 0)
            ->where('a.id_project', $id_project)
            ->order_by('a.position')
            ->get();
    }

    public function data_project_catatan($id_project, $jenis_epm)
    {
        return $this->db->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_catatan.jenis_epm', $jenis_epm)
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_catatan.id_project_area')
            ->get('tbl_project_catatan');
    }

    public function read_ic_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_ic.*')

            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_sa_config_ic.jenis_epm =', $jenis)
            ->order_by('tbl_project_sa_config_ic.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_ic.id_project_area')
            ->get('tbl_project_sa_config_ic');
    }
    public function read_ex($id_project)
    {
        return $this->db
            ->where('tbl_executive_summary.id_project', $id_project)
            ->get('tbl_executive_summary');
    }

    public function read_km_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_km.*')
            ->where('tbl_project_area.id_project', $id_project)
            ->order_by('tbl_project_sa_config_km.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_km.id_project_area')
            ->get('tbl_project_sa_config_km');
    }

    public function read_kp_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_kp.*')
            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_sa_config_kp.jenis_epm =', $jenis)
            ->order_by('tbl_project_sa_config_kp.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_kp.id_project_area')

            ->get('tbl_project_sa_config_kp');
    }

    public function read_minreq_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_minreq.*')
            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_sa_config_minreq.jenis_epm =', $jenis)
            ->order_by('tbl_project_sa_config_minreq.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_minreq.id_project_area')

            ->get('tbl_project_sa_config_minreq');
    }

    public function read_ket_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_ket.*')

            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_sa_config_ket.jenis_epm =', $jenis)
            ->order_by('tbl_project_sa_config_ket.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_ket.id_project_area')

            ->get('tbl_project_sa_config_ket');
    }

    public function read_tps_line_where($id_project, $jenis)
    {
        return $this->db
            ->select('tbl_project_sa_config_tps_line.*')

            ->where('tbl_project_area.id_project', $id_project)
            ->where('tbl_project_sa_config_tps_line.jenis_epm =', $jenis)
            ->order_by('tbl_project_sa_config_tps_line.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_tps_line.id_project_area')

            ->get('tbl_project_sa_config_tps_line');
    }

    public function read_project_area_pic($id_project, $jenisEpm)
    {
        return $this->db
            ->select('tbl_project_area_pic.*')

            ->where('tbl_project_area.id_project', $id_project)
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_area_pic.id_project_area')
            ->get('tbl_project_area_pic');
    }

    public function read_project_pic($id_project)
    {
        return $this->db->where('is_delete =', 0)
            ->where('id_project', $id_project)
        //->where('jenis_epm', $jenisEpm)
            ->get('tbl_project_pic');
    }

    public function data_project_eat($idProject, $jenisEpm)
    {
        return $this->db->where('is_delete =', 0)
            ->where('id_project =', $idProject)
            ->where('jenis_epm =', $jenisEpm)
            ->order_by('position', 'ASC')
            ->get('tbl_project_config_equipment_and_tools');
    }

    public function data_project_config_pic($idProject, $jenisEpm)
    {
        return $this->db->select('tbl_project_area_pic.*,tbl_project_sa_config_pic.*')
            ->where('tbl_project_area.id_project =', $idProject)
            ->where('jenis_epm =', $jenisEpm)
            ->order_by('tbl_project_sa_config_pic.position', 'ASC')
            ->join('tbl_project_area', 'tbl_project_area.id=tbl_project_sa_config_pic.id_project_area')
            ->join('tbl_project_area_pic', "tbl_project_area_pic.id=tbl_project_sa_config_pic.id_project_area_pic AND tbl_project_area_pic.is_delete='0'", 'left')
            ->get('tbl_project_sa_config_pic');
    }
    public function read_project_target_investasi($id_project)
    {

        return $this->db
            ->where('id_project =', $id_project)
            ->get('tbl_project_target_investasi');
    }
    public function read_project_sdm_sales($id_project)
    {

        return $this->db
            ->where('id_project =', $id_project)
            ->get('tbl_project_sdm_sales');
    }
    public function read_project_sdm_after_sales($id_project)
    {

        return $this->db
            ->where('id_project =', $id_project)
            ->get('tbl_project_sdm_after_sales');
    }
    public function data_evaluasi_kunjungan($idProject, $jenisEpm)
    {
        return $this->db->where('is_delete =', 0)
            ->where('id_project =', $idProject)
            ->where('jenis_epm =', $jenisEpm)
            ->get('tbl_evaluasi_kunjungan');
    }

    public function data_detail_evaluasi_kunjungan($id)
    {
        return $this->db->select("ekd.id, ekd.id_project_area, ekd.id_evaluasi_kunjungan, ekd.eval_problem_deskripsi, ekd.eval_tindak_lanjut, ekd.eval_target, ekd.eval_pic, ek.jenis_epm, ek.tgl_kunjungan, p.nama_outlet, p.fungsi_outlet, pa.id_sub_area, a.area_name, sa.sub_area_name")

            ->from('tbl_evaluasi_kunjungan_detail as ekd')
            ->join('tbl_evaluasi_kunjungan as ek', 'ek.id=ekd.id_evaluasi_kunjungan', 'left')
            ->join('tbl_project as p', 'p.id=ek.id_project', 'left')
            ->join('tbl_project_area as pa', 'pa.id=ekd.id_project_area')
            ->join('tbl_area as a', 'a.id=pa.id_area', 'left')
            ->join('tbl_sub_area as sa', 'sa.id=pa.id_sub_area')
            ->group_start()
            ->where('ekd.id_evaluasi_kunjungan', $id)
            ->or_where('ekd.id=', $id)
            ->group_end()
            ->where('ek.is_delete', 0)
            ->group_by('ekd.id')
            ->order_by('ekd.id', 'asc')
            ->get();
    }

    public function dataEvaluasi($id)
    {
        return $this->db->where('id_evaluasi_kunjungan =', $id)
            ->where('eval_problem_deskripsi IS NULL')
            ->where('is_delete = 0')
            ->get('tbl_evaluasi_kunjungan_detail');
    }

    public function read_project_man_power($id_project, $jenis_epm)
    {
        return $this->db->select('mp.*, lt.level_training_name')
            ->from('tbl_project_man_power as mp')
            ->join('tbl_project_level_training as lt', 'lt.id = mp.id_project_level_training')
            ->where('mp.is_delete', 0)
            ->where('mp.id_project', $id_project)
            ->where('jenis_epm', $jenis_epm)
            ->get();
    }

    public function dateMySqlConvertForEPM($date)
    {

        $dateArr = explode(" ", $date);

        $day   = $dateArr[0];
        $year  = $dateArr[2];
        $month = $dateArr[1];

        if ($day >= 1 && $day <= 7) {$day = 'W1';}
        if ($day >= 8 && $day <= 14) {$day = 'W2';}
        if ($day >= 15 && $day <= 21) {$day = 'W3';}
        if ($day >= 22) {$day = 'W4';}

        // if(month == 1){month = 'Januari'};
        // if(month == 2){month = 'Februari'};
        // if(month == 3){month = 'Maret'};
        // if(month == 4){month = 'April'};
        // if(month == 5){month = 'Mei'};
        // if(month == 6){month = 'Juni'};
        // if(month == 7){month = 'Juli'};
        // if(month == 8){month = 'Agustus'};
        // if(month == 9){month = 'September'};
        // if(month == 10){month = 'Oktober'};
        // if(month == 11){month = 'November'};
        // if(month == 12){month = 'Desember'};

        $dateFormat = $day . ' ' . $month . ' ' . $year;
        return $dateFormat;
    }

    public function generate_pdf_report($report_type, $param = null, $param2 = null, $param3 = null)
    {

        $this->load->library('pdf');
        $htmlReport = '';

        if ($report_type == 'buku_epm') {

            $idProject = $param;
            $jenisEpm  = $param2;
            $chapter   = $param3;

            $data_project = $this->data_project($idProject)->row();

            $dataArea               = $this->data_project_area($idProject)->result_array();
            $dataCatatan            = $this->data_project_catatan($idProject)->result_array();
            $dataItemChecklist      = $this->read_ic_where($idProject, $jenisEpm)->result_array();
            $dataKriteriaPekerjaan  = $this->read_kp_where($idProject, $jenisEpm)->result_array();
            $dataMinimumRequirement = $this->read_minreq_where($idProject, $jenisEpm)->result_array();
            $dataKet                = $this->read_ket_where($idProject, $jenisEpm)->result_array();
            $dataProjectPic         = $this->read_project_pic()->result_array();

            ob_start();
            setlocale(LC_ALL, 'IND');

            if ($jenisEpm != 100) {
                $reportCover = '<h2 align="center">' . $data_project->nama_outlet . ' ' . $jenisEpm . '%</h2><br><br>
                <table class="center">
                <tbody>
                <tr>
                <td style="padding: 5px">Nama Outlet</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $data_project->nama_outlet . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Dealer</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $data_project->main_dealer . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Fungsi Outlet</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $data_project->fungsi_outlet . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Type Pembangunan</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $data_project->type_pembangunan . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Alamat</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $data_project->alamat . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Tanggal EPM</td>
                </tr>
                <tr>
                <td style="padding: 5px">1. EPM 50%</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $this->dateMySqlConvertForEPM(date('d F Y', strtotime($data_project->epm_50_date))) . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">2. Target EPM 75%</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $this->dateMySqlConvertForEPM(date('d F Y', strtotime($data_project->epm_75_date))) . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">3. Target EPM 100%</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . $this->dateMySqlConvertForEPM(date('d F Y', strtotime($data_project->epm_100_date))) . '</td>
                </tr>
                <tr>
                <td style="padding: 5px">Target Otoritasi</td>
                <td style="padding: 5px"> : </td>
                <td style="padding: 5px">' . date('d F Y', strtotime($data_project->target_otorisasi)) . '</td>
                </tr>
                </tbody>
                </table>
                <p>
                Evaluasi Establishment Progress Monitoring (EPM) ' . $jenisEpm . '% ini telah diisi dan diperiksa oleh Main
                Dealer, outlet tersebut diatas sesuai dengan keadaan yang sesungguhnya. Pengecekan sesuai
                dengan kolom yang disediakan :<br><br>
                1. Kolom #1 diisi oleh Konsultan<br>
                2. Kolom #2 diisi oleh Dealer/Dealer<br>
                3. Kolom #3 diisi oleh TAM<br><br>
                Kunjungan Establishment Progress Monitoring (EPM) akan dilakukan apabila progress
                pekerjaan sudah sesuai dengan kriteria pekerjaan Establisment Progress Monitoring (EPM)
                ' . $jenisEpm . '%.
                </p>
                <br><p align="right">Jakarta , ' . strftime("%d %B %Y") . '</p><br>
                <table align="center" border="2" width="100%">
                <thead>
                <tr style="background-color:#99c0ff; color: #000">
                <th style="padding: 8px; text-align: center;" width="20%">KONSULTAN</th>
                <th style="padding: 8px; text-align: center;" width="20%">DEALER / DEALER</th>
                <th style="padding: 8px; text-align: center;" width="20%">TAM</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td style="padding: 40px"></td>
                <td style="padding: 40px"></td>
                <td style="padding: 40px"></td>
                </tr>
                <tr>
                <td style="padding: 5px">TANGGAL CHECK :</td>
                <td style="padding: 5px">TANGGAL CHECK :</td>
                <td style="padding: 5px">TANGGAL CHECK :</td>
                </tr>
                </tbody>
                </table><br><br>';

                $reportContent     = '';
                $oldHeaderAreaName = '';

                for ($i = 0; $i < count($dataArea); $i++) {

                    //header content
                    if ($oldHeaderAreaName == $dataArea[$i]['area_name']) {
                        $reportContentHeaderSubArea = '<h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000"><b>' . $dataArea[$i]['position'] . '. ' . $dataArea[$i]['sub_area_name'] . '</b></h4><br>';
                        $reportContent              = $reportContent . '' . $reportContentHeaderSubArea;
                    } else {
                        $reportContentHeaderArea = '<h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f"><b>' . $dataArea[$i]['area_name'] . '</b></h3>';
                        $reportContent           = $reportContent . '' . $reportContentHeaderArea;

                        $reportContentHeaderSubArea = '<h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000"><b>' . $dataArea[$i]['position'] . '. ' . $dataArea[$i]['sub_area_name'] . '</b></h4><br>';
                        $reportContent              = $reportContent . '' . $reportContentHeaderSubArea;

                        $oldHeaderAreaName = $dataArea[$i]['area_name'];
                    }

                    //item checklist

                    //ini buat item checklist model
                    $checklistModel = '';
                    if (count($dataItemChecklist) > 0) {
                        for ($x = 0; $x < count($dataItemChecklist); $x++) {

                            if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {
                                $checklistModel = $dataItemChecklist[$x]['model'];
                                break;
                            }
                        }
                    }

                    //ini buat item checklist content
                    $totalItemIC = 0;
                    if (count($dataItemChecklist) > 0 && $checklistModel == 'Model 1') {

                        $reportContentIC = '<table align="center" border="2" width="100%" border="1">
                        <thead>
                        <tr style="background-color:#99c0ff; color: #000">
                        <th style="text-align: center;">Item Check</th>
                        <th style="text-align: center;">Kriteria</th>
                        <th width="40%" colspan="3">
                        <table border="1" width="100%">
                        <thead>
                        <tr>
                        <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                        </tr>
                        <tr>
                        <th style="text-align: center;">#1</th>
                        <th style="text-align: center;">#2</th>
                        <th style="text-align: center;">#3</th>
                        </tr>
                        </thead>
                        </table>
                        </th>
                        </tr>
                        </thead>
                        <tbody>';

                        for ($x = 0; $x < count($dataItemChecklist); $x++) {

                            if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {

                                if ($dataItemChecklist[$x]['score_konsultan'] == '0') {
                                    $scoreKonsultan = 'X';
                                } else if ($dataItemChecklist[$x]['score_konsultan'] == '1') {
                                    $scoreKonsultan = 'V';
                                } else {
                                    $scoreKonsultan = '';
                                }

                                if ($dataItemChecklist[$x]['score_dealer'] == '0') {
                                    $scoreDealer = 'X';
                                } else if ($dataItemChecklist[$x]['score_dealer'] == '1') {
                                    $scoreDealer = 'V';
                                } else {
                                    $scoreDealer = '';
                                }

                                if ($dataItemChecklist[$x]['score_tam'] == '0') {
                                    $scoreTam = 'X';
                                } else if ($dataItemChecklist[$x]['score_tam'] == '1') {
                                    $scoreTam = 'V';
                                } else {
                                    $scoreTam = '';
                                }

                                $reportContentIC = $reportContentIC . '<tr>
                                <td style="padding: 7px">' . $dataItemChecklist[$x]['item_name'] . '</td>
                                <td style="padding: 7px">' . $dataItemChecklist[$x]['kriteria'] . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                                </tr>';

                                $totalItemIC++;
                            }
                        }

                        $reportContentIC = $reportContentIC . '</tbody>
                        </table><br><br>';

                    } else if (count($dataItemChecklist) > 0 && $checklistModel == 'Model 2') {

                        $reportContentIC = '<table align="center" border="2" width="100%" border="1">
                        <thead>
                        <tr style="background-color:#99c0ff; color: #000">
                        <th style="text-align: center;">Item Check</th>
                        <th style="text-align: center;">Kriteria</th>
                        <th style="text-align: center;">Jarak</th>
                        <th width="40%" colspan="3">
                        <table border="1" width="100%">
                        <thead>
                        <tr>
                        <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                        </tr>
                        <tr>
                        <th style="text-align: center;">#1</th>
                        <th style="text-align: center;">#2</th>
                        <th style="text-align: center;">#3</th>
                        </tr>
                        </thead>
                        </table>
                        </th>
                        </tr>
                        </thead>
                        <tbody>';

                        for ($x = 0; $x < count($dataItemChecklist); $x++) {

                            if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {

                                if ($dataItemChecklist[$x]['score_konsultan'] == '0') {
                                    $scoreKonsultan = 'X';
                                } else if ($dataItemChecklist[$x]['score_konsultan'] == '1') {
                                    $scoreKonsultan = 'V';
                                } else {
                                    $scoreKonsultan = '';
                                }

                                if ($dataItemChecklist[$x]['score_dealer'] == '0') {
                                    $scoreDealer = 'X';
                                } else if ($dataItemChecklist[$x]['score_dealer'] == '1') {
                                    $scoreDealer = 'V';
                                } else {
                                    $scoreDealer = '';
                                }

                                if ($dataItemChecklist[$x]['score_tam'] == '0') {
                                    $scoreTam = 'X';
                                } else if ($dataItemChecklist[$x]['score_tam'] == '1') {
                                    $scoreTam = 'V';
                                } else {
                                    $scoreTam = '';
                                }

                                $jarakVal = '';
                                if ($dataItemChecklist[$x]['jarak'] !== '' && $dataItemChecklist[$x]['jarak'] !== null) {
                                    $jarakVal = $dataItemChecklist[$x]['jarak'];
                                }

                                $reportContentIC = $reportContentIC . '<tr>
                                <td style="padding: 7px">' . $dataItemChecklist[$x]['item_name'] . '</td>
                                <td style="padding: 7px">' . $dataItemChecklist[$x]['kriteria'] . '</td>
                                <td style="padding: 7px">' . $jarakVal . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                                <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                                </tr>';

                                $totalItemIC++;
                            }
                        }

                        $reportContentIC = $reportContentIC . '</tbody>
                        </table><br><br>';

                    }

                    //jika item checklist 0
                    if ($totalItemIC == 0) {$reportContentIC = '';}

                    $reportContent = $reportContent . '' . $reportContentIC;

                    //kriteria pekerjaan
                    $totalItemKP     = 0;
                    $reportContentKP = '<table align="center" border="2" width="100%" border="1">
                    <thead>
                    <tr style="background-color:#99c0ff; color: #000">
                    <th style="text-align: center;">No.</th>
                    <th style="text-align: center;">Kriteria Pekerjaan</th>
                    <th width="40%" colspan="3">
                    <table border="1" width="100%">
                    <thead>
                    <tr>
                    <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                    </tr>
                    <tr>
                    <th style="text-align: center;">#1</th>
                    <th style="text-align: center;">#2</th>
                    <th style="text-align: center;">#3</th>
                    </tr>
                    </thead>
                    </table>
                    </th>
                    </tr>
                    </thead>';

                    for ($y = 0; $y < count($dataKriteriaPekerjaan); $y++) {

                        if ($dataKriteriaPekerjaan[$y]['id_project_area'] == $dataArea[$i]['id']) {
                            if ($dataKriteriaPekerjaan[$y]['score_konsultan'] == '0') {
                                $scoreKonsultan = 'X';
                            } else if ($dataKriteriaPekerjaan[$y]['score_konsultan'] == '1') {
                                $scoreKonsultan = 'V';
                            } else {
                                $scoreKonsultan = '';
                            }

                            if ($dataKriteriaPekerjaan[$y]['score_dealer'] == '0') {
                                $scoreDealer = 'X';
                            } else if ($dataKriteriaPekerjaan[$y]['score_dealer'] == '1') {
                                $scoreDealer = 'V';
                            } else {
                                $scoreDealer = '';
                            }

                            if ($dataKriteriaPekerjaan[$y]['score_tam'] == '0') {
                                $scoreTam = 'X';
                            } else if ($dataKriteriaPekerjaan[$y]['score_tam'] == '1') {
                                $scoreTam = 'V';
                            } else {
                                $scoreTam = '';
                            }

                            $totalItemKP++;

                            $reportContentKP = $reportContentKP . '<tr>
                            <td style="padding: 7px">' . $totalItemKP . '</td>
                            <td style="padding: 7px">' . $dataKriteriaPekerjaan[$y]['item_kriteria_pekerjaan'] . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                            </tr>';

                        }

                    }

                    $reportContentKP = $reportContentKP . '</tbody>
                    </table><br><br>';

                    if ($totalItemKP == 0) {$reportContentKP = '';}

                    $reportContent = $reportContent . '' . $reportContentKP;

                    //minreq
                    $totalItemMinReq     = 0;
                    $reportContentMinReq = '<div style="border: 2px solid #000; color: #000; padding: 5px;">
                    <span style="font-size: large"><b>Minreq</b></span>
                    <ul>';

                    for ($z = 0; $z < count($dataMinimumRequirement); $z++) {
                        if ($dataMinimumRequirement[$z]['id_project_area'] == $dataArea[$i]['id']) {

                            $reportContentMinReq = $reportContentMinReq . '<li>' . $dataMinimumRequirement[$z]['item_minreq'] . '</li>';

                            $totalItemMinReq++;
                        }
                    }

                    $reportContentMinReq = $reportContentMinReq . '</ul>
                    </div><br>';

                    if ($totalItemMinReq == 0) {$reportContentMinReq = '';};

                    $reportContent = $reportContent . '' . $reportContentMinReq;

                    //keterangan
                    $totalItemKet     = 0;
                    $reportContentKet = '<div style="border: 2px solid #000; color: #000; padding: 5px;">
                    <span style="font-size: large"><b>Keterangan</b></span>
                    <ol>';

                    for ($z = 0; $z < count($dataKet); $z++) {
                        if ($dataKet[$z]['id_project_area'] == $dataArea[$i]['id']) {

                            $reportContentKet = $reportContentKet . '<li>' . $dataKet[$z]['item_ket'] . '</li>';

                            $totalItemKet++;
                        }
                    }

                    $reportContentKet = $reportContentKet . '</ol>
                    </div><br>';

                    if ($totalItemKet == 0) {$reportContentKet = '';}

                    $reportContent = $reportContent . '' . $reportContentKet;

                    //catatan
                    $reportContentCatatan = '<div style="border: 2px solid #000; color: #000; padding: 5px;"><span style="font-size: large"><b>Catatan :</b></span>';

                    //loop catatan Konsultan
                    $contentCatatanKonsultan = '';

                    for ($a = 0; $a < count($dataCatatan); $a++) {
                        if ($dataCatatan[$a]['id_project_area'] == $dataArea[$i]['id'] && $dataCatatan[$a]['jenis_epm'] == $jenisEpm && $dataCatatan[$a]['catatan'] !== null && $dataCatatan[$a]['catatan'] !== '' && $dataCatatan[$a]['user_type'] == 'Konsultan') {
                            $reportContentCatatan    = $reportContentCatatan . '<br><br><i>Konsultan</i> : ' . $dataCatatan[$a]['catatan'];
                            $contentCatatanKonsultan = $dataCatatan[$a]['catatan'];
                            break;
                        }
                    }

                    $contentCatatanDealer = '';

                    for ($a = 0; $a < count($dataCatatan); $a++) {
                        if ($dataCatatan[$a]['id_project_area'] == $dataArea[$i]['id'] && $dataCatatan[$a]['jenis_epm'] == $jenisEpm && $dataCatatan[$a]['catatan'] !== null && $dataCatatan[$a]['catatan'] !== '' && $dataCatatan[$a]['user_type'] == 'Dealer') {
                            $reportContentCatatan = $reportContentCatatan . '<br><br><i>Dealer</i> : ' . $dataCatatan[$a]['catatan'];
                            $contentCatatanDealer = $dataCatatan[$a]['catatan'];
                            break;
                        }
                    }

                    $contentCatatanTam = '';

                    for ($a = 0; $a < count($dataCatatan); $a++) {
                        if ($dataCatatan[$a]['id_project_area'] == $dataArea[$i]['id'] && $dataCatatan[$a]['jenis_epm'] == $jenisEpm && $dataCatatan[$a]['catatan'] !== null && $dataCatatan[$a]['catatan'] !== '' && $dataCatatan[$a]['user_type'] == 'TAM') {
                            $reportContentCatatan = $reportContentCatatan . '<br><br><i>TAM</i> : ' . $dataCatatan[$a]['catatan'];
                            $contentCatatanTam    = $dataCatatan[$a]['catatan'];
                            break;
                        }
                    }

                    $reportContentCatatan = $reportContentCatatan . '</div><br>';

                    if ($contentCatatanKonsultan == '' && $contentCatatanDealer == '' && $contentCatatanTam == '') {$reportContentCatatan = '';};

                    $reportContent = $reportContent . '' . $reportContentCatatan;

                    //foto
                    //layout photo
                    $totalItemLayoutPhoto     = 0;
                    $reportContentLayoutPhoto = '<h3><b>Layout Photo :</b></h3><br><table style=""><tr>';

                    for ($b = 0; $b < count($dataProjectPic); $b++) {
                        if ($dataProjectPic[$b]['id_project_area'] == $dataArea[$i]['id'] && $dataProjectPic[$b]['type'] == 'layout') {
                            $totalItemLayoutPhoto++;

                            // if(b == data.dataProjectPic.length-1 || totalItemLayoutPhoto % 2 == 0){
                            //     reportContentLayoutPhoto = reportContentLayoutPhoto + '<div style="padding: 10px; vertical-align: top; "><img src="data.dataProjectPic[b].file_path+'" style="max-width: 300px; border:2px solid black; padding : 10px;"></div>';
                            // }else{

                            $res = fmod($totalItemLayoutPhoto, 3);
                            if ($res == 0) {
                                $reportContentLayoutPhoto = $reportContentLayoutPhoto . '<td style="width: 190px;vertical-align: top">&nbsp;<div style="padding: 10px; vertical-align: middle; display : inline"><img src="' . $dataProjectPic[$b]['file_path'] . '" style="width: 190px; border:2px solid black; padding : 10px; vertical-align: top;"></div>&nbsp;</td></tr><tr>';
                            } else {
                                $reportContentLayoutPhoto = $reportContentLayoutPhoto . '<td style="width: 190px;vertical-align: top">&nbsp;<div style="padding: 10px; vertical-align: middle; display : inline"><img src="' . $dataProjectPic[$b]['file_path'] . '" style="width: 190px; border:2px solid black; padding : 10px; vertical-align: top;"></div>&nbsp;</td>';
                            }

                            // }
                        }

                    }

                    $reportContentLayoutPhoto = $reportContentLayoutPhoto . '</tr></table><br><br>';
                    if ($totalItemLayoutPhoto == 0) {$reportContentLayoutPhoto = '';}

                    $reportContent = $reportContent . '' . $reportContentLayoutPhoto;

                    //area photo
                    $totalItemAreaPhoto     = 0;
                    $reportContentAreaPhoto = '<h3><b>Area Photo :</b></h3><br><table style=""><tr>';

                    for ($b = 0; $b < count($dataProjectPic); $b++) {
                        if ($dataProjectPic[$b]['id_project_area'] == $dataArea[$i]['id'] && $dataProjectPic[$b]['type'] == 'area') {
                            $totalItemAreaPhoto++;

                            // if(b == data.dataProjectPic.length-1){
                            //     reportContentAreaPhoto = reportContentAreaPhoto + '<div style="padding: 10px; vertical-align: top; "><img src="data.dataProjectPic[b].file_path+'" style="max-width: 300px; border:2px solid black; padding : 10px;"></div>';
                            // }else{

                            $res = fmod($totalItemAreaPhoto, 3);
                            if ($res == 0) {
                                $reportContentAreaPhoto = $reportContentAreaPhoto . '<td style="width: 190px;vertical-align: top">&nbsp;<div style="padding: 10px; vertical-align: middle; display : inline"><img src="' . $dataProjectPic[$b]['file_path'] . '" style="width: 190px; border:2px solid black; padding : 10px; vertical-align: top;"></div>&nbsp;</td></tr><tr>';

                            } else {
                                $reportContentAreaPhoto = $reportContentAreaPhoto . '<td style="width: 190px;vertical-align: top">&nbsp;<div style="padding: 10px; vertical-align: middle; display : inline"><img src="' . $dataProjectPic[$b]['file_path'] . '" style="width: 190px; border:2px solid black; padding : 10px; vertical-align: top;"></div>&nbsp;</td>';
                            }

                            // }
                        }

                    }

                    $reportContentAreaPhoto = $reportContentAreaPhoto . '</tr></table><br><br>';
                    if ($totalItemAreaPhoto == 0) {$reportContentAreaPhoto = '';}

                    $reportContent = $reportContent . '' . $reportContentAreaPhoto;

                    if ($reportContentIC == '' && $reportContentKP == '' && $reportContentMinReq == '') {$reportHtmlData = $reportHtmlData . '<center><h3>Configuration Data Not Found</h3></center><br>';};

                }
            } else {
                if ($chapter == 'lbr_pernyataan') {
                    $reportContent = '<div style="font-size: 12px"><h3 align="center">Lembar Pernyataan Cabang / Dealer</h3><br><br>
                    <table class="center">
                    <tbody>
                    <tr>
                    <td style="padding: 5px">Nama Outlet</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . $data_project->nama_outlet . '</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Dealer</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . $data_project->main_dealer . '</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Fungsi Outlet</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . $data_project->fungsi_outlet . '</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Type Pembangunan</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . $data_project->type_pembangunan . '</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Alamat</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . $data_project->alamat . '</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Telp/Fax</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">021 6515551</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">Target Otoritasi</td>
                    <td style="padding: 5px"> : </td>
                    <td style="padding: 5px">' . date('d F Y', strtotime($data_project->target_otorisasi)) . '</td>
                    </tr>
                    </tbody>
                    </table>
                    <p> Kami dengan ini menyatakan bahwa cabang kami telah melengkapi dan mengevaluasi seluruh fasilitas outlet di Area Sales dan After Sales sehingga telah sesuai standar yang terdapat dalam Buku Evaluasi Otorisasi Outlet Toyota.</p>
                    <p align="right">Jakarta , ' . strftime("%d %B %Y") . '</p>
                    <table align="center" border="2" width="80%">
                    <thead>
                    <tr style="background-color:#88a854; color: #000">
                    <th style="padding: 5px; text-align: center;" width="20%" colspan="3">Dinyatakan Oleh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 30px"></td>
                    <td style="padding: 30px"></td>
                    <td style="padding: 30px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 15px"></td>
                    <td style="padding: 15px"></td>
                    <td style="padding: 15px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">Kepala Cabang</td>
                    <td style="padding: 5px; text-align: center">Kepala Bengkel</td>
                    <td style="padding: 5px; text-align: center">Kepala Administrasi</td>
                    </tr>
                    </tbody>
                    </table><br><br>
                    <table align="center" border="2" width="80%">
                    <thead>
                    <tr style="background-color:#88a854; color: #000">
                    <th style="padding: 5px; text-align: center;" width="20%">Disetujui Oleh</th>
                    <th style="padding: 5px; text-align: center;" width="20%" colspan="2">Dinyatakan Oleh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 30px" width="40%"></td>
                    <td style="padding: 30px" width="30%"></td>
                    <td style="padding: 30px" width="30%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 15px"></td>
                    <td style="padding: 15px"></td>
                    <td style="padding: 15px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">Sales & Outlet Dev. Div. Head </td>
                    <td style="padding: 5px; text-align: center">Outlet Dev. Dept. Head</td>
                    <td style="padding: 5px; text-align: center">PIC – DDC MD</td>
                    </tr>
                    </tbody>
                    </table><br><br>
                    <table align="center" border="2" width="80%">
                    <thead>
                    <tr style="background-color:#88a854; color: #000">
                    <th style="padding: 5px; text-align: center;" width="20%">Disetujui Oleh</th>
                    <th style="padding: 5px; text-align: center;" width="20%" colspan="2">Dinyatakan Oleh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 30px" width="40%"></td>
                    <td style="padding: 30px" width="30%"></td>
                    <td style="padding: 30px" width="30%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 15px; text-align: center"><b>Bansar Maduma</b></td>
                    <td style="padding: 15px; text-align: center"><b>R. Abu Musa</b></td>
                    <td style="padding: 15px; text-align: center"><b>Ario Cahya G.</b></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">Outlet Devl. Dep. Div. Head</td>
                    <td style="padding: 5px; text-align: center">Outlet Phy. & Std. Sec. Head</td>
                    <td style="padding: 5px; text-align: center">PIC – DDC TAM</td>
                    </tr>
                    </tbody>
                    </table><div>';
                } elseif ($chapter == 'bab1') {
                    $reportContent = '<h3 align="center"><b>BAB I</b></h3>
                    <h3 align="center"><b>PENDAHULUAN</b></h3>
                    <h5><b>1. Tujuan</b></h5>
                    <p>Toyota dengan segenap jaringan outlet yang tersebar di Indonesia diharapkan dapat selalu memberikan suatu bentuk pelayanan yang terbaik kepada pelanggan baik dari layanan penjualan maupun layanan purna jual sebagai suatu bentuk komitmen untuk terus meningkatkan kepuasan pelanggan secara berkesinambungan. Oleh karena itu Toyota mengharapkan adanya suatu standar yang sama untuk seluruh outlet yang beroperasi dalam memberikan pelayanan, sehingga dalam operasinya perlu dipersiapkan suatu sistem yang baik dengan didukung oleh Fasilitas dan Sumber Daya Manusia yang memadai.</p>
                    <p>Untuk mewujudkan hal tersebut, PT. Toyota-Astra Motor mensyaratkan kepada outlet yang akan beroperasi agar dapat memenuhi seluruh kriteria Fasilitas dan Sumber Daya Manusia baik yang menyangkut pelayanan penjualan (Sales) maupun purna jual (After Sales) sebelum outlet tersebut beroperasi. Persyaratan-persyaratan tersebut wajib dilengkapi di dalam 3 tahap selama proses pembangunan Outlet Toyota. 3 Tahap tersebut adalah</p>
                    <h5><b>1. Establishment Progress Monitoring 50% (EPM 50%)</b></h5>
                    <h5><b>2.  Establishment Progress Monitoring 75% (EPM 75%)</b></h5>
                    <h5><b>3.  Establishment Progress Monitoring 100% (EPM 100%) atau Evaluasi Otorisasi</b></h5>
                    <p>Tahapan-tahapan tersebut bertujuan untuk syarat mendapatkan Status Otorisasi Outlet Toyota dengan mengikuti standar fasilitas dan Sumber Daya Manusia yang dikeluarkan oleh PT. Toyota Astra Motor.</p>
                    <h5><b>2. Prosedur Pelaksanaan Otorisasi</b></h5>
                    <img src="' . base_url('assets/prosedur_pelaksanaan_otorasi.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:600px">
                    <h5><b>3. Peta Lokasi</b></h5>
                    <p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; align: center"><b>Peta Provinsi</b></p>
                    <img src="' . base_url('assets/provinsi.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:400px">
                    <p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; ; align: center"><b>Peta Kota</b></p>
                    <img src="' . base_url('assets/lokasi.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:400px">';
                } elseif ($chapter == 'bab2') {
                    $reportContent = '<h3 align="center"><b>BAB II</b></h3>
                    <h3 align="center"><b>TARGET DAN INVESTASI</b></h3>
                    <h5><b>1. Target</b></h5>
                    <table class="center">
                    <tbody>
                    <tr>
                    <td style="padding: 10px">a.</td>
                    <td style="padding: 10px">Target Penjualan</td>
                    <td style="padding: 10px">:</td>
                    <td style="padding: 10px">6</td>
                    <td style="padding: 10px">Unit/Bulan</td>
                    </tr>
                    <tr>
                    <td style="padding: 10px">b.</td>
                    <td style="padding: 10px">Jumlah Salesman</td>
                    <td style="padding: 10px">:</td>
                    <td style="padding: 10px">12</td>
                    <td style="padding: 10px">Orang</td>
                    </tr>
                    <tr>
                    <td style="padding: 10px">b.</td>
                    <td style="padding: 10px">Target Servis</td>
                    <td style="padding: 10px">:</td>
                    <td style="padding: 10px">10</td>
                    <td style="padding: 10px">Unit/Hari</td>
                    </tr>
                    <tr>
                    <td style="padding: 10px; vertical-align: top">c.</td>
                    <td style="padding: 10px; vertical-align: top">Jumlah Stall GR</td>
                    <td style="padding: 10px; vertical-align: top">:</td>
                    <td style="padding: 10px" colspan="2">
                    <table width="100%">
                    <tbody>
                    <tr>
                    <td>EM</td>
                    <td> = </td>
                    <td align="right">4</td>
                    </tr>
                    <tr>
                    <td>SBNP</td>
                    <td> = </td>
                    <td align="right">12</td>
                    </tr>
                    <tr>
                    <td>GR</td>
                    <td> = </td>
                    <td align="right">5</td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    </tbody>
                    </table>
                    <table width="100%" border="2">
                    <tbody>
                    <tr>
                    <td style="padding: 5px; text-align: center" width="13%"></td>
                    <td style="padding: 5px; text-align: center" width="13%">Y</td>
                    <td style="padding: 5px; text-align: center" width="13%">Y+1</td>
                    <td style="padding: 5px; text-align: center" width="13%">Y+2</td>
                    <td style="padding: 5px; text-align: center" width="13%">Y+3</td>
                    <td style="padding: 5px; text-align: center" width="13%">Y+4</td>
                    <td style="padding: 5px; text-align: center" width="13%">Y+5</td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">Market/Tahun</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">Sales/Bulan</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px; text-align: center">UE GR/Hari</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    </tbody>
                    </table><br>
                    <b>*Y=Tahun Otorisasi</b>
                    <h5><b>2. Investasi</b></h5>
                    <table class="center">
                    <tbody>
                    <tr>
                    <td style="padding: 10px">a.</td>
                    <td style="padding: 10px">Land Total</td>
                    <td style="padding: 10px">:</td>
                    <td style="padding: 10px">6</td>
                    <td style="padding: 10px">m2</td>
                    </tr>
                    <tr>
                    <td style="padding: 10px">b.</td>
                    <td style="padding: 10px">Building Total</td>
                    <td style="padding: 10px">:</td>
                    <td style="padding: 10px">12</td>
                    <td style="padding: 10px">m2</td>
                    </tr>
                    <tr>
                    <td style="padding: 10px; vertical-align: top">c.</td>
                    <td style="padding: 10px" colspan="4">Investasi
                    <table width="100%">
                    <tbody>
                    <tr>
                    <td> - </td>
                    <td>Land</td>
                    <td> : </td>
                    <td>Rp</td>
                    <td></td>
                    </tr>
                    <tr>
                    <td> - </td>
                    <td>Building</td>
                    <td> : </td>
                    <td>Rp</td>
                    <td></td>
                    </tr>
                    <tr>
                    <td> - </td>
                    <td>Equipment</td>
                    <td> : </td>
                    <td>Rp</td>
                    <td></td>
                    </tr>
                    <tr>
                    <td> - </td>
                    <td>TOTAL</td>
                    <td> : </td>
                    <td>Rp</td>
                    <td></td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    </tbody>
                    </table>';
                } elseif ($chapter == 'bab3') {
                    $reportContent = '<h3 align="center"><b>BAB III</b></h3>
                    <h3 align="center"><b>SUMBER DAYA MANUSIA</b></h3>
                    <h5><b>1. Struktur Organisasi</b></h5>
                    <p>Catatan<b></b></p>
                    <p style="padding-right: 5px">a. Seluruh manpower yang berada di cabang dimasukan ke dalam struktur organisasi baik sales, after sales dan supporting.<p>
                    <img src="' . base_url('assets/struktur_organisasi.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top">
                    <p style="padding-right: 5px">b.  Apabali terdapat bentuk bagan struktur organisasi yang berbeda, dapat melampirkan struktur yang sesuai.</p>
                    <h5><b>2. Klasifikasi Sumber Daya Manusia Area Sales </b></h5>
                    <div class="table-responsive">
                    <small><small><table border="2" style="font-size: 10px">
                    <thead>
                    <tr>
                    <th style="padding: 0px; text-align: center" rowspan="4">No</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Nama Karyawan</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Jabatan</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Jenis Kelamin (L/P)</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Tanggal Lahir</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Agama</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Pendidikan Terakhir</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">No Telepon</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Tanggal Masuk</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">No. ID TAM (*Jika Punya)</th>
                    <th style="padding: 0px; text-align: center" rowspan="4">Cabang Asal (*Jika Mutasi)</th>
                    <th style="padding: 0px; text-align: center" colspan="20">Tanggal Training</th>
                    <th style="padding: 0px; text-align: center" colspan="2">(*Untuk CS & SM)</th>
                    </tr>
                    <tr>
                    <th style="padding: 0px; text-align: center" rowspan="2" colspan="3">CRC / Partsman</th>
                    <th style="padding: 0px; text-align: center" colspan="10">Training &#60; tahun 2011</th>
                    <th style="padding: 0px; text-align: center" colspan="7">Training >= tahun 2011</th>
                    <th style="padding: 0px; text-align: center" rowspan="3">Grade</th>
                    <th style="padding: 0px; text-align: center" rowspan="3">Kategori Sales</th>
                    </tr>
                    <tr>
                    <th style="padding: 0px; text-align: center" colspan="4">Salesman & Countersales</th>
                    <th style="padding: 0px; text-align: center" colspan="3">Supervisor</th>
                    <th style="padding: 0px; text-align: center" colspan="3">Kepala Cabang</th>
                    <th style="padding: 0px; text-align: center" colspan="2">Salesman & Countersales</th>
                    <th style="padding: 0px; text-align: center" colspan="3">Supervisor</th>
                    <th style="padding: 0px; text-align: center" colspan="2">Kepala Cabang</th>
                    </tr>
                    <tr>
                    <th style="padding: 0px; text-align: center">Level 1</th>
                    <th style="padding: 0px; text-align: center">Level 2</th>
                    <th style="padding: 0px; text-align: center">Level 3</th>
                    <th style="padding: 0px; text-align: center">FST</th>
                    <th style="padding: 0px; text-align: center">BSST</th>
                    <th style="padding: 0px; text-align: center">SPD</th>
                    <th style="padding: 0px; text-align: center">ANS</th>
                    <th style="padding: 0px; text-align: center">SAM 1</th>
                    <th style="padding: 0px; text-align: center">SAM 2</th>
                    <th style="padding: 0px; text-align: center">SAM 3</th>
                    <th style="padding: 0px; text-align: center">DOM 1</th>
                    <th style="padding: 0px; text-align: center">DOM 2</th>
                    <th style="padding: 0px; text-align: center">DOM 3</th>
                    <th style="padding: 0px; text-align: center">PSST</th>
                    <th style="padding: 0px; text-align: center">ASST</th>
                    <th style="padding: 0px; text-align: center">BST</th>
                    <th style="padding: 0px; text-align: center">MSTT</th>
                    <th style="padding: 0px; text-align: center">SST</th>
                    <th style="padding: 0px; text-align: center">DFMT</th>
                    <th style="padding: 0px; text-align: center">DBMT</th>
                    </tr>
                    </thead>
                    <tbody>';
                    $reportContent = $reportContent . '';
                    $b             = 1;
                    for ($i = 0; $i < 40; $i++) {
                        $reportContent = $reportContent . '<tr>
                        <td style="padding: 0px; text-align: center">' . $b++ . '</td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        </tr>';
                    }
                    $reportContent = $reportContent . '';

                    $reportContent = $reportContent . '</tbody>
                    </table></small></small><br></div>
                    <small><small>
                    <p>Cara Pengisian
                    <br>1.  <b>Tanggal masuk</b> adalah tanggal masuk karyawan ke Toyota
                    <br>2.  Untuk kolom-kolom <b>training yang</b> di ikuti di isi dengan bulan dan tahun lulus
                    <br>3.  <b>Kolom cabang asal</b> dan <b>posisi asal</b> diisi apabila karyawan tersebut adalah rotasi/mutasi dari cabang lain
                    <br>4.  <b>Kategori sales</b> diisikan jenis kategorinya (Umum, Dyna, atau sebutkan jika ada yang lain)
                    <br>5.  <b>Grade</b> diisikan jabatan salesman (Trainee, Junior, Executive, Senior)</p></small></small>
                    <div style="page-break-before: always"></div>
                    <p>Keterangan :</p>
                    <small><small><table width="100%">
                    <tbody>
                    <tr>
                    <td align="left" width="15%" style="vertical-align: top"><p>CRC</p>L1: Level 1 Basic<br>L2: Level 2 Intermediate<br>L3: Level 3 Advance</td>
                    <td align="left" width="15%" style="vertical-align: top"><p>SUKU CADANG & PART</p>L1: Level 1<br>L2: Level 2<br>L3: Level 3</td>
                    <td align="left" width="25%" style="vertical-align: top"><p>SALESMAN & COUNTER SALES</p>FST : First Selling Training<br>BSST : Basic Sellig Step Training<br>SPD : Sales Potential Dealer<br>ANS : Advance Negotiation Skill<br>PSST : Personal Selling Skill Training<br>ASST :  Advance Selling Skill Training
                    <td align="left" width="25%" style="vertical-align: top"><p>SALES SUPERVISOR</p>FST : First Selling Training<br>BSST : Basic Sellig Step Training<br>SPD : Sales Potential Dealer<br>ANS : Advance Negotiation Skill<br>BST : Basic Supervisory Training)<br>MSTT : Managing Supervisory Task Training)<br>SST : Strategic Supervisory Training
                    <td align="left" width="20%" style="vertical-align: top"><p>KEPALA CABANG</p>DOM : Dealer Operation Management<br>DFMT : Dealer Function Management Training<br>DBMT : Dealer Branch Management Training</td>
                    </tbody>
                    </table></small></small>
                    <div style="page-break-before: always"></div>
                    <h5><b>3. Klasifikasi Sumber Daya Manusia Area After Sales  </b></h5>
                    <p>1. General Repair</p>
                    <div class="table-responsive">
                    <small><small><table border="2">
                    <thead>
                    <tr>
                    <th style="padding: 3px; text-align: center" rowspan="4">No</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Nama Karyawan</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Tanggal Lahir</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Agama</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Pendidikan Terakhir</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">No Telepon</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Tanggal Masuk</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">Jabatan</th>
                    <th style="padding: 3px; text-align: center" rowspan="4">No. ID TAM (*Jika Punya)</th>
                    <th style="padding: 3px; text-align: center" colspan="18">Tanggal Training</th>
                    </tr>
                    <tr>
                    <th style="padding: 3px; text-align: center" colspan="7">Teknisi</th>
                    <th style="padding: 3px; text-align: center" rowspan="2" colspan="3">SA GR</th>
                    <th style="padding: 3px; text-align: center" rowspan="2" colspan="3">PARTMANT</th>
                    <th style="padding: 3px; text-align: center" rowspan="2" colspan="3">FOREMAN</th>
                    <th style="padding: 3px; text-align: center" rowspan="2" colspan="2">KABENG</th>
                    </tr>
                    <tr>
                    <th style="padding: 3px; text-align: center" colspan="2">LEVEL 1</th>
                    <th style="padding: 3px; text-align: center" colspan="3">LEVEL 2</th>
                    <th style="padding: 3px; text-align: center" colspan="2">LEVEL 3</th>
                    </tr>
                    <tr>
                    <th style="padding: 3px; text-align: center">TT</th>
                    <th style="padding: 3px; text-align: center">PT</th>
                    <th style="padding: 3px; text-align: center">DTG</th>
                    <th style="padding: 3px; text-align: center">DTL</th>
                    <th style="padding: 3px; text-align: center">DTC</th>
                    <th style="padding: 3px; text-align: center">DMT</th>
                    <th style="padding: 3px; text-align: center">LD</th>
                    <th style="padding: 3px; text-align: center">L1</th>
                    <th style="padding: 3px; text-align: center">L2</th>
                    <th style="padding: 3px; text-align: center">L3</th>
                    <th style="padding: 3px; text-align: center">P-L1</th>
                    <th style="padding: 3px; text-align: center">P-L2</th>
                    <th style="padding: 3px; text-align: center">P-L3</th>
                    <th style="padding: 3px; text-align: center">FO-L1</th>
                    <th style="padding: 3px; text-align: center">FO-L2</th>
                    <th style="padding: 3px; text-align: center">FO-L3</th>
                    <th style="padding: 3px; text-align: center">TSMTL1</th>
                    <th style="padding: 3px; text-align: center">TSMTL2</th>
                    </tr>
                    </thead>
                    <tbody>';
                    $reportContent = $reportContent . '';
                    $a             = 1;
                    for ($i = 0; $i < 40; $i++) {
                        $reportContent = $reportContent . '<tr>
                        <td style="padding: 0px; text-align: center">' . $a++ . '</td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        <td style="padding: 0px; text-align: center"></td>
                        </tr>';
                    }
                    $reportContent = $reportContent . '';
                    $reportContent = $reportContent . '</tbody>
                    </table></small></small><br></div>
                    <p>Keterangan :</p>
                    <small><small><table width="100%">
                    <tbody>
                    <tr>
                    <td align="left" width="15%" style="vertical-align: top"><p>TRAINING TEKNISI/FO</p>TT   : Toyota Technician<br>PT  : Pro - Technician<br>DT  : Diagnosis Technician<br>- DTG : Diagnosis Technician Engine<br>- DTL  : Diagnosis Technician Electical<br>- DTC : Diagbosis Technician Chassis<br>DMT : Diagnosis Master Technician<br>LD  : Latest Diagnosis<br></td>
                    <td align="left" width="20%" style="vertical-align: top"><p>KEPALA BENGKEL</p>TSMTL 1 : Toyota Service Managament Training (Managing Basic Operation)<br>TSMTL 2 : Toyota Service Managament Training (Managing & Improving Service & CS)<br></td>
                    <td align="left" width="25%" style="vertical-align: top"><p>FOREMAN</p>L1   : Level 1 Toyota Foremanship Training (Basic Procedure Standard & Leadership)<br>L2  : Level 2 Toyota Foremanship Training (Motivate & Develop Team)<br>L3  : Level 3 Toyota Foremanship Training (MRA – SA – PART Sychronization & Kaizen)<br></td>
                    <td align="left" width="15%" style="vertical-align: top"><p>SERVICE ADVISOR GR</p>L1    : Level 1 TSA21<br>L2  : Level 2 TSA21<br>L3  : Level 3 TSA21<br></td>
                    <td align="left" width="25%" style="vertical-align: top"><p>PARTMAN</p>L1   : Level 1 (Basic Part Operation)<br>L2  : Level 2 (Smooth Process of Parts & Synchronization)<br>L3  : Level 3 (Efficiant & Profitable Parts Operation)<br></td>
                    </tbody>
                    </table></small></small>';
                } elseif ($chapter == 'bab4') {
                    $reportContent = '<div style="font-size: 12px"><h3 align="center"><b>BAB IV</b></h3>
                    <h3 align="center"><b>FASILITAS UMUM</b></h3>
                    <p><b>Cara Pengisian</b></p>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/cara2.png') . '" style="padding : 10px; width: 590px"></td></tr></table><br>
                    <b>Keterangan</b>
                    <br>1. <b>Lampirkan foto dengan resolusi yang jelas</b> sesuai dengan keterangan di bawah foto
                    <p>2.  Lakukan pengecekan sesuai dengan kolom  yang disediakan :
                    <p><b>Kolom #1 diisi oleh (Tipe user yang dipilih untuk kolom 1 disini)</b></p>
                    <p><b>Kolom #2 diisi dan di cek oleh (Tipe user yang dipilih untuk kolom 2 disini)</b></p>
                    <p><b>Kolom #3 diisi dan dicek oleh (Tipe user yang dipilih untuk kolom 3 disini)</b></p>
                    <p>- bubuhkan  tanda tick    <b>( v )</b> = apabila kriteria minimum sudah sesuai actual</p>
                    <p>- bubuhkan  tanda cross <b>( X )</b> = apabila kriteria minimum tidak sesuai actual </p><div style="page-break-before: always"></div>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
                    <b>1. PERSPEKTIF OUTLET</b>
                    </div><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/perspektif.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table>
                    <p align="center">Foto menunjukkan perspektif outlet secara keseluruhan dari depan</p>
                    <table border="1" align="center" width="80%">
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 5px; text-align: center" width="5%">No</th>
                    <th style="padding: 5px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 5px; text-align: center" width="15%">#1</th>
                    <th style="padding: 5px; text-align: center" width="15%">#2</th>
                    <th style="padding: 5px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 5px">1.</td>
                    <td style="padding: 5px">Tampak depan outlet terlihat cukup jelas dari jalan</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">2.</td>
                    <td style="padding: 5px">Primary Sign dapat terlihat jelas dari jalan umum</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">3.</td>
                    <td style="padding: 5px">Pemasangan umbul-umbul</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tbody>
                    </table><br>
                    </div><br>
                    <div style="border: 2px solid black; padding: 5px">
                    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
                    <b>2. PRIMARY SIGN</b>
                    </div><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/bab4.png') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table>
                    <p align="center">Foto menunjukkan Primary sign </p>
                    <table border="1" align="center" width="80%">
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 5px; text-align: center" width="5%">No</th>
                    <th style="padding: 5px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 5px; text-align: center" width="15%">#1</th>
                    <th style="padding: 5px; text-align: center" width="15%">#2</th>
                    <th style="padding: 5px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 5px">1.</td>
                    <td style="padding: 5px">Lokasi ID sign sesuai KPD </td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">2.</td>
                    <td style="padding: 5px">Lampu ID sign berfungsi dengan baik</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">3.</td>
                    <td style="padding: 5px">Keterangan fungsi outlet sesuai dengan ketentuan otorisasi</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tbody>
                    </table><br>
                    </div></div>';
                } elseif ($chapter == 'bab5') {
                    $reportContent = '<h3 align="center"><b>BAB V</b></h3>
                    <h3 align="center"><b>AREA PENJUALAN (SALES)</b></h3>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
                    <b>1. PINTU MASUK SHOWROOM </b>
                    </div><br><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/pintumasuk.jpg') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table><br>
                    <p align="center">Foto pintu masuk showroom</p>
                    <table border="1" align="center" width="80%"><br><br>
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 5px; text-align: center" width="5%">No</th>
                    <th style="padding: 5px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 5px; text-align: center" width="15%">#1</th>
                    <th style="padding: 5px; text-align: center" width="15%">#2</th>
                    <th style="padding: 5px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 5px">1.</td>
                    <td style="padding: 5px">Terdapat teras dan kanopi di depan pintu masuk</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">2.</td>
                    <td style="padding: 5px">Terdapat informasi jam operasional showroom</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">3.</td>
                    <td style="padding: 5px">Lebar pintu masuk minimal 1,6 dan tinggi minimal 2,4 m</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tbody>
                    </table>
                    </div><div style="page-break-before: always"></div>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
                    <b>2. AREA GENERAL SHOWROOM </b>
                    </div><br><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/areageneralshowroom.jpg') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table><br>
                    <p align="center">Foto menunjukkan interior didalam showroom </p>
                    <table border="1" align="center" width="80%"><br><br>
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 5px; text-align: center" width="5%">No</th>
                    <th style="padding: 5px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 5px; text-align: center" width="15%">#1</th>
                    <th style="padding: 5px; text-align: center" width="15%">#2</th>
                    <th style="padding: 5px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 5px">1.</td>
                    <td style="padding: 5px">Terdapat unit display dilengkapi name plate, spec & catalog stand, product display, aksesoris toyota dilengkapi dengan label nama dan harga</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">2.</td>
                    <td style="padding: 5px">Tersedia AC untuk kenyamanan pelanggan</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">3.</td>
                    <td style="padding: 5px">Terdapat spesial display dengan ring diatas unit dan lampu spotlight yang dapat menyala (jika ada)</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">4.</td>
                    <td style="padding: 5px">Pemasangan lantai baik dan rapih</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">5.</td>
                    <td style="padding: 5px">Kaca showroom tidak ada yang retak</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">6.</td>
                    <td style="padding: 5px">Tersedia fasilitas APAR di dalam showroom dengan keterangan batas kadaluarsa</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">7.</td>
                    <td style="padding: 5px">Semua lampu di showroom berfungsi dengan baik</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 5px">8.</td>
                    <td style="padding: 5px">Terdapat ruang panel dan ruang server sesuai dengan KPD</td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    <td style="padding: 5px"></td>
                    </tr>
                    <tbody>
                    </table>
                    </div>';
                } elseif ($chapter == 'bab6') {
                    $reportContent = '<h3 align="center"><b>BAB VI</b></h3>
                    <h3 align="center"><b>AREA PURNA JUAL (AFTER SALES)</b></h3>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
                    <b>1. STALL PENERIMAAN SERVIS </b>
                    </div><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/stallpenerimaanservice.jpg') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table><br>
                    <p align="center">Foto menunjukkan stall penerimaan servis</p>
                    <table border="1" align="center" width="80%"><br><br>
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 5px; text-align: center" width="5%">No</th>
                    <th style="padding: 5px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 5px; text-align: center" width="15%">#1</th>
                    <th style="padding: 5px; text-align: center" width="15%">#2</th>
                    <th style="padding: 5px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 2px">1.</td>
                    <td style="padding: 2px">Terdapat kanopi pada bagian atas</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">2.</td>
                    <td style="padding: 2px">Memiliki penerangan 350 lux</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">3.</td>
                    <td style="padding: 2px">Ukuran Stall 3.5 x 6 m, garis batas stall 10 cm (putih) dan tinggi plafond min. 4 m</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">4.</td>
                    <td style="padding: 2px">Terdapat sign board “STALL PENERIMAAN”</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">5.</td>
                    <td style="padding: 2px">Keramik menggunakan warna hijau atau merah bata unpolish</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">6.</td>
                    <td style="padding: 2px">Terdapat lemari/rak penyimpanan (seat cover & floor mat)</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">7.</td>
                    <td style="padding: 2px">Terdapat informasi jam operasional di area penerimaan <br>-referensi ukuran : 50 x 40 cm</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tbody>
                    </table>
                    </div><div style="page-break-before: always"></div>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 2px; background-color: yellow; max-width:300px">
                    <b>2. AREA PENCATATAN SERVICE </b>
                    </div><br><br>
                    <table width="100%"><tr><td align="center"><img src="' . base_url('assets/areageneralshowroom.jpg') . '" style="border:2px solid black; padding : 10px; vertical-align: top; max-width:300px"></td></tr></table><br>
                    <p align="center">Foto menunjukkan area pencatatan servis dan booking</p>
                    <table border="1" align="center" width="80%"><br><br>
                    <thead>
                    <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 2px; text-align: center" width="5%">No</th>
                    <th style="padding: 2px; text-align: center" width="50%">Kriteria Minimun</th>
                    <th style="padding: 2px; text-align: center" width="15%">#1</th>
                    <th style="padding: 2px; text-align: center" width="15%">#2</th>
                    <th style="padding: 2px; text-align: center" width="15%">#3</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="padding: 2px">1.</td>
                    <td style="padding: 2px">Tersedia sign board “PENCATATAN SERVIS”</td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">2.</td>
                    <td style="padding: 3px">Terdapat meja dan kursi untuk SA (sesuai KPD)</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">3.</td>
                    <td style="padding: 3px">Terdapat kursi untuk pelanggan</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">4.</td>
                    <td style="padding: 3px">Terdapat kursi untuk menunggu sebelum diterima SA</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">5.</td>
                    <td style="padding: 3px">Terdapat papan  menu servis , Appointment Scheduling Board, Appoinment Preparation Board, Today Appointment Board, JPCB</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">6.</td>
                    <td style="padding: 3px">Terdapat seperangkat komputer dan telepon dengan akses internal dan eksternal</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">7.</td>
                    <td style="padding: 3px">Kapasitas cahaya 400 lux</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">8.</td>
                    <td style="padding: 3px">Terdapat 1 unit printer untuk setiap 3 SA</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">9.</td>
                    <td style="padding: 3px">Terdapat counter atau mesin antrian otomatis dengan ketentuan U/E ≥ 30/hari</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 3px">10.</td>
                    <td style="padding: 3px">Dinding batas ke area bengkel menggunakan material kaca</td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    <td style="padding: 3px"></td>
                    </tr>
                    <tbody>
                    </table>
                    </div>';
                } elseif ($chapter == 'bab7') {
                    $reportContent = '<div style="font-size: 10px"><h3 align="center"><b>BAB VII</b></h3>
                    <h3 align="center"><b>LAMPIRAN</b></h3>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 2px; background-color: yellow; max-width:250px">
                    <b>1. EXECUTIVE SUMMARY </b>
                    </div><br><br>
                    <p align="center"><b>Outlet Authorized Executive Summary</b></p>
                    <small><small><table style="padding: 2px" width="100%" border="1">
                    <tr>
                    <td style="padding: 2px" colspan="2"><b>I. General</b></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" width="15%">Outlet Name</td>
                    <td style="padding: 2px" width="40%"> : </td>
                    <td style="padding: 2px" width="25%">- No. of parking stall</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Function</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px">Service</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Date of Authorized</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px">Customer Car / Motorcycle</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Address</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px">Employee Car / Motorcycle</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Phone</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px" colspan="2"><b>V. Man Power</b></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Fax</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px" colspan="2"> - Management</td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Sales</td>
                    <td style="padding: 2px"> : </td>
                    <td style="padding: 2px; padding-left: 30px">Branch Head</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" colspan="2"><b>II. Investasi</b></td>
                    <td style="padding: 2px; padding-left: 30px">Service Head</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Land Total</td>
                    <td style="padding: 2px; padding-left: 30px"> m2 </td>
                    <td style="padding: 2px; padding-left: 30px">Administration Head</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Building Total</td>
                    <td style="padding: 2px; padding-left: 30px"> m2 </td>
                    <td style="padding: 2px" colspan="2"> - Sales (No. of person current/room capacity</td>
                    </tr>
                    <tr>
                    <td style="padding: 2px">Invesment Total</td>
                    <td style="padding: 2px"> Rp. </td>
                    <td style="padding: 2px; padding-left: 30px">Sales Supervisor/room capacity</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Land</td>
                    <td style="padding: 2px"> Rp. </td>
                    <td style="padding: 2px; padding-left: 30px">Sales Man/room capacity</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Buliding</td>
                    <td style="padding: 2px"> Rp. </td>
                    <td style="padding: 2px; padding-left: 30px">Counter Sales/room capacity</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Equipment</td>
                    <td style="padding: 2px"> Rp. </td>
                    <td style="padding: 2px" colspan="2"> - Service</td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" colspan="2"><b>III. Target</b></td>
                    <td style="padding: 2px">GR</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 1px" rowspan="7" colspan="2">
                    <table width="100%" border="1">
                    <tbody>
                    <tr>
                    <td style="padding: 1px; text-align: center" width="13%"></td>
                    <td style="padding: 1px; text-align: center" width="13%">Y</td>
                    <td style="padding: 1px; text-align: center" width="13%">Y+1</td>
                    <td style="padding: 1px; text-align: center" width="13%">Y+2</td>
                    <td style="padding: 1px; text-align: center" width="13%">Y+3</td>
                    <td style="padding: 1px; text-align: center" width="13%">Y+4</td>
                    </tr>
                    <tr>
                    <td style="padding: 1px">Market (/year)</td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 1px"> - Sales (/mth)</td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 1px" colspan="6"> - Service</td>
                    </tr>
                    <tr>
                    <td style="padding: 1px; padding-left: 20px"> - UE GR (/day)</td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 1px; padding-left: 20px"> - UE BP (/day)</td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    <td style="padding: 1px"></td>
                    </tr>
                    </tbody>
                    </table>
                    <br><b>Y = Year of authorization</b>
                    </td>
                    <td style="padding: 2px; padding-left: 30px">Service Supervisor</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Technical Leader</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Foreman</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">PTM</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Technician</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Service Advisor</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px">Partsman</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" colspan="2"><b>IV. Facility</b></td>
                    <td style="padding: 2px">BP</td>
                    <td style="padding: 2px"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" width="15%"> - Sales</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 30px" width="25%">Service Supervisor</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Unit display</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 30px" width="25%">Foreman</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Stock capacity</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 30px" width="25%">Technician</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" width="15%"> - Production Stall</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 50px" width="25%">Body</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">EM</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 50px" width="25%">Paint</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">GR</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px" width="25%">Others</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Dyna/LC</td>
                    <td style="padding: 2px" width="40%"></td>
                    <td style="padding: 2px; padding-left: 50px" width="25%">Paint</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%" colspan="2">Allocation for Expansion</td>
                    <td style="padding: 2px; padding-left: 50px" width="25%">Paint</td>
                    <td style="padding: 2px" width="20%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">No. of lift</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px" width="15%"> - Other Stall</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">PDS/DIO</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Spooring</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Washing for service</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    <tr>
                    <td style="padding: 2px; padding-left: 30px" width="15%">Washing for new car</td>
                    <td style="padding: 2px" width="40%"></td>
                    </tr>
                    </table></small></small>
                    <p><b>* File ms Excel terlampir</b></p>
                    </div><div style="page-break-before: always"></div>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 2px; background-color: yellow; max-width:250px">
                    <b>2. DOKUMEN PENDUKUNG </b>
                    </div>
                    <p>Sebagai data pendukung untuk proses otorisasi outlet, maka kami mohon bantuan pihak Dealer / Dealer untuk melampirkan <br> dokumen-dokumen seperti berikut, yaitu : <br><br>a.  Akta Pendirian Cabang<br>b.  Nomor Pokok Wajib Pajak (NPWP) Cabang<br>c.  Tanda Daftar Perusahaan (TDP) Cabang<br>d.  Surat Ijin Usaha Perdagangan (SIUP) Cabang<br>e.  Surat Keterangan Domisili Cabang<br><br>NOTE : <br>Seluruh kelengkapan dokumen yang dilampirkan adalah kelengkapan dokumen yang beralamat atau atas nama cabang outlet baru (Bukan Kantor Pusat). Apabila ada beberapa dokumen yang <b>menjadi satu dengan Kantor Pusat tidak perlu dilampirkan</b>, yang dilampirkan hanya yang beralamat atau atas nama cabang baru.<br></p>
                    </div><br><br>
                    <div style="border: 2px solid black; padding: 10px">
                    <div style="border: 2px solid black; padding: 2px; background-color: yellow; max-width:250px">
                    <b>3. PENUTUPAN SALES SERVICE POINT (SSP)</b>
                    </div>
                    <p>NOTE : Lampirkan foto SSP yang sudah tidak beroperasi  apabila outlet tersebut memiliki SSP</p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <p align="center">Foto SSP yang sudah tidak beropersi/tutup</p>

                    </div></div><br><br>';
                }
            }

        } else {
            $reportCover   = '';
            $reportContent = '';
        }

        $htmlReport = $htmlReport . '' . $reportCover . '' . $reportContent;

        // $reportCover;
        // die();

        ob_end_clean();

        $fileName = 'tes.pdf';
        if ($chapter == 'bab3') {
            $paperOrientation = 'landscape';
            $this->pdf->base_html($htmlReport, $fileName, 'A3', $paperOrientation);
        } else {
            $paperOrientation = 'potrait';
            $this->pdf->base_html($htmlReport, $fileName, '', $paperOrientation);
        }

    }

    public function generate_pdf_report_summary($report_type, $param = null, $param2 = null)
    {

        $this->load->library('pdf');
        $htmlReport = '';

        if ($report_type == 'checklist_summary') {

            $idProject = $param;
            $jenisEpm  = $param2;

            $data_project = $this->data_project($idProject)->row();

            $dataArea               = $this->data_project_area($idProject)->result_array();
            $dataCatatan            = $this->data_project_catatan($idProject)->result_array();
            $dataItemChecklist      = $this->read_ic_where($idProject, $jenisEpm)->result_array();
            $dataKriteriaPekerjaan  = $this->read_kp_where($idProject, $jenisEpm)->result_array();
            $dataMinimumRequirement = $this->read_minreq_where($idProject, $jenisEpm)->result_array();
            $dataKet                = $this->read_ket_where($idProject, $jenisEpm)->result_array();
            $dataProjectPic         = $this->read_project_pic()->result_array();

            ob_start();
            setlocale(LC_ALL, 'IND');

            $reportContent     = '';
            $oldHeaderAreaName = '';

            for ($i = 0; $i < count($dataArea); $i++) {

                //header content
                if ($oldHeaderAreaName == $dataArea[$i]['area_name']) {
                    $reportContentHeaderSubArea = '<h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000"><b>' . $dataArea[$i]['position'] . '. ' . $dataArea[$i]['sub_area_name'] . '</b></h4><br>';
                    $reportContent              = $reportContent . '' . $reportContentHeaderSubArea;
                } else {
                    $reportContentHeaderArea = '<h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f"><b>' . $dataArea[$i]['area_name'] . '</b></h3>';
                    $reportContent           = $reportContent . '' . $reportContentHeaderArea;

                    $reportContentHeaderSubArea = '<h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000"><b>' . $dataArea[$i]['position'] . '. ' . $dataArea[$i]['sub_area_name'] . '</b></h4><br>';
                    $reportContent              = $reportContent . '' . $reportContentHeaderSubArea;

                    $oldHeaderAreaName = $dataArea[$i]['area_name'];
                }

                //item checklist

                //ini buat item checklist model
                $checklistModel = '';
                if (count($dataItemChecklist) > 0) {
                    for ($x = 0; $x < count($dataItemChecklist); $x++) {

                        if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {
                            $checklistModel = $dataItemChecklist[$x]['model'];
                            break;
                        }
                    }
                }

                //ini buat item checklist content
                $totalItemIC = 0;
                if (count($dataItemChecklist) > 0 && $checklistModel == 'Model 1') {

                    $reportContentIC = '<table align="center" border="2" width="100%" border="1">
                    <thead>
                    <tr style="background-color:#99c0ff; color: #000">
                    <th style="text-align: center;">Item Check</th>
                    <th style="text-align: center;">Kriteria</th>
                    <th width="40%" colspan="3">
                    <table border="1" width="100%">
                    <thead>
                    <tr>
                    <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                    </tr>
                    <tr>
                    <th style="text-align: center;">#1</th>
                    <th style="text-align: center;">#2</th>
                    <th style="text-align: center;">#3</th>
                    </tr>
                    </thead>
                    </table>
                    </th>
                    </tr>
                    </thead>
                    <tbody>';

                    for ($x = 0; $x < count($dataItemChecklist); $x++) {

                        if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {

                            if ($dataItemChecklist[$x]['score_konsultan'] == '0') {
                                $scoreKonsultan = 'X';
                            } else if ($dataItemChecklist[$x]['score_konsultan'] == '1') {
                                $scoreKonsultan = 'V';
                            } else {
                                $scoreKonsultan = '';
                            }

                            if ($dataItemChecklist[$x]['score_dealer'] == '0') {
                                $scoreDealer = 'X';
                            } else if ($dataItemChecklist[$x]['score_dealer'] == '1') {
                                $scoreDealer = 'V';
                            } else {
                                $scoreDealer = '';
                            }

                            if ($dataItemChecklist[$x]['score_tam'] == '0') {
                                $scoreTam = 'X';
                            } else if ($dataItemChecklist[$x]['score_tam'] == '1') {
                                $scoreTam = 'V';
                            } else {
                                $scoreTam = '';
                            }

                            $reportContentIC = $reportContentIC . '<tr>
                            <td style="padding: 7px">' . $dataItemChecklist[$x]['item_name'] . '</td>
                            <td style="padding: 7px">' . $dataItemChecklist[$x]['kriteria'] . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                            </tr>';

                            $totalItemIC++;
                        }
                    }

                    $reportContentIC = $reportContentIC . '</tbody>
                    </table><br><br>';

                } else if (count($dataItemChecklist) > 0 && $checklistModel == 'Model 2') {

                    $reportContentIC = '<table align="center" border="2" width="100%" border="1">
                    <thead>
                    <tr style="background-color:#99c0ff; color: #000">
                    <th style="text-align: center;">Item Check</th>
                    <th style="text-align: center;">Kriteria</th>
                    <th style="text-align: center;">Jarak</th>
                    <th width="40%" colspan="3">
                    <table border="1" width="100%">
                    <thead>
                    <tr>
                    <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                    </tr>
                    <tr>
                    <th style="text-align: center;">#1</th>
                    <th style="text-align: center;">#2</th>
                    <th style="text-align: center;">#3</th>
                    </tr>
                    </thead>
                    </table>
                    </th>
                    </tr>
                    </thead>
                    <tbody>';

                    for ($x = 0; $x < count($dataItemChecklist); $x++) {

                        if ($dataItemChecklist[$x]['id_project_area'] == $dataArea[$i]['id']) {

                            if ($dataItemChecklist[$x]['score_konsultan'] == '0') {
                                $scoreKonsultan = 'X';
                            } else if ($dataItemChecklist[$x]['score_konsultan'] == '1') {
                                $scoreKonsultan = 'V';
                            } else {
                                $scoreKonsultan = '';
                            }

                            if ($dataItemChecklist[$x]['score_dealer'] == '0') {
                                $scoreDealer = 'X';
                            } else if ($dataItemChecklist[$x]['score_dealer'] == '1') {
                                $scoreDealer = 'V';
                            } else {
                                $scoreDealer = '';
                            }

                            if ($dataItemChecklist[$x]['score_tam'] == '0') {
                                $scoreTam = 'X';
                            } else if ($dataItemChecklist[$x]['score_tam'] == '1') {
                                $scoreTam = 'V';
                            } else {
                                $scoreTam = '';
                            }

                            $jarakVal = '';
                            if ($dataItemChecklist[$x]['jarak'] !== '' && $dataItemChecklist[$x]['jarak'] !== null) {
                                $jarakVal = $dataItemChecklist[$x]['jarak'];
                            }

                            $reportContentIC = $reportContentIC . '<tr>
                            <td style="padding: 7px">' . $dataItemChecklist[$x]['item_name'] . '</td>
                            <td style="padding: 7px">' . $dataItemChecklist[$x]['kriteria'] . '</td>
                            <td style="padding: 7px">' . $jarakVal . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                            <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                            </tr>';

                            $totalItemIC++;
                        }
                    }

                    $reportContentIC = $reportContentIC . '</tbody>
                    </table><br><br>';

                }

                //jika item checklist 0
                if ($totalItemIC == 0) {$reportContentIC = '';}

                $reportContent = $reportContent . '' . $reportContentIC;

                //kriteria pekerjaan
                $totalItemKP     = 0;
                $reportContentKP = '<table align="center" border="2" width="100%" border="1">
                <thead>
                <tr style="background-color:#99c0ff; color: #000">
                <th style="text-align: center; width: 5%">No.</th>
                <th style="text-align: center;">Kriteria Pekerjaan</th>
                <th width="40%" colspan="3">
                <table border="1" width="100%">
                <thead>
                <tr>
                <th style="text-align: center;" colspan="3">Di check oleh ("V" atau "X")</th>
                </tr>
                <tr>
                <th style="text-align: center;">#1</th>
                <th style="text-align: center;">#2</th>
                <th style="text-align: center;">#3</th>
                </tr>
                </thead>
                </table>
                </th>
                </tr>
                </thead>';

                for ($y = 0; $y < count($dataKriteriaPekerjaan); $y++) {

                    if ($dataKriteriaPekerjaan[$y]['id_project_area'] == $dataArea[$i]['id']) {
                        if ($dataKriteriaPekerjaan[$y]['score_konsultan'] == '0') {
                            $scoreKonsultan = 'X';
                        } else if ($dataKriteriaPekerjaan[$y]['score_konsultan'] == '1') {
                            $scoreKonsultan = 'V';
                        } else {
                            $scoreKonsultan = '';
                        }

                        if ($dataKriteriaPekerjaan[$y]['score_dealer'] == '0') {
                            $scoreDealer = 'X';
                        } else if ($dataKriteriaPekerjaan[$y]['score_dealer'] == '1') {
                            $scoreDealer = 'V';
                        } else {
                            $scoreDealer = '';
                        }

                        if ($dataKriteriaPekerjaan[$y]['score_tam'] == '0') {
                            $scoreTam = 'X';
                        } else if ($dataKriteriaPekerjaan[$y]['score_tam'] == '1') {
                            $scoreTam = 'V';
                        } else {
                            $scoreTam = '';
                        }

                        $totalItemKP++;

                        $reportContentKP = $reportContentKP . '<tr>
                        <td style="padding: 7px">' . $totalItemKP . '</td>
                        <td style="padding: 7px">' . $dataKriteriaPekerjaan[$y]['item_kriteria_pekerjaan'] . '</td>
                        <td width="7%" style="padding: 7px" align="center">' . $scoreKonsultan . '</td>
                        <td width="7%" style="padding: 7px" align="center">' . $scoreDealer . '</td>
                        <td width="7%" style="padding: 7px" align="center">' . $scoreTam . '</td>
                        </tr>';

                    }

                }

                $reportContentKP = $reportContentKP . '</tbody>
                </table><br><br>';

                if ($totalItemKP == 0) {$reportContentKP = '';}

                $reportContent = $reportContent . '' . $reportContentKP;

                if ($reportContentIC == '' && $reportContentKP == '') {$reportHtmlData = $reportHtmlData . '<center><h3>Configuration Data Not Found</h3></center><br>';};

            }

        } else {
            $reportContent = '';
        }

        $htmlReport = $htmlReport . '' . $reportContent;

        // $reportCover;
        // die();

        ob_end_clean();

        $paperOrientation = 'potrait';
        $fileName         = 'tes.pdf';

        $this->pdf->base_html($htmlReport, $fileName, '', $paperOrientation);

    }

    public function generate_pdf_report_catatan($report_type, $param = null, $param2 = null)
    {

        $this->load->library('pdf');
        $htmlReport = '';

        if ($report_type == 'catatan_evaluasi') {

            $idProject = $param;
            $jenisEpm  = $param2;

            $dataProject = $this->data_project($idProject)->result_array();

            $dataEvaluasiKunjungan = $this->MReport->data_evaluasi_kunjungan($idProject, $jenisEpm)->result_array();

            ob_start();
            setlocale(LC_ALL, 'IND');

            $reportContent = '';

            if ($this->MReport->dataEvaluasi($dataEvaluasiKunjungan[0]['id'])->num_rows() > 0) {
                $reportContent = '<center><h3>Configuration Data Not Found</h3></center><br>';
            } else {
                $dataDetailEvaluasiKunjungan = $this->MReport->data_detail_evaluasi_kunjungan($dataEvaluasiKunjungan[0]['id'])->result_array();

                $reportHeader = '<h3 align="center">Evaluasi Kunjungan Outlet</h3><br><br>
                <table align="center" width="100%">
                <thead>
                <tr>
                <th width="50%">
                <table width="100%">
                <tbody>
                <tr>
                <td style="padding: 10px">Nama Outlet</td>
                <td> : </td>
                <td>' . $dataProject[0]['nama_outlet'] . '</td>
                </tr>
                <tr>
                <td style="padding: 10px">Dealer</td>
                <td> : </td>
                <td>' . $dataProject[0]['main_dealer'] . '</td>
                </tr>
                <tr>
                <td style="padding: 10px">Tgl Kunjungan</td>
                <td> : </td>
                <td>' . date('d F Y', strtotime($dataEvaluasiKunjungan[0]['tgl_kunjungan'])) . '</td>
                </tr>
                <tr>
                <td style="padding: 10px">Target Otoritasi</td>
                <td> : </td>
                <td>' . date('d F Y', strtotime($dataProject[0]['target_otorisasi'])) . '</td>
                </tr>
                </tbody>
                </table>
                </th>
                <th width="50%" colspan="3">
                <table border="1" width="100%">
                <thead>
                <tr style="background-color:#99c0ff; color: #000">
                <th style="padding: 5px; text-align: center;" colspan="4">Approval</th>
                </tr>
                <tr>
                <th style="padding: 5px; text-align: center;">TAM</th>
                <th style="padding: 5px; text-align: center;">Dealer</th>
                <th style="padding: 5px; text-align: center;">Consultant</th>
                <th style="padding: 5px; text-align: center;">Contractor</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td style="padding: 30px"></td>
                <td style="padding: 30px"></td>
                <td style="padding: 30px"></td>
                <td style="padding: 30px"></td>
                </tr>
                </tbody>
                </table>
                </th>
                </tr>
                </thead>
                </table><br><br>';

                $reportContent = $reportContent . '' . $reportHeader;

                $totalItemEval     = 0;
                $reportContentEval = '<table align="center" border="2" width="100%" border="1">
                <thead>
                <tr style="background-color:#99c0ff; color: #000">
                <th style="text-align: center;">No.</th>
                <th style="text-align: center;">Area</th>
                <th style="text-align: center;">Fasilitas</th>
                <th style="text-align: center;">Problem / Deskripsi</th>
                <th style="text-align: center;">Tindak lanjut</th>
                <th style="text-align: center;">Target</th>
                <th style="text-align: center;">PIC</th>
                </tr>
                </thead>';

                for ($y = 0; $y < count($dataDetailEvaluasiKunjungan); $y++) {

                    $totalItemEval++;

                    $reportContentEval = $reportContentEval . '<tbody><tr>
                    <td style="padding: 7px">' . $totalItemEval . '</td>
                    <td style="padding: 7px">' . $dataDetailEvaluasiKunjungan[$y]['area_name'] . '</td>
                    <td style="padding-left:10px">' . $dataProject[0]['fungsi_outlet'] . '</td>';
                    if ($dataDetailEvaluasiKunjungan[$y]['eval_problem_deskripsi'] == null) {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px"></td>';
                    } else {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px">' . $dataDetailEvaluasiKunjungan[$y]['eval_problem_deskripsi'] . '</td>';
                    }
                    if ($dataDetailEvaluasiKunjungan[$y]['eval_tindak_lanjut'] == null) {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px"></td>';
                    } else {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px">' . $dataDetailEvaluasiKunjungan[$y]['eval_tindak_lanjut'] . '</td>';
                    }
                    if ($dataDetailEvaluasiKunjungan[$y]['eval_target'] == null) {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px"></td>';
                    } else {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px">' . $dataDetailEvaluasiKunjungan[$y]['eval_target'] . '</td>';
                    }
                    if ($dataDetailEvaluasiKunjungan[$y]['eval_pic'] == null) {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px"></td>';
                    } else {
                        $reportContentEval = $reportContentEval . '<td style="padding-left:10px">' . $dataDetailEvaluasiKunjungan[$y]['eval_pic'] . '</td>';
                    }

                }

                $reportContentEval = $reportContentEval . '</tbody>
                </table><br><br>';

                if ($totalItemEval == 0) {$reportContentEval = '';}

                $reportContent = $reportContent . '' . $reportContentEval;
            }
        } else {
            $reportContent = '<center><h3>Configuration Data Not Found</h3></center><br>';
        }

        $htmlReport = $htmlReport . '' . $reportContent;

        // $reportCover;
        // die();

        ob_end_clean();

        $paperOrientation = 'potrait';
        $fileName         = 'tes.pdf';

        $this->pdf->base_html($htmlReport, $fileName, '', $paperOrientation);

    }

    public function generate_pdf_report_toss($report_type, $param = null, $param2 = null)
    {

        $this->load->library('pdf');
        $htmlReport = '';

        if ($report_type == 'toss') {

            ob_start();
            setlocale(LC_ALL, 'IND');

            $reportContent = '';

            $contentA = '<h3><b>A. ToSS Profile</b></h3>
            <div class="form-group text-center">
            <center>
            <img align="center" src="' . base_url('assets/d1.png') . '" style="width: 350px; border:2px solid black; padding : 10px; vertical-align: top; text-align: center">
            </center>
            <table border="1" style="margin-top: 20px; width: 100%">
            <tr>
            <td style="padding: 7px; color: red; text-align: left;"><b>Nama ToSS</b></td>
            <td style="padding: 7px; color: red; text-align: left;"><b> : CENTRAL MOTOR KEMAYORAN</b></td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left;">Dealer</td>
            <td style="padding: 7px; text-align: left;"> : CENTRAL MOTOR</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left;">Cabang Induk</td>
            <td style="padding: 7px; text-align: left;"> : CENTRAL MOTOR SUNTER</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left">Tipe Bangunan</td>
            <td style="padding: 7px; text-align: left"> : RUKO</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left">Status Bangunan</td>
            <td>
            <table>
            <tr>
            <td style="padding: 7px; text-align: left"> : SEWA</td>
            <td style="padding: 7px; text-align: left">Dari</td>
            <td style="padding: 7px; text-align: left"> : 01-Jan-17</td>
            <td style="padding: 7px; text-align: left">Sampai</td>
            <td style="padding: 7px; text-align: left"> : 01-Jan-19</td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: lef;">Alamat</td>
            <td style="padding: 7px; text-align: left;"> : JL. YOS SUDARSO, SUNTER II</td>
            </tr>
            </tr>
            <tr>
            <td style="padding: 7px" align="left">Kecamatan</td>
            <td style="padding: 7px; text-align: left"> : TANJUNG PRIOK</td>
            </tr>
            <tr>
            <td style="padding: 7px" align="left">Kabupaten/Kota</td>
            <td style="padding: 7px; text-align: left"> : JAKARTA UTARA</td>
            </tr>
            <tr>
            <td style="padding: 7px" align="left">Provinsi</td>
            <td style="padding: 7px; text-align: left"> : DKI JAKARTA</td>
            </tr>
            <tr>
            <td style="padding: 7px" align="left">Latitude</td>
            <td style="padding: 7px; text-align: left"> : -6.1432525</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left">Longitude</td>
            <td style="padding: 7px; text-align: left"> : 106.8902601</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left">Telp/Fax</td>
            <td style="padding: 7px; text-align: left"> : (021) 651 5551</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left;" colspan="2"><b>PIC ToSS</b></td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left;">Nama</td>
            <td style="padding: 7px; text-align: left;"> : Bp. Toyota Astra Motor</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left;">Jabatan</td>
            <td style="padding: 7px; text-align: lef;t"> : SA</td>
            </tr>
            <tr>
            <td style="padding: 7px; text-align: left">No. HP</td>
            <td style="padding: 7px; text-align: left"> : 0813497XXX</td>
            <tr>
            <td style="padding: 7px; text-align: left">Email</td>
            <td style="padding: 7px; text-align: left"> : toyota@toyota.co.id</td>
            </tr>
            </table>
            <br><br>

            <table border="1" style="margin-top: 20px" width="70%" align="center">
            <thead>
            <tr>
            <td style="padding: 10px; text-align: center"><b><h4>Perwakilan Dealer</h4></b></td>
            <td style="padding: 10px; text-align: center"><b><h4>Perwakilan TAM</h4></b></td>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td style="height: 200px"></td>
            <td></td>
            </tr>
            <tr>
            <td style="padding: 10px;text-align: center">Nama Perwakilan disini<br>Jabatan Disini</td>
            <td style="padding: 10px;text-align: center">Nama Perwakilan disini<br>Jabatan Disini</td>
            </tr>
            </tbody>
            </table>

            </div><br>';

            $reportContent = $reportContent . '' . $contentA;

            $contentB = '<h3><b>B. Kapasitas Bengkel</b></h3>
            <div class="form-group">
            <table width="60%" border="1">
            <tr>
            <td class="text-center" rowspan="6" width="30%"><h1 style="color: red"><center><b>2</b></center></h1><br><br><center>Jumlah Stall</center></td>
            <td class="text-center" rowspan="6"><h1 style="color: red"><center><b>2</b></center></h1><br><br><center>Stall GR</center></td>
            <td style="padding: 15px" width="40%"><b>Stall lain</b> :</td>
            </tr>
            <tr>
            <td style="padding: 5px;">
            THS Motor : 5 Unit
            </td>
            </tr>
            <tr>
            <td style="padding: 5px;">
            THS Mobil : 15 Unit
            </td>
            </tr>
            <tr>
            <td style="padding: 5px;">
            Lain-lain : 10 Unit
            </td>
            </tr>
            <tr>
            <td style="padding: 5px;">
            Keterangan :
            </td>
            </tr>
            <tr>
            <td style="padding: 5px">* Jika ada</td>
            </tr>
            </table><br>
            <table border="1" width="90%">
            <tr>
            <td align="center" width="15%" style="padding: 5px; padding-right: 20px; padding-left: 20px">Jumlah Manpower</td>
            <td align="center" width="25%" style="padding: 5px; padding-left: 15px; padding-right: 15px"><h2 style="color: red"><b>1</b></h2>Pro Tech</td>
            <td align="center" width="15%" style="padding: 5px; padding-left: 25px; padding-right: 25px"><h2 style="color: red"><b>1</b></h2>Tech</td>
            <td align="center" width="15%" style="padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b>1</b></h2>Admin</td>
            <td align="center" width="15%" style=" padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b>1</b></h2>Others</td>
            <td width="25%" style="padding: 5px;">
            <span>Catatan :</span>
            <br>
            <span align="center"><small>Pro Tech merangkap sebagai SA dan 1 staff adalah office boy merangkap security</small></span>
            <br>
            <span><small><b>Total Manpower : 4 Orang</b></small></span>
            </td>
            </tr>
            </table>
            </div><br>';
            $reportContent = $reportContent . '' . $contentB;

            $contentC = '<h3><b>C. Target & Investasi</b></h3>
            <div class="form-group">
            <div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">C1. TARGET</span>
            </div>
            </div><br>
            <table border="1" width="60%">
            <thead>
            <tr>
            <th style="text-align: center; padding: 10px; background-color: grey; color: white">Item</th>
            <th style="text-align: center; padding: 10px; background-color: grey; color: white">Y</th>
            <th style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 1</th>
            <th style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 2</th>
            <th style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 3</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td style="padding: 5px">UE/Bulan</td>
            <td style="text-align: center;">120</td>
            <td style="text-align: center;">130</td>
            <td style="text-align: center;">140</td>
            <td style="text-align: center;">150</td>
            </tr>
            <tr>
            <td style="padding: 5px">UE/Hari</td>
            <td style="text-align: center;">5</td>
            <td style="text-align: center;">6</td>
            <td style="text-align: center;">6</td>
            <td style="text-align: center;">7</td>
            </tr>
            </tbody>
            </table>
            <br>
            <b>Note :</b>
            <br>
            <p>Y = Tahun sertifikasi | 1 Bulan = 22.5 hari
            <div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">C2. Investasi</span>
            </div>
            </div><br>
            <table border="1" width="70%">
            <tbody>
            <tr>
            <td style="padding: 3px" width="15%">Pembelian lahan *</td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> - </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td style="padding: 3px" width="15%">Sewa Bangunan</td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> 100.000.000 </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td style="padding: 3px" width="15%">Renovasi Bangunan</td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> 43.000.000 </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td style="padding: 3px" width="15%">Corporate Identity</td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> 10.000.000 </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td style="padding: 3px" width="15%">Equipment</td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> 292.000.000 </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td width="15%" style="background-color: grey; color: white"><b>TOTAL</b></td>
            <td width="15%">
            <table width="100%">
            <tr>
            <td style="padding: 3px"> : IDR </td>
            <td align="right" style="padding-right: 15px"> 445.000.000 </td>
            </tr>
            </table>
            </td>
            </tr>
            </tbody>
            </table>
            <br>
            <b>Note :</b>
            <br>
            <p>* Apabila milik sendiri
            </div><br>';
            $reportContent = $reportContent . '' . $contentC;

            $contentD = '<h3><b>D. Checklist Sertifikasi</b></h3>
            <div class="form-body">
            <div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">D1. FASILITAS UMUM</span>
            </div>
            </div><br>
            <div class="form-group">
            <table border="1" width="100%" class="text-center">
            <thead>
            <tr><th colspan="7" align="center" style="text-align: center; padding: 10px; background-color: grey; color: white">1. PERSPEKTIF & FACIA TOSS</th></tr>
            </thead>
            <tbody>
            <tr>
            <td width="3%" style="text-align: center; padding: 5px; background-color: #fffc7c">No.</td>
            <td width="45%" style="text-align: center; padding: 5px; background-color: #fffc7c">Kriteria Minimum</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Jumlah</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Foto Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi TAM</td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">1.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Memiliki Fascia Toyota & Dealer Name sesuai dengan standar*</td>
            <td align="center"></td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">2.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Terdapat identitas Toyota Service Station yang mudah dibaca oleh pelanggan</td>
            <td align="center"></td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            </tbody>
            </table>
            </div><br><br>
            <div class="form-group">
            <table border="1" width="100%">
            <thead>
            <tr><th colspan="7" align="center" style="padding: 5px; background-color: grey; color: white">2. AREA TUNGGU PELANGGAN SERVIS</th></tr>
            </thead>
            <tbody>
            <tr>
            <td width="3%" style="text-align: center; padding: 5px; background-color: #fffc7c">No.</td>
            <td width="45%" style="text-align: center; padding: 5px; background-color: #fffc7c">Kriteria Minimum</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Jumlah</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Foto Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi TAM</td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">1</td>
            <td align="left" style="padding: 5px; vertical-align: top">Posisi pelanggan dapat melihat ke area bengkel</td>
            <td align="center"></td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top">Terdapat pula TV yang dilengkapi kamera untuk melihat bengkel</td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">2</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas Pendukung Meja dan Kursi untuk pelangan</td>
            <td align="center">4</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top">1 meja 3 kursi kap. 6 org</td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">3</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas Pendukung Food & Beverage(Min Air Mineral)</td>
            <td align="center"></td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top">Camilan & Air mineral</td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            </tbody>
            </table>
            </div><br>

            </div><br><br>
            <div class="form-body">
            <div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">D2. AREA PELANGGAN</span>
            </div>
            </div><br>
            <div class="form-group">
            <table border="1" width="100%" class="text-center">
            <thead>
            <tr><th colspan="7" align="center" style="text-align: center; padding: 5px; background-color: grey; color: white">1. AREA PENERIMAAN SERVIS</th></tr>
            </thead>
            <tbody>
            <tr>
            <td width="3%" style="text-align: center; padding: 5px; background-color: #fffc7c">No.</td>
            <td width="45%" style="text-align: center; padding: 5px; background-color: #fffc7c">Kriteria Minimum</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Jumlah</td>
            <td width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Foto Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi Dealer</td>
            <td style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi TAM</td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">1</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung meja untuk SA & Admin</td>
            <td align="center">2</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top">1 meja kapasitas 4 org</td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">2</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung kursi untuk SA & Admin</td>
            <td align="center">2</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">3</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung kursi untuk Pelanggan</td>
            <td align="center">2</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">4</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung cost Estimation Board Service</td>
            <td align="center">1</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td style="padding: 5px; vertical-align: top"></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">5</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung komputer</td>
            <td align="center">1</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">6</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung printer</td>
            <td align="center">1</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top"></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">7</td>
            <td align="left" style="padding: 5px; vertical-align: top">Fasilitas pendukung telephone</td>
            <td align="center">1</td>
            <td><img class="img-thumbnail" src="' . base_url() . 'upload/default.jpg" style="max-width: 100px"></td>
            <td align="left" style="padding: 5px; vertical-align: top">No. Kontak PIC ToSS</td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            </tbody>
            </table>
            </div>
            </div><br><br>';

            $reportContent = $reportContent . '' . $contentD;

            $contentD5 = '<div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">D3. Manpower</span><br><br>
            <span class="caption-subject bold uppercase">A. Klasifikasi SDM After Sales</span>
            </div>
            </div><br>
            <div class="form-group">
            <table border="1" width="100%" class="text-center" style="font-size: 12px">
            <thead>
            <tr>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">No.</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c; width: 150px" rowspan="4">Nama Karyawan</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c; width: 60px" rowspan="4">Tgl Masuk</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c; width: 110px" rowspan="4">No. KTP</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c; width: 100px" rowspan="4">Jabatan</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="10">Sertifikasi</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c; width: 80px" rowspan="4">Keterangan</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Evaluasi<br>Dealer</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Evaluasi<br>TAM</th>
            </tr>
            <tr>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="7">Teknisi</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="2" colspan="3">SA GR</th>
            </tr>
            <tr>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="2">Level 1</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="3">Level 2</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="2">Level 3</th>
            </tr>
            <tr>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">TT</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">PT</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">DTG</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">DTL</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">DTC</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">DMT</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">LD</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">L1</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">L2</th>
            <th style="text-align: center; padding: 2px; background-color: #fffc7c">L3</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>1.</td>
            <td>Krisna Kurnia Perdana</td>
            <td align="center">09-05-2016</td>
            <td>3520151204940002</td>
            <td>Tech</td>
            <td align="center">V</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td></td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            <tr>
            <td>2.</td>
            <td>Sukron Karim</td>
            <td align="center">18-02-2016</td>
            <td>3529010305960006</td>
            <td>Pro Tech</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center">V</td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td align="center"></td>
            <td></td>
            <td>Merangkap SA</td>
            <td><center>V</center></td>
            <td><center>V</center></td>
            </tr>
            </tbody>
            </table><br><br>


            Keterangan :<br><br>

            <table>
            <tr><td>
            <table width="100%">

            <tbody>
            <tr>
            <td colspan="2"><u>1. TRAINING TEKNISI</u><br><br></td>
            </tr>
            <tr>
            <td>TT </td>
            <td>: Toyota Technician</td>
            </tr>
            <tr>
            <td>PT </td>
            <td>: Pro Technician</td>
            </tr>
            <tr>
            <td>DT </td>
            <td>: Diagnostic Technician</td>
            </tr>
            <tr>
            <td>DTG </td>
            <td>: Diagnostic Technician Engine</td>
            </tr>
            <tr>
            <td>DTL </td>
            <td>: Diagnostic Technician Electrica</td>
            </tr>
            <tr>
            <td>DTC </td>
            <td>: Diagnostic Technician Chassis</td>
            </tr>
            <tr>
            <td>DMT </td>
            <td>: Diagnosis Master Technician</td>
            </tr>
            <tr>
            <td>LD </td>
            <td>: Latest Diagnosis</td>
            </tr>
            </tbody>
            </table>
            </td>
            <td style="vertical-align: top; padding-left: 20px">
            <table>
            <tbody>
            <tr>
            <td colspan="2"><u>2. SERVICE ADVISOR GR</u><br><br></td>
            </tr>
            <tr>
            <td>L1 </td>
            <td>: Level 1 TS A21</td>
            </tr>
            <tr>
            <td>L2 </td>
            <td>: Level 2 TS A21</td>
            </tr>
            <tr>
            <td>L3 </td>
            <td>: Level 3 TS A21</td>
            </tr>
            </tbody>
            </table>
            </td></tr>
            </table>

            </div><br>

            <div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">B. Klasifikasi SDM Umum</span>
            </div>
            </div><br>
            <div class="form-group">

            <table border="1" style="width: 800px" class="text-center">
            <thead>
            <tr>
            <th style="text-align: center; padding: 5px; background-color: #fffc7c">No.</th>
            <th style="text-align: center; padding: 5px; background-color: #fffc7c">Nama Karyawan</th>
            <th style="text-align: center; padding: 5px; background-color: #fffc7c">Tgl Masuk</th>
            <th style="text-align: center; padding: 5px; background-color: #fffc7c">Jabatan</th>
            <th style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td width="5%">1.</td>
            <td width="15%">Krisna Kurnia Perdana</td>
            <td width="10%">09-05-2016</td>
            <td width="15%">Admin</td>
            <td width="15%">Admin & Kasir</td>
            </tr>
            <tr>
            <td width="5%">2.</td>
            <td width="15%">Sukron Karim</td>
            <td width="10%">18-2-2016</td>
            <td width="15%">Others</td>
            <td width="15%">Security & Office Boy</td>
            </tr>
            </tbody>
            </table>
            </div><br>';

            $reportContent = $reportContent . '' . $contentD5;

            $contentD6 = '<div class="portlet-title">
            <div class="caption font-dark">
            <span class="caption-subject bold uppercase">D4. Equipment</span>
            </div>
            <br>
            </div>
            <div class="form-group">
            <table border="1" width="100%" class="text-center">
            <thead>
            <tr>
            <th width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">No.</th>
            <th width="30%" style="text-align: center; padding: 5px; background-color: #fffc7c">Kriteria Minimum</th>
            <th width="10%" style="text-align: center; padding: 5px; background-color: #fffc7c">Jumlah<br>Min.</th>
            <th width="15%" style="text-align: center; padding: 5px; background-color: #fffc7c">Foto Dealer</th>
            <th width="15%" style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</th>
            <th width="10%" style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi Dealer</th>
            <th width="10%" style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi TAM</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td colspan="7" style="text-align: center; padding: 5px; background-color: grey; color: white">Power Equipment Tools</td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">1.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Air Comp. Min. 2 PK</td>
            <td style="padding: 5px; vertical-align: top">1 Set</td>
            <td align="center" style="padding: 3px">
            <img class=""  src="' . base_url() . 'upload/toss_equipment/1.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/1.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/1.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/1.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/1.png" style="max-width: 150px; border: 2px solid black"><br><br>
            </td>
            <td style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">2.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Air House Rail & Kabel row</td>
            <td style="padding: 5px; vertical-align: top">1 Set</td>
            <td align="center" style="padding: 3px">
            <img class=""  src="' . base_url() . 'upload/toss_equipment/2.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/2.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/2.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/2.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/2.png" style="max-width: 150px; border: 2px solid black"><br><br>
            </td>
            <td style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td colspan="7" style="text-align: center; padding: 5px; background-color: grey; color: white">Lifting And Garage Equipment</td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">1.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Hydraullic Garage Jack ST</td>
            <td style="padding: 5px; vertical-align: top">1 Pcs</td>
            <td align="center" style="padding: 3px">
            <img class=""  src="' . base_url() . 'upload/toss_equipment/3.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/3.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/3.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/3.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/3.png" style="max-width: 150px; border: 2px solid black"><br><br>
            </td>
            <td style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            <tr>
            <td style="padding: 5px; vertical-align: top">2.</td>
            <td align="left" style="padding: 5px; vertical-align: top">Jack Stand 3T</td>
            <td style="padding: 5px; vertical-align: top">4 Pcs</td>
            <td align="center" style="padding: 3px">
            <img class=""  src="' . base_url() . 'upload/toss_equipment/4.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/4.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/4.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/4.png" style="max-width: 150px; border: 2px solid black"><br><br>
            <img class=""  src="' . base_url() . 'upload/toss_equipment/4.png" style="max-width: 150px; border: 2px solid black"><br><br>
            </td>
            <td style="padding: 5px; vertical-align: top"></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            <td style="padding: 5px; vertical-align: top"><center>V</center></td>
            </tr>
            </tbody>
            </table>
            </div>';

            $reportContent = $reportContent . '' . $contentD6;

        }

        $htmlReport = $htmlReport . '' . $reportContent;

        // $reportCover;
        // die();

        ob_end_clean();

        $paperOrientation = 'potrait';
        $fileName         = 'tes.pdf';

        $this->pdf->base_html($htmlReport, $fileName, 'A3', $paperOrientation);

    }

    public function gen_toPDF($report_type)
    {
        $this->load->library('pdf');
        $htmlReport = '';

        if ($report_type == 'toss') {

            ob_start();
            setlocale(LC_ALL, 'IND');
        }
        ob_end_clean();

        $htmlReport = $this->load->view('print_report_toss', '', true);

        $paperOrientation = 'potrait';
        $fileName         = 'tes.pdf';

        $this->pdf->base_html($htmlReport, $fileName, 'A3', $paperOrientation);
    }

    public function areaFE($id_toss_database)
    {
        return $this->db->select('a.id_toss_database,a.id_area,(select area_name from tbl_area where id=a.id_area) as area_name')
            ->from('tbl_toss_database_cs_area as a')
            ->where('a.is_delete', 0)
            ->where('a.id_toss_database', $id_toss_database)
            ->group_by('a.id_area')
            ->order_by('a.id_area')->get()->result();
    }

    public function sub_areaFE($id_area, $id_toss_database)
    {
        return $this->db->select('a.id_toss_database,a.id_area,a.id_sub_area,(select sub_area_name from tbl_sub_area where id=a.id_sub_area) as sub_area_name')
            ->from('tbl_toss_database_cs_area as a')
            ->where('a.is_delete', 0)
            ->where('a.id_area', $id_area)
            ->where('a.id_toss_database', $id_toss_database)
            ->group_by('a.id_sub_area')
            ->order_by('a.id')->get()->result();
    }

    public function items($id_toss_database, $id_sub_area)
    {
        return $this->db
            ->select('a.*,(select sub_area_name from tbl_sub_area where id=a.id_sub_area) as sub_area_name')
            ->from('tbl_toss_database_cs_area as a')
            ->where('a.is_delete', 0)
            ->where('a.id_sub_area', $id_sub_area)
            ->where('a.id_toss_database', $id_toss_database)
            ->order_by('a.id')->get()->result();
    }

    public function GroupEQFE($id_toss_database)
    {
        return $this->db->select('id,id_toss_database,group_key,group_name')
            ->from('tbl_toss_database_cs_equipment')
            ->where('id_toss_database', $id_toss_database)
            ->group_by('group_key')
            ->order_by('group_key', 'asc')->get()->result();
    }

    public function ItemsEQFE($id_toss_database, $group_key)
    {
        return $this->db->from('tbl_toss_database_cs_equipment')->where('id_toss_database', $id_toss_database)
            ->where('group_key', $group_key)
            ->order_by('group_key', 'asc')->get()->result();
    }

    public function Isi_EQ($id_items)
    {
        return $this->db->select('id,foto_dealer_file_name_64')
            ->from('tbl_toss_database_cs_equipment_foto_dealer')->where('is_delete', 0)
            ->where('id_toss_database_cs_equipment', $id_items)
            ->order_by('id', 'asc')->get()->result();
    }

    public function ls_manpower($id)
    {
        return $this->db->from('tbl_toss_database_cs_manpower')
            ->where('is_delete', 0)
            ->where('id_toss_database', $id)->get()->result();
    }

    public function ls_manpower_sdm_umum($id)
    {
        $data = $this->db->from('tbl_toss_database_cs_manpower')->where_in('jabatan', ["admin", "others"])->where('is_delete', 0)->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_manpower_sdm_after_sales($id)
    {
        $data = $this->db->from('tbl_toss_database_cs_manpower')->where_in('jabatan', ["tech", "pro_tech"])->where('is_delete', 0)->where('id_toss_database', $id)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

    public function ls_dealer()
    {
        $data = $this->db->select('tb.id,tb.dealer,(select nama from tbl_lokasi_kota_kabupaten where id=tb.kota_kabupaten) as kota_kabupaten')->from('tbl_toss_database as tb')->where('tb.is_delete', 0)->get();
        if ($data->num_rows() > 0) {
            return $data->result();
        } else {
            return null;
        }
    }

}
