<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

defined('BASEPATH') or exit('No direct script access allowed');

class MMaster_data extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    public function read_users()
    {
        return $this->db->get('tbl_user');
    }

    //Master ordinary soft delete
    public function ordinary_soft_delete($id, $table_name)
    {

        $data = array(
            'is_delete' => '1',
        );

        return $this->db->where('id', $id)
            ->update($table_name, $data);
    }

    //Master ordinary soft delete
    public function ordinary_delete($id, $table_name)
    {

        return $this->db->where('id', $id)
            ->delete($table_name);
    }

    //Master ordinary insert
    public function ordinary_insert($data, $table_name)
    {
        return $this->db->insert($table_name, $data);
    }

    //Master ordinary update
    public function ordinary_update($id, $data, $table_name)
    {
        return $this->db->where('id', $id)
            ->update($table_name, $data);
    }

    //Master Area.
    public function showAllArea()
    {
        return $this->db->where('is_delete', 0)
            ->order_by('id', 'desc')
            ->get('tbl_area');
    }

    public function area_id($id)
    {
        return $this->db->where('id', $id)
            ->where('is_delete', 0)
            ->get('tbl_area');
    }

    //Master sub area
    public function showAllSub()
    {
        return $this->db->join('tbl_area', 'tbl_area.id=tbl_sub_area.id_area')
            ->order_by('tbl_sub_area.id', 'desc')
            ->where('tbl_sub_area.is_delete', 0)
            ->from('tbl_sub_area')
            ->select('tbl_sub_area.*, tbl_area.area_name')
            ->get();
    }

    public function sub_area_id($id)
    {
        return $this->db->where('id', $id)
            ->where('is_delete', 0)
            ->get('tbl_sub_area');
    }

    // Master Equipment & Tools Group
    public function read_equipment_and_tools_group()
    {
        return $this->db->where('is_delete =', 0)
            ->get('tbl_equipment_and_tools_group');
    }

    public function get_equipment_and_tools_group_by_id($id)
    {
        return $this->db->where('id =', $id)
            ->get('tbl_equipment_and_tools_group');
    }

    // Master Equipment & Tools Item
    public function read_equipment_and_tools_item()
    {
        return $this->db->where('is_delete =', 0)
            ->get('tbl_equipment_and_tools_item');
    }

    public function get_equipment_and_tools_item_by_id($id)
    {
        return $this->db->where('id =', $id)
            ->get('tbl_equipment_and_tools_item');
    }

    // MASTER PROJECT
    public function read_project()
    {
        return $this->db->where('tbl_project.is_delete =', 0)
            ->select("tbl_project.*,user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3")
            ->where('tbl_project.type', 'project')
            ->order_by('id', 'desc')
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1', 'left')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2', 'left')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3', 'left')
            ->get('tbl_project');
    }
    public function assign_project_template($id_project, $id_template)
    {
        $project_template_area = $this->db->where('id_project', $id_template)->get('tbl_project_area')->result_array();
        foreach ($project_template_area as $row_area) {
            $id_template_area = $row_area['id'];
            unset($row_area['id']);
            $row_area['id_project'] = $id_project;
            $this->db->insert('tbl_project_area', $row_area);
            $id_project_area = $this->db->insert_id();
            $this->assign_sub_area($id_project_area, $id_template_area);
        }
        $this->assign_table('tbl_project_config_equipment_and_tools', 'id_project', $id_template, $id_project);
    }
    private function assign_sub_area($id_project_area, $id_template_area)
    {
        $this->assign_table('tbl_project_sa_config_ic', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_km', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_kp', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_minreq', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_ket', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_pic', 'id_project_area', $id_template_area, $id_project_area);
        $this->assign_table('tbl_project_sa_config_tps_line', 'id_project_area', $id_template_area, $id_project_area);
    }
    private function assign_table($table, $key, $val, $new_value)
    {
        $qry    = $this->db->where($key, $val)->get($table);
        $insert = [];
        foreach ($qry->result_array() as $row) {
            unset($row['id']);
            $row[$key] = $new_value;
            $insert[]  = $row;
        }
        if (count($insert) > 0) {
            $this->db->insert_batch($table, $insert);
        }
    }
    // MASTER PROJECT TEMPLATE
    public function read_project_template()
    {
        return $this->db->where('is_delete =', 0)
            ->where('type', 'template')
            ->order_by('id', 'desc')
            ->get('tbl_project');
    }

    public function read_users_kontraktor()
    {
        return $this->db->where('user_type = ', 'Kontraktor')
            ->where('is_delete = ', 0)
            ->get('tbl_user');
    }

    //Master user
    public function showALLUser($user_type = ['all'])
    {
        return $this->db
            ->where('is_delete', 0)
            ->group_start()
            ->where_in('user_type', $user_type)
            ->or_where_in("'all'", $user_type)
            ->group_end()
            ->order_by('id', 'DESC')
            ->get('tbl_user');
    }

    public function get_user_by_id($id)
    {
        return $this->db->where('id', $id)
            ->where('user_type <>', 'superadmin')
            ->where('is_delete', '0')
            ->get('tbl_user', 1);
    }

    public function emailExist($idUser, $email)
    {
        if ($idUser == '') {
            $q = $this->db->query("select count(id) as total from tbl_user where email= '$email'")->row('total');
        } else {
            $q = $this->db->query("select count(id) as total from tbl_user where email= '$email' and id<>'" . $idUser . "'")->row('total');
        }

        if ($q > 0) {
            return true;
        } else {
            return false;
        }
    }

    //Master Man Power Level Training.
    public function showAllManPowerLevelTraining()
    {
        return $this->db->where('is_delete =', 0)
            ->order_by('id', 'DESC')
            ->get('tbl_project_level_training');
    }

    public function project_level_training_id($id)
    {
        return $this->db->where('is_delete =', 0)
            ->where('id', $id)
            ->get('tbl_project_level_training');
    }

    ///////////////

    public function read_users_admin()
    {
        $where = 'Admin';
        return $this->db->where('user_type', $where)
            ->where('is_delete = ', 0)
            ->get('tbl_user');
    }

    // public function read_users_dealer() {
    //     $where = 'Dealer';
    //     return $this->db->where('user_type = ', $where)
    //                     ->where('is_delete = ', 0)
    //                     ->get('tbl_user');
    // }

    // public function read_users_tam() {
    //     $where = 'TAM';
    //     return $this->db->where('user_type = ', $where)
    //                     ->where('is_delete = ', 0)
    //                     ->get('tbl_user');
    // }

    // public function read_users_kontraktor_where($id_kontraktor) {
    //     $where = 'Kontraktor';
    //     return $this->db->where('user_type = ', $where)
    //                     ->where('id = ', $id_kontraktor)
    //                     ->where('is_delete = ', $id_kontraktor)
    //                     ->get('tbl_user');
    // }

    // public function read_users_dealer_where($id_dealer) {
    //     $where = 'Dealer';
    //     return $this->db->where('user_type = ', $where)
    //                     ->where('id = ',$id_dealer)
    //                     ->get('tbl_user');
    // }

    // public function read_users_tam_where($id_tam) {
    //     $where = 'TAM';
    //     return $this->db->where('user_type = ', $where)
    //                     ->where('id = ', $id_tam)
    //                     ->get('tbl_user');
    // }
    public function list_user($where = ['all'])
    {
        $data      = $this->MMaster_data->showALLUser($where)->result_array();
        $result[0] = '- belum ada user yang terpilih -';
        foreach ($data as $row) {
            if ($row['user_type'] != 'superadmin') {
                $result[$row['id']] = $row['username'] . ' - ' . strtoupper($row['user_type']);
            }
        }
        return $result;
    }
    public function get_by_id($id)
    {
        return $this->db
            ->select("tbl_project.*, user_level_1.user_type as type_level_1, user_level_2.user_type as type_level_2, user_level_3.user_type as type_level_3")
            ->where('tbl_project.id =', $id)
            ->join('tbl_user user_level_1', 'user_level_1.id=tbl_project.id_user_level_1', 'left')
            ->join('tbl_user user_level_2', 'user_level_2.id=tbl_project.id_user_level_2', 'left')
            ->join('tbl_user user_level_3', 'user_level_3.id=tbl_project.id_user_level_3', 'left')
            ->get('tbl_project');
    }

    public function add_project($data)
    {
        return $this->db->insert('tbl_project', $data);
    }

    public function edit_project($data, $id)
    {
        return $this->db->where('id = ', $id)
            ->update('tbl_project', $data);
    }

    public function delete_by_id($id, $data)
    {
        return $this->db->where('id', $id)
            ->update('tbl_project', $data);
    }

    // SET AREA PROJECT

    public function read_project_area_where_id_project($id)
    {
        $this->db->select('tbl_project_area.id, tbl_project_area.is_delete, tbl_project_area.id_project, tbl_project_area.id_area, tbl_project_area.id_sub_area, tbl_project_area.position, tbl_area.area_name, tbl_sub_area.sub_area_name, tbl_sub_area.description');
        $this->db->from('tbl_project_area');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left');
        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left');
        $this->db->where('id_project =', $id);
        $this->db->where('tbl_project_area.is_delete =', 0);
        $this->db->order_by('tbl_project_area.id', 'desc');
        return $query = $this->db->get();
    }

    public function read_project_join()
    {
        $this->db->select('tbl_project.*, tbl_project_sa_config_ic.id_project_area, tbl_project_sa_config_ic.jenis_epm, tbl_project_area.id, tbl_project_area.id_sub_area, tbl_project_area.id_area, tbl_area.area_name, tbl_sub_area.sub_area_name');
        $this->db->from('tbl_project_sa_config_ic');
        $this->db->join('tbl_project_area', 'tbl_project_area.id = tbl_project_sa_config_ic.id_project_area', 'left');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left');
        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left');
        $this->db->where('tbl_project_area.is_delete =', 0);
        $this->db->where('tbl_project.is_delete =', 0);
        $this->db->where("tbl_project.type ='project'");
        $this->db->group_by('tbl_project_sa_config_ic.id_project_area');
        $this->db->group_by('tbl_project_sa_config_ic.jenis_epm');
        return $query = $this->db->get();
    }

    public function read_project_template_join($jenis_epm)
    {
        $this->db->select('tbl_project.*, tbl_project_sa_config_ic.id_project_area, tbl_project_sa_config_ic.jenis_epm, tbl_project_area.id, tbl_project_area.id_sub_area, tbl_project_area.id_area, tbl_area.area_name, tbl_sub_area.sub_area_name');
        $this->db->from('tbl_project_sa_config_ic');
        $this->db->join('tbl_project_area', 'tbl_project_area.id = tbl_project_sa_config_ic.id_project_area', 'left');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left');
        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left');
        $this->db->where('tbl_project_area.is_delete =', 0);
        $this->db->where('tbl_project.is_delete =', 0);
        $this->db->where("tbl_project.type ='template'");
        $this->db->where('tbl_project_sa_config_ic.jenis_epm =', $jenis_epm);
        $this->db->group_by('tbl_project_sa_config_ic.id_project_area');
        $this->db->group_by('tbl_project_sa_config_ic.jenis_epm');
        return $query = $this->db->get();
    }

    public function read_project_template100_join()
    {
        $this->db->select('tbl_project.template_name, tbl_project_sa_config_km.id_project_area,  tbl_project_area.id, tbl_project_area.id_sub_area, tbl_project_area.id_area, tbl_area.area_name, tbl_sub_area.sub_area_name, 100 as jenis_epm');
        $this->db->from('tbl_project_sa_config_km');
        $this->db->join('tbl_project_area', 'tbl_project_area.id = tbl_project_sa_config_km.id_project_area', 'left');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project', 'left');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area', 'left');
        $this->db->join('tbl_sub_area', 'tbl_sub_area.id = tbl_project_area.id_sub_area', 'left');
        $this->db->where('tbl_project_area.is_delete =', 0);
        $this->db->where('tbl_project.is_delete =', 0);
        $this->db->where("tbl_project.type ='template'");
        $this->db->group_by('tbl_project_sa_config_km.id_project_area');
        return $query = $this->db->get();
    }

    public function read_area()
    {
        return $this->db->where('is_delete = ', 0)
            ->get('tbl_area');
    }

    public function read_project_where($id)
    {
        return $this->db->where('id = ', $id)
            ->where('is_delete = ', 0)
            ->get('tbl_project');
    }

    // read project kpd pic
    public function read_project_kpd_pic_where($id)
    {
        return $this->db->where('id_project = ', $id)
            ->get('tbl_project_kpd_pic');
    }

    public function get_project_equipment_and_tools_data($id, $epm)
    {
        return $this->db->query("
            select * from tbl_project_config_equipment_and_tools where is_delete = 0 and id_project = '$id' and jenis_epm='$epm'");
    }

    public function read_sub_area($id)
    {
        #Create where clause
        $this->db->select('id_sub_area');
        $this->db->where('is_delete =', 0);
        $this->db->where('id_project =', $id);
        $this->db->from('tbl_project_area');
        $where_clause = $this->db->get_compiled_select();

        #Create main query
        $this->db->select('*');
        $this->db->where("id NOT IN ($where_clause)", null, false);
        $this->db->where('is_delete =', 0);
        return $this->db->get('tbl_sub_area');
    }

    public function add_project_area($data)
    {
        return $this->db->insert('tbl_project_area', $data);
    }

    public function add_project_catatan($data)
    {
        return $this->db->insert('tbl_project_catatan', $data);
    }

    public function delete_project_area_by_id($id, $data)
    {
        return $this->db->where('id', $id)
            ->update('tbl_project_area', $data);
    }

    // Equipment & Tools Config

    public function read_eat($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->get('tbl_project_sa_config_ic');
    }

    public function add_project_config_eat($dataeat)
    {
        return $this->db->insert('tbl_project_config_equipment_and_tools', $dataeat);
    }

    //clean area configuration for item checklist
    public function delete_config_eat($id_project_area, $jenis_epm)
    {
        return $this->db->query("DELETE FROM tbl_project_config_equipment_and_tools WHERE id_project = '$id_project_area' and jenis_epm='$jenis_epm'");
    }

    // Set Area IC

    public function read_ic_where_posisi($id)
    {
        return $this->db->where('position =', $id)
            ->get('tbl_project_sa_config_ic');
    }

    public function read_project_area_where_id_project_area($id)
    {
        $this->db->select('tbl_project_area.id, tbl_project_area.id_project, tbl_project_area.id_area, tbl_area.area_name, tbl_project.jenis_epm, tbl_project.nama_outlet,tbl_project.template_name,(select sub_area_name from tbl_sub_area where id = tbl_project_area.id_sub_area) as sub_area_name');
        $this->db->from('tbl_project_area');
        $this->db->join('tbl_project', 'tbl_project.id = tbl_project_area.id_project');
        $this->db->join('tbl_area', 'tbl_area.id = tbl_project_area.id_area');
        $this->db->where('tbl_project_area.id =', $id);

        return $query = $this->db->get();
    }

    // Set Area IC
    public function read_ic_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_ic');
    }

    public function add_project_config_ic($dataic)
    {
        return $this->db->insert('tbl_project_sa_config_ic', $dataic);
    }

    // Set Area KP
    public function read_kp_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_kp');
    }

    public function add_project_config_kp($datakp)
    {
        return $this->db->insert('tbl_project_sa_config_kp', $datakp);
    }

    // Set Area Minreq
    public function read_minreq_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_minreq');
    }

    public function add_project_config_minreq($dataminreq)
    {
        return $this->db->insert('tbl_project_sa_config_minreq', $dataminreq);
    }

    // Set Area Tps Line
    public function read_tps_line_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_tps_line');
    }

    public function add_project_config_tps_line($datatpsline)
    {
        return $this->db->insert('tbl_project_sa_config_tps_line', $datatpsline);
    }

    // Set Area Keterangan
    public function read_ket_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_ket');
    }

    public function add_project_config_ket($dataket)
    {
        return $this->db->insert('tbl_project_sa_config_ket', $dataket);
    }

    public function read_pic_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_pic');
    }

    public function add_project_config_tps($dataket)
    {
        return $this->db->insert('tbl_project_sa_config_tps_line', $dataket);
    }

    public function add_project_config_pic($dataket)
    {
        return $this->db->insert('tbl_project_sa_config_pic', $dataket);
    }

    public function read_km_where($id, $jenis)
    {
        return $this->db->where('id_project_area =', $id)
            ->get('tbl_project_sa_config_km');
    }

    public function add_project_config_km($dataket)
    {
        return $this->db->insert('tbl_project_sa_config_km', $dataket);
    }
    // Set Area Catatan
    public function read_project_catatan_where($id, $jenis)
    {
        return $this->db->query("select tpc.* from tbl_project_catatan as tpc where tpc.id_project_area='$id' and tpc.jenis_epm='$jenis'");
    }

    // Get Config
    public function get_config_ic($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_ic');
    }

    public function get_config_kp($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_kp');
    }

    public function get_config_minreq($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_minreq');
    }

    public function get_config_ket($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_ket');
    }

    public function get_config_tps_line($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_tps_line');
    }

    public function get_config_km($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->get('tbl_project_sa_config_km');
    }

    public function get_config_pic($jenis, $id_project_area)
    {
        return $this->db->where('id_project_area = ', $id_project_area)
            ->where('jenis_epm =', $jenis)
            ->get('tbl_project_sa_config_pic');
    }

    //clean area configuration for item checklist
    public function delete_config_ic($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_ic');
    }

    public function delete_config_pic($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_pic');
    }

    public function delete_config_km($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_km');
    }

    //clean area configuration for minreq
    public function delete_config_minreq($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_minreq');
    }

    //clean area configuration for kriteria pekerjaan
    public function delete_config_kp($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_kp');
    }

    //clean area configuration for keterangan
    public function delete_config_ket($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_ket');
    }

    //clean area configuration for tps line
    public function delete_config_tps($id_project_area, $jenis_epm)
    {
        return $this->db->where('id_project_area',$id_project_area)->where('jenis_epm',$jenis_epm)->delete('tbl_project_sa_config_tps_line');
    }

    //clean project catatan all epm
    public function delete_all_project_catatan_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_catatan');
    }

    //clean project sub area config item checklist all epm
    public function delete_all_project_sa_config_ic_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_ic');
    }

    //clean project sub area config keterangan all epm
    public function delete_all_project_sa_config_ket_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_ket');
    }

    //clean project sub area config kriteria pekerjaan all epm
    public function delete_all_project_sa_config_kp_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_kp');
    }

    //clean project sub area config minimum requirement pekerjaan all epm
    public function delete_all_project_sa_config_minreq_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_minreq');
    }

    //clean project sub area config tps line pekerjaan all epm
    public function delete_all_project_sa_config_tps_line_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_tps_line');
    }

    //clean project sub area config kriteria minimum pekerjaan all epm
    public function delete_all_project_sa_config_km_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_km');
    }

    //clean project sub area config pic pekerjaan all epm
    public function delete_all_project_sa_config_pic_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_sa_config_pic');
    }

    //clean project area picture/photo
    public function delete_all_project_area_pic_where_id_pa($id_project_area)
    {
        return $this->db->where('id_project_area',$id_project_area)->delete('tbl_project_area_pic');
    }

    //clean project area
    public function delete_all_project_area_where_id_p($id_project)
    {
        return $this->db->where('id_project',$id_project)->delete('tbl_project_area');
    }

}
