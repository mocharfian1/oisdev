<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
defined('BASEPATH') or exit('No direct script access allowed');

class MEvaluasi_kunjungan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function add_evaluasi_kunjungan($data)
    {
        return $this->db->insert('tbl_evaluasi_kunjungan', $data);
    }

    public function insert_evaluasi_detail($data_evaluasi_detail)
    {
        return $this->db->insert('tbl_evaluasi_kunjungan_detail', $data_evaluasi_detail);
    }

    public function update_evaluasi($id, $data)
    {
        return $this->db->where('id', $id)
            ->update('tbl_evaluasi_kunjungan', $data);
    }

    public function add_detail_evaluasi($data_eval)
    {
        return $this->db->insert('tbl_evaluasi_kunjungan_detail', $data_eval);
    }

    public function update_detail_evaluasi($id, $data)
    {
        return $this->db->where('id', $id)
            ->update('tbl_evaluasi_kunjungan_detail', $data);
    }

    public function showProject($id)
    {
        return $this->db->select('ek.jenis_epm, p.nama_outlet')
            ->from('tbl_evaluasi_kunjungan as ek')
            ->join('tbl_project as p', 'p.id=ek.id_project')
            ->where('ek.id', $id)
            ->where('ek.is_delete', 0)
            ->get();
    }

    public function showAllEvaluasi()
    {
        return $this->db->select("ek.id, ek.id_project, ek.jenis_epm, ek.tgl_kunjungan, p.nama_outlet, p.main_dealer, p.fungsi_outlet, pa.id_area, pa.id_sub_area, a.area_name, sa.sub_area_name")
            ->from('tbl_evaluasi_kunjungan as ek')
            ->join('tbl_project p', 'p.id=ek.id_project')
            ->join('tbl_project_area as pa', 'pa.id_project=p.id')
            ->join('tbl_area as a', 'a.id=pa.id_area')
            ->join('tbl_sub_area as sa', 'sa.id=pa.id_sub_area')
            ->where('ek.is_delete', 0)
            ->group_by('ek.id')
            ->order_by('ek.id', 'desc')
            ->get();
    }

    public function read_eval($id, $jenis_epm)
    {
        return $this->db->where('id_project =', $id)
            ->where('jenis_epm =', $jenis_epm)
            ->get('tbl_evaluasi_kunjungan');
    }

    public function showAllDetailEvaluasi($id)
    {
        return $this->db->select('ekd.id, ekd.id_project_area, ekd.id_evaluasi_kunjungan, ekd.eval_problem_deskripsi, ekd.eval_tindak_lanjut, ekd.eval_target, ekd.eval_pic, ek.jenis_epm, ek.tgl_kunjungan, p.nama_outlet, p.fungsi_outlet, pa.id_sub_area, a.area_name, sa.sub_area_name')
            ->from('tbl_evaluasi_kunjungan_detail as ekd')
            ->join('tbl_evaluasi_kunjungan as ek', 'ek.id=ekd.id_evaluasi_kunjungan')
            ->join('tbl_project as p', 'p.id=ek.id_project')
            ->join('tbl_project_area as pa', 'pa.id=ekd.id_project_area')
            ->join('tbl_area as a', 'a.id=pa.id_area')
            ->join('tbl_sub_area as sa', 'sa.id=pa.id_sub_area')
            ->group_start()
            ->where('ekd.id_evaluasi_kunjungan', $id)
            ->or_where('ekd.id', $id)
            ->group_end()
            ->where('ek.is_delete', 0)
            ->where('ekd.is_delete', 0)
            ->group_by('ekd.id')
            ->order_by('ekd.id', 'asc')
            ->get();
    }

    public function dataEvaluasi($id)
    {
        return $this->db->where('id_evaluasi_kunjungan =', $id)
            ->where('eval_problem_deskripsi IS NULL')
            ->where('is_delete = 0')
            ->get('tbl_evaluasi_kunjungan_detail');
    }

    public function evaluasi_id($id)
    {
        return $this->db->where('id', $id)
            ->get('tbl_evaluasi_kunjungan');
    }

    public function delete_evaluasi($id, $data)
    {
        return $this->db->where('id', $id)
            ->update('tbl_evaluasi_kunjungan', $data);
    }

    public function delete_evaluasi_from_project($id, $data)
    {
        return $this->db->where('id_project', $id)
            ->update('tbl_evaluasi_kunjungan', $data);
    }

    public function delete_evaluasi_from_area($id, $data)
    {
        return $this->db->where('id_project_area', $id)
            ->update('tbl_evaluasi_kunjungan_detail', $data);
    }

    public function get_epm($id_project)
    {
        return $this->db->where('id_project = ', $id_project)
            ->where('is_delete', '0')
            ->get('tbl_evaluasi_kunjungan');
    }

}
