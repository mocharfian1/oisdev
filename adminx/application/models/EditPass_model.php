<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditPass_model extends CI_Model
{

  function __construct(){
    parent::__construct();
  }

  public function updatePass($data, $id){
    return $this->db->where('id', $id)
                    ->update('tbl_user', $data);
  }

}
