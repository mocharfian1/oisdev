<h3><b>A. ToSS Profile</b></h3> 
<div class="form-group text-center"> 
  <center><img class="img-thumbnail" src="<?php echo !empty($data->outlet_photo_file_name_64)?$data->outlet_photo_file_name_64:''; ?>" style="max-width: 400px; max-height: 200px; <?php echo !empty($data->outlet_photo_file_name_64)?$data->outlet_photo_file_name_64:'display:none;'; ?>"> </center>
    <table border="1" style="margin-top: 20px" width="100%"> 
        <tr> 
          <td style="padding: 10px; color: red; text-align: left"><b>Nama ToSS</b></td> 
          <td style="padding: 10px; color: red; text-align: left"><b> : <?=html_escape($data->nama_toss)?></b></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Dealer</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->dealer)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Cabang Induk</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->cabang_induk)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Tipe Bangunan</td> 
          <td style="padding: 10px; text-align: left"> : <?php echo strtoupper($data->tipe_bangunan); ?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Status Bangunan</td> 
          <td> 
            <table> 
              <tr> 
                <td style="padding: 10px; text-align: left"> : <?php echo strtoupper($data->status_bangunan); ?></td> 
                <td style="padding: 10px; text-align: left">Dari</td> 

                <td style="padding: 10px; text-align: left"> : <?php echo strtotime($data->tgl_sewa_start)>strtotime('2000-01-01')?date('d M Y', strtotime($data->tgl_sewa_start)):'-'; ?></td> 
                <td style="padding: 10px; text-align: left">Sampai</td> 
                <td style="padding: 10px; text-align: left"> : <?php echo strtotime($data->tgl_sewa_end)>strtotime('2000-01-01')?date('d M Y', strtotime($data->tgl_sewa_end)):'-'; ?></td> 
              </tr> 
            </table> 
          </td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Alamat</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->alamat)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px" align="left">Kecamatan</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->nama_kecamatan)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px" align="left">Kabupaten/Kota</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->nama_kota_kabupaten)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px" align="left">Provinsi</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->nama_provinsi)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px" align="left">Latitude</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->latitude)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Longitude</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->longitude)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Telp/Fax</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->telp_fax)?></td> 
        </tr> 
        <tr>
          <td style="padding: 10px; text-align: left" colspan="2"><b>PIC ToSS</b></td> 
        </tr>
        <tr> 
          <td style="padding: 10px; text-align: left">Nama</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->pic_nama)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">Jabatan</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->pic_jabatan)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 10px; text-align: left">No. HP</td> 
          <td style="padding: 10px; text-align: left"> : <?=html_escape($data->pic_no_hp)?></td> 
          <tr> 
            <td style="padding: 10px; text-align: left">Email</td> 
            <td style="padding: 10px; text-align: left"> : <?=html_escape($data->pic_email)?></td> 
          </tr> 
        </tr>

    </table> 
    <br>

    <div style="page-break-after: always; display: flex; " >
        <table width="100%" style="text-align: center;">
          <tr>
            <td colspan="3"><center><h3>PIC</h3></center></td>
          </tr>
          <tr>
            <td style="width: 60%;">
              <table border="1" width="100%" style="text-align: center;">
                <tr>
                  <td style="width: 30%;"><h4>PIC Dealer</h4></td>
                  <td style="width: 30%;"><h4>PIC TAM</h4></td>
                </tr>
                <tr >
                  <td style="height: 200px;"><img src="<?php echo $data->hand_sign_dealer_image_64; ?>" style="max-height: 200px; max-width: 200px;"></td>
                  <td style="height: 200px;"></td>
                </tr>
                <tr>
                  <td><?php echo !empty($data->hand_sign_dealer_nama)?$data->hand_sign_dealer_nama:'-'; ?><br><?php echo !empty($data->hand_sign_dealer_jabatan)?$data->hand_sign_dealer_jabatan:'-'; ?></td>
                  <td>.....<br>.....</td>
                </tr>
              </table>
            </td>
            <td>
                <table width="10%">
                  <tr>
                    <td></td>
                  </tr>
                </table>
            </td>
<!--             <td style="width: 30%; padding-right: 10px;">
              <table border="1" width="100%" style="text-align: center;">
                <tr>
                  <td><h4>PIC TAM</h4></td>
                </tr>
                <tr>
                  <td style="height: 200px;"></td>
                </tr>
                <tr>
                    <td>.....<br>.....</td>
                </tr>
              </table>
            </td> -->
            <td style="width: 30%;">
              <table border="1" width="100%" style="text-align: center;">
                <tr>
                  <td><h4>Diperiksa Oleh </h4></td>
                </tr>
                <tr>
                  <td style="height: 200px;"></td>
                </tr>
                <tr>
                    <td>.....<br>.....</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        

        

        
    </div>

  </div>

  <br>
  <h3 style="page-break-before: always;"><b>B. Kapasitas Bengkel</b></h3> 
  <div class="form-group"> 
    <table style="width: 700px;" border="1" > 
      <tr style=" text-align: center;"> 
        <td class="text-center" rowspan="6" width="30%"><h1 style="color: red"><b><?=html_escape($data->jumlah_stall)?></b></h1><br><br>Jumlah Stall</td> 
        <td class="text-center" rowspan="6"><h1 style="color: red"><b><?=html_escape($data->jumlah_stall_stall_gr)?></b></h1><br><br>Stall GR</td> 
        <td style="padding: 15px" width="40%"><b>Stall lain</b> :</td> 
      </tr> 
      <tr> 
        <td style="padding: 5px;"> 
          THS Motor : <?=html_escape($data->jumlah_stall_ths_motor)?> Unit 
        </td> 
      </tr> 
      <tr> 
        <td style="padding: 5px;"> 
          THS Mobil : <?=html_escape($data->jumlah_stall_ths_mobil)?> Unit 
        </td> 
      </tr> 
      <tr> 
        <td style="padding: 5px;"> 
          Lain-lain : <?=html_escape($data->jumlah_stall_lain_lain)?> Unit
        </td> 
      </tr> 
      <tr> 
        <td style="padding: 5px;"> 
          Keterangan : <?=html_escape($data->jumlah_stall_keterangan)?>
        </td> 
      </tr> 
      <tr> 
        <td style="padding: 5px">* Jika ada</td> 
      </tr> 
    </table><br> 
    <table border="1" width="100%">
        <tr>
            <td align="center" width="20%" style="padding: 5px; padding-right: 20px; padding-left: 20px">Jumlah Manpower</td>
            <td align="center" width="10%" style="padding: 5px; padding-left: 15px; padding-right: 15px"><h2 style="color: red"><b><?=html_escape($jumlah_manpower->pro_tech)?></b></h2>Pro Tech</td>
            <td align="center" width="10%" style="padding: 5px; padding-left: 25px; padding-right: 25px"><h2 style="color: red"><b><?=html_escape($jumlah_manpower->tech)?></b></h2>Tech</td>
            <td align="center" width="10%" style="padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b><?=html_escape($jumlah_manpower->admin)?></b></h2>Admin</td>
            <td align="center" width="10%" style=" padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b><?=html_escape($jumlah_manpower->others)?></b></h2>Others</td>
            <td width="40%" style="padding: 5px;">
              <span>Catatan :</span>
              <br>
              <?php echo wordwrap($data->jumlah_manpower_catatan,35,"<br>",TRUE); ?>
              <br>
              <span><small><b>Total Manpower : <?=html_escape($jumlah_manpower->semua)?> Orang</b></small></span>
            </td>
        </tr>
    </table>
  </div>

  <h3><b>C. Target & Investasi</b></h3> 
  <div class="form-group"> 
    <div class="portlet-title"> 
      <div class="caption font-dark"> 
        <span class="caption-subject bold uppercase">C1. TARGET</span> 
      </div> 
    </div><br> 
    <table border="1" width="60%" class="text-center" style="text-align: center;"> 
      <thead> 
        <tr> 
          <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Item</th> 
          <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y</th> 
          <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y  1</th> 
          <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y  2</th> 
          <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y  3</th> 
        </tr> 
      </thead> 
      <tbody>
        <tr> 
          <td style="padding: 3px">UE/Bulan</td> 
          <td style="text-align: center;"><?=html_escape($data->target_ue_bulan_year)?></td> 
          <td style="text-align: center;"><?=html_escape($data->target_ue_bulan_year_plus_1)?></td> 
          <td style="text-align: center;"><?=html_escape($data->target_ue_bulan_year_plus_2)?></td> 
          <td style="text-align: center;"><?=html_escape($data->target_ue_bulan_year_plus_3)?></td> 
        </tr> 
        <tr> 
          <td style="padding: 3px">UE/Hari</td> 
          <td><?=html_escape($data->target_ue_hari_year)?></td> 
          <td><?=html_escape($data->target_ue_hari_year_plus_1)?></td> 
          <td><?=html_escape($data->target_ue_hari_year_plus_2)?></td> 
          <td><?=html_escape($data->target_ue_hari_year_plus_3)?></td> 
        </tr> 
      </tbody> 
    </table> 
    <br> 
    <b>Note :</b> 
    <br> 
    <p>Y = Tahun sertifikasi | 1 Bulan = 22.5 hari 
      <div class="portlet-title"> 
        <div class="caption font-dark"> 
          <span class="caption-subject bold uppercase">C2. Investasi</span> 
        </div> 
      </div><br> 









      <table border="1" width="80%"> 
        <tbody> 
          <tr> 
            <td style="padding: 5px" width="5%">Pembelian lahan *</td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="padding: 5px"> : IDR </td> 
                  <td align="right" style="padding-right: 15px"><?php echo number_format_dots($data->investasi_pembelian_lahan); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
          <tr> 
            <td style="padding: 5px" width="5%">Sewa Bangunan</td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="padding: 5px"> : IDR </td> 
                  <td align="right" style="padding-right: 15px"><?php echo number_format_dots($data->investasi_sewa_bangunan); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
          <tr> 
            <td style="padding: 5px" width="5%">Renovasi Bangunan</td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="padding: 5px"> : IDR </td> 
                  <td align="right" style="padding-right: 15px"><?php echo number_format_dots($data->investasi_renovasi_bangunan); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
          <tr> 
            <td style="padding: 5px" width="5%">Corporate Identity</td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="padding: 5px"> : IDR </td> 
                  <td align="right" style="padding-right: 15px"><?php echo number_format_dots($data->investasi_corporate_identity); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
          <tr> 
            <td style="padding: 5px" width="5%">Equipment</td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="padding: 5px"> : IDR </td> 
                  <td align="right" style="padding-right: 15px"><?php echo number_format_dots($data->investasi_equipment); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
          <tr> 
            <td width="5%" style="padding: 5px; background-color: grey; color: white"><b>TOTAL</b></td> 
            <td width="10%"> 
              <table width="100%"> 
                <tr> 
                  <td style="background-color: grey; color: white; padding: 5px"> : IDR </td> 
                  <td style="text-align: right; border: none; background-color: grey; color: white; padding-right: 15px"><?php echo number_format_dots($data->investasi_total); ?></td> 
                </tr> 
              </table> 
            </td> 
          </tr> 
        </tbody> 
      </table> 
      <br> 
      <b>Note :</b> 
      <br> 
      <p>* Apabila milik sendiri 
      </div>

      <h3 style="page-break-before: always; "><b>D. Checklist Sertifikasi</b></h3> 
      <div class="form-body"> 

      <?php $no_area=1; ?>
      <?php if(!empty($area)){ ?>
          <?php $no_area=1; foreach ($area as $key => $value) { ?>
              <div class="portlet-title"> 
                <div class="caption font-dark"> 
                  <span class="caption-subject bold uppercase">D<?php echo $no_area; ?>. <?=html_escape($value->area_name)?></span> 
                </div> 
              </div><br>
              <?php if(!empty($value->sub_area)){ ?>
                  <?php $no_sub = 1; foreach ($value->sub_area as $key_sub => $value_sub) { ?>
                      <div class="form-group"> 
                        <table border="1" width="100%" class="text-center" style="font-size: 12px;"> 
                          <thead> 
                            <tr>
                              <th colspan="7" align="center" style="text-align: center; padding: 10px; background-color: grey; color: white"><?php echo $no_sub; ?>. <?=html_escape($value_sub->sub_area_name)?></th>
                            </tr> 
                            <tr> 
                              <td width="3%" style="text-align: center;  background-color: #fffc7c">No.</td> 
                              <td width="30%" style="text-align: center;  background-color: #fffc7c">Kriteria Minimum</td> 
                              <td width="5%" style="text-align: center;  background-color: #fffc7c">Jumlah</td> 
                              <td width="20%" style="text-align: center;  background-color: #fffc7c">Foto Dealer</td> 
                              <td width="20%" style="text-align: center;  background-color: #fffc7c">Keterangan</td> 
                              <td width="5%" style="text-align: center;  background-color: #fffc7c">Evaluasi Dealer</td> 
                              <td width="5%" style="text-align: center;  background-color: #fffc7c">Evaluasi TAM</td> 
                            </tr> 
                          </thead> 
                          <tbody> 
                            <?php if(!empty($value_sub->items)){ ?>
                                <?php $no_item = 1; foreach ($value_sub->items as $key_it => $value_it) { ?>
                                    <tr> 
                                      <td style=" vertical-align: top"><?php echo $no_item; ?>.</td> 
                                      <td align="left" style=" vertical-align: top"><?=html_escape($value_it->kriteria_minimum)?></td> 
                                      <td></td> 
                                      <td>
                                        <?php if(!empty($value_it->foto_dealer_file_name_64)){ ?>
                                            <img class="img-thumbnail" src="<?php echo (!empty($value_it->foto_dealer_file_name_64))?$value_it->foto_dealer_file_name_64:''; ?>" style="max-width: 200px">
                                        <?php } ?>
                                      </td> 
                                      <td align="left" style=" vertical-align: top"><?=html_escape($value_it->keterangan)?></td> 
                                      <td style=" vertical-align: top; text-align: center;"><?php echo $value_it->score_evaluasi_dealer==1?'V':''; ?></td> 
                                      <td style=" vertical-align: top; text-align: center;"><?php echo $value_it->score_evaluasi_tam==1?'V':''; ?></td> 
                                    </tr> 
                                <?php $no_item++; } ?>
                            <?php } ?>
                          </tbody> 
                        </table> 
                      </div> 
                      <br>
                  <?php $no_sub++; } ?>
              <?php }  ?>
              <br>
          <?php $no_area++; } ?>
      <?php } ?>
      </div> 

      <div class="portlet-title"> 
        <div class="caption font-dark"> 
          <span class="caption-subject bold uppercase">D<?php echo $no_area; $no_area++; ?>. Manpower</span><br><br> 
          <span class="caption-subject bold uppercase">A. Klasifikasi SDM After Sales</span> 
        </div> 
      </div><br>
      <div class="form-group">
        <table border="1" width="100%" class="text-center" style="font-size: 10px;">
          <thead>
            <tr>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">No.</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Nama Karyawan</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c" rowspan="4">Tgl Masuk</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c" rowspan="4">No. KTP</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Jabatan</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="10">Sertifikasi</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Keterangan</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Evaluasi Dealer</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="4">Evaluasi TAM</th>
            </tr>
            <tr>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="7">Teknisi</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" rowspan="2" colspan="3">SA GR</th>
            </tr>
            <tr>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="2">Level 1</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="3">Level 2</th>
              <th style="text-align: center; padding: 2px; background-color: #fffc7c" colspan="2">Level 3</th>
            </tr>
            <tr>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">TT</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">PT</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">DTG</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">DTL</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">DTC</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">DMT</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">LD</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">L1</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">L2</th>
              <th style="text-align: center; padding: 1px; background-color: #fffc7c">L3</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!empty($manpower_as)){ ?>
                <?php $no_manpower = 1; foreach ($manpower_as as $key => $value) { ?>
                    <tr>
                      <td width="3%"><?php echo $no_manpower; ?>.</td>
                      <td width="15%"><?=html_escape($value->nama_karyawan)?></td>
                      <td width="10%"><?php echo date('d M Y',strtotime($value->tgl_masuk)); ?></td>
                      <td width="10%"><?=html_escape($value->no_ktp)?></td>
                      <td width="10%"><?php echo $value->jabatan=='pro_tech'?'Pro Tech':ucfirst($value->jabatan); ?></td>
                      <td width="2%"><?php echo $value->jabatan=='tech'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='PT'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='DTG'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='DTL'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='DTC'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='DMT'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='LD'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='L1'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='L2'?'V':''; ?></td>
                      <td width="2%"><?php echo $value->level_training=='L3'?'V':''; ?></td>
                      <td width="15%"><?=html_escape($value->keterangan)?></td>
                      <td width="7%" style="text-align: center;"><?php echo $value->score_evaluasi_dealer==1?'V':''; ?></td>
                      <td width="7%" style="text-align: center;"><?php echo $value->score_evaluasi_tam==1?'V':''; ?></td>
                    </tr>
                <?php $no_manpower++; } ?>

            <?php }else{ ?>
                <tr>
                  <td colspan="18"><center>Data tidak ditemukan.</center></td>
                </tr>
            <?php } ?>
          </tbody>
        </table><br><br>

        
        Keterangan :<br><br>
        
        <table> 
          <tr><td>
            <table width="100%">

              <tbody>
                <tr>
                  <td colspan="2"><u>1. TRAINING TEKNISI</u><br><br></td>
                </tr>
                <tr>
                  <td>TT </td>
                  <td>: Toyota Technician</td>
                </tr>
                <tr>
                  <td>PT </td>
                  <td>: Pro Technician</td>
                </tr>
                <tr>
                  <td>DT </td>
                  <td>: Diagnostic Technician</td>
                </tr>
                <tr>
                  <td>DTG </td>
                  <td>: Diagnostic Technician Engine</td>
                </tr>
                <tr>
                  <td>DTL </td>
                  <td>: Diagnostic Technician Electrica</td>
                </tr>
                <tr>
                  <td>DTC </td>
                  <td>: Diagnostic Technician Chassis</td>
                </tr>
                <tr>
                  <td>DMT </td>
                  <td>: Diagnosis Master Technician</td>
                </tr>
                <tr>
                  <td>LD </td>
                  <td>: Latest Diagnosis</td>
                </tr>
              </tbody>
            </table>
          </td>
          <td style="vertical-align: top; padding-left: 20px">
            <table>
              <tbody>
                <tr>
                  <td colspan="2"><u>2. SERVICE ADVISOR GR</u><br><br></td>
                </tr>
                <tr>
                  <td>L1 </td>
                  <td>: Level 1 TS A21</td>
                </tr>
                <tr>
                  <td>L2 </td>
                  <td>: Level 2 TS A21</td>
                </tr>
                <tr>
                  <td>L3 </td>
                  <td>: Level 3 TS A21</td>
                </tr>
              </tbody>
            </table>
          </td></tr>
        </table>

      </div><br>

      <div class="portlet-title"> 
        <div class="caption font-dark"> 
          <span class="caption-subject bold uppercase">B. Klasifikasi SDM Umum</span><br>
        </div> 
      </div><br>
      <div class="form-group">

        <table border="1" style="width: 800px" class="text-center">
          <thead>
            <tr>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c">No.</th>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c">Nama Karyawan</th>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c">Tgl Masuk</th>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c">Jabatan</th>
              <th style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php $no_manpower_um = 1; if(!empty($manpower_umum)){ ?>
              <?php foreach ($manpower_umum as $key => $value) { ?>
                <tr>
                  <td width="5%"><?php echo $no_manpower_um; ?>.</td>
                  <td width="15%"><?=html_escape($value->nama_karyawan)?></td>
                  <td width="10%"><?php echo date('d-m-Y',strtotime($value->tgl_masuk)); ?></td>
                  <td width="15%"><?php echo ucfirst(str_replace('_', ' ',$value->jabatan)); ?></td>
                  <td width="15%"><?=html_escape($value->keterangan)?></td>
                </tr>
              <?php $no_manpower_um++; } ?>
            <?php }else{ ?>
                <tr>
                  <td colspan="5"><center>Data tidak ditemukan.</center></td>
                </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>  <br>



      <div class="portlet-title"> 
        <div class="caption font-dark"> 
          <span class="caption-subject bold uppercase">D<?php echo $no_area; ?>. Equipment</span> 
        </div> 
      </div><br>
      <div class="form-group"> 
        <table border="1" width="100%" class="text-center" style="font-size: 12px;"> 
          <thead> 
            <tr> 
              <th width="3%" style="text-align: center; padding: 5px; background-color: #fffc7c">No.</th> 
              <th width="20%" style="text-align: center; padding: 5px; background-color: #fffc7c">Kriteria Minimum</th> 
              <th width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Jumlah Min.</th> 
              <th width="20%" style="text-align: center; background-color: #fffc7c">Foto Dealer</th> 
              <th width="20%" style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</th> 
              <th width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi Dealer</th> 
              <th width="5%" style="text-align: center; padding: 5px; background-color: #fffc7c">Evaluasi TAM</th> 
            </tr> 
          </thead> 
          <tbody> 
            <?php if(!empty($equipment)){ ?>
                <?php $no_eq = 1; foreach ($equipment as $key => $value) { ?>
                    <tr> 
                      <td colspan="7" style="text-align: center; padding: 10px; background-color: grey; color: white"><?=html_escape($value->group_name)?></td> 
                    </tr> 
                    <?php if(!empty($value->items)){ ?>
                        <?php $no_item=1; foreach ($value->items as $key_it => $value_it) { ?>
                            <tr> 
                              <td style="padding: 5px; vertical-align: top"><?php echo $no_item; ?>.</td> 
                              <td align="left" style="padding: 5px; vertical-align: top"><?=html_escape($value_it->kriteria_minimum)?></td> 
                              <td style="padding: 5px; vertical-align: top"><?=html_escape($value_it->jumlah_minimum)?></td> 
                              <td> 
                                <?php if(!empty($value_it->foto)){ ?>
                                    <?php foreach ($value_it->foto as $key_ft => $value_ft) { ?>
                                        <?php if(!empty($value_ft->foto_dealer_file_name_64)){ ?>
                                            <img class="img-thumbnail" src="<?php echo (!empty($value_ft->foto_dealer_file_name_64))?$value_ft->foto_dealer_file_name_64:''; ?>" style="max-width: 200px"><br><br>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                              </td> 
                              <td style="padding: 5px; vertical-align: top"><?=html_escape($value_it->keterangan)?></td> 
                              <td style="padding: 5px; vertical-align: top; text-align: center;"><?php echo $value_it->score_evaluasi_dealer==1?'V':''; ?></td> 
                              <td style="padding: 5px; vertical-align: top; text-align: center;"><?php echo $value_it->score_evaluasi_tam==1?'V':''; ?></td> 
                            </tr> 
                        <?php $no_item++; } ?>
                    <?php } ?>
                <?php } ?>
            <?php }else{ ?>
                <tr>
                  <td colspan="7"><center>Data tidak ditemukan.</center></td>
                </tr>
            <?php } ?>
          </tbody> 
        </table> 
      </div> 
    </div>