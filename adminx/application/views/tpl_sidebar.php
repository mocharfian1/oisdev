<?php
    $user_type = $this->session->userdata('user_type');
?>
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-compact " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <?php if ($user_type=='superadmin') { ?>

                <li class="nav-item start <?php if(!empty($menu) && $menu == 'home'){echo 'active open';}; ?>">
                    <a href="<?=base_url('home')?>" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Home</span>
                        <?php if(!empty($menu) && $menu == 'home'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item start <?php if(!empty($menu) && $menu == 'project_picture'){echo 'active open';}; ?>">
                    <a href="<?=base_url('project_picture/set_page/view')?>" class="nav-link nav-toggle">
                        <i class="fa fa-image"></i>
                        <span class="title">Project Picture</span>
                        <?php if(!empty($menu) && $menu == 'project_picture'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo 'active open';}; ?>">
                    <a href="<?php echo base_url('evaluasi_kunjungan/set_page/view'); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="title"> Evaluasi Kunjungan</span>
                        <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo '<span class="selected"></span><span class=""></span>';}else{echo '<span class=""></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'report'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-book"></i>
                        <span class="title"> Report</span>
                        <?php if(!empty($menu) && $menu == 'report'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/buku_epm'); ?>" class="nav-link ">
                                
                                <span class="title"> Buku EPM</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/checklist_summary'); ?>" class="nav-link ">
                                
                                <span class="title"> Checklist Summary</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/catatan_evaluasi'); ?>" class="nav-link ">
                                
                                <span class="title"> Catatan Evaluasi</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/toss'); ?>" class="nav-link ">
                                
                                <span class="title"> TOSS</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'master_data'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database"></i>
                        <span class="title"> Master Data</span>
                        <?php if(!empty($menu) && $menu == 'master_data'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/area/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/sub_area/view'); ?>" class="nav-link ">
                                
                                <span class="title">Sub Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/user/view'); ?>" class="nav-link ">
                                
                                <span class="title"> User</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        

                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'guidance_toss'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/guidance_toss/config'); ?>" class="nav-link ">
                                
                                <span class="title">Guidance TOSS</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'guidance_toss'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_database/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Database</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_dealer/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Dealer</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project_level_training'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project_level_training/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project Level Training</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project_level_training'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project_template'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project_template/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project Template</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project_template'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        
                    </ul>
                </li>

            <?php } elseif ($user_type=='admin_project') { ?>
            
                <li class="nav-item start <?php if(!empty($menu) && $menu == 'home'){echo 'active open';}; ?>">
                    <a href="<?=base_url('home')?>" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Home</span>
                        <?php if(!empty($menu) && $menu == 'home'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item start <?php if(!empty($menu) && $menu == 'project_picture'){echo 'active open';}; ?>">
                    <a href="<?=base_url('project_picture/set_page/view')?>" class="nav-link nav-toggle">
                        <i class="fa fa-image"></i>
                        <span class="title">Project Picture</span>
                        <?php if(!empty($menu) && $menu == 'project_picture'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo 'active open';}; ?>">
                    <a href="<?php echo base_url('evaluasi_kunjungan/set_page/view'); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="title"> Evaluasi Kunjungan</span>
                        <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo '<span class="selected"></span><span class=""></span>';}else{echo '<span class=""></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'report'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-book"></i>
                        <span class="title"> Report</span>
                        <?php if(!empty($menu) && $menu == 'report'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/buku_epm'); ?>" class="nav-link ">
                                
                                <span class="title"> Buku EPM</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/checklist_summary'); ?>" class="nav-link ">
                                
                                <span class="title"> Checklist Summary</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/catatan_evaluasi'); ?>" class="nav-link ">
                                
                                <span class="title"> Catatan Evaluasi</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'master_data'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database"></i>
                        <span class="title"> Master Data</span>
                        <?php if(!empty($menu) && $menu == 'master_data'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/area/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/sub_area/view'); ?>" class="nav-link ">
                                
                                <span class="title">Sub Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/user/view'); ?>" class="nav-link ">
                                
                                <span class="title"> User</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project_level_training'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project_level_training/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project Level Training</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project_level_training'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project_template'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project_template/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project Template</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project_template'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'project'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/project/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Project</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'project'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        
                    </ul>
                </li>

            <?php } elseif ($user_type=='admin_toss') { ?>

                <li class="nav-item start <?php if(!empty($menu) && $menu == 'home'){echo 'active open';}; ?>">
                    <a href="<?=base_url('home')?>" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Home</span>
                        <?php if(!empty($menu) && $menu == 'home'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'report'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-book"></i>
                        <span class="title"> Report</span>
                        <?php if(!empty($menu) && $menu == 'report'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/toss'); ?>" class="nav-link ">
                                
                                <span class="title"> TOSS</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'master_data'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database"></i>
                        <span class="title"> Master Data</span>
                        <?php if(!empty($menu) && $menu == 'master_data'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/area/view'); ?>" class="nav-link ">
                                
                                <span class="title"> Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/sub_area/view'); ?>" class="nav-link ">
                                
                                <span class="title">Sub Area</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'sub_area'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data/user/view'); ?>" class="nav-link ">
                                
                                <span class="title"> User</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'user'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        

                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'guidance_toss'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/guidance_toss/config'); ?>" class="nav-link ">
                                
                                <span class="title">Guidance TOSS</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'guidance_toss'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_database/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Database</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>

                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_dealer/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Dealer</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php } elseif ($user_type=='tam') { ?>
           
                <li class="nav-item start <?php if(!empty($menu) && $menu == 'home'){echo 'active open';}; ?>">
                    <a href="<?=base_url('home')?>" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Home</span>
                        <?php if(!empty($menu) && $menu == 'home'){echo '<span class="selected"></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo 'active open';}; ?>">
                    <a href="<?php echo base_url('evaluasi_kunjungan/set_page/view'); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-calendar-check-o"></i>
                        <span class="title"> Evaluasi Kunjungan</span>
                        <?php if(!empty($menu) && $menu == 'evaluasi_kunjungan'){echo '<span class="selected"></span><span class=""></span>';}else{echo '<span class=""></span>';}; ?>
                    </a>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'report'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-book"></i>
                        <span class="title"> Report</span>
                        <?php if(!empty($menu) && $menu == 'report'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/buku_epm'); ?>" class="nav-link ">
                                
                                <span class="title"> Buku EPM</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'buku_epm'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/checklist_summary'); ?>" class="nav-link ">
                                
                                <span class="title"> Checklist Summary</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'checklist_summary'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/catatan_evaluasi'); ?>" class="nav-link ">
                                
                                <span class="title"> Catatan Evaluasi</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'catatan_evaluasi'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('report/view_report/toss'); ?>" class="nav-link ">
                                
                                <span class="title"> TOSS</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item <?php if(!empty($menu) && $menu == 'master_data'){echo 'active open';}; ?>">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-database"></i>
                        <span class="title"> Master Data</span>
                        <?php if(!empty($menu) && $menu == 'master_data'){echo '<span class="selected"></span><span class="arrow open"></span>';}else{echo '<span class="arrow"></span>';}; ?>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_database/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Database</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_database'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>

                        <li class="nav-item <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo 'active open';}; ?>">
                            <a href="<?php echo base_url('master_data_toss/toss_dealer/view'); ?>" class="nav-link ">
                                
                                <span class="title">TOSS Dealer</span>
                                <?php if(!empty($sub_menu) && $sub_menu == 'toss_dealer'){echo '<span class="selected"></span>';}; ?>
                            </a>
                        </li>
                    </ul>
                </li>
            
            <?php } ?>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
