<script type="text/javascript">
      var URL = '<?php echo base_url(); ?>';
</script>

<?php if ($sub_menu == 'guidance_toss' && $mode=='config') { ?>

  <div class="page-content">
   
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Guidance TOSS
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Guidance TOSS</a>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <form action="#" id="form_toss" class="form-horizontal" role="form">
              
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">A. TOSS Profile</span>
                      </div>
                      <div class="tools"> 
                        <input type="text" value="<?php echo $sub_menu; ?>" id="sub_menu_active" style="display: none">
                        <input type="text" value="<?php echo $mode; ?>" id="mode_active" style="display: none">
                      </div>
                  </div>
                  <div class="portlet-body form">

                          <!-- <form action="javascript:;" id="formPhotoOutlet" name="formPhotoOutlet" method="post" enctype="multipart/form-data"> 
                          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        -->
                            <div class="form-body">
                            <div class="form-group">
                              <div class="col-sm-2">
                                <label for="imgInput2" class="control-label pull-right">Outlet</label>
                              </div>
                              <div class="col-sm-8">
                                <div id="formHeader2" runat="server" class="col-xs-4">
                                    <a onclick="$('#imgInput2').click();"><img id="imgHdr2" class="img-thumbnail" src="<?php echo !empty($data_project->outlet_pic_file_path) ? $data_project->outlet_pic_file_path : base_url('upload/outlet/default.jpg');?>" alt="your image" width="200" height="130"/></a>                                            
                                    <input type="text" name="imgInput2Path" value="" style="display: none">
                                    <input type='file' id="imgInput2" name="imgInput2" accept=".png, .jpg, .jpeg" style="display: none"/>
                                </div>
                              </div>
                            </div>
                            </div>
                            <!-- <input type="submit" id="submitPhotoOutlet" name="" style="display: none"> -->
                          <!-- </form>   -->
                          <div class="form-body">
                              
                              
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Nama TOSS</label>
                                  <div class="col-md-4">
                                      <input id="nama_toss" name="nama_toss" type="text" value="<?=(isset($data_toss)?$data_toss->nama_toss:'')?>" placeholder="please fill nama TOSS" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Dealer</label>
                                  <div class="col-md-4">
                                      <input id="dealer" name="dealer" type="text" value="<?=(isset($data_toss)?$data_toss->dealer:'')?>" placeholder="please fill dealer" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Cabang Induk</label>
                                  <div class="col-md-4">
                                      <input id="cabang_induk" name="cabang_induk" type="text" value="<?=(isset($data_toss)?$data_toss->cabang_induk:'')?>" placeholder="please fill cabang induk" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Tipe Bangunan</label>
                                  <div class="col-md-3" style="padding-right: 40px">
                                      <select id="tipe_bangunan" name="tipe_bangunan" style="margin-left: 15px" class="frm_ form-control" >
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="Ruko" <?php if(!empty($data_toss) && $data_toss->tipe_bangunan == 'Ruko'){echo 'selected';}; ?> >Ruko</option>
                                            <option value="Non-Ruko" <?php if(!empty($data_toss) && $data_toss->tipe_bangunan == 'Non-Ruko'){echo 'selected';}; ?> >Non-Ruko</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-6 control-label">Status Bangunan</label>
                                  <div class="col-md-6">
                                      <select name="status_bangunan" id="status_bangunan" style="margin-left: 15px; min-width: 150px" class="frm_ form-control" >
                                          <option selected="true" disabled="disabled">- please select -</option>
                                          <option value="Sewa" <?php if(!empty($data_toss) && $data_toss->status_bangunan == 'Sewa'){echo 'selected';}; ?> >Sewa</option>
                                          <option value="Milik Pribadi" <?php if(!empty($data_toss) && $data_toss->status_bangunan == 'Renovation'){echo 'selected';}; ?> >Milik Pribadi</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-2 control-label">Dari</label>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="sewa_dari" name="tgl_sewa_start" placeholder="please choose date" class="frm_ form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_toss)){echo date('d M Y', strtotime($data_toss->tgl_sewa_start));}; ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-4 control-label">Sampai</label>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                        <input id="sewa_sampai" name="tgl_sewa_end" placeholder="please choose date" class="frm_ form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_toss)){echo date('d M Y', strtotime($data_toss->tgl_sewa_end));}; ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>
                              <div class="row"></div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Alamat</label>
                                  <div class="col-md-5">
                                        <textarea class="frm_ form-control" name="provinsi" placeholder="Please fill alamat" style="margin-left: 15px"></textarea>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Provinsi</label>
                                  <div class="col-md-5">
                                        <input type="text" class="frm_ form-control" name="provinsi" placeholder="Please fill provinsi" style="margin-left: 15px">
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Kota/Kabupaten</label>
                                  <div class="col-md-5">
                                        <input type="text" class="frm_ form-control" name="kota_kabupaten" placeholder="Please fill kota/kabupaten" style="margin-left: 15px">
                                  </div>
                              </div>

                              <div class="form-group col-sm-12">
                                  <label class="col-md-2 control-label">Kecamatan</label>
                                  <div class="col-md-5">
                                        <input type="text" class="frm_ form-control" name="kecamatan" placeholder="Please fill kecamatan" style="margin-left: 15px">
                                  </div>
                              </div>


                             
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Telp/Fax.</label>
                                  <div class="col-md-4">
                                      <input id="telp_fax" name="telp_fax" type="text" value="" placeholder="please fill telp/fax" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>

                              <hr>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Latitude & Longitude</h4>
                                    </div>
                                </div>
                              <hr>
                              <center>
                                <div style="z-index: 1">
                                  <input id="pac-input" class="form-control controls" type="text" placeholder="Anything you want, enter your keyword here...">
                                  <br><br>
                                  <div id="map" style="width: 100%;height:400px"></div>
                                </div>
                                <br>
                                <div class="form-group">
                                    
                                    <div class="col-md-3 col-md-offset-3">
                                        <label class="control-label" style="font-size: large;">Latitude</label>
                                        <br><br>
                                        <input id="latitude" name="latitude" type="text" value="" placeholder="please set location" class="frm_ name form-control" style="text-align: center;"  />

                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label" style="font-size: large;">Longitude</label>
                                        <br><br>
                                        <input id="longitude" name="longitude" type="text" value="" placeholder="please set location" class="frm_ name form-control" style="text-align: center;"  />
                                    </div>
                                </div>
                              </center>
                              <hr>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">PIC TOSS</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nama</label>
                                    <div class="col-md-4">
                                        <input id="nama" name="pic_nama" type="text" value="" placeholder="please fill nama" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jabatan</label>
                                    <div class="col-md-4">
                                        <input id="jabatan" name="pic_jabatan" type="text" value="" placeholder="please fill jabatan" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">No Hp.</label>
                                    <div class="col-md-4">
                                        <input id="no_hp" name="pic_no_hp" type="text" value="" placeholder="please fill No HP." class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" name="pic_email" type="text" value="" placeholder="please fill email" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                              <hr style="margin-left: 50px; margin-right: 50px">
                        </div>
                  </div>


              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">B. Kapasitas Bengkel</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                       
                          <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Jumlah Stall</h4>
                                    </div>
                                </div>
                                <hr>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Stall GR</label>
                                  <div class="col-md-4">
                                      <input id="stall_gr" name="jumlah_stall_stall_gr" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_stall_stall_gr:'')?>" placeholder="please fill Stall GR" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label"><b>Stall Lain</b></label>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">THS Motor</label>
                                  <div class="col-md-4">
                                      <input id="ths_motor" name="jumlah_stall_ths_motor" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_stall_ths_motor:'')?>" placeholder="please fill THS Motorcycle" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">THS Mobil</label>
                                  <div class="col-md-4">
                                      <input id="ths_mobil" name="jumlah_stall_ths_mobil" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_stall_ths_mobil:'')?>" placeholder="please fill THS Car" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Lain-lain</label>
                                  <div class="col-md-4">
                                      <input id="lain_lain" name="jumlah_stall_lain_lain" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_stall_lain_lain:'')?>" placeholder="please fill lain-lain" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Keterangan</label>
                                  <div class="col-md-4">
                                      <input id="lain_lain" name="jumlah_stall_keterangan" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_stall_keterangan:'')?>" placeholder="please fill keterangan" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Jumlah Manpower</h4>
                                    </div>
                                </div>
                                <hr>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Pro Tech</label>
                            <div class="col-md-4">
                                <input id="pro_tech" name="jumlah_manpower_pro_tech" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_pro_tech:'')?>" placeholder="please fill pro tech" class="frm_ name form-control" style="margin-left: 15px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Tech</label>
                            <div class="col-md-4">
                                <input id="tech" name="jumlah_manpower_tech" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_tech:'')?>" placeholder="please fill tech" class="frm_ name form-control" style="margin-left: 15px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Admin</label>
                            <div class="col-md-4">
                                <input id="admin" name="jumlah_manpower_admin" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_admin:'')?>" placeholder="please fill admin" class="frm_ name form-control" style="margin-left: 15px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Other</label>
                            <div class="col-md-4">
                                <input id="other" name="jumlah_manpower_other" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_other:'')?>" placeholder="please fill other" class="frm_ name form-control" style="margin-left: 15px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-4">
                                <input id="total" name="jumlah_manpower_total" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_total:'')?>" placeholder="please fill total" class="frm_ name form-control" style="margin-left: 15px" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Catatan</label>
                            <div class="col-md-4">
                                <textarea id="catatan" name="jumlah_manpower_catatan" type="text" value="<?=(isset($data_toss)?$data_toss->jumlah_manpower_catatan:'')?>" placeholder="please fill catatan" class="frm_ name form-control" style="margin-left: 15px"/></textarea>
                            </div>
                        </div>
                        </div>
                  </div>


              </div>

               <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">C. Target dan Investasi</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                            
                          <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <h4 style="margin-left: 15px">C1.Target</h4>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">&nbsp;</label>
                                <div class="col-md-6">
                                    <table border="1" width="100%" class="text-center">
                                        <thead>
                                            <tr>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Item</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 1</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 2</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="padding: 3px">UE/Bulan</td>
                                                <td><input style="text-align: center;" type="number" name="target_ue_bulan_year" value="<?=((isset($data_toss)?$data_toss->target_ue_bulan_year:''))?>" class="frm_ form-control target"></td>
                                                <td><input style="text-align: center;" type="number" name="target_ue_bulan_year_plus_1" value="<?=((isset($data_toss)?$data_toss->target_ue_bulan_year_plus_1:''))?>" class="frm_ form-control target"></td>
                                                <td><input style="text-align: center;" type="number" name="target_ue_bulan_year_plus_2" value="<?=((isset($data_toss)?$data_toss->target_ue_bulan_year_plus_2:''))?>" class="frm_ form-control target"></td>
                                                <td><input style="text-align: center;" type="number" name="target_ue_bulan_year_plus_3" value="<?=((isset($data_toss)?$data_toss->target_ue_bulan_year_plus_3:''))?>" class="frm_ form-control target"></td>
                                            </tr>

                                            
                                            <tr>
                                                <td style="padding: 3px">UE/Hari</td>
                                                <td><input  style="text-align: center;" type="text" name="target_ue_hari_year" value="<?=((isset($data_toss)?$data_toss->target_ue_hari_year:''))?>" class="frm_ form-control"></td>
                                                <td><input  style="text-align: center;" type="text" name="target_ue_hari_year_plus_1" value="<?=((isset($data_toss)?$data_toss->target_ue_hari_year_plus_1:''))?>" class="frm_ form-control"></td>
                                                <td><input  style="text-align: center;" type="text" name="target_ue_hari_year_plus_2" value="<?=((isset($data_toss)?$data_toss->target_ue_hari_year_plus_2:''))?>" class="frm_ form-control"></td>
                                                <td><input  style="text-align: center;" type="text" name="target_ue_hari_year_plus_3" value="<?=((isset($data_toss)?$data_toss->target_ue_hari_year_plus_3:''))?>" class="frm_ form-control"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <b>Note :</b>
                                    <br>
                                    <p>Y = Tahun sertifikasi | 1 Bulan = 22.5 hari</p>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <h4 style="margin-left: 15px">C2. Investasi</h4>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">&nbsp;</label>
                                <div class="col-md-6">
                                    <table border="1" width="100%" id="tb_investasi">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Pembelian lahan *</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="investasi_pembelian_lahan" value="<?=((isset($data_toss)?$data_toss->investasi_pembelian_lahan:''))?>" class="frm_ form-control sum"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Sewa Bangunan</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="investasi_sewa_bangunan" value="<?=((isset($data_toss)?$data_toss->investasi_sewa_bangunan:''))?>" class="frm_ form-control sum"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Renovasi Bangunan</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="investasi_renovasi_bangunan" value="<?=((isset($data_toss)?$data_toss->investasi_renovasi_bangunan:''))?>" class="frm_ form-control sum"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Corporate Identity</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="investasi_corporate_identity" value="<?=((isset($data_toss)?$data_toss->investasi_corporate_identity:''))?>" class="frm_ form-control sum"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Equipment</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="investasi_equipment" value="<?=((isset($data_toss)?$data_toss->investasi_equipment:''))?>" class="frm_ form-control sum"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" style="padding: 5px; background-color: grey; color: white"><b>TOTAL</b></td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="background-color: grey; color: white; padding: 5px"> : IDR </td>
                                                            <td style="background-color: grey; color: white; padding: 5px; text-align: right"> 
                                                              <b id="investasi_total"></b>
                                                              <input style="text-align: right; border: none" type="hidden" name="investasi_total" value="<?=((isset($data_toss)?$data_toss->investasi_total:''))?>" class="frm_ form-control">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <b>Note :</b>
                                    <br>
                                    <p>* Apabila milik sendiri</p>
                                </div>
                            </div>

                          </div>

                  </div>
               </div>

                <div class="portlet light ">
                  <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-doc font-dark"></i>
                            <span class="caption-subject bold">D. Checklist Sertifikasi (Area)</span>
                            <!-- <a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" > Add New Area </a> -->
                        </div>
                     
                        <div class="actions pull-left" style="padding-left: 30px">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addAreaDialog();"> Add Area </a>
                            </div>
                        </div>
                    
                  </div>
                  <div class="portlet-body form">
                      <div id="__area" class="form-body">
                          <?php if(!empty($getDataItems)){ ?>
                              <?php $no=1; foreach ($getDataArea as $key => $value) { ?>
                                  <div class="area_" id_area="<?php echo $value->id_area; ?>" status="from_server">
                                      <div class="form-group form-control-inline">
                                          <div class="col-md-12">
                                              <table>
                                                  <tr>
                                                      <td><h4 style="margin-left:15px;padding-right: 30px;">D<span class="num_area"><?php echo $no;  ?></span>. <?=html_escape($value->area_name)?></h4></td>
                                                      <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addSubAreaDialog(<?php echo $value->id_area; ?>,$(this));"> Add Sub Area </a></td>
                                                      <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_area',null,null,$(this));"> Delete </a></td>
                                                  </tr>
                                              </table>
                                          </div>
                                      </div>
                                      <!-- SUB AREA -->
                                            <?php $no_sub=1; foreach ($getDataSub as $key_sub => $value_sub) { ?>
                                                      <?php if($value_sub->id_area==$value->id_area){ ?>
                                                            <div class="sub_area_" id_sub="<?php echo $value_sub->id_sub_area; ?>" status="from_server">
                                                                  <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <table>
                                                                            <tr>
                                                                                <td><h5 style="margin-left:15px;padding-right: 30px"><span class="num_sub" ar_id=<?php echo $value->id_area; ?>><?php echo $no_sub; ?></span>.<?=html_escape($value_sub->sub_area_name)?></h5></td>
                                                                                <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('sub_area',<?php echo $value->id_area; ?>,<?=html_escape($value_sub->id_sub_area)?>,$(this));"> Add Item </a></td>
                                                                                <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_sub_area',null,null,$(this));"> Delete </a></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                  </div>
                                                                  <hr>

                                                                  <div style="margin-left:15px;" status="form-group col-sm-12">
                                                                        <table class="table table-striped table-hover tb_sub_item">
                                                                                <thead>
                                                                                        <tr>
                                                                                            <th>Kriteria Minimum</th>
                                                                                            <th style="width: 5%">Jumlah</th>
                                                                                            <th>Foto Contoh</th>
                                                                                            <th>&nbsp;</th>
                                                                                        </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                        <!-- ITEMS -->
                                                                                        <?php foreach ($getDataItems as $key_item => $value_item) { ?>
                                                                                              <?php if(($value_item->id_area==$value->id_area) &&($value_item->id_sub_area==$value_sub->id_sub_area)){ ?>
                                                                                                      <tr class="items__" id_item="<?php echo $value_item->id_item; ?>" id_area="<?php echo $value_item->id_area; ?>" id_sub="<?php echo $value_item->id_sub_area; ?>" status="from_server">
                                                                                                          <td><?=html_escape($value_item->kriteria_minimum)?></td>
                                                                                                          <td><?=html_escape($value_item->jumlah)?></td>
                                                                                                          <td>
                                                                                                              <a href="javascript:;"><img src="<?php echo $value_item->foto_contoh_file_name_64; ?>" class="img-thumbnail token-`+t_img+`" onclick="imagePreviewDialog('<?php echo $value_item->foto_contoh_file_name_64;  ?>')" style="max-width: 100px"></a>
                                                                                                          </td>
                                                                                                          <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item',<?php echo $value_item->id_item; ?>,'from_server',$(this));" data-toggle="dropdown"> Delete </a></td>
                                                                                                      </tr>
                                                                                              <?php } ?>
                                                                                        <?php } ?>
                                                                                        <!-- END ITEMS -->
                                                                                </tbody>
                                                                        </table>
                                                                  </div>
                                                            </div>
                                                      <?php $no_sub++; } ?>
                                            <?php } ?>
                                      <!-- END SUB AREA -->
                                      <hr>
                                  </div>
                              <?php $no++; } ?>
                          <?php } ?>
                          
                      </div>

                      <div id="__equipment" class="form-body">
                          <!-- EQUIMENT -->
                          <div class="form-group form-control-inline">
                              <div class="col-md-12">
                                  <table>
                                      <tr>
                                          <td><h4 style="margin-left: 15px; padding-right: 30px">Equipment</h4></td>
                                          <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addEquipmentGroupDialog();"> Add Group </a></td>
                                      </tr>
                                  </table>
                                  <?php if(!empty($getDataEQGroup)){ ?>
                                      <?php foreach ($getDataEQGroup as $key => $value) { ?>
                                          <div class="equipment_" key="<?php echo $value->group_key; ?>" >
                                              <div class="form-group">
                                                  <div class="col-md-12">
                                                      <table>
                                                          <tr>
                                                              <td>
                                                                  <h5 style="margin-left: 15px; padding-right: 30px"><?=html_escape($value->group_name)?></h5>
                                                              </td>
                                                              <td>
                                                                  <a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('equipment','','',$(this),<?php echo $value->group_key; ?>,'<?=html_escape($value->group_name)?>');"> Add Item </a>
                                                              </td>
                                                              <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_group',null,null,$(this));"> Delete </a></td>
                                                          </tr>
                                                      </table>
                                                  </div>
                                              </div>
                                              <hr>

                                              <div  style="margin-left:15px;">
                                                  <table class="table table-striped table-hover tb_group_item">
                                                      <thead>
                                                          <tr>
                                                              <th>Kriteria Minimum</th>
                                                              <th style="width: 5%">Jumlah</th>
                                                              <th>Foto Contoh</th>
                                                              <th>Keterangan</th>
                                                              <th>&nbsp;</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <?php foreach ($getDataEQItems as $k_eq_items => $v_eq_items) { ?>
                                                              <?php if($v_eq_items->group_key==$value->group_key){ ?>
                                                                  <tr key="<?php echo $v_eq_items->group_key; ?>" group_name="<?php echo $v_eq_items->group_name; ?>" class="eq_items__" id_eq_item="<?php echo $v_eq_items->id; ?>" status="from_server">
                                                                      <td><?=html_escape($v_eq_items->kriteria_minimum)?></td>
                                                                      <td><?=html_escape($v_eq_items->jumlah_minimum)?></td>
                                                                      <td>
                                                                          <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" src="<?php echo $v_eq_items->foto_contoh_file_name_64; ?>" onclick="imagePreviewDialog('<?php echo $v_eq_items->foto_contoh_file_name_64; ?>')" style="max-width: 100px"></a>
                                                                      </td>
                                                                      <td><?=html_escape($v_eq_items->keterangan)?></td>
                                                                      <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item',<?php echo $v_eq_items->id; ?>,'from_server',$(this),'yes');" data-toggle="dropdown"> Delete </a></td>
                                                                  </tr>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </tbody>
                                                  </table>
                                              </div>
                                          </div>
                                      <?php } ?>
                                  <?php } ?>                                  
                              </div>
                          </div>

                          <?php if(1+1==3){ ?>
                              <div class="form-group form-control-inline">
                                  <div class="col-md-12">
                                      <table>
                                          <tr>
                                              <td><h4 style="margin-left: 15px; padding-right: 30px">Equipment</h4></td>
                                              <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addEquipmentGroupDialog();"> Add Group </a></td>
                                          </tr>
                                      </table>
                                      
                                      
                                  </div>
                              </div>
                          <?php } ?>
                          <!-- <hr> -->
                          
                          <!-- /////////////////////////////////////////// -->
                      </div>
                  </div>


              </div>

              <div class="form-group">
                  <div class="col-md-5" style="padding-left: 45px">
                    <a href="javascript:;" id="btnSave" onclick="submit_($('#form_toss'),'guidance')"class="btn btn-lg green">Save</a>
                    <a id="btnCancel" href="<?php echo base_url('master_data_toss/guidance_toss/config'); ?>" class="btn btn-lg btn-danger">Reload</a>
                  </div>
              </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>
  <style type="text/css">
      .select2-dropdown {
          z-index: 99999999
      }
  </style>

<?php } elseif ($sub_menu == 'toss_database' && $mode=='view') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          TOSS Database
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Database</a>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <form action="javascript:;" id="form" class="form-horizontal" role="form">
              
                <div class="portlet light ">
                    <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">TOSS List</span>
                      </div>
                      <div class="pull-right">
                          <a href="<?php echo base_url("master_data_toss/toss_database/add") ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                                  <tr>
                                      <th class="" width="5%">No.</th>
                                      <th class="">Nama Toss</th>
                                      <th class="">Dealer</th>
                                      <th class="">Cabang Induk</th>
                                      <th class="">Kota / Kabupaten</th>
                                      <th class="">Status</th>
                                      <th class="">Attachment</th>
                                      <th class="">Last Update</th>
                                  </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($tb_db_toss)){ ?>
                                  <?php $no=1; foreach ($tb_db_toss as $key => $value) { ?>
                                            <tr>
                                              <td><?php echo $no; ?></td>
                                              <td><a href="<?php echo base_url('master_data_toss/toss_database/content/').$value->id; ?>"><?=html_escape($value->nama_toss)?></a></td>
                                              <td><?=html_escape($value->dealer)?></td>
                                              <td><?=html_escape($value->cabang_induk)?></td>
                                              <td><?=html_escape($value->kota_kabupaten_name)?></td>
                                              <td><?=html_escape($value->status)?></td>
                                              <td><a href="<?php echo !empty($value->sertifikasi_attachment_file_name)?base_url('upload/toss_attachments/').$value->sertifikasi_attachment_file_name:'#'; ?>"><?php echo !empty($value->sertifikasi_attachment_file_name)?'Download Attach':''; ?></a></td>
                                              <td><?php echo $value->pernah_update==1?date('d M Y',strtotime($value->final_submit_date)):'-'; ?></td>
                                            </tr>
                                  <?php $no++; }   ?>
                                <?php } ?>
                            </tbody>
                      </table>
                    </div>

                </div>

            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } elseif ($sub_menu == 'toss_database' && $mode=='content') { ?>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  
  <?php 
    function toIDR($x){
        return number_format($x,0,',','.');
    }
  ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          TOSS Content
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Database</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Content</a>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <form action="javascript:;" id="form_TOSS_DB" class="form-horizontal" role="form">
              
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">Additional Content</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">

                          <div class="form-body" id="__frm_pertama">
                           
                              <div class="form-group">
                                  <label class="col-md-2 control-label">User Dealer</label>
                                  <div class="col-md-4">
                                      <input id="dealer" name="dealer" type="text" value="<?php echo $data_toss->user_dealer; ?>" class="name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Last Update</label>
                                  <div class="col-md-4">
                                      <input id="dealer" name="dealer" type="text" value="<?php echo $data_toss->pernah_update==1?date('d M Y',strtotime($data_toss->final_submit_date)):'-'; ?>" class="name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                                <div class="form-group frm_pic_visit">
                                  <label class="col-md-2 control-label">Visit</label>
                                  <div class="col-md-3">
                                      <input type="text" class="form-control" placeholder="PIC Name" id="item_name" name="visit_name" style="margin-left: 15px">
                                  </div>
                                  <div class="col-md-3">
                                    <div class="input-group">
                                        <input id="sewa_dari" name="visit_tanggal" class="form-control form-control-inline input-medium date-picker" placeholder="Date" type="text" value="" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <a href="javascript:;" class="btn btn-primary" onclick="addPIC(<?php echo $data_toss->id; ?>,$(this))" style="margin-left: 15px">Add To List</a>
                                  </div>
                                </div>

                              
                                <div class="form-group">
                                  <label class="col-md-2 control-label">List Visit</label>
                                  <div class="col-md-5">
                                        
                                           <table border="1" width="100%" style="margin-left: 15px" id="list_PIC">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center; padding: 5px; background-color: #f2f2f2">PIC Name</td>
                                                        <th style="text-align: center; padding: 5px; background-color: #f2f2f2">Date</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if(!empty($ls_pic_visit)){ ?>
                                                        <?php foreach($ls_pic_visit as $key => $value){ ?>
                                                            <tr class="ls_pic" pic_visit_id="<?php echo $value->id; ?>" pic_visit_name="<?php echo $value->visit_pic_name ?>" pic_visit_date="<?php echo $value->visit_date; ?>" status="from_server">
                                                                <td style="padding: 5px" width="70%"><a onclick="del_pic(<?php echo $value->id; ?>,$(this))"><?=html_escape($value->visit_pic_name)?></a></td>
                                                                <td style="padding: 5px; width: 150px; text-align: center"><?php echo date('d M Y',strtotime($value->visit_date)); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <!-- <tr>
                                                        <td style="padding: 5px" width="70%"><a onclick="alert('Not available in demo');">aaa</a></td>
                                                        <td style="padding: 5px; width: 150px; text-align: center">17 Nov 2018</td>
                                                    </tr> -->
                                                </tbody>
                                            </table>
                                            
                                            <i style="margin-left: 15px">*click PIC name to delete data</i>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-md-2 control-label">Sertifikasi</label>
                                  <div class="col-md-3">
                                    <div style="margin-left: 15px !important" >
                                        <input type="checkbox" tb="tbl_toss_database" data-toggle="toggle" data-on="Certified" data-off="Uncertified" name="status" data_id="<?php echo $data_toss->id; ?>" class="ck_ev" data-checkbox="icheckbox_square-grey" <?php echo $data_toss->status=='certified'?'checked="checked"':''; ?>>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nama PIC Sertifikasi</label>
                                    <div class="col-md-4">
                                      <input id="dealer" placeholder="Nama PIC sertifikasi" name="sertifikasi_nama_pic" type="text" value="<?php echo $data_toss->sertifikasi_nama_pic; ?>" class="frm_ name form-control" style="margin-left: 15px"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Tgl. Sertifikasi</label>
                                    <div class="col-md-4">
                                      <input id="dealer" name="sertifikasi_tgl" placeholder="Tanggal Sertifikasi" type="text" value="<?php echo !empty($data_toss->sertifikasi_tgl)?date('d M Y',strtotime($data_toss->sertifikasi_tgl)):''; ?>" class="frm_ name form-control date-picker" style="margin-left: 15px"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Attach. Sertifikasi</label>
                                    <div class="col-md-4">
                                      <input type="file" id="attach" name="attach" class="form-control" style="margin-left: 15px" />
                                      <a href="<?php echo base_url('upload/toss_attachments/').$data_toss->sertifikasi_attachment_file_name; ?>"><?php echo $data_toss->sertifikasi_attachment_file_name; ?></a>
                                    </div>
                                </div>
                              
                              <hr style="margin-left: 50px; margin-right: 50px">
                          </div>
                  </div>


              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">A. TOSS Profile</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">

                          <!-- <form action="javascript:;" id="formPhotoOutlet" name="formPhotoOutlet" method="post" enctype="multipart/form-data"> 
                          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        -->
                            <div class="form-body">
                            <div class="form-group">
                              <div class="col-sm-2">
                                <label for="imgInput2" class="control-label pull-right">Outlet</label>
                              </div>
                              <div class="col-sm-8">
                                <div id="formHeader2" runat="server" class="col-xs-4">
                                    <!-- <a href="javascript:;" &apos;);"> -->
                                      <img class="img-thumbnail img_outlet" onclick="imagePreviewDialog('<?php echo !empty($data_toss->outlet_photo_file_name_64) ? $data_toss->outlet_photo_file_name_64:base_url('upload/default.jpg'); ?>')" src="<?php echo !empty($data_toss->outlet_photo_file_name_64) ? $data_toss->outlet_photo_file_name_64:base_url('upload/default.jpg'); ?>" alt="your image" width="200" height="130">
                                    <!-- </a> -->
                                    <center style="padding-top: 5px;"><a onclick="$(this).parent().parent().find('.upload_new input').click()"><div class="btn btn-success btn-xs">Ubah Foto</div></a></center>
                                    <div class="upload_new" img_key='' img_data="">
                                        <input type="file"  style="display: none;" onchange="ch_imgTmp(this,'toss_database')">
                                    </div>
                                </div>
                              </div>
                            </div>
                            </div>
                            <input type="submit" id="submitPhotoOutlet" name="" style="display: none">
                          <!-- </form>   -->
                          <div class="form-body">

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Sertifikasi</label>
                                  <div class="col-md-3" style="padding-right: 40px">
                                      <select id="sertifikasi" name="sertifikasi" style="margin-left: 15px" class="frm_ form-control" required disabled="disabled">
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="sertifikasi" <?php echo $data_toss->sertifikasi=='sertifikasi'?'selected="selected"':''; ?>>Sertifikasi</option>
                                            <option value="resertifikasi" <?php echo $data_toss->sertifikasi=='resertifikasi'?'selected="selected"':''; ?>>Re-sertifikasi</option>
                                      </select>
                                  </div>
                              </div>
                           
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Nama TOSS</label>
                                  <div class="col-md-4">
                                      <input id="nama_toss" name="nama_toss" type="text" value="<?=html_escape($data_toss->nama_toss)?>" placeholder="please fill nama TOSS" class="name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Dealer</label>
                                  <div class="col-md-4">
                                      <input id="dealer" name="dealer" type="text" value="<?=html_escape($data_toss->dealer)?>" placeholder="please fill dealer" class="name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Cabang Induk</label>
                                  <div class="col-md-4">
                                      <input id="cabang_induk" name="cabang_induk" type="text" value="<?=html_escape($data_toss->cabang_induk)?>" placeholder="please fill cabang induk" class="name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Tipe Bangunan</label>
                                  <div class="col-md-3" style="padding-right: 40px">
                                      <select id="tipe_bangunan" name="tipe_bangunan" style="margin-left: 15px" class="form-control" required disabled="disabled">
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="Ruko" <?php echo $data_toss->tipe_bangunan=='Ruko'?'selected="selected"':''; ?>>Ruko</option>
                                            <option value="Non-Ruko" <?php echo $data_toss->tipe_bangunan=='Non-Ruko'?'selected="selected"':''; ?>>Non-Ruko</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-6 control-label">Status Bangunan</label>
                                  <div class="col-md-6">
                                      <select name="status_bangunan" id="status_bangunan" style="margin-left: 15px; min-width: 150px" class="form-control" required disabled="disabled">
                                          <option selected="true" disabled="disabled">- please select -</option>
                                          <option value="Sewa" <?php echo $data_toss->status_bangunan=='Sewa'?'selected="selected"':''; ?>>Sewa</option>
                                          <option value="Milik Pribadi" <?php echo $data_toss->status_bangunan=='Milik Pribadi'?'selected="selected"':''; ?>>Milik Pribadi</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-2 control-label">&nbsp;Dari</label>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline input-medium" type="text" value="<?php echo $data_toss->tgl_sewa_start>'0000-00-00 00:00:00'?f_date($data_toss->tgl_sewa_start):''; ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-4 control-label">Sampai</label>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline input-medium" type="text" value="<?php echo $data_toss->tgl_sewa_end>'0000-00-00 00:00:00'?f_date($data_toss->tgl_sewa_end):''; ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>
                              <div class="row"></div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Alamat</label>
                                  <div class="col-md-5">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                          <textarea id="alamat" name="alamat" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->alamat)?>" ><?=html_escape($data_toss->alamat)?></textarea>
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Provinsi</label>
                                  <div class="col-md-5">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                          <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->provinsi_name)?>" />
                                        </div>
                                  </div>
                              </div>


                              <div class="form-group">
                                  <label class="col-md-2 control-label">Kota/Kabupaten</label>
                                  <div class="col-md-5">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                          <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->kota_kabupaten_name)?>" />
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Kecamatan</label>
                                  <div class="col-md-5">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                          <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->kecamatan_name)?>" />
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Latitude</label>
                                  <div class="col-md-4">
                                    <div style="margin-left: 15px; margin-right: 75px">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->latitude)?>" />
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Longitude</label>
                                  <div class="col-md-4">
                                    <div style="margin-left: 15px; margin-right: 75px">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->longitude)?>" />
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Telp/Fax.</label>
                                  <div class="col-md-4">
                                    <div style="margin-left: 15px; margin-right: 75px">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->telp_fax)?>" />
                                    </div>
                                  </div>
                              </div>
                              <hr>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">PIC TOSS</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nama</label>
                                    <div class="col-md-4">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->pic_nama)?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jabatan</label>
                                    <div class="col-md-4">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->pic_jabatan)?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">No Hp.</label>
                                    <div class="col-md-4">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->pic_no_hp)?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email</label>
                                    <div class="col-md-4">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                        <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" value="<?=html_escape($data_toss->pic_email)?>" />
                                        </div>
                                    </div>
                                </div>
                              <hr style="margin-left: 50px; margin-right: 50px">
                        </div>
                  </div>


              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">B. Kapasitas Bengkel</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                       
                          <div class="form-body">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Jumlah Stall</h4>
                                    </div>
                                </div>
                                <hr>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Stall GR</label>
                                  <div class="col-md-4">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($data_toss->jumlah_stall_stall_gr)?>" />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label"><b>Stall Lain</b></label>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">THS Motor</label>
                                  <div class="col-md-4">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($data_toss->jumlah_stall_ths_motor)?>" />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">THS Mobil</label>
                                  <div class="col-md-4">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($data_toss->jumlah_stall_ths_mobil)?>" />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Lain-lain</label>
                                  <div class="col-md-4">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($data_toss->jumlah_stall_lain_lain)?>" />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Keterangan</label>
                                  <div class="col-md-4">
                                      <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($data_toss->jumlah_stall_keterangan)?>" />
                                  </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Jumlah Manpower</h4>
                                    </div>
                                </div>
                                <hr>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Pro Tech</label>
                            <div class="col-md-4">
                                <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($getCountManPower[0]->pro_tech)?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Tech</label>
                            <div class="col-md-4">
                                <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($getCountManPower[0]->tech)?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Admin</label>
                            <div class="col-md-4">
                                <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($getCountManPower[0]->admin)?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Other</label>
                            <div class="col-md-4">
                                <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($getCountManPower[0]->other)?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-4">
                                <input id="sewa_dari" name="sewa_dari" class="form-control form-control-inline" size="16" type="text" style="margin-left: 15px" value="<?=html_escape($getCountManPower[0]->total)?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Catatan</label>
                            <div class="col-md-4">
                                <textarea id="catatan" name="catatan" type="text" class="name form-control" style="margin-left: 15px"  /><?=html_escape($data_toss->jumlah_manpower_catatan)?></textarea>
                            </div>
                        </div>
                        </div>
                  </div>


              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">C. Target dan Investasi</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                            
                          <div class="form-body">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <h4 style="margin-left: 15px">C1.Target</h4>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                
                                <div class="col-md-6">
                                    <table border="1" width="100%" class="text-center" style="margin-left: 15px">
                                        <thead>
                                            <tr>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Item</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 1</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 2</th>
                                                <th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="padding: 3px">UE/Bulan</td>
                                                <td><input style="text-align: center;" type="text" name="y_awal" class="form-control" value="<?php echo $data_toss->target_ue_bulan_year==0?'-':$data_toss->target_ue_bulan_year; ?>" ></td>
                                                <td><input style="text-align: center;" type="text" name="y_plus1" class="form-control" value="<?php echo $data_toss->target_ue_bulan_year_plus_1==0?'-':$data_toss->target_ue_bulan_year_plus_1; ?>" ></td>
                                                <td><input style="text-align: center;" type="text" name="y_plus2" class="form-control" value="<?php echo $data_toss->target_ue_bulan_year_plus_2==0?'-':$data_toss->target_ue_bulan_year_plus_2; ?>" ></td>
                                                <td><input style="text-align: center;" type="text" name="y_plus3" class="form-control" value="<?php echo $data_toss->target_ue_bulan_year_plus_3==0?'-':$data_toss->target_ue_bulan_year_plus_3; ?>" ></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 3px">UE/Hari</td>
                                                <td><?php echo $data_toss->target_ue_hari_year==0?'-':$data_toss->target_ue_hari_year; ?></td>
                                                <td><?php echo $data_toss->target_ue_hari_year_plus_1==0?'-':$data_toss->target_ue_hari_year_plus_1; ?></td>
                                                <td><?php echo $data_toss->target_ue_hari_year_plus_2==0?'-':$data_toss->target_ue_hari_year_plus_2; ?></td>
                                                <td><?php echo $data_toss->target_ue_hari_year_plus_3==0?'-':$data_toss->target_ue_hari_year_plus_3; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <b style="margin-left: 15px">Note :</b>
                                    <br>
                                    <p style="margin-left: 15px">Y = Tahun sertifikasi | 1 Bulan = 22.5 hari</p>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <h4 style="margin-left: 15px">C2. Investasi</h4>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                
                                <div class="col-md-6">
                                    <table border="1" width="100%" style="margin-left: 15px">
                                        <tbody>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Pembelian lahan *</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="<?php echo $data_toss->investasi_pembelian_lahan==0?'-':toIDR($data_toss->investasi_pembelian_lahan); ?>" class="form-control" > </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Sewa Bangunan</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="<?php echo $data_toss->investasi_sewa_bangunan==0?'-':toIDR($data_toss->investasi_sewa_bangunan); ?>" class="form-control" > </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Renovasi Bangunan</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="<?php echo $data_toss->investasi_renovasi_bangunan==0?'-':toIDR($data_toss->investasi_renovasi_bangunan); ?>" class="form-control" > </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Corporate Identity</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="<?php echo $data_toss->investasi_corporate_identity==0?'-':toIDR($data_toss->investasi_corporate_identity); ?>" class="form-control" > </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px" width="30%">Equipment</td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="padding: 5px"> : IDR </td>
                                                            <td> <input style="text-align: right; border: none" type="text" name="y_awal" value="<?php echo $data_toss->investasi_equipment==0?'-':toIDR($data_toss->investasi_equipment); ?>" class="form-control" > </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" style="padding: 5px; background-color: grey; color: white"><b>TOTAL</b></td>
                                                <td width="30%">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="background-color: grey; color: white; padding: 5px"> : IDR </td>
                                                            <td> <input ="" style="text-align: right; border: none; background-color: grey; color: white" type="text" name="y_awal" value="<?php echo $data_toss->investasi_total==0?'-':toIDR($data_toss->investasi_total); ?>" class="form-control"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <b style="margin-left: 15px">Note :</b>
                                    <br>
                                    <p style="margin-left: 15px">* Apabila milik sendiri</p>
                                </div>
                            </div>

                          </div>

                  </div>
              </div>

              <div class="portlet light ">
                  <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-doc font-dark"></i>
                            <span class="caption-subject bold">D. Checklist Sertifikasi (Area)</span>
                            <!-- <a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" > Add New Area </a> -->
                        </div>
                     
                        <div class="actions pull-left" style="padding-left: 30px">
                            <div class="btn-group">
                                
                            </div>
                        </div>
                    
                  </div>
                  <div class="portlet-body form">
                       
                    <div class="form-body">
                        <div id="__area" class="form-body">
                            <?php if(!empty($getDataItems)){ ?>
                                <?php $no=1; foreach ($getDataArea as $key => $value) { ?>
                                    <div class="area_" id_area="<?php echo $value->id_area; ?>" status="from_server">
                                        <div class="form-group form-control-inline">
                                            <div class="col-md-12">
                                                <table>
                                                    <tr>
                                                        <td><h4 style="margin-left:15px;padding-right: 30px;">D<?php echo $no;  ?><span class="num_area"></span>. <?=html_escape($value->area_name)?></h4></td>
                                                        <!-- <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addSubAreaDialog(<?php echo $value->id_area; ?>,$(this));"> Add Sub Area </a></td> -->
                                                        <!-- <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_area',null,null,$(this));"> Delete </a></td> -->
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- SUB AREA -->
                                        <?php $no_sub=1; foreach ($getDataSub as $key_sub => $value_sub) { ?>
                                          <?php if($value_sub->id_area==$value->id_area){ ?>
                                              <div class="sub_area_" id_sub="<?php echo $value_sub->id_sub_area; ?>" status="from_server">
                                                  <div class="form-group">
                                                    <div class="col-md-12">
                                                        <table>
                                                            <tr>
                                                                <td><h5 style="margin-left:15px;padding-right: 30px"><span class="num_sub-`+area+`"><?php echo $no_sub; ?></span>.<?=html_escape($value_sub->sub_area_name)?></h5></td>
                                                                <!-- <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('sub_area',<?php echo $value->id_area; ?>,<?php echo $value_sub->id_sub_area ?>,$(this));"> Add Item </a></td> -->
                                                                <!-- <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_sub_area',null,null,$(this));"> Delete </a></td> -->
                                                            </tr>
                                                        </table>
                                                    </div>
                                                  </div>
                                                  <hr>

                                                  <div style="margin-left:15px;" status="form-group col-sm-12">
                                                      <table class="table table-striped table-hover tb_sub_item">
                                                        <thead>
                                                          <tr>
                                                              <th>Kriteria Minimum</th>
                                                              <th style="width: 5%">Jumlah</th>
                                                              <th><center>Foto Contoh</center></th>
                                                              <th><center>Foto Dealer</center></th>
                                                              <th>Evaluasi Dealer</th>
                                                              <th>Evaluasi TAM</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          <!-- ITEMS -->
                                                          <?php foreach ($getDataItems as $key_item => $value_item) { ?>
                                                            <?php if(($value_item->id_area==$value->id_area) &&($value_item->id_sub_area==$value_sub->id_sub_area)){ ?>
                                                              <tr class="items__" id_item="<?php echo $value_item->id_item; ?>" id_area="<?php echo $value_item->id_area; ?>" id_sub="<?php echo $value_item->id_sub_area; ?>" status="from_server">
                                                                  <td><?php echo $value_item->kriteria_minimum;?></td>
                                                                  <td><?=html_escape($value_item->jumlah)?></td>
                                                                  <td>
                                                                      <center>
                                                                          <a href="javascript:;"><img src="<?php echo $value_item->foto_contoh_file_name_64; ?>" class="img-thumbnail token-`+t_img+`" onclick="imagePreviewDialog('<?php echo $value_item->foto_contoh_file_name_64;  ?>')" style="max-width: 100px"></a><br>
                                                                          <a href="<?php echo base_url('master_data_toss/toss_database/histori_foto/').$value_item->img_key.'/'.'area'; ?>"><div class="btn btn-primary btn-xs">Histori</div></a>
                                                                      </center>
                                                                  </td>
                                                                  <td>
                                                                      <center>
                                                                          <img class="img-thumbnail img_area" img_key='<?php echo $value_item->img_key; ?>' is_change=0 src="<?php echo !empty($value_item->foto_dealer_file_name_64)?$value_item->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>" style="max-width: 100px" onclick="imagePreviewDialog('<?php echo !empty($value_item->foto_dealer_file_name_64)?$value_item->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>');">
                                                                          
                                                                          <br>
                                                                          <a onclick="$(this).parent().find('.upload_new input').click()"><div class="btn btn-success btn-xs">Ubah Foto</div></a>
                                                                          <div class="upload_new" img_key='<?php echo $value_item->img_key; ?>' img_data="">
                                                                              <input type="file"  style="display: none;" onchange="ch_imgTmp(this,'toss_database')">
                                                                          </div>
                                                                      </center>
                                                                  </td>
                                                                  <td>
                                                                      <input type="checkbox" disabled="disabled" <?php echo $value_item->score_evaluasi_dealer==1?'checked="checked"':''; ?>>
                                                                  </td>
                                                                  <td>
                                                                      <input type="checkbox" dealer="<?php echo $value_item->score_evaluasi_dealer; ?>" tb="tbl_toss_database_cs_area" tb_fe="tbl_toss_front_end_cs_area" name="score_evaluasi_tam" data_id="<?php echo $value_item->id_item; ?>" class="icheck ck_ev" data-checkbox="icheckbox_square-grey" <?php echo $value_item->score_evaluasi_tam==1?'checked="checked"':''; ?>>
                                                                  </td>
                                                              </tr>
                                                            <?php } ?>
                                                          <?php } ?>
                                                          <!-- END ITEMS -->
                                                        </tbody>
                                                      </table>
                                                  </div>
                                              </div>
                                          <?php $no_sub++; } ?>
                                        <?php } ?>
                                        <!-- END SUB AREA -->
                                        <hr>
                                    </div>
                                <?php $no++; } ?>
                            <?php } ?>
                        </div>

                        <div class="__manpower">
                            <div class="form-group form-control-inline">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td><h4 style="margin-left: 15px; padding-right: 30px">Manpower</h4></td>
                                         
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td><h5 style="margin-left: 15px; padding-right: 30px">A. Klasifikasi SDM After Sales</h5></td>
                                           
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table border="1" width="100%" class="text-center" style="margin-left: 15px">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">No.</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Nama Karyawan</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Tgl Masuk</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">No. KTP</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Jabatan</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" colspan="10">Sertifikasi</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Keterangan</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Evaluasi Dealer</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="4">Evaluasi TAM</th>
                                            </tr>
                                            <tr>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" colspan="7">Teknisi</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" rowspan="2" colspan="3">SA GR</th>
                                            </tr>
                                            <tr>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" colspan="2">Level 1</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" colspan="3">Level 2</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;" colspan="2">Level 3</th>
                                            </tr>
                                            <tr>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">TT</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">PT</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">DTG</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">DTL</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">DTC</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">DMT</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">LD</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">L1</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">L2</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2; font-size: x-small;">L3</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($ls_manpower_sdm_after_sales)){ ?>
                                                <?php $no_man_as=1; ?>
                                                <?php foreach ($ls_manpower_sdm_after_sales as $key => $value) { ?>
                                                    <tr>
                                                        <td style="font-size: x-small;" width="5%"><?php echo $no_man_as;  ?></td>
                                                        <td style="font-size: x-small;" width="15%"><?=html_escape($value->nama_karyawan)?></td>
                                                        <td style="font-size: x-small;" width="10%"><?php echo date("d M Y",strtotime($value->tgl_masuk)); ?></td>
                                                        <td style="font-size: x-small;" width="15%"><?=html_escape($value->no_ktp)?></td>
                                                        <td style="font-size: x-small;" width="15%"><?php echo $value->jabatan=='pro_tech'?'Pro Tech':ucfirst($value->jabatan); ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->jabatan=='tech'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='PT'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='DTG'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='DTL'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='DTC'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='DMT'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='LD'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='L1'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='L2'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="2%"><?php echo $value->level_training=='L3'?'V':''; ?></td>
                                                        <td style="font-size: x-small;" width="15%"><?php echo $value->keterangan; ?></td>
                                                        <td><input type="checkbox" <?php echo $value->score_evaluasi_dealer==1?'checked="checked"':''; ?> disabled="disabled"></td>
                                                        <td><input type="checkbox" dealer="<?php echo $value->score_evaluasi_dealer; ?>" data-checkbox="icheckbox_square-grey" tb="tbl_toss_database_cs_manpower" tb_fe="tbl_toss_front_end_cs_manpower" name="score_evaluasi_tam" data_id="<?php echo $value->id; ?>" class="icheck ck_ev" <?php echo $value->score_evaluasi_tam==1?'checked="checked"':''; ?>></td>
                                                    </tr>
                                                <?php $no_man_as++; } ?>
                                            <?php }else{ ?>
                                                      <tr>
                                                        <td colspan="18">Data tidak ditemukan.</td>
                                                      </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table><br>
                                    <p style="margin-left: 15px">Keterangan : </p>
                                    <div class="col-md-4">
                                        <table style="width: 500px; margin-left: 15px">
                                            <thead>
                                                <tr><td colspan="2"><b>1. TRAINING TEKNISI<br><br></b></td></tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>TT </td>
                                                    <td>: Toyota Technician</td>
                                                </tr>
                                                <tr>
                                                    <td>PT </td>
                                                    <td>: Pro Technician</td>
                                                </tr>
                                                <tr>
                                                    <td>DT </td>
                                                    <td>: Diagnostic Technician</td>
                                                </tr>
                                                <tr>
                                                    <td>DTG </td>
                                                    <td>: Diagnostic Technician Engine</td>
                                                </tr>
                                                <tr>
                                                    <td>DTL </td>
                                                    <td>: Diagnostic Technician Electrica</td>
                                                </tr>
                                                <tr>
                                                    <td>DTC </td>
                                                    <td>: Diagnostic Technician Chassis</td>
                                                </tr>
                                                <tr>
                                                    <td>DMT </td>
                                                    <td>: Diagnosis Master Technician</td>
                                                </tr>
                                                <tr>
                                                    <td>LD </td>
                                                    <td>: Latest Diagnosis</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-5">
                                        <table style="width: 500px; margin-left: 15px">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">2. SERVICE ADVISOR GR<br><br></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>L1 </td>
                                                    <td>: Level 1 TS A21</td>
                                                </tr>
                                                <tr>
                                                    <td>L2 </td>
                                                    <td>: Level 2 TS A21</td>
                                                </tr>
                                                <tr>
                                                    <td>L3 </td>
                                                    <td>: Level 3 TS A21</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td><h5 style="margin-left: 15px; padding-right: 30px">B. Klasifikasi SDM Umum</h5></td>
                                           
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table border="1" width="100%" class="text-center" style="margin-left: 15px; width: 700px">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2">No.</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2">Nama Karyawan</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2">Tgl Masuk</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2">Jabatan</th>
                                                <th style="text-align: center; padding: 5px; background-color: #f2f2f2">Keterangan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($ls_manpower_sdm_umum)){ ?>
                                                <?php $no_man_sdm=1; ?>
                                                <?php foreach ($ls_manpower_sdm_umum as $key => $value) { ?>
                                                    <tr>
                                                        <td style="padding: 5px" width="5%"><?php echo $no_man_sdm; ?></td>
                                                        <td style="padding: 5px" width="15%"><?=html_escape($value->nama_karyawan)?></td>
                                                        <td style="padding: 5px" width="10%"><?php echo date("d-m-Y",strtotime($value->tgl_masuk)); ?></td>
                                                        <td style="padding: 5px" width="15%"><?php echo ucfirst($value->jabatan); ?></td>
                                                        <td style="padding: 5px" width="15%"><?=html_escape($value->keterangan)?></td>
                                                    </tr>
                                                <?php $no_man_sdm++; } ?>
                                            <?php }else{ ?>
                                                      <tr>
                                                        <td colspan="5">Data tidak ditemukan</td>
                                                      </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                        </div>

                        <div id="__equipment" class="form-body">
                            <!-- EQUIMENT -->
                            <div class="form-group form-control-inline">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <td><h4 style="margin-left: 15px; padding-right: 30px">Equipment</h4></td>
                                            <!-- <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addEquipmentGroupDialog();"> Add Group </a></td> -->
                                        </tr>
                                    </table>
                                    <?php if(!empty($getDataEQGroup)){ ?>
                                        <?php foreach ($getDataEQGroup as $key => $value) { ?>
                                            <div class="equipment_" key="<?php echo $value->group_key; ?>" >
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <h5 style="margin-left: 15px; padding-right: 30px"><?=html_escape($value->group_name)?></h5>
                                                                </td>
                                                                <!-- <td>
                                                                    <a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('equipment','','',$(this),<?php echo $value->group_key; ?>,'<?php echo $value->group_name; ?>');"> Add Item </a>
                                                                </td>
                                                                <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_group',null,null,$(this));"> Delete </a></td>-->
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <hr>

                                                <div  style="margin-left:15px;">
                                                    <table class="table table-striped table-hover tb_group_item">
                                                        <thead>
                                                            <tr>
                                                                <th>Kriteria Minimum</th>
                                                                <th style="width: 5%">Jumlah</th>
                                                                <th><center>Foto Contoh</center></th>
                                                                <th><center>Foto Dealer</center></th>
                                                                <th>Keterangan</th>
                                                                <th>Evaluasi Dealer</th>
                                                                <th>Evaluasi TAM</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($getDataEQItems as $k_eq_items => $v_eq_items) { ?>
                                                                <?php if($v_eq_items->group_key==$value->group_key){ ?>
                                                                    <tr key="<?php echo $v_eq_items->group_key; ?>" group_name="<?php echo $v_eq_items->group_name; ?>" class="eq_items__" id_eq_item="<?php echo $v_eq_items->id; ?>" status="from_server">
                                                                        <td><?=html_escape($v_eq_items->kriteria_minimum)?>&nbsp;<div class="btn btn-primary btn-xs" onclick="$(this).parent().parent().find('input.eq_handle').click()">Tambah Foto</div></td>
                                                                        <td><?=html_escape($v_eq_items->jumlah_minimum)?></td>
                                                                        <td>
                                                                          <center>
                                                                            <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" src="<?php echo $v_eq_items->foto_contoh_file_name_64; ?>" onclick="imagePreviewDialog('<?php echo $v_eq_items->foto_contoh_file_name_64; ?>')" style="max-width: 100px"></a><br>
                                                                            <a href="<?php echo base_url('master_data_toss/toss_database/histori_foto/').$v_eq_items->item_key.'/'.'equipment'; ?>"><div class="btn btn-primary btn-xs">Histori Foto</div></a>
                                                                          </center>
                                                                        </td>
                                                                        <td>
                                                                          <center>
                                                                            <div class="add_image" id_eq_item="<?php echo $v_eq_items->id; ?>" group_key="<?php echo $v_eq_items->group_key; ?>" item_key="<?php echo $v_eq_items->item_key; ?>">
                                                                                
                                                                                <input type="file" class="eq_handle" style="display: none;" onchange="ch_imgTmp(this,'upload_eq')">
                                                                            </div>
                                                                            <?php if(!empty($v_eq_items->img)){ ?>

                                                                                <?php foreach ($v_eq_items->img as $key_img => $value_img) { ?>
                                                                                    <div><img class="img-thumbnail" img_key='<?php echo $value_img->img_key; ?>' src="<?php echo !empty($value_img->foto_dealer_file_name_64)?$value_img->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>" style="max-width: 100px" onclick="imagePreviewDialog('<?php echo !empty($value_img->foto_dealer_file_name_64)?$value_img->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>');"><br><div class="btn btn-danger btn-xs" onclick="del_img_eq('<?php echo $value_img->img_key; ?>','<?php echo $value_img->item_key; ?>',$(this))">Hapus Foto</div></div><br>
                                                                                    <!-- <a href="javascript:;" onclick="imagePreviewDialog('<?php echo !empty($value_img->foto_dealer_file_name_64)?$value_img->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>');"><img class="img-thumbnail" src="<?php echo !empty($value_img->foto_dealer_file_name_64)?$value_img->foto_dealer_file_name_64:base_url("upload/default.jpg"); ?>" style="max-width: 100px"></a><br><br> -->
                                                                                <?php } ?>
                                                                            <?php }else{ ?>
                                                                                    <a class="no_image" href="javascript:;" onclick="imagePreviewDialog('<?php echo base_url("upload/default.jpg"); ?>');"><img class="img-thumbnail" src="<?php echo base_url("upload/default.jpg"); ?>" style="max-width: 100px"></a>
                                                                            <?php } ?>
                                                                          </center>
                                                                            <!-- <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/toss_equipment/3.png'); ?>&apos;);"><img class="img-thumbnail" src="<?php echo base_url('upload/toss_equipment/3.png'); ?>" style="max-width: 100px"></a><br><br>
                                                                            <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/toss_equipment/3.png'); ?>&apos;);"><img class="img-thumbnail" src="<?php echo base_url('upload/toss_equipment/3.png'); ?>" style="max-width: 100px"></a><br><br>
                                                                            <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/toss_equipment/3.png'); ?>&apos;);"><img class="img-thumbnail" src="<?php echo base_url('upload/toss_equipment/3.png'); ?>" style="max-width: 100px"></a><br><br>
                                                                            <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/toss_equipment/3.png'); ?>&apos;);"><img class="img-thumbnail" src="<?php echo base_url('upload/toss_equipment/3.png'); ?>" style="max-width: 100px"></a><br><br> -->
                                                                        </td>
                                                                        <td><?=html_escape($v_eq_items->keterangan)?></td>
                                                                        <td><input type="checkbox" disabled="disabled" <?php echo $v_eq_items->score_evaluasi_dealer==1?'checked="checked"':''; ?>></td>
                                                                        <td><input type="checkbox" dealer="<?php echo $v_eq_items->score_evaluasi_dealer; ?>" class="icheck ck_ev" data-checkbox="icheckbox_square-grey" tb="tbl_toss_database_cs_equipment" tb_fe="tbl_toss_front_end_cs_equipment" name="score_evaluasi_tam" data_id="<?php echo $v_eq_items->id; ?>" <?php echo $v_eq_items->score_evaluasi_tam==1?'checked="checked"':''; ?>></td>
                                                                    </tr>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>                                  
                                </div>
                            </div>
                            <!-- <hr> -->
                            
                            <!-- /////////////////////////////////////////// -->
                        </div>



                          
                    </div>
                  </div>


              </div>

              <div class="form-group">
                  <div class="col-md-12" style="padding-left: 45px">
                    <button type="button" id="btnSave" onclick="SUBMIT_toss_database(<?php echo $data_toss->id; ?>,$(this))" class="btn btn-lg green">Save</button>
                    <!-- <button type="button" id="btnSave" onclick="$('#submitPhotoKpd').click();" class="btn green">Save</button> -->
                    <!-- <button type="button" id="btnSave" onclick="$('#submitPhotoOutlet').click();" class="btn green">Save</button> -->
                    <a id="btnCancel" href="<?php echo base_url('master_data_toss/toss_database/view'); ?>" class="btn btn-lg btn-danger">Cancel</a>
                    <a id="btnDelete" href="javascript:;" onclick="delete_toss(<?php echo $data_toss->id; ?>)" class="btn btn-lg btn-default">Delete this TOSS</a>
                  </div>
              </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

  <script type="text/javascript">
      var cert = "<?php echo $data_toss->status; ?>";

      if(cert=='certified'){
          var ck_ev_cer = document.getElementsByClassName('icheck ck_ev');
          Array.prototype.forEach.call(ck_ev_cer, function(el,index) {
              ck_ev_cer[index].setAttribute('disabled','disabled');
          });

          
      }
  </script>

<?php } if ($sub_menu == 'toss_database' && $mode=='add') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Add New Data
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Database</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Add New Data</a>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <form action="javascript:;" id="form_add" class="form-horizontal" role="form">
              
                <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold">Form add TOSS</span>
                      </div>
                      <div class="tools">
                        <input type="text" value="<?php echo $sub_menu; ?>" id="sub_menu_active" style="display: none">
                        <input type="text" value="<?php echo $mode; ?>" id="mode_active" style="display: none">
                      </div>
                  </div>
                  <div class="portlet-body form">

                          <!-- <form action="javascript:;" id="formPhotoOutlet" name="formPhotoOutlet" method="post" enctype="multipart/form-data"> 
                          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        -->
                            <div class="form-body">
                            <div class="form-group">
                              <div class="col-sm-2">
                                <label for="imgInput2" class="control-label pull-right">Outlet*</label>
                              </div>
                              <div class="col-sm-8">
                                <div id="formHeader2" runat="server" class="col-xs-4">
                                    <a onclick="$('#imgInput2').click();"><img id="imgHdr2" class="img-thumbnail" src="<?php echo !empty($data_project->outlet_pic_file_path) ? $data_project->outlet_pic_file_path : base_url('upload/outlet/default.jpg');?>" alt="your image" width="200" height="130"/></a>                                            
                                    <input type="text" name="imgInput2Path" value="" style="display: none">
                                    <input type='file' id="imgInput2" name="imgInput2" accept=".png, .jpg, .jpeg" style="display: none"/>
                                    <input type="text" name="outlet_photo_file_name_64"  class="frm_" value="" style="display: none">
                                </div>
                              </div>
                            </div>
                            </div>
                            <input type="submit" id="submitPhotoOutlet" name="" style="display: none">
                          <!-- </form>   -->
                          <div class="form-body">
                              
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Sertifikasi</label>
                                  <div class="col-md-3" style="padding-right: 40px">
                                      <select id="sertifikasi" name="sertifikasi" style="margin-left: 15px" class="frm_ form-control" required>
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="sertifikasi" >Sertifikasi</option>
                                            <option value="resertifikasi" >Re-sertifikasi</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Nama TOSS*</label>
                                  <div class="col-md-4">
                                      <input id="nama_toss" name="nama_toss" type="text" value="<?=(isset($data_toss)?$data_toss->nama_toss:'')?>" placeholder="please fill nama TOSS" class="frm_ name form-control" style="margin-left: 15px" required />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Dealer*</label>
                                  <div class="col-md-5" style="margin-left: 15px;">
                                      <select class="select2 frm_ form-control" name="dealer" placeholder="Please fill Nama Dealer">
                                            <option disabled="disabled" selected="selected">-- Pilih Dealer --</option>
                                            <?php if(!empty($ls_dealer)){ ?>
                                                <?php foreach ($ls_dealer as $key => $value) { ?>
                                                    <option value="<?php echo $value->dealer_name; ?>"><?=html_escape($value->dealer_name)?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Cabang Induk*</label>
                                  <div class="col-md-5" style="margin-left: 15px;">
                                        <select class="select2 frm_ form-control" name="cabang_induk" placeholder="Please fill Cabang Induk" >
                                            <option disabled="disabled" selected="selected">-- Pilih Cabang Induk --</option>
                                        </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Tipe Bangunan</label>
                                  <div class="col-md-3" style="padding-right: 40px">
                                      <select id="tipe_bangunan" name="tipe_bangunan" style="margin-left: 15px" class="frm_ form-control">
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="Ruko" <?php if(!empty($data_toss) && $data_toss->tipe_bangunan == 'Ruko'){echo 'selected';}; ?> >Ruko</option>
                                            <option value="Non-Ruko" <?php if(!empty($data_toss) && $data_toss->tipe_bangunan == 'Non-Ruko'){echo 'selected';}; ?> >Non-Ruko</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-6 control-label">Status Bangunan</label>
                                  <div class="col-md-6">
                                      <select name="status_bangunan" id="status_bangunan" style="margin-left: 15px; min-width: 150px" class="frm_ form-control" >
                                          <option selected="true" disabled="disabled">- please select -</option>
                                          <option value="Sewa" <?php if(!empty($data_toss) && $data_toss->status_bangunan == 'Sewa'){echo 'selected';}; ?> >Sewa</option>
                                          <option value="Milik Pribadi" <?php if(!empty($data_toss) && $data_toss->status_bangunan == 'Renovation'){echo 'selected';}; ?> >Milik Pribadi</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-2 control-label">Dari</label>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                        <input id="sewa_dari" name="tgl_sewa_start" placeholder="please choose date" class="frm_ form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_toss)){echo date('d M Y', strtotime($data_toss->sewa_dari));}; ?>" />

                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>
                              <div class="form-group col-md-4" style="padding-left: 25px">
                                  <label class="col-md-4 control-label">Sampai</label>
                                  <div class="col-md-4">
                                    <div class="input-group">
                                        <input id="sewa_sampai" name="tgl_sewa_end" placeholder="please choose date" class="frm_ form-control form-control-inline input-medium date-picker " size="16" type="text" value="<?php if(!empty($data_toss)){echo date('d M Y', strtotime($data_toss->sewa_sampai));}; ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Alamat</label>
                                  <div class="col-md-4">
                                      <textarea id="alamat" name="alamat" type="text" value="" placeholder="please fill alamat" class="frm_ name form-control" style="margin-left: 15px"></textarea>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Provinsi*</label>
                                  <div class="col-md-5" style="margin-left: 15px;">
                                        <!-- <input type="text" class="frm_ form-control" name="provinsi" placeholder="Please fill provinsi" style="margin-left: 15px"> -->
                                        <select class="select2 frm_ form-control" name="provinsi" placeholder="Please fill Provinsi">
                                            <option disabled="disabled" selected="selected">-- Pilih Provinsi --</option>
                                            <?php if(!empty($provinsi)){ ?>
                                                <?php foreach ($provinsi as $key => $value) { ?>
                                                    <option value="<?php echo $value->id; ?>"><?=html_escape($value->nama)?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Kota/Kabupaten*</label>
                                  <div class="col-md-5" style="margin-left: 15px;">
                                        <select class="select2 frm_ form-control" name="kota_kabupaten" placeholder="Please fill Kota/Kabupaten" >
                                            <option disabled="disabled" selected="selected">-- Pilih Kota/Kabupaten --</option>
                                        </select>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Kecamatan*</label>
                                  <div class="col-md-5" style="margin-left: 15px;">
                                        <select class="select2 frm_ form-control" name="kecamatan" placeholder="Please fill Kecamatan" >
                                            <option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>
                                        </select>
                                  </div>
                              </div>


                              <div class="form-group">
                                  <label class="col-md-2 control-label">Telp/Fax.</label>
                                  <div class="col-md-4">
                                      <input id="telp_fax" name="telp_fax" type="text" value="" placeholder="please fill telp/fax" class="frm_ name form-control" style="margin-left: 15px"  />
                                  </div>
                              </div>

                              <hr>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">Latitude & Longitude</h4>
                                    </div>
                                </div>
                              <hr>
                              <center>
                                <div style="z-index: 1">
                                  <input id="pac-input" class="form-control controls" type="text" placeholder="Anything you want, enter your keyword here...">
                                  <br><br>
                                  <div id="map" style="width: 100%;height:400px"></div>
                                </div>
                                <br>
                                <div class="form-group">
                                    
                                    <div class="col-md-3 col-md-offset-3">
                                        <label class="control-label" style="font-size: large;">Latitude</label>
                                        <br><br>
                                        <input id="latitude" name="latitude" type="text" value="" placeholder="please set location" class="frm_ name form-control" style="text-align: center;" required />
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label" style="font-size: large;">Longitude</label>
                                        <br><br>
                                        <input id="longitude" name="longitude" type="text" value="" placeholder="please set location" class="frm_ name form-control" style="text-align: center;" required />
                                    </div>
                                </div>
                              </center>
                              <hr>

                                
                                
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <h4 style="margin-left: 15px">PIC TOSS</h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Nama *</label>
                                    <div class="col-md-4">
                                        <input id="nama" name="pic_nama" type="text" value="" placeholder="please fill nama" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jabatan *</label>
                                    <div class="col-md-4">
                                        <input id="jabatan" name="pic_jabatan" type="text" value="" placeholder="please fill jabatan" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">No Hp. *</label>
                                    <div class="col-md-4">
                                        <input id="no_hp" name="pic_no_hp" type="text" value="" placeholder="please fill No HP." class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email *</label>
                                    <div class="col-md-4">
                                        <input id="email" name="pic_email" type="text" value="" placeholder="please fill email" class="frm_ name form-control" style="margin-left: 15px"  />
                                    </div>
                                </div>
                                <hr>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">User Dealer*</label>
                                    <div class="col-md-4">
                                        <div style="margin-left: 15px; margin-right: 75px">
                                          <select name="id_user_dealer" id="kabupaten" class="frm_ form-control select2" required>
                                            
                                            <option selected="true" disabled="disabled">- Belum ada user yang dipilih -</option>
                                            <?php foreach ($user_dealer as $key => $value) { ?>
                                                    <option value="<?php echo $value->id; ?>"><?=html_escape($value->username)?></option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                    </div>
                                </div>

                              <hr>
                                <div class="form-group">
                                  <div class="col-md-12" style="padding-left: 45px">
                                    <button type="button" id="btnSave" onclick="submit_($('#form_add'),'add')" class="btn btn-lg green" >Save</button>
                                    <a id="btnCancel" href="<?php echo base_url('master_data_toss/toss_database/view'); ?>" class="btn btn-lg btn-danger">Cancel</a>
                                  </div>
                                </div>
                        </div>
                  </div>


                </div>


                
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php }  if ($sub_menu == 'toss_database' && $mode=='histori_foto') { ?>
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Histori Foto
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Database</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Content</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Histori Foto</a>
                  
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <form action="javascript:;" id="form" class="form-horizontal" role="form">
              
                <div class="portlet light ">
                    <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">List Histori Foto</span>
                      </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                                  <tr>
                                      <th class="" width="5%">No.</th>
                                      <th class="">Foto</th>
                                      <th class="">Information</th>
                                  </tr>
                            </thead>
                            <tbody>
                              <?php if($type=='equipment'){ ?>
                                <?php if(!empty($img_list)){ ?>
                                  <?php $no=1; foreach ($img_list as $key => $value) { ?>
                                            <tr>
                                              <td><?php echo $no; ?></td>
                                              <td><img height="100px" src="<?php echo $value->foto_dealer_file_name_64; ?>" onclick="imagePreviewDialog('<?php echo $value->foto_dealer_file_name_64; ?>');"></td>
                                              <td>
                                                  <b>Di upload oleh : </b><b><?=html_escape($value->diupload_oleh)?></b><br>
                                                  <b>Tanggal upload : </b><b><?php echo date("d M Y, H:i:s",strtotime($value->upload_date));?></b><br>
                                              </td>
                                            </tr>
                                  <?php $no++; }   ?>
                                <?php } ?>
                              <?php } ?>

                              <?php if($type=='area'){ ?>
                                <?php if(!empty($img_list)){ ?>
                                  <?php $no=1; foreach ($img_list as $key => $value) { ?>
                                            <tr>
                                              <td><?php echo $no; ?></td>
                                              <td><img height="100px" src="<?php echo $value->foto_64; ?>" onclick="imagePreviewDialog('<?php echo $value->foto_64; ?>');"></td>
                                              <td>
                                                  <b>Di upload oleh : </b><b><?=html_escape($value->diupload_oleh)?></b><br>
                                                  <b>Tanggal upload : </b><b><?php echo date("d M Y, H:i:s",strtotime($value->upload_date));?></b><br>
                                              </td>
                                            </tr>
                                  <?php $no++; }   ?>
                                <?php } ?>
                              <?php } ?>
                            </tbody>
                      </table>
                    </div>

                </div>

            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>
<?php } ?>


<style type="text/css">
  input.target::-webkit-outer-spin-button,
  input.target::-webkit-inner-spin-button {
      /* display: none; <- Crashes Chrome on hover */
      -webkit-appearance: none;
      margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
  }
</style>