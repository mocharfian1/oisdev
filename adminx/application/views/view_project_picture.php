<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 ?>
 
<?php if ($mode == 'view') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Project Picture
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Project Picture</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Sub Area Picture</span>


                      </div>
                      <div class="pull-right">
                          
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="all">No.</th>
                                  <th class="all">Thumbnail</th>
                                  <th class="all">Nama Outlet</th>
                                  <th class="none">Main Dealer</th>
                                  <th class="none">Fungsi Outlet</th>
                                  <th class="none">Type Pembangunan</th>
                                  <th class="all">Alamat</th>
                                  <th class="all">EPM</th>
                                  <th class="all">Area Name</th>
                                  <th class="all">Sub Area Name</th>
                                  <th class="all" style="text-align: center; width: 20px">Act.</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                              $no = 1;
                              foreach ($read_project_area_pic as $row) {
                            ?>
                              <tr>
                                  <td><?php echo $no++; ?></td>
                                  <td><a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/').$row['file_name']; ?>&apos;);"><img src="<?php echo base_url('upload/').$row['file_name']; ?>" style="max-width: 100px"></a></td>
                                  <td><?=html_escape($row['nama_outlet'])?></td>
                                  <td><?=html_escape($row['main_dealer'])?></td>
                                  <td><?=html_escape($row['fungsi_outlet'])?></td>
                                  <td><?=html_escape($row['type_pembangunan'])?></td>
                                  <td><?=html_escape($row['alamat'])?></td>
                                  <td><?=html_escape($row['jenis_epm'])?></td>
                                  <td><?=html_escape($row['area_name'])?></td>
                                  <td><?=html_escape($row['sub_area_name'])?></td>
                                  <td>
                                    <a href="javascript:;" onclick="delete_pic(<?php echo $row['id']; ?>)" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a>
                                  </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Picture/Dokumen</span>


                      </div>
                      <div class="pull-right">
                          
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover " width="100%" id="sample_3">
                          <thead>
                              <tr>
                                  <th class="all">No.</th>
                                  <th class="all">Thumbnail</th>
                                  <th class="all">Nama Outlet</th>
                                  <th class="none">Main Dealer</th>
                                  <th class="none">Fungsi Outlet</th>
                                  <th class="none">Type Pembangunan</th>
                                  <th class="all">Alamat</th>
                                  <th class="all" style="text-align: center; width: 20px">Act.</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                              $no = 1;
                              foreach ($data_project_pic as $row) {
                            ?>
                              <tr>
                                  <td><?php echo $no++; ?></td>
                                  <td>
                                    <?php if($row['type']=='dokumen'):?>
                                    <a href="<?=$row['file_path']?>" target="_blank"><?=$row['file_name']?></a>
                                    <?php else:?>

                                    <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?php echo base_url('upload/').$row['file_name']; ?>&apos;);"><img src="<?php echo base_url('upload/').$row['file_name']; ?>" style="max-width: 100px"></a>
                                  <?php endif;?>
                                  </td>
                                  <td><?php echo $row['nama_outlet']; ?></td>
                                  <td><?php echo $row['main_dealer']; ?></td>
                                  <td><?php echo $row['fungsi_outlet']; ?></td>
                                  <td><?php echo $row['type_pembangunan']; ?></td>
                                  <td><?php echo $row['alamat']; ?></td>
                                  <td>
                                    <a href="javascript:;" onclick="delete_project_pic(<?php echo $row['id']; ?>)" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a>
                                  </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>
