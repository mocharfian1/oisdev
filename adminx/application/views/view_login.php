<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />

        <title>OIS | Admin Login</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="YK Digital" name="author" />
        <link rel="icon" type="image/png" href="<?=base_url('assets/global/img/favicon/')?>favicon-toyota.png" sizes="32x32" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=base_url()?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- offline js -->
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-theme-chrome.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-language-english.css" rel="stylesheet" type="text/css" />
        <!-- holdon js -->
        <link href="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url()?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url()?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?=base_url()?>assets/pages/css/login-2.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <!-- <link rel="shortcut icon" href="favicon.ico" />  -->
        <style type="text/css">
            .login a{
                color: black !important;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class=" login" style="background: url('<?php echo base_url('assets/pages/img/toyota-background.png'); ?>') left top no-repeat;">
        <input type="text" id="isHoldOnOpen" value="false" style="display: none">
        <input type="text" id="base_url" class="base_url" value="<?php echo base_url(''); ?>" style="display: none"/>
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="javascript:;">
                <img src="<?php echo base_url('assets/pages/img/logo-tam.png'); ?>" alt="logo" class="logo-default" style="width:15em; margin-top:21px" />
            </a>
            <br>
            <h3 style="color: white"><b>Log In</b> to your <b>OIS Admin</b> account</h3>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <?php if(!empty($this->session->flashdata('message'))){ ?>
            <div class="alert alert-danger alert-dismissible bg-green-turquoise">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Error!</h4>
              <?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php } ?>
            <!-- BEGIN LOGIN FORM -->

            <form class=" from-horizontal form-material" action="<?php echo base_url('login/validate');?>" method="post">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any email and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" style="background-color: black; border: 1px solid silver" autocomplete="off" placeholder="Email" name="email" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" style="background-color: black; border: 1px solid silver" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <input type="submit" class="btn btn-primary btn-block btn-lg text-uppercase " value="Login" style="color: black; background-color: silver !important" />
                </div>
               
                
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright hide"> 2018 © OIS. </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

        <!-- offline js -->
        <script src="<?=base_url()?>assets/global/plugins/offline-js/offline.min.js" type="text/javascript"></script>
        <!-- holdon js -->
        <script src="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?=base_url()?>assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->

        <script type="text/javascript">
            var burl = $('#base_url').val();
            //offline js with hold on js for network down error handling
            var run = function(){
            
              var req = new XMLHttpRequest();
              req.timeout = 5000;
              req.open('GET', burl, true);
              req.send();

                Offline.on('confirmed-down', function () {
                   
                        if($('#isHoldOnOpen').val() == 'true'){

                        }else if($('#isHoldOnOpen').val() == 'false'){
                            console.log('open holdon');
                            $('#isHoldOnOpen').val('true');
                            HoldOn.open();
                        }
                        
                });

                Offline.on('confirmed-up', function () {

                    if($('#isHoldOnOpen').val() == 'true'){
                        console.log('open holdon');
                        $('#isHoldOnOpen').val('false');
                        HoldOn.close();
                    }

                });


            }

            setInterval(run, 3000);
            // end of offline js with hold on js for network down error handling
        </script>

    </body>

</html>
