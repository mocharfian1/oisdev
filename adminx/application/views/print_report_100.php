<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 ?>
<style type="text/css">
  table td{
    vertical-align: top;
  }
</style>
<?php if ($page == 'lbr_pernyataan'): ?>
      <h3 align="center"><b>Lembar Pernyataan Cabang / Dealer</b></h3><br><br>
      <table class="center">
            <tbody>
                  <tr>
                        <td style="padding: 10px">Nama Outlet</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($nama_outlet)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Dealer</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($main_dealer)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Fungsi Outlet</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($fungsi_outlet)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Type Pembangunan</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($type_pembangunan)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Alamat</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($alamat)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Telp/Fax</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=html_escape($telp_fax)?></td>
                  </tr>
                  <tr>
                        <td style="padding: 10px">Target Otoritasi</td>
                        <td style="padding: 10px"> : </td>
                        <td style="padding: 10px"><?=format_date_full($target_otorisasi)?></td>
                  </tr>
            </tbody>
      </table>
      <p> Kami dengan ini menyatakan bahwa cabang kami telah melengkapi dan mengevaluasi seluruh fasilitas outlet di Area Sales dan After Sales sehingga telah sesuai standar yang terdapat dalam Buku Evaluasi Otorisasi Outlet Toyota.</p>
      <br>
      <p align="right">Jakarta , <?=todayIndDate()?></p><br>
      <table align="center" border="1" width="80%">
            <thead>
                  <tr style="background-color:#88a854; color: #000">
                        <th style="padding: 2px; text-align: center;" width="20%" colspan="3">Dinyatakan Oleh</th>
                  </tr>
            </thead>
            <tbody>
                  <tr>
                        <td style="padding: 40px"></td>
                        <td style="padding: 40px"></td>
                        <td style="padding: 40px"></td>
                  </tr>
                  <tr>
                        <td style="padding: 15px"></td>
                        <td style="padding: 15px"></td>
                        <td style="padding: 15px"></td>
                  </tr>
                  <tr>
                        <td style="padding: 5px; text-align: center">Kepala Cabang</td>
                        <td style="padding: 5px; text-align: center">Kepala Bengkel</td>
                        <td style="padding: 5px; text-align: center">Kepala Administrasi</td>
                  </tr>
            </tbody>
      </table>
      <br/>
      <br/>
      <table align="center" border="1" width="80%">
            <thead>
                  <tr style="background-color:#88a854; color: #000">
                  <th style="padding: 2px; text-align: center;" width="20%">Disetujui Oleh</th>
                  <th style="padding: 2px; text-align: center;" width="20%" colspan="2">Dinyatakan Oleh</th>
                  </tr>
            </thead>
            <tbody>
                  <tr>
                        <td style="padding: 40px" width="40%"></td>
                        <td style="padding: 40px" width="30%"></td>
                        <td style="padding: 40px" width="30%"></td>
                  </tr>
                  <tr>
                        <td style="padding: 15px"></td>
                        <td style="padding: 15px"></td>
                        <td style="padding: 15px"></td>
                  </tr>
                  <tr>
                        <td style="padding: 5px; text-align: center">Sales & Outlet Dev. Div. Head </td>
                        <td style="padding: 5px; text-align: center">Outlet Dev. Dept. Head</td>
                        <td style="padding: 5px; text-align: center">PIC – DDC MD</td>
                  </tr>
            </tbody>
      </table>
      <br/>
      <br/>
      <table align="center" border="1" width="80%">
            <thead>
                  <tr style="background-color:#88a854; color: #000">
                        <th style="padding: 2px; text-align: center;" width="20%">Disetujui Oleh</th>
                        <th style="padding: 2px; text-align: center;" width="20%" colspan="2">Dinyatakan Oleh</th>
                  </tr>
            </thead>
            <tbody>
                  <tr>
                        <td style="padding: 40px" width="40%"></td>
                        <td style="padding: 40px" width="30%"></td>
                        <td style="padding: 40px" width="30%"></td>
                  </tr>
                  <tr>
                        <td style="padding: 15px; text-align: center"><b>Bansar Maduma</b></td>
                        <td style="padding: 15px; text-align: center"><b>R. Abu Musa</b></td>
                        <td style="padding: 15px; text-align: center"><b>Ario Cahya G.</b></td>
                  </tr>
                  <tr>
                        <td style="padding: 5px; text-align: center">Outlet Devl. Dep. Div. Head</td>
                        <td style="padding: 5px; text-align: center">Outlet Phy. & Std. Sec. Head</td>
                        <td style="padding: 5px; text-align: center">PIC – DDC TAM</td>
                  </tr>
            </tbody>
      </table>
      <br/>
      <br/>
<?php elseif ($page == 'bab1'): ?>
      <h3 align="center"><b>BAB I</b></h3>
      <h3 align="center"><b>PENDAHULUAN</b></h3>
      <b>1. Tujuan</b>
        <p>Toyota dengan segenap jaringan outlet yang tersebar di Indonesia diharapkan dapat selalu memberikan suatu bentuk pelayanan yang terbaik kepada pelanggan baik dari layanan penjualan maupun layanan purna jual sebagai suatu bentuk komitmen untuk terus meningkatkan kepuasan pelanggan secara berkesinambungan. Oleh karena itu Toyota mengharapkan adanya suatu standar yang sama untuk seluruh outlet yang beroperasi dalam memberikan pelayanan, sehingga dalam operasinya perlu dipersiapkan suatu sistem yang baik dengan didukung oleh Fasilitas dan Sumber Daya Manusia yang memadai.</p>
        <p>Untuk mewujudkan hal tersebut, PT. Toyota-Astra Motor mensyaratkan kepada outlet yang akan beroperasi agar dapat memenuhi seluruh kriteria Fasilitas dan Sumber Daya Manusia baik yang menyangkut pelayanan penjualan (Sales) maupun purna jual (After Sales) sebelum outlet tersebut beroperasi. Persyaratan-persyaratan tersebut wajib dilengkapi di dalam 3 tahap selama proses pembangunan Outlet Toyota. 3 Tahap tersebut adalah</p>
        <b>1. Establishment Progress Monitoring 50% (EPM 50%)</b><br/>
        <b>2.  Establishment Progress Monitoring 75% (EPM 75%)</b><br/>
        <b>3.  Establishment Progress Monitoring 100% (EPM 100%) atau Evaluasi Otorisasi</b><br/>
        <p>Tahapan-tahapan tersebut bertujuan untuk syarat mendapatkan Status Otorisasi Outlet Toyota dengan mengikuti standar fasilitas dan Sumber Daya Manusia yang dikeluarkan oleh PT. Toyota Astra Motor.</p>
      <b>2. Prosedur Pelaksanaan Otorisasi</b><br/><br/>
        <img src="<?=https_to_http(base_url('/assets/prosedur_pelaksanaan_otorasi.png'))?>" style="border:2px solid black; padding : 10px; vertical-align: top; width:100%">
        <br/><br/>
      <b>3. Peta Lokasi</b><br/><br/>
      <?php foreach ($project_pic as $pic):?>
        <?php if($pic['type']=='lokasi'):?>
          <!--<p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; align: center"><b>Peta Provinsi</b></p>!-->
          <img src="<?=https_to_http($pic['file_path'])?>"  style="max-width:100%;border:2px solid black; padding : 10px; vertical-align: top;">
        <?php endif;?>
      <?php endforeach;?>
      
        
      <!--<p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; ; align: center"><b>Peta Kota</b></p>
        <img src="<?=base_url('/assets/lokasi.png')?>" style="border:2px solid black; padding : 10px; vertical-align: top;">!-->
<?php elseif ($page == 'bab2'): ?>
      <h3 align="center"><b>BAB II</b></h3>
      <h3 align="center"><b>TARGET DAN INVESTASI</b></h3>
      <b>1. Target</b>
      <table class="center">
            <tbody>
            <tr>
              <td style="padding: 10px">a.</td>
              <td style="padding: 10px">Target Penjualan</td>
              <td style="padding: 10px">:</td>
              <td style="padding: 10px"><?=html_escape($ti['target_penjualan'])?></td>
              <td style="padding: 10px">Unit/Bulan</td>
            </tr>
            <tr>
              <td style="padding: 10px">b.</td>
              <td style="padding: 10px">Jumlah Salesman</td>
              <td style="padding: 10px">:</td>
              <td style="padding: 10px"><?=html_escape($ti['jumlah_salesman'])?></td>
              <td style="padding: 10px">Orang</td>
            </tr>
            <tr>
              <td style="padding: 10px">c.</td>
              <td style="padding: 10px">Target Servis</td>
              <td style="padding: 10px">:</td>
              <td style="padding: 10px"><?=html_escape($ti['target_servis'])?></td>
              <td style="padding: 10px">Unit/Hari</td>
            </tr>
            <tr>
              <td style="padding: 10px; vertical-align: top">d.</td>
              <td style="padding: 10px; vertical-align: top">Jumlah Stall GR</td>
              <td style="padding: 10px; vertical-align: top">:</td>
              <td style="padding: 10px" colspan="2">
                 <table width="100%">
                     <tbody>
                         <tr>
                             <td>EM</td>
                             <td> = </td>
                             <td align="right"><?=html_escape($ti['em'])?></td>
                         </tr>
                         <tr>
                             <td>SBNP</td>
                             <td> = </td>
                             <td align="right"><?=html_escape($ti['sbnp'])?></td>
                         </tr>
                         <tr>
                             <td>GR</td>
                             <td> = </td>
                             <td align="right"><?=html_escape($ti['gr'])?></td>
                         </tr>
                     </tbody>
                 </table>
              </td>
            </tr>
            </tbody>
      </table>
      <table width="100%" border="1">
            <tbody>
             <tr>
                 <td style="padding: 5px; text-align: center" width="13%"></td>
                 <td style="padding: 5px; text-align: center" width="13%">Y</td>
                 <td style="padding: 5px; text-align: center" width="13%">Y+1</td>
                 <td style="padding: 5px; text-align: center" width="13%">Y+2</td>
                 <td style="padding: 5px; text-align: center" width="13%">Y+3</td>
                 <td style="padding: 5px; text-align: center" width="13%">Y+4</td>
                 <td style="padding: 5px; text-align: center" width="13%">Y+5</td>
             </tr>
             <tr>
                 <td style="padding: 5px; text-align: center">Market/Tahun</td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y1'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y2'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y3'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y4'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['market_y5'])?></td>
             </tr>
             <tr>
                 <td style="padding: 5px; text-align: center">Sales/Bulan</td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y1'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y2'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y3'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y4'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['sales_y5'])?></td>
             </tr>
             <tr>
                 <td style="padding: 5px; text-align: center">UE GR/Hari</td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y1'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y2'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y3'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y4'])?></td>
                 <td style="padding: 5px"><?=html_escape($ti['uegr_y5'])?></td>
             </tr>
            </tbody>
      </table><br>
      <b>*Y=Tahun Otorisasi</b><br/><br/>
      <b>2. Investasi</b>
      <table class="center">
            <tbody>
            <tr>
              <td style="padding: 10px">a.</td>
              <td style="padding: 10px">Land Total</td>
              <td style="padding: 10px">:</td>
              <td style="padding: 10px"><?=number_format_dot($ti['land_total'])?></td>
              <td style="padding: 10px">m2</td>
            </tr>
            <tr>
              <td style="padding: 10px">b.</td>
              <td style="padding: 10px">Building Total</td>
              <td style="padding: 10px">:</td>
              <td style="padding: 10px"><?=number_format_dot($ti['building_total'])?></td>
              <td style="padding: 10px">m2</td>
            </tr>
            <tr>
              <td style="padding: 10px; vertical-align: top">c.</td>
              <td style="padding: 10px" colspan="4">Investasi
                 <table width="100%">
                     <tbody>
                         <tr>
                             <td> - </td>
                             <td>Land</td>
                             <td> : </td>
                             <td>Rp</td>
                             <td><?=number_format_dot($ti['land'])?></td>
                         </tr>
                         <tr>
                             <td> - </td>
                             <td>Building</td>
                             <td> : </td>
                             <td>Rp</td>
                             <td><?=number_format_dot($ti['building'])?></td>
                         </tr>
                         <tr>
                             <td> - </td>
                             <td>Equipment</td>
                             <td> : </td>
                             <td>Rp</td>
                             <td><?=number_format_dot($ti['equipment'])?></td>
                         </tr>
                         <tr>
                             <td> - </td>
                             <td>TOTAL</td>
                             <td> : </td>
                             <td>Rp</td>
                             <td><?=number_format_dot($ti['total'])?></td>
                         </tr>
                     </tbody>
                 </table>
              </td>
            </tr>
            </tbody>
      </table>
<?php elseif ($page == 'bab3'): ?>
      <h3 align="center"><b>BAB III</b></h3>
      <h3 align="center"><b>SUMBER DAYA MANUSIA</b></h3>
      <b>1. Struktur Organisasi</b><br/><br/>
      <?php foreach ($project_pic as $pic):?>
        <?php if($pic['type']=='struktur'):?>
          <!--<p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; align: center"><b>Peta Provinsi</b></p>!-->
          <img src="<?=https_to_http($pic['file_path'])?>" style="max-width:100%;border:2px solid black; padding : 10px; vertical-align: top;">
        <?php endif;?>
      <?php endforeach;?>
      <p>Catatan</p>
      <p style="padding-right: 5px">a. Seluruh manpower yang berada di cabang dimasukan ke dalam struktur organisasi baik sales, after sales dan supporting.<p>
      <p style="padding-right: 5px">b.  Apabali terdapat bentuk bagan struktur organisasi yang berbeda, dapat melampirkan struktur yang sesuai.</p>
      <b>2. Klasifikasi Sumber Daya Manusia Area Sales </b><br/><br/>
      <div class="table-responsive">
        <table border="1" style="font-size: 43%;" width="100%">
         <thead>
             <tr>
                 <th style="padding: 2px; text-align: center" rowspan="4">No</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Nama Karyawan</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Jabatan</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Jenis Kelamin (L/P)</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Tanggal Lahir</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Agama</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Pendidikan Terakhir</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">No Telepon</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Tanggal Masuk</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">No. ID TAM (*Jika Punya)</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Cabang Asal (*Jika Mutasi)</th>
                 <th style="padding: 2px; text-align: center" colspan="20">Tanggal Training</th>
                 <th style="padding: 2px; text-align: center" colspan="2">(*Untuk CS & SM)</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center" rowspan="2" colspan="3">CRC / Partsman</th>
                 <th style="padding: 2px; text-align: center" colspan="10">Training &#60; tahun 2011</th>
                 <th style="padding: 2px; text-align: center" colspan="7">Training >= tahun 2011</th>
                 <th style="padding: 2px; text-align: center" rowspan="3">Grade</th>
                 <th style="padding: 2px; text-align: center" rowspan="3">Kategori Sales</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center" colspan="4">Salesman & Countersales</th>
                 <th style="padding: 2px; text-align: center" colspan="3">Supervisor</th>
                 <th style="padding: 2px; text-align: center" colspan="3">Kepala Cabang</th>
                 <th style="padding: 2px; text-align: center" colspan="2">Salesman & Countersales</th>
                 <th style="padding: 2px; text-align: center" colspan="3">Supervisor</th>
                 <th style="padding: 2px; text-align: center" colspan="2">Kepala Cabang</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center">Level 1</th>
                 <th style="padding: 2px; text-align: center">Level 2</th>
                 <th style="padding: 2px; text-align: center">Level 3</th>
                 <th style="padding: 2px; text-align: center">FST</th>
                 <th style="padding: 2px; text-align: center">BSST</th>
                 <th style="padding: 2px; text-align: center">SPD</th>
                 <th style="padding: 2px; text-align: center">ANS</th>
                 <th style="padding: 2px; text-align: center">SAM 1</th>
                 <th style="padding: 2px; text-align: center">SAM 2</th>
                 <th style="padding: 2px; text-align: center">SAM 3</th>
                 <th style="padding: 2px; text-align: center">DOM 1</th>
                 <th style="padding: 2px; text-align: center">DOM 2</th>
                 <th style="padding: 2px; text-align: center">DOM 3</th>
                 <th style="padding: 2px; text-align: center">PSST</th>
                 <th style="padding: 2px; text-align: center">ASST</th>
                 <th style="padding: 2px; text-align: center">BST</th>
                 <th style="padding: 2px; text-align: center">MSTT</th>
                 <th style="padding: 2px; text-align: center">SST</th>
                 <th style="padding: 2px; text-align: center">DFMT</th>
                 <th style="padding: 2px; text-align: center">DBMT</th>
             </tr>
         </thead>
         <tbody>
      <?php $n = 0;?>
      <?php if(count($sdm_sales)==0):?>
        <tr><td colspan="33" align="center" style="font-size: 12px;">Data not found</td></tr>
      <?php endif;?>
      <?php foreach ($sdm_sales as $row): $n++?>
           <tr>
                   <td style="padding: 2px; text-align: center"><?=$n?></td>
                   <td style="padding: 2px; "><?=html_escape($row['nama'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['jabatan'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['jenis_kelamin'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['tanggal_lahir'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['agama'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['pendidikan_terakhir'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['no_telp'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['tanggal_masuk'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['no_id_tam'])?></td>
                   <td style="padding: 2px; "><?=html_escape($row['cabang_asli'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['level_1'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['level_2'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['level_3'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['fst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['bsst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['spd'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['ans'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['sam1'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['sam2'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['sam3'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['dom1'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['dom2'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['dom3'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['psst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['asst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['bst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['mstt'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['sst'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['dfmt'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['dbmt'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['grade'])?></td>
                   <td style="padding: 2px; text-align: center"><?=html_escape($row['kategori_sales'])?></td>
           </tr>

        <?php endforeach;?>
      </tbody>
        </table><br></div>
      <p>Cara Pengisian</p>
      <p>1.  <b>Tanggal masuk</b> adalah tanggal masuk karyawan ke Toyota</p>
      <p>2.  Untuk kolom-kolom <b>training yang</b> di ikuti di isi dengan bulan dan tahun lulus</p>
      <p>3.  <b>Kolom cabang asal</b> dan <b>posisi asal</b> diisi apabila karyawan tersebut adalah rotasi/mutasi dari cabang lain</p>
      <p>4.  <b>Kategori sales</b> diisikan jenis kategorinya (Umum, Dyna, atau sebutkan jika ada yang lain)</p>
      <p>5.  <b>Grade</b> diisikan jabatan salesman (Trainee, Junior, Executive, Senior)</p><br/>
      Keterangan :<br/><br/>
      <table width="100%" style="font-size: 10px">
         <tbody>
          <tr>
            <td valign="top" width="15%">CRC</td>
            <td valign="top" width="15%">SUKU CADANG & PART</td>
            <td valign="top" width="25%">SALESMAN & COUNTER SALES</td>
            <td valign="top" width="25%">SALES SUPERVISOR</td>
            <td valign="top" width="20%">KEPALA CABANG</td>
          </tr>
          <tr>
            <td valign="top">
              <table width="100%">
                  <tr><td>L1</td><td>:</td><td> Level 1 Basic</td></tr>
                  <tr><td>L2</td><td>:</td><td> Level 2 Intermediate</td></tr>
                  <tr><td>L3</td><td>:</td><td> Level 3 Advance</td></tr>
              </table>
            </td>
            <td valign="top">
              <table width="100%">
                <tr><td>L1</td><td>:</td><td> Level 1<br></td></tr>
                <tr><td>L2</td><td>:</td><td> Level 2<br></td></tr>
                <tr><td>L3</td><td>:</td><td> Level 3</td></tr>

              </table>
            </td>
            <td valign="top">
              <table width="100%">
              <tr><td>FST </td><td>:</td><td> First Selling Training<br></td></tr>
              <tr><td>BSST </td><td>:</td><td> Basic Sellig Step Training<br></td></tr>
              <tr><td>SPD </td><td>:</td><td> Sales Potential Dealer<br></td></tr>
              <tr><td>ANS </td><td>:</td><td> Advance Negotiation Skill<br></td></tr>
              <tr><td>PSST </td><td>:</td><td> Personal Selling Skill Training<br></td></tr>
              <tr><td>ASST </td><td>:</td><td>  Advance Selling Skill Training</td></tr>
              </table>
            </td>

            <td valign="top" >
              <table width="100%">
                <tr><td>FST </td><td>:</td><td> First Selling Training<br></td></tr>
                <tr><td>BSST </td><td>:</td><td> Basic Sellig Step Training<br></td></tr>
                <tr><td>SPD </td><td>:</td><td> Sales Potential Dealer<br></td></tr>
                <tr><td>ANS </td><td>:</td><td> Advance Negotiation Skill<br></td></tr>
                <tr><td>BST </td><td>:</td><td> Basic Supervisory Training)<br></td></tr>
                <tr><td>MSTT </td><td>:</td><td> Managing Supervisory Task Training)<br></td></tr>
                <tr><td>SST </td><td>:</td><td> Strategic Supervisory Training</td></tr>
              </table>
            </td>

            <td valign="top">
              <table width="100%">
                <tr><td>DOM </td><td>:</td><td> Dealer Operation Management<br></td></tr>
                <tr><td>DFMT </td><td>:</td><td> Dealer Function Management Training<br></td></tr>
                <tr><td>DBMT </td><td>:</td><td> Dealer Branch Management Training</td></tr>
              </table>
            </td>
          </tr>
         </tbody>
      </table><br/>
      <b>3. Klasifikasi Sumber Daya Manusia Area After Sales  </b>
      <p>1. General Repair</p>
      <div class="table-responsive">
        <table border="1" style="font-size: 50%;" width="100%">
         <thead>
             <tr>
                 <th style="padding: 2px; text-align: center" rowspan="4">No</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Nama Karyawan</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Tanggal Lahir</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Agama</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Pendidikan Terakhir</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">No Telepon</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Tanggal Masuk</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">Jabatan</th>
                 <th style="padding: 2px; text-align: center" rowspan="4">No. ID TAM (*Jika Punya)</th>
                 <th style="padding: 2px; text-align: center" colspan="18">Tanggal Training</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center" colspan="7">Teknisi</th>
                 <th style="padding: 2px; text-align: center" rowspan="2" colspan="3">SA GR</th>
                 <th style="padding: 2px; text-align: center" rowspan="2" colspan="3">PARTMANT</th>
                 <th style="padding: 2px; text-align: center" rowspan="2" colspan="3">FOREMAN</th>
                 <th style="padding: 2px; text-align: center" rowspan="2" colspan="2">KABENG</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center" colspan="2">LEVEL 1</th>
                 <th style="padding: 2px; text-align: center" colspan="3">LEVEL 2</th>
                 <th style="padding: 2px; text-align: center" colspan="2">LEVEL 3</th>
             </tr>
             <tr>
                 <th style="padding: 2px; text-align: center">TT</th>
                 <th style="padding: 2px; text-align: center">PT</th>
                 <th style="padding: 2px; text-align: center">DTG</th>
                 <th style="padding: 2px; text-align: center">DTL</th>
                 <th style="padding: 2px; text-align: center">DTC</th>
                 <th style="padding: 2px; text-align: center">DMT</th>
                 <th style="padding: 2px; text-align: center">LD</th>
                 <th style="padding: 2px; text-align: center">L1</th>
                 <th style="padding: 2px; text-align: center">L2</th>
                 <th style="padding: 2px; text-align: center">L3</th>
                 <th style="padding: 2px; text-align: center">P-L1</th>
                 <th style="padding: 2px; text-align: center">P-L2</th>
                 <th style="padding: 2px; text-align: center">P-L3</th>
                 <th style="padding: 2px; text-align: center">FO-L1</th>
                 <th style="padding: 2px; text-align: center">FO-L2</th>
                 <th style="padding: 2px; text-align: center">FO-L3</th>
                 <th style="padding: 2px; text-align: center">TSMTL1</th>
                 <th style="padding: 2px; text-align: center">TSMTL2</th>
             </tr>
         </thead>
         <tbody>

      <?php if(count($sdm_after_sales)==0):?>
        <tr><td colspan="27" align="center" style="font-size: 12px;">Data not found</td></tr>
      <?php endif;?>
            <?php $n=0;foreach ($sdm_after_sales as $row): $n++?>
                 <tr>
                         <td style="padding: 2px; text-align: center"><?=$n?></td>
                         <td style="padding: 2px; "><?=html_escape($row['nama'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['jabatan'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['jenis_kelamin'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['tanggal_lahir'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['agama'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['pendidikan_terakhir'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['no_telp'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['tanggal_masuk'])?></td>
                         <td style="padding: 2px; "><?=html_escape($row['no_id_tam'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['tt'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['pt'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['dtg'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['dtl'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['dtc'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['dmt'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['l1'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['l2'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['l3'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['pl1'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['pl2'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['pl3'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['fol1'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['fol2'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['fol3'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['tsmtl2'])?></td>
                         <td style="padding: 2px; text-align: center"><?=html_escape($row['tsmtl2'])?></td>
                     </tr>

              <?php endforeach;?>
        </table><br></div>
      Keterangan :<br/><br/>

      <table width="100%" style="font-size: 10px">
         <tbody>
          <tr>
            <td valign="top" width="20%">TRAINING TEKNISI/FO</td>
            <td valign="top" width="25%">KEPALA BENGKEL</td>
            <td valign="top" width="25%">FOREMAN</td>
            <td valign="top" width="15%">SERVICE ADVISOR GR</td>
            <td valign="top" width="20%">PARTMAN</td>
          </tr>
          <tr>
            <td valign="top">
              <table width="100%">
                <tr><td valign="top">TT </td><td>:</td><td> Toyota Technician</td></tr>
                <tr><td valign="top">PT </td><td>:</td><td> Pro - Technician</td></tr>
                <tr><td valign="top">DT </td><td>:</td><td> Diagnosis Technician</td></tr>
                <tr><td valign="top">DTG</td><td>:</td><td> Diagnosis Technician Engine</td></tr>
                <tr><td valign="top">DTL</td><td>:</td><td> Diagnosis Technician Electical</td></tr>
                <tr><td valign="top">DTC</td><td>:</td><td> Diagbosis Technician Chassis</td></tr>
                <tr><td valign="top">DMT</td><td>:</td><td> Diagnosis Master Technician</td></tr>
                <tr><td valign="top">LD </td><td>:</td><td> Latest Diagnosis</td></tr>
              </table>
            </td>
            <td valign="top">
              <table width="100%">
                <tr><td>TSMTL1 </td><td>:</td><td> Toyota Service Managament Training (Managing Basic Operation)<br></td></tr>
                <tr><td>TSMTL2 </td><td>:</td><td> Toyota Service Managament Training (Managing & Improving Service & CS)</td></tr>

              </table>
            </td>
            <td valign="top">
              <table width="100%">
                <tr><td>L1  </td><td>:</td><td> Level 1 Toyota Foremanship Training (Basic Procedure Standard & Leadership)<br></td></tr>
                <tr><td>L2  </td><td>:</td><td> Level 2 Toyota Foremanship Training (Motivate & Develop Team)<br></td></tr>
                <tr><td>L3  </td><td>:</td><td> Level 3 Toyota Foremanship Training (MRA – SA – PART Sychronization & Kaizen)</td></tr>

              </table>
            </td>

            <td valign="top" >
              <table width="100%">
                <tr><td>L1  </td><td>:</td><td> Level 1 TSA21<br></td></tr>
                <tr><td>L2  </td><td>:</td><td> Level 2 TSA21<br></td></tr>
                <tr><td>L3  </td><td>:</td><td> Level 3 TSA21</td></tr>

              </table>
            </td>

            <td valign="top">
              <table width="100%">
                <tr><td>L1  </td><td>:</td><td> Level 1 (Basic Part Operation)<br></td></tr>
                <tr><td>L2  </td><td>:</td><td> Level 2 (Smooth Process of Parts & Synchronization)<br></td></tr>
                <tr><td>L3  </td><td>:</td><td> Level 3 (Efficiant & Profitable Parts Operation)</td></tr>

              </table>
            </td>
          </tr>
         </tbody>
      </table>
<?php elseif ($page == 'last'): ?>
    <h3 align="center"><b>BAB <?=$lastbab?></b></h3>
    <h3 align="center"><b>LAMPIRAN</b></h3>
    <div style="border: 2px solid black; padding: 10px">
    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
        <b>1. EXECUTIVE SUMMARY </b>
    </div><br><br>
    <p align="center"><b>Outlet Authorized Executive Summary</b></p>
    <style type="text/css">
      .border td{
        border: 1px solid black;
      }
    </style>
  <table style="padding: 5px;border:none;" class="border" width="100%" border="1">
    <tr>
        <td style="padding: 5px" colspan="4"><b>I. General</b></td>
    </tr>
    <tr>
        <td style="padding: 5px" width="15%">Outlet Name</td>
        <td style="padding: 5px" width="40%"><?=html_escape($nama_outlet)?></td>
        <td style="padding: 5px" width="25%" colspan="2">- No. of parking stall</td>
    </tr>
    <tr>
        <td style="padding: 5px">Function</td>
        <td style="padding: 5px"><?=html_escape($ex['func'])?></td>
        <td style="padding: 5px">Service</td>
        <td style="padding: 5px"><?=html_escape($ex['park_service'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px">Date of Authorized</td>
        <td style="padding: 5px"><?=html_escape($ex['date'])?></td>
        <td style="padding: 5px">Customer Car / Motorcycle</td>
        <td style="padding: 5px"><?=html_escape($ex['park_customer'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px">Address</td>
        <td style="padding: 5px"><?=html_escape($ex['address'])?></td>
        <td style="padding: 5px">Employee Car / Motorcycle</td>
        <td style="padding: 5px"><?=html_escape($ex['park_employee'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px">Phone</td>
        <td style="padding: 5px"><?=html_escape($ex['phone'])?></td>
        <td style="padding: 5px" colspan="2"><b>V. Man Power</b></td>
    </tr>
    <tr>
        <td style="padding: 5px">Fax</td>
        <td style="padding: 5px"><?=html_escape($ex['fax'])?> </td>
        <td style="padding: 5px" colspan="2"> - Management</td>
    </tr>
    <tr>
        <td style="padding: 5px">Sales</td>
        <td style="padding: 5px"><?=html_escape($ex['sales_category'])?></td>
        <td style="padding: 5px; padding-left: 30px">Branch Head</td>
        <td style="padding: 5px"><?=html_escape($ex['branch_head'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px" colspan="2"><b>II. Investasi</b></td>
        <td style="padding: 5px; padding-left: 30px">Service Head</td>
        <td style="padding: 5px"><?=html_escape($ex['service_head'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px">Land Total</td>
        <td style="padding: 5px; padding-left: 30px"><?=number_format_dot($ex['land_total'])?>m2 </td>
        <td style="padding: 5px; padding-left: 30px">Administration Head</td>
        <td style="padding: 5px"><?=html_escape($ex['admin_head'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px">Building Total</td>
        <td style="padding: 5px; padding-left: 30px"><?=number_format_dot($ex['building_total'])?> m2 </td>
        <td style="padding: 5px" colspan="2" > - Sales (No. of person current/room capacity</td>
    </tr>
    <tr>
        <td style="padding: 5px">Invesment Total</td>
        <td style="padding: 5px"> Rp.<?=number_format_dot($ex['total'])?> </td>
        <td style="padding: 5px; padding-left: 30px">Sales Supervisor/room capacity</td>
        <td style="padding: 5px"><?=html_escape($ex['supervisor_per_room'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px">Land</td>
        <td style="padding: 5px"> Rp.<?=number_format_dot($ex['land'])?> </td>
        <td style="padding: 5px; padding-left: 30px">Sales Man/room capacity</td>
        <td style="padding: 5px"><?=html_escape($ex['salesman_per_room'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px">Buliding</td>
        <td style="padding: 5px"> Rp.<?=number_format_dot($ex['building'])?> </td>
        <td style="padding: 5px; padding-left: 30px">Counter Sales/room capacity</td>
        <td style="padding: 5px"><?=html_escape($ex['countersales_per_room'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px">Equipment</td>
        <td style="padding: 5px"> Rp.<?=number_format_dot($ex['equipment'])?> </td>
        <td style="padding: 5px" colspan="2"> - Service</td>
    </tr>
    <tr>
        <td style="padding: 5px" colspan="2"><b>III. Target</b></td>
        <td style="padding: 5px" colspan="2">GR</td>
    </tr>
    <tr>
        <td style="padding: 5px" valign="top" rowspan="7" colspan="2">
          <table width="100%" border="1">
           <tbody>
               <tr>
                   <td style=" text-align: center" width="13%"></td>
                   <td style=" text-align: center" width="13%">Y</td>
                   <td style=" text-align: center" width="13%">Y+1</td>
                   <td style=" text-align: center" width="13%">Y+2</td>
                   <td style=" text-align: center" width="13%">Y+3</td>
                   <td style="padding: 5px; text-align: center" width="13%">Y+4</td>
               </tr>
               <tr>
                   <td style="padding: 5px">Market (/year)</td>
                   <td style="padding: 5px"><?=html_escape($ex['my_awal'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['my_plus1'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['my_plus2'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['my_plus3'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['my_plus4'])?></td>
               </tr>
               <tr>
                   <td style="padding: 5px"> - Sales (/mth)</td>
                   <td style="padding: 5px"><?=html_escape($ex['sy_awal'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['sy_plus1'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['sy_plus2'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['sy_plus3'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['sy_plus4'])?></td>
               </tr>
               <tr>
                   <td style="padding: 5px" colspan="6"> - Service</td>
               </tr>
               <tr>
                   <td style="padding: 5px; padding-left: 20px"> - UE GR (/day)</td>
                   <td style="padding: 5px"><?=html_escape($ex['gry_awal'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['gry_plus1'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['gry_plus2'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['gry_plus3'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['gry_plus4'])?></td>
               </tr>
               <tr>
                   <td style="padding: 5px; padding-left: 20px"> - UE BP (/day)</td>
                   <td style="padding: 5px"><?=html_escape($ex['bpy_awal'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['bpy_plus1'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['bpy_plus2'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['bpy_plus3'])?></td>
                   <td style="padding: 5px"><?=html_escape($ex['bpy_plus4'])?></td>
               </tr>
           </tbody>
          </table>
          <p><b>Y = Year of authorization</b></p>
        </td>
        <td style="padding: 5px; padding-left: 30px">Service Supervisor</td>
        <td style="padding: 5px"><?=html_escape($ex['gr_supervisor'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">Technical Leader</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_technical_leader'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">Foreman</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_foreman'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">PTM</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_ptm'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">Technician</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_technician'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">Service Advisor</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_service_advisor'])?></td>
    </tr>
    <tr>
      <td style="padding: 5px; padding-left: 30px">Partsman</td>
      <td style="padding: 5px"><?=html_escape($ex['gr_partsman'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px;" colspan="2"><b>IV. Facility</b></td>
        <td style="padding: 5px" colspan="2">BP</td>
    </tr>
    <tr>
        <td style="padding: 5px" colspan="2"> - Sales</td>
        <td style="padding: 5px; padding-left: 30px" width="25%">Service Supervisor</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['bp_supervisor'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Unit display</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['display'])?></td>
        <td style="padding: 5px; padding-left: 30px" width="25%">Foreman</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['bp_foreman'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Stock capacity</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['capacity'])?></td>
        <td style="padding: 5px; padding-left: 30px" width="25%">Technician</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['bp_technician'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px" colspan="2"> - Production Stall</td>
        <td style="padding: 5px; padding-left: 50px" width="25%">Body</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['bp_body'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">EM</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['em'])?></td>
        <td style="padding: 5px; padding-left: 50px" width="25%">Paint</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['bp_paint'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">GR</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['gr'])?></td>
        <td style="padding: 5px" width="25%" colspan="2">Others</td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Dyna/LC</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['lc'])?></td>
        <td style="padding: 5px; padding-left: 50px" width="25%">Head</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['other_head'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Allocation for Expansion</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['allocation'])?></td>
        <td style="padding: 5px; padding-left: 50px" width="25%">Salesman</td>
        <td style="padding: 5px" width="20%"><?=html_escape($ex['other_salesman'])?></td>
    </tr>
    <tr style="border-right: none;">
        <td style="padding: 5px; padding-left: 30px" width="15%">No. of lift</td>
        <td style="padding: 5px;border-right:1px solid black;" width="40%";><?=html_escape($ex['lift'])?></td>
        <td style="border:none;" colspan="2" rowspan="6"></td>
    </tr>
    <tr>
        <td style="padding: 5px" width="15%" colspan="2"> - Other Stall</td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">PDS/DIO</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['psd'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Spooring</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['spooring'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Washing for service</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['washing_service'])?></td>
    </tr>
    <tr>
        <td style="padding: 5px; padding-left: 30px" width="15%">Washing for new car</td>
        <td style="padding: 5px" width="40%"><?=html_escape($ex['washing_new'])?></td>
    </tr>
  </table>
  <p><b>* File ms Excel terlampir</b></p>
  </div><br><br>
  <div style="border: 2px solid black; padding: 10px">
    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
        <b>2. DOKUMEN PENDUKUNG </b>
    </div>
  <p>Sebagai data pendukung untuk proses otorisasi outlet, maka kami mohon bantuan pihak Dealer untuk melampirkan <br> dokumen-dokumen seperti berikut, yaitu : <br><br>a.  Akta Pendirian Cabang<br>b.  Nomor Pokok Wajib Pajak (NPWP) Cabang<br>c.  Tanda Daftar Perusahaan (TDP) Cabang<br>d.  Surat Ijin Usaha Perdagangan (SIUP) Cabang<br>e.  Surat Keterangan Domisili Cabang<br><br>NOTE : <br>Seluruh kelengkapan dokumen yang dilampirkan adalah kelengkapan dokumen yang beralamat atau atas nama cabang outlet baru (Bukan Kantor Pusat). Apabila ada beberapa dokumen yang <b>menjadi satu dengan Kantor Pusat tidak perlu dilampirkan</b>, yang dilampirkan hanya yang beralamat atau atas nama cabang baru.<br></p>
  </div><br><br>
  <div style="border: 2px solid black; padding: 10px">
    <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:350px">
        <b>3. PENUTUPAN SALES SERVICE POINT (SSP)</b>
    </div>
  <p>NOTE : Lampirkan foto SSP yang sudah tidak beroperasi  apabila outlet tersebut memiliki SSP</p>
  <center>
  <?php foreach ($project_pic as $pic):?>
        <?php if($pic['type']=='ssp'):?>
          <!--<p style="max-width: 150px; border:2px solid black; padding : 10px; vertical-align: top; align: center"><b>Peta Provinsi</b></p>!-->
          
          <img src="<?=https_to_http($pic['file_path'])?>" style="max-width:500px;border:2px solid black; padding : 10px; vertical-align: top;">
        <?php endif;?>
      <?php endforeach;?>
      </center>
  <p align="center">Foto SSP yang sudah tidak beropersi/tutup</p>

      
  </div><br><br>
<?php else: ?>
<?php $area = (object)$data_area[$page];?>
  <h3 align="center"><b>BAB <?=$babroman[$page]?></b></h3>
  <h3 align="center"><b><?=$area->area_name?></b></h3>
  
  <?php foreach ($area->sub as $sub):?>
    <?php if($sub['position']==1):?>
      <p><b>Cara Pengisian</b></p>
          <img src="<?=https_to_http(base_url('/assets/cara2.png'))?>" style="padding : 10px;width: 100%" />
      <br>
      <p><b>Keterangan</b></p>
      <br>1. <b>Lampirkan foto dengan resolusi yang jelas</b> sesuai dengan keterangan di bawah foto
      <br>2.  Lakukan pengecekan sesuai dengan kolom  yang disediakan :
      <br><b>Kolom #1 diisi oleh <?=strtoupper($type_level_1)?></b>
      <br><b>Kolom #2 diisi dan dicek oleh <?=strtoupper($type_level_2)?></b>
      <br><b>Kolom #3 diisi dan dicek oleh <?=strtoupper($type_level_3)?></b>
      <br>- bubuhkan  tanda tick    <b>( √ )</b> = apabila kriteria minimum sudah sesuai actual
      <br>- bubuhkan  tanda cross <b>( X )</b> = apabila kriteria minimum tidak sesuai actual<br><br>
      <div style="page-break-after: always;"></div>
    <?php endif;?>
    <?php if($sub['position']>1):?>
      <br/><br/>
      <div style="page-break-after: always;"></div>
    <?php endif;?>
      <div style="border: 2px solid black; padding: 5px;">
        <div style="border: 2px solid black; padding: 5px; background-color: yellow; max-width:300px">
            <b><?=html_escape($sub['position'])?>. <?=html_escape($sub['sub_area_name'])?></b>
        </div><br><br>
        <?php if(isset($sub['config_pic'])):?>
          <?php foreach ($sub['config_pic'] as $pic):?>
            <center>
            <img src="<?=https_to_http($pic['file_path'])?>" width="50%">
            </center>
            <br>
            <p align="center"><?=html_escape($pic['item_pic'])?></p>
          <?php endforeach;?>
        <?php endif;?>
        <?php if(isset($sub['km'])):?>
          <table border="1" align="center" width="80%"><br><br>
              <thead>
                <tr style="background-color:#a0c66b; color: #000">
                    <th style="padding: 10px; text-align: center" width="5%">No</th>
                    <th style="padding: 10px; text-align: center" width="50%">Kriteria Minimun</th>
                       <?php if ($sub['km'][0]['model'] == 'Model 2'): ?>
                              <th style="text-align: center;">Jarak</th>
                       <?php endif;?>
                    <th style="padding: 10px; text-align: center" width="15%">#1</th>
                    <th style="padding: 10px; text-align: center" width="15%">#2</th>
                    <th style="padding: 10px; text-align: center" width="15%">#3</th>
                </tr>
              </thead>
              <tbody>
                    <?php foreach ($sub['km'] as $item): ?>
                           <?php
                            $check     = ['1' => '√', '0' => 'X', '' => ''];
                            $dataScore = [
                                'dealer'    => $check[$item['score_dealer']],
                                'konsultan' => $check[$item['score_konsultan']],
                                'tam'       => $check[$item['score_tam']],
                            ];
                            ?>
                           <tr>
                                  <td style="padding: 7px"><?=html_escape($item['position'])?></td>
                                  <td style="padding: 7px"><?=html_escape($item['item_kriteria_minimum'])?></td>
                              <?php if ($item['model'] == 'Model 2'): ?>
                                     <td style="padding: 7px"><?=$item['jarak']?></td>
                              <?php endif;?>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                           </tr>
                    <?php endforeach;?>
              <tbody>
          </table><br/><br/>
        <?php else:?>
          <center>Data Not Found</center>
        <?php endif;?>
        <?php if (isset($sub['catatan'])): ?>
          <?php if ((isset($sub['catatan']['dealer']) and trim($sub['catatan']['dealer'])!='') or (isset($sub['catatan']['konsultan']) and trim($sub['catatan']['konsultan'])!='') or (isset($sub['catatan']['tam']) and trim($sub['catatan']['tam'])!='')):?>
              <div style="border: 2px solid #000; color: #000; padding: 5px;">
                     <b>Catatan :</b><br/>
                     <?=((isset($sub['catatan']['dealer']) and trim($sub['catatan']['dealer'])!='') ? 'Dealer : '.$sub['catatan']['dealer'].'<br/>' : '')?>
                     <?=((isset($sub['catatan']['konsultan']) and trim($sub['catatan']['konsultan'])!='') ? 'Konsultan : '.$sub['catatan']['konsultan'].'<br/>' : '')?>
                     <?=((isset($sub['catatan']['tam']) and trim($sub['catatan']['tam'])!='') ? 'TAM : '.$sub['catatan']['tam'].'<br/>' : '')?>
              </div>
              <br/><br/>
            <?php endif;?>
        <?php endif;?>
      </div>  
  <?php endforeach;?>
<?php endif;?>