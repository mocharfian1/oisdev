<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 ?>
 
<?php if ($mode == 'view') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Evaluasi Kunjungan
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Evaluasi Kunjungan</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Evaluasi Kunjungan</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?php echo base_url('evaluasi_kunjungan/set_page/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="all">No.</th>
                                  <th class="all">Nama Outlet</th>
                                  <th class="all">Main Dealer</th>
                                  <th class="all">EPM</th>
                                  <th class="all">Tgl. Evaluasi</th>
                                  <th class="all" style="text-align: center; width: 100px">Act.</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($data_evaluasi as $row) {?>
                              <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?=html_escape($row['nama_outlet'])?></td>
                                <td><?=html_escape($row['main_dealer'])?></td>
                                <td><?=html_escape($row['jenis_epm'])?>%</td>
                                <td><?php echo date('d M Y', strtotime($row['tgl_kunjungan'])); ?></td>
                                <td class="text-center">
                                  <a href="<?php echo base_url('evaluasi_kunjungan/set_page/edit/'.$row['id']); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                  <a href="<?php echo base_url('evaluasi_kunjungan/set_page/detail_evaluasi_kunjungan/'.$row['id']); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Set Evaluasi Kunjungan"><i class="fa fa-calendar-check-o"></i></a>
                                  <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                      <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                      <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo $row['id']; ?>', base_url+'evaluasi_kunjungan/delete_data/');"><i class="fa fa-close"></i></a>   
                                  </form>
                                </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($mode == 'add' || $mode == 'edit') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Evaluasi Kunjungan</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                    
                  
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                          <input type="hidden" name="id" value="<?php if(!empty($data_evaluasi)){echo $data_evaluasi->id;} ?>">
                          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                          <div class="form-body">

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Project</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">
                                          <select onchange="filteringepm(this)" name="id_project" id="id_project" class="form-control id-project">
                                            
                                            <option selected="true" disabled="disabled">- belum ada project yang terpilih -</option>
                                            <?php foreach ($data_project as $row) { ?>
                                              <option value="<?php echo $row['id']; ?>" <?php if(!empty($data_evaluasi) && $row['id'] == $data_evaluasi->id_project){echo 'selected';}; ?>><?=html_escape($row['nama_outlet'])?></option>
                                            <?php } ?>
                                          </select>
                                        </div>
                                  </div>
                              </div>
                            
                              <div class="form-group">
                                  <label class="col-md-2 control-label">EPM</label>
                                  <div class="col-md-2">
                                        <div style="margin-left: 15px">
                                          <select name="epm" id="epm" class="form-control" size="1">
                                            <option selected="true" disabled="disabled">- Please select -</option>
                                            <option value="50" <?php if(!empty($data_evaluasi) && $data_evaluasi->jenis_epm == '50'){echo 'selected';}; ?>>50%</option>
                                            <option value="75" <?php if(!empty($data_evaluasi) && $data_evaluasi->jenis_epm == '75'){echo 'selected';}; ?>>75%</option>
                                            <option value="100" <?php if(!empty($data_evaluasi) && $data_evaluasi->jenis_epm == '100'){echo 'selected';}; ?>>100%</option>
                                          </select>
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">Tgl. Kunjungan</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="tgl_kunjungan" name="tgl_kunjungan" style="margin-left: 15px" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_evaluasi)){echo date('d M Y', strtotime($data_evaluasi->tgl_kunjungan));}; ?>" placeholder="please choose date" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>

                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                <a href="javascript:;" id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_evaluasi_kunjungan';}else{echo 'edit_evaluasi_kunjungan';}; ?>&#39;);" class="btn green">Save</a>
                                <a id="btnCancel" href="<?php echo base_url('evaluasi_kunjungan/set_page/view'); ?>" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                          <!-- <div class="form-actions"> -->

                          <!-- </div> -->
                      </form>
                    
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($mode == 'detail_evaluasi_kunjungan') { ?>

<div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Detail Evaluasi Kunjungan
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Evaluasi Kunjungan</span>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Detail Evaluasi Kunjungan</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Data Detail Evaluasi Kunjungan (<?php echo $project[0]['nama_outlet']; echo " - "; echo $project[0]['jenis_epm']; ?>%)</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?php echo base_url('evaluasi_kunjungan/set_page/view'); ?>" class="btn btn-danger btn-md">Back</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                    <?php if (count($data_inspeksi)>0) { ?>
                      <h3 class="page-title text-center">Data Inspeksi Belum Lengkap
                    <?php } else { ?>
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="all">No.</th>
                                  <th class="all">Area</th>
                                  <th class="all">Fasilitas</th>
                                  <th class="all">Problem/Deskripsi</th>
                                  <th class="all">Tindak Lanjut</th>
                                  <th class="all">Target</th>
                                  <th class="all">PIC</th>
                                  <th class="all" style="text-align: center; width: 25px">Act.</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $no = 1; foreach ($data_detail_evaluasi as $row) {?>
                              <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $row['area_name']; echo " ("; echo $row['sub_area_name']; echo ")"; ?></td>
                                <td><?=html_escape($row['fungsi_outlet'])?></td>
                                <td><?=html_escape($row['eval_problem_deskripsi'])?></td>
                                <td><?=html_escape($row['eval_tindak_lanjut'])?></td>
                                <td><?=html_escape($row['eval_target'])?></td>
                                <td><?=html_escape($row['eval_pic'])?></td>
                                <td class="text-center">
                                  <a href="<?php echo base_url('evaluasi_kunjungan/set_page/edit_detail_evaluasi_kunjungan/'.$row['id']); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                    <?php } ?>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($mode == 'edit_detail_evaluasi_kunjungan') { ?>

 <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Edit Data
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Evaluasi Kunjungan</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Detail Evaluasi Kunjungan</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Edit Data</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">Form Edit Data Detail Evaluasi Kunjungan pada <u><?php echo $data_detail_evaluasi[0]['nama_outlet'] ?></u></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                    
                  
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                          <input type="hidden" name="id" value="<?php if(!empty($data_detail_evaluasi)){echo $data_detail_evaluasi[0]['id'];} ?>">
                          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                          <input type="hidden" name="id_evaluasi_kunjungan" value="<?php if(!empty($data_detail_evaluasi)){echo $data_detail_evaluasi[0]['id_evaluasi_kunjungan'];} ?>">
                          <div class="form-body">

                              <div class="form-group">
                                <label class="col-md-2 control-label">Area</label>
                                <div class="col-md-3">
                                    <input style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['area_name'] ?>" readonly/>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">Sub Area</label>
                                <div class="col-md-3">
                                    <input style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['sub_area_name'] ?>" readonly/>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">Fasilitas</label>
                                <div class="col-md-2">
                                    <input style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['fungsi_outlet'] ?>" readonly/>
                                </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                <label class="col-md-2 control-label">Problem/Deskripsi</label>
                                <div class="col-md-10">
                                    <input id="eval_problem_deskripsi" name="eval_problem_deskripsi" style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['eval_problem_deskripsi'] ?>"/>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">Tindak Lanjut</label>
                                <div class="col-md-10">
                                    <input name="tindak_lanjut" id="tindak_lanjut" style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['eval_tindak_lanjut'] ?>"/>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">Target</label>
                                <div class="col-md-10">
                                    <input name="target" id="target" style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['eval_target'] ?>"/>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-2 control-label">PIC</label>
                                <div class="col-md-10">
                                    <input name="pic" id="pic" style="margin-left: 15px" class="form-control" type="text" value="<?php echo $data_detail_evaluasi[0]['eval_pic'] ?>"/>
                                </div>
                              </div>

                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                <a href="javascript:;" id="btnSave" onclick="save('edit_detail_evaluasi_kunjungan')" class="btn green">Save</a>
                                <a id="btnCancel" href="<?php echo base_url('evaluasi_kunjungan/set_page/detail_evaluasi_kunjungan/'.$data_detail_evaluasi[0]['id_evaluasi_kunjungan']); ?>" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                          <!-- <div class="form-actions"> -->

                          <!-- </div> -->
                      </form>
                    
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>