
<!DOCTYPE html>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>404</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="icon" type="image/png" href="<?=base_url('assets/global/img/favicon/')?>favicon-toyota.png" sizes="32x32" /> 
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
       
        <link href="<?php echo base_url(); ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
       
        <!-- END GLOBAL MANDATORY STYLES/ -->
        <!-- BEGIN THEME GLOBAL STYLES -->
       
        <link href="<?php echo base_url(); ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?php echo base_url(); ?>assets/pages/css/custom.error.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
       
        <link href="<?php echo base_url(); ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        
    </head>
    <!-- END HEAD -->

    <body class="" style="background-color: #fff; width: 98vw !important">
        
       
        <div class="row">
            <div class="col-md-12 page-404">
                <center>
                    <div class="number font-green"> <?php echo html_escape($error_code); ?> </div><br><br>
                    <div class="details" style="margin-left: : 0px !important">
                        <center>
                        <h3>Oops!</h3>
                        <!-- <p> We can not find the page you're looking for.</p> -->
                        <p><?php echo html_escape($user_message); ?></p>
                        </center>
                    </div>
                </center>
            </div>
        </div>
       
        <!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
       
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
     
    </body>

</html>