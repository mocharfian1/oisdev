<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 ?>
 
<?php if($sub_menu == 'area' && $mode == 'view'){ ?>

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->

        <!-- END THEME PANEL -->
        <h3 class="page-title"> Area
            <!-- <small>first demo</small> -->
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Master Data</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Area</span>
                </li>
            </ul>
            <div class="page-toolbar">

            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-list font-dark"></i>
                            <span class="caption-subject bold uppercase">List Area</span>


                        </div>
                        <div class="pull-right">
                            <a href="<?=base_url('master_data/area/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                                <tr>
                                    <th class="">No.</th>
                                    <th class="">Area Name</th>
                                    <th class="">Area Code</th>
                                    <th class="">Description</th>
                                    <th class="" style="text-align: center; width: 75px">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php
                              $no = 1;
                              foreach ($data_area as $row) { ?>
                                <tr>
                                    <td><?= html_escape($no++); ?></td>
                                    <td><?= html_escape($row['area_name']); ?></td>
                                    <td><?= html_escape($row['area_code']); ?></td>
                                    <td><?= html_escape($row['description']); ?></td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td style="padding: 0px 0px;">
                                                    <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/area/edit'); ?>">
                                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                        <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>   
                                                    </form>
                                                    <!-- <a class="btn btn-sm btn-default" onclick="goToEditPage('<?php echo html_escape($row['id']); ?>');"><i class="fa fa-pencil"></i></a> -->
                                                </td>
                                                <td style="padding: 0px 0px;">
                                                    <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                        <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo $row['id']; ?>', base_url+'master_data/delete_data/area/');"><i class="fa fa-close"></i></a>   
                                                    </form>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                              <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>

<?php } ?>

<?php if($sub_menu == 'area' && ($mode == 'add' || $mode == 'edit')){ ?>

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->

        <!-- END THEME PANEL -->
        <h3 class="page-title">
            <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
            <!-- <small>first demo</small> -->
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Master Data</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Area</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
                </li>
            </ul>
            <div class="page-toolbar">

            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-doc font-dark"></i>
                            <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body form">
                      
                        <form action="#" id="form" class="form-horizontal" role="form">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php if(!empty($data_area)){echo xss_clean($data_area->id);} ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Area Name</label>
                                    <div class="col-md-4">
                                        <input type="text" id="area_name" name="area_name" maxlength="150" value="<?php if(!empty($data_area)){echo html_escape($data_area->area_name);} ?>" placeholder="Please input area name" class="form-control alphanumeric" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Area Code</label>
                                    <div class="col-md-4">
                                        <input type="text" id="area_code" name="area_code" maxlength="150" value="<?php if(!empty($data_area)){echo html_escape($data_area->area_code);} ?>" placeholder="Please input area code" class="form-control alphanumeric" required />
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-md-2 control-label">Description</label>
                                    <div class="col-md-4">
                                        <input type="text" id="description" name="description" value="<?php if(!empty($data_area)){echo html_escape($data_area->description);} ?>" placeholder="Please input description" class="form-control alphanumeric" required />
                                    </div>
                                </div>
                                
                                <hr>
                            </div>

                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-2">
                                    <a id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_area';}else{echo 'edit_area';}; ?>&#39;);" class="btn green">Save</a>
                                    <a id="btnCancel" href="<?=base_url('master_data/area/view'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                                <div class="col-md-9"></div>
                            </div>
                          </form>
                     
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>

<?php } ?>

<!-- ini cuma mockup -->
<?php if($sub_menu == 'equipment_and_tools_group' && $mode == 'view'){ ?>

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->

        <!-- END THEME PANEL -->
        <h3 class="page-title"> Equipment & Tools Group
            <!-- <small>first demo</small> -->
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Master Data</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Equipment & Tools Group</span>
                </li>
            </ul>
            <div class="page-toolbar">

            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-list font-dark"></i>
                            <span class="caption-subject bold uppercase">List Equipment & Tools Group</span>


                        </div>
                        <div class="pull-right">
                            <a href="<?=base_url('master_data/equipment_and_tools_group/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                                <tr>
                                    <th class="" style="width: 10%">No.</th>
                                    <th class="">Group Name</th>
                                    <th class="" style="text-align: center; width: 50px">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php
                              $no = 1;
                              foreach ($data_list as $row) { ?>
                                <tr>
                                    <td><?= html_escape($no++); ?></td>
                                    <td><?= html_escape($row['group_name']); ?></td>
                                    <td>
                                        <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <a href="#" onclick="delete_data('form_delete_<?=$row['id']; ?>',base_url+'master_data/delete_data/equipment_and_tools_group/')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a>
                                        </form>
                                    </td>
                                </tr>
                              <?php } ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>

<?php } ?>

<!-- ini cuma mockup -->
<?php if($sub_menu == 'equipment_and_tools_group' && ($mode == 'add')){ ?>

    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->

        <!-- END THEME PANEL -->
        <h3 class="page-title">
            Add New Data
            <!-- <small>first demo</small> -->
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Master Data</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Equipment & Tools Group</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <span>Add New Data</span>
                </li>
            </ul>
            <div class="page-toolbar">

            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-doc font-dark"></i>
                            <span class="caption-subject bold uppercase">Form Add New Data</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body form">
                      
                        <form action="#" id="form" class="form-horizontal" role="form">
                        <input type="hidden" name="id" value=">">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Group Name</label>
                                    <div class="col-md-4">
                                        <input type="text" id="group_name" name="group_name" value="" placeholder="Please input group name" class="form-control" required />
                                    </div>
                                </div>

                                <hr>
                            </div>

                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-2">
                                    <a id="btnSave" onclick="save('add_eat_group')" class="btn green">Save</a>
                                    <a href="<?=base_url('master_data/equipment_and_tools_group/view'); ?>" class="btn btn-danger">Cancel</a>
                                </div>
                                <div class="col-md-9"></div>
                            </div>
                          </form>
                        
                      
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>

<?php } ?>

<?php if($sub_menu == 'sub_area' && $mode == 'view') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Sub Area
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Sub Area</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Sub Area</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?=base_url('master_data/sub_area/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="">No.</th>
                                  <th class="">Area Name</th>
                                  <th class="">Sub Area Name</th>
                                  <th class="">Description</th>
                                  <th class="" style="text-align: center; width: 50px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                            if(!empty($data_sub_area)){
                            $no = 1;
                            foreach ($data_sub_area as $row) { ?>
                              <tr>
                                  <td><?=html_escape($no++); ?></td>
                                  <td><?=html_escape($row['area_name']); ?></td>
                                  <td><?=html_escape($row['sub_area_name']); ?></td>
                                  <td><?=html_escape($row['description']); ?></td>
                                  <td>
                                    
                                    <table>
                                        <tr>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/sub_area/edit'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>   
                                                </form>
                                            </td>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo $row['id']; ?>', base_url+'master_data/delete_data/sub_area/');"><i class="fa fa-close"></i></a>   
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                  </td>
                              </tr>
                            <?php }} ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'sub_area' && ($mode == 'add' || $mode == 'edit')) { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Sub Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                   
                      <form class="form-horizontal" role="form" id="form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" name="id" value="<?php if(!empty($data_sub_area)){echo $data_sub_area->id;}; ?>">
                        <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Area Name</label>
                                  <div class="col-md-4">
                                      <select id="id_area" name="id_area" class="form-control select2">
                                          <option selected="true" disabled="disabled">- Please select -</option>
                                        <?php foreach ($data_area as $da) { ?>
                                          <option value="<?=html_escape($da['id']); ?>" <?php if(!empty($data_sub_area) && html_escape($da['id']) == html_escape($data_sub_area->id_area)){echo 'selected';}; ?> ><?=html_escape($da['area_name']); ?></option>
                                        <?php } ?>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Sub Area Name</label>
                                  <div class="col-md-4">
                                      <input type="text" id="sub_area_name" name="sub_area_name" maxlength="150" value="<?php if(!empty($data_sub_area)){ echo html_escape($data_sub_area->sub_area_name);} ?>" placeholder="Please input sub area name" class="name form-control" required />
                                  </div>
                              </div>
                             
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Description</label>
                                  <div class="col-md-4">
                                      <input type="text" id="description" name="description" value="<?php if(!empty($data_sub_area)){ echo html_escape($data_sub_area->description);} ?>" placeholder="Please input description" class="name form-control" required />
                                  </div>
                              </div>
                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a id="btnSS" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_sub_area';}else{echo 'edit_sub_area';}; ?>&#39;);" class="btn green">Save</a>
                                  <a href="<?=base_url('master_data/sub_area/view'); ?>" id="btnCancel" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                          <!-- <div class="form-actions"> -->

                          <!-- </div> -->
                      </form>
                      
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if($sub_menu == 'user' && $mode == 'view') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> User
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>User</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List User</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?=base_url('master_data/user/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                        
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="">No.</th>
                                  <th class="">Username</th>
                                  <th class="">User Type</th>
                                  <th class="">No. HP</th>
                                  <th class="">Description</th>
                                  <th class="">Email</th>
                                  <th class="">Password</th>
                                  <th class="" style="text-align: center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 1;
                            foreach ($data_user as $row) { 
                                if($row['id'] !== $this->session->userdata('id') && $row['user_type'] !== 'superadmin'){
                              ?>
                              <tr>
                                  <td><?=$no++; ?></td>
                                  <td><?= html_escape($row['username']); ?></td>
                                  <td style="width: 150px">
                                    <?php 

                                        if($row['user_type'] == 'admin_toss'){
                                            echo 'Admin TOSS';
                                        }else if($row['user_type'] == 'admin_project'){
                                            echo 'Admin Project';
                                        }else if($row['user_type'] == 'konsultan'){
                                            echo 'Konsultan';
                                        }else if($row['user_type'] == 'dealer'){
                                            echo 'Dealer';
                                        }else if($row['user_type'] == 'tam'){
                                            echo 'TAM';
                                        }

                                    ?>
                                      
                                  </td>
                                  <td><?= html_escape($row['no_hp']); ?></td>
                                  <td><?= html_escape($row['description']); ?></td>
                                  <td><?= html_escape($row['email']); ?></td>
                                  <td><?= html_escape($row['password']); ?></td>
                                  <td style="text-align: center; width: 80px">
                                    <table>
                                        <tr>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/user/edit'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>   
                                                </form>
                                            </td>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo $row['id']; ?>', base_url+'master_data/delete_data/user/');"><i class="fa fa-close"></i></a>   
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                  </td>
                              </tr>
                            <?php 
                                }
                            } 
                            ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'user' && ($mode == 'add' || $mode == 'edit')) { ?>
 
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">User</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">

                    <form id="form" class="form-horizontal" role="form">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="id" value="<?php if(!empty($data_user)){echo $data_user->id;} ?>">
                            <div class="form-body">
                                  <div class="form-group">
                                      <label class="col-md-2 control-label">Username</label>
                                      <div class="col-md-4">
                                          <input id="username" name="username" onpaste="return false" type="text" value="<?php if(!empty($data_user)){echo html_escape($data_user->username);} ?>" placeholder="Please Fill Username" class="name form-control" maxlength="100" required />
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-2 control-label">User Type</label>
                                      <div class="col-md-4">
                                          <select name="user_type" id="user_type" class="form-control">
                                              <option selected="true" disabled="disabled">- Please Select -</option>
                                              <option value="admin_toss" <?php if(!empty($data_user) && $data_user->user_type == 'admin_toss'){ echo 'selected'; }?> >Admin TOSS</option>
                                              <option value="admin_project" <?php if(!empty($data_user) && $data_user->user_type == 'admin_project'){ echo 'selected'; }?> >Admin Project</option>
                                              <option value="konsultan" <?php if(!empty($data_user) && $data_user->user_type == 'konsultan'){ echo 'selected'; }?>  >Konsultan</option>
                                              <option value="dealer" <?php if(!empty($data_user) && $data_user->user_type == 'dealer'){ echo 'selected'; }?>  >Dealer</option>
                                              <option value="tam" <?php if(!empty($data_user) && $data_user->user_type == 'tam'){ echo 'selected'; }?>  >TAM</option>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-2 control-label">No. HP</label>
                                      <div class="col-md-4">
                                          <input id="no_hp" name="no_hp" type="text" onpaste="return false" value="<?php if(!empty($data_user)){echo html_escape($data_user->no_hp);} ?>" placeholder="Please Fill Phone Number" class="form-control numeric" maxlength="50" required />
                                      </div>
                                  </div>
                                 
                                  <div class="form-group">
                                      <label class="col-md-2 control-label">Description</label>
                                      <div class="col-md-4">
                                          <input id="description" name="description" onpaste="return false" type="text" value="<?php if(!empty($data_user)){echo html_escape($data_user->description);} ?>" placeholder="Please Fill Main Description" class="name form-control" required />
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <label class="col-md-2 control-label">Email</label>
                                      <div class="col-md-4">
                                          <input id="email" name="email" onpaste="return false" type="email" value="<?php if(!empty($data_user)){echo html_escape($data_user->email);} ?>" placeholder="Please Fill Main Email" class="name form-control email" maxlength="50" required />
                                      </div>
                                  </div>

                                  <div class="form-group">
                                        <label class="col-md-2 control-label">Password</label>
                                        <div class="col-md-4">
                                            <input id="password" name="password" type="text" value="<?php if(!empty($data_user)){echo html_escape($data_user->password);} ?>" placeholder="Please Fill Password" class="name form-control" max="200" required />
                                        </div>
                                  </div>
                                  

                                  <hr>
                            </div>
                            
                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_user';}else{echo 'edit_user';}; ?>&#39;);" class="btn green">Save</a>
                                  <a href="<?=base_url('master_data/user/view'); ?>" id="btnCancel" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                      </form>
                   
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'view') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Project
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Project</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Project</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?=base_url('master_data/project/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th width="30px" class="">No.</th>
                                  <th class="">Nama Outlet</th>
                                  <th class="">Main Dealer</th>
                                  <th class="">Fungsi Outlet</th>
                                  <th class="">Type Pembangunan</th>
                                  <th class="">Alamat</th>
                                  <th class="">Jenis EPM</th>
                                  <th class="none">EPM 50%</th>
                                  <th class="none">EPM 75%</th>
                                  <th class="none">EPM 100%</th>
                                  <th class="">Target Otorisasi</th>
                                  <th class="" style="text-align: center; width: 220px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                              $no = 1;
                              foreach ($data_project as $row) {
                            ?>
                              <tr>
                                  <td><?=html_escape($no++); ?></td>
                                  <td><?=html_escape($row['nama_outlet']); ?></td>
                                  <td><?=html_escape($row['main_dealer']); ?></td>
                                  <td><?=html_escape($row['fungsi_outlet']); ?></td>
                                  <td><?=html_escape($row['type_pembangunan']); ?></td>
                                  <td><?=html_escape($row['alamat']); ?></td>
                                  <td><?php if($row['jenis_epm'] == '50'){echo '50%';}elseif($row['jenis_epm'] == '75'){echo '75%';}else{echo '100%';}; ?></td>
                                  <td><?=html_escape(date('d M Y', strtotime($row['epm_50_date']))); ?></td>
                                  <td><?=html_escape(date('d M Y', strtotime($row['epm_75_date']))); ?></td>
                                  <td><?=html_escape(date('d M Y', strtotime($row['epm_100_date']))); ?></td>
                                  <td><?=html_escape(date('d M Y', strtotime($row['target_otorisasi']))); ?></td>

                                  <td>
                                    <div class="btn-group">
                                    <a href="<?=base_url("master_data/project/edit/").$row['id'] ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a href="<?=base_url('master_data/project/set_area/').$row['id']; ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Set Area"><i class="fa fa-tags"></i></a>

                                    <!-- <a href="<?=base_url('master_data/project_manpower/'); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Manpower"><i class="fa fa-user"></i></a> -->

                                    <!-- <a href="<?=base_url('master_data/project/equipment_and_tools_configuration/').$row['id']; ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Equipment & Tools Configuration"><i class="fa fa-gavel"></i></a> -->

                                    <div class="btn-group">
                                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" href="javascript:;"> <i class="fa fa-gavel"></i>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu" style="width: 50px">
                                            <li data-toggle="tooltip" title="Equipment & Tools 50% Configuration">
                                                <a href="<?=base_url('master_data/project/equipment_and_tools_configuration_50/').$row['id']; ?>" style="width: 100%"><i class="fa fa-gavel"></i> 50% </a>
                                            </li>
                                            <li data-toggle="tooltip" title="Equipment & Tools 75% Configuration">
                                                <a href="<?=base_url('master_data/project/equipment_and_tools_configuration_75/').$row['id']; ?>" style="width: 100%"><i class="fa fa-gavel"></i> 75% </a>
                                            </li>
                                            <li data-toggle="tooltip" title="Equipment & Tools 100% Configuration">
                                                <a href="<?=base_url('master_data/project/equipment_and_tools_configuration_100/').$row['id']; ?>" style="width: 100%"><i class="fa fa-gavel"></i> 100% </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <a href="<?=base_url("master_data/project/kpd_picture/").$row['id']; ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="KPD Picture"><i class="fa fa-image"></i></a>
                                    <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                        <a href="#" onclick="delete_data('form_delete_<?=$row['id']; ?>',base_url+'master_data/delete_data/project/')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a>
                                    </form>

                                  </td>
                              </div>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>
    <script type="text/javascript">
        function lock(e,id,type){
            checkbox=$(e);
            checked=checkbox.prop('checked');
            $(document).css('cursor','wait');
            $.post("<?=base_url('master_data/lock_user')?>",{'type':type,'checked':checked,'id':id})
            .done(function(){
                swal("Saved");
                $(document).css('cursor','default');
            }).fail(function(){
                swal("Error");
                $(document).css('cursor','default');
                checkbox.prop('checked',!checked);
            });
        }
    </script>
<?php } ?>

<?php if ($sub_menu == 'project' && ($mode == 'add' || $mode == 'edit')) { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                    <form action="javascript:;" id="formPhotoOutlet" name="formPhotoOutlet" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="form-body">
                        <div class="form-group">
                          <div class="col-sm-2">
                            <label for="imgInput2" class="control-label pull-right">Outlet</label>
                          </div>
                          <div class="col-sm-8">
                            <div id="formHeader2" runat="server" class="col-xs-4">
                                <a onclick="$('#imgInput2').click();"><img id="imgHdr2" class="img-thumbnail" src="<?=!empty($data_project->outlet_pic_file_path) ? html_escape($data_project->outlet_pic_file_path) : base_url('upload/outlet/default.jpg');?>" alt="your image" width="200" height="130"/></a>                                            
                                <input type="text" name="imgInput2Path" value="" style="display: none">
                                <input type='file' id="imgInput2" name="imgInput2" accept=".png, .jpg, .jpeg" style="display: none"/>
                            </div>
                          </div>
                        </div>
                        </div>
                        <input type="submit" id="submitPhotoOutlet" name="" style="display: none">
                    </form>
                  
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" name="outlet_file_name" id="outlet_file_name">
                            <input type="hidden" name="outlet_file_path" id="outlet_file_path">

                            <input type="hidden" name="outlet_file_true_path" id="outlet_file_true_path">
                          <input type="hidden" id="id" name="id" value="<?php if(!empty($data_project)){echo $data_project->id;}; ?>">
                          <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Nama Outlet</label>
                                  <div class="col-md-4">
                                      <input id="nama_outlet" name="nama_outlet" type="text" value="<?php if(!empty($data_project)){echo html_escape($data_project->nama_outlet);}; ?>" placeholder="please fill outlate name" class="name form-control" style="margin-left: 15px" required />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Main Dealer</label>
                                  <div class="col-md-4">
                                      <input id="main_dealer" name="main_dealer" type="text" value="<?php if(!empty($data_project)){echo html_escape($data_project->main_dealer);}; ?>" placeholder="please fill main dealer" class="name form-control" style="margin-left: 15px" required />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Fungsi Outlet</label>
                                  <div class="col-md-2">
                                      <select id="fungsi_outlet" name="fungsi_outlet" style="margin-left: 15px" class="form-control">
                                            <option selected="true" disabled="disabled">- please select -</option>
                                            <option value="VSP" <?php if(!empty($data_project) && $data_project->fungsi_outlet == 'VSP'){echo 'selected';}; ?> >VSP</option>
                                            <option value="V" <?php if(!empty($data_project) && $data_project->fungsi_outlet == 'V'){echo 'selected';}; ?> >V</option>
                                            <option value="SP" <?php if(!empty($data_project) && $data_project->fungsi_outlet == 'SP'){echo 'selected';}; ?> >SP</option>
                                            <option value="VSPBP" <?php if(!empty($data_project) && $data_project->fungsi_outlet == 'VSPBP'){echo 'selected';}; ?> >VSPBP</option>
                                            <option value="BP" <?php if(!empty($data_project) && $data_project->fungsi_outlet == 'BP'){echo 'selected';}; ?> >BP</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Type Pembangunan</label>
                                  <div class="col-md-2">
                                      <select name="type_pembangunan" id="type_pembangunan" style="margin-left: 15px" class="form-control">
                                          <option selected="true" disabled="disabled">- please select -</option>
                                          <option value="New" <?php if(!empty($data_project) && $data_project->type_pembangunan == 'New'){echo 'selected';}; ?> >New</option>
                                          <option value="Renovation" <?php if(!empty($data_project) && $data_project->type_pembangunan == 'Renovation'){echo 'selected';}; ?> >Renovation</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Alamat</label>
                                  <div class="col-md-4">
                                      <input id="alamat" name="alamat" type="text" style="margin-left: 15px" value="<?php if(!empty($data_project)){echo html_escape($data_project->alamat);}; ?>" placeholder="please fill adderss" class="name form-control" required />
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Jenis EPM</label>
                                  <div class="col-md-2">
                                      <select name="jenis_epm" id="jenis_epm" style="margin-left: 15px" class="form-control">
                                          <option selected="true" disabled="disabled">- please select -</option>
                                          <option value="50" <?php if(!empty($data_project) && $data_project->jenis_epm == '50'){echo 'selected';}; ?> >50%</option>
                                          <option value="75" <?php if(!empty($data_project) && $data_project->jenis_epm == '75'){echo 'selected';}; ?> >75%</option>
                                          <option value="100" <?php if(!empty($data_project) && $data_project->jenis_epm == '100'){echo 'selected';}; ?> >100%</option>
                                       
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">EPM 50%</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="epm_50" name="epm_50" style="margin-left: 15px" placeholder="please choose date" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_project)){echo html_escape(date('d M Y', strtotime($data_project->epm_50_date)));}; ?>" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">EPM 75%</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="epm_75" name="epm_75" style="margin-left: 15px" placeholder="please choose date" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_project)){echo date('d M Y', strtotime($data_project->epm_75_date));}; ?>" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">EPM 100%</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="epm_100" name="epm_100" style="margin-left: 15px" placeholder="please choose date" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_project)){echo html_escape(date('d M Y', strtotime($data_project->epm_100_date)));}; ?>" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-2 control-label">Target Otorisasi</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="target_otorisasi" name="target_otorisasi" style="margin-left: 15px" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_project)){echo html_escape(date('d M Y', strtotime($data_project->target_otorisasi)));}; ?>" placeholder="please choose date" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Telp/Fax</label>
                                  <div class="col-md-4">
                                      <input id="telp_fax" name="telp_fax" type="text" style="margin-left: 15px" placeholder="please fill telp/fax" class="fax form-control" value="<?php if(!empty($data_project)){echo html_escape($data_project->telp_fax);}; ?>" required />
                                  </div>
                              </div>
                             <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Otorisasi</label>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input id="tanggal_otorisasi" name="tanggal_otorisasi" style="margin-left: 15px" class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php if(!empty($data_project)){echo html_escape(date('d M Y', strtotime($data_project->tanggal_otorisasi)));}; ?>" placeholder="please choose date" readonly/>
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>

                                </div>
                              </div>
                              <hr>                             
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Assign User Level 1</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">
                                        <?php
                                        $selected=(isset($data_project->id_user_level_1))?$data_project->id_user_level_1:0;
                                        $extra=[
                                            'class'=>'form-control select2',
                                            'id'=>'assign_user_level_1'];
                                        echo my_form_dropdown('assign_user_level_1',$data_user,$selected,$extra,'');
                                        ?>
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Assign User Level 2</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">
                                        <?php
                                        $selected=(isset($data_project->id_user_level_2))?$data_project->id_user_level_2:0;
                                        $extra=[
                                            'class'=>'form-control select2',
                                            'id'=>'assign_user_level_2'];
                                        echo my_form_dropdown('assign_user_level_2',$data_user,$selected,$extra,'');
                                        ?>
                                        </div>
                                  </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-2 control-label">Assign User Level 3</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">
                                        <?php
                                        $selected=(isset($data_project->id_user_level_3))?$data_project->id_user_level_3:0;
                                        $extra=[
                                            'class'=>'form-control select2',
                                            'id'=>'assign_user_level_3'];
                                        echo my_form_dropdown('assign_user_level_3',$data_user,$selected,$extra,'');
                                        ?>
                                        </div>
                                  </div>
                              </div>

                              <hr>
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Template</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">                                              
                                          <select required name="template_set" id="template_set" class="form-control select2" <?php if($mode=='edit'){echo 'disabled="true"';}; ?> >
                                            <option selected="true" disabled="disabled">- belum ada template yang terpilih -</option>
                                            <?php if (!empty($data_template)): ?>
                                                <?php foreach($data_template as $row):?>                                                
                                                <option value="<?=$row['id'] ?>" <?php if(isset($data_project->id_project_template) && $data_project->id_project_template == $row['id']){echo 'selected';}; ?> ><?=html_escape(ucwords($row['template_name'])); ?></option>                                
                                                <?php endforeach;?>
                                            <?php endif;?>
                                          </select>
                                        </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Template Set Date</label>
                                  <div class="col-md-4">
                                        <div style="margin-left: 15px">
                                            <input type="text" name="template_set_date" id="template_set_date" class="form-control" readonly="" value="<?=html_escape(isset($data_project->template_set_date))?html_escape(date('d M Y H:i:s',strtotime($data_project->template_set_date))):''?>">
                                        </div>
                                  </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">
                                        Lock User
                                    </label>
                                    <div class="col-md-4" style="padding-left: 30px">
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" value="1" class="md-check" name="lock_user_level_1" 
                                            <?=(((isset($data_project)) and ($data_project->lock_user_level_1=='1'))?'checked':'')?>/>
                                            Level 1
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" value="1" class="md-check" name="lock_user_level_2" 
                                            <?=(((isset($data_project)) and ($data_project->lock_user_level_2=='1'))?'checked':'')?>/>
                                            Level 2
                                            </label>
                                        </div>

                                        <div class="checkbox">
                                          <label>
                                            <input type="checkbox" value="1" class="md-check" name="lock_user_level_3" 
                                            <?=(((isset($data_project)) and ($data_project->lock_user_level_3=='1'))?'checked':'')?>/>
                                            Level 3
                                            </label>
                                        </div>
                                    </div>
                          </div>
                              <hr>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                <!-- <button type="button" id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_project';}else{echo 'edit_project';}; ?>&#39;);" class="btn green">Save</button> -->
                                <!-- <button type="button" id="btnSave" onclick="$('#submitPhotoKpd').click();" class="btn green">Save</button> -->
                                <button type="button" id="btnSave" onclick="$('#submitPhotoOutlet').click();" class="btn green">Save</button>
                                <a id="btnCancel" href="<?=base_url('master_data/project/view'); ?>" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                          <!-- <div class="form-actions"> -->

                          <!-- </div> -->
                      </form>
                    
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>
    <script type="text/javascript">
        function time(){
            month = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var dt = new Date();
            date=dt.getDate()+" "+month[dt.getMonth()]+" "+dt.getFullYear()+" "+dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds();
            $("#template_set_date").val(date);
        }
        <?php if(!isset($data_project->template_set_date)):?>
            setInterval(time,1000);
        <?php endif;?>
    </script>
<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'set_area') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Set Area
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Set Area</span>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <!-- <span class="caption-subject bold uppercase">List Area (Nama Outlet : XXX)</span> -->
                          <span class="caption-subject bold uppercase">List Area <?=html_escape($data_current_project->nama_outlet); ?></span>

                      </div>
                      <div class="pull-right">
                        <a href="<?=base_url('master_data/project/view'); ?>" class="btn btn-danger btn-md">Back</a>
                       
                      </div>
                  </div>
                  <div class="portlet-body">
                    <!-- Notifikasi -->
                        <?php if ($info = $this->session->flashdata('info')) { ?>
                          <div class='alert alert-success alert-dismissible'>
                              <?=$info; ?>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span>
                              </button>
                          </div>
                        <?php } ?>

                      <!-- /Notifikasi -->
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="">No.</th>
                                  <th class="">Area Name</th>
                                  <th class="">Sub Area Name</th>
                                  <th class="">Sub Area Description</th>
                                  <th class="">Position</th>
                                  <th class="" style="text-align: center; width: 130px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                <?php if($list_area->num_rows()>0):?>  
                                    <?php $n=0;?>        
                                    <?php foreach($list_area->result() as $row):?>       
                                        <?php $n++;?>                                               
                                        <tr>
                                            <td><?=$n;?></td>
                                            <td><?=html_escape($row->area_name);?></td>
                                            <td><?=html_escape($row->sub_area_name);?></td>
                                            <td><?=html_escape($row->sub_area_description);?></td>
                                            <td><?=html_escape($row->position);?></td>
                                            <td class="text-center">

                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" title="EPM Configuration" href="javascript:;"> EPM
                                                        <i class="fa fa-angle-down"></i>
                                                    </a>
                                                    <ul class="dropdown-menu" style="width: 50px">
                                                        <li data-toggle="tooltip" title="EPM 50% Configuration">
                                                            <a href="<?=base_url('master_data/project/sub_area_epm50/'.$row->id); ?>" style="width: 100%">50%</a>
                                                        </li>
                                                        <li data-toggle="tooltip" title="EPM 75% Configuration">
                                                            <a href="<?=base_url('master_data/project/sub_area_epm75/'.$row->id); ?>" style="width: 100%">75%</a>
                                                        </li>
                                                        <li data-toggle="tooltip" title="EPM 100% Configuration">
                                                            <a href="<?=base_url('master_data/project/sub_area_epm100/'.$row->id); ?>" style="width: 100%">100%</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif; ?>                                  
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'sub_area_epm50') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        Sub Area EPM 50% Configuration 
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                    Sub Area EPM 50% Configuration 
                    
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">                                                                
                  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                  <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                  <i class="icon-doc font-dark"></i>
                                  <span class="caption-subject bold uppercase">
                                    <!-- <?php if($mode == 'sub_area_epm50' || $mode == 'sub_area_epm75' || $mode == 'sub_area_epm100'){ ?> -->
                                        Form Configuration
                                    <!-- <?php } ?> -->

                                    &nbsp;(<?= xss_clean($data_project_area[0]['nama_outlet']).' - '.xss_clean($data_project_area[0]['area_name']).' - '.xss_clean($data_project_area[0]['sub_area_name']) ?>)
                                    <input type="hidden" id="jenis_epm" value="<?=$jenis_epm ?>">
                                    <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                                    <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">
                                  </span>
                                </div>
                            <div class="pull-right">

                                <a id="btnCancel" href="<?=base_url('master_data/project/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                          <form class="form-horizontal" role="form">
                              <div class="form-body">
                                
                                    <div class="form-group">
                                      <label class="col-md-2 control-label">Item Check Model</label>
                                      <div class="col-md-3">
                                            <select disabled class="form-control" id="item_checklist_model" name="item_checklist_model">
                                            <option value="Model 1" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                            </select>  
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="col-md-2 control-label">Item Check</label>
                                        <div class="col-md-8">
                                              <table id="tbic" class="table table-bordered">
                                                <thead>
                                                  <tr style="background-color: #ffff99">
                                                    <th style="width: 60px">Position</th>
                                                    <th>Item Name</th>
                                                    <th>Kriteria</th>
                                                  </tr>
                                                </thead>
                                                <tbody id="ic">
                                                    <?php foreach ($data_config_ic as $row) { ?>
                                                        <input id="idic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                        <tr class="rowic" id="removeRowIc<?=$row['position']; ?>">
                                                          <td id="p<?=$row['position']; ?>"><?=html_escape($row['position']); ?></td>
                                                          <td id="i<?=$row['position']; ?>"><?=html_escape($row['item_name']); ?></td>
                                                          <td id="k<?=$row['position']; ?>"><?=html_escape($row['kriteria']); ?></td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                              </table>
                                        </div>
                                    </div>

                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Kriteria Pekerjaan</label>
                                        
                                        <div class="col-md-8">
                                          
                                            <table id="tbkp" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Kriteria Pekerjaan</th>
                                                </tr>
                                              </thead>
                                              <tbody id="kp">
                                              <?php foreach ($data_config_kp as $row) { ?>
                                                  <input id="idkp<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowkp" id="removeRowKp<?=$row['position']; ?>">
                                                    <td id="pkp<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="ikp<?=$row['position']; ?>"><?=$row['item_kriteria_pekerjaan']; ?></td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                          
                                        </div>
                                    </div>

                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Minreq</label>
                                       
                                        <div class="col-md-8">
                                         
                                            <table id="tbminreq" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Minreq</th>
                                                </tr>
                                              </thead>
                                              <tbody id="minreq">
                                              <?php foreach ($data_config_minreq as $row) { ?>
                                                  <input id="idminreq<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowminreq" id="removeRowMinreq<?=$row['position']; ?>">
                                                    <td id="pminrek<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="iminreq<?=$row['position']; ?>"><?=$row['item_minreq']; ?></td>
                                                  
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                          
                                        </div>
                                    </div>


                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Keterangan</label>
                                       
                                        <div class="col-md-8">
                                          
                                            <table id="tbket" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Keterangan</th>
                                                </tr>
                                              </thead>
                                              <tbody id="ket">
                                              <?php foreach ($data_config_ket as $row) { ?>
                                                  <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                    <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                         
                                        </div>
                                    </div>
                                  
                                  <hr>
                              </div>
                          </form>
                        </div>
                      </div>
                      <!-- END EXAMPLE TABLE PORTLET-->

          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'sub_area_epm75') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        
            Sub Area EPM 75% Configuration 
        
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                        Sub Area EPM 75% Configuration 
                 
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">

                <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                  <i class="icon-doc font-dark"></i>
                                  <span class="caption-subject bold uppercase">
                                    
                                    Form Configuration 
                                    

                                    &nbsp;<?= $data_project_area[0]['template_name'].' - '.$data_project_area[0]['area_name'].' - '.$data_project_area[0]['sub_area_name'] ?>
                                    <input type="hidden" id="jenis_epm" value="<?=$jenis_epm?>">
                                    <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                                    <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">
                                  </span>
                              </div>
                                <div class="pull-right">

                                <a id="btnCancel" href="<?=base_url('master_data/project/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Back</a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                              <form class="form-horizontal" role="form">
                                  <div class="form-body">
                                    
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Item Check Model</label>
                                          <div class="col-md-3">
                                            <select disabled class="form-control" id="item_checklist_model" name="item_checklist_model">
                                            <option value="Model 1" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                            </select>     
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Item Check</label>
                                          
                                            <div class="col-md-8">
                                                  <table id="tbic" class="table table-bordered">
                                                    <thead>
                                                      <tr style="background-color: #ffff99">
                                                        <th style="width: 60px">Position</th>
                                                        <th>Item Name</th>
                                                        <th>Kriteria</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody id="ic">
                                                    <?php foreach ($data_config_ic as $row) { ?>
                                                        <input id="idic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                        <tr class="rowic" id="removeRowIc<?=$row['position']; ?>">
                                                          <td id="p<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                          <td id="i<?=$row['position']; ?>"><?=$row['item_name']; ?></td>
                                                          <td id="k<?=$row['position']; ?>"><?=$row['kriteria']; ?></td>
                                                        </tr>
                                                      <?php } ?>
                                                    </tbody>
                                                  </table>
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Kriteria Pekerjaan</label>
                                           
                                            <div class="col-md-8">
                                              
                                                <table id="tbkp" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Kriteria Pekerjaan</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="kp">
                                                  <?php foreach ($data_config_kp as $row) { ?>
                                                      <input id="idkp<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowkp" id="removeRowKp<?=$row['position']; ?>">
                                                        <td id="pkp<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="ikp<?=$row['position']; ?>"><?=$row['item_kriteria_pekerjaan']; ?></td>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Minreq</label>
                                           
                                            <div class="col-md-8">
                                             
                                                <table id="tbminreq" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Minreq</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="minreq">
                                                  <?php foreach ($data_config_minreq as $row) { ?>
                                                      <input id="idminreq<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowminreq" id="removeRowMinreq<?=$row['position']; ?>">
                                                        <td id="pminrek<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="iminreq<?=$row['position']; ?>"><?=$row['item_minreq']; ?></td>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">TPS Line</label>
                                            <div class="col-md-8">
                                             
                                                <table id="tbTpsLine" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item TPS Line</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="tpsLine">
                                                  <?php foreach ($data_config_tps_line as $row) { ?>
                                                      <input id="idtps<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="datatpsline" id="removeRowTpsLine<?=$row['position']; ?>">
                                                        <td id="ptps<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="itps<?=$row['position']; ?>"><?=$row['item_tps_line']; ?></td>
                                                        
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Keterangan</label>
                                           
                                            <div class="col-md-8">
                                              
                                                <table id="tbket" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Keterangan</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="ket">
                                                  <?php foreach ($data_config_ket as $row) { ?>
                                                      <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                        <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                      </tr>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                             
                                            </div>
                                        </div>
                                      
                                      <hr>
                                  </div>
                              </form>
                            </div>
                      </div>

          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'sub_area_epm100') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        Sub Area EPM 100% Configuration 
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                    Sub Area EPM 100% Configuration 
                    
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              
          <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">
                            
                            Form Configuration 
                           

                             &nbsp;<?= $data_project_area[0]['template_name'].' - '.$data_project_area[0]['area_name'].' - '.$data_project_area[0]['sub_area_name'] ?>
                            <input type="hidden" id="jenis_epm" value="<?=$jenis_epm?>">
                            <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                            <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">
                          </span>
                        </div>
                        <div class="pull-right">

                                <a id="btnCancel" href="<?=base_url('master_data/project/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Back</a>

                        </div>                      
                    </div>
                    <div class="portlet-body form">
                      
                      <form class="form-horizontal" role="form">
                          <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Pictures</label>
                                    
                                    <div class="col-md-8">
                                        <table id="tbpic" class="table table-bordered">
                                          <thead>
                                            <tr style="background-color: #ffff99">
                                              <th style="width: 60px">Position</th>
                                              <th>Picture name</th>
                                            </tr>
                                          </thead>
                                          <tbody id="pic">
                                            <?php foreach ($data_config_pic as $row) { ?>
                                                <input id="idpic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowpic" id="removeRowPic<?=$row['position']; ?>">
                                                <td id="ppic<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="ipic<?=$row['position']; ?>"><?=$row['item_pic']; ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Model Kriteria Min.</label>
                                  <div class="col-md-3">
                                    <select disabled class="form-control" id="item_checklist_model" name="item_checklist_model">
                                    <option value="Model 1" <?=((!empty($data_config_km) and $data_config_km[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_km) and $data_config_km[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                    </select>     
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-md-2 control-label">Kriteria Minimum</label>
                                  
                                    <div class="col-md-8">
                                          <table id="tbkm" class="table table-bordered">
                                            <thead>
                                              <tr style="background-color: #ffff99">
                                                <th style="width: 60px">Position</th>
                                                <th>Item Kriteria Minimum</th>
                                              </tr>
                                            </thead>
                                            <tbody id="km">
                                            <?php foreach ($data_config_km as $row) { ?>
                                                <input id="idkm<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowkm" id="removeRowKm<?=$row['position']; ?>">
                                                <td id="pkm<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="ikm<?=$row['position']; ?>"><?=$row['item_kriteria_minimum']; ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                          </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Keterangan</label>
                                    
                                    <div class="col-md-8">
                                        <table id="tbket" class="table table-bordered">
                                          <thead>
                                            <tr style="background-color: #ffff99">
                                              <th style="width: 60px">Position</th>
                                              <th>Item Keterangan</th>
                                            </tr>
                                          </thead>
                                          <tbody id="ket">
                                            <?php foreach ($data_config_ket as $row) { ?>
                                                <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                </tr>
                                                </tr>
                                            <?php } ?>
                                          </tbody>
                                        </table>
                                    </div>
                                </div> 
                              
                                
                              
                              <hr>
                          </div>

                      </form>
                  </div>
              </div>

          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'add_set_area') { ?>
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Add New Data
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Add New Data</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">Form Add New Data</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                          <input type="hidden" name="id_project" id="id_project" value="<?=$id_project ?>">
                          <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Area Name</label>
                                  <div class="col-md-6">
                                    <select name="id_area" id="id_area" onchange="filteringsubarea(this);" class="form-control id-area select2">
                                      <option disabled="" selected="">-- Select --</option>
                                        <?php if($list_area->num_rows()>0):?>
                                            <?php foreach($list_area->result() as $row):?>
                                                <option value='<?=$row->id?>'><?=$row->area_name?></option>
                                            <?php endforeach;?>
                                        <?php endif; ?>                                        
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Sub Area Name</label>
                                  <div class="col-md-6">
                                    <select name="id_sub_area" id="id_sub_area" class="form-control id-sub-area select2">                                     
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Position</label>
                                  <div class="col-md-2">
                                    <input type="number" min="1" class="form-control position" name="position">
                                  </div>
                              </div>
                              
                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a href="javascript:;" id="btnSave" onclick="save_project_area('project')" class="btn green">Save</a>
                                  <a id="btnCancel" href="<?=base_url('master_data/project/set_area/'.$id_project); ?>" class="btn btn-danger">Back</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                          
                      </form>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && ($mode == 'equipment_and_tools_configuration_50' || $mode == 'equipment_and_tools_configuration_75' || $mode == 'equipment_and_tools_configuration_100')) { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Equipment & Tools <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> Configuration
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Equipment & Tools <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> Configuration</span>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">Equipment & Tools Configuration <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> -  &nbsp;<?= $data_project[0]['nama_outlet'] ?></span>

                      </div>
                      <div class="pull-right">
                        
                      </div>
                  </div>
                  <div class="portlet-body">
                        <form class="form-horizontal" role="form">
                           
                            <div class="form-body">
                            
                            <input type="hidden" name="id_project" id="id_project" value="<?=$data_project[0]['id']; ?>">
                            <input type="hidden" name="jenis_epm" id="jenis_epm" value="<?=$jenis_epm; ?>">
                            
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Equipment & Tools</label>
                                    <div class="col-md-8">
                                        <table id="tbeat" class="table table-bordered">
                                        <thead>
                                          <tr style="background-color: #ffff99">
                                            <th style="width: 60px">Position</th>
                                            <th>Group Name</th>
                                            <th>Item Name</th>
                                          </tr>
                                        </thead>
                                            <tbody id="eat">
                                            <?php foreach ($data_config_eat as $row) { ?>
                                                <tr class="roweat" id="removeRowEat<?=$row['position']; ?>">
                                                  <td id="p<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                  <td id="g<?=$row['position']; ?>"><?=$row['group_name']; ?></td>
                                                  <td id="i<?=$row['position']; ?>"><?=$row['item_name']; ?></td>
                                                 
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-md-5 col-md-offset-2">
                                      <a id="btnSave" onclick="saveEat();" class="btn green" style="display: none">Save</a>
                                      <a id="btnSave" href="<?=base_url('master_data/project/view'); ?>" class="btn btn-danger">Back</a>
                                  </div>
                                  <div class="col-md-9"></div>
                                </div>

                           

                            
                            </div>

                            
                        </form>
                     
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'add_set_project_equipment_and_tools') { ?>
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Add New Data
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Equipment & Tools</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Add New Data</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">Form Add New Data</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                          <input type="hidden" name="id_project" value="<?=$id_project ?>">
                          <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Group Name</label>
                                  <div class="col-md-6">
                                    <select name="id_area" id="id_group" class="form-control">
                                      <option disabled="" selected="">-- Select --</option>
                                      <?php foreach ($data_group as $row) { ?>
                                        <option value='<?=$row['id'] ?>'><?=$row['group_name'] ?></option>
                                      <?php } ?>
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Item Name</label>
                                  <div class="col-md-6">
                                    <select name="id_sub_area" id="id_item" class="form-control">
                                      <option data-group='SHOW' value='' disabled="" selected="">-- Select --</option>
                                      <?php foreach ($data_item as $row) { ?>
                                        <option value='<?=$row['id'] ?>'><?=$row['item_name'] ?></option>
                                      <?php } ?>
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Position</label>
                                  <div class="col-md-2">
                                    <input type="number" class="form-control position" name="position">
                                  </div>
                              </div>
                              
                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a href="javascript:;" id="btnSave" onclick="save('add_project_equipment_and_tools')" class="btn green">Save</a>
                                  <a id="btnCancel" href="<?=base_url('master_data/set_area/'.$id_project); ?>" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                      </form>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project' && $mode == 'kpd_picture') { ?>

    <style type="text/css">
  
    .hovereffect {
        width: 100%;
        height: 100%;
        float: left;
        overflow: hidden;
        position: relative;
        text-align: center;
        cursor: default;
    }
    .hovereffect .overlay {
        width: 100%;
        position: absolute;
        overflow: hidden;
        left: 0;
        top: auto;
        bottom: 0;
        padding: 1em;
        height: 4.75em;
        background: #79FAC4;
        color: #3c4a50;
        -webkit-transition: -webkit-transform 0.35s;
        transition: transform 0.35s;
        /*-webkit-transform: translate3d(0,100%,0);*/
        /*transform: translate3d(0,100%,0);*/
        visibility: hidden;

    }

    .hovereffect img {
        display: block;
        position: relative;
        -webkit-transition: -webkit-transform 0.35s;
        transition: transform 0.35s;
        width: 100%;
        height: 200px;
    }

    .hovereffect:hover img {
    /*-webkit-transform: translate3d(0,-10%,0);*/
        /*transform: translate3d(0,-10%,0);*/
    }

    .hovereffect h2 {
        text-transform: uppercase;
        color: #fff;
        text-align: center;
        position: relative;
        font-size: 17px;
        padding: 10px;
        background: rgba(0, 0, 0, 0.6);
        float: left;
        margin: 0px;
        display: inline-block;
    }

    .hovereffect a.info {
        display: inline-block;
        text-decoration: none;
        padding: 7px 14px;
        text-transform: uppercase;
        color: #fff;
        border: 1px solid #fff;
        margin: 50px 0 0 0;
        background-color: transparent;
    }
    .hovereffect a.info:hover {
        box-shadow: 0 0 5px #fff;
    }


    .hovereffect p.icon-links a {
        float: right;
        color: #3c4a50;
        font-size: 1.4em;
    }

    .hovereffect:hover p.icon-links a:hover,
    .hovereffect:hover p.icon-links a:focus {
        color: #252d31;
    }

    .hovereffect h2,
    .hovereffect p.icon-links a {
        -webkit-transition: -webkit-transform 0.35s;
        transition: transform 0.35s;
        /*-webkit-transform: translate3d(0,200%,0);*/
        /*transform: translate3d(0,200%,0);*/
        visibility: visible;
    }

    .hovereffect p.icon-links a span:before {
        display: inline-block;
        padding: 8px 10px;
        speak: none;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }


    .hovereffect:hover .overlay,
    .hovereffect:hover h2,
    .hovereffect:hover p.icon-links a {
        -webkit-transform: translate3d(0,0,0);
        transform: translate3d(0,0,0);
    }

    .hovereffect:hover h2 {
        /*-webkit-transition-delay: 0.05s;*/
        /*transition-delay: 0.05s;*/
    }

    .hovereffect:hover p.icon-links a:nth-child(3) {
        -webkit-transition-delay: 0.1s;
        transition-delay: 0.1s;
    }

    .hovereffect:hover p.icon-links a:nth-child(2) {
        -webkit-transition-delay: 0.15s;
        transition-delay: 0.15s;
    }

    .hovereffect:hover p.icon-links a:first-child {
        -webkit-transition-delay: 0.2s;
        transition-delay: 0.2s;
    }

    </style>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> KPD Picture
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>KPD Picture</span>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <!-- <span class="caption-subject bold uppercase">List Area (Nama Outlet : XXX)</span> -->
                          <span class="caption-subject bold uppercase">List KPD Picture <?=$data_project[0]['nama_outlet'] ?></span>
                      </div>
                      <div class="pull-right">
                        <a href="<?=base_url('master_data/project/view'); ?>" id="btnBack" class="btn btn-danger btn-md">Back</a>
                          <a href="javascript:;" onclick="$('#imgInputKpd').click();" id="btnAddNewData" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <input type="text" id="id_project" name="id_project" value="<?=$data_project[0]['id'] ?>" style="display: none">
                  <form action="javascript:;" id="formPhotoKpd" name="formPhotoKpd" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type='file' id="imgInputKpd" name="imgInputKpd" accept=".png, .jpg, .jpeg" style="display: none" />
                        <input type="submit" id="submitPhotoKpd" name="" style="display: none">
                    </form>
                  <div class="portlet-body">
                        <div class="container" style="width: 100%">
                        <?php
                            if(count($data_kpd_pic) == 0){
                        ?>

                            <center>
                                <h2>Data not found</h2>
                            </center>

                        <?php }else{ ?>

                            <?php 
                                  $no = 1;
                                  foreach ($data_kpd_pic as $row) {
                                ?>


                                
                                    <div class="col-md-3">
                                        <div class="hovereffect">
                                            <img class="img-responsive img-thumbnail" src="<?=base_url('upload/kpd/').$row['file_name']; ?>" alt="">
                                                <div class="overlay">
                                                    <!-- <h2>Effect 10</h2> -->
                                                    <p class="icon-links">
                                                        <a href="javascript:;" onclick="imagePreviewDialog(&apos;<?=base_url('upload/kpd/').$row['file_name']; ?>&apos;);" >
                                                            <span class="fa fa-eye" style="background: white; border-radius: 5px !important; border: 1px solid white"></span>
                                                        </a>&nbsp;&nbsp;
                                                        <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                                            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                            <a href="#" onclick="delete_data('form_delete_<?=$row['id']; ?>',base_url+'master_data/delete_data/project_kpd_picture/')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><span class="fa fa-close" style="background: white; border-radius: 5px !important; border: 1px solid white"></span></a>
                                                        </form>
                                                        <!-- <a href="#">
                                                            <span class="fa fa-instagram" style="color: white"></span>
                                                        </a> -->
                                                    </p>
                                                </div>
                                        </div>
                                    </div>
                                

                         
                            <?php } ?>

                        <?php } ?>

                        </div>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if($sub_menu == 'project_level_training' && $mode == 'view') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Project Level Training
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Project Level Training</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Level Training</span>
                      </div>
                      <div class="pull-right">
                          <a href="<?=base_url('master_data/project_level_training/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="">No.</th>
                                  <td class="">Level Training Name</td>
                                  <th class="" style="text-align: center; width: 50px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php
                            $no = 1;
                            foreach ($data_level_training as $row) { 
                              ?>
                              <tr>
                                  <td><?=html_escape($no++); ?></td>
                                  <td><?=html_escape($row['level_training_name']); ?></td>
                                  <td>
                                    <!-- <a href="<?=base_url('Master_data/project_level_training/edit/').$row['id'] ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a> -->
                                    <!-- <a href="#" onclick="delete_data('project_level_training', <?=$row['id']; ?>)" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a> -->
                                  
                                    <table>
                                        <tr>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/project_level_training/edit'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>   
                                                </form>
                                            </td>
                                            <td style="padding: 0px 0px;">
                                                <form method="post" accept-charset="utf-8" id="form_delete_<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo $row['id']; ?>', base_url+'master_data/delete_data/project_level_training/');"><i class="fa fa-close"></i></a>   
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                  
                                  </td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_level_training' && ($mode == 'add' || $mode == 'edit')) { ?>
 
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Level Training</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">

                    <form id="form" class="form-horizontal" role="form">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" name="id" value="<?php if(!empty($data_level_training)){echo $data_level_training->id;} ?>">
                        <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Level Training Name</label>
                                  <div class="col-md-4">
                                      <input id="level_training_name" name="level_training_name" type="text" value="<?php if(!empty($data_level_training)){echo html_escape($data_level_training->level_training_name);} ?>" placeholder="Please Fill Level Training Name" class="name form-control alphanumeric" required />
                                  </div>
                              </div>
                              
                              <hr>
                        </div>
                            
                        <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_project_level_training';}else{echo 'edit_project_level_training';}; ?>&#39;);" class="btn green">Save</a>
                                  <a href="<?=base_url('master_data/project_level_training/view'); ?>" id="btnCancel" class="btn btn-danger">Cancel</a>
                              </div>
                              <div class="col-md-9"></div>
                        </div>
                      </form>
                   
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'view') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Project Template
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Project Template</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Project Template</span>


                      </div>
                      <div class="pull-right">
                          <a href="<?=base_url('master_data/project_template/add'); ?>" class="btn btn-primary btn-md">Add New Data</a>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="" style="width: 10%">No.</th>
                                  <th class="">Template Name</th>
                                  <th class="" style="text-align: center; width: 165px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                <?php if(count($project_template)>0):?>  
                                    <?php $n=0;?>        
                                    <?php foreach($project_template as $row):?>       
                                    <?php $n++;?>                                               
                                        <tr>                       
                                            <td><?=html_escape($n)?></td>
                                            <td><?=html_escape($row['template_name'])?></td>                                            
                                            <td style="display: inline-flex;">
                                              
                                                <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/project_template/edit'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo html_escape($row['id']); ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>   
                                                </form>

                                                <!-- <a href="<?=base_url('master_data/project_template/set_area/'.$row['id']); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="Set Area"><i class="fa fa-tags"></i></a> -->

                                                <form method="post" accept-charset="utf-8" action="<?php echo base_url('master_data/project_template/set_area'); ?>">
                                                    <input type="hidden" name="id" value="<?php echo html_escape($row['id']); ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <button type="submit" class="btn btn-sm btn-default"><i class="fa fa-tags"></i></button>   
                                                </form>

                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" href="javascript:;"> <i class="fa fa-gavel"></i>
                                                        <i class="fa fa-angle-down"></i>
                                                    </a>
                                                    <ul class="dropdown-menu" style="width: 50px">
                                                        <li data-toggle="tooltip" title="Equipment & Tools 50% Configuration">
                                                            <a href="<?=base_url('master_data/project_template/equipment_and_tools_configuration_50/'.$row['id']); ?>" style="width: 100%"><i class="fa fa-gavel"></i> 50% </a>
                                                        </li>
                                                        <li data-toggle="tooltip" title="Equipment & Tools 75% Configuration">
                                                            <a href="<?=base_url('master_data/project_template/equipment_and_tools_configuration_75/'.$row['id']); ?>" style="width: 100%"><i class="fa fa-gavel"></i> 75% </a>
                                                        </li>
                                                        <li data-toggle="tooltip" title="Equipment & Tools 100% Configuration">
                                                            <a href="<?=base_url('master_data/project_template/equipment_and_tools_configuration_100/'.$row['id']); ?>" style="width: 100%"><i class="fa fa-gavel"></i> 100% </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <!-- <a href="<?=base_url("master_data/project_template/kpd_picture/0"); ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="KPD Picture"><i class="fa fa-image"></i></a> -->
                                                
                                                <!-- <a href="javascript:;" onclick="delete_data('project_template',<?=$row->id?>)" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a> -->

                                                <form method="post" accept-charset="utf-8" id="form_delete_<?php echo html_escape($row['id']); ?>">
                                                    <input type="hidden" name="id" value="<?php echo html_escape($row['id']); ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <a class="btn btn-sm btn-default" onclick="delete_data('form_delete_<?php echo html_escape($row['id']); ?>', base_url+'master_data/delete_data/project_template/');"><i class="fa fa-close"></i></a>   
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif; ?>                              
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && ($mode == 'add' || $mode == 'edit')) { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          <?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?>
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span><?php if($mode == 'add'){ ?> Add New Data <?php }else{ ?> Edit Data <?php } ?></span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase"><?php if($mode == 'add'){ ?> Form Add New Data <?php }else{ ?> Form Edit Data <?php } ?></span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                    
                  
                      <form action="javascript:;" id="form" class="form-horizontal" role="form">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <input type="hidden" id="id" name="id" value="<?=!empty($id) ? $id : ''?>"/>
                            <div class="form-body">

                                <div class="form-group">
                                      <label class="col-md-2 control-label">Template Name</label>
                                      <div class="col-md-4">
                                          <input id="template_name" name="template_name" type="text" value="<?=!empty($project_template->template_name) ? html_escape($project_template->template_name) : ''?>" placeholder="please fill template name" class="name form-control alphanumeric" maxlength="255" required />
                                      </div>
                                </div>
                              
                                <div class="form-group">
                                    <div class="col-md-5 col-md-offset-2">
                                        <button type="button" id="btnSave" onclick="save(&#39;<?php if($mode == 'add'){echo 'add_project_template';}else{echo 'edit_project_template';}; ?>&#39;);" class="btn green">Save</button>
                                        <a id="btnCancel" href="<?=base_url('master_data/project_template/view'); ?>" class="btn btn-danger">Back</a>
                                    </div>
                                    
                                </div>

                          </div>
                      </form>
                    
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'set_area') { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Set Area
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Set Area</span>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">List Area (Template : <?=html_escape($data_current_project->template_name)?>)</span>

                      </div>
                      <div class="pull-right" style="display: inline-flex;">
                        <a href="<?=base_url('master_data/project_template/view'); ?>" class="btn btn-danger btn-md">Back</a>
                        <!-- <a href="<?=base_url('master_data/project_template/add_set_area/'.$id_project); ?>" class="btn btn-primary btn-md">Add New Data</a> -->

                        <form method="post" accept-charset="utf-8" id="add_new_data_area" action="<?php echo base_url('master_data/project_template/add_set_area/'); ?>">
                            <input type="hidden" name="id" value="<?php echo html_escape($id_project); ?>">
                            <input type="hidden" name="template_name" value="<?php echo html_escape($data_current_project->template_name); ?>">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-md btn-primary">Add New Data</button>   
                        </form>
                        

                        <!-- button get configuration -->
                        <a href="#get_set_area_dialog" class="btn btn-warning btn-md" data-toggle="modal">Get set area</a>
                        
                        <!-- modal get set area start here -->
                        <div class="modal fade" id="get_set_area_dialog" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                  <h4 class="modal-title"><b>Get Set Area Confirmation</b></h4>
                                </div>
                                <form action="<?=base_url("master_data/set_area_complete_for_project/") ?>" method="POST">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                  <input type="hidden" id="id_project_current" name="id_project_current" value="<?=$id_project?>">
                                  <input type="hidden" id="menu" name="menu" value="project_template">
                                  <div class="modal-body">
                                    <p>You can use area that has been set for another project template. Please choose project template below</p>

                                    <select id="id_project_get_set_area" name="id_project_get_set_area" class="form-control select2" style="width:100%">
                                    <?php 
                                        foreach ($data_project_template as $row) {
                                            if($row['id'] !== $data_current_project->id){ 
                                      ?>
                                      <option value="<?=$row['id']; ?>"><?=$row['template_name'] ?></option>
                                      <?php 
                                            }
                                        } 
                                      ?>
                                    </select>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn green">Save changes</button>
                                  </div>
                                </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- modal get set area end here -->

                      </div>
                  </div>
                  <div class="portlet-body">
                    <!-- Notifikasi -->
                        <?php if ($info = $this->session->flashdata('info')) { ?>
                          <div class='alert alert-success alert-dismissible'>
                              <?=$info; ?>
                              <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span>
                              </button>
                          </div>
                        <?php } ?>

                      <!-- /Notifikasi -->
                      <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                          <thead>
                              <tr>
                                  <th class="">No.</th>
                                  <th class="">Area Name</th>
                                  <th class="">Sub Area Name</th>
                                  <th class="">Sub Area Description</th>
                                  <th class="">Position</th>
                                  <th class="" style="text-align: center; width: 130px">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                                <?php if(count($list_area) > 0):?>  
                                    <?php $n=0;?>        
                                    <?php foreach($list_area as $row):?>       
                                        <?php $n++;?>                                               
                                        <tr>
                                            <td><?=$n;?></td>
                                            <td><?=$row->area_name;?></td>
                                            <td><?=$row->sub_area_name;?></td>
                                            <td><?=$row->sub_area_description;?></td>
                                            <td><?=$row->position;?></td>
                                            <td class="text-center" style="display: inline-flex;">

                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" title="EPM Configuration" href="javascript:;"> EPM
                                                        <i class="fa fa-angle-down"></i>
                                                    </a>
                                                    <ul class="dropdown-menu" style="width: 50px">
                                                        <li data-toggle="tooltip" title="EPM 50% Configuration">
                                                            <form method="POST" action="<?=base_url('master_data/project_template/sub_area_epm50'); ?>">
                                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                                <input type="" hidden name="id" value="<?=$row->id?>" />
                                                                <button type="submit" class="btn btn-default text-left borderless" style="width: 100%">50%</button>
                                                            </form>
                                                        </li>
                                                        <li data-toggle="tooltip" title="EPM 75% Configuration">
                                                            <form method="POST" action="<?=base_url('master_data/project_template/sub_area_epm75'); ?>">
                                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                                <input type="" hidden name="id" value="<?=$row->id?>" />
                                                                <button type="submit" class="btn btn-default text-left borderless" style="width: 100%">75%</button>
                                                            </form>
                                                        </li>
                                                        <li data-toggle="tooltip" title="EPM 100% Configuration">
                                                            <form method="POST" action="<?=base_url('master_data/project_template/sub_area_epm100'); ?>">
                                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                                <input type="" hidden name="id" value="<?=$row->id?>" />
                                                                <button type="submit" class="btn btn-default text-left borderless" style="width: 100%">100%</button>
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <form method="post" accept-charset="utf-8" id="form_delete_<?=$row->id; ?>">
                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                    <a href="#" onclick="delete_data('form_delete_<?=$row->id; ?>',base_url+'master_data/delete_data/project_template_area/')" class="btn btn-sm btn-default" data-toggle="tooltip" title="Delete"><i class="fa fa-close"></i></a>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif; ?>                                  
                          </tbody>
                      </table>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'sub_area_epm50') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        Sub Area EPM 50% Configuration 
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                    Sub Area EPM 50% Configuration 
                    
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">

                

                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                              <i class="icon-doc font-dark"></i>
                              <span class="caption-subject bold uppercase">
                                
                                Form Configuration 
                               

                                &nbsp;<?= $data_project_area[0]['template_name'].' - '.$data_project_area[0]['area_name'].' - '.$data_project_area[0]['sub_area_name'] ?>
                                <input type="hidden" id="jenis_epm" value="<?=$jenis_epm?>">
                                <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                                <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">

                              </span>
                            </div>
                            <div class="pull-right">

                                <!-- button get configuration -->
                                <a class="btn btn-info" data-toggle="modal" href="#small"> Get Configuration </a>

                                <!-- modal get configuration start here -->
                                <div class="modal fade" id="small" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                          <h4 class="modal-title"><b>Get Configuration Confirmation</b></h4>
                                        </div>
                                        <form action="<?=base_url("master_data/set_config_for_project_area/") ?>" method="POST">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                              <div class="modal-body">
                                                <p>You can use configuration from another project data for this project. Please choose project below</p>
                                                <input type="hidden" name="jenis_epmnya" value="<?=$jenis_epm ?>">
                                                <input type="hidden" name="id_project_areanya" value="<?=$data_project_area[0]['id'] ?>">
                                                <input type="hidden" name="id_projectnya" value="<?=$data_project_area[0]['id_project'] ?>">
                                                <input type="hidden" id="menu" name="menu" value="project_template">
                                                <select id="config" name="config" class="form-control select2" style="width:100%">
                                                  <?php foreach ($data_project_join as $row) { ?>
                                                  <option value="<?=$row['jenis_epm']; echo " "; echo $row['id']; ?>">
                                                    <?=$row['template_name'] ?> - <?=$row['area_name'] ?> - <?=$row['sub_area_name'] ?> - (<?=$row['jenis_epm'] ?>)</option>
                                                  <?php } ?>
                                                </select>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="submit" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn green">Save changes</button>
                                              </div>
                                            </form>
                                      </div>
                                      <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- modal get configuration end here -->

                            </div>
                        </div>
                        <div class="portlet-body form">
                          <form class="form-horizontal" role="form">
                              <div class="form-body">
                                
                                    <div class="form-group">
                                      <label class="col-md-2 control-label">Item Check Model</label>
                                      <div class="col-md-3">
                                        <select class="form-control" id="item_checklist_model" name="item_checklist_model">
                                            <option value="Model 1" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                        </select>     
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="col-md-2 control-label">Item Check</label>
                                      <div class="col-md-2">
                                          <input type="number" min="1" class="form-control" placeholder="Position" id="position">
                                      </div>
                                      <div class="col-md-3">
                                          <input type="text" class="form-control" placeholder="Item Name" id="item_name">
                                      </div>
                                      <div class="col-md-3">
                                          <input type="text" class="form-control" placeholder="Kriteria" id="kriteria">
                                      </div>
                                      <div class="col-md-1">
                                        <a href="javascript:;" class="btn btn-primary" onclick="add_ic();">Add To List</a>
                                      </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                              <table id="tbic" class="table table-bordered">
                                                <thead>
                                                  <tr style="background-color: #ffff99">
                                                    <th style="width: 60px">Position</th>
                                                    <th>Item Name</th>
                                                    <th>Kriteria</th>
                                                    <th style="width: 95px; text-align: center">Act.</th>
                                                  </tr>
                                                </thead>
                                                <tbody id="ic">
                                                    <?php foreach ($data_config_ic as $row) { ?>
                                                        <input id="idic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                        <tr class="rowic" id="removeRowIc<?=$row['position']; ?>">
                                                          <td id="p<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                          <td id="i<?=$row['position']; ?>"><?=$row['item_name']; ?></td>
                                                          <td id="k<?=$row['position']; ?>"><?=$row['kriteria']; ?></td>
                                                          <td>
                                                            <a href='javascript:;' onclick='removeIc(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editIc(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                          </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                              </table>
                                        </div>
                                    </div>

                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Kriteria Pekerjaan</label>
                                        <div class="col-md-2">
                                            <input id="positionkp" type="number" min="1" class="form-control" placeholder="Position">
                                        </div>
                                        <div class="col-md-3">
                                            <input id="item_kriteria_pekerjaan" type="text" class="form-control" placeholder="Item Kriteria Pekerjaan">
                                        </div>

                                        <div class="col-md-3">
                                            <input id="keterangan_kriteria_pekerjaan" type="text" class="form-control" placeholder="Keterangan">
                                        </div>

                                        <div class="col-md-1">
                                          <a href="javascript:;" onclick="add_kp();" class="btn btn-primary">Add To List</a>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                          
                                            <table id="tbkp" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Kriteria Pekerjaan</th>
                                                  <th>Keterangan</th>
                                                  <th style="width: 95px; text-align: center">Act.</th>
                                                </tr>
                                              </thead>
                                              <tbody id="kp">
                                              <?php foreach ($data_config_kp as $row) { ?>
                                                  <input id="idkp<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowkp" id="removeRowKp<?=$row['position']; ?>">
                                                    <td id="pkp<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="ikp<?=$row['position']; ?>"><?=$row['item_kriteria_pekerjaan']; ?></td>

                                                    <td id="kkp<?=$row['position']; ?>"><?=$row['keterangan']; ?></td>
                                                    <td>
                                                      <a href='#' onclick='removeKp(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='#' onclick='editKp(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                          
                                        </div>
                                    </div>

                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Minreq</label>
                                        <div class="col-md-2">
                                            <input id="positionminreq" type="number" min="1" class="form-control" placeholder="Position">
                                        </div>
                                        <div class="col-md-6">
                                            <input id="itemminreq" type="text" class="form-control" placeholder="Item Minreq">
                                        </div>
                                        <div class="col-md-1">
                                          <a href="javascript:;" onclick="add_minreq();" class="btn btn-primary">Add To List</a>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                         
                                            <table id="tbminreq" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Minreq</th>
                                                  <th style="width: 95px; text-align: center">Act.</th>
                                                </tr>
                                              </thead>
                                              <tbody id="minreq">
                                              <?php foreach ($data_config_minreq as $row) { ?>
                                                  <input id="idminreq<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowminreq" id="removeRowMinreq<?=$row['position']; ?>">
                                                    <td id="pminrek<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="iminreq<?=$row['position']; ?>"><?=$row['item_minreq']; ?></td>
                                                    <td>
                                                      <a href='javascript:;' onclick='removeMinreq(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editMinreq(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                          
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">TPS Line</label>
                                        <div class="col-md-2">
                                            <input id="positionTpsLine" type="number" min="1" class="form-control" placeholder="Position">
                                        </div>
                                        <div class="col-md-6">
                                            <input id="itemTpsLine" type="text" class="form-control" placeholder="Item TPS Line">
                                        </div>
                                        <div class="col-md-1">
                                            <a href="javascript:;" onclick="add_tpsline()" class="btn btn-primary">Add To List</a>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                            
                                            <table id="tbTpsLine" class="table table-bordered">
                                                <thead>
                                                <tr style="background-color: #ffff99">
                                                    <th style="width: 60px">Position</th>
                                                    <th>Item TPS Line</th>
                                                    <th style="width: 95px; text-align: center"ss>Act.</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tpsLine">
                                                <?php foreach ($data_config_tps_line as $row) { ?>
                                                    <input id="idtps<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                    <tr class="datatpsline" id="removeRowTpsLine<?=$row['position']; ?>">
                                                    <td id="ptps<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="itps<?=$row['position']; ?>"><?=$row['item_tps_line']; ?></td>
                                                    <td>
                                                        <a href='javascript:;' onclick='removeTpsLine(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editTps(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Keterangan</label>
                                        <div class="col-md-2">
                                            <input id="positionket" type="number" min="1" class="form-control" placeholder="Position">
                                        </div>
                                        <div class="col-md-6">
                                            <input id="itemket" type="text" class="form-control" placeholder="Item Keterangan">
                                        </div>
                                        <div class="col-md-1">
                                          <a href="javascript:;" onclick="add_ket();" class="btn btn-primary">Add To List</a>
                                        </div>
                                    </div>
                                  
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-8">
                                          
                                            <table id="tbket" class="table table-bordered">
                                              <thead>
                                                <tr style="background-color: #ffff99">
                                                  <th style="width: 60px">Position</th>
                                                  <th>Item Keterangan</th>
                                                  <th style="width: 95px; text-align: center">Act.</th>
                                                </tr>
                                              </thead>
                                              <tbody id="ket">
                                              <?php foreach ($data_config_ket as $row) { ?>
                                                  <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                  <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                    <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                    <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                    <td>
                                                      <a href='javascript:;' onclick='removeKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                  </tr>
                                                <?php } ?>
                                              </tbody>
                                            </table>
                                         
                                        </div>
                                    </div>
                                  
                                  <hr>
                              </div>

                              <div class="form-group">
                                  <div class="col-md-5 col-md-offset-2">
                                      <a id="btnSave" onclick="saveSub('project_template');" class="btn green">Save</a>
                                      <a id="btnCancel" href="<?=base_url('master_data/project_template/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Cancel</a>
                                  </div>
                                  <div class="col-md-9"></div>
                              </div>
                            
                          </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->

                

          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'sub_area_epm75') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        
            Sub Area EPM 75% Configuration 
        
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                        Sub Area EPM 75% Configuration 
                 
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">

                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                      <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                  <i class="icon-doc font-dark"></i>
                                  <span class="caption-subject bold uppercase">
                                    
                                    Form Configuration 
                                    

                                    &nbsp;<?= $data_project_area[0]['template_name'].' - '.$data_project_area[0]['area_name'].' - '.$data_project_area[0]['sub_area_name'] ?>
                                    <input type="hidden" id="jenis_epm" value="<?=$jenis_epm?>">
                                    <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                                    <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">
                                  </span>
                                </div>
                                <div class="pull-right">

                                    <!-- button get configuration -->
                                    <a class="btn btn-info" data-toggle="modal" href="#small"> Get Configuration </a>

                                    <!-- modal get configuration start here -->
                                    <div class="modal fade" id="small"  role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                              <h4 class="modal-title"><b>Get Configuration Confirmation</b></h4>
                                            </div>
                                            <form action="<?=base_url("master_data/set_config_for_project_area/") ?>" method="POST">
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                              <div class="modal-body">
                                                <p>You can use configuration from another project data for this project. Please choose project below</p>
                                                <input type="hidden" name="jenis_epmnya" value="<?=$jenis_epm ?>">
                                                <input type="hidden" name="id_project_areanya" value="<?=$data_project_area[0]['id'] ?>">
                                                <input type="hidden" name="id_projectnya" value="<?=$data_project_area[0]['id_project'] ?>">
                                                <input type="hidden" id="menu" name="menu" value="project_template">
                                                <select id="config" name="config" class="form-control select2" style="width:100%">
                                                  <?php foreach ($data_project_join as $row) { ?>
                                                  <option value="<?=$row['jenis_epm']; echo " "; echo $row['id']; ?>"><?=$row['template_name'] ?> - <?=$row['area_name'] ?> - <?=$row['sub_area_name'] ?> - (<?=$row['jenis_epm'] ?>)</option>
                                                  <?php } ?>
                                                </select>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="submit" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn green">Save changes</button>
                                              </div>
                                            </form>
                                          </div>
                                          <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- modal get configuration end here -->

                                </div>
                            </div>
                            <div class="portlet-body form">
                              <form class="form-horizontal" role="form">
                                  <div class="form-body">
                                    
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Item Check Model</label>
                                          <div class="col-md-3">
                                            <select class="form-control" id="item_checklist_model" name="item_checklist_model">
                                            <option value="Model 1" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_ic) and $data_config_ic[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                            </select>     
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Item Check</label>
                                          <div class="col-md-2">
                                              <input type="number" min="1" class="form-control" placeholder="Position" id="position">
                                          </div>
                                          <div class="col-md-3">
                                              <input type="text" class="form-control" placeholder="Item Name" id="item_name">
                                          </div>
                                          <div class="col-md-3">
                                              <input type="text" class="form-control" placeholder="Kriteria" id="kriteria">
                                          </div>
                                          <div class="col-md-1">
                                            <a href="javascript:;" class="btn btn-primary" onclick="add_ic();">Add To List</a>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8">
                                                  <table id="tbic" class="table table-bordered">
                                                    <thead>
                                                      <tr style="background-color: #ffff99">
                                                        <th style="width: 60px">Position</th>
                                                        <th>Item Name</th>
                                                        <th>Kriteria</th>
                                                        <th style="width: 95px; text-align: center">Act.</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody id="ic">
                                                    <?php foreach ($data_config_ic as $row) { ?>
                                                        <input id="idic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                        <tr class="rowic" id="removeRowIc<?=$row['position']; ?>">
                                                          <td id="p<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                          <td id="i<?=$row['position']; ?>"><?=$row['item_name']; ?></td>
                                                          <td id="k<?=$row['position']; ?>"><?=$row['kriteria']; ?></td>
                                                          <td>
                                                            <a href='javascript:;' onclick='removeIc(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editIc(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                          </td>
                                                        </tr>
                                                      <?php } ?>
                                                    </tbody>
                                                  </table>
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Kriteria Pekerjaan</label>
                                            <div class="col-md-2">
                                                <input id="positionkp" type="number" min="1" class="form-control" placeholder="Position">
                                            </div>
                                            <div class="col-md-3">
                                                <input id="item_kriteria_pekerjaan" type="text" class="form-control" placeholder="Item Kriteria Pekerjaan">
                                            </div>

                                            <div class="col-md-3">
                                                <input id="keterangan_kriteria_pekerjaan" type="text" class="form-control" placeholder="Keterangan">
                                            </div>

                                            <div class="col-md-1">
                                              <a href="javascript:;" onclick="add_kp();" class="btn btn-primary">Add To List</a>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8">
                                              
                                                <table id="tbkp" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Kriteria Pekerjaan</th>
                                                      <th>Keterangan</th>
                                                      <th style="width: 95px; text-align: center">Act.</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="kp">
                                                  <?php foreach ($data_config_kp as $row) { ?>
                                                      <input id="idkp<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowkp" id="removeRowKp<?=$row['position']; ?>">
                                                        <td id="pkp<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="ikp<?=$row['position']; ?>"><?=$row['item_kriteria_pekerjaan']; ?></td>

                                                        <td id="kkp<?=$row['position']; ?>"><?=$row['keterangan']; ?></td>
                                                        <td>
                                                          <a href='#' onclick='removeKp(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='#' onclick='editKp(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                        </td>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Minreq</label>
                                            <div class="col-md-2">
                                                <input id="positionminreq" type="number" min="1" class="form-control" placeholder="Position">
                                            </div>
                                            <div class="col-md-6">
                                                <input id="itemminreq" type="text" class="form-control" placeholder="Item Minreq">
                                            </div>
                                            <div class="col-md-1">
                                              <a href="javascript:;" onclick="add_minreq();" class="btn btn-primary">Add To List</a>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8">
                                             
                                                <table id="tbminreq" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Minreq</th>
                                                      <th style="width: 95px; text-align: center">Act.</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="minreq">
                                                  <?php foreach ($data_config_minreq as $row) { ?>
                                                      <input id="idminreq<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowminreq" id="removeRowMinreq<?=$row['position']; ?>">
                                                        <td id="pminrek<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="iminreq<?=$row['position']; ?>"><?=$row['item_minreq']; ?></td>
                                                        <td>
                                                            <a href='javascript:;' onclick='removeMinreq(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editMinreq(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                          </td>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">TPS Line</label>
                                            <div class="col-md-2">
                                                <input id="positionTpsLine" type="number" min="1" class="form-control" placeholder="Position">
                                            </div>
                                            <div class="col-md-6">
                                                <input id="itemTpsLine" type="text" class="form-control" placeholder="Item TPS Line">
                                            </div>
                                            <div class="col-md-1">
                                              <a href="javascript:;" onclick="add_tpsline()" class="btn btn-primary">Add To List</a>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8">
                                             
                                                <table id="tbTpsLine" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item TPS Line</th>
                                                      <th style="width: 95px; text-align: center"ss>Act.</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="tpsLine">
                                                  <?php foreach ($data_config_tps_line as $row) { ?>
                                                      <input id="idtps<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="datatpsline" id="removeRowTpsLine<?=$row['position']; ?>">
                                                        <td id="ptps<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="itps<?=$row['position']; ?>"><?=$row['item_tps_line']; ?></td>
                                                        <td>
                                                            <a href='javascript:;' onclick='removeTpsLine(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editTps(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                          </td>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                              
                                            </div>
                                        </div>

                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Keterangan</label>
                                            <div class="col-md-2">
                                                <input id="positionket" type="number" min="1" class="form-control" placeholder="Position">
                                            </div>
                                            <div class="col-md-6">
                                                <input id="itemket" type="text" class="form-control" placeholder="Item Keterangan">
                                            </div>
                                            <div class="col-md-1">
                                              <a href="javascript:;" onclick="add_ket();" class="btn btn-primary">Add To List</a>
                                            </div>
                                        </div>
                                      
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8">
                                              
                                                <table id="tbket" class="table table-bordered">
                                                  <thead>
                                                    <tr style="background-color: #ffff99">
                                                      <th style="width: 60px">Position</th>
                                                      <th>Item Keterangan</th>
                                                      <th style="width: 95px; text-align: center">Act.</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="ket">
                                                  <?php foreach ($data_config_ket as $row) { ?>
                                                      <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                      <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                        <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                        <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                        <td>
                                                            <a href='javascript:;' onclick='removeKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                          </td>
                                                      </tr>
                                                      </tr>
                                                    <?php } ?>
                                                  </tbody>
                                                </table>
                                             
                                            </div>
                                        </div>
                                      
                                      <hr>
                                  </div>

                                  <div class="form-group">
                                      <div class="col-md-5 col-md-offset-2">
                                          <a id="btnSave" onclick="saveSub('project_template');" class="btn green">Save</a>
                                          <a id="btnCancel" href="<?=base_url('master_data/project_template/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Back</a>
                                      </div>
                                      <div class="col-md-9"></div>
                                  </div>
                                
                              </form>
                            </div>
                      </div>
                      <!-- END EXAMPLE TABLE PORTLET-->


          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'sub_area_epm100') { ?>

  <style type="text/css">
    .select2-container{
        z-index:100000000;
    }
  </style>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
        Sub Area EPM 100% Configuration 
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>
                    
                    Sub Area EPM 100% Configuration 
                    
                  </span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">
                            
                            Form Configuration 
                           

                             &nbsp;<?= $data_project_area[0]['template_name'].' - '.$data_project_area[0]['area_name'].' - '.$data_project_area[0]['sub_area_name'] ?>
                            <input type="hidden" id="jenis_epm" value="<?=$jenis_epm?>">
                            <input type="hidden" id="id_project_area" value="<?=$data_project_area[0]['id'] ?>">
                            <input type="hidden" id="id_project" value="<?=$data_project_area[0]['id_project'] ?>">
                          </span>
                        </div>
                        <div class="pull-right">

                            <!-- button get configuration -->
                            <a class="btn btn-info" data-toggle="modal" href="#small"> Get Configuration </a>

                            <!-- modal get configuration start here -->
                            <div class="modal fade" id="small"  role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title"><b>Get Configuration Confirmation</b></h4>
                                    </div>
                                    <form action="<?=base_url("master_data/set_config_for_project_area/") ?>" method="POST">
                                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                      <div class="modal-body">
                                        <p>You can use configuration from another project data for this project. Please choose project below</p>
                                        <input type="hidden" name="jenis_epmnya" value="<?=$jenis_epm ?>">
                                        <input type="hidden" name="id_project_areanya" value="<?=$data_project_area[0]['id'] ?>">
                                        <input type="hidden" name="id_projectnya" value="<?=$data_project_area[0]['id_project'] ?>">
                                        <input type="hidden" id="menu" name="menu" value="project_template">
                                        <select id="config" name="config" class="form-control select2" style="width:100%">
                                          <?php foreach ($data_project_join as $row) { ?>
                                          <option value="<?=$row['jenis_epm']; echo " "; echo $row['id']; ?>"><?=$row['template_name'] ?> - <?=$row['area_name'] ?> - <?=$row['sub_area_name'] ?> - (<?=$row['jenis_epm'] ?>)</option>
                                          <?php } ?>
                                        </select>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn green">Save changes</button>
                                      </div>
                                    </form>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- modal get configuration end here -->

                        </div>
                    </div>
                    <div class="portlet-body form">
                      <form class="form-horizontal" role="form">
                          <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Pictures</label>
                                    <div class="col-md-2">
                                        <input id="positionpic" type="number" min="1" class="form-control" placeholder="Position">
                                    </div>
                                    <div class="col-md-6">
                                        <input id="itempic" type="text" class="form-control" placeholder="Item picture">
                                    </div>
                                    <div class="col-md-1">
                                      <a href="javascript:;" onclick="add_pic();" class="btn btn-primary">Add To List</a>
                                    </div>
                                </div>
                                 

                                <div class="form-group">
                                    <label class="col-md-2 control-label">&nbsp;</label>
                                    <div class="col-md-8">
                                        <table id="tbpic" class="table table-bordered">
                                          <thead>
                                            <tr style="background-color: #ffff99">
                                              <th style="width: 60px">Position</th>
                                              <th>Picture name</th>
                                              <th style="width: 95px; text-align: center">Act.</th>
                                            </tr>
                                          </thead>
                                          <tbody id="pic">
                                            <?php foreach ($data_config_pic as $row) { ?>
                                                <input id="idpic<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowpic" id="removeRowPic<?=$row['position']; ?>">
                                                <td id="ppic<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="ipic<?=$row['position']; ?>"><?=$row['item_pic']; ?></td>
                                                <td>
                                                    <a href='javascript:;' onclick='removePic(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editPic(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Model Kriteria Min.</label>
                                  <div class="col-md-3">
                                    <select class="form-control" id="item_checklist_model" name="item_checklist_model">
                                    <option value="Model 1" <?=((!empty($data_config_km) and $data_config_km[0]['model']=='Model 1') ? 'selected' : '' )?>>Model 1 (Default)</option>
                                            <option value="Model 2" <?=((!empty($data_config_km) and $data_config_km[0]['model']=='Model 2') ? 'selected' : '' )?>>Model 2 (Dengan jarak)</option>
                                    </select>     
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-md-2 control-label">Kriteria Minimum</label>
                                  <div class="col-md-2">
                                      <input type="number" min="1" class="form-control" placeholder="Position" id="positionkm">
                                  </div>
                                  <div class="col-md-5">
                                      <textarea class="form-control" placeholder="Item Kriteria Minimum" id="itemkm" rows="1" style="resize: vertical;"></textarea>
                                  </div>
                                  <div class="col-md-1">
                                    <a href="javascript:;" class="btn btn-primary" onclick="add_km();">Add To List</a>
                                  </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">&nbsp;</label>
                                    <div class="col-md-8">
                                          <table id="tbkm" class="table table-bordered">
                                            <thead>
                                              <tr style="background-color: #ffff99">
                                                <th style="width: 60px">Position</th>
                                                <th>Item Kriteria Minimum</th>
                                                <th style="width: 95px; text-align: center">Act.</th>
                                              </tr>
                                            </thead>
                                            <tbody id="km">
                                            <?php foreach ($data_config_km as $row) { ?>
                                                <input id="idkm<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowkm" id="removeRowKm<?=$row['position']; ?>">
                                                <td id="pkm<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="ikm<?=$row['position']; ?>"><?=$row['item_kriteria_minimum']; ?></td>
                                                <td>
                                                    <a href='javascript:;' onclick='removeKm(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKm(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                          </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Keterangan</label>
                                    <div class="col-md-2">
                                        <input id="positionket" type="number" min="1" class="form-control" placeholder="Position">
                                    </div>
                                    <div class="col-md-6">
                                        <input id="itemket" type="text" class="form-control" placeholder="Item Keterangan">
                                    </div>
                                    <div class="col-md-1">
                                      <a href="javascript:;" onclick="add_ket();" class="btn btn-primary">Add To List</a>
                                    </div>
                                </div>
                                 

                                <div class="form-group">
                                    <label class="col-md-2 control-label">&nbsp;</label>
                                    <div class="col-md-8">
                                        <table id="tbket" class="table table-bordered">
                                          <thead>
                                            <tr style="background-color: #ffff99">
                                              <th style="width: 60px">Position</th>
                                              <th>Item Keterangan</th>
                                              <th style="width: 95px; text-align: center">Act.</th>
                                            </tr>
                                          </thead>
                                          <tbody id="ket">
                                            <?php foreach ($data_config_ket as $row) { ?>
                                                <input id="idket<?=$row['position']; ?>" type="hidden" value="<?=$row['id'] ?>">
                                                <tr class="rowket" id="removeRowKet<?=$row['position']; ?>">
                                                <td id="pket<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                <td id="iket<?=$row['position']; ?>"><?=$row['item_ket']; ?></td>
                                                <td>
                                                    <a href='javascript:;' onclick='removeKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKet(<?=$row['position']; ?>)' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>
                                                    </td>
                                                </tr>
                                                </tr>
                                            <?php } ?>
                                          </tbody>
                                        </table>
                                    </div>
                                </div> 
                              
                                
                              
                              <hr>
                          </div>

                          <div class="form-group">
                              <div class="col-md-5 col-md-offset-2">
                                  <a id="btnSave" onclick="saveSub('project_template');" class="btn green">Save</a>
                                  <a id="btnCancel" href="<?=base_url('master_data/project_template/set_area/'.$data_project_area[0]['id_project']); ?>" class="btn btn-danger">Back</a>
                              </div>
                              <div class="col-md-9"></div>
                          </div>
                        
                      </form>
                  </div>
              </div>

          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && $mode == 'add_set_area') { ?>
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          Add New Data
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Set Area</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Add New Data</span>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">Form Add New Data Area (TEMPLATE : <?php echo html_escape($template_name); ?>)</span>
                      </div>
                      <div class="tools"> </div>
                  </div>
                  <div class="portlet-body form">
                        <form action="javascript:;" id="form" class="form-horizontal" role="form">
                           <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                          <input type="hidden" name="id_project" id="id_project" value="<?=$id_project ?>">
                          <div class="form-body">
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Area Name</label>
                                  <div class="col-md-6">
                                    <select name="id_area" id="id_area" onchange="filteringsubarea(this);" class="form-control id-area select2">
                                      <option disabled="" selected="">-- Select --</option>
                                        <?php if($list_area->num_rows()>0):?>
                                            <?php foreach($list_area->result() as $row):?>
                                                <option value='<?=html_escape($row->id)?>'><?=html_escape($row->area_name)?></option>
                                            <?php endforeach;?>
                                        <?php endif; ?>                                        
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Sub Area Name</label>
                                  <div class="col-md-6">
                                    <select name="id_sub_area" id="id_sub_area" class="form-control id-sub-area select2">                                     
                                    <select>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-md-2 control-label">Position</label>
                                  <div class="col-md-2">
                                    <input type="number" min="1" class="form-control position" name="position">
                                  </div>
                              </div>
                              
                              <hr>
                          </div>

                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-2">
                                    <a href="javascript:;" id="btnSave" onclick="save_project_template_area('project_template')" class="btn green">Save</a>
                                    <a onclick="$('#btnBack').trigger('click');" class="btn btn-danger">Back</a>
                                </div>
                            </div>
                          
                        </form>
                        <form method="post" id="form_cancel" accept-charset="utf-8" action="<?php echo base_url('master_data/project_template/set_area'); ?>" style="display: none">
                            <input type="hidden" name="id" value="<?php echo html_escape($id_project); ?>">
                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                            <button type="submit" class="btn btn-danger" id="btnBack">Back</button>   
                        </form>
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>

<?php if ($sub_menu == 'project_template' && ($mode == 'equipment_and_tools_configuration_50' || $mode == 'equipment_and_tools_configuration_75' || $mode == 'equipment_and_tools_configuration_100')) { ?>
  
  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title"> Equipment & Tools <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> Configuration
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Project Template</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <span>Equipment & Tools <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> Configuration</span>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->
      <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">Equipment & Tools Configuration <?php if($jenis_epm == '50'){echo '50%';}elseif ($jenis_epm == '75') {echo '75%';}else{echo '100%';}; ?> -  &nbsp;<?= $data_project[0]['template_name'] ?></span>

                      </div>
                      <div class="pull-right">
                        
                      </div>
                  </div>
                  <div class="portlet-body">
                        <form class="form-horizontal" role="form">
                           
                            <div class="form-body">
                            
                            <input type="hidden" name="id_project" id="id_project" value="<?=$data_project[0]['id']; ?>">
                            <input type="hidden" name="jenis_epm" id="jenis_epm" value="<?=$jenis_epm; ?>">
                            
                                <div class="form-group">
                                  <label class="col-md-2 control-label">Equipment & Tools</label>
                                  <div class="col-md-2">
                                      <input type="number" class="form-control" placeholder="Position" id="position_eat" name="position_eat">
                                  </div>
                                  <div class="col-md-3">
                                      <input type="text" id="group_name_eat" name="group_name_eat" class="form-control" placeholder="Group Name">
                                  </div>
                                  <div class="col-md-3">
                                      <input type="text" id="item_name_eat" name="item_name_eat" class="form-control" placeholder="Item Name">
                                  </div>
                                  <div class="col-md-1">
                                    <a href="javascript:;" class="btn btn-primary" onclick="add_eat();">Add To List</a>
                                  </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">&nbsp;</label>
                                    <div class="col-md-8">
                                        <table id="tbeat" class="table table-bordered">
                                        <thead>
                                          <tr style="background-color: #ffff99">
                                            <th style="width: 60px">Position</th>
                                            <th>Group Name</th>
                                            <th>Item Name</th>
                                            <th style="width: 95px; text-align: center">Act.</th>
                                          </tr>
                                        </thead>
                                            <tbody id="eat">
                                            <?php foreach ($data_config_eat as $row) { ?>
                                                <tr class="roweat" id="removeRowEat<?=$row['position']; ?>">
                                                  <td id="p<?=$row['position']; ?>"><?=$row['position']; ?></td>
                                                  <td id="g<?=$row['position']; ?>"><?=$row['group_name']; ?></td>
                                                  <td id="i<?=$row['position']; ?>"><?=$row['item_name']; ?></td>
                                                  <td><a href="javascript:;" onclick="removeEat('<?=$row['position']; ?>')" class="btn btn-default btn-sm"><i class="fa fa-close"></i></a><a href="javascript:;" onclick="editEat('<?=$row['position']; ?>')" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i></a></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-md-5 col-md-offset-2">
                                      <a id="btnSave" onclick="saveEat();" class="btn green">Save</a>
                                      <a id="btnSave" href="<?=base_url('master_data/project_template/view'); ?>" class="btn btn-danger">Back</a>
                                  </div>
                                  <div class="col-md-9"></div>
                                </div>

                           

                            
                            </div>

                            
                        </form>
                     
                  </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>
