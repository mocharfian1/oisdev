<h3 align="center"><?=$nama_outlet . ' (' . $jenis_epm . '%)'?></h3><br><br>
<?php $n=0;foreach ($data_area as $row): $n++;?>
  <?php if($n>1):?>
    <div class="nextpage"></div>
  <?php endif;?>
       <h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f;font-weight: bold;"><?=$row['area_name']?></h3>
<?php foreach ($row['sub'] as $sub): ?>
       <h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000;font-weight: bold;"><?=$sub['position']?>. <?=$sub['sub_area_name']?></h4>
       <?php if(isset($sub['ic']) or isset($sub['kp']) or (isset($sub['km']) and $jenis_epm==100)):?>
         <?php if (isset($sub['ic'])): ?>
                <table align="center" width="100%" border="1">
                  <thead>
                    <tr style="background-color:#99c0ff; color: #000">
                      <th style="text-align: center;">Item Check</th>
                      <th style="text-align: center;">Kriteria</th>
                       <?php if ($sub['ic'][0]['model'] == 'Model 2'): ?>
                              <th style="text-align: center;">Jarak</th>
                       <?php endif;?>
                      <th width="40%" colspan="3">
                        <table border="1" width="100%">
                          <thead>
                            <tr>
                              <th style="text-align: center;" colspan="3">Di check oleh ("√" atau "X")</th>
                            </tr>
                            <tr>
                              <th style="text-align: center;">#1</th>
                              <th style="text-align: center;">#2</th>
                              <th style="text-align: center;">#3</th>
                            </tr>
                          </thead>
                        </table>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                <?php foreach ($sub['ic'] as $item): ?>
                       <?php
                  $check     = ['1' => '√', '0' => 'X', '' => ''];
                  $dataScore = [
                      'dealer'    => $check[$item['score_dealer']],
                      'konsultan' => $check[$item['score_konsultan']],
                      'tam'       => $check[$item['score_tam']],
                  ];
                  ?>
                       <tr>
                              <td style="padding: 7px"><?=$item['item_name']?></td>
                              <td style="padding: 7px"><?=$item['kriteria']?></td>
                              <?php if ($item['model'] == 'Model 2'): ?>
                                     <td style="padding: 7px"><?=$item['jarak']?></td>
                              <?php endif;?>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                       </tr>
                <?php endforeach;?>
                  </tbody>
                </table>
                <br/>
                <br/>
         <?php endif;?>
         <?php if (isset($sub['kp'])): ?>
                <table align="center" width="100%" border="1">
                  <thead>
                    <tr style="background-color:#99c0ff; color: #000">
                      <th style="text-align: center;">No.</th>
                      <th style="text-align: center;">Kriteria Pekerjaan</th>
                      <th width="40%" colspan="3">
                        <table border="1" width="100%">
                          <thead>
                            <tr>
                              <th style="text-align: center;" colspan="3">Di check oleh ("√" atau "X")</th>
                            </tr>
                            <tr>
                              <th style="text-align: center;">#1</th>
                              <th style="text-align: center;">#2</th>
                              <th style="text-align: center;">#3</th>
                            </tr>
                          </thead>
                        </table>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                <?php foreach ($sub['kp'] as $item): ?>
                       <?php
                      $check     = ['1' => '√', '0' => 'X', '' => ''];
                      $dataScore = [
                          'dealer'    => $check[$item['score_dealer']],
                          'konsultan' => $check[$item['score_konsultan']],
                          'tam'       => $check[$item['score_tam']],
                      ];
                      ?>
                       <tr>
                              <td style="padding: 7px"><?=$item['position']?></td>
                              <td style="padding: 7px"><?=$item['item_kriteria_pekerjaan']?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                       </tr>
                <?php endforeach;?>
                  </tbody>
                </table>
                <br/>
                <br/>
         <?php endif;?>
        <?php if(isset($sub['km']) and $jenis_epm==100):?>
          <table border="1" align="center" width="100%"><br><br>
              <thead>
                <tr style="background-color:#99c0ff; color: #000">
                    <th style="padding: 10px; text-align: center" width="5%">No</th>
                    <th style="padding: 10px; text-align: center" width="50%">Kriteria Minimun</th>
                       <?php if ($sub['km'][0]['model'] == 'Model 2'): ?>
                              <th style="text-align: center;">Jarak</th>
                       <?php endif;?>
                    <th style="padding: 10px; text-align: center" width="15%">#1</th>
                    <th style="padding: 10px; text-align: center" width="15%">#2</th>
                    <th style="padding: 10px; text-align: center" width="15%">#3</th>
                </tr>
              </thead>
              <tbody>
                  <tbody>
                    <?php foreach ($sub['km'] as $item): ?>
                           <?php
                          $check     = ['1' => '√', '0' => 'X', '' => ''];
                          $dataScore = [
                              'dealer'    => $check[$item['score_dealer']],
                              'konsultan' => $check[$item['score_konsultan']],
                              'tam'       => $check[$item['score_tam']],
                          ];
                          ?>
                           <tr>
                                  <td style="padding: 7px"><?=$item['position']?></td>
                                  <td style="padding: 7px"><?=$item['item_kriteria_minimum']?></td>
                              <?php if ($item['model'] == 'Model 2'): ?>
                                     <td style="padding: 7px"><?=$item['jarak']?></td>
                              <?php endif;?>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                                  <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                           </tr>
                    <?php endforeach;?>
              <tbody>
          </table>
        <?php endif;?>
      <?php else:?>
        <center>Data Not Found</center>
      <?php endif;?>
      <br/><br/>
<?php endforeach;?>
<?php endforeach;?>