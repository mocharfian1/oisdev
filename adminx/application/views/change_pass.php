<div class="page-content">
    <h3 class="page-title"> Change Password
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?=site_url('Home/changePass')?>">Change Password</a>
            </li>
        </ul>

    </div>

        <?php if ($info = $this->session->flashdata('message')) {
                echo $info;
            } ?>
        <br/>
        <div class="portlet-body form">
            <div class="form-body">
            <form class=" from-horizontal form-material" action="<?php echo base_url('Home/editPass/'); ?>" method="post" autocomplete="off">
                <div class="form-group">
                    <label for="newpass" class="col-md-2 label-control">New Password</label>
                    <div class="col-md-4">
                        <input type="password" name="newpass" id="newpass" value="" required class="form-control" autocomplete="off">
                    </div>
                </div>
                </br>
                </br>
                </br>
                <div class="form-group">
                    <label for="confpass" class="label-control col-md-2">Confirm Password</label>
                    <div class="col-md-4">
                        <input type="password" name="confpass" id="confPass" value="" required class="form-control">
                    </div>
                </div>
            </br>
                <hr>
            </div>
                <div class="form-group">
                    <div class="col-md-5 col-md-offset-2">
                    <input id="submit" type="submit" class="btn btn-primary" value="Save" />
                    <span type="submit"><a href="<?php echo base_url('home/index'); ?>" class="btn btn-default">Close</a></span>
                </div>
                <div class="col-md-9"></div>
                </div>
            </form>
            </br>
            
        </div>
    </div>