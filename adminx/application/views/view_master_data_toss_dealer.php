<script type="text/javascript">
  var URL = "<?php echo base_url(); ?>";
</script>
<?php if ($mode=='view') { ?>

  <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <!-- BEGIN THEME PANEL -->

      <!-- END THEME PANEL -->
      <h3 class="page-title">
          TOSS Dealer
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Master Data</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">TOSS Dealer</a>
              </li>
          </ul>
          <div class="page-toolbar">

          </div>
      </div>
      <!-- END PAGE HEADER-->

      <div class="row">
          <div class="col-md-12">
            <div action="javascript:;" id="form" class="form-horizontal" >
              
                <div class="portlet light ">
                    <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-doc font-dark"></i>
                          <span class="caption-subject bold uppercase">TOSS Dealer List</span>
                      </div>
                      <div class="pull-right">
                        <a href="javascript:;" class="btn btn-primary btn-md" onclick="$('input[name=\'upload_excel\']').click()">Upload Data</a>
                        <input name="upload_excel" type="file" style="display: none;" onchange="conf_upload()">
                      </div>    
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                            <thead>
                                  <tr>
                                      <th class="" width="5%">No.</th>
                                      <th class="">Nama Dealer</th>
                                      <th class="">Nama Outlet</th>
                                  </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($ls_dealer)){ ?>
                                  <?php $no=1; ?>
                                  <?php foreach ($ls_dealer as $key => $value) { ?>
                                      <tr>
                                        <th class="" width="5%"><?php echo $no; ?></th>
                                        <th class=""><?=html_escape($value->dealer_name)?></th>
                                        <th class=""><?=html_escape($value->outlet_name)?></th>
                                      </tr>
                                  <?php $no++; } ?>
                                <?php } ?>
                            </tbody>
                      </table>
                    </div>

                </div>

            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
          </div>
      </div>


  </div>

<?php } ?>