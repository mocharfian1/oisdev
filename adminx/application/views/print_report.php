<?php
/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 ?>

 <?php include('view-cover.php'); ?>
 <h3 align="center"><?=$nama_outlet . ' (' . $jenis_epm . '%)'?></h3><br><br>
  <table class="center">
    <tbody>
      <tr>
        <td style="padding: 10px">Nama Outlet</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=html_escape($nama_outlet)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Dealer</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=html_escape($main_dealer)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Fungsi Outlet</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=html_escape($fungsi_outlet)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Type Pembangunan</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=html_escape($type_pembangunan)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Alamat</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=html_escape($alamat)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Tanggal EPM</td>
      </tr>
      <tr>
        <td style="padding: 10px">1. EPM 50%</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=format_date_week($epm_50_date)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">2. Target EPM 75%</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=format_date_week($epm_75_date)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">3. Target EPM 100%</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=format_date_week($epm_100_date)?></td>
      </tr>
      <tr>
        <td style="padding: 10px">Target Otoritasi</td>
        <td style="padding: 10px"> : </td>
        <td style="padding: 10px"><?=format_date_full($target_otorisasi)?></td>
      </tr>
    </tbody>
  </table>
  <p>
    Evaluasi Establishment Progress Monitoring (EPM) <?=html_escape($jenis_epm)?>% ini telah diisi dan diperiksa oleh 
    Dealer, outlet tersebut diatas sesuai dengan keadaan yang sesungguhnya. Pengecekan sesuai
    dengan kolom yang disediakan :<br><br>
    1. Kolom #1 diisi oleh <?=strtoupper($type_level_1)?><br>
    2. Kolom #2 diisi oleh <?=strtoupper($type_level_2)?><br>
    3. Kolom #3 diisi oleh <?=strtoupper($type_level_3)?><br>
    Kunjungan Establishment Progress Monitoring (EPM) akan dilakukan apabila progress
    pekerjaan sudah sesuai dengan kriteria pekerjaan Establisment Progress Monitoring (EPM) <?=html_escape($jenis_epm)?>%.
  </p>
  <br><p align="right">Jakarta , <?=todayIndDate()?></p><br>
  <table align="center" border="1" width="80%" style="border-collapse: collapse;">
    <thead>
      <tr style="background-color:#99c0ff; color: #000">
        <th style="padding: 10px; text-align: center;" width="20%">DEALER</th>
        <th style="padding: 10px; text-align: center;" width="20%">KONSULTAN</th>
        <th style="padding: 10px; text-align: center;" width="20%">TAM</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="padding: 40px"></td>
        <td style="padding: 40px"></td>
        <td style="padding: 40px"></td>
      </tr>
      <tr>
        <td style="padding: 5px">TANGGAL CHECK :</td>
        <td style="padding: 5px">TANGGAL CHECK :</td>
        <td style="padding: 5px">TANGGAL CHECK :</td>
      </tr>
    </tbody>
  </table><br><br>
  <div class="nextpage"></div>
<?php $n=0;foreach ($data_area as $row): $n++;?>

    <?php if($n>1):?>
      <div class="nextpage"></div>
    <?php endif;?>
       <h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f;font-weight: bold;"><?=html_escape($row['area_name'])?></h3>
<?php foreach ($row['sub'] as $sub):?>
       <h4 style="align-items: center; color: #FFF; padding: 10px; width: 40%; background-color: #000;font-weight: bold;"><?=html_escape($sub['position'])?>. <?=html_escape($sub['sub_area_name'])?></h4><br>
       <?php if(isset($sub['ic']) or isset($sub['kp']) or isset($sub['minreq']) or isset($sub['tps_line'])):?>

         <?php if (isset($sub['ic'])): ?>
                <table align="center" border="1" width="100%" style="border-collapse: collapse;">
                  <thead style="background-color:#99c0ff; color: #000">
                    <tr>
                      <th rowspan="2" style="text-align: center;">Item Check</th>
                      <th rowspan="2" style="text-align: center;">Kriteria</th>
                       <?php if ($sub['ic'][0]['model'] == 'Model 2'): ?>
                              <th  rowspan="2"style="text-align: center;">Jarak</th>
                       <?php endif;?>

                        <th colspan="3" style="text-align: center;" colspan="3">Di check oleh ("√" atau "X")</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">#1</th>
                        <th style="text-align: center;">#2</th>
                        <th style="text-align: center;">#3</th>
                    </tr>
                  </thead>
                  <tbody>
                <?php foreach ($sub['ic'] as $item): ?>
                       <?php
                      $check     = ['1' => '√', '0' => 'X', '' => ''];
                      $dataScore = [
                          'dealer'    => $check[$item['score_dealer']],
                          'konsultan' => $check[$item['score_konsultan']],
                          'tam'       => $check[$item['score_tam']],
                          
                      ];
                      ?>
                       <tr>
                              <td style="padding: 7px"><?=html_escape($item['item_name'])?></td>
                              <td style="padding: 7px"><?=html_escape($item['kriteria'])?></td>
                              <?php if ($item['model'] == 'Model 2'): ?>
                                     <td style="padding: 7px"><?=html_escape($item['jarak'])?></td>
                              <?php endif;?>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                       </tr>
                <?php endforeach;?>
                  </tbody>
                </table>
                <br/>
                <br/>
         <?php endif;?>

         <h3>Layout Photo :</h3>
         <div style="width: 100%;">
         <?php if (isset($sub['pic'])): ?>
                <?php foreach ($sub['pic'] as $pic): ?>
                       <?php if ($pic['type'] == 'layout'): ?>
                              <div style="padding: 10px; vertical-align: middle; display : inline">
                                <?php print_image($pic['file_path']) ?>
                              </div>
                       <?php endif;?>
                <?php endforeach;?>
         <?php else:?>
          <center>No Picture Found</center>
         <?php endif;?>
         </div>
         <br>
         
         <h3>Area Photo :</h3>
         <div style="width: 100%;">
         <?php if (isset($sub['pic'])): ?>
                <?php foreach ($sub['pic'] as $pic): ?>
                       <?php if ($pic['type'] == 'area'): ?>
                              <div style="padding: 10px; vertical-align: middle; display : inline">
                                     <?php print_image($pic['file_path']) ?>
                              </div>
                       <?php endif;?>
                <?php endforeach;?>
         <?php else:?>
          <center>No Picture Found</center>
         <?php endif;?>

         </div>
         <br>
         <br>
         <?php if (isset($sub['kp'])): ?>
                <table align="center" border="1" width="100%" style="border-collapse: collapse;" >
                  <thead style="background-color:#99c0ff; color: #000">
                    <tr>
                      <th rowspan="2" style="text-align: center;">No.</th>
                      <th rowspan="2" style="text-align: center;">Kriteria Pekerjaan</th>
                      <th colspan="3" style="text-align: center;" colspan="3">Di check oleh ("√" atau "X")</th>
                      <th rowspan="2" style="text-align: center;">Keterangan</th>
                    </tr>
                    <tr>
                        <th style="text-align: center;">#1</th>
                        <th style="text-align: center;">#2</th>
                        <th style="text-align: center;">#3</th>
                    </tr>
                  </thead>
                  <tbody>
                <?php foreach ($sub['kp'] as $item): ?>
                       <?php
                        $check     = ['1' => '√', '0' => 'X', '' => ''];
                        $dataScore = [
                            'dealer'    => $check[$item['score_dealer']],
                            'konsultan' => $check[$item['score_konsultan']],
                            'tam'       => $check[$item['score_tam']],
                        ];
                        ?>
                       <tr>
                              <td style="padding: 7px"><?=html_escape($item['position'])?></td>
                              <td style="padding: 7px"><?=html_escape($item['item_kriteria_pekerjaan'])?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_1]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_2]?></td>
                              <td width="7%" style="padding: 7px" align="center"><?=$dataScore[$type_level_3]?></td>
                              <td style="padding: 7px"><?=html_escape($item['keterangan'])?></td>
                       </tr>
                <?php endforeach;?>
                  </tbody>
                </table>
                <br/>
                <br/>
         <?php endif;?>
         <?php if (isset($sub['minreq'])): ?>
                <div class="solid" style="border: 2px solid #000; color: #000; padding: 5px;">
                       <b>Minimum Requirement</b>
                       <ul>
                       <?php foreach ($sub['minreq'] as $item): ?>
                              <li><?=html_escape($item['item_minreq'])?></li>
                       <?php endforeach;?>
                       </ul>
                </div>
                <br/><br/>
         <?php endif;?>

         <?php if (isset($sub['tps_line'])): ?>
                <div class="solid" style="border: 2px solid #000; color: #000; padding: 5px;">
                       <b>TPS LINE</b>
                       <ul>
                       <?php foreach ($sub['tps_line'] as $item): ?>
                              <li><?=html_escape($item['item_tps_line'])?></li>
                       <?php endforeach;?>
                       </ul>
                </div>
                <br/><br/>
         <?php endif;?>

       <?php else:?>
        <center>Data Not Found</center>
       <?php endif;?>

        

       <?php if (isset($sub['ket'])){ ?>
              <div  class="solid" style="border: 2px solid #000; color: #000; padding: 5px;">
                     <b>Keterangan</b>
                     <ol>
                     <?php foreach ($sub['ket'] as $item): ?>
                            <li><?=html_escape($item['item_ket'])?></li>
                     <?php endforeach;?>
                     </ol>
              </div>
              <br/><br/>
       <?php }else{ ?>
              <div  class="solid" style="border: 2px solid #000; color: #000; padding: 5px; height: 100px;">
                     <b>Keterangan</b>
                     
              </div>
              <br/><br/>
       <?php } ?>

       <?php if (isset($sub['catatan'])): ?>
          <?php if ((isset($sub['catatan']['dealer']) and trim($sub['catatan']['dealer'])!='') or (isset($sub['catatan']['konsultan']) and trim($sub['catatan']['konsultan'])!='') or (isset($sub['catatan']['tam']) and trim($sub['catatan']['tam'])!='')):?>
              <div  class="solid" style="border: 2px solid #000; color: #000; padding: 5px;">
                    <b>Catatan :</b><br/>
                     <?=((isset($sub['catatan']['dealer']) and trim($sub['catatan']['dealer'])!='') ? 'Dealer : '.$sub['catatan']['dealer'].'<br/>' : '')?>
                     <?=((isset($sub['catatan']['konsultan']) and trim($sub['catatan']['konsultan'])!='') ? 'Konsultan : '.$sub['catatan']['konsultan'].'<br/>' : '')?>
                     <?=((isset($sub['catatan']['tam']) and trim($sub['catatan']['tam'])!='') ? 'TAM : '.$sub['catatan']['tam'].'<br/>' : '')?>
              </div>
              <br/><br/>
            <?php endif;?>
       <?php endif;?>
       
       
<?php endforeach;?>
       
<?php endforeach;?>
<h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f"><b>Equipment & Tools</b></h3>
       <table align="center" border="1" width="100%" style="border-collapse: collapse;">
              <tr>
                     <th style="padding: 10px">No</th>
                     <th style="padding: 10px" colspan="2">Equipment &amp; Tools</th>
                     <th style="padding: 10px">Belum Order</th>
                     <th style="padding: 10px">Order</th>
                     <th style="padding: 10px">Tanggal Order</th>
                     <th style="padding: 10px">Material Onsite</th>
                     <th style="padding: 10px">Terpasang</th>
                     <th style="padding: 10px">Keterangan</th>
                     <?php $check = ['1' => '√', '0' => '-', '' => ''];?>
                     <?php if(count($eat)>0):?>
                     <?php foreach ($eat as $row): ?>
                            <tr>
                               <td style="padding: 10px"><?=html_escape($row['position'])?></td>
                               <td style="padding: 10px"><?=html_escape($row['group_name'])?></td>
                               <td style="padding: 10px"><?=html_escape($row['item_name'])?></td>
                               <td style="padding: 10px"><?=$check[$row['belum_order_score']]?></td>
                               <td style="padding: 10px"><?=$check[$row['order_score']]?></td>
                               <td style="padding: 10px"><?=date('d-m-Y',strtotime($row['tanggal_order']))?></td>
                               <td style="padding: 10px"><?=$check[$row['material_onsite_score']]?></td>
                               <td style="padding: 10px"><?=$check[$row['terpasang_score']]?></td>
                               <td style="padding: 10px"><?=html_escape($row['keterangan'])?></td>
                           </tr>
                     <?php endforeach;?>
                     <?php else:?>
                        <tr><td align="center" colspan="9">Data Not found</td></tr>
                     <?php endif;?>
              </tr>
       </table>
       <br/>
       <br/>
       <h3 style="border: 2px solid #000; padding: 5px; width: 40%; background-color: #fcf40f"><b>Man Power</b></h3>
       <table align="center" border="1" width="100%" style="border-collapse: collapse;">
              <tr>
                  <th style="padding: 10px">No</th>
                  <th style="padding: 10px">Posisi</th>
                  <th style="padding: 10px">Nama</th>
                  <th style="padding: 10px">Level Training</th>
                  <th style="padding: 10px">Rotasi/Mutasi (V)</th>
                  <th style="padding: 10px">Cabang Sumber</th>
                  <th style="padding: 10px">Keterangan</th>
              </tr>
              <?php $check = ['1' => '√', '0' => '-', '' => ''];$n=0;?>
              <?php if(count($manpower)>0):?>
                <?php foreach ($manpower as $row): $n++;?>
                       <tr>
                          <td style="padding: 10px"><?=$n?></td>
                          <td style="padding: 10px"><?=html_escape($row['posisi'])?></td>
                          <td style="padding: 10px"><?=html_escape($row['nama'])?></td>
                          <td style="padding: 10px"><?=html_escape($row['level_training_name'])?></td>
                          <td style="padding: 10px"><?=$check[$row['rotasi_mutasi_score']]?></td>
                          <td style="padding: 10px"><?=html_escape($row['cabang_sumber'])?></td>
                          <td style="padding: 10px"><?=html_escape($row['keterangan'])?></td>
                       </tr>
                <?php endforeach;?>
               <?php else:?>
                  <tr><td align="center" colspan="7">Data Not found</td></tr>
               <?php endif;?>
       </table>