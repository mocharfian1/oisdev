<?php

/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Ayub Anggara
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */


$username = $this->session->userdata('username');
$email = $this->session->userdata('email');
$user_type = $this->session->userdata('user_type');

if(empty($username) || empty($email) || empty($user_type)){
    redirect('login');
}else{
    if($user_type == 'konsultan' || $user_type == 'dealer'){
        redirect('login');    
    }
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>OIS</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />

        <meta content="" name="author" />
        <!-- <input type="text" id="base_url" class="base_url" value="<?php echo base_url(); ?>" style="display: none"/> -->
        <script>
            var base_url = '<?php echo base_url(); ?>';
            paceOptions = {
                ajax: {ignoreURLs: [base_url+'connection_test']}
            }
        </script>

         <!-- BEGIN PAGE FIRST SCRIPTS -->
        <script src="<?=base_url()?>assets/global/plugins/pace/pace.min.js" type="text/javascript"></script>
        <!-- END PAGE FIRST SCRIPTS -->
        <!-- BEGIN PAGE TOP STYLES -->
        <link href="<?=base_url()?>assets/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE TOP STYLES -->


        <meta content="YK Digital" name="author" />
        <link rel="icon" type="image/png" href="<?=base_url('assets/global/img/favicon/')?>favicon-toyota.png" sizes="32x32" />

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

        <link href="<?=base_url()?>assets/global/plugins/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?=base_url()?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?=base_url()?>assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />

        <link href="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />

        <!-- offline js -->
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-theme-chrome.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/global/plugins/offline-js/themes/offline-language-english.css" rel="stylesheet" type="text/css" />
        <!-- holdon js -->
        <link href="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

        <?php if($plugin_mode=='1'){ ?>

        <?php }else if($plugin_mode=='2'){ ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="<?=base_url()?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->

        <?php }else if($plugin_mode=='3'){ ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="<?=base_url()?>assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->

        <?php }else if($plugin_mode=='4'){ ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="<?=base_url()?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->

        <?php } ?>

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?=base_url()?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?=base_url()?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->


        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?=base_url()?>assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>assets/layouts/layout2/css/themes/dark.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?=base_url()?>assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->


        <link rel="shortcut icon" href="#" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <input type="text" id="isHoldOnOpen" value="false" style="display: none">
        
        <!-- BEGIN HEADER -->
        <?php $this->load->view('tpl_header') ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view('tpl_sidebar') ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <?php $this->load->view($content) ?>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->

            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2018 &copy; Online Inspection System Admin V1.0 By Toyota Astra Motor
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="<?=base_url()?>assets/global/plugins/respond.min.js"></script>
<script src="<?=base_url()?>assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

        <script src="<?=base_url()?>assets/global/plugins/jquery-confirm/jquery-confirm.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-toggle.min.js"></script>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?=base_url()?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>

        

        <script src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

        <!-- offline js -->
        <script src="<?=base_url()?>assets/global/plugins/offline-js/offline.min.js" type="text/javascript"></script>
        <!-- holdon js -->
        <script src="<?=base_url()?>assets/global/plugins/holdon-js/HoldOn.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <?php if($plugin_mode == '1'){ ?>

            <script src="<?=base_url()?>assets/scripts/sweetalert.min.js" type="text/javascript"></script>

        <?php }else if($plugin_mode == '2'){ ?>

            <!-- TAMBAH SWEET ALERT YA -->
            <script src="<?=base_url()?>assets/scripts/sweetalert.min.js" type="text/javascript"></script>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="<?=base_url()?>assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?=base_url()?>assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->

        <?php }else if($plugin_mode == '3'){ ?>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="<?=base_url()?>assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?=base_url()?>assets/pages/scripts/form-icheck.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->

        <?php }else if($plugin_mode == '4'){ ?>

            <!-- TAMBAH SWEET ALERT YA -->
            <script src="<?=base_url()?>assets/scripts/sweetalert.min.js" type="text/javascript"></script>

            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="<?=base_url()?>assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
            <script src="<?=base_url()?>assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->

            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="<?=base_url()?>assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->

            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkT5BSWYhFpd-qliznJjlMtQP-L5A9LJg&libraries=places&callback=initMap" async defer></script>

        <?php } ?>

        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?=base_url()?>assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
        <!-- <script src="<?=base_url()?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script> -->
        <!-- END THEME LAYOUT SCRIPTS -->

        <!-- BEGIN CUSTOM SCRIPT -->
        <script type="text/javascript">
            var csrf = {
                name:'<?php echo $this->security->get_csrf_token_name();?>',
                token:'<?php echo $this->security->get_csrf_hash(); ?>',
            };
            var burl='<?=base_url()?>';
        </script>
        <script src="<?=base_url('assets/scripts/'.$tpl_js.'.js')?>"></script>
        <!-- END CUSTOM SCRIPT -->

        <script type="text/javascript">
            
            function PopupCenter(url, title, w, h) {
              var left = (screen.width/2)-(w/2);
              
              var top = (screen.height/2)-(h/2);
              
              return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+10+', left='+left);
            }

            function number_format_dots(x) {
                  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            }

            function clear_format_dots(x) {
                  return x.toString().replace(/[.]/g, "");
            }

            function capitalizeFirstLetter(string) {
                  return string.charAt(0).toUpperCase() + string.slice(1);
            } 

            function todayIndDate(){
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();

                if(dd<10) {
                    dd = '0'+dd
                } 

                if(mm == 1){mm = 'Januari'};
                if(mm == 2){mm = 'Februari'};
                if(mm == 3){mm = 'Maret'};
                if(mm == 4){mm = 'April'};
                if(mm == 5){mm = 'Mei'};
                if(mm == 6){mm = 'Juni'};
                if(mm == 7){mm = 'Juli'};
                if(mm == 8){mm = 'Agustus'};
                if(mm == 9){mm = 'September'};
                if(mm == 10){mm = 'Oktober'};
                if(mm == 11){mm = 'November'};
                if(mm == 12){mm = 'Desember'};

                today = dd + ' ' + mm + ' ' + yyyy;

                return today;
            }

            function dateMySqlConvertMonthShort(date){
                var dateArr = date.split("-");

                var day = dateArr[2];
                var year = dateArr[0];
                var month = dateArr[1];

                if(month == 1){month = 'Januari'};
                if(month == 2){month = 'Februari'};
                if(month == 3){month = 'Maret'};
                if(month == 4){month = 'April'};
                if(month == 5){month = 'Mei'};
                if(month == 6){month = 'Juni'};
                if(month == 7){month = 'Juli'};
                if(month == 8){month = 'Agustus'};
                if(month == 9){month = 'September'};
                if(month == 10){month = 'Oktober'};
                if(month == 11){month = 'November'};
                if(month == 12){month = 'Desember'};

                var dateFormat = day +' '+ month +' '+ year;
                return dateFormat;
            }

            //offline js with hold on js for network down error handling
            var run = function(){
            
              var req = new XMLHttpRequest();
              req.timeout = 5000;
              req.open('GET', base_url+'connection_test', true);
              req.send();

                Offline.on('confirmed-down', function () {
                   
                        if($('#isHoldOnOpen').val() == 'true'){

                        }else if($('#isHoldOnOpen').val() == 'false'){
                            console.log('open holdon');
                            $('#isHoldOnOpen').val('true');
                            HoldOn.open();
                        }
                        
                });

                Offline.on('confirmed-up', function () {

                    if($('#isHoldOnOpen').val() == 'true'){
                        console.log('open holdon');
                        $('#isHoldOnOpen').val('false');
                        HoldOn.close();
                    }

                });


            }

            setInterval(run, 3000);
            // end of offline js with hold on js for network down error handling

            $(document).ready(function(){
                $('.buttons-print').hide();
                $('.buttons-pdf').hide();
                $('.buttons-csv').hide();
            });

            //filter input
            $(document).on('keydown', '.alpha', function(e) {
                            var a = e.key;
                            if (a.length == 1) return /[a-z]|\$|#|\*/i.test(a);
                            return true;
                        })
                        .on('keydown', '.numeric', function(e) {
                            var a = e.key;
                            if (a.length == 1) return /[0-9]|\+|-/.test(a);
                            return true;
                        })
                        .on('keydown', '.alphanumeric', function(e) {
                            var a = e.key;
                            if (a.length == 1) return /[a-z]|[0-9]|&/i.test(a);
                            return true;
                        })
                        .on('keydown', '.email', function(e) {
                            var a = e.key;
                            if (a.length == 1) return /[a-z]|[0-9]|[@]|[.]|&/i.test(a);
                            return true;
                        })
        </script>

    </body>

</html>
