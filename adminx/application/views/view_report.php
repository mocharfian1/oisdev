
<?php if($sub_menu == 'buku_epm'){ ?>

  <style type="text/css">
    .loaderAnim {
      border: 16px solid #f3f3f3;
      border-radius: 50% !important;
      border-top: 16px solid blue;
      border-bottom: 16px solid blue;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>
  <div class="page-content">
      <!-- END THEME PANEL -->
      <h3 class="page-title"> Buku EPM
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Report</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Buku EPM</a>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-list font-dark"></i>
                            <span class="caption-subject bold uppercase">Report Filter</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                      <div class="form-group">
                        <center>
                          <!-- <form action="<?php echo base_url("Report/get_report") ?>" method="POST"> -->
                            <table>
                              <tr>
                                <td>
                                  
                                  <select id="project" name="project" class="form-control select2" style="width: 200px">
                                    <option disabled style="display: none;" selected="">Select Project</option>
                                    <?php foreach ($data_project as $row) { ?>
                                      <option value="<?php echo $row['id']; ?>"><?=html_escape($row['nama_outlet'])?></option>
                                    <?php } ?>

                                  </select>
                                  
                                </td>
                                <td style="padding-left: 20px">
                                  
                                  <select id="jenis_epm" name="jenis_epm" class="form-control" style="width: 150px">
                                    <option disabled style="display: none;" val="" selected="">Select EPM</option>
                                    <option value="50">50%</option>
                                    <option value="75">75%</option>
                                    <option value="100">100%</option>
                                  </select>
                                  
                                </td>
                               <!--  <td style="padding-left: 20px">
                                  
                                </td> -->
                              </tr>
                            </table><br>

                            <div id="chapter_select_wrapper">
                              <!-- <select id="chapter" name="chapter" class="form-control" style="width: 200px" disabled="true">
                                <option disabled style="display: none; color: grey" val="" selected="">Select Chapter</option>
                                <option value="lbr_pernyataan">Lembar Pernyataan</option>
                                <option value="bab1">Bab I</option>
                                <option value="bab2">Bab II</option>
                                <option value="bab3">Bab III</option>
                                <option value="bab4">Bab IV</option>
                                <option value="bab5">Bab V</option>
                                <option value="bab6">Bab VI</option>
                                <option value="bab7">Bab VII</option>
                                <option value="bab8">Bab VIII</option>
                              </select> -->
                            </div>
                            

                           
                            <br>
                            <!-- <a href="<?php echo base_url('assets/report_example.pdf'); ?>" class="btn green" target="_blank">Generate PDF</a> -->
                            <div id="reportbutton">
                                <button class="btn green" id="btnGeneratePdf" onclick="reportPdf();">Generate PDF</button>
                                <button class="btn green" id="btnGenerateHtml" onclick="reportHtml();">Display Report</button>
                            </div>
                            <a class="btn green" id="downloadbtn" style='display: none;' href="" target="_blank">Download</a>
                            
                          <!-- </form> -->
                        </center>
                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div id="report" class="portlet light">
              <div style="margin: 50px" id="reportWrapper">
                <center>
                  <div id="loaderWrapper" style="display: none">
                    <div class="loaderAnim"></div>
                    <h3>Please wait...</h3>
                  </div>
                  
                </center>
                <div id="reportContentWrapper">
                    <h1 style="text-align: center;">No report to display</h1>
                  </div>
              </div>
          
            </div>
          
        </div>
    </div>
  </div>

<?php } ?>

<?php if($sub_menu == 'checklist_summary'){ ?>

  <style type="text/css">
    .loaderAnim {
      border: 16px solid #f3f3f3;
      border-radius: 50% !important;
      border-top: 16px solid blue;
      border-bottom: 16px solid blue;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>
  <div class="page-content">
      <!-- END THEME PANEL -->
      <h3 class="page-title"> Checklist Summary
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Report</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Checklist Summary</a>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>

      </div>
      <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-list font-dark"></i>
                        <span class="caption-subject bold uppercase">Report Filter</span>
                    </div>
                </div>
                <div class="portlet-body">
                  <div class="form-group">
                    <center>
                      <!-- <form action="<?php echo base_url("Report/get_report") ?>" method="POST"> -->
                        <table>
                          <tr>
                            <td>
                              <center>
                              <select id="project" name="project" class="form-control select2" style="width: 200px">
                                <option disabled style="display: none" selected="">--Select Project--</option>
                                <?php foreach ($data_project as $row) { ?>
                                  <option value="<?php echo $row['id']; ?>"><?=html_escape($row['nama_outlet'])?></option>
                                <?php } ?>

                              </select>
                              </center>
                            </td>
                            <td style="padding-left: 20px">
                              <center>
                              <select id="jenis_epm" name="jenis_epm" class="form-control select2" style="width: 150px">
                                <option disabled style="display: none" selected="">--Select EPM--</option>
                                <option value="50">50%</option>
                                <option value="75">75%</option>
                                <option value="100">100%</option>
                              </select>
                              </center>
                            </td>
                          </tr>
                        </table>
                        

                       
                        <br>
                        <!-- <a href="<?php echo base_url('assets/report_example.pdf'); ?>" class="btn green" target="_blank">Generate PDF</a> -->
                        <button class="btn green" id="btnGeneratePdf" onclick="reportPdf();">Generate PDF</button>
                        <button class="btn green" id="btnGenerateHtml" onclick="reportHtml();">Display Report</button>
                      <!-- </form> -->
                    </center>
                  </div>
                </div>

            <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div id="report" class="portlet light">
              <div style="margin: 50px" id="reportWrapper">
                <center>
                  <div id="loaderWrapper" style="display: none">
                    <div class="loaderAnim"></div>
                    <h3>Please wait...</h3>
                  </div>
                  
                </center>
                <div id="reportContentWrapper">
                    <h1 style="text-align: center;">No report to display</h1>
                  </div>
              </div>
          
            </div>
        
        </div>
      </div>
  </div>

<?php } ?>

<?php if($sub_menu == 'catatan_evaluasi'){ ?>

  <style type="text/css">
    .loaderAnim {
      border: 16px solid #f3f3f3;
      border-radius: 50% !important;
      border-top: 16px solid blue;
      border-bottom: 16px solid blue;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>
  <div class="page-content">
      <!-- END THEME PANEL -->
      <h3 class="page-title"> Catatan Evaluasi
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
          <ul class="page-breadcrumb">
              <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Home</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Report</a>
                  <i class="fa fa-angle-right"></i>
              </li>
              <li>
                  <a href="javascript:;">Catatan Evaluasi</a>
              </li>

          </ul>
          <div class="page-toolbar">

          </div>

      </div>
      <div class="row">
            <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light">
                  <div class="portlet-title">
                      <div class="caption font-dark">
                          <i class="icon-list font-dark"></i>
                          <span class="caption-subject bold uppercase">Report Filter</span>
                      </div>
                  </div>
                  <div class="portlet-body">
                    <div class="form-group">
                      <center>
                        <!-- <form action="<?php echo base_url("Report/get_report") ?>" method="POST"> -->
                          <table>
                            <tr>
                              <td>
                                <center>
                                <input type="hidden" id="jenis_epm" name="jenis_epm">
                                <select id="project" name="project" class="form-control select2" style="width: 200px">
                                  <option disabled style="display: none" selected="">--Select Project--</option>
                                  <?php foreach ($data_evaluasi as $row) { ?>
                                    <option data-id="<?php echo $row['jenis_epm']; ?>" value="<?php echo $row['id_project']; ?>"><?php echo $row['nama_outlet']; echo " - "; echo $row['jenis_epm']; ?>%</option>
                                  <?php } ?>

                                </select>
                                </center>
                              </td>
                            </tr>
                          </table>
                          

                         
                          <br>
                          <!-- <a href="<?php echo base_url('assets/report_example.pdf'); ?>" class="btn green" target="_blank">Generate PDF</a> -->
                          <button class="btn green" id="btnGeneratePdf" onclick="reportPdf();">Generate PDF</button>
                          <button class="btn green" id="btnGenerateHtml" onclick="reportHtml();">Display Report</button>
                        <!-- </form> -->
                      </center>
                    </div>
                  </div>

              <!-- END EXAMPLE TABLE PORTLET-->
              </div>
              <div id="report" class="portlet light">
              <div style="margin: 50px" id="reportWrapper">
                <center>
                  <div id="loaderWrapper" style="display: none">
                    <div class="loaderAnim"></div>
                    <h3>Please wait...</h3>
                  </div>
                  
                </center>
                <div id="reportContentWrapper">
                    <h1 style="text-align: center;">No report to display</h1>
                </div>
              </div>
          
              </div>
          
            </div>
      </div>
  </div>

<?php } ?>

<?php if($sub_menu == 'toss'){ ?>
  <script type="text/javascript">var URL = '<?php echo base_url(); ?>';</script>
  <style type="text/css">
    .loaderAnim {
      border: 16px solid #f3f3f3;
      border-radius: 50% !important;
      border-top: 16px solid blue;
      border-bottom: 16px solid blue;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
    }
  </style>
  <div class="page-content">
      <!-- END THEME PANEL -->
      <h3 class="page-title"> TOSS
          <!-- <small>first demo</small> -->
      </h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="javascript:;">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="javascript:;">Report</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="javascript:;">TOSS</a>
            </li>

        </ul>
        <div class="page-toolbar">

        </div>

      </div>
      <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-list font-dark"></i>
                        <span class="caption-subject bold uppercase">Report Filter</span>
                    </div>
                </div>
                <div class="portlet-body">
                  <div class="form-group">
                      <center>
                        <!-- <form action="<?php echo base_url("Report/get_report") ?>" method="POST"> -->
                          <table>
                            <tr>
                              <td>
                                <center>
                                <select id="project" name="project" class="form-control select2" style="width: 250px">
                                  <option disabled style="display: none" selected="">--Select TOSS--</option>
                                  <?php if(!empty($ls_dealer)){ ?>
                                      <?php foreach ($ls_dealer as $key => $value) { ?>
                                          <option value="<?php echo $value->id; ?>"><?=html_escape($value->dealer)?> (<?php echo $value->kota_kabupaten; ?>)</option>
                                      <?php } ?>
                                  <?php } ?>
                                </select>
                                </center>
                              </td>
                            </tr>
                          </table>
                         
                          <br>
                          <!-- <a href="<?php echo base_url('assets/report_example.pdf'); ?>" class="btn green" target="_blank">Generate PDF</a> -->
                          <button class="btn green" id="btnGeneratePdf" onclick="reportPdf($('#project').val());">Generate PDF</button>
                          <button class="btn green" id="btnGenerateHtml" onclick="reportHtml($('#project').val());">Display Report</button>
                        <!-- </form> -->
                      </center>
                  </div>
                </div>

            <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div id="report" class="portlet light">
              <div style="margin: 50px" id="reportWrapper">
                <center>
                  <div id="loaderWrapper" style="display: none">
                    <div class="loaderAnim"></div>
                    <h3>Please wait...</h3>
                  </div>
                  
                </center>
                <div id="reportContentWrapper">
                    <h1 style="text-align: center;">No report to display</h1>
                </div>
              </div>
          
            </div>
          
        </div>
      </div>
  </div>

<?php } ?>