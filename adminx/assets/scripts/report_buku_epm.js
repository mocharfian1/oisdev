/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

var reportHtmlCover = '';
var reportHtmlContent = '';
var reportHtmlEat = '';
var reportHtmlManPower = '';
var reportHtmlData = '';


function loadingStart(){
    $('#reportContentWrapper').fadeOut();
    $('#loaderWrapper').fadeIn();
    $('#btnGeneratePdf').text('please wait..'); //change button text
    $('#btnGenerateHtml').text('please wait..'); //change button text
    $('#btnGeneratePdf').attr('disabled',true); //set button disable
    $('#btnGenerateHtml').attr('disabled',true); //set button disable
}

function loadingEnd(){
    // $('#reportContentWrapper').html('<h1>No report to display</h1>');
    $('#reportContentWrapper').fadeIn();
    $('#loaderWrapper').fadeOut();
    $('#btnGeneratePdf').text('Generate PDF'); //change button text
    $('#btnGenerateHtml').text('Display Report'); //change button text
    $('#btnGeneratePdf').attr('disabled',false); //set button enable
    $('#btnGenerateHtml').attr('disabled',false); //set button enable
}

function dateMySqlConvertForEPM(date){
    var dateArr = date.split("-");

    var day = dateArr[2];
    var year = dateArr[0];
    var month = dateArr[1];

    if(day >= 1 && day <=7){ day = 'W1';}
    if(day >= 8 && day <=14){ day = 'W2';}
    if(day >= 15 && day <=21){ day = 'W3';}
    if(day >=22){ day = 'W4';}

    if(month == 1){month = 'Januari'};
    if(month == 2){month = 'Februari'};
    if(month == 3){month = 'Maret'};
    if(month == 4){month = 'April'};
    if(month == 5){month = 'Mei'};
    if(month == 6){month = 'Juni'};
    if(month == 7){month = 'Juli'};
    if(month == 8){month = 'Agustus'};
    if(month == 9){month = 'September'};
    if(month == 10){month = 'Oktober'};
    if(month == 11){month = 'November'};
    if(month == 12){month = 'Desember'};


    var dateFormat = day +' '+ month +' '+ year;
    return dateFormat;
}

function reportPdf(){
    
    if($('#project').val() == '' || $('#project').val() == null || $('#jenis_epm').val() == '' || $('#jenis_epm').val() == null){
            swal('Plase select project and EPM');
    }else{

        var param = $('#project').val();
        var param2 = $('#jenis_epm').val();
        var param3 = $('#chapter').val();

        window.open(burl+'report/pdf_report/buku_epm/'+param+'/'+param2+'/'+param3);
    }
}

function reportHtml(){
    //clean content
    $('#reportContentWrapper').html('');

    if($('#project').val() == '' || $('#project').val() == null || $('#jenis_epm').val() == '' || $('#jenis_epm').val() == null){
        swal('Plase select project and EPM');
        $('#reportContentWrapper').html('<h1 style="text-align: center;">No report to display</h1>');
    }else{

        loadingStart();

        //empty data in variable
        reportHtmlCover = '';
        reportHtmlContent = '';
        reportHtmlEat = '';
        reportHtmlManPower = '';
        reportHtmlData = '';

        $('#reportContentWrapper').html('');
        if ($('#jenis_epm').val() != '100') {
            //createReportCover();
            showReport();
        } else {
            // alert($('#chapter').val());
            if ($('#chapter').val() == '' || $('#chapter').val() == null) {
                swal('Plase select Chapter first!');
                loadingEnd();
                $('#reportContentWrapper').html('<h1 style="text-align: center;">No report to display</h1>');
            } else {
                showReport100();
            }
        }
    }
    
}




function showReport(){
    var id= $('#project').val();
    var type= $('#jenis_epm').val();
    var data = {
        'id_project': id,
        'jenis_epm' : type
     };
     data[csrf.name]=csrf.token;
    loadingStart();
    $.ajax({
          url : burl+"report/print_report",
          type: "POST",
          data: data,
          success: function(content)
          {
            $('#reportContentWrapper').html(content);
            loadingEnd();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            loadingEnd();
            console.log('Error');
            console.log(jqXHR.responseText);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            swal('Error get data');
          }
    });
}
function showReport100(){
    var id= $('#project').val();
    var type= $('#jenis_epm').val();
    var chapter = $('#chapter').val();
    var data = {'id_project': id,'jenis_epm' : type, 'page' : chapter};
    data[csrf.name]=csrf.token;
    loadingStart();
    $.ajax({
          url : burl+"report/print_report_100",
          type: "POST",
          data: data,
          success: function(content)
          {
            $('#reportContentWrapper').html(content);
            loadingEnd();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            loadingEnd();
            console.log('Error');
            console.log(jqXHR.responseText);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            swal('Error get data');
          }
    });
}
function chapterselect(e){
    if($(e).val()=='dokumen'){
        $("#reportbutton").fadeOut();
        $("#downloadbtn").fadeIn();
    }else{
        $("#reportbutton").fadeIn();
        $("#downloadbtn").fadeOut();
    }
}
jQuery(document).ready(function() {
    $('.select2').select2();

    $('#jenis_epm, #project').on('change', function(){

        $("#reportbutton").fadeIn();
        $("#downloadbtn").fadeOut();
        if($('#project').val() !== null && $('#project').val() !== '' && $('#jenis_epm').val() !== null && $('#jenis_epm').val() !== '' && $('#jenis_epm').val() == '100'){
            $("#chapter_select_wrapper").html('');
            loadingStart();
            var id= $('#project').val();
            var type= $('#jenis_epm').val();
            var data = {'id_project': id,'jenis_epm' : type};
            data[csrf.name]=csrf.token;
            $.ajax({
                  url : burl+"report/get_bab",
                  type: "POST",
                  data: data,
                  dataType: "JSON",
                  success: function(data)
                  {
                    console.log(data);
                    bab_options="";
                    for(i in data.bab){
                        bab_options+='<option value="'+data.bab[i].id+'"> Bab '+data.bab[i].label+'</option>';
                    }
                    $("#chapter_select_wrapper").html(''+
                        '<select id="chapter" name="chapter" onchange="chapterselect(this)" class="form-control" style="width: 200px" disabled="true">' +
                        '    <option disabled style="display: none" val="" selected="">Select Chapter</option>' +
                        '    <option value="lbr_pernyataan">Lembar Pernyataan</option>' +
                        '    <option value="bab1">Bab I</option>' +
                        '    <option value="bab2">Bab II</option>' +
                        '    <option value="bab3">Bab III</option>' + bab_options+
                        '    <option value="last">Bab '+data.nextbab+'</option>'+
                        '    <option value="dokumen">Dokumen Pendukung</option>'+
                        '</select>');
                    if(data.file!=''){
                        $("#downloadbtn").attr("href",data.file);  
                         $('#downloadbtn').removeAttr('disabled');
                    }else{
                        $('#downloadbtn').attr('disabled',true); //set button disable
                        //$('#downloadbtn').html('disabled',true); //set button disable
                    }
                    $("#chapter").prop('disabled', false);
                    $("#chapter_select_wrapper").fadeIn();
                    loadingEnd();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    loadingEnd();
                    console.log('Error');
                    console.log(jqXHR.responseText);
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    swal('Error get data');
                  }
            });
        }else{
            $("#chapter_select_wrapper").fadeOut();
            $("#chapter_select_wrapper").html('');
        }
    });

    
});
