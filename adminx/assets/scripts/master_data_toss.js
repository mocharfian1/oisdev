/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Ayub Anggara
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 18 9 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */


//Start GMAP

function initMap() {

  var sub_menu_active = document.getElementById('sub_menu_active').value;
  var mode_active = document.getElementById('mode_active').value;

  if((sub_menu_active == 'guidance_toss' && mode_active == 'config') || (sub_menu_active == 'toss_database' && mode_active == 'add')){
    
    var marker = '';
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -4.402969, lng: 122.380632},
      zoom: 4,
      mapTypeId: 'roadmap',
      // mapTypeControl: false,
      streetViewControl: false,
      // zoomControl: false,
      // disableDoubleClickZoom: true,
      fullscreenControl: false,
      // fullscreenControlOptions : {
      //   position: google.maps.ControlPosition.RIGHT_BOTTOM
      // },
    });

    marker = new google.maps.Marker({
      map: map,
      draggable: true,
      position: {lat: -4.402969, lng: 122.380632}
    });

    // set lat lng to input text
    document.getElementById('latitude').value = '-4.402969';
    document.getElementById('longitude').value = '122.380632';

    //get client location and set map to view
    // if (navigator.geolocation) {
    //    navigator.geolocation.getCurrentPosition(function (position) {
    //        initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    //        map.setCenter(initialLocation);
    //    });
    // }

    google.maps.event.addListener(marker, 'dragend', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });

    google.maps.event.addListener(marker, 'dragstart', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });

    google.maps.event.addListener(marker, 'idle', function (evt) {
      document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
      document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
    });
   
    var input = document.getElementById('pac-input');
   
    var autocomplete = new google.maps.places.Autocomplete(input);
    
    //set input text inside map
    // map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

   
    

    autocomplete.addListener('place_changed', function() {
      
      // marker.setVisible(false);
      marker.setMap(null);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        // window.alert("No details available for input: '" + place.name + "'");
        // set alert with style if place not found

        $.alert("No details available for input: '" + place.name + "'");

        return;
      }

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      // marker.setPosition(place.geometry.location);
      // marker.setVisible(true);

        marker = new google.maps.Marker({
          map: map,
          draggable: true,
          position: place.geometry.location
        });

        document.getElementById('latitude').value = place.geometry.location.lat().toFixed(6);
        document.getElementById('longitude').value = place.geometry.location.lng().toFixed(6);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      
      google.maps.event.addListener(marker, 'dragend', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });

      google.maps.event.addListener(marker, 'dragstart', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });

      google.maps.event.addListener(marker, 'idle', function (evt) {
        document.getElementById('latitude').value = evt.latLng.lat().toFixed(6);
        document.getElementById('longitude').value = evt.latLng.lng().toFixed(6);
      });


    });

  }

}

function startLoading(x,all=null){
    if(all==1){
        $('#btnSave').attr('disabled','disabled');
        $('#btnSave').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnCancel').attr('disabled','disabled');
        $('#btnCancel').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');

        $('#btnDelete').attr('disabled','disabled');
        $('#btnDelete').append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }else{
        x.attr('disabled','disabled');
        x.append('<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
    }
}

function endLoading(x=null,all=null){
    if(all==1){
        $('#btnSave').removeAttr('disabled');
        $('#btnSave').find('span').remove();

        $('#btnCancel').removeAttr('disabled');
        $('#btnCancel').find('span').remove();

        $('#btnDelete').removeAttr('disabled');
        $('#btnDelete').find('span').remove();
    }else{
        x.removeAttr('disabled');
        x.find('span').remove();
      
    }
    // x.append('&nbsp;&nbsp;<span class="fa fa-circle-o-notch fa-spin"></span>');
}

//end of GMAP

//image code input file with thumb
function readURL2(input) {

    if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                if(input.id=="imgInput2"){
                    $('#imgHdr2').attr('src', e.target.result);
                    // if(!empty(input_64)){
                    // alert(e.target.result);
                    $('input[name="outlet_photo_file_name_64"]').attr('value',e.target.result);
                    // }
                }                 
                
            }
            
            reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInput2").change(function(){
    filterImg2(this);                        
});


var imgTmp;
var valSizeImg = 1;
var noImage = 1;

function sh_thumb(input,el) {
    if(noImage==1){
        el.attr('src', URL+'upload/default.jpg');
        el.attr('onclick','imagePreviewDialog("'+URL+'upload/default.jpg'+'")');
    }else{
        if (input.files && input.files[0]) {
                var reader = new FileReader();

                
                reader.onload = function (e) {
                      el.attr('src', e.target.result);
                      el.attr('onclick','imagePreviewDialog("'+e.target.result+'")');
                      noImage = 1;
                      imgTmp = null;
                }
                
                reader.readAsDataURL(input.files[0]);
        }
        //return input.files[0];
    }
}

function thumb_toss_db(input,el,is_eq=null) {
    if(noImage==1){
        el.attr('src', URL+'upload/default.jpg');
        el.attr('onclick','imagePreviewDialog("'+URL+'upload/default.jpg'+'")');
    }else{
        if (input.files && input.files[0]) {
                var reader = new FileReader();

                
                reader.onload = function (e) {
                      el.attr('src', e.target.result);
                      el.attr('onclick','imagePreviewDialog("'+e.target.result+'")');
                      // el.attr('onclick','imagePreviewDialog("'+e.target.result+'")');
                      if(is_eq==1){
                          $(input).parent().parent().find('.no_image').remove();
                          el.parent().append(`<br><div class="btn btn-danger btn-xs" onclick="del_img_eq(null,null,$(this))">Hapus Foto</div><br>`);
                      }else{
                          el.attr('is_change',1);
                      }
                      noImage = 1;
                      imgTmp = null;
                }
                
                reader.readAsDataURL(input.files[0]);
        }
        //return input.files[0];
    }
}

function ch_imgTmp(x,mode=null){
    valSizeImg = 1;
    noImage = 1;

    if(x.files.length==0){
        noImage = 1;
    }else{
        noImage = 0;
        console.log(x.files[0]);
        var type = [
                    'image/jpg',
                    'image/jpeg',
                    'image/png',
                    'image/bmp'
                    ];
        var t = type.indexOf(x.files[0].type);
        if(t>=0){
            if(x.files[0].size<=2000000){
              imgTmp = x;
              if(mode=='toss_database'){
                  thumb_toss_db(x,$(x).parent().parent().find('img.img-thumbnail'));
              }

              if(mode=='upload_eq'){
                  var key = new Date().getTime();
                  var id_eq_item = $(x).parent().attr('id_eq_item');
                  var group_key = $(x).parent().attr('group_key');
                  var item_key = $(x).parent().attr('item_key');

                  $(x).parent().parent().append(`
                      <div><img class="img-thumbnail img_eq" key=`+key+` style="max-width: 100px" id_eq_item="`+id_eq_item+`" group_key="`+group_key+`" item_key="`+item_key+`"></div>
                  `)
                  thumb_toss_db(x,$(x).parent().parent().find('img.img-thumbnail[key='+key+']'),1);
              }
            }else{
              valSizeImg = 0;
              $.alert('Ukuran gambar melebihi 2MB');
              if(mode=='toss_database'){
                $(x).parent().append(`<input type="file" name="input" style="display:none;" onchange="ch_imgTmp(this,'toss_database')">`);
                $(x).remove();
              }
            }
        }else{
            $.alert('Hanya boleh menggunakan format gambar (jpg,jpeg,png,bmp)');
            if(mode=='toss_database'){
                $(x).parent().append(`<input type="file" name="input" style="display:none;" onchange="ch_imgTmp(this,'toss_database')">`);
                $(x).remove();
            }
        }
    }
}


function filterImg2(fileElement){
  
    var file = $(fileElement).val();
    var splitStringFile = file.split(".");
    var arrayLength = splitStringFile.length;
    var extFile = splitStringFile[arrayLength - 1];

    if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'JPG' || extFile == 'JPEG' || extFile == 'PNG' || extFile == 'png') {
        
        if(fileElement.files[0].size < 2000000){  //size in KB
            readURL2(fileElement);
        }else{
            $(fileElement).val('');
            $.alert('File size is too big, please use a file with size no more than 2 MB in jpg or jpeg format.');
        }

    }else if($(fileElement).val() !== ''){
        $(fileElement).val('');
        $.alert('File type not supported');
    }
}
//end of image code input file with thumb for outlet in project add & edit
var Area = new Array();

function isAvail(x,sel){
          var ck_area = $('#__area').find('.'+x+'');
          var arrReturn = [];

          ck_area.toArray().forEach(function(item,index){
                    arrReturn.push(ck_area.eq(index).attr(sel));
          });

          return arrReturn;
}

function sortArea(){
    var arrNum = $('.num_area').toArray();
    arrNum.forEach(function(item,index){
          $('.num_area').eq(index).html(index+1);
    });

    var arrAllSub = $('.num_sub');
    arrAllSub.toArray().forEach(function(item,index){
      var id_area = arrAllSub.eq(index).attr('ar_id');
      var arrNumSub = $('.num_sub[ar_id="'+id_area+'"]').toArray();
      arrNumSub.forEach(function(it,i){
              $('.num_sub[ar_id="'+id_area+'"]').eq(i).html(i+1);
      });
    });
}

$(function(){
  $('input[name="target_ue_bulan_year"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('input[name="target_ue_hari_year"]').val(angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_1"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('input[name="target_ue_hari_year_plus_1"]').val(angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_2"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('input[name="target_ue_hari_year_plus_2"]').val(angka.toFixed(0));
    },10);
  });

  $('input[name="target_ue_bulan_year_plus_3"]').on('keydown change focusout click focus',function(e){
    var self = this;
    var num = new String;
    setTimeout(function(){
      num = $(self).val();
      let angka = num/22.5;
      $('input[name="target_ue_hari_year_plus_3"]').val(angka.toFixed(0));
    },10);
  });


  $('input[name="investasi_pembelian_lahan"],input[name="investasi_sewa_bangunan"],input[name="investasi_renovasi_bangunan"],input[name="investasi_corporate_identity"],input[name="investasi_equipment"],input[name="investasi_total"]').on('keydown',function(e){
    
    setTimeout(function(){
        var arrSUM = $('#tb_investasi').find('input.sum');
        var SUM_TOTAL = 0;
        arrSUM.toArray().forEach(function(item,index){
          let angka = isNaN(parseInt(arrSUM.eq(index).val()))?0:parseInt(arrSUM.eq(index).val());
          SUM_TOTAL += angka;
        });
        
        $('input[name="investasi_total"]').val(SUM_TOTAL);
        $('#investasi_total').html(SUM_TOTAL);
    },10);
  });
});

function addAreaDialog(){
  var ck_AV = isAvail('area_','id_area');
  $.confirm({
      title: 'Add Area',
      content: function(){
              var self = this;
              return $.get(URL+'toss_func/ls_area').done(function(data){
                    var dt = JSON.parse(data);

                    var konten = `<select id="ls_area" class="select2 form-control select2"><option disabled="disabled" selected="selected">- Select Area -</option>`;
                    self.setContent('Please choose area below. <br><br>');
                    
                    if(dt.length>0){
                          dt.forEach(function(item,index){
                                if(ck_AV.indexOf(item.id)>=0){
                                        //konten += `<option disabled="disabled" value='{"id":`+item.id+`,"area_name":"`+item.area_name+`"}'>`+item.area_name+` (Sudah ada di Daftar)</option>`;
                                }else{
                                        konten += `<option value='{"id":`+item.id+`,"area_name":"`+item.area_name+`"}'>`+item.area_name+`</option>`;
                                }
                          });
                    }

                    konten += `</select><script>$('#ls_area').select2();</script>`;
                    self.setContentAppend(konten);

              }).fail(function(){});
      },
      buttons: {
          add: {
              text: 'Confirm',
              btnClass: 'btn-blue',
              keys: ['enter'],
              action: function(){
                    var data = JSON.parse(this.$content.find('select').val());
                    
                    $('#__area').append(`
                                <div class="area_"  id_area="`+data.id+`" status="from_local">
                                      <div class="form-group form-control-inline">
                                          <div class="col-md-12">
                                              <table>
                                                  <tr>
                                                      <td><h4 style="margin-left:15px;padding-right: 30px;">D<span class="num_area"></span>. `+data.area_name+`</h4></td>
                                                      <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addSubAreaDialog(`+data.id+`,$(this));"> Add Sub Area </a></td>
                                                      <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_area',null,null,$(this));"> Delete </a></td>
                                                  </tr>
                                              </table>
                                          </div>
                                      </div>
                                      <hr>
                                </div>
                    `);

                    sortArea();
                    
              }
          },
          cancel: {
              text: 'Cancel',
              btnClass: 'btn-red',
              action: function(){

              }
          }
      }
  });
}

function addSubAreaDialog(id_area=null,el=null,token_area=null){
      var area = el.closest('tr').find('.num_area').html();
      var ck_AV = isAvail('sub_area_','id_sub');

      $.confirm({
          title: 'Add Sub Area',
          content: function(){
                  var self = this;
                  return $.get(URL+'toss_func/ls_sub_area/'+id_area).done(function(data){
                        var dt = JSON.parse(data);

                        var konten = `<select id="ls_sub_area" class="form-control select2"><option disabled="disabled" selected="selected">- Select Sub Area -</option>`;
                        self.setContent('Please choose area below. <br><br>');
                        
                        if(dt.length>0){
                              dt.forEach(function(item,index){
                                    if(ck_AV.indexOf(item.id)>=0){
                                            //konten += `<option disabled="disabled" value='{"id":`+item.id+`,"sub_area_name":"`+item.sub_area_name+`"}'>`+item.sub_area_name+` (Sudah ada di Daftar)</option>`;
                                    }else{
                                            konten += `<option value='{"id":`+item.id+`,"sub_area_name":"`+item.sub_area_name+`"}'>`+item.sub_area_name+`</option>`;
                                    }
                              });
                        }

                        konten += `</select><script>$('#ls_sub_area').select2();</script>`;
                        self.setContentAppend(konten);

                  }).fail(function(){});
          },
          buttons: {
              add: {
                  text: 'Confirm',
                  btnClass: 'btn-blue',
                  keys: ['enter'],
                  action: function(){
                        var data_sub = JSON.parse(this.$content.find('select').val());
                        // var token_sub = new Date().getTime();
                        el.closest('.area_').append(`
                                    <div class="sub_area_" id_sub="`+data_sub.id+`" status="from_local">
                                          <div class="form-group">
                                            <div class="col-md-12">
                                                <table>
                                                    <tr>
                                                        <td><h5 style="margin-left:15px;padding-right: 30px"><span class="num_sub" ar_id=`+id_area+`></span>. `+data_sub.sub_area_name+`</h5></td>
                                                        <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('sub_area',`+id_area+`,`+data_sub.id+`,$(this));"> Add Item </a></td>
                                                        <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_sub_area',null,null,$(this));"> Delete </a></td>
                                                    </tr>
                                                </table>
                                            </div>
                                          </div>
                                          <hr>
                                    </div>
                        `);


                        sortArea();
                        
                  }
              },
              cancel: {
                  text: 'Cancel',
                  btnClass: 'btn-red',
                  action: function(){
                     
                  }
              }
          }
      });
}

var itemsArea = new Array();
function addItemDialog(mode,id_area=null,id_sub=null,el=null,key_eq=null,group_name=null){


  if(mode == 'sub_area'){

    var dialogContent = '<form>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Kriteria Minimum</label>' +
                        '    <textarea class="form-control" name="kriteria_minimum" style="resize: none"></textarea>'+
                        '  </div>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Jumlah</label>' +
                        '    <input type="text" value="" name="jumlah" class="name form-control" required/>'+
                        '  </div>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Foto Contoh</label>' +
                        '    <input type="file"  id="fotoitem_" name="foto" class="form-control" onchange="ch_imgTmp(this)" required />'+
                        '  </div>' +
                        '</form>';

    $.confirm({
        title: 'Add Sub Area Item',
        content: dialogContent,
        columnClass: 'medium',
        buttons: {

            add: {
                text: 'add',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function(){
                      if(valSizeImg==0){
                          $.alert('Ukuran gambar melebihi 2MB.');
                          return false;
                      }


                      var self = this;

                      var kriteria_minimum = self.$content.find('textarea[name="kriteria_minimum"]').val();
                      var jumlah = self.$content.find('input[name="jumlah"]').val();
                      var url_foto = self.$content.find('input[name="foto"]').val();
                      var t_img = new Date().getTime();


                      var item = el.closest('.sub_area_').find('table.tb_sub_item tbody');
                      var ck_item= el.closest('.sub_area_').find('table.tb_sub_item').length;
                      // var token_item = new Date().getTime();
                      if(ck_item>0){
                            item.append(`<tr class="items__" id_area="`+id_area+`" id_sub="`+id_sub+`" status="from_local">
                                                    <td>`+kriteria_minimum+`</td>
                                                    <td>`+jumlah+`</td>
                                                    <td>
                                                        <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" style="max-width: 100px"></a>
                                                    </td>
                                                    <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item',null,null,$(this));" data-toggle="dropdown"> Delete </a></td>
                                                </tr>`);

                      }else{
                            el.closest('.sub_area_').append(`
                                  <div style="margin-left:15px;" status="form-group col-sm-12">
                                        <table class="table table-striped table-hover tb_sub_item">
                                                <thead>
                                                        <tr>
                                                            <th>Kriteria Minimum</th>
                                                            <th style="width: 5%">Jumlah</th>
                                                            <th>Foto Contoh</th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                        <tr class="items__" id_area="`+id_area+`" id_sub="`+id_sub+`" status="from_local">
                                                            <td>`+kriteria_minimum+`</td>
                                                            <td>`+jumlah+`</td>
                                                            <td>
                                                                <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" style="max-width: 100px"></a>
                                                            </td>
                                                            <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item',null,null,$(this));" data-toggle="dropdown"> Delete </a></td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                  </div>`);
                            
                      }

                      sh_thumb(imgTmp,$('img.img-thumbnail.token-'+t_img));
                      sortArea();

                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                   
                }
            }
        }
    });

  }

  if(mode == 'equipment'){

    var dialogContent = '<form>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Kriteria Minimum</label>' +
                        '    <textarea name="kriteria_minimum" class="form-control" style="resize: none"></textarea>'+
                        '  </div>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Jumlah Min.</label>' +
                        '    <input name="jumlah_minimum" type="text" value="" class="name form-control" required/>'+
                        '  </div>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Foto Contoh</label>' +
                        '    <input name="foto" type="file" class="form-control" onchange="ch_imgTmp(this)" required />'+
                        '  </div>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Keterangan</label>' +
                        '    <input name="keterangan" type="text" value="" class="name form-control" required/>'+
                        '  </div>' +
                        '</form>';

    $.confirm({
        title: 'Add Equipment Item',
        content: dialogContent,
        columnClass: 'medium',
        buttons: {
            // confirm: function () {
            //     $.alert('Confirmed!');
            // },
            add: {
                text: 'add',
                btnClass: 'btn-blue',
                keys: ['enter'],
                action: function(){
                      if(valSizeImg==0){
                          $.alert('Ukuran gambar melebihi 2MB.');
                          return false;
                      }

                      var self = this;

                      var kriteria_minimum = self.$content.find('textarea[name="kriteria_minimum"]').val();
                      var jumlah = self.$content.find('input[name="jumlah_minimum"]').val();
                      var url_foto = self.$content.find('input[name="foto"]').val();
                      var keterangan = self.$content.find('input[name="keterangan"]').val();
                      var t_img = new Date().getTime();


                      var item = el.closest('.equipment_').find('table.tb_group_item tbody');
                      var ck_item= el.closest('.equipment_').find('table.tb_group_item').length;
                      if(ck_item>0){
                            item.append(`<tr key="`+key_eq+`" group_name="`+group_name+`" class="eq_items__" status="from_local">
                                                            <td>`+kriteria_minimum+`</td>
                                                            <td>`+jumlah+`</td>
                                                            <td>
                                                                <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" style="max-width: 100px"></a>
                                                            </td>
                                                            <td>`+keterangan+`</td>
                                                            <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item','','',$(this),'yes');" data-toggle="dropdown"> Delete </a></td>
                                                        </tr>`);
                            sh_thumb(imgTmp,$('img.img-thumbnail.token-'+t_img));

                      }else{
                            el.closest('.equipment_').append(`
                                  <div  style="margin-left:15px;">
                                        <table class="table table-striped table-hover tb_group_item">
                                                <thead>
                                                        <tr>
                                                            <th>Kriteria Minimum</th>
                                                            <th style="width: 5%">Jumlah</th>
                                                            <th>Foto Contoh</th>
                                                            <th>Keterangan</th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                </thead>
                                                <tbody>
                                                        <tr key="`+key_eq+`" group_name="`+group_name+`" class="eq_items__" status="from_local">
                                                            <td>`+kriteria_minimum+`</td>
                                                            <td>`+jumlah+`</td>
                                                            <td>
                                                                <a href="javascript:;"><img class="img-thumbnail token-`+t_img+`" style="max-width: 100px"></a>
                                                            </td>
                                                            <td>`+keterangan+`</td>
                                                            <td><a class="btn btn-sm btn-danger btn-circle" href="javascript:;" onclick="confirmDialog('delete_item','','',$(this),'yes');" data-toggle="dropdown"> Delete </a></td>
                                                        </tr>
                                                </tbody>
                                        </table>
                                  </div>`);
                            sh_thumb(imgTmp,$('img.img-thumbnail.token-'+t_img));
                      }
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-red',
                action: function(){
                   
                }
            }
        }
    });

  }
}
// ------- EQUIPMENT ------ //
function addEquipmentGroupDialog(){

    var dialogContent = '<form>' +
                        '  <div class="form-group">' +
                        '    <label class="control-label">Group Name</label>' +
                        '    <input name="group_name" class="form-control" style="resize: none"></input>'+
                        '  </div>' +
                        '</form>';

    $.confirm({
        title: 'Add Equipment Group',
        content: dialogContent,
        columnClass: 'medium',
        buttons: {
            add: {
                    text: 'add',
                    btnClass: 'btn-blue',
                    keys: ['enter'],
                    action: function(){
                          var self = this;
                          var gr_name = self.$content.find('input[name="group_name"]').val();
                          var keyEQ = new Date().getTime();

                          $('#__equipment').append(`
                                <div class="equipment_" key="`+keyEQ+`" >
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <table>
                                                  <tr>
                                                      <td><h5 style="margin-left: 15px; padding-right: 30px">`+gr_name+`</h5></td>
                                                      <td><a class="btn btn-sm btn-primary btn-circle" href="javascript:;" data-toggle="dropdown" onclick="addItemDialog('equipment','','',$(this),`+keyEQ+`,'`+gr_name+`');"> Add Item </a></td>
                                                      <td>&nbsp;<a class="btn btn-sm btn-danger btn-circle" href="javascript:;" data-toggle="dropdown" onclick="confirmDialog('delete_group','','',$(this));"> Delete </a></td>
                                                  </tr>
                                              </table>
                                          </div>
                                      </div>
                                      <hr>

                                </div>
                          `);
                    }
            },
            cancel: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                    action: function(){
                       
                    }
            }
        }
    });

}

function countItems(el,tbl_name){
    var count = el.find('tr').length;
    if(count<=0){
        el.closest('table.'+tbl_name).parent().remove();
    }
}

var arrDelete = new Array();
var arrItemsEQDelete = new Array();

function confirmDialog(mode,id=null,from=null,el=null,equipment=null){
  if(mode=='delete_area'){
    $.confirm({
      title: 'Delete Area Confirmation',
      content: 'Delete area will also delete all of its contents and all user input data that has been set for this area. Are you sure?',
      buttons: {
          // confirm: function () {
          //     $.alert('Confirmed!');
          // },
          confirm: {
              text: 'Confirm',
              btnClass: 'btn-blue',
              keys: ['enter'],
              action: function(){
                        var elArrDel = el.closest('.area_').find('.items__');
                        var arrItemsToDelete = elArrDel.toArray();
                        arrItemsToDelete.forEach(function(item,index){
                                arrDelete.push(elArrDel.eq(index).attr('id_item'));
                        });

                        el.closest('.area_').remove();

                        sortArea();
              }
          },
          cancel: {
              text: 'Cancel',
              btnClass: 'btn-red',
              action: function(){
                 
              }
          }
      }
    });
  }

  if(mode=='delete_sub_area'){
    $.confirm({
      title: 'Delete Sub Area Confirmation',
      content: 'Delete sub area will also delete all of its contents and all user input data that has been set for this sub area. Are you sure?',
      buttons: {
          // confirm: function () {
          //     $.alert('Confirmed!');
          // },
          confirm: {
              text: 'Confirm',
              btnClass: 'btn-blue',
              keys: ['enter'],
              action: function(){
                        var elArrDel = el.closest('.sub_area_').find('.items__');
                        var arrItemsToDelete = elArrDel.toArray();
                        arrItemsToDelete.forEach(function(item,index){
                                arrDelete.push(elArrDel.eq(index).attr('id_item'));
                        });

                        el.closest('.sub_area_').remove();
                        sortArea();
              }
          },
          cancel: {
              text: 'Cancel',
              btnClass: 'btn-red',
              action: function(){
                 
              }
          }
      }
    });
  }

  if(mode=='delete_item'){
    $.confirm({
      title: 'Delete Item Confirmation',
      content: 'Delete item will also delete all of its contents and all user input data that has been set for this item. Are you sure?',
      buttons: {
          // confirm: function () {
          //     $.alert('Confirmed!');
          // },
          confirm: {
              text: 'Confirm',
              btnClass: 'btn-blue',
              keys: ['enter'],
              action: function(){
                      if(equipment=='yes'){
                          if(from=='from_server'){
                                  arrItemsEQDelete.push(id);
                                  let elm = el.closest('tbody');
                                  el.closest('tr').remove();
                                  countItems(elm,'tb_group_item');
                          }else{
                                  let elm = el.closest('tbody');
                                  el.closest('tr').remove();
                                  countItems(elm,'tb_group_item');
                          }
                      }else{
                          if(from=='from_server'){
                                  arrDelete.push(id);
                                  let elm = el.closest('tbody');
                                  el.closest('tr').remove();
                                  countItems(elm,'tb_sub_item');
                          }else{
                                  let elm = el.closest('tbody');
                                  el.closest('tr').remove();
                                  countItems(elm,'tb_sub_item');
                          }
                      }

                      sortArea();
              }
          },
          cancel: {
              text: 'Cancel',
              btnClass: 'btn-red',
              action: function(){
                 
              }
          }
      }
    });
  }

  if(mode=='delete_group'){
    $.confirm({
      title: 'Delete Group Confirmation',
      content: 'Delete group will also delete all of its contents and all user input data that has been set for this group. Are you sure?',
      buttons: {
          // confirm: function () {
          //     $.alert('Confirmed!');
          // },
          confirm: {
              text: 'Confirm',
              btnClass: 'btn-blue',
              keys: ['enter'],
              action: function(){
                  var elArrDel = el.closest('.equipment_').find('.eq_items__');
                  var arrEQItemsToDelete = elArrDel.toArray();
                  arrEQItemsToDelete.forEach(function(item,index){
                          arrItemsEQDelete.push(elArrDel.eq(index).attr('id_eq_item'));
                  });

                  el.closest('.equipment_').remove();
              }
          },
          cancel: {
              text: 'Cancel',
              btnClass: 'btn-red',
              action: function(){
                 
              }
          }
      }
    });
  }
}

function imagePreviewDialog(imgPath){
  
  var dialogContent = '<br><img src="'+imgPath+'" style="width: 100%">';      

  $.dialog({
      title: '',
      boxWidth: '100%',
      closeIcon: true, // explicitly show the close icon
      content: dialogContent,
      buttons: {},
      defaultButtons: {},
      onContentReady: function () {

       
          // bind to events
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
              // if the user submits the form by pressing enter in the field.
              e.preventDefault();
              jc.$$formSubmit.trigger('click'); // reference the button and click it
          });
      }
  });
            
}


jQuery(document).ready(function() {

  // google.maps.event.addDomListener(window, 'load', initMap);
   
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: 'd M yyyy',
        autoclose: true
    });

    $('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });

});

//ini untuk TOSS guidance dan add TOSS database 
function submit_(x,type=null){

        x.find('input.frm_').css('background-color','white');
        x.find('select.frm_').css('background-color','white');
        x.find('textarea.frm_').css('background-color','white');

        x.find('input').removeClass('warning-label');
        x.find('select').removeClass('warning-label');
        x.find('textarea').removeClass('warning-label');

        var arrInput = x.find('input').toArray();
        var arrSelect = x.find('select').toArray();
        var arrTextarea = x.find('select').toArray();

        var vSend = new Object;
        var Validate = 0;


        arrInput.forEach(function(item,index){

            var name = x.find('input.frm_').eq(index).attr('name');
            var value = x.find('input.frm_').eq(index).val();
            
            if(x.find('input.frm_').eq(index).attr('required')=='required'){
                  if(value==''||value==null){
                      Validate++;
                      x.find('input.frm_').eq(index).addClass('warning-label');
                      x.find('input.frm_').eq(index).css('background-color','#ff000038');
                      x.find('input.frm_').eq(index).on('keypress',function(){
                          $(this).css('background-color','white');
                      });

                  }else{
                      vSend[name] = value;
                  }
            }else{
                  vSend[name] = value;
            }
        });

        arrTextarea.forEach(function(item,index){

            var name = x.find('textarea.frm_').eq(index).attr('name');
            var value = x.find('textarea.frm_').eq(index).val();
            
            if(x.find('textarea.frm_').eq(index).attr('required')=='required'){
                  if(value==''||value==null){
                      Validate++;
                      x.find('textarea.frm_').eq(index).addClass('warning-label');
                      x.find('textarea.frm_').eq(index).css('background-color','#ff000038');
                      x.find('textarea.frm_').eq(index).on('keypress',function(){
                          $(this).css('background-color','white');
                      });

                  }else{
                      vSend[name] = value;
                  }
            }else{
                  vSend[name] = value;
            }
        });

        arrSelect.forEach(function(item,index){
                var name = x.find('select.frm_').eq(index).attr('name');
                var value = x.find('select.frm_').eq(index).val();
                
                if(x.find('select.frm_').eq(index).attr('required')=='required'){
                          if(value==''||value==null){
                                  Validate++;
                                  x.find('select.frm_').eq(index).addClass('warning-label');
                                  x.find('select.frm_').eq(index).css('background-color','#ff000038');
                                  x.find('select.frm_').eq(index).on('change',function(){
                                      $(this).css('background-color','white');
                                  });
                          }else{
                                  vSend[name] = value;
                          }
                }else{
                          vSend[name] = value;
                }
        });


        //------- AREA --------------------
        var arrArea = x.find('.area_');
        var arrSubArea = x.find('.sub_area_');
        var arrItems_ = x.find('.items__');

        ValidateItems = 0;
        
        arrSubArea.toArray().forEach(function(item,index){
            var lenItems = arrSubArea.eq(index).find('.items__').length;

            if(lenItems<=0){
                  ValidateItems++;
            }
        });

        arrArea.toArray().forEach(function(item,index){
            var lenItems = arrArea.eq(index).find('.items__').length;

            if(lenItems<=0){
                  ValidateItems++;
            }
        });
        //----------------------------------

        //----------- EQUIPMENT ----------------
        var arrGroupEq = x.find('.equipment_');
        var arrEquipment = x.find('.eq_items__');
        
        arrGroupEq.toArray().forEach(function(item,index){
            var lenItems = arrGroupEq.eq(index).find('.eq_items__').length;

            if(lenItems<=0){
                  ValidateItems++;
            }
        });
        //----------------------------------------

        console.log(vSend);
        // return false;
        // Validate = 0;
        var goSubmit = function(){
                if(Validate==0){
                    if(type=='guidance'){
                        var items = $('#__area').find('.items__');
                        var arrItems = items.toArray();
                        itemsArea = new Array();

                        arrItems.forEach(function(item,index){
                            if(items.eq(index).attr('status')=='from_local'){
                                itemsArea.push({
                                    id_area:items.eq(index).attr('id_area'),
                                    id_sub_area:items.eq(index).attr('id_sub'),
                                    kriteria_minimum:items.eq(index).find('td').eq(0).html(),
                                    jumlah:items.eq(index).find('td').eq(1).html(),
                                    foto_contoh_file_name_64:items.eq(index).find('td').eq(2).find('img').attr('src')
                                });
                            }
                        });

                        var arrItemsEQ = $('#__equipment').find('.eq_items__');
                        var itemsEQ = new Array();

                        arrItemsEQ.toArray().forEach(function(item,index){
                            if(arrItemsEQ.eq(index).attr('status')=='from_local'){
                                itemsEQ.push({
                                    group_key:arrItemsEQ.eq(index).attr('key'),
                                    group_name:arrItemsEQ.eq(index).attr('group_name'),
                                    kriteria_minimum:arrItemsEQ.eq(index).find('td').eq(0).html(),
                                    jumlah_minimum:arrItemsEQ.eq(index).find('td').eq(1).html(),
                                    foto_contoh_file_name_64:arrItemsEQ.eq(index).find('td').eq(2).find('img').attr('src'),
                                    keterangan:arrItemsEQ.eq(index).find('td').eq(3).html()
                                });
                            }
                        });

                        // console.log(arrDelete);
                        let ALL_DATA = {
                            data:vSend,
                            area:itemsArea,
                            delete:arrDelete,
                            equipment:itemsEQ,
                            delete_eq:arrItemsEQDelete,
                        }
                        ALL_DATA[csrf.name]=csrf.token;
                        $.post(URL+'master_data_toss/submit_save_guidance',ALL_DATA).done(function(data){
                            $.confirm({
                                title:'',
                                content:'Sukses mengubah data.',
                                icon: 'fa fa-check',
                                theme: 'modern',
                                closeIcon: false,
                                animation: 'scale',
                                type: 'green',
                                buttons:{
                                  ok:{
                                    text:"OK",
                                    action:function(){
                                        location.reload();
                                    }
                                  }
                                }
                            });
                        });
                    }

                    if(type=='add'){
                          startLoading(null,1);
                          var DATA={data:vSend};
                          DATA[csrf.name]=csrf.token;
                          $.post(URL+'master_data_toss/submit_save_data',DATA).done(function(data){
                              var res = JSON.parse(data);

                              if(res.status==1){
                                  $.confirm({
                                      title:'',
                                      content:'Sukses mengubah data.',
                                      icon: 'fa fa-check',
                                      theme: 'modern',
                                      closeIcon: false,
                                      animation: 'scale',
                                      type: 'green',
                                      buttons:{
                                        ok:{
                                          text:"OK",
                                          action:function(){
                                              location.href = URL+ 'master_data_toss/toss_database/view';
                                          }
                                        }
                                      }
                                  });
                              }else{
                                  $.confirm({
                                      title:'',
                                      content:'Gagal mengubah data.',
                                      icon: 'fa fa-exclamation-triangle',
                                      theme: 'modern',
                                      closeIcon: false,
                                      animation: 'scale',
                                      type: 'red',
                                      buttons:{
                                        ok:{
                                          text:"OK",
                                          action:function(){
                                              endLoading(null,1);
                                          }
                                        }
                                      }
                                  });
                              }
                          });
                    }
                }else{
                        $('.warning-label').eq(0).parent().find('input').focus();
                        $('.warning-label').eq(0).parent().find('select').focus();
                        
                        $.alert('Terdapat beberapa form yang belum diisi.');
                }
        }

        if(ValidateItems>0){
                $.confirm({
                          title:'',
                          content:'Terdapat beberapa Area yang belum memiliki Item, Area otomatis akan terhapus.',
                          buttons:{
                                  ok:{
                                        text:'Lanjutkan!',
                                        btnClass:'btn-warning',
                                        action:function(){
                                                goSubmit();
                                        }
                                  },
                                  cancel:{
                                        text:'Mengisi dahulu',
                                        btnClass:'btn-success'
                                  }
                          }
                });
        }

        if(ValidateItems==0){
              goSubmit();
        }

}


var arrAddPIC = new Array();
function addPIC(id,el){

    var name = el.closest('.frm_pic_visit').find('input[name="visit_name"]').val();
    var date = el.closest('.frm_pic_visit').find('input[name="visit_tanggal"]').val();

    if(name==''||date==''){
        $.alert('Nama atau Tanggal tidak boleh kosong.');
    }else{
        arrAddPIC.push({
          id_toss_database:id,
          visit_pic_name:name,
          visit_date:date
        });

        $('#list_PIC').find('tbody').append(`
            <tr class="ls_pic" pic_visit_name="`+name+`" pic_visit_date="`+date+`" status="from_local">
                <td style="padding: 5px" width="70%"><a onclick="del_pic(null,$(this))">`+name+`</a></td>
                <td style="padding: 5px; width: 150px; text-align: center">`+date+`</td>
            </tr>
        `);

        el.closest('.frm_pic_visit').find('input[name="visit_name"]').val('');
        el.closest('.frm_pic_visit').find('input[name="visit_tanggal"]').val('');
    }

    // var name = el.closest('.frm_pic_visit').find('input[name="visit_name"]').val();
    // var date = el.closest('.frm_pic_visit').find('input[name="visit_tanggal"]').val();

    // $.post(URL+'toss_func/add_pic_visit',{data:{id_toss_database:id,visit_pic_name:name,visit_date:date}}).done(function(data){
    //     alert(data);
    //     $('#list_PIC').find('tbody').append(`
    //         <tr>
    //             <td></td>
    //             <td></td>
    //         </tr>
    //     `);
    // }).fail(function(e){

    // });
    
}

var arrDelPIC = new Array();
function del_pic(x=null,el=null){
    $.confirm({
      title:'',
      content:'Anda yakin ingin menghapus PIC ini?',
      buttons:{
        ok:{
          text:"Ya. Hapus!",
          btnClass:'btn-danger',
          action:function(){
              if(x!=null){
                  arrDelPIC.push(x);
              }
              el.parents().eq(1).remove();
          }
        },cancel:{
          text:"Tidak"
        }
      }
    });

}

var vDEL_EQ = [];
//ini untuk save toss database tapi lewat halaman content nya
function SUBMIT_toss_database(id,el,ex=null){
    // console.log($('select[name="sertifikasi"]').attr('required'));
    if($('select[name="sertifikasi"]').val()==null){
        $('select[name="sertifikasi"]').addClass('warning-label');
        $('select[name="sertifikasi"]').css('background-color','#ff000038');
        $('select[name="sertifikasi"]').on('change',function(){
            $(this).css('background-color','white');
        });
        $('select[name="sertifikasi"]').focus();
        return false;
    }
    var ck = new Array();

    var ck_ = $('input[type="checkbox"].ck_ev');
    // var ck_manpower = $('input[type="checkbox"][tb="tbl_toss_database_cs_manpower"]');
    // var ck_equipment = $('input[type="checkbox"][tb="tbl_toss_database_cs_equipment"]');
    ck_.toArray().forEach(function(item,index){
        // if(ck_.eq(index).is(':checked')==true){
        //   alert('');
        // }
        if(ck_.eq(index).attr('name')=='status'){
          ck.push({
            table:ck_.eq(index).attr('tb'),
            field:ck_.eq(index).attr('name'),
            id:ck_.eq(index).attr('data_id'),
            value:ck_.eq(index).is(':checked')==true?'certified':'uncertified'
          });
        }else{
          ck.push({
            table:ck_.eq(index).attr('tb'),
            field:ck_.eq(index).attr('name'),
            id:ck_.eq(index).attr('data_id'),
            value:ck_.eq(index).is(':checked')==true?1:0
          });
        }
    });

    console.log(ck);

    // alert(ck_area);
    // return false;
    startLoading(el);
    var elm = el.closest('#form_TOSS_DB');
    var elmInput = elm.find('input.frm_');
    var elmSelect = elm.find('select.frm_');
    var elmOther = elm.find('.input.frm_');
    var elmListPIC = $('#list_PIC').find('.ls_pic');
    var elmListPHOTO_AREA = $('.img_area[is_change=1]');
    var elmListPHOTO_OUTLET = $('img.img_outlet[is_change=1]').attr('src');
    var elmListPHOTO_EQ = $('.img_eq');

    var vSend = new Object();
    var vPIC = new Array();
    var vAREA = new Array();
    var vEQ = new Array();

    elmInput.toArray().forEach(function(item,index){
        var key = elmInput.eq(index).attr('name');
        var value = elmInput.eq(index).val();

        vSend[key] = value;
    });

    elmSelect.toArray().forEach(function(item,index){
        var key = elmSelect.eq(index).attr('name');
        var value = elmSelect.eq(index).val();

        vSend[key] = value;
    });

    elmOther.toArray().forEach(function(item,index){
        var key = elmOther.eq(index).attr('name');
        var value = elmOther.eq(index).html();

        vSend[key] = value;
    });

    elmListPIC.toArray().forEach(function(item,index){
        var status = elmListPIC.eq(index).attr('status');
        var name = elmListPIC.eq(index).attr('pic_visit_name');
        var date = elmListPIC.eq(index).attr('pic_visit_date');

        if(status=='from_local'){
            vPIC.push({
              'id_toss_database':id,
              'visit_pic_name':name,
              'visit_date':date
            });
        }
    });

    elmListPHOTO_AREA.toArray().forEach(function(item,index){
        var key = elmListPHOTO_AREA.eq(index).attr('img_key');
        var src = elmListPHOTO_AREA.eq(index).attr('src');

        vAREA.push({
          'img_key':key,
          'src':src
        });
        
    });

    elmListPHOTO_EQ.toArray().forEach(function(item,index){
        var src = elmListPHOTO_EQ.eq(index).attr('src');
        var id_eq = elmListPHOTO_EQ.eq(index).attr('id_eq_item');
        var group_key = elmListPHOTO_EQ.eq(index).attr('group_key');
        var item_key = elmListPHOTO_EQ.eq(index).attr('item_key');

        vEQ.push({
          'foto_dealer_file_name_64':src,
          'group_key':group_key,
          'item_key':item_key,
          'id_toss_database_cs_equipment':id_eq
        });
        
    });

    console.log(vSend);
    console.log(vAREA);
    console.log(vEQ);
    var DATA={id:id,data:vSend,extra:ex,pic:vPIC,del_pic:arrDelPIC,evaluasi:ck,ch_area:vAREA,ch_eq:vEQ,del_eq_photo:vDEL_EQ,img_outlet:elmListPHOTO_OUTLET};
    DATA[csrf.name]=csrf.token;
    $.post(URL+'master_data_toss/submit_toss_database',DATA).done(function(data){
        var res = JSON.parse(data);
        if(res.status==1){
            el.html('Uploading...<span style="margin-left:5px;" class="fa fa-circle-o-notch fa-spin"></span>');
            
            var uploadfile = function(){
                var file_data = $('#attach').prop('files')[0];
                var form_data = new FormData();                  
                form_data.append('file', file_data);             
                form_data.append(csrf.name, csrf.token);
                form_data.append('submit','');


                $.ajax({
                  url: URL+'master_data_toss/uploadfile/'+id, // point to server-side PHP script 
                  dataType: 'text',  // what to expect back from the PHP script, if anything
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,                         
                  type: 'post',
                  success: function(php_script_response){
                      $.confirm({
                          title:'',
                          content:'Sukses mengubah data.',
                          icon: 'fa fa-check',
                          theme: 'modern',
                          closeIcon: false,
                          animation: 'scale',
                          type: 'green',
                          buttons:{
                            ok:{
                              text:"OK",
                              action:function(){
                                  location.reload();
                                  // endLoading(el);
                              }
                            }
                          }
                      });
                  }
                });
            }

            uploadfile();


        }else{
            $.confirm({
                title:'',
                content:'Gagal mengubah data.',
                icon: 'fa fa-exclamation-triangle',
                theme: 'modern',
                closeIcon: false,
                animation: 'scale',
                type: 'red',
                buttons:{
                  ok:{
                    text:"OK",
                    action:function(){
                        location.reload();
                    }
                  }
                }
            });
        }
    }).fail(function(e){

    });
}


function delete_toss(x){
    $.confirm({
      title:'',
      content:'Anda yakin ingin menghapus TOSS ini?',
      buttons:{
        hapus:{
          text:'Ya! Hapus.',
          btnClass:'btn-danger',
          action:function(){
              startLoading(null,1);
              var DATA={id:x};
              DATA[csrf.name]=csrf.token;
              $.post(URL+'master_data_toss/delete_toss/',DATA).done(function(data){
                  var res = JSON.parse(data);
                  if(res.status==1){
                      $.confirm({
                          title:'',
                          content:'Sukses menghapus TOSS.',
                          icon: 'fa fa-check',
                          theme: 'modern',
                          closeIcon: false,
                          animation: 'scale',
                          type: 'green',
                          buttons:{
                            ok:{
                              text:"OK",
                              action:function(){
                                  location.href = URL+'master_data_toss/toss_database/view';
                              }
                            }
                          }
                      });
                  }else{
                      $.confirm({
                          title:'',
                          content:'Gagal menghapus TOSS.',
                          icon: 'fa fa-exclamation-triangle',
                          theme: 'modern',
                          closeIcon: false,
                          animation: 'scale',
                          type: 'red',
                          buttons:{
                            ok:{
                              text:"OK",
                              action:function(){
                                  // location.href = URL+'master_data_toss/toss_database/view';
                              }
                            }
                          }
                      });
                      endLoading(null,1);
                  }
              }).fail(function(e){
                  endLoading(null,1);
              });
          }
        },cancel:{
          text:'Cancel'
        }
      }
    });
}


function del_img_eq(key=null,item_key=null,el){
  $.alert({
    title:'',
    content:'Anda ingin menghapus gambar ini?',
    buttons:{
      ok:{
        text:'Ya!',
        action:function(){
          el.parent().parent().find('.add_image input').remove();
          el.parent().parent().find(`.add_image`).append(`<input class="eq_handle" type="file" style="display: none;" onchange="ch_imgTmp(this,'upload_eq')">`);
          if(key!=null){
              vDEL_EQ.push({
                'key':key,
                'item_key':item_key
              });
              el.parent().remove();
          }else{
              el.parent().remove();
          }

        }
      },cancel:{
        text:'Batal'
      }
    }
  });
    
}

var cek = setInterval(function(){
    // console.log('zz');
    if($('.icheckbox_square-grey').length>0){
      var t = document.getElementsByTagName('ins');

      for(var i=0; i<t.length; i++){
          document.getElementsByTagName('ins')[i].addEventListener("click", function(){
              $(this).parent().removeClass('icheckbox_square-grey');
              $(this).parent().addClass('fa fa-circle-o-notch fa-spin');
              var self = $(this);
              if($(this).parent().hasClass('checked')){
                  var tb = $(this).parent().find('input').attr('tb');
                  var tb_fe = $(this).parent().find('input').attr('tb_fe');
                  var id = $(this).parent().find('input').attr('data_id');
                  var name = $(this).parent().find('input').attr('name');
                  var DATA = {tb:tb,tb_fe:tb_fe,id:id,name:name};
                  DATA[csrf.name]=csrf.token; 
                  $.post(URL+'master_data_toss/cek_dealer',DATA).done(function(data){
                      if(data=='belum'){
                          $.alert('Dealer belum mengevaluasi');
                          self.parent().find('input').removeAttr('checked');
                          self.parent().removeClass('checked');

                          self.parent().addClass('icheckbox_square-grey');
                          self.parent().removeClass('fa fa-circle-o-notch fa-spin');
                      }else{
                          if(data=='sudah'){
                              self.parent().addClass('icheckbox_square-grey');
                              self.parent().removeClass('fa fa-circle-o-notch fa-spin');
                          }else{
                              $.alert('Error! Terjadi kesalahan.');
                              self.parent().find('input').removeAttr('checked');
                              self.parent().removeClass('checked');

                              self.parent().addClass('icheckbox_square-grey');
                              self.parent().removeClass('fa fa-circle-o-notch fa-spin');
                          }
                      }

                  }).fail();
              }else{
                  self.parent().addClass('icheckbox_square-grey');
                  self.parent().removeClass('fa fa-circle-o-notch fa-spin');
              }
          });
      }
      clearInterval(cek);
    }
},100);

setTimeout(function(){
    console.log('End');
    clearInterval(cek);
},10000);



$('.select2').select2();

$('select[name="provinsi"]').on('change',function(){
    $('select[name="kota_kabupaten"]').html('<option disabled="disabled" selected="selected">-- Pilih Kota/Kabupaten --</option>').select2();
    $('select[name="kecamatan"]').html('<option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>').select2();

    $.get(URL+'master_data_toss/getkota/'+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="kota_kabupaten"]').append(`<option value=`+item.id+`>`+item.nama+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();

    $('select[name="kota_kabupaten"]').select2();
});

$('select[name="kota_kabupaten"]').on('change',function(){
    $('select[name="kecamatan"]').html('<option disabled="disabled" selected="selected">-- Pilih Kecamatan --</option>').select2();

    $.get(URL+'master_data_toss/getkec/'+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="kecamatan"]').append(`<option value=`+item.id+`>`+item.nama+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();

    $('select[name="kecamatan"]').select2();
});


function conf_upload(){
    $.confirm({
      title:'Submit Dealer',
      content:'(Be Careful!) Anda yakin ingin mengganti daftar dealer?',
      buttons:{
          ya:{
              text:'Ya. Ganti!',
              btnClass:'btn-danger',
              action:function(){
                  var file_data = $('input[name="upload_excel"]').prop('files')[0];   
                  var form_data = new FormData();                  
                  form_data.append('file', file_data);
                  form_data.append(csrf.name, csrf.token);
                                              
                  $.ajax({
                      url: URL+'master_data_toss/baca_excel', // point to server-side PHP script 
                      dataType: 'text',  // what to expect back from the PHP script, if anything
                      cache: false,
                      contentType: false,
                      processData: false,
                      data: form_data,                         
                      type: 'post',
                      success: function(data){
                          var res = JSON.parse(data);
                          if(res.status==1){
                              $.confirm({
                                  title:'',
                                  content:'Sukses mengubah data.',
                                  icon: 'fa fa-check',
                                  theme: 'modern',
                                  closeIcon: false,
                                  animation: 'scale',
                                  type: 'green',
                                  buttons:{
                                    ok:{
                                      text:"OK",
                                      action:function(){
                                          location.href = URL+ 'master_data_toss/toss_dealer/view';
                                      }
                                    }
                                  }
                              });
                          }else{
                              $.confirm({
                                  title:'',
                                  content:'Gagal mengubah data.',
                                  icon: 'fa fa-exclamation-triangle',
                                  theme: 'modern',
                                  closeIcon: false,
                                  animation: 'scale',
                                  type: 'red',
                                  buttons:{
                                    ok:{
                                      text:"OK",
                                      action:function(){
                                          endLoading(null,1);
                                      }
                                    }
                                  }
                              });
                          }
                      }
                   });
              }
          },
          cancel:{
              text:'Cancel',
              action:function(){

              }
          }
      }
    });
}

$('select[name="dealer"]').on('change',function(){
    $('select[name="cabang_induk"]').html('<option disabled="disabled" selected="selected">-- Pilih Cabang Induk --</option>').select2();

    $.get(URL+'master_data_toss/getcabanginduk?dealer='+$(this).val()).done(function(data){
        try{
            var x = JSON.parse(data);
            x.forEach(function(item,index){
                $('select[name="cabang_induk"]').append(`<option value="`+item.outlet_name+`">`+item.outlet_name+`</option>`);
            });
        }catch(e){
            
        }
    }).fail();

    $('select[name="cabang_induk"]').select2();
});