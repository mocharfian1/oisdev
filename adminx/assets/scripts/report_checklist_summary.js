/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

var reportHtmlCover = '';
var reportHtmlContent = '';
var reportHtmlEat = '';
var reportHtmlManPower = '';
var reportHtmlData = '';


function loadingStart(){
    $('#reportContentWrapper').fadeOut();
    $('#loaderWrapper').fadeIn();
    $('#btnGeneratePdf').text('please wait..'); //change button text
    $('#btnGenerateHtml').text('please wait..'); //change button text
    $('#btnGeneratePdf').attr('disabled',true); //set button disable
    $('#btnGenerateHtml').attr('disabled',true); //set button disable
}

function loadingEnd(){
    // $('#reportContentWrapper').html('<h1>No report to display</h1>');
    $('#reportContentWrapper').fadeIn();
    $('#loaderWrapper').fadeOut();
    $('#btnGeneratePdf').text('Generate PDF'); //change button text
    $('#btnGenerateHtml').text('Display Report'); //change button text
    $('#btnGeneratePdf').attr('disabled',false); //set button enable
    $('#btnGenerateHtml').attr('disabled',false); //set button enable
}

function dateMySqlConvertForEPM(date){
    var dateArr = date.split("-");

    var day = dateArr[2];
    var year = dateArr[0];
    var month = dateArr[1];

    if(day >= 1 && day <=7){ day = 'W1';}
    if(day >= 8 && day <=14){ day = 'W2';}
    if(day >= 15 && day <=21){ day = 'W3';}
    if(day >=22){ day = 'W4';}

    if(month == 1){month = 'Januari'};
    if(month == 2){month = 'Februari'};
    if(month == 3){month = 'Maret'};
    if(month == 4){month = 'April'};
    if(month == 5){month = 'Mei'};
    if(month == 6){month = 'Juni'};
    if(month == 7){month = 'Juli'};
    if(month == 8){month = 'Agustus'};
    if(month == 9){month = 'September'};
    if(month == 10){month = 'Oktober'};
    if(month == 11){month = 'November'};
    if(month == 12){month = 'Desember'};


    var dateFormat = day +' '+ month +' '+ year;
    return dateFormat;
}

function reportPdf(){
    
    if($('#project').val() == '' || $('#project').val() == null || $('#jenis_epm').val() == '' || $('#jenis_epm').val() == null){
            swal('Plase select project and EPM');
    }else{

        var param = $('#project').val();
        var param2 = $('#jenis_epm').val();

        window.open(burl+'report/pdf_report_summary/checklist_summary/'+param+'/'+param2);
    }
}

function reportHtml(){
   
    //clean content
    $('#reportContentWrapper').html('');

    if($('#project').val() == '' || $('#project').val() == null || $('#jenis_epm').val() == '' || $('#jenis_epm').val() == null){
        swal('Plase select project and EPM');
        $('#reportContentWrapper').html('<h1 style="text-align: center;">No report to display</h1>');
    }else{

        loadingStart();

        //empty data in variable
        reportHtmlCover = '';
        reportHtmlContent = '';
        reportHtmlEat = '';
        reportHtmlManPower = '';
        reportHtmlData = '';

        $('#reportContentWrapper').html('');
        showReport();
    }
    
}
function showReport(){
    var id= $('#project').val();
    var type= $('#jenis_epm').val();
    var data = {'id_project': id,'jenis_epm' : type};
    data[csrf.name]=csrf.token;
    loadingStart();
    $.ajax({
          url : burl+"report/print_check",
          type: "POST",
          data: data,
          success: function(content)
          {
            $('#reportContentWrapper').html(content);
            loadingEnd();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            loadingEnd();
            console.log('Error');
            console.log(jqXHR.responseText);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            swal('Error get data');
          }
    });
}
jQuery(document).ready(function() {
    $('.select2').select2();
    
});
