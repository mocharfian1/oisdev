/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */


  // function goToEdit(id){
  //   var ftoken = '';
  //   $.get(base_url+"login/ftoken", function(data, status){
  //     alert("Data: " + data + "\nStatus: " + status);
  //     if(status == 'Success'){

  //     }
  //     ftoken = data;
  //   });
  // }

  function filteringsubarea(elem){    
		$('#id_sub_area').html('');
	   	$('#id_sub_area').prop('disabled',true);
		$.get(base_url+'master_data/get_subarea_by_area/'+$('#id_area').val(), function(data, status){		
			var obj = JSON.parse(data);			  		
			if(obj==null){
				console.log('Empty data');
			}else{
				$('#id_sub_area').prop('disabled',false);
				var items='';       	           
				for(var i in obj){
					items += '<option value="'+obj[i].id+'">'+obj[i].sub_area_name+'</option>';
				}

				$('#id_sub_area').html(items);
				$('#id_sub_area').trigger('change');
			} 	                		   
		}).fail(function(data) {				
			$('#id_sub_area').prop('disabled',false);
			console.log(data.responseText);
		});		
  }

  function save_project_template_area(redir){
    
    if($('#id_area').val() == null || $('#id_sub_area').val() == null || $('.position').val() == ''){
      swal('Please complete all field!');
    }else{
	  
    	$('#btnSave').text('saving...'); //change button text
    	$('#btnSave').attr('disabled',true); //set button disable 
    	$('#btnCancel').attr('disabled',true); //set button disable 

    	$.post(base_url+'master_data/save_project_template_area/',$('#form').serialize(), function(data, status){					  
    		obj = JSON.parse(data);console.log(obj);
    		if(obj.status==true){
    			swal({
    				title: "Data berhasil disimpan!",
    				icon: "success",
    				button: "OK!",
    			  }).then((value) => {
    				  // window.location.assign(base_url+"master_data/"+redir+"/set_area/"+$("#id_project").val()); 
              $('#btnBack').trigger('click');
    			  });
    		}else{
    			swal({
    				title: obj.ket,
    				icon: "error",
    				button: "OK!",
    			  });
    		}
    		
    		  
    		  $('#btnSave').text('Save'); //change button text
    		  $('#btnSave').attr('disabled',false); //set button disable 
    		  $('#btnCancel').attr('disabled',false); //set button disable 
    	}).fail(function(data) {				
    		console.log(data.responseText);
    	});
    }
  }

  $(document).ready(function() {
    $(".fax").on("keyup change",function(){
      val=$(this).val();
      $(this).val(val.replace(/[^0-9\-\+\(\)\s]/g, ''));
    });
    $('#position').blur(function(e) {
        $('#position').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#item_name').blur(function(e) {
        $('#item_name').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#kriteria').blur(function(e) {
        $('#kriteria').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#positionkp').blur(function(e) {
        $('#positionkp').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#item_kriteria_pekerjaan').blur(function(e) {
        $('#item_kriteria_pekerjaan').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#positionminreq').blur(function(e) {
        $('#positionminreq').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#itemminreq').blur(function(e) {
        $('#itemminreq').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#position_eat').blur(function(e) {
        $('#position_eat').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#item_name_eat').blur(function(e) {
        $('#item_name_eat').css({"border-color": ""}).removeClass("alert-danger");
    });
    $('#group_name_eat').blur(function(e) {
        $('#group_name_eat').css({"border-color": ""}).removeClass("alert-danger");
    });
  });

  // Tools & Equipment
  function add_eat() {
    posisinya = $('#position_eat').val();
    if($.trim($('#position_eat').val()) == '' || $.trim($('#group_name_eat').val()) == '' || $.trim($('#item_name_eat').val()) == '' ) {
      swal({
        title: "Please complete field!",
        icon: "warning",
        dangerMode: true,
      });
      $('#position_eat').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#group_name_eat').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#item_name_eat').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
    } else if($.trim($('#position_eat').val()) == $("#tbeat #p"+posisinya).text()){
      swal("Position is already exist!", "You can change if you want!");
    } else {
        var $tr = $('<tr class="roweat" id="removeRowEat'+$("#position_eat").val()+'" />');
        $tr.append($("<td />", { text: $("#position_eat").val().replace(/[^0-9]+/g," "), id:'p'+$("#position_eat").val() }));
        $tr.append($("<td />", { text: $("#group_name_eat").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'g'+$("#position_eat").val() }));
        $tr.append($("<td />", { text: $("#item_name_eat").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'i'+$("#position_eat").val() }));
        $tr.append($("<td><a href='javascript:;' onclick='removeEat("+ $("#position_eat").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editEat("+ $("#position_eat").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
        $('#position_eat').css({"border-color": ""});
        $('#group_name_eat').css({"border-color": ""});
        $('#item_name_eat').css({"border-color": ""});
        // $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
        $tr.appendTo($("#eat"));
        $('#position_eat, #group_name_eat, #item_name_eat').val('');
        $('#position_eat').focus();
      }
  }

  function removeEat(id){
    $('#removeRowEat'+id).remove();
  }

  function editEat(id){
    
    var ideat = $("#ideat"+id).val();
    deletewhenupdate('eat',ideat);
    $('#position_eat').val('');
    $('#group_name_eat').val('');
    $('#item_name_eat').val('');

    var position = $("#tbeat #p"+id).text();
    var group_name = $("#tbeat #g"+id).text();
    var item_name = $("#tbeat #i"+id).text();
   
    $('#position_eat').val(position).css("border-color","#ffd260").removeClass("alert-danger");
    $('#group_name_eat').val(group_name).css("border-color","#ffd260").removeClass("alert-danger");
    $('#item_name_eat').val(item_name).css("border-color","#ffd260").removeClass("alert-danger");

    $('#removeRowEat'+id).remove();
    // var a = position + item_name + kriteria;
    // console.log(a);
  }

  // Item Checklist
  function add_ic() {
    posisinya = $('#position').val();
    if($.trim($('#position').val()) == '' || $.trim($('#item_name').val()) == '' || $.trim($('#kriteria').val()) == '' ) {
      swal({
        title: "Please complete field!",
        icon: "warning",
        dangerMode: true,
      });
      $('#position').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#item_name').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#kriteria').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
    } else if($.trim($('#position').val()) == $("#tbic #p"+posisinya).text()){
      swal("Position is already exist!", "You can change if you want!");
    } else {
        var $tr = $('<tr class="rowic" id="removeRowIc'+$("#position").val()+'" />');
        $tr.append($("<td />", { text: $("#position").val().replace(/[^0-9]+/g," "), id:'p'+$("#position").val() }));
        $tr.append($("<td />", { text: $("#item_name").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'i'+$("#position").val() }));
        $tr.append($("<td />", { text: $("#kriteria").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'k'+$("#position").val() }));
        $tr.append($("<td><a href='javascript:;' onclick='removeIc("+ $("#position").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editIc("+ $("#position").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
        $('#position').css({"border-color": ""});
        $('#item_name').css({"border-color": ""});
        $('#kriteria').css({"border-color": ""});
        // $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
        $tr.appendTo($("#ic"));
        $('#position, #item_name, #kriteria').val('');
        $('#position').focus();
      }
  }

  function removeIc(id){
    $('#removeRowIc'+id).remove();
  }

  function editIc(id){
    // alert(position);
    var idic = $("#idic"+id).val();
    deletewhenupdate('ic',idic);
    $('#position').val('');
    $('#item_name').val('');
    $('#kriteria').val('');

    var position = $("#tbic #p"+id).text();
    var item_name = $("#tbic #i"+id).text();
    var kriteria = $("#tbic #k"+id).text();
    // alert(position);
    // alert(item_name);
    // alert(kriteria);
    $('#position').val(position).css("border-color","#ffd260").removeClass("alert-danger");
    $('#item_name').val(item_name).css("border-color","#ffd260").removeClass("alert-danger");
    $('#kriteria').val(kriteria).css("border-color","#ffd260").removeClass("alert-danger");

    $('#removeRowIc'+id).remove();
    // var a = position + item_name + kriteria;
    // console.log(a);
  }

  // Kriteria Pekerjaan
  function add_kp() {
    posisinya = $('#positionkp').val();
    if($.trim($('#positionkp').val()) == '' || $.trim($('#item_kriteria_pekerjaan').val()) == '' || $.trim($('#keterangan_kriteria_pekerjaan').val()) == '') {
      swal({
        title: "Please complete field!",
        icon: "warning",
        dangerMode: true,
      });
      $('#positionkp').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#item_kriteria_pekerjaan').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#keterangan_kriteria_pekerjaan').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
    } else if($.trim($('#positionkp').val()) == $("#tbkp #pkp"+posisinya).text()){
      swal("Position is already exist!", "You can change if you want!");
    } else {
        var $tr = $('<tr class="rowkp" id="removeRowKp'+$("#positionkp").val()+'" />');
        $tr.append($("<td />", { text: $("#positionkp").val().replace(/[^0-9]+/g," "), id:'pkp'+$("#positionkp").val() }));
        $tr.append($("<td />", { text: $("#item_kriteria_pekerjaan").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'ikp'+$("#positionkp").val() }));
        $tr.append($("<td />", { text: $("#keterangan_kriteria_pekerjaan").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'kkp'+$("#positionkp").val() }));
        $tr.append($("<td><a href='javascript:;' onclick='removeKp("+ $("#positionkp").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKp("+ $("#positionkp").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
        $('#positionkp').css({"border-color": ""});
        $('#item_kriteria_pekerjaan').css({"border-color": ""});
        $('#keterangan_kriteria_pekerjaan').css({"border-color": ""});
        // $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
        $tr.appendTo($("#kp"));
        $('#positionkp, #item_kriteria_pekerjaan').val('');
        $('#positionkp, #keterangan_kriteria_pekerjaan').val('');
        $('#positionkp').focus();
      }
  }

  function removeKp(id){
    $('#removeRowKp'+id).remove();
    $('#tbkp').focus();
  }

  function editKp(id){
    
    var idkp = $("#idkp"+id).val();
    deletewhenupdate('kp',idkp);
    $('#positionkp').val('');
    $('#item_kriteria_pekerjaan').val('');
    $('#keterangan_kriteria_pekerjaan').val('');

    var positionkp = $("#tbkp #pkp"+id).text();
    var item_kriteria_pekerjaan = $("#tbkp #ikp"+id).text();
    var keterangan_kriteria_pekerjaan = $("#tbkp #kkp"+id).text();
   
    $('#positionkp').val(positionkp).css("border-color","#ffd260").removeClass("alert-danger");
    $('#item_kriteria_pekerjaan').val(item_kriteria_pekerjaan).css("border-color","#ffd260").removeClass("alert-danger");
    $('#keterangan_kriteria_pekerjaan').val(keterangan_kriteria_pekerjaan).css("border-color","#ffd260").removeClass("alert-danger");

    $('#removeRowKp'+id).remove();
    $('#positionkp').focus();
  }

  // Minreq
  function add_minreq() {
    posisinya = $('#positionminreq').val();
    if($.trim($('#positionminreq').val()) == '' || $.trim($('#itemminreq').val()) == '') {
      swal({
        title: "Please complete field!",
        icon: "warning",
        dangerMode: true,
      });
      $('#positionminreq').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#itemminreq').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
    } else if($.trim($('#positionminreq').val()) == $("#tbminreq #pminrek"+posisinya).text()){
      swal("Position is already exist!", "You can change if you want!");
    } else {
        var $tr = $('<tr class="rowminreq" id="removeRowMinreq'+$("#positionminreq").val()+'" />');
        $tr.append($("<td />", { text: $("#positionminreq").val().replace(/[^0-9]+/g," "), id:'pminrek'+$("#positionminreq").val() }));
        $tr.append($("<td />", { text: $("#itemminreq").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'iminreq'+$("#positionminreq").val() }));
        $tr.append($("<td><a href='javascript:;' onclick='removeMinreq("+ $("#positionminreq").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editMinreq("+ $("#positionminreq").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
        $('#positionminreq').css({"border-color": ""});
        $('#itemminreq').css({"border-color": ""});
        // $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
        $tr.appendTo($("#minreq"));
        $('#positionminreq, #itemminreq').val('');
        $('#positionminreq').focus();
      }
  }

  function removeMinreq(id){
    $('#removeRowMinreq'+id).remove();
  }


  function editMinreq(id){
    
    var idminreq = $("#idminreq"+id).val();
    deletewhenupdate('minreq',idminreq);
    $('#positionminreq').val('');
    $('#itemminreq').val('');

    var positionminreq = $("#tbminreq #pminrek"+id).text();
    var itemminreq = $("#tbminreq #iminreq"+id).text();
   
    $('#positionminreq').val(positionminreq).css("border-color","#ffd260").removeClass("alert-danger");
    $('#itemminreq').val(itemminreq).css("border-color","#ffd260").removeClass("alert-danger");

    $('#removeRowMinreq'+id).remove();
    // var a = position + item_name + kriteria;
    // console.log(a);
    $('#positionminreq').focus();
  }

   // TPS Line
	function add_tpsline() {
		posisinya = $('#positionTpsLine').val();
		if($.trim($('#positionTpsLine').val()) == '' || $.trim($('#itemTpsLine').val()) == '') {
		  swal({
			title: "Please complete field!",
			icon: "warning",
			dangerMode: true,
		  });
		  $('#positionTpsLine').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		  $('#itemTpsLine').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		} else if($.trim($('#positionTpsLine').val()) == $("#tbTpsLine #ptps"+posisinya).text()){
		  swal("Position is already exist!", "You can change if you want!");
		} else {
			var $tr = $('<tr class="datatpsline" id="removeRowTpsLine'+$("#positionTpsLine").val()+'" />');
			$tr.append($("<td />", { text: $("#positionTpsLine").val().replace(/[^0-9]+/g," "), id:'ptps'+$("#positionTpsLine").val() }));
			$tr.append($("<td />", { text: $("#itemTpsLine").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'itps'+$("#positionTpsLine").val() }));
			$tr.append($("<td><a href='javascript:;' onclick='removeTpsLine("+ $("#positionTpsLine").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editTps("+ $("#positionTpsLine").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
			$('#positionTpsLine').css({"border-color": ""});
			$('#itemTpsLine').css({"border-color": ""});
			// $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
			$tr.appendTo($("#tpsLine"));
			$('#positionTpsLine, #itemTpsLine').val('');
			$('#positionTpsLine').focus();
		  }
	}

	function removeTpsLine(id){
		$('#removeRowTpsLine'+id).remove();
	}

	function editTps(id){

		var idtps = $("#idtps"+id).val();
		//deletewhenupdate('minreq',idtps);
		$('#positionTpsLine').val('');
		$('#itemTpsLine').val('');

		var positionTpsLine = $("#tbTpsLine #ptps"+id).text();
		var itemTpsLine = $("#tbTpsLine #itps"+id).text();
		
		$('#positionTpsLine').val(positionTpsLine).css("border-color","#ffd260").removeClass("alert-danger");
		$('#itemTpsLine').val(itemTpsLine).css("border-color","#ffd260").removeClass("alert-danger");

		$('#removeRowTpsLine'+id).remove();
		// var a = position + item_name + kriteria;
		// console.log(a);
		$('#positionTpsLine').focus();
	}

	// PIC
	function add_pic() {
		posisinya = $('#positionpic').val();
		if($.trim($('#positionpic').val()) == '' || $.trim($('#itempic').val()) == '') {
		  swal({
			title: "Please complete field!",
			icon: "warning",
			dangerMode: true,
		  });
		  $('#positionpic').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		  $('#itempic').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		} else if($.trim($('#positionpic').val()) == $("#tbpic #ppic"+posisinya).text()){
		  swal("Position is already exist!", "You can change if you want!");
		} else {
			var $tr = $('<tr class="rowpic" id="removeRowPic'+$("#positionpic").val()+'" />');
			$tr.append($("<td />", { text: $("#positionpic").val().replace(/[^0-9]+/g," "), id:'ppic'+$("#positionpic").val() }));
			$tr.append($("<td />", { text: $("#itempic").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'ipic'+$("#positionpic").val() }));
			$tr.append($("<td><a href='javascript:;' onclick='removePic("+ $("#positionpic").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editPic("+ $("#positionpic").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
			$('#positionpic').css({"border-color": ""});
			$('#itempic').css({"border-color": ""});
			// $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
			$tr.appendTo($("#pic"));
			$('#positionpic, #itempic').val('');
			$('#positionpic').focus();
		  }
	}

	function removePic(id){
		$('#removeRowPic'+id).remove();
	}

	function editPic(id){

		//var idtps = $("#idtps"+id).val();
		//deletewhenupdate('minreq',idtps);
		$('#positionpic').val('');
		$('#itempic').val('');

		var positionpic = $("#tbpic #ppic"+id).text();
		var itempic = $("#tbpic #ipic"+id).text();
		
		$('#positionpic').val(positionpic).css("border-color","#ffd260").removeClass("alert-danger");
		$('#itempic').val(itempic).css("border-color","#ffd260").removeClass("alert-danger");

		$('#removeRowPic'+id).remove();
		// var a = position + item_name + kriteria;
		// console.log(a);
		$('#positionpic').focus();
	}

	// KRiteria Minimum
	function add_km() {
		posisinya = $('#positionkm').val();
		if($.trim($('#positionkm').val()) == '' || $.trim($('#itemkm').val()) == '') {
		  swal({
			title: "Please complete field!",
			icon: "warning",
			dangerMode: true,
		  });
		  $('#positionkm').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		  $('#itemkm').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
		} else if($.trim($('#positionkm').val()) == $("#tbkm #pkm"+posisinya).text()){
		  swal("Position is already exist!", "You can change if you want!");
		} else {
			var $tr = $('<tr class="rowkm" id="removeRowKm'+$("#positionkm").val()+'" />');
			$tr.append($("<td />", { text: $("#positionkm").val().replace(/[^0-9]+/g," "), id:'pkm'+$("#positionkm").val() }));
			$tr.append($("<td />", { text: $("#itemkm").val().replace(/[^a-zA-Z0-9.,]+/g," "), id:'ikm'+$("#positionkm").val() }));
			$tr.append($("<td><a href='javascript:;' onclick='removeKm("+ $("#positionkm").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKm("+ $("#positionkm").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
			$('#positionkm').css({"border-color": ""});
			$('#itemkm').css({"border-color": ""});
			// $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
			$tr.appendTo($("#km"));
			$('#positionkm, #itemkm').val('');
			$('#positionkm').focus();
		  }
	}

	function removeKm(id){
		$('#removeRowKm'+id).remove();
	}

	function editKm(id){

		//var idtps = $("#idtps"+id).val();
		//positionkm('minreq',idtps);
		$('#positionkm').val('');
		$('#itemkm').val('');

		var positionkm = $("#tbkm #pkm"+id).text();
		var itemkm = $("#tbkm #ikm"+id).text();
		
		$('#positionkm').val(positionkm).css("border-color","#ffd260").removeClass("alert-danger");
		$('#itemkm').val(itemkm).css("border-color","#ffd260").removeClass("alert-danger");

		$('#removeRowKm'+id).remove();
		// var a = position + item_name + kriteria;
		// console.log(a);
		$('#positionkm').focus();
	}

  // Keterangan
  function add_ket() {
    posisinya = $('#positionket').val();
    if($.trim($('#positionket').val()) == '' || $.trim($('#itemket').val()) == '') {
      swal({
        title: "Please complete field!",
        icon: "warning",
        dangerMode: true,
      });
      $('#positionket').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
      $('#itemket').css({"border-color": "#FF9EA6","color": "#000"}).addClass("alert-danger");
    } else if($.trim($('#positionket').val()) == $("#tbket #pket"+posisinya).text()){
      swal("Position is already exist!", "You can change if you want!");
    } else {
        var $tr = $('<tr class="rowket" id="removeRowKet'+$("#positionket").val()+'" />');
        $tr.append($("<td />", { text: $("#positionket").val(), id:'pket'+$("#positionket").val() }));
        $tr.append($("<td />", { text: $("#itemket").val(), id:'iket'+$("#positionket").val() }));
        $tr.append($("<td><a href='javascript:;' onclick='removeKet("+ $("#positionket").val() +")' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='javascript:;' onclick='editKet("+ $("#positionket").val() +")' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>"));
        $('#positionket').css({"border-color": ""});
        $('#itemket').css({"border-color": ""});
        // $tr.append($("<td />", { html: "<a href='#' onclick='removeIc()' class='btn btn-default btn-sm'><i class='fa fa-close'></i></a><a href='' class='btn btn-default btn-sm'><i class='fa fa-pencil'></i></a>" }));
        $tr.appendTo($("#ket"));
        $('#positionket, #itemket').val('');
        $('#positionket').focus();
      }
  }

  function removeKet(id){
    $('#removeRowKet'+id).remove();
  }

  function editKet(id){
    
    var idket = $("#idket"+id).val();
    deletewhenupdate('ket',idket);
    $('#positionket').val('');
    $('#itemket').val('');

    var positionket = $("#tbket #pket"+id).text();
    var itemket = $("#tbket #iket"+id).text();
   
    $('#positionket').val(positionket).css("border-color","#ffd260").removeClass("alert-danger");
    $('#itemket').val(itemket).css("border-color","#ffd260").removeClass("alert-danger");

    $('#removeRowKet'+id).remove();
    // var a = position + item_name + kriteria;
    // console.log(a);
    $('#positionket').focus();
  }

  function saveEat(menu){
    
    var eatCount = $('#tbeat #eat .roweat').length;

    if($('#tbeat #eat .roweat').length == 0){
      swal("Data tidak boleh kosong!");
    }else{
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        $('#btnCancel').attr('disabled',true); //set button disable

      eat(menu);  
    }

  }

  function eat(menu) {

    var id_project = $("#id_project").val();
    var jenis_epm = $("#jenis_epm").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbeat #eat tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function() {
             itArr.push('"' + $(this).text() + '"');
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project + '"' +'}';

       console.log(json);
       
       $.ajax({
          url : base_url+"Master_data/add_config_eat",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {
              console.log(data);
             
              if(data.status) //if success close modal and reload ajax table
              {
               
                  swal({
                    title: "Data berhasil disimpan!",
                    icon: "success",
                    button: "OK!",
                  })
                  .then((value) => {
                    window.location.assign(base_url+"master_data/"+ (menu=='' ? 'project' : 'project_template') +"/view/"+id_project);
                  });

                  // console.log("datanya :",data.jenis);
              }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable
              $('#btnCancel').attr('disabled',true); //set button enable
          }
      });

    });

  }

  function saveSub(menu){
    
    var icCount = $('#tbic #ic .rowic').length;
    var kpCount = $('#tbkp #kp .rowkp').length;
    var tpsLineCount = $('#tbTpsLine #tpsLine .datatpsline').length;
    var picCount = $('#tbpic #pic .rowpic').length;
    var kmCount = $('#tbkm #km .rowkm').length;
    var minreqCount = $('#tbminreq #minreq .rowminreq').length;
    var ketCount = $('#tbket #ket .rowket').length; 
    var stop=false;

        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        $('#btnCancel').attr('disabled',true); //set button disable
  

  		jenis_epm = $("#jenis_epm").val();
  		if(jenis_epm=='75' || jenis_epm=='50'){
        if(icCount==0 && kpCount==0 && minreqCount==0 && ketCount==0 && tpsLineCount==0){
          swal("Konfigurasi tidak boleh kosong!");
          stop=true;
        }else{
    			tps_line(menu);
    			ic(icCount, kpCount, minreqCount, ketCount,menu); 
        } 
  		}else if(jenis_epm=='100'){
        if(kmCount==0 && picCount==0 && ketCount==0){
          swal("Konfigurasi tidak boleh kosong!");
          stop=true;
        }else{
    			pic(menu);
    			km(menu);
    			ic(icCount, kpCount, minreqCount, ketCount,menu);  
        }
      }
      // else{	
      // 	if(icCount==0 && kpCount==0 && minreqCount==0 && ketCount==0){
      //     swal("Konfigurasi tidak boleh kosong!");
      //     stop=true;
      //   }else{
    	// 		ic(icCount, kpCount, minreqCount, ketCount,menu);  
      //   }
  		// }
    if(stop){

                  $('#btnSave').text('save'); //change button text
                  $('#btnSave').attr('disabled',false); //set button enable
                  $('#btnCancel').attr('disabled',false); //set button enable
    }
    // if ($('#tbic #ic .rowic').length == 0 && $('#tbkp #kp .rowkp').length == 0 && $('#tbminreq #minreq .rowminreq').length == 0) {
    //   swal("Tidak boleh ada list data yang kosong!");
    // } else if ($('#tbic #ic .rowic').length > 0 && $('#tbkp #kp .rowkp').length > 0 && $('#tbminreq #minreq .rowminreq').length > 0) {
    //   $('#btnSave').text('saving...'); //change button text
    //   $('#btnSave').attr('disabled',true); //set button disable
    //   $('#btnCancel').attr('disabled',true); //set button disable
    //   $('.data').remove();
    //   $('.datakp').remove();
    //   $('.dataminreq').remove();
    //   ic();
    //   kp();
    //   minreq();
    // } else if ($('#tbic #ic .rowic').length > 0) {
    //   $('.data').remove();
    //   ic();
    // } else if ($('#tbkp #kp .rowkp').length > 0) {
    //   $('.datakp').remove();
    //   kp();
    // } else if ($('#tbminreq #minreq .rowminreq').length > 0) {
    //   $('.dataminreq').remove();
    //   minreq();
    // }
  }

  function ic(icCount, kpCount, minreqCount, ketCount,menu) {

    //if (icCount > 0){

    jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    ic_model = $("#item_checklist_model").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbic #ic tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
              
              // kolom terakhir tidak diambil karena cuma action, jika diambil masalah di object json
              if(y<3){
                itArr.push('"' + $(this).text() + '"');
              }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '","item_checklist_model"' + ': "' + ic_model + '"' +'}';

       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_ic",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {
             
              if(data.status) //if success close modal and reload ajax table
              {
                //if(kpCount>0){
                  kp('done', kpCount, minreqCount, ketCount,menu);
              //  }else if(kpCount == 0 && minreqCount > 0){
              //    minreq('done', 'done', minreqCount, ketCount,menu);
              //  }else if(kpCount == 0 && minreqCount == 0 && ketCount > 0){
              //    ket('done', 'done', 'done', ketCount,menu);
              //  }else if(kpCount == 0 && minreqCount == 0 && ketCount == 0){
               //   swal({
               //     title: "Data berhasil disimpan!",
               //     icon: "success",
                //    button: "OK!",
                //  })
                //  .then((value) => {
               //     window.location.assign(base_url+"master_data/"+menu+"/set_area/"+id_project);
                //  });

                  console.log("datanya :",data.jenis);
               // }
              }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable
              $('#btnCancel').attr('disabled',true); //set button enable
          }
      });

    });

   // }else{
   //   kp('done', kpCount, minreqCount, ketCount, menu);
   // };
  }

  // Kriteria Pekerjaan
  function kp(icCount, kpCount, minreqCount, ketCount, menu) {

    //if(kpCount > 0){

    jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbkp #kp tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<3){
              itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_kp",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {

              if(data.status) //if success close modal and reload ajax table
              {

               // if(minreqCount > 0){
                  minreq('done', 'done', minreqCount, ketCount, menu);
              //  }else if(minreqCount == 0 && ketCount > 0){
              //    ket('done', 'done', 'done', ketCount, menu);
              //  }else if(minreqCount == 0 && ketCount == 0){
              //    swal({
              //      title: "Data berhasil disimpan!",
              //      icon: "success",
              //      button: "OK!",
              //    })
               //   .then((value) => {
                //    window.location.assign(base_url+"master_data/"+menu+"/set_area/"+id_project);
               //   });
                //  console.log("datanya :",data.jenis);
               // }

              } 

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable
              $('#btnCancel').attr('disabled',false); //set button enable
          }
      });

    });

   // }else{
   //   minreq('done', 'done', minreqCount, ketCount, menu);
   // }
  }

  // Minreq
  function minreq(icCount, kpCount, minreqCount, ketCount,menu) {

   // if(minreqCount > 0){

    jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbminreq #minreq tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<2){
             itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_minreq",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {


             // if(ketCount > 0){
                  ket('done', 'done', 'done', ketCount,menu);
              //  }else if(ketCount == 0){
             //     swal({
             //       title: "Data berhasil disimpan!",
              //      icon: "success",
             //       button: "OK!",
             //     })
              //    .then((value) => {
                 //   window.location.assign(base_url+"master_data/"+menu+"/set_area/"+id_project);
              //    });
              //    console.log("datanya :",data.jenis);
               // }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable
              $('#btnCancel').attr('disabled',false); //set button enable
          }
      });

    });

    //}else{
    //  ket('done', 'done', 'ket', ketCount,menu);
    //}
  }

  // Ket
  function ket(icCount, kpCount, minreqCount, ketCount,menu) {
    jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbket #ket tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<2){
             itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_ket",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {


              if(data.status) //if success close modal and reload ajax table
              {
                swal({
                  title: "Data berhasil disimpan!",
                  text: "Anda tidak dapat melakukan perubahan data kembali!",
                  icon: "success",
                  button: "OK!",
                })
                .then((value) => {
                  window.location.assign(base_url+"master_data/"+menu+"/set_area/"+id_project);
                });
                
              }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              swal('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable
              $('#btnCancel').attr('disabled',false); //set button enable
          }
      });

    });
  }

  function tps_line(menu){
	 jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbTpsLine #tpsLine tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<2){
             itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_tps_line",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {
			console.log('Tps Line: Success');
          },
          error: function (response)
          {
             console.log('Tps LIne: '+ response.responseText);
          }
      });

    });
  }

  function pic(menu){
	  jenis_epm = $("#jenis_epm").val();
    id_project_area = $("#id_project_area").val();
    id_project = $("#id_project").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbpic #pic tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<2){
             itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
       });
       json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_pic",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {
			console.log('Picture: Success');
          },
          error: function (response)
          {
             console.log('Picture: '+ response.responseText);
          }
      });

    });
  }

  function km(menu){
	 jenis_epm = $("#jenis_epm").val();
   id_project_area = $("#id_project_area").val();
	 id_project = $("#id_project").val();
	 ic_model = $("#item_checklist_model").val();
    $(function(){
       var json = '{';
       var otArr = [];
       var tbl = $('#tbkm #km tr').each(function(i) {        
          x = $(this).children();
          var itArr = [];
          x.each(function(y) {
            if(y<2){
             itArr.push('"' + $(this).text() + '"');
            }
          });
          otArr.push('"' + i + '": [' + itArr.join(',') + ']');
	   });	   	   
	   json += '"data":{'+otArr.join(",") + '},"jenis"' + ': "' + jenis_epm + '","id"' + ': "' + id_project_area + '","item_checklist_model"' + ': "' + ic_model + '"' +'}';

       // console.log(json);
       $.ajax({
          url : base_url+"Master_data/add_config_sub_area_km",
          type: "POST",
          data: "datanya="+json+"&"+csrf.name+"="+csrf.token,
          dataType: "JSON",
          success: function(data)
          {
			console.log('Kriteria Minimum: Success');
          },
          error: function (response)
          {
             console.log('Kriteria Minimum: '+ response.responseText);
          }
      });

    });
  }

  function save(save_method)
  {
    var url;
	  var valid;

    if(save_method == 'add_area'){  //add Area
    
      url = base_url+"Master_data/insert_data/area";
      valid = true;  
      
    }else if(save_method == 'edit_area'){ // edit area

      url = base_url+"Master_data/update_data/area";
      valid = true;

    }else if(save_method == 'add_sub_area'){  //add sub area
    
      url = base_url+"Master_data/insert_data/sub_area";
      valid = true;  
      
    }else if(save_method == 'edit_sub_area'){ // edit sub area

      url = base_url+"Master_data/update_data/sub_area";
      valid = true;  
      
    }else if(save_method == 'add_eat_group'){ // add equipment and tools group
      url = base_url+"Master_data/insert_data/equipment_and_tools_group";
      if($('#group_name').val() == ''){
        swal('Please complete group name field!');
        valid = false;
      }else{
        valid = true;  
      }
    }else if(save_method == 'add_eat_item'){ // add equipment and tools item
      url = base_url+"Master_data/insert_data/equipment_and_tools_item";
      if($('#item_name').val() == ''){
        swal('Please complete item name field!');
        valid = false;
      }else{
        valid = true;  
      }
    }else if(save_method == 'add_user'){ // add user
      
      url = base_url+"Master_data/add_user";
      valid = true;  
      
    }else if(save_method == 'edit_user'){ // edit sub area
      
      url = base_url+"Master_data/update_user";
      valid = true;  
      
    }else if(save_method == 'add_project') {
      url = base_url+"Master_data/insert_data/project";
      if($('#nama_outlet').val() == '' || $('#main_dealer').val() == '' || $('#fungsi_outlet').val() == '' || $('#type_pembangunan').val() == '' || $('#alamat').val() == '' || $('#jenis_epm').val() == '' || $('#epm_50_date').val() == '' || $('#epm_75_date').val() == '' || $('#epm_100_date').val() == '' || $('#target_otorisasi').val() == '' || $('#assign_user_level_1').val() == '' || $('#assign_user_level_2').val() == '' || $('#assign_user_level_3').val() == '' || $('#template_set').val() == ''){
        swal('Please complete all field!');
        valid = false;
      }else{
        // valid = false;
        valid = true;
      } 
    } else if (save_method == 'edit_project') {
        url = base_url+"Master_data/update_data/project";
        if($('#nama_outlet').val() == '' || $('#main_dealer').val() == '' || $('#fungsi_outlet').val() == '' || $('#type_pembangunan').val() == '' || $('#alamat').val() == '' || $('#jenis_epm').val() == '' || $('#epm_50_date').val() == '' || $('#epm_75_date').val() == '' || $('#epm_100_date').val() == '' || $('#target_otorisasi').val() == '' || $('#id_user_konsultan').val() == '' || $('#id_user_dealer').val() == '' || $('#id_user_tam').val() == ''){
          swal('Please complete all field!');
          valid = false;
        }else{
          valid = true;
        } 
    } else if (save_method == 'add_project_area') {

        url = base_url+"Master_data/add_project_area";
        if($('.id-area').val() == '' || $('.id-sub-area').val() == '' || $('.position').val() == ''){
          swal('Please complete all field!');
          valid = false;
        }else if($('.id-area').val() !== '' && $('.id-sub-area').val() !== '' && $('.position').val() !== ''){
          valid = true;
        }
    } else if(save_method == 'add_project_equipment_and_tools'){

        url = base_url+"Master_data/do_add_project_area";

    }else if(save_method == 'add_project_level_training'){ 

      url = base_url+"Master_data/insert_data/project_level_training";
      valid = true;

    }else if(save_method == 'edit_project_level_training'){

      url = base_url+"Master_data/update_data/project_level_training";
      valid = true;  
      
    }else if(save_method == 'add_project_template'){

  		url = base_url+"Master_data/insert_data/project_template";
  		valid = true;  
  	
	  }else if(save_method == 'edit_project_template'){ 

  		url = base_url+"Master_data/update_data/project_template";
  		valid = true;  
  		
	  }

    if(valid == true){

      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      $('#btnCancel').attr('disabled',true); //set button disable 

      // ajax adding data to database
      $.ajax({
          url : url,
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
            // console.log(data);
              if(data.status) //if success close modal and reload ajax table
              {
                // console.log(data);
                swal({
                  title: "Data berhasil disimpan!",
                  icon: "success",
                  button: "OK!",
                })
                .then((value) => {
                  if (data.ket == 'insert project area data' || data.ket == 'update project area data') {
                    window.location.assign(base_url+"master_data/project/set_area/"+data.id);
                  } else if(data.ket == 'insert area data' || data.ket == 'update area data'){
                    window.location.assign(base_url+"master_data/area/view");
                  } else if(data.ket == 'insert sub area data' || data.ket == 'update sub area data'){
                    window.location.assign(base_url+"master_data/sub_area/view");
                  } else if(data.ket == 'insert eat group data'){
                    // window.location.assign(base_url+"master_data/equipment_and_tools_group/view");
                  // } else if(data.ket == 'insert user data' || data.ket == 'update user data'){
                    window.location.assign(base_url+"master_data/user/view");
                  } else if(data.ket == 'insert project data' || data.ket == 'update project data'){
                    window.location.assign(base_url+"master_data/project/view");
                  } else if(data.ket == 'insert project level training data' || data.ket == 'update project level training data'){
                    window.location.assign(base_url+"master_data/project_level_training/view");
                  } else if(data.ket == 'insert project template' || data.ket == 'update project template'){
                    window.location.assign(base_url+"master_data/project_template/view");
                  } else if(data.ket == 'insert user data' || data.ket == 'update user data'){
                    window.location.assign(base_url+"master_data/user/view");
                  }
                });
              }else{  // if failed
                // swal(data.ket);
                swal({
                  title : "Save data failed",
                  icon : "error",
                  text : data.ket
                });
              }

              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 
              $('#btnCancel').attr('disabled',false); //set button disable

          },
          error: function (response)
          {
			        if(response.status == 403){
                console.log(response.status);

                swal({
                  title : "Delete data failed",
                  icon : "error",
                  text : "Oops 403, sorry that's an error.\n\nYou does not have permission to use this action/url or maybe it's because your token is expired. So Please refresh page and try again.\n\nIf you already done those step before on this page please contact admin"
                });

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
                $('#btnCancel').attr('disabled',false); //set button disable
              }else{
                console.log(response.responseText);
                swal('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
                $('#btnCancel').attr('disabled',false); //set button disable
              }
              
          }
      });

    }      
  }

  function delete_data(formid, url)
  {
    swal({
      title: "Are you sure?",
      text: "Once Deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        $.ajax({
            url : url,
            type: "POST",
            dataType: "JSON",
            data:$('#'+formid).serialize(),
            success: function(data)
            {
              if(data.status){
                swal({
                  title: "Data berhasil dihapus!",
                  icon: "success",
                  button: "OK!",
                })
                .then((value) => {
                  window.location.reload();
                });
              }else{
                swal({
                  title : "Delete data failed",
                  icon : "error",
                  text : data.ket
                });
              }
            },
            error: function (response)
            {
              if(response.status == 403){
                console.log(response.status);

                swal({
                  title : "Delete data failed",
                  icon : "error",
                  text : "Oops 403, sorry that's an error.\n\nYou does not have permission to use this action/url or maybe it's because your token is expired. So Please refresh page and try again.\n\nIf you already done those step before on this page please contact admin"
                });

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable
                $('#btnCancel').attr('disabled',false); //set button disable
              }else{
                swal({
                  title : "Delete data failed",
                  icon : "error",
                  text : response.responseText
                });
              }
            }
        });

      }
    });
  }

  function deletewhenupdate(ket,id)
  {
    // if (ket=='ic') {
    //   url = base_url+"master_data/delete_config_sub_area/"+id;
    // } else if (ket=='kp') {
    //   url = base_url+"master_data/delete_config_sub_area_kp/"+id;
    // } else if (ket=='minreq') {
    //   url = base_url+"master_data/delete_config_sub_area_minreq/"+id;
    // } else if (ket=='ket') {
    //   url = base_url+"master_data/delete_config_sub_area_ket/"+id;
    // }
    // $.ajax({
    //     url : url,
    //     type: "POST",
    //     dataType: "JSON",
    //     success: function(data)
    //     {},
    //     error: function (jqXHR, textStatus, errorThrown)
    //     {}
    // });
  }

  function imagePreviewDialog(imgPath){
    
    var dialogContent = '<br><img src="'+imgPath+'" style="width: 100%">';      

    $.dialog({
        title: '',
        boxWidth: '100%',
        closeIcon: true, // explicitly show the close icon
        content: dialogContent,
        buttons: {},
        defaultButtons: {},
        onContentReady: function () {

         
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
              
  }


jQuery(document).ready(function() {

    $('input[type=text]').on('keypress', function (event) {

        // var regex = new RegExp("^[a-zA-Z0-9*&:;,._ -]+$");
        if(!$(this).hasClass("fax")){
          var regex = new RegExp("^[a-zA-Z0-9., ]*$");
          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
          if (!regex.test(key)) {
             event.preventDefault();
             return false;
          }
        }
    });


    $('.buttons-print').hide();
    $('.buttons-pdf').hide();
    $('.buttons-csv').hide();

    $(".select2").select2();

    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: 'dd M yyyy',
        autoclose: true
    });

    $('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });

    

    //image code input file with thumb for Outlet in project add & edit
    function readURL2(input) {

        if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    if(input.id=="imgInput2"){
                        $('#imgHdr2').attr('src', e.target.result);
                    }                 
                    
                }
                
                reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInput2").change(function(){
        filterImg2(this);                        
    });

    function filterImg2(fileElement){
      
        var file = $(fileElement).val();
        var splitStringFile = file.split(".");
        var arrayLength = splitStringFile.length;
        var extFile = splitStringFile[arrayLength - 1];

        if (extFile == 'jpg' || extFile == 'jpeg' || extFile == 'JPG' || extFile == 'JPEG' || extFile == 'PNG' || extFile == 'png') {
            
            if(fileElement.files[0].size < 2000000){  //size in KB
                readURL2(fileElement);
            }else{
                $(fileElement).val('');
                $.alert('File size is too big, please use a file with size no more than 2 MB in jpg or jpeg format.');
            }

        }else if($(fileElement).val() !== ''){
            $(fileElement).val('');
            $.alert('File type not supported');
        }
    }
    //end of image code input file with thumb for outlet in project add & edit

    // event submit image file outlet in master project
    $("#formPhotoOutlet").submit(function(e) {
          e.preventDefault(); 
          console.log($(this));
          var formData = new FormData($(this)[0]);
          var id = $('#id').val();
          var mode = '';
          var fieldStatus = false;
          if(id==""){mode='add';}else{mode='edit';};

          //filter field and image
          if(mode=='add' && ($('#nama_outlet').val() == '' || $('#main_dealer').val() == '' || $('#fungsi_outlet').val() == '' || $('#type_pembangunan').val() == '' || $('#alamat').val() == '' || $('#jenis_epm').val() == '' || $('#epm_50_date').val() == '' || $('#epm_75_date').val() == '' || $('#epm_100_date').val() == '' || $('#target_otorisasi').val() == '' || $('#id_user_konsultan').val() == '' || $('#id_user_dealer').val() == '' || $('#id_user_tam').val() == '' ||  $('#imgInput2').val() == '')){
            
            return swal('All field is required, except for lock user. Please complete the remaining fields!');

          }else if(mode=='edit' && ($('#nama_outlet').val() == '' || $('#main_dealer').val() == '' || $('#fungsi_outlet').val() == '' || $('#type_pembangunan').val() == '' || $('#alamat').val() == '' || $('#jenis_epm').val() == '' || $('#epm_50_date').val() == '' || $('#epm_75_date').val() == '' || $('#epm_100_date').val() == '' || $('#target_otorisasi').val() == '' || $('#id_user_konsultan').val() == '' || $('#id_user_dealer').val() == '' || $('#id_user_tam').val() == '')){
          
            return swal('All field is required, except for lock user. Please complete the remaining fields!');
          
          }else{

            fieldStatus = true;

          }

          //edit tanpa ganti image outlet
          if(fieldStatus == true && $('#imgInput2').val() == '' && mode == 'edit'){
            save('edit_project');
          }


          //edit atau add dengan ada file image outlet
          if(fieldStatus == true && $('#imgInput2').val() !== ''){

              $('#btnSave').text('saving...'); //change button text
              $('#btnSave').attr('disabled',true); //set button disable 
              $('#btnCancel').attr('disabled',true); //set button disable 

            $.ajax({
               url:base_url+'master_data/file_upload_outlet', //URL submit
               type:"post", //method Submit
               
               datatype : "JSON",
                data: formData,
                // async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){

                  // console.log(data);

                  var res = JSON.parse(data);
                  console.log(res);

                  if(res.status == 'success'){
                   
                    $('#outlet_file_name').val(res.file_name);
                    $('#outlet_file_path').val(res.file_path);
                    $('#outlet_file_true_path').val(res.file_true_path);

                    if(mode == 'add'){
                      save('add_project');  
                    }else{
                      save('edit_project');
                    }
                    

                  }

                  if(res.status == 'error'){
                    // $('#photoAreaMessage').html('(Error : '+data.message+')');
                    // $('#photoAreaMessage').fadeIn();

                    // setTimeout(function () {
                    //     $('#photoAreaMessage').fadeOut();
                    // }, 5000);
                    swal('Error :'+res.message);
                  }

                }
            });

          }

           
    });
    // end of event submit image file outlet in master project

    // event submit image file outlet in master project
    $("#formPhotoKpd").submit(function(e) {
          e.preventDefault(); 
          console.log($(this));
          var formData = new FormData($(this)[0]);
          var id_project = $('#id_project').val();
          var mode = '';
          var fieldStatus = false;
        
          $('#btnAddNewData').text('saving...'); //change button text
          $('#btnAddNewData').attr('disabled',true); //set button disable 
          $('#btnBack').attr('disabled',true); //set button disable 

          $.ajax({
             url:base_url+'master_data/file_upload_kpd/'+id_project, //URL submit
             type:"post", //method Submit
             
             datatype : "JSON",
              data: formData,
              // async: false,
              cache: false,
              contentType: false,
              processData: false,
              success: function(data){

                // console.log(data);

                var res = JSON.parse(data);
                console.log(res);

                if(res.status == 'success'){
                 
                  window.location.reload();

                }

                if(res.status == 'error'){
                  // $('#photoAreaMessage').html('(Error : '+data.message+')');
                  // $('#photoAreaMessage').fadeIn();

                  // setTimeout(function () {
                  //     $('#photoAreaMessage').fadeOut();
                  // }, 5000);
                  swal('Error :'+res.message);
                }

              }
          });   
    });
    // end of event submit image file outlet in master project

    $("#imgInputKpd").change(function() {
        $('#submitPhotoKpd').click();
    });

});
