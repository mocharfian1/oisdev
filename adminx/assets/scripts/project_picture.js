/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */
 function imagePreviewDialog(imgPath){
  // alert(imgPath);
  // var base_url = $('.base_url').val();
  var dialogContent = '<br><img src="'+imgPath+'" style="width: 100%">';      

  $.dialog({
      title: '',
      boxWidth: '100%',
      closeIcon: true, // explicitly show the close icon
      content: dialogContent,
      buttons: {},
      defaultButtons: {},
      onContentReady: function () {

       
          // bind to events
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
              // if the user submits the form by pressing enter in the field.
              e.preventDefault();
              jc.$$formSubmit.trigger('click'); // reference the button and click it
          });
      }
  });
            
}

function delete_pic(id)
  {

    swal({
      title: "Are you sure?",
      text: "Once Deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url : burl+"project_picture/delete_project_area_pic/"+id,
            dataType: "JSON",
            success: function(data)
            {
              swal({
                title: "Data berhasil dihapus!",
                icon: "success",
                button: "OK!",
              })
              .then((value) => {
                window.location.reload();
              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              swal('Error deleting data');
            }
        });
      }
    });
  }
function delete_project_pic(id)
  {

    swal({
      title: "Are you sure?",
      text: "Once Deleted, you will not be able to recover this data!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
            url : burl+"project_picture/delete_project_pic/"+id,
            dataType: "JSON",
            success: function(data)
            {
              swal({
                title: "Data berhasil dihapus!",
                icon: "success",
                button: "OK!",
              })
              .then((value) => {
                window.location.reload();
              });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              swal('Error deleting data');
            }
        });
      }
    });
  }


jQuery(document).ready(function() {
    $('.buttons-print').hide();
    $('.buttons-pdf').hide();
    $('.buttons-csv').hide();

    $(".select2").select2();

    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: 'dd M yyyy',
        autoclose: true
    });

    $('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });
});
