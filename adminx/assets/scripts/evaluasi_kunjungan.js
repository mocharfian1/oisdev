/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Ayub Anggara
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 18 9 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

function filteringepm(elem){
  $('#epm option').show();
  var id_project = $(elem).val();
  $('#epm').attr('disabled',true);
  $.ajax({
          url : burl+"Evaluasi_kunjungan/get_epm/"+id_project,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            console.log(data);
            $.each(data, function($key, $value){
             $('#epm option[value='+$value.jenis_epm+']').hide();
            });
            $('#epm').attr('disabled',false);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            console.log('Error');
            console.log(jqXHR.responseText);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            swal('Error get data');
          }
    });
}

function save(save_method) {
  // alert(burl);
  var url;
  var valid;
  if(save_method == 'add_evaluasi_kunjungan'){ 
    url = burl+"Evaluasi_kunjungan/insert_data";
    if($('#id_project').val() == '' || $('#epm').val() == '' || $('#tgl_kunjungan').val() == ''){
      swal('Please complete all field!');
      valid = false;
    }else{
      valid = true;  
    }
  } else if(save_method == 'edit_evaluasi_kunjungan'){
      url = burl+"Evaluasi_kunjungan/update_data/evaluasi_kunjungan";
      if($('#id_project').val() == '' || $('#epm').val() == '' || $('#tgl_kunjungan').val() == ''){
        swal('Please complete all field!');
        valid = false;
      }else{
        valid = true;  
      }
  } else if(save_method == 'edit_detail_evaluasi_kunjungan'){
      url = burl+"Evaluasi_kunjungan/update_data/detail_evaluasi_kunjungan";
      if($('#eval_problem_deskripsi').val() == '' || $('#eval_tindak_lanjut').val() == '' || $('#eval_target').val() == '' || $('#eval_pic').val() == ''){
        swal('Please complete all field!');
        valid = false;
      }else{
        valid = true;  
      }
  }

  if(valid == true){

    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    $('#btnCancel').attr('disabled',true); //set button disable 

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          // console.log(data);
            if(data.status) //if success close modal and reload ajax table
            {
              console.log(data);
              swal({
                title: "Data berhasil disimpan!",
                icon: "success",
                button: "OK!",
              })
              .then((value) => {
                if (data.ket == 'insert data evaluasi kunjungan' || data.ket == 'update data evaluasi kunjungan') {
                  window.location.assign(burl+"Evaluasi_kunjungan/set_page/view");
                } else if (data.ket == 'update data evaluasi kunjungan detail') {
                  window.location.assign(burl+"Evaluasi_kunjungan/set_page/detail_evaluasi_kunjungan/"+data.id_evaluasi_kunjungan);
                }
              });
            }else{  // if failed
              swal(data.ket);
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
            $('#btnCancel').attr('disabled',false); //set button disable

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            $('#btnCancel').attr('disabled',false); //set button disable
        }
    });

  }
}

function delete_data(formid,url)
{
  swal({
    title: "Are you sure?",
    text: "Once Deleted, you will not be able to recover this data!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {

      $.ajax({
          url : url,
          type: "POST",
          dataType: "JSON",
          data:$('#'+formid).serialize(),
          success: function(data)
          {
            swal({
              title: "Data berhasil dihapus!",
              icon: "success",
              button: "OK!",
            })
            .then((value) => {
              window.location.reload();
            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            swal('Error deleting data');
          }
      });
    }
  });
}


jQuery(document).ready(function() {
    $('.buttons-print').hide();
    $('.buttons-pdf').hide();
    $('.buttons-csv').hide();

    $(".select2").select2();

    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        format: 'dd M yyyy',
        autoclose: true
    });

    $('.timepicker-24').timepicker({
        autoclose: true,
        minuteStep: 5,
        showSeconds: false,
        showMeridian: false
    });
});
