/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Rifki Dermawan
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 05 12 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

var reportHtmlCover = '';
var reportHtmlContent = '';
var reportHtmlEat = '';
var reportHtmlManPower = '';
var reportHtmlData = '';

$('#project').change(function(e){
    $('#jenis_epm').val($('option:selected', this).data('id'));
});

function loadingStart(){
    $('#reportContentWrapper').fadeOut();
    $('#loaderWrapper').fadeIn();
    $('#btnGeneratePdf').text('please wait..'); //change button text
    $('#btnGenerateHtml').text('please wait..'); //change button text
    $('#btnGeneratePdf').attr('disabled',true); //set button disable
    $('#btnGenerateHtml').attr('disabled',true); //set button disable
}

function loadingEnd(){
    // $('#reportContentWrapper').html('<h1>No report to display</h1>');
    $('#reportContentWrapper').fadeIn();
    $('#loaderWrapper').fadeOut();
    $('#btnGeneratePdf').text('Generate PDF'); //change button text
    $('#btnGenerateHtml').text('Display Report'); //change button text
    $('#btnGeneratePdf').attr('disabled',false); //set button enable
    $('#btnGenerateHtml').attr('disabled',false); //set button enable
}

function dateMySqlConvertForEPM(date){
    var dateArr = date.split("-");

    var day = dateArr[2];
    var year = dateArr[0];
    var month = dateArr[1];

    if(day >= 1 && day <=7){ day = 'W1';}
    if(day >= 8 && day <=14){ day = 'W2';}
    if(day >= 15 && day <=21){ day = 'W3';}
    if(day >=22){ day = 'W4';}

    if(month == 1){month = 'Januari'};
    if(month == 2){month = 'Februari'};
    if(month == 3){month = 'Maret'};
    if(month == 4){month = 'April'};
    if(month == 5){month = 'Mei'};
    if(month == 6){month = 'Juni'};
    if(month == 7){month = 'Juli'};
    if(month == 8){month = 'Agustus'};
    if(month == 9){month = 'September'};
    if(month == 10){month = 'Oktober'};
    if(month == 11){month = 'November'};
    if(month == 12){month = 'Desember'};


    var dateFormat = day +' '+ month +' '+ year;
    return dateFormat;
}

function reportPdf(){
    
    if($('#project').val() == '' || $('#project').val() == null){
            swal('Plase select project');
    }else{

        var param = $('#project').val();
        var param2 = $('#jenis_epm').val();

        window.open(burl+'report/pdf_report_catatan/catatan_evaluasi/'+param+'/'+param2);
    }
}

function reportHtml(){
   
    //clean content
    $('#reportContentWrapper').html('');

    if($('#project').val() == '' || $('#project').val() == null){
        swal('Plase select project');
        $('#reportContentWrapper').html('<h1 style="text-align: center;">No report to display</h1>');
    }else{

        loadingStart();

        //empty data in variable
        reportHtmlCover = '';
        reportHtmlContent = '';
        reportHtmlEat = '';
        reportHtmlManPower = '';
        reportHtmlData = '';

        $('#reportContentWrapper').html('');
        createReportContent();
    }
    
}

function createReportContent(){
    var id= $('#project').val();
    var type= $('#jenis_epm').val();
    var data= {'id_project': id,'jenis_epm' : type};
    data[csrf.name]=csrf.token;
    $.ajax({
          url : burl+"report/get_eval_kunjungan",
          type: "POST",
          data: data,
          dataType: "JSON",
          success: function(data)
          {
            console.log(data);
            if (data.dataInsp.length > 0) {
                  reportHtmlData = '<center><h3>Configuration Data Not Found</h3></center><br>';
                } else {
                  reportHtmlCover = '<h3 align="center">Evaluasi Kunjungan Outlet</h3><br><br>' +
                              '<table align="center" width="100%">' +
                              '  <thead>' +
                              '    <tr>' +
                              '      <th width="60%">' +
                              '         <table width="100%">' +
                              '             <tbody>' +
                              '                 <tr>' +
                              '                     <td style="padding: 10px">Nama Outlet</td>' +
                              '                     <td style="padding: 10px"> : </td>' +
                              '                     <td style="padding: 10px">'+data['dataProject'].nama_outlet+'</td>' +
                              '                 </tr>' +
                              '                 <tr>' +
                              '                     <td style="padding: 10px">Main Dealer</td>' +
                              '                     <td style="padding: 10px"> : </td>' +
                              '                     <td style="padding: 10px">'+data['dataProject'].main_dealer+'</td>' +
                              '                 </tr>' +
                              '                 <tr>' +
                              '                     <td style="padding: 10px">Tgl Kunjungan</td>' +
                              '                     <td style="padding: 10px"> : </td>' +
                              '                     <td style="padding: 10px">'+dateMySqlConvertMonthShort(data.dataEvaluasiKunjungan[0].tgl_kunjungan)+'</td>' +
                              '                 </tr>' +
                              '                 <tr>' +
                              '                     <td style="padding: 10px">Target Otoritasi</td>' +
                              '                     <td style="padding: 10px"> : </td>' +
                              '                     <td style="padding: 10px">'+dateMySqlConvertMonthShort(data['dataProject'].target_otorisasi)+'</td>' +
                              '                 </tr>' +
                              '             </tbody>' +
                              '           </table>' +
                              '        </th>' +
                              '      <th width="40%" colspan="3">' +
                              '        <table border="1" width="100%">' +
                              '          <thead>' +
                              '            <tr style="background-color:#99c0ff; color: #000">'+
                              '              <th style="padding: 5px; text-align: center;" colspan="4">Approval</th>'+
                              '            </tr>' +
                              '            <tr>' +
                              '             <th style="padding: 5px; text-align: center;">TAM</th>' +
                              '             <th style="padding: 5px; text-align: center;">Dealer</th>' +
                              '             <th style="padding: 5px; text-align: center;">Consultant</th>' +
                              '             <th style="padding: 5px; text-align: center;">Contractor</th>' +
                              '            </tr>' +
                              '          </thead>' +
                              '          <tbody>' +
                              '            <tr>' +
                              '              <td style="padding: 30px"></td>' +
                              '              <td style="padding: 30px"></td>' +
                              '              <td style="padding: 30px"></td>' +
                              '              <td style="padding: 30px"></td>' +
                              '            </tr>' +
                              '          </tbody>' +
                              '        </table>' +
                              '      </th>' +
                              '    </tr>' +
                              '  </thead>' +
                              '</table><br><br>';

            reportHtmlData = reportHtmlData + reportHtmlCover;
            //kriteria pekerjaan
                var totalItemEval = 0;
                var reportContentEval = '<table align="center" border="2" width="100%" border="1">' +
                                      '  <thead>' +
                                      '    <tr style="background-color:#99c0ff; color: #000">' +
                                      '      <th style="text-align: center;">No.</th>' +
                                      '      <th style="text-align: center;">Area</th>' +
                                      '      <th style="text-align: center;">Fasilitas</th>' +
                                      '      <th style="text-align: center;">Problem / Deskripsi</th>' +
                                      '      <th style="text-align: center;">Tindak lanjut</th>' +
                                      '      <th style="text-align: center;">Target</th>' +
                                      '      <th style="text-align: center;">PIC</th>' +
                                      '    </tr>' +
                                      '  </thead>';

                for(y=0;y<data.dataDetailEvaluasiKunjungan.length;y++){
                        
                        totalItemEval++;

                        reportContentEval = reportContentEval + '<tbody><tr>' +
                                                            '    <td style="padding: 7px">'+totalItemEval+'</td>' +
                                                            '    <td style="padding: 7px">'+data.dataDetailEvaluasiKunjungan[y].area_name+'</td>' +
                                                            '    <td style="padding-left:10px">' + data.dataDetailEvaluasiKunjungan[y].fungsi_outlet + '</td>';
                                                            if(data.dataDetailEvaluasiKunjungan[y].eval_problem_deskripsi==null){
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px"></td>';
                                                            }else{
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px">' + data.dataDetailEvaluasiKunjungan[y].eval_problem_deskripsi + '</td>';
                                                            }
                                                            if(data.dataDetailEvaluasiKunjungan[y].eval_tindak_lanjut==null){
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px"></td>';
                                                            }else{
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px">' + data.dataDetailEvaluasiKunjungan[y].eval_tindak_lanjut + '</td>';
                                                            }
                                                            if(data.dataDetailEvaluasiKunjungan[y].eval_target==null){
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px"></td>';
                                                            }else{
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px">' + data.dataDetailEvaluasiKunjungan[y].eval_target + '</td>';
                                                            }
                                                            if(data.dataDetailEvaluasiKunjungan[y].eval_pic==null){
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px"></td>';
                                                            }else{
                                                                reportContentEval = reportContentEval + '    <td style="padding-left:10px">' + data.dataDetailEvaluasiKunjungan[y].eval_pic + '</td>'+
                                                                '  </tr>';
                                                            }

                }

                reportContentEval = reportContentEval + '</tbody>' +
                                    '</table><br><br>';

                if(totalItemEval==0){reportContentEval = '';}

                reportHtmlData = reportHtmlData + reportContentEval;
                };

            $('#reportContentWrapper').html(reportHtmlData);

            // end
            loadingEnd();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            loadingEnd();
            console.log('Error');
            console.log(jqXHR.responseText);
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            swal('Error get data');
              // $('#btnSave').text('save'); //change button text
              // $('#btnSave').attr('disabled',false); //set button enable
              // $('#btnCancel').attr('disabled',true); //set button enable
          }
    });
}

jQuery(document).ready(function() {
    $('.select2').select2();
    
});