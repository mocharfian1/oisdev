/*
 *---------------------------------------------------
 * Nama Project               : E-Checklist (OIS)
 * Pemilik                    : PT Toyota Astra Motor
 * Nama Pengembang            : Ayub Anggara
 * Perusahaan Pengembang      : AFEDIGI
 * Tanggal Pengembangan       : 18 9 2018
 *---------------------------------------------------
 * Copyright (C) 2018 AFEDIGI - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of the license or permission from AFEDIGI.
 * For Contact Person please visit : https://afedigi.com/
 */

var reportHtmlCover = '';
var reportHtmlContent = '';
var reportHtmlEat = '';
var reportHtmlManPower = '';
var reportHtmlData = '';


function loadingStart(){
    $('#reportContentWrapper').fadeOut();
    $('#loaderWrapper').fadeIn();
    $('#btnGeneratePdf').text('please wait..'); //change button text
    $('#btnGenerateHtml').text('please wait..'); //change button text
    $('#btnGeneratePdf').attr('disabled',true); //set button disable
    $('#btnGenerateHtml').attr('disabled',true); //set button disable
}

function loadingEnd(){
    // $('#reportContentWrapper').html('<h1>No report to display</h1>');
    $('#reportContentWrapper').fadeIn();
    $('#loaderWrapper').fadeOut();
    $('#btnGeneratePdf').text('Generate PDF'); //change button text
    $('#btnGenerateHtml').text('Display Report'); //change button text
    $('#btnGeneratePdf').attr('disabled',false); //set button enable
    $('#btnGenerateHtml').attr('disabled',false); //set button enable
}

function dateMySqlConvertForEPM(date){
    var dateArr = date.split("-");

    var day = dateArr[2];
    var year = dateArr[0];
    var month = dateArr[1];

    if(day >= 1 && day <=7){ day = 'W1';}
    if(day >= 8 && day <=14){ day = 'W2';}
    if(day >= 15 && day <=21){ day = 'W3';}
    if(day >=22){ day = 'W4';}

    if(month == 1){month = 'Januari'};
    if(month == 2){month = 'Februari'};
    if(month == 3){month = 'Maret'};
    if(month == 4){month = 'April'};
    if(month == 5){month = 'Mei'};
    if(month == 6){month = 'Juni'};
    if(month == 7){month = 'Juli'};
    if(month == 8){month = 'Agustus'};
    if(month == 9){month = 'September'};
    if(month == 10){month = 'Oktober'};
    if(month == 11){month = 'November'};
    if(month == 12){month = 'Desember'};


    var dateFormat = day +' '+ month +' '+ year;
    return dateFormat;
}

function reportPdf(id){
  if(id==null){
    swal('Please select TOSS before generate pdf');
  }else{
    var param = null;
    var param2 = null;
    window.open(burl+'report/pdf_report_toss/'+id+'/pdf');
  }
}

function reportHtml(id){
  if(id == null){
    swal('Please select TOSS before display report');
  }else{

    //clean content
    $('#reportContentWrapper').html('');

      loadingStart();

      //empty data in variable
      reportHtmlCover = '';
      reportHtmlContent = '';
      reportHtmlEat = '';
      reportHtmlManPower = '';
      reportHtmlData = '';

      $('#reportContentWrapper').html('');
      $.get(URL+'report/pdf_report_toss/'+id+'/html').done(function(data){
          $('#reportContentWrapper').html(data);
          loadingEnd();
      }).fail();
    
  }
}

function createReportContent(){
    contentA = '<h3><b>A. ToSS Profile</b></h3>' +
    '<div class="form-group text-center">' +
      '<img class="img-thumbnail" src="'+burl+'assets/d1.png'+'" style="max-width: 500px">' +
      '<table border="1" style="margin-top: 20px" width="100%">' +
        '<tr>' +
          '<td style="padding: 10px; color: red; text-align: left"><b>Nama ToSS</b></td>' +
          '<td style="padding: 10px; color: red; text-align: left"><b> : CENTRAL MOTOR KEMAYORAN</b></td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Dealer</td>' +
          '<td style="padding: 10px; text-align: left"> : CENTRAL MOTOR</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Cabang Induk</td>' +
          '<td style="padding: 10px; text-align: left"> : CENTRAL MOTOR SUNTER</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Tipe Bangunan</td>' +
          '<td style="padding: 10px; text-align: left"> : RUKO</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Status Bangunan</td>' +
          '<td>' +
            '<table>' +
              '<tr>' +
                '<td style="padding: 10px; text-align: left"> : SEWA</td>' +
                '<td style="padding: 10px; text-align: left">Dari</td>' +
                '<td style="padding: 10px; text-align: left"> : 01-Jan-17</td>' +
                '<td style="padding: 10px; text-align: left">Sampai</td>' +
                '<td style="padding: 10px; text-align: left"> : 01-Jan-19</td>' +
              '</tr>' +
            '</table>' +
          '</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Alamat</td>' +
          '<td style="padding: 10px; text-align: left"> : JL. YOS SUDARSO, SUNTER II</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px" align="left">Kecamatan</td>' +
          '<td style="padding: 10px; text-align: left"> : TANJUNG PRIOK</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px" align="left">Kabupaten/Kota</td>' +
          '<td style="padding: 10px; text-align: left"> : JAKARTA UTARA</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px" align="left">Provinsi</td>' +
          '<td style="padding: 10px; text-align: left"> : DKI JAKARTA</td>' +
        '</tr>' +
        '<tr>' +
            '<td style="padding: 10px" align="left">Latitude</td>' +
            '<td style="padding: 10px; text-align: left"> : -6.1432525</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Longitude</td>' +
          '<td style="padding: 10px; text-align: left"> : 106.8902601</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Telp/Fax</td>' +
          '<td style="padding: 10px; text-align: left"> : (021) 651 5551</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left" colspan="2"><b>PIC ToSS</b></td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Nama</td>' +
          '<td style="padding: 10px; text-align: left"> : Bp. Toyota Astra Motor</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Jabatan</td>' +
          '<td style="padding: 10px; text-align: left"> : SA</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">No. HP</td>' +
          '<td style="padding: 10px; text-align: left"> : 0813497XXX</td>' +
        '<tr>' +
          '<td style="padding: 10px; text-align: left">Email</td>' +
          '<td style="padding: 10px; text-align: left"> : toyota@toyota.co.id</td>' +
        '</tr>' +
        
      '</table>' +
      '<br><br>' +
      '<center>'+
      '<table border="1" style="margin-top: 20px" width="70%">' +
      '  <thead>' +
      '    <tr>' +
      '      <td style="padding: 10px; text-align: center"><b><h4>Perwakilan Dealer</h4></b></td>' +
      '      <td style="padding: 10px; text-align: center"><b><h4>Perwakilan TAM</h4></b></td>' +
      '    </tr>' +
      '  </thead>' +
      '  <tbody>' +
      '    <tr>' +
      '      <td style="height: 200px"></td>' +
      '      <td></td>' +
      '    </tr>' +
      '    <tr>' +
      '      <td style="padding: 10px;text-align: center">Nama Perwakilan disini<br>Jabatan Disini</td>' +
      '      <td style="padding: 10px;text-align: center">Nama Perwakilan disini<br>Jabatan Disini</td>' +
      '    </tr>' +
      '  </tbody>' +
      '</table>' +
      '</center>'+
    '</div>';

    contentB = '<h3><b>B. Kapasitas Bengkel</b></h3>' +
    '<div class="form-group">' +
      '<table style="width: 700px" border="1">' +
        '<tr>' +
          '<td class="text-center" rowspan="6" width="30%"><h1 style="color: red"><b>2</b></h1><br><br>Jumlah Stall</td>' +
          '<td class="text-center" rowspan="6"><h1 style="color: red"><b>2</b></h1><br><br>Stall GR</td>' +
          '<td style="padding: 15px" width="40%"><b>Stall lain</b> :</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 5px;">' +
            'THS Motor : 5 Unit' +
          '</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 5px;">' +
            'THS Mobil : 15 Unit' +
          '</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 5px;">' +
            'Lain-lain : 10 Unit'+
          '</td>' +
        '</tr>' +
        '<tr>' +
          '<td style="padding: 5px;">' +
            'Keterangan : '+
          '</td>'+ 
        '</tr>' +
        '<tr>' +
          '<td style="padding: 5px">* Jika ada</td>' +
        '</tr>' +
      '</table><br>' +
      '<table border="1" style="font-size: 12px">' +
      '  <tr>' +
      '    <td align="center" width="15%" style="padding: 5px; padding-right: 20px; padding-left: 20px">Jumlah Manpower</td>' +
      '    <td align="center" width="25%" style="padding: 5px; padding-left: 15px; padding-right: 15px"><h2 style="color: red"><b>1</b></h2>Pro Tech</td>' +
      '    <td align="center" width="15%" style="padding: 5px; padding-left: 25px; padding-right: 25px"><h2 style="color: red"><b>1</b></h2>Tech</td>' +
      '    <td align="center" width="15%" style="padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b>1</b></h2>Admin</td>' +
      '    <td align="center" width="15%" style=" padding: 5px; padding-left: 20px; padding-right: 20px"><h2 style="color: red"><b>1</b></h2>Others</td>' +
      '    <td width="25%" style="padding: 5px;">' +
      '        <span>Catatan :</span>' +
      '        <br>' +
      '        <span align="center"><small>Pro Tech merangkap sebagai SA dan 1 staff adalah office boy merangkap security</small></span>' +
      '        <br>' +
      '        <span><small><b>Total Manpower : 4 Orang</b></small></span>' +
      '    </td>' +
      '  </tr>' +
      '</table>' +
    '</div>';

    contentC = '<h3><b>C. Target & Investasi</b></h3>' +
    '<div class="form-group">' +
        '<div class="portlet-title">' +
            '<div class="caption font-dark">' +
                '<span class="caption-subject bold uppercase">C1. TARGET</span>' +
            '</div>' +
        '</div><br>' +
        '<table border="1" width="100%" class="text-center">' +
          '<thead>' +
              '<tr>' +
                  '<th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Item</th>' +
                  '<th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y</th>' +
                  '<th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 1</th>' +
                  '<th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 2</th>' +
                  '<th width="20%" style="text-align: center; padding: 10px; background-color: grey; color: white">Y + 3</th>' +
              '</tr>' +
          '</thead>' +
          '<tbody>' +
              '<tr>' +
                  '<td style="padding: 3px">UE/Bulan</td>' +
                  '<td style="text-align: center;">120</td>' +
                  '<td style="text-align: center;">130</td>' +
                  '<td style="text-align: center;">140</td>' +
                  '<td style="text-align: center;">150</td>' +
              '</tr>' +
              '<tr>' +
                  '<td style="padding: 3px">UE/Hari</td>' +
                  '<td>5</td>' +
                  '<td>6</td>' +
                  '<td>6</td>' +
                  '<td>7</td>' +
              '</tr>' +
          '</tbody>' +
      '</table>' +
      '<br>' +
      '<b>Note :</b>' +
      '<br>' +
      '<p>Y = Tahun sertifikasi | 1 Bulan = 22.5 hari' +
      '<div class="portlet-title">' +
        '<div class="caption font-dark">' +
            '<span class="caption-subject bold uppercase">C2. Investasi</span>' +
        '</div>' +
      '</div><br>' +
      '<table border="1" width="80%">' +
          '<tbody>' +
              '<tr>' +
                  '<td style="padding: 5px" width="5%">Pembelian lahan *</td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="padding: 5px"> : IDR </td>' +
                              '<td align="right" style="padding-right: 15px"> - </td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
              '<tr>' +
                  '<td style="padding: 5px" width="5%">Sewa Bangunan</td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="padding: 5px"> : IDR </td>' +
                              '<td align="right" style="padding-right: 15px"> 100.000.000 </td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
              '<tr>' +
                  '<td style="padding: 5px" width="5%">Renovasi Bangunan</td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="padding: 5px"> : IDR </td>' +
                              '<td align="right" style="padding-right: 15px"> 43.000.000 </td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
              '<tr>' +
                  '<td style="padding: 5px" width="5%">Corporate Identity</td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="padding: 5px"> : IDR </td>' +
                              '<td align="right" style="padding-right: 15px"> 10.000.000 </td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
              '<tr>' +
                  '<td style="padding: 5px" width="5%">Equipment</td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="padding: 5px"> : IDR </td>' +
                              '<td align="right" style="padding-right: 15px"> 292.000.000 </td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
              '<tr>' +
                  '<td width="5%" style="padding: 5px; background-color: grey; color: white"><b>TOTAL</b></td>' +
                  '<td width="10%">' +
                      '<table width="100%">' +
                          '<tr>' +
                              '<td style="background-color: grey; color: white; padding: 5px"> : IDR </td>' +
                              '<td style="text-align: right; border: none; background-color: grey; color: white; padding-right: 15px">445.000.000</td>' +
                          '</tr>' +
                      '</table>' +
                  '</td>' +
              '</tr>' +
          '</tbody>' +
      '</table>' +
      '<br>' +
      '<b>Note :</b>' +
      '<br>' +
      '<p>* Apabila milik sendiri' +
    '</div>';

    contentD = '<h3><b>D. Checklist Sertifikasi</b></h3>' +
    '<div class="form-body">' +
        '<div class="portlet-title">' +
            '<div class="caption font-dark">' +
                '<span class="caption-subject bold uppercase">D1. Fasilitas Umum</span>' +
            '</div>' +
        '</div><br>'+
        '<div class="form-group">' +
          '<table border="1" width="100%" class="text-center">' +
              '<thead>' +
                  '<tr><th colspan="7" align="center" style="text-align: center; padding: 10px; background-color: grey; color: white">1. PERSPEKTIF & FACIA TOSS</th></tr>' +
              '</thead>' +
              '<tbody>' +
                  '<tr>' +
                      '<td width="3%" style="text-align: center; padding: 10px; background-color: #fffc7c">No.</td>' +
                      '<td width="45%" style="text-align: center; padding: 10px; background-color: #fffc7c">Kriteria Minimum</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Jumlah</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Foto Dealer</td>' +
                      '<td width="22%" style="text-align: center; padding: 10px; background-color: #fffc7c">Keterangan</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi Dealer</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi TAM</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">1.</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Memiliki Fascia Toyota & Dealer Name sesuai dengan standar*</td>' +
                      '<td></td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">2.</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Terdapat identitas Toyota Service Station yang mudah dibaca oleh pelanggan</td>' +
                      '<td></td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
              '</tbody>' +
          '</table>' +
      '</div>' +
      '<div class="form-group">' +
          '<table border="1" width="100%" class="text-center">' +
              '<thead>' +
                  '<tr><th colspan="7" align="center" style="text-align: center; padding: 10px; background-color: grey; color: white">2. AREA TUNGGU PELANGGAN SERVIS</th></tr>' +
              '</thead>' +
              '<tbody>' +
                  '<tr>' +
                      '<td width="3%" style="text-align: center; padding: 10px; background-color: #fffc7c">No.</td>' +
                      '<td width="45%" style="text-align: center; padding: 10px; background-color: #fffc7c">Kriteria Minimum</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Jumlah</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Foto Dealer</td>' +
                      '<td width="22%" style="text-align: center; padding: 10px; background-color: #fffc7c">Keterangan</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi Dealer</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi TAM</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">1</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Posisi pelanggan dapat melihat ke area bengkel</td>' +
                      '<td></td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Terdapat pula TV yang dilengkapi kamera untuk melihat bengkel</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">2</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas Pendukung Meja dan Kursi untuk pelangan</td>' +
                      '<td>4</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">1 meja 3 kursi kap. 6 org</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">3</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas Pendukung Food & Beverage(Min Air Mineral)</td>' +
                      '<td></td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Camilan & Air mineral</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
             '</tbody>' +
          '</table>' +
     '</div>' +
      '<div class="portlet-title">' +
          '<div class="caption font-dark">' +
              '<span class="caption-subject bold uppercase">D2. Area Pelanggan</span>' +
          '</div>' +
      '</div><br>'+
     '<div class="form-group">' +
         '<table border="1" width="100%" class="text-center">' +
              '<thead>' +
                  '<tr><th colspan="7" align="center" style="text-align: center; padding: 10px; background-color: grey; color: white">1. AREA PENERIMAAN SERVIS</th></tr>' +
              '</thead>' +
              '<tbody>' +
                  '<tr>' +
                      '<td width="3%" style="text-align: center; padding: 10px; background-color: #fffc7c">No.</td>' +
                      '<td width="45%" style="text-align: center; padding: 10px; background-color: #fffc7c">Kriteria Minimum</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Jumlah</td>' +
                      '<td width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Foto Dealer</td>' +
                      '<td width="22%" style="text-align: center; padding: 10px; background-color: #fffc7c">Keterangan</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi Dealer</td>' +
                      '<td width="10%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi TAM</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">1</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung meja untuk SA & Admin</td>' +
                      '<td>2</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">1 meja kapasitas 4 org</td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">2</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung kursi untuk SA & Admin</td>' +
                      '<td>2</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">3</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung kursi untuk Pelanggan</td>' +
                      '<td>2</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">4</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung cost Estimation Board Service</td>' +
                      '<td>1</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td style="padding: 10px; vertical-align: top"></td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">5</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung komputer</td>' +
                      '<td>1</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">6</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung printer</td>' +
                      '<td>1</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top"></td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
                  '<tr>' +
                      '<td style="padding: 10px; vertical-align: top">7</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">Fasilitas pendukung telephone</td>' +
                      '<td>1</td>' +
                      '<td><img class="img-thumbnail" src="'+burl+'upload/default.jpg'+'" style="max-width: 500px">'+'</td>' +
                      '<td align="left" style="padding: 10px; vertical-align: top">No. Kontak PIC ToSS</td>' +
                       '<td style="padding: 10px; vertical-align: top">V</td>' +
                      '<td style="padding: 10px; vertical-align: top">V</td>' +
                  '</tr>' +
             '</tbody>' +
         '</table>' +
     '</div>' +
   
      '<div class="portlet-title">' +
          '<div class="caption font-dark">' +
              '<span class="caption-subject bold uppercase">D3. Manpower</span><br><br>' +
              '<span class="caption-subject bold uppercase">A. Klasifikasi SDM After Sales</span>' +
          '</div>' +
      '</div><br>'+
      '<div class="form-group">'+
        '<table border="1" width="100%" class="text-center">'+
            '<thead>'+
                '<tr>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">No.</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Nama Karyawan</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Tgl Masuk</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">No. KTP</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Jabatan</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="10">Sertifikasi</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Keterangan</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Evaluasi Dealer</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="4">Evaluasi TAM</th>'+
                '</tr>'+
                '<tr>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="7">Teknisi</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" rowspan="2" colspan="3">SA GR</th>'+
                '</tr>'+
                '<tr>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="2">Level 1</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="3">Level 2</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c" colspan="2">Level 3</th>'+
                '</tr>'+
                '<tr>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">TT</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">PT</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">DTG</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">DTL</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">DTC</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">DMT</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">LD</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">L1</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">L2</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">L3</th>'+
                '</tr>'+
            '</thead>'+
            '<tbody>'+
                '<tr>'+
                    '<td width="5%">1.</td>'+
                    '<td width="15%">Krisna Kurnia Perdana</td>'+
                    '<td width="10%">09-05-2016</td>'+
                    '<td width="15%">3520151204940002</td>'+
                    '<td width="10%">Tech</td>'+
                    '<td width="2%">V</td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="15%"></td>'+
                    '<td width="7%">V</td>'+
                    '<td width="7%">V</td>'+
                '</tr>'+
                '<tr>'+
                    '<td width="5%">2.</td>'+
                    '<td width="15%">Sukron Karim</td>'+
                    '<td width="10%">18-02-2016</td>'+
                    '<td width="15%">3529010305960006</td>'+
                    '<td width="15%">Pro Tech</td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%">V</td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="2%"></td>'+
                    '<td width="15%">Merangkap SA</td>'+
                    '<td width="7%">V</td>'+
                    '<td width="7%">V</td>'+
                '</tr>'+
            '</tbody>'+
        '</table><br><br>'+

        
        'Keterangan :<br><br>'+
        
        '<table>' +
        '<tr><td>'+
        '<table width="100%">'+
           
            '<tbody>'+
                '<tr>'+
                    '<td colspan="2"><u>1. TRAINING TEKNISI</u><br><br></td>'+
                '</tr>'+
                '<tr>'+
                    '<td>TT </td>'+
                    '<td>: Toyota Technician</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>PT </td>'+
                    '<td>: Pro Technician</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>DT </td>'+
                    '<td>: Diagnostic Technician</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>DTG </td>'+
                    '<td>: Diagnostic Technician Engine</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>DTL </td>'+
                    '<td>: Diagnostic Technician Electrica</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>DTC </td>'+
                    '<td>: Diagnostic Technician Chassis</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>DMT </td>'+
                    '<td>: Diagnosis Master Technician</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>LD </td>'+
                    '<td>: Latest Diagnosis</td>'+
                '</tr>'+
            '</tbody>'+
        '</table>'+
        '</td>'+
        '<td style="vertical-align: top; padding-left: 20px">'+
        '<table>'+
            '<tbody>'+
                '<tr>'+
                    '<td colspan="2"><u>2. SERVICE ADVISOR GR</u><br><br></td>'+
                '</tr>'+
                '<tr>'+
                    '<td>L1 </td>'+
                    '<td>: Level 1 TS A21</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>L2 </td>'+
                    '<td>: Level 2 TS A21</td>'+
                '</tr>'+
                '<tr>'+
                    '<td>L3 </td>'+
                    '<td>: Level 3 TS A21</td>'+
                '</tr>'+
            '</tbody>'+
        '</table>'+
        '</td></tr>'+
        '</table>'+

      '</div><br>'+

      '<div class="portlet-title">' +
          '<div class="caption font-dark">' +
              '<span class="caption-subject bold uppercase">B. Klasifikasi SDM Umum</span>' +
          '</div>' +
      '</div><br>'+
      '<div class="form-group">'+
        
        '<table border="1" style="width: 800px" class="text-center">'+
            '<thead>'+
                '<tr>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">No.</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">Nama Karyawan</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">Tgl Masuk</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">Jabatan</th>'+
                    '<th style="text-align: center; padding: 5px; background-color: #fffc7c">Keterangan</th>'+
                '</tr>'+
            '</thead>'+
            '<tbody>'+
                '<tr>'+
                    '<td width="5%">1.</td>'+
                    '<td width="15%">Krisna Kurnia Perdana</td>'+
                    '<td width="10%">09-05-2016</td>'+
                    '<td width="15%">Admin</td>'+
                    '<td width="15%">Admin & Kasir</td>'+
                '</tr>'+
                '<tr>'+
                    '<td width="5%">2.</td>'+
                    '<td width="15%">Sukron Karim</td>'+
                    '<td width="10%">18-2-2016</td>'+
                    '<td width="15%">Others</td>'+
                    '<td width="15%">Security & Office Boy</td>'+
                '</tr>'+
            '</tbody>'+
        '</table>'+
      '</div>' + 
        
            
        
      '<div class="portlet-title">' +
          '<div class="caption font-dark">' +
              '<span class="caption-subject bold uppercase">D4. Equipment</span>' +
          '</div>' +
      '</div><br>'+
      '<div class="form-group">' +
        '<table border="1" width="100%" class="text-center">' +
            '<thead>' +
                '<tr>' +
                    '<th width="3%" style="text-align: center; padding: 10px; background-color: #fffc7c">No.</th>' +
                    '<th width="45%" style="text-align: center; padding: 10px; background-color: #fffc7c">Kriteria Minimum</th>' +
                    '<th width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Jumlah Min.</th>' +
                    '<th width="5%" style="text-align: center; padding: 10px; background-color: #fffc7c">Foto Dealer</th>' +
                    '<th width="15%" style="text-align: center; padding: 10px; background-color: #fffc7c">Keterangan</th>' +
                    '<th width="15%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi Dealer</th>' +
                    '<th width="15%" style="text-align: center; padding: 10px; background-color: #fffc7c">Evaluasi TAM</th>' +
                '</tr>' +
            '</thead>' +
            '<tbody>' +
                '<tr>' +
                    '<td colspan="7" style="text-align: center; padding: 10px; background-color: grey; color: white">Power Equipment Tools</td>' +
                '</tr>' +
                '<tr>' +
                    '<td style="padding: 10px; vertical-align: top">1.</td>' +
                    '<td align="left" style="padding: 10px; vertical-align: top">Air Comp. Min. 2 PK</td>' +
                    '<td style="padding: 10px; vertical-align: top">1 Set</td>' +
                    '<td>' +
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/1.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/1.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/1.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/1.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/1.png'+'" style="max-width: 300px"><br><br>'+
                    '</td>' +
                    '<td style="padding: 10px; vertical-align: top"></td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                '</tr>' +
                '<tr>' +
                    '<td style="padding: 10px; vertical-align: top">2.</td>' +
                    '<td align="left" style="padding: 10px; vertical-align: top">Air House Rail & Kabel row</td>' +
                    '<td style="padding: 10px; vertical-align: top">1 Set</td>' +
                    '<td>' +
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/2.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/2.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/2.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/2.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/2.png'+'" style="max-width: 300px"><br><br>'+
                    '</td>' +
                    '<td style="padding: 10px; vertical-align: top"></td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                '</tr>' +
                '<tr>' +
                    '<td colspan="7" style="text-align: center; padding: 10px; background-color: grey; color: white">Lifting And Garage Equipment</td>' +
                '</tr>' +
                '<tr>' +
                    '<td style="padding: 10px; vertical-align: top">3.</td>' +
                    '<td align="left" style="padding: 10px; vertical-align: top">Hydraullic Garage Jack ST</td>' +
                    '<td style="padding: 10px; vertical-align: top">1 Pcs</td>' +
                    '<td>' +
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/3.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/3.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/3.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/3.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/3.png'+'" style="max-width: 300px"><br><br>'+
                    '</td>' +
                    '<td style="padding: 10px; vertical-align: top"></td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                '</tr>' +
                '<tr>' +
                    '<td style="padding: 10px; vertical-align: top">4.</td>' +
                    '<td align="left" style="padding: 10px; vertical-align: top">Jack Stand 3T</td>' +
                    '<td style="padding: 10px; vertical-align: top">4 Pcs</td>' +
                    '<td>' +
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/4.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/4.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/4.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/4.png'+'" style="max-width: 300px"><br><br>'+
                      '<img class="img-thumbnail" src="'+burl+'upload/toss_equipment/4.png'+'" style="max-width: 300px"><br><br>'+
                    '</td>' +
                    '<td style="padding: 10px; vertical-align: top"></td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                    '<td style="padding: 10px; vertical-align: top">V</td>' +
                '</tr>' +
            '</tbody>' +
        '</table>' +
    '</div>' +
    '</div>';

    reportHtmlData = contentA+contentB+contentC+contentD;
    // +contentC+contentD1+contentD2+contentD3+contentD4+contentD5+contentD6
    $('#reportContentWrapper').html(reportHtmlData);

    loadingEnd();
}

jQuery(document).ready(function() {
    $('.select2').select2();
    
});
